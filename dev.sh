#!/bin/bash
osascript <<END
tell application "Terminal" to activate
tell application "Terminal" 
  do script "cd \"`pwd`\";cd client; npm run client" in selected tab of the front window
end tell
tell application "System Events" to keystroke "t" using {command down}
tell application "Terminal" 
  do script "cd \"`pwd`\";sleep 1; cd servicelib; npm run test:watch" in selected tab of the front window
end tell
#tell application "System Events" to keystroke "t" using {command down}
#tell application "Terminal"
#  do script "cd \"`pwd`\";sleep 2; cd client; sudo npm run server" in selected tab of the front window
#end tell
END
