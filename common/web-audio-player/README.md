Web-Audio-Player (WAP)
=======================

### Introduction 

This project builds a headless, audio player for SiriusXM. Since HTML5 Audio APIs are currently *immature* and do not support HLS playback and/or encrypted media, a Flash-based engine will be used for the core HLS functionality, chunk decryption, and actual audio output.  

It should be noted that the Flash player `web-audio-player.swf` does not provide any GUI and is intended for use  within applications as a **headless-player** to provide music playback [only] for SiriusXM HLS v3 sources. 

To integrate the AS3 playback functionality into AngularJS HTML5 applications, several processes must occur:

*  A SWF wrapper must be used to expose external functionality contained within the core HLS.v3 library and establish a *Javascript-to-Flash* bridge. This SWF will include both the [Web-Audio-Core](https://github.siriusxm.com/WebClientTeam/as3-audio-player/tree/master/) audio v3 playback functionality and the [AS3 SWFService](https://github.com/CodeCatalyst/SWFService) bridge code used to publish Javascript access to the AS3 features.

*  An AngularJS module that loads the AS3 Player SWF, performs a lookup of the AS3 Player service instance,  publishes an Javascript Player service instance that can be injected into other HTML5 modules and controllers.

Since this project includes both **ActionScript** code to build the `web-audio-player.swf` and **JavaScript** code to the manage bridges and facades for the SiriusXM audio playback features, the repository has been partitioned into two (2) distinct sections.

**NOTE**: All of the code herein is psudeo-code and represents high-level designs for the AS3 and JS player code base; simply put, implementaion details are likely different.

---

### Actionscript3

This section of the repository [Web-Audio-Player / Actionscript](https://github.siriusxm.com/WebClientTeam/web-audio-player/actionscript) provides all the code for the application wrapper and construction of the `web-audio-player.swf`. 

The `WebAudioPlayer.mxml` application will instantiate the `PlayerService` and register it with the SWFBridge:

```js
/**
     * Build SiriusXM services
     */
    playerService = new PlayerService();
    
    /**
     * Register the service instance with the SWFBridge
     */
    SWFService.register( "Sirius.PlayerService", playerService );
    
```

**Note** that the same service name must be used to lookup the service from the AngularJS/Javascript client applications.

The `PlayService` provides a *promise-returning* API and internally wraps an instance of `sirius.hls.v3.Player`.

```js
package sirius.services
{
    /**
     * Implements a high-level API for audio playback, to be exposed as a
     * remote service that can be called by JavaScript.
     * @see sirius.services.events.PlayerServiceEvent
     */
    public class PlayerService
    {

        // *********************************************************
        // Public Properties
        // *********************************************************

        public function set volume( value:Number ):void
        {
            //...
        }

        /**
         * Constructor
         * @param config
         */
        public function PlayerService( )
        {
            _player = new sirius.hls.v3.Player( );
        }

        /**
         * Play the specified channel; at the specified
         * start time. 
         *
         * @param manifestURL String specifying URI for variant or playlist manifest
         * @param zuluStart   Targeted playhead UTC time (which chunk file should play 1st)
         * @return Promise    to return the playlist start, play, and end dates (zulu and local)
         */
        public function play( manifestURL:String, zuluStart:Date = null ):Promise
        {
            return _player.play( manifestURL, zuluStart );
        }

        /**
         * Pause the Player (if playing ):
         * @return Promise
         */
        public function pause():Promise
        {
            //...
        }

        /**
         * Stop the Player (if playing )
         * @return Promise
         */
        public function stop():Promise
        {
            //...
        }

    }
}
```

*  Source code in the repository [Web-Audio-Core-AS3](https://github.siriusxm.com/WebClientTeam/as3-audio-player)
*  Library SWC bytecode in the `sxm-audio-core.swc`

---

### Javascript

This section of the repository [Web-Audio-Player / Actionscript](https://github.siriusxm.com/WebClientTeam/web-audio-player/actionscript) provides all the code responsible for loading of the SWF, initializing the bridge and publishing a javascript Player service as an injectable Javascript audio player faćade. 



---

### Bridging the AS3/Flash-to-Javscript boundary

Communicating between the AS3/Flash environment and the Javscript environment is non-trivial. Simple communications can be achieved with `ExternalInterface`. For more sophisticated elegant communications, another solution is needed.

A special open-source library [SwfService](https://github.com/CodeCatalyst/SWFService) is used to dramatically simplify bridge management and support automatic two-way (2-way) communication between Javascript and AS3 instances. This bridge understands and manages promises, data-binding notifications, events, and links to [Bindable] properties.

**Note** that the same service name must be used to both register the AS3 service and to lookup the service instance for the AngularJS/Javascript client applications.

---

### Integration of Web-Audio-Player 

To integration the Web-Audio-Player component into the Web Client Application ( AngularJS with headless flash component ), review the [Integration Process](https://github.siriusxm.com/WebClientTeam/web-client-app/wiki/Player) notes.

