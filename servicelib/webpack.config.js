/**
 * This file contains the entry point for configuring webpack.
 *
 * This is the "advanced" config technique that is specified in the webpack 2 documentation, see link below
 * https://webpack.js.org/guides/production/#advanced-approach
 *
 */
module.exports = function (env) {
  const Merge           = require("webpack-merge");
  const commonConfig    = require("./webpack.common.js");
  const buildTypeConfig = require(`./webpack.${env}.js`);

  return Merge(commonConfig, buildTypeConfig);
};
