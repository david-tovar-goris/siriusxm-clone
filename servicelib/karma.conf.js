// // Karma configuration file, see link for more information
// // https://karma-runner.github.io/0.13/config/configuration-file.html
module.exports = function (config) {
    config.set({
        basePath: "./",
        port: 9877,
        frameworks: ["jasmine", "karma-typescript"],
        client:{
            // leave Jasmine Spec Runner output visible in browser
            clearContext: false,
            captureConsole: true
        },
        files: [
            {pattern: "src/**/*.ts"},
            './src/test/mocks/global-variable.mock.js'
        ],
        preprocessors: {
            "**/*.ts": ["karma-typescript"],
        },
        coverageIstanbulReporter: {
            reports: [ 'html', 'lcovonly' ],
            fixWebpackSourcePaths: true
        },
        plugins: [
            require('karma-typescript'),
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-jasmine-html-reporter'),
            require('karma-coverage-istanbul-reporter')
        ],
        reporters: ["progress", "karma-typescript","kjhtml"],
        browsers: ["Chrome"],
        colors: true,
        karmaTypescriptConfig: {
            compilerOptions: {
                baseUrl: "./",
                lib: [
                    "es6", // Fix for "'Promise' only refers to a type" error.
                    "dom"
                ],
                paths: {
                    "app/*": ["./../client/src/app/*"],
                    "sxmServices": ["./../servicelib"],
                    "sxm-audio-player": ["../client/node_modules/sxm-audio-player/bin/web-audio-player.js"]
                }
            }
        }
    });
};
