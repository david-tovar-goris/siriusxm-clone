/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import {
    addProvider,
    findProvider,
    providers
}                              from "./providerDescriptors";
import { IProviderDescriptor } from "./provider.descriptor.interface";

interface ThisContext
{
    providerA: IProviderDescriptor;
}

class TestClassA
{
    private static providerDescriptor: IProviderDescriptor = function ()
    {
        return addProvider(TestClassA, TestClassA, []);
    }();
    constructor() {}
}

describe("Provider Descriptors test suite >>", () =>
{
    beforeEach(function (this: ThisContext)
               {
                   this.providerA = providers.find((provider : IProviderDescriptor) : boolean =>
                                                   { return provider.useClass === TestClassA; });
               });

    it('findProvider returns the correct singleton for a class provider descriptor', function (this: ThisContext)
    {
        const providerDescriptor = findProvider(TestClassA);

        expect(providerDescriptor).toBe(this.providerA);
    });
});
