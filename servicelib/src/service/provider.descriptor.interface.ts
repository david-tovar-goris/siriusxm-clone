/**
 * @MODULE:     service-lib
 * @CREATED:    07/19/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *      Defines the interface for a descriptor for a provider; name, class, and dependency injection requirements
 */
export interface IProviderDescriptor
{
    provide : any;
    useClass : any;
    singleton? : boolean; // optional, true or undefined means this is a singleton
    deps : Array<Function>;
}
