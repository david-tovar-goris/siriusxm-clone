/**
 * This is known as a barrel file. It allows developers to package and rollup multiple exports
 * into a single file and thus single import/export; e.g., `export * from "./authentication/service/";`
 */
export * from "./consts";
export * from "./types";
export * from "./provider.descriptor.interface";
export * from "./providerDescriptors";
export * from "./service.factory";
