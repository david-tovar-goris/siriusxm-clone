/// <reference path="../../../node_modules/@types/jasmine/index.d.ts" />
import * as _ from "lodash";
import { TokenStrategies } from "./token.strategies";
import { ICdnAccessToken } from "../../http/http.provider.response.interceptor";

describe("Token Strategies Test Suite >>", function()
{
    const additionString = "?test";
    const expectedAdditionalString = "&test";
    const testAkamaiUrl = "http://Siriusxm.livetest.akamaized.net/";
    const testlimeLightUrl = "http://Siriusxm.altcom.hs.llnwd.net/";
    const startegyNotFoundUrl = "siriusxm.com";
    const cdnAccessToken: ICdnAccessToken =
              {
                  akamai: "1506009602_b2966524608a5fd016c49ba375de85a9",
                  limeLight: "cf=1506009602&p=57&cd=3600&e=1506009602&h=cceea9bc4eb613766ac0d69e8c47bd31"
              };
    describe("Infrastructure >> ", function()
    {
        it("Should have a test subject.", function()
        {
            expect(TokenStrategies).toBeDefined();
        });
    });

    describe("`getTokenStrategy()` >> ", function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(TokenStrategies.getTokenStrategy).toBeDefined();
                expect(_.isFunction(TokenStrategies.getTokenStrategy)).toBeTruthy();
            });
        });

        describe("Execution >> ", function()
        {
            it("Should return the strategy if strategy found.", function()
            {
                const expectedValue = {
                    urlPattern: "livetest.akamaized.net",
                    strategy: TokenStrategies.API.AKTOKEN_STRATEGY,
                    keyPattern: "/key/",
                    assetStartPattern: "/AAC",
                    invalidTokenHTTPCode: 403
                };
                const value = TokenStrategies.getTokenStrategy(testAkamaiUrl);
                expect(value).toEqual(expectedValue);
            });

            it("Should return null if the strategy not found.", function()
            {
                const value = TokenStrategies.getTokenStrategy(startegyNotFoundUrl);
                expect(value).toBe(null);
            });
        });
    });

    describe("`getTokenizedUrl()` >> ", function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(TokenStrategies.getTokenizedUrl).toBeDefined();
                expect(_.isFunction(TokenStrategies.getTokenizedUrl)).toBeTruthy();
            });
        });

        describe("Execution >> ", function()
        {
            it("Should return the tokenized URL for akamai strategy.", function()
            {
                const value = TokenStrategies.getTokenizedUrl(testAkamaiUrl + additionString, cdnAccessToken);
                expect(value)
                    .toEqual(testAkamaiUrl + "?token=" + cdnAccessToken.akamai + expectedAdditionalString);
            });

            it("Should return the tokenized URL for limeLight strategy.", function()
            {
                const value = TokenStrategies.getTokenizedUrl(testlimeLightUrl, cdnAccessToken);
                expect(value).toEqual(testlimeLightUrl + "?" + cdnAccessToken.limeLight);
            });

            it("Should return the  URL when token is empty.", function()
            {
                const cdnAccessToken: ICdnAccessToken =
                          {
                              akamai: "",
                              limeLight: ""
                          };
                const value = TokenStrategies.getTokenizedUrl(testlimeLightUrl, cdnAccessToken);
                expect(value).toEqual(testlimeLightUrl);
            });

            it("Should return the  URL when cdnToken is null is empty.", function()
            {
                const cdnAccessToken: ICdnAccessToken = null;
                const value = TokenStrategies.getTokenizedUrl(testlimeLightUrl, cdnAccessToken);
                expect(value).toEqual(testlimeLightUrl);
            });

            it("Should return the  URL when strategy not found.", function()
            {
                const value = TokenStrategies.getTokenizedUrl(startegyNotFoundUrl, cdnAccessToken);
                expect(value).toEqual(startegyNotFoundUrl);
            });
        });
    });
});
