export interface IDeepLinkData
{
    id: string;
    type: string;
}
