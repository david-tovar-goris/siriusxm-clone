export interface IConnectInfo
{
    email : string;
    facebook : string;
    phone : string;
    twitter : string;
}
