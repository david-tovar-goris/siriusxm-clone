import {
    IMediaVideo,
    IMediaCut,
    IMediaEpisode,
    IMediaSegment,
    IMediaShow
} from "../../tune/tune.interface";

export interface IMarkerList
{
    layer: string;
    markers: Array<IMarker>;
}

export interface IMarker
{
    assetGUID: string;
    duration?: number;
    layer: string;
    time: number;
    zeroStartTime?: any;
    timestamp: { absolute: string };
}

export interface ICutMarker extends IMarker, IMediaCut
{
    consumptionInfo: string;
    containerGUID: string;
    galaxyAssetId? : string;
    cut?: IMediaCut; // API puts extra cut info here, will get flattened and then removed during normalization
}

export interface IEpisodeMarker extends IMarker, IMediaEpisode
{
    episode?: IMediaEpisode;
    dataSiftStreamId? : string;
}

export interface ISegmentMarker extends IMarker, IMediaSegment
{
    containerGUID: string;
    segment?: IMediaSegment;
}

export interface IShowMarker extends IMarker, IMediaShow
{
    show?: IMediaShow;
    containerGUID ? : string;
}

export interface IVideoMarker extends IMarker
{
    video?: IMediaVideo; // API puts extra cut info here, will get flattened and then removed during normalization
}
