export interface ILegacyId
{
    siriusXMId?: string;
    pid?: string;
    shortId?: string;
}
