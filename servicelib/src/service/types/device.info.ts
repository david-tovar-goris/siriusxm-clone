export interface IDeviceInfo
{
    osVersion : string;
    platform : string;
    sxmAppVersion : string;
    browser : string;
    browserVersion : string;
    appRegion : string;
    deviceModel : string;
    clientDeviceId : string;
    isMobile: boolean;
}
