export interface IPausePoint
{
    contentType : string;
    channelId : string;
    channelName: string;
    timestamp : number;
    caId? : string;
    publicInfoIdentifier?: string;
}
