/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import {addProvider}         from "./providerDescriptors";
import {IProviderDescriptor} from "./provider.descriptor.interface";
import {ServiceFactory}      from "./service.factory";

interface ThisContext
{
    classCInstance: TestChildClassC;
    classBSingleton: TestChildClassB;
    classASingleton: TestChildClassA;
    testClassSingleton: TestClass;
}

class TestChildClassC
{
    private static providerDescriptor: IProviderDescriptor = function ()
    {
        return addProvider(TestChildClassC, TestChildClassC, [], false);
    }();

    public testProperty = "test";

    constructor() {}
}

class TestChildClassB
{
    private static providerDescriptor: IProviderDescriptor = function ()
    {
        return addProvider(TestChildClassB, TestChildClassB, [TestChildClassC]);
    }();

    constructor(public childC: TestChildClassC) {}
}

class TestChildClassA
{
    private static providerDescriptor: IProviderDescriptor = function ()
    {
        return addProvider(TestChildClassA, TestChildClassA, [
            TestChildClassB,
            TestChildClassC
        ]);
    }();

    constructor(public childB: TestChildClassB,
                public childC: TestChildClassC)
    {}
}

class TestClass
{
    private static providerDescriptor: IProviderDescriptor = function ()
    {
        return addProvider(TestClass,
                           TestClass, [
                               TestChildClassA,
                               TestChildClassB,
                               TestChildClassC
                           ]);
    }();

    constructor(public childA: TestChildClassA,
                public childB: TestChildClassB,
                public childC: TestChildClassC)
    {}
}

class TestClassD
{
    private static deps: Function[] = [Function];

    private static providerDescriptor: IProviderDescriptor = function ()
    {
        return addProvider(TestClassD,
                           TestClassD, TestClassD.deps);
    }();

    public static addDep(classConstructor: Function)
    {
        TestClassD.deps.push(classConstructor);
    }

    constructor(public child: TestClassWithCircDep) {}
}

class TestClassWithCircDep
{
    private static providerDescriptor: IProviderDescriptor = function ()
    {
        return addProvider(TestClassWithCircDep,
                           TestClassWithCircDep, [TestClassD]);
    }();

    constructor(public child: TestClassD) {}
}

class TestClassAWithTypescriptCircDep
{
    private static providerDescriptor: IProviderDescriptor = function ()
    {
        return addProvider(TestClassAWithTypescriptCircDep,
                           TestClassAWithTypescriptCircDep, [TestClassBWithTypescriptCircDep], false);
    }();

    constructor(public child: TestClassBWithTypescriptCircDep) {}
}

class TestClassBWithTypescriptCircDep
{
    private static providerDescriptor: IProviderDescriptor = function ()
    {
        return addProvider(TestClassBWithTypescriptCircDep,
                           TestClassBWithTypescriptCircDep, [TestClassAWithTypescriptCircDep], false);
    }();

    constructor(public child: TestClassAWithTypescriptCircDep) {}
}

class BogusClass {constructor() {}}

describe("ServiceFactory class test suite >>", () =>
{
    beforeEach(function (this: ThisContext)
               {
                   this.classCInstance     = ServiceFactory.getInstance(TestChildClassC) as TestChildClassC;
                   this.classBSingleton    = ServiceFactory.getInstance(TestChildClassB) as TestChildClassB;
                   this.classASingleton    = ServiceFactory.getInstance(TestChildClassA) as TestChildClassA;
                   this.testClassSingleton = ServiceFactory.getInstance(TestClass) as TestClass;
               });

    it('returns undefined for an invalid class constructor', function (this: ThisContext)
    {
        const providerDescriptor = ServiceFactory.getInstance(BogusClass);

        expect(providerDescriptor).not.toBeDefined();
    });

    it('creates a singleton for a class with no dependencies', function (this: ThisContext)
    {
        const singleton = ServiceFactory.getInstance(TestChildClassC);

        expect(singleton).toBeDefined();
        expect(singleton instanceof TestChildClassC).toBe(true);
        expect((singleton as TestChildClassC).testProperty).toEqual("test");
    });

    it('creates a singleton for a class with one dependency', function (this: ThisContext)
    {
        const singleton = ServiceFactory.getInstance(TestChildClassB);

        expect(singleton).toBeDefined();
        expect(singleton instanceof TestChildClassB).toBe(true);
        expect((singleton as TestChildClassB).childC).not.toBe(this.classCInstance);
    });

    it('creates a singleton for a class with two dependencies', function (this: ThisContext)
    {
        const singleton = ServiceFactory.getInstance(TestChildClassA);

        expect(singleton).toBeDefined();
        expect(singleton instanceof TestChildClassA).toBe(true);
        expect((singleton as TestChildClassA).childB).toBe(this.classBSingleton);
        expect((singleton as TestChildClassA).childC).not.toBe(this.classCInstance);
    });

    it('creates a singleton for a class with three dependencies', function (this: ThisContext)
    {
        const singleton = ServiceFactory.getInstance(TestClass);

        expect(singleton).toBeDefined();
        expect(singleton instanceof TestClass).toBe(true);
        expect((singleton as TestClass).childA).toBe(this.classASingleton);
        expect((singleton as TestClass).childB).toBe(this.classBSingleton);
        expect((singleton as TestClass).childC).not.toBe(this.classCInstance);
    });

    it('only creates one instance of a singleton', function (this: ThisContext)
    {
        expect(ServiceFactory.getInstance(TestChildClassC)).not.toBe(this.classCInstance);
        expect(ServiceFactory.getInstance(TestChildClassB)).toBe(this.classBSingleton);
        expect(ServiceFactory.getInstance(TestChildClassA)).toBe(this.classASingleton);
        expect(ServiceFactory.getInstance(TestClass)).toBe(this.testClassSingleton);
    });

    it('detects actual circular dependencies and returns undefined when cycle is detected',
       function (this: ThisContext)
    {
        TestClassD.addDep(TestClassWithCircDep);

        expect(ServiceFactory.getInstance(TestClassWithCircDep)).toBeUndefined();
    });

    it('detects Typescript circular dependencies and returns undefined when cycle is detected',
       function (this: ThisContext)
       {
           TestClassD.addDep(TestClassWithCircDep);

           expect(ServiceFactory.getInstance(TestClassBWithTypescriptCircDep)).toBeUndefined();
           expect(ServiceFactory.getInstance(TestClassAWithTypescriptCircDep)).toBeUndefined();
       });

});
