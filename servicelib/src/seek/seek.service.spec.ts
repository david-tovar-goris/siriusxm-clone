import { SeekService } from "./seek.service";
import { ConsumeService, MediaPlayerService, MediaTimestampService } from "../index";
import { of, NEVER, Observable } from 'rxjs';

describe('SeekService', function()
{
    beforeEach(function()
    {
        this.mediaTimestampService = {
            setPlayheadTimestamp: jasmine.createSpy('setPlayheadTimestamp')
        };

        this.consumeService = {

        };

        this.mediaPlayerService = {
            mediaPlayer: {
                seek: jasmine.createSpy('seek'),
                resume: jasmine.createSpy('resume'),
                convertZeroBasedSecondsToZulu: jasmine.createSpy('convertZeroBasedSecondsToZulu'),
                getPlayheadZuluTime: jasmine.createSpy('getPlayheadZuluTime')
            }
        };

        this.service = new SeekService(this.mediaPlayerService,this.mediaTimestampService,this.consumeService);
    });

    describe('seek()', function()
    {
        beforeEach(function ()
        {
            this.mediaPlayerService.mediaPlayer.seek.and.returnValue(of('done'));
            this.mediaPlayerService.mediaPlayer.convertZeroBasedSecondsToZulu.and.returnValue(1521596012);
        });

        describe('when the user chooses to restart the episode', function()
        {
            it('sets the playheadTimestamp to 0', function()
            {
                this.service.seek(0, true);
                expect(this.mediaTimestampService.setPlayheadTimestamp).toHaveBeenCalledWith(0);
            });
        });

        describe('when the user does not choose to restart', function()
        {
            it('sets the playheadTimestamp to the given seconds', function()
            {
                this.service.seek(10, false);
                expect(this.mediaTimestampService.setPlayheadTimestamp).toHaveBeenCalledWith(10);
            });
        });

        it('seeks to the given seconds', function()
        {
            this.service.seek(10, false);
            expect(this.mediaPlayerService.mediaPlayer.seek).toHaveBeenCalledWith(1521596012);
        });
    });


    describe('seekThenPlay()', function()
    {
        beforeEach(function()
        {
            this.mediaPlayerService.mediaPlayer.convertZeroBasedSecondsToZulu.and.returnValue(1521596012);

            this.mediaPlayerService.mediaPlayer.seek
            .and.returnValue(of('seeked'));

            this.resumeSubscriberSpy = jasmine.createSpy('resumeSubscriber');

            this.mediaPlayerService.mediaPlayer.resume
            .and.callFake(() =>
            {
                return new Observable(subscriber =>
                {
                    this.resumeSubscriberSpy();
                    subscriber.next(true);
                    subscriber.complete();
                });
            });
        });

        it('calls seek() with target', function()
        {
            this.service.seekThenPlay(10, false);
            expect(this.mediaPlayerService.mediaPlayer.seek).toHaveBeenCalledWith(1521596012);
        });

        it('calls resume() with target', function()
        {
            this.service.seekThenPlay(10, false);
            expect(this.mediaPlayerService.mediaPlayer.resume).toHaveBeenCalled();
        });

        it('subscribes to resume', function()
        {
            this.service.seekThenPlay(10, false);
            expect(this.resumeSubscriberSpy).toHaveBeenCalled();
        });

        describe('when seek does not complete', function()
        {
            beforeEach(function()
            {
                this.spy = jasmine.createSpy('resumeSubscriber');

                this.mediaPlayerService.mediaPlayer.seek
                .and.returnValue(NEVER);
            });

            it('does not subscribe to resume', function()
            {
                this.service.seekThenPlay(10, false);
                expect(this.resumeSubscriberSpy).not.toHaveBeenCalled();
            });
        });
    });
});
