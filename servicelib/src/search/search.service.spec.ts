
import {throwError as observableThrowError,  of as observableOf } from 'rxjs';
import { mock } from "ts-mockito";

import { SearchService } from "./search.service";
import { SettingsService } from "../settings/settings.service";

import {
    mockAutoCompleteSearchList,
    mockRecentSearchList,
    mockSearchList
} from "../test/mocks/search/search.service.mock";
import { mockSettingListResponse } from "../test/mocks/settings.response";
import { ChannelLineupService } from "../channellineup/channel.lineup.service";

describe("Search Service Test Suite", function()
{
    const keyword: string = "pop";

    beforeEach(function()
    {
        this.getRecentSearchResultsResponse = JSON.parse(JSON.stringify(mockRecentSearchList));
        this.getSearchResultsResponse = JSON.parse(JSON.stringify(mockSearchList));
        this.getAutoCompleteSearchResultsResponse = JSON.parse(JSON.stringify(mockAutoCompleteSearchList));

        this.mockSearchDelegate = {
            getRecentSearchResults: () =>
            {
                return observableOf(this.getRecentSearchResultsResponse);
            },
            getSearchResults: () =>
            {
                return observableOf(this.getSearchResultsResponse);
            },
            getAutoCompleteSearchResults: () =>
            {
                return observableOf(this.getAutoCompleteSearchResultsResponse);
            }
        };

        this.mockChannelLineupService= mock(ChannelLineupService);

        this.mockSettingsService = mock(SettingsService);
        this.mockSettingsService.settings = observableOf(JSON.parse(JSON.stringify(mockSettingListResponse)));

        this.searchService = new SearchService(this.mockSearchDelegate, this.mockSettingsService,this.mockChannelLineupService);

    });

    afterEach(function()
    {
        jasmine.clock().uninstall();
    });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.searchService).toBeDefined();
            expect(this.searchService instanceof SearchService).toEqual(true);
        });

        it("should have public properties defined..", function()
        {
            expect(this.searchService.getRecentSearchResults).toBeDefined();
            expect(this.searchService.getSearchResults).toBeDefined();
            expect(this.searchService.getAutoCompleteSearchResults).toBeDefined();
        });

    });

    describe("Search Service Execution ", function()
    {

        describe("Get Recent Search Results >>", function()
        {
            it("Success", function()
            {
                spyOn(this.mockSearchDelegate, "getRecentSearchResults").and
                    .callFake(() =>
                    {
                        return observableOf(this.getRecentSearchResultsResponse);
                    });

                this.searchService.getRecentSearchResults().subscribe(response =>
                {
                    expect(response).toEqual(true);
                });
                expect(this.mockSearchDelegate.getRecentSearchResults).toHaveBeenCalled();
            });

            it("Failure", function()
            {
                spyOn(this.mockSearchDelegate, "getRecentSearchResults").and
                    .callFake(() =>
                    {
                        return observableThrowError(false);
                    });

                this.searchService.getRecentSearchResults().subscribe(response =>
                {
                    expect(response).toEqual(true);
                });
                expect(this.mockSearchDelegate.getRecentSearchResults).toHaveBeenCalled();
            });
        });

        describe("Get Search Results >>", function()
        {
            it("should call updateRecentSearchList", function()
            {
                spyOn<any>(this.searchService, "updateRecentSearchList");
                this.searchService.getSearchResults(keyword);
                expect(this.searchService["updateRecentSearchList"]).toHaveBeenCalled();
            });
        });

        describe("Get AutoComplete Search Results >>", function()
        {
            it("Success", function()
            {
                spyOn(this.mockSearchDelegate, "getAutoCompleteSearchResults").and
                    .callFake(() =>
                    {
                        return observableOf(this.getAutoCompleteSearchResultsResponse);
                    });

                this.searchService.getAutoCompleteSearchResults(keyword).subscribe(response =>
                {
                    expect(response).toEqual(true);
                });
                expect(this.mockSearchDelegate.getAutoCompleteSearchResults).toHaveBeenCalled();

            });

            it("Failure", function()
            {
                spyOn(this.mockSearchDelegate, "getAutoCompleteSearchResults").and
                    .callFake(() =>
                    {
                        return observableThrowError(false);
                    });

                this.searchService.getAutoCompleteSearchResults(keyword).subscribe(response =>
                {
                    expect(response).toEqual(false);
                });
                expect(this.mockSearchDelegate.getAutoCompleteSearchResults).toHaveBeenCalled();
            });

            it("Get direct tune results when user enters channelNumber", function()
            {
                let channelNumber = "46";
                spyOn(this.mockChannelLineupService, "findChannelByNumber").and
                    .callFake(() =>
                    {
                        return [];
                    });

                this.searchService.getAutoCompleteSearchResults(channelNumber).subscribe(response =>
                {
                    expect(response).toEqual(true);
                });
                expect(this.mockChannelLineupService.findChannelByNumber).toHaveBeenCalled();

            });
        });

        describe("Update Search Keyword >>", function()
        {
            it("should reflect the updated value", function()
            {
                let keyword = "pop";
                this.searchService.updateSearchKeyword(keyword);
                this.searchService.searchKeyword.subscribe(response =>
                {
                    expect(response).toEqual(keyword);
                });
            });
        });

        describe("clearSearchResults >>", function()
        {
            it("for existing user used the clear search before>>", function()
            {
                spyOn(this.mockSettingsService, "updateSettings").and.returnValue(observableOf(true));
                this.searchService.clearSearchResults().subscribe(response =>
                {
                    expect(response).toEqual(true);
                });
                expect(this.mockSettingsService.updateSettings).toHaveBeenCalled();
            });

            it("for new user haven't used the clear search before>>", function()
            {
                //for new user the searchDeletionTimestamp will not be there in global settings then
                // we will be adding that property
                let globalSettings = [
                    {
                        name: "test",
                        value: "adasd"
                    }
                ];
                this.mockSettingsService.settings = observableOf({
                    globalSettings: globalSettings
                });

                this.searchService = new SearchService(this.mockSearchDelegate, this.mockSettingsService,this.mockChannelLineupService);

                spyOn(this.mockSettingsService, "updateSettings").and.returnValue(observableOf(true));
                this.searchService.clearSearchResults().subscribe(response =>
                {
                    expect(response).toEqual(true);
                });
                expect(this.mockSettingsService.updateSettings).toHaveBeenCalled();
            });
        });

        describe("updateRecentSearchList >>", function()
        {
            it('should update the recent search history with new keyword', function()
            {
                let sampleNewKeyword = 'howard';
                this.searchService.getSearchResults(sampleNewKeyword);
                this.searchService.recentSearchResults.subscribe((data) =>
                {
                    expect(data[0].searchString).toEqual(sampleNewKeyword);
                });
            });
        });
    });
});
