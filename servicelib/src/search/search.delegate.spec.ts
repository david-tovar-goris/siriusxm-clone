import { of as observableOf } from "rxjs";

import { SearchDelegate } from "./search.delegate";

import {
    mockAutoCompleteSearchResults,
    mockRecentSearchResults,
    mockSearchResults
} from "../test/mocks/search/search.delegate.mock";

describe("SearchDelegate Test Suite >>", function()
{
    const keyword = "pop";
    const recentSearchResults = mockRecentSearchResults;
    const searchResults = mockSearchResults;
    const autoCompleteSearchResults = mockAutoCompleteSearchResults;

    beforeEach(function()
    {
        this.mockHttp =
            {
                postModuleAreaRequests: jasmine.createSpy("postModuleAreaRequest").and.returnValue(observableOf(null)),
                postModuleAreaRequest: jasmine.createSpy("postModuleAreaRequest").and.returnValue(observableOf(null))
            };

        this.delegate = new SearchDelegate(this.mockHttp);
    });

    describe("Infrastructure >> ", function()
    {
        it("Should have a test subject.", function()
        {
            expect(this.delegate instanceof SearchDelegate).toBe(true);
        });
    });

    describe("SearchDelegate >> ", function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should forward the call to http", function()
            {
                expect(this.delegate.getRecentSearchResults).toBeDefined();
                expect(this.delegate.getSearchResults).toBeDefined();
                expect(this.delegate.getAutoCompleteSearchResults).toBeDefined();
            });
        });

        describe("calls to search api >> ", function()
        {
            it("Get RecentSearch Results ", function()
            {
                this.mockHttp.postModuleAreaRequests.and
                    .callFake((url, data, config) =>
                    {
                        expect(Array.isArray(data)).toEqual(true);
                        expect(data[ 0 ].moduleArea).toEqual('Search');
                        expect(data[ 0 ].moduleType).toEqual(undefined);
                        expect(data[ 0 ].moduleSubType).toEqual('History');
                        expect(config).toEqual(null);

                        return observableOf(recentSearchResults);
                    });

                this.delegate.getRecentSearchResults().subscribe(response =>
                {

                });

                expect(this.mockHttp.postModuleAreaRequests).toHaveBeenCalled();
            });

            it("Get Search Results", function()
            {
                this.mockHttp.postModuleAreaRequests.and
                    .callFake((url, reqPayload, config) =>
                    {
                        let data = reqPayload[0];
                        expect(data.moduleArea).toEqual('Search');
                        expect(data.moduleType).toEqual(undefined);
                        expect(data.moduleSubType).toEqual('Snippet');
                        expect(config).toEqual(null);
                        return observableOf(searchResults);
                    });

                this.delegate.getSearchResults(keyword).subscribe(response =>
                {

                });

                expect(this.mockHttp.postModuleAreaRequests).toHaveBeenCalled();
            });


            it("Get AutoComplete SearchResults ", function()
            {
                this.mockHttp.postModuleAreaRequests.and.callFake((url, reqPayload, config) =>
                {
                    let data = reqPayload[0];
                    expect(data.moduleArea).toEqual('Search');
                    expect(data.moduleType).toEqual(undefined);
                    expect(data.moduleSubType).toEqual('AutoSuggest');
                    expect(data.moduleRequest).toBeDefined();
                    expect(config).toEqual(null);
                    return observableOf(autoCompleteSearchResults);

                });

                this.delegate.getAutoCompleteSearchResults(keyword).subscribe(response =>
                {
                });
                expect(this.mockHttp.postModuleAreaRequests).toHaveBeenCalled();
            });

        });
    });
});
