import { IDmcaInfo, IDrmInfo } from "../dmca/dmca.interface";

export interface IShowSearchResults
{
    channels: Array<IChannelSearchResults>;
    contextualBanner: string;
    creativeArts: Array<any>;
    disableRecommendations: Array<any>;
    episodeCount: number;
    futureAiring: Array<any>;
    host: Array<any>;
    hostGuid: any;
    legacyIds: any;
    longDescription: string;
    longTitle: string;
    mediumTitle: string;
    programType: string;
    shortDescription: string;
    showGuid: string;
}

export interface IEpisodeSearchResults
{
    alternateDescription: string;
    aodEpisodeGUID: string;
    channelListRecommendationOverrides: Array<string>;
    contentValue: string;
    contextualBanner: string;
    creativeArts: Array<any>;
    dmcaInfo: IDmcaInfo;
    drmInfo: IDrmInfo;
    duration: number;
    episodeGUID: string;
    episodeScheduleGuid: string;
    episodeStartTime: string;
    featured: boolean;
    featuredPriority: string;
    guest: Array<string>;
    highlighted: false;
    host: Array<string>;
    hot: boolean;
    legacyIds: any;
    longDescription: string;
    longTitle: string;
    mediumTitle: string;
    nowPlayingRecommendationOverrides: Array<string>;
    onAirContactInformation: Array<string>;
    originalAirDate: string;
    percentConsumed: number;
    programType: string;
    publicationInfo: any;
    shortDescription: string;
    show: IShowSearchResults;
    sidePanelRecommendationOverrides: Array<string>;
    special: boolean;
    valuable: boolean;
}

export interface IChannelSearchResults
{
    alternateDescription: string;
    channelGuid: string;
    channelId: string;
    channelLineUp: Array<number>;
    channelNumber: string;
    contextualBanner: string;
    creativeArts: Array<any>;
    isAvailable: boolean;
    isBizMature: boolean;
    isFavorite: boolean;
    isMature: boolean;
    isMySxm: boolean;
    longDescription: string;
    mediumDescription: string;
    name: string;
    shortDescription: string;
    siriusChannelNumber: string;
    siriusServiceId: string;
    sortOrder: number;
    spanishContent: string;
    streamingName: string;
    superCategories: any;
    url: string;
    xmChannelNumber: string;
    xmServiceId: string;
}

export interface ISearchResults
{
    shows: Array<IShowSearchResults>;
    episodes: Array<IEpisodeSearchResults>;
    channels: Array<IChannelSearchResults>;
}

export interface IAutoCompleteResult
{
    suggestion: string;
}

export interface IRecentSearchResult
{
    searchString: string;
    searchDateTime: Date;
}
