import {
    ICurrentlyPlayingMedia,
    MediaTimeLine
} from "../index";

export class ReplayMedia
{
    public currentlyPlaying: ICurrentlyPlayingMedia = null;
    public mediaTimeLine: MediaTimeLine = null;
    public allowReplayOfSameContent: boolean = false;
}
