import { AudioPlayerConstants } from "./audio-player.consts";

export class AudioPlayerUtil
{
    /**
     * Simple method that indicates if a player is using the `Live` playback type.
     *
     * @param {string} type - Requested playback type.
     * @return {boolean} Flag indicating if playing Live.
     */
    public static isPlaybackTypeLive(type: string): boolean
    {
        return (type === AudioPlayerConstants.LIVE);
    }

    /**
     * Simple method that indicates if a player is using the `AOD` playback type.
     *
     * @param {string} type - Requested playback type.
     * @return {boolean} Flag indicating if playing Live.
     */
    public static isPlaybackTypeAod(type: string): boolean
    {
        return (type === AudioPlayerConstants.AOD);
    }
}
