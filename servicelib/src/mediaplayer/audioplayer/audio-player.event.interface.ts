export interface IAudioPlayerPlayheadEvent
{
    bitRate: string;
    bufferComplete: false;
    buffered: number;
    duration: number;
    error: any;
    id: string;
    isLive: boolean;
    playbackType: string;
    playhead: number;
    playheadZulu: number;
    started: number;
    startedZulu: number;
    timeIntoPlaylist: number;
    type: string;
    zuluBaseline: number;
    currentTarget: any;
    target: any;
}
