import { PlayerConfig } from "./audio-player.config";

/**
 * Represents the completion of an asynchronous operation
 */
interface PromisePlus<T>
{
    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T,
         TResult2 = never>(onfulfilled? : ((value : T) => TResult1 | PromiseLike<TResult1>) | undefined | null,
                           onrejected? : ((reason : any) => TResult2 | PromiseLike<TResult2>) | undefined | null) : PromisePlus<TResult1 | TResult2>;

    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected? : ((reason : any) => TResult | PromiseLike<TResult>) | undefined | null) : PromisePlus<T | TResult>;

    finally<TResult1 = T,
            TResult2 = never>(onfinished? : ((value : T) => TResult1 | PromiseLike<TResult1>) | undefined | null) : PromisePlus<T>;
}

/**
 * This is the interface of the core audio player.
 */
export interface IAudioPlayer
{
    addEventListener(event : string, callback : Function) : void;
    getPlayerId() : string;
    getVolume() : number;
    setVolume(volume : number) : void;
    isPaused() : boolean;
    isPlaying() : boolean;
    isStopped() : boolean;
    pause() : PromisePlus<string>;
    resume() : PromisePlus<string>;
    seek(timestamp : number, url? : string) : PromisePlus<string>;
    play(url : string, config : PlayerConfig) : PromisePlus<string>;
    preload(url : string, config : PlayerConfig) : PromisePlus<string>;
    stop(id? : string | number) : PromisePlus<string>;
    setNextTrack(trackId : string, nextTrackId : string) : PromisePlus<string>;
    skip(url : string, config : PlayerConfig): PromisePlus<string>;
    rewind(url : string, config : PlayerConfig): PromisePlus<string>;
    warmUp(): PromisePlus<string>;
    enableCrossfade(bool: boolean);
    getDuration() : number;
}
