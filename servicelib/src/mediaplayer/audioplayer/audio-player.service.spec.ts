/// <reference path="../../../node_modules/@types/jasmine/index.d.ts" />

///////////////////////////////////////////////////////////////////////////////////////////////////
// TODO: BMR: 10/02/2017: Not creating detailed unit tests for now as entire audio player state will change next sprint.
///////////////////////////////////////////////////////////////////////////////////////////////////

import { Observable, BehaviorSubject, of as observableOf } from "rxjs";
import * as AudioPlayer from "sxm-audio-player";
import { mock } from "ts-mockito";
import { ConfigService } from "../../config/config.service";
import { InitializationService } from "../../initialization/initialization.service";
import { NoopService } from "../../noop/noop.service";
import { TuneService } from "../../tune/tune.service";
import { SettingsService } from "../../settings/settings.service";
import {
    mediaTimeLineAodMock,
    mediaTimeLineLiveMock,
    mockSettingListResponse,
    normalizedRelativeUrlsResponse,
    tuneStartValue
} from "../../test/index";
import { AudioPlayerConfigFactory } from "./audio-player.config.factory";
import { AudioPlayerService } from "./audio-player.service";
import { CurrentlyPlayingService } from "../../currently-playing/currently.playing.service";
import {PlayheadTimestampService} from "../playhead-timestamp.service";
import { ChromecastService } from "../../chromecast/chromecast.service";
import { SessionTransferService } from "../../sessiontransfer/session.transfer.service";
import { MediaTimeLineService } from "../../media-timeline/media.timeline.service";
import { AudioPlayerEventMonitor } from "../audio-player.event-monitor";

describe("Audio Player Service Test Suite >>", function()
{
    const audioPlayer = new AudioPlayer();

    const resumeFaultErrorMessage = "Resume Fault";
    const pauseFaultErrorMessage = "Pause Fault";
    const playFaultErrorMessage = "Pause Fault";

    beforeEach(function()
    {
        this.cdnAccessTokenMock = {
            akamai: "AkamainToken",
            limeLight: "limeLightToken"
        };
        this.aodMediaTimeLine = mediaTimeLineAodMock;

        // Create mocks for the expected constructor params.
        this.initServiceMock = mock(InitializationService);
        this.configServiceMock = mock(ConfigService);
        this.settingsServiceMock = mock(SettingsService);
        this.tuneServiceMock = mock(TuneService);
        this.noopServiceMock = mock(NoopService);
        this.audioPlayerConfigFactoryMock = mock(AudioPlayerConfigFactory);
        this.currentlyPlayingServiceMock = mock(CurrentlyPlayingService);
        this.playheadTimestampServiceMock = mock(PlayheadTimestampService);
        this.chromecastServiceMock = mock(ChromecastService);
        this.sessionTransferServiceMock = mock(SessionTransferService);
        this.mediaTimeLineServiceMock = mock(MediaTimeLineService);
        this.audioPlayerEventMonitorMock  = mock(AudioPlayerEventMonitor);

        this.chromecastServiceMock.state$ = Observable.create(function() { });

        this.configServiceMock.relativeUrlConfiguration = {
            subscribe: (result: Function, fault: Function) => result(normalizedRelativeUrlsResponse)
        };

        this.settingsServiceMock.settings = {
            subscribe: (result: Function, fault: Function) => result(mockSettingListResponse)
        };

        this.mediaTimeLineServiceMock.mediaTimeLine = {
            subscribe: (result: Function, fault: Function) =>
            {
                result(this.liveMediaTimeLine);
            },
            pipe: () => observableOf("")
        };

        this.currentlyPlayingServiceMock.currentlyPlayingData = {
            subscribe: (result: Function, fault: Function) =>
            {
                result(null);
            },
            pipe: () => observableOf("")
        };

        this.tuneServiceMock.scrubTimes = {
            subscribe: (result: Function, fault: Function) =>
            {
                result(null);
            }
        };

        this.audioPlayerEventMonitorMock.playbackComplete$ = new BehaviorSubject(false);

        this.audioPlayerEventMonitorMock.bufferEmpty$ = new BehaviorSubject(false);

        this.audioPlayerEventMonitorMock.playbackReady$ = new BehaviorSubject(false);

        this.appConfig = {
            apiEndpoint: "myApi",
            appId: "myApp",
            resultTemplate: "web",
            deviceInfo: {
                appRegion: "region",
                browser: "unittest",
                browserVersion: "version",
                clientDeviceId: "id",
                clientDeviceType: "type",
                deviceModel: "model",
                osVersion: "OS",
                platform: "platform",
                player: "player",
                sxmAppVersion: "version"
            }
        };

        this.playSpy = spyOn(audioPlayer, "play").and.callFake(params =>
        {
            const promise =
                {
                    then: function (callback)
                    {
                        if (callback)
                        {
                            callback(true);
                        }
                        return promise;
                    },
                    catch: function ()
                    {
                        return promise;
                    },
                    finally: function (callback)
                    {
                        if (callback)
                        {
                            callback();
                        }
                        return promise;
                    }
                };

            return promise;
        });

        this.isPlayingSpy = spyOn(audioPlayer, "isPlaying").and.returnValue(false);

        this.isLoadedSpy = spyOn(audioPlayer, "isLoaded").and.returnValue(true);

        this.stopSpy = spyOn(audioPlayer, "stop").and.callFake(params =>
        {
            return {
                then: function (callback)
                {
                    callback();
                },
                catch: function (callback)
                {
                },
                finally: function (callback)
                {
                    callback();
                }
            };
        });

        this.resumeSpy = spyOn(audioPlayer, "resume").and.callFake(params =>
        {
            return {
                then: function (callback)
                {
                    callback(true);
                    return {
                        catch: function ()
                        {

                        }
                    };
                }
            };
        });

        this.pauseSpy = spyOn(audioPlayer, "pause").and.callFake(params =>
        {
            return {
                then: function (callback)
                {
                    callback(true);
                    return {
                        catch: function ()
                        {

                        }
                    };
                }
            };
        });
    });

    afterEach(function()
    {
        this.testSubject = null;
        mediaTimeLineLiveMock.mediaType = "live";
        tuneStartValue.value = "off";
    });

    describe("Infrastructure>> ", function()
    {
        it("Should have a test subject.", function()
        {
            let testSubject = new AudioPlayerService(audioPlayer,
                this.initServiceMock,
                this.tuneServiceMock,
                this.currentlyPlayingServiceMock,
                this.playheadTimestampServiceMock,
                this.noopServiceMock,
                this.audioPlayerConfigFactoryMock,
                this.chromecastServiceMock,
                this.sessionTransferServiceMock,
                this.mediaTimeLineServiceMock,
                this.audioPlayerEventMonitorMock);

            expect(testSubject).toBeDefined();
            expect(testSubject instanceof AudioPlayerService).toEqual(true);
        });

        // it("Should have a test subject if subscribers sends empty", () =>
        // {
        //     configServiceMock.relativeUrlConfiguration = {
        //         subscribe: (result: Function, fault: Function) => result(undefined)
        //     };
        //     settingsServiceMock.globalSettings = {
        //         subscribe: (result: Function, fault: Function) => result(undefined)
        //     };
        //     settingsServiceMock.deviceSettings = {
        //         subscribe: (result: Function, fault: Function) => result(undefined)
        //     };
        //     nowPlayingServiceMock.mediaTimeLine = {
        //         subscribe: (result: Function, fault: Function) => result(undefined)
        //     };
        //     responseInterceptor.cdnAccessTokens = {
        //         subscribe: (result: Function, fault: Function) => result(undefined)
        //     };
        // });

        // it("Should have a test subject if mediaTimeLine is AOD", () =>
        // {
        //     isPlayingSpy = isPlayingSpy.and.returnValue(false);
        //     isLoadedSpy = isLoadedSpy.and.returnValue(false);
        //
        //     nowPlayingServiceMock.mediaTimeLine = {
        //         subscribe: (result: Function, fault: Function) => result(mediaTimeLineAodMock)
        //     };
        //
        //     let testSubject = new AudioPlayerService(
        //         configServiceMock,
        //         settingsServiceMock,
        //         nowPlayingServiceMock,
        //         noopServiceMock,
        //         responseInterceptor,
        //         appConfig);
        //
        //     expect(testSubject).toBeDefined();
        //
        //     expect(playSpy).toHaveBeenCalled();
        //     expect(isPlayingSpy).toHaveBeenCalled();
        //     expect(isLoadedSpy).toHaveBeenCalled();
        //     expect(resumeSpy).toHaveBeenCalled();
        // });
        //
        // it("Should have a test subject if mediaTimeLine is Live the data comes from resume", () =>
        // {
        //     liveMediaTimeLine = mediaTimeLineLiveMock;
        //
        //     mediaTimeLineLiveMock.isDataComeFromResume = false;
        //
        //     nowPlayingServiceMock.mediaTimeLine = {
        //         subscribe: (result: Function, fault: Function) => result(liveMediaTimeLine)
        //     };
        //
        //     let testSubject = new AudioPlayerService(
        //         configServiceMock,
        //         settingsServiceMock,
        //         nowPlayingServiceMock,
        //         noopServiceMock,
        //         responseInterceptor,
        //         appConfig);
        //
        //     expect(testSubject).toBeDefined();
        // });
        //
        // it("Should have a test subject and play resume spy to have been called if mediaType is empty"
        //     , () =>
        //     {
        //         mediaTimeLineLiveMock.mediaType = "";
        //         tuneStartValue.value = "on";
        //         nowPlayingServiceMock.mediaTimeLine = {
        //             subscribe: (result: Function, fault: Function) => result(mediaTimeLineLiveMock)
        //         };
        //
        //         let testSubject = new AudioPlayerService(
        //             configServiceMock,
        //             settingsServiceMock,
        //             nowPlayingServiceMock,
        //             noopServiceMock,
        //             responseInterceptor,
        //             appConfig);
        //
        //         expect(testSubject).toBeDefined();
        //
        //         expect(stopSpy).toHaveBeenCalled();
        //         expect(playSpy).toHaveBeenCalled();
        //         expect(isPlayingSpy).toHaveBeenCalled();
        //         expect(isLoadedSpy).toHaveBeenCalled();
        //         expect(resumeSpy).toHaveBeenCalled();
        //     });
        //
        // it("Should expect exception when play throws exception", () =>
        // {
        //     playSpy = playSpy.and.callFake(params =>
        //     {
        //         const failedPromise = {
        //             catch: function (callback)
        //             {
        //                 callback(playFaultErrorMessage);
        //             }
        //         };
        //         const okPromise = {
        //             then: function ()
        //             {
        //                 return failedPromise;
        //             }
        //         };
        //
        //         return okPromise;
        //     });
        //
        //     try
        //     {
        //         new AudioPlayerService(
        //             configServiceMock,
        //             settingsServiceMock,
        //             nowPlayingServiceMock,
        //             noopServiceMock,
        //             responseInterceptor,
        //             appConfig);
        //     }
        //     catch (exception)
        //     {
        //         expect(exception).toEqual(playFaultErrorMessage);
        //     }
        // });
    });

    // describe("togglePausePlay", () =>
    // {
    //     it("expect pauseSpy or resumeSpy to have been called", () =>
    //     {
    //         testSubject.togglePausePlay();
    //
    //         expect(pauseSpy).toHaveBeenCalled();
    //
    //         testSubject.togglePausePlay();
    //
    //         expect(resumeSpy).toHaveBeenCalled();
    //
    //         testSubject.togglePausePlay();
    //
    //         expect(pauseSpy).toHaveBeenCalled();
    //
    //     });
    //
    //     it("expect exception when exception thrown by resume call", () =>
    //     {
    //         testSubject.togglePausePlay();
    //
    //         expect(pauseSpy).toHaveBeenCalled();
    //
    //         resumeSpy = resumeSpy.and.callFake(params =>
    //         {
    //             return {
    //                 then: function (callback)
    //                 {
    //                     return {
    //                         catch: function (callback)
    //                         {
    //                             callback(resumeFaultErrorMessage);
    //                         }
    //                     };
    //                 }
    //             };
    //         });
    //
    //         try
    //         {
    //             testSubject.togglePausePlay();
    //         }
    //         catch (exception)
    //         {
    //             expect(exception).toContain(resumeFaultErrorMessage);
    //         }
    //
    //         expect(resumeSpy).toHaveBeenCalled();
    //
    //     });
    //
    //     it("expect exception when exception thrown by pause call", () =>
    //     {
    //         pauseSpy = pauseSpy.and.callFake(params =>
    //         {
    //             return {
    //                 then: function (callback)
    //                 {
    //
    //                     return {
    //                         catch: function (callback)
    //                         {
    //                             callback(pauseFaultErrorMessage);
    //                         }
    //                     };
    //                 }
    //             };
    //         });
    //
    //         try
    //         {
    //             testSubject.togglePausePlay();
    //
    //         }
    //         catch (exception)
    //         {
    //             expect(exception).toContain(pauseFaultErrorMessage);
    //         }
    //
    //         expect(pauseSpy).toHaveBeenCalled();
    //
    //     });
    //
    // });
});
