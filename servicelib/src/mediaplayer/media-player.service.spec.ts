import { mediaPlayerMock } from "../test/mocks/media-player.mock";
import {BehaviorSubject, of as observableOf} from "rxjs";
import { ChromecastPlayerService } from "./chromecastplayer/chromecast-player.service";
import { MediaPlayerService } from "./media-player.service";
import { AppMonitorService, ClientFault } from "../app-monitor";
import { MediaPlayerFactory } from "./media-player.factory";
import { MediaTimeLineService } from "../media-timeline";
import { MediaPlayerFactoryMock } from "../test/mocks/media-player.factory.mock";
import { mock } from "ts-mockito";
import { CurrentlyPlayingService } from "../currently-playing";

describe('MediaPlayerService', function()
{
    beforeEach(function()
    {
        this.mockAppMonitorService = {
            faultStream: observableOf({} as ClientFault)
        } as any;

        this.mockMediaPlayerFactory = new MediaPlayerFactoryMock() as any;

        this.mockCurrentlyPlayingService = {
            ...mock(MediaTimeLineService),
            currentlyPlayingData: observableOf({})
        } as any;

        this.mockChromecastPlayerService = {
            switchPlayerType$: observableOf("live")
        } as any;

        this.mockMediaPlayerModel = {
            playbackState$ : new BehaviorSubject('')
        } as any;

        this.service = new MediaPlayerService(
            this.mockMediaPlayerFactory,
            this.mockCurrentlyPlayingService,
            this.mockAppMonitorService,
            this.mockChromecastPlayerService,
            this.mockMediaPlayerModel
        );
    });

    describe('PRIVATE - retrieveMediaPlayer()', function()
    {
        it('retrieves the mediaPlayer on instantiation', function()
        {
            expect(this.service.mediaPlayer).toEqual(mediaPlayerMock);
        });
    });

    describe('isNewMedia()', function()
    {
        it('returns true if the media player has been reset', function()
        {
            let foobar = {
            } as any;
            foobar.mediaId = 'live';
            expect(this.service.isNewMedia(foobar)).toEqual(true);
        });
    });
});
