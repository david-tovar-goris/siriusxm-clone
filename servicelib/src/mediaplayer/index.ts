/**
 * This is known as a barrel file. It allows developers to package and rollup multiple exports
 * into a single file and thus single import/export; e.g., `export * from "./authentication/service/";`
 */
export { IMediaPlayer } from "./media-player.interface";
export {
    IApronSegment,
    IMediaAssetMetadata
} from "./media-asset-metadata.interface";
export { MediaPlayerFactory } from "./media-player.factory";
export { MediaUtil } from "./media.util";
export { MediaPlayerConstants } from "./media-player.consts";
export { IPlayhead } from "./playhead.interface";
export { VolumeService } from "./volume.service";
export { MultiTrackList } from "./multitrackaudioplayer/multi-track-list";
export { MediaPlayerService } from './media-player.service';
export { PlayheadTimestampService } from './playhead-timestamp.service';
export { MediaTimestampService } from './media-timestamp.service';
