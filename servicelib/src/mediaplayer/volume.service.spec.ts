import {
    VolumeService,
    StorageService
} from "../index";

import { mediaPlayerMock } from "../test/mocks/media-player.mock";
import { Observable, of as observableOf } from "rxjs";
import { StorageKeyConstant } from "../storage/storage.constants";
import { MediaPlayerConstants } from "./media-player.consts";


describe("VolumeService Test Suite >>", function()
{
    beforeEach(function()
    {
        this.storageService = new StorageService();
        spyOn(this.storageService, "setItem");

        this.mockMediaFactory = {
            currentMediaPlayer: observableOf(mediaPlayerMock)
        };

        this.mockChromcastModel = {
            volumeChangedFromRemotePlayer$: observableOf({})
        };
        this.service          = new VolumeService(this.storageService, this.mockMediaFactory, this.mockChromcastModel);
    });

    describe("VolumeService()", function()
    {
        it("Should set volume to media player when mediaPlayer changes ", function()
        {
            expect(this.storageService.setItem).toHaveBeenCalledWith(StorageKeyConstant.VOLUME,
                MediaPlayerConstants.MID_VOLUME_VALUE);
        });
    });

    describe("getVolumeImageSrc", function()
    {
        it("Should have getVolumeImageSrc defined", function()
        {
            expect(this.service.getVolumeImageSrc).toBeDefined();
        });

        it("Should return the 'muted' image Url", function()
        {
            expect(this.service.getVolumeImageSrc(0, false)).toEqual("../../assets/images/volume/volume-0-blue");
            expect(this.service.getVolumeImageSrc(0, true)).toEqual("../../assets/images/volume/volume-0-white");
        });

        it("Should return the 'high volume' image Url", function()
        {
            expect(this.service.getVolumeImageSrc(90, false)).toEqual("../../assets/images/volume/volume-5-blue");
            expect(this.service.getVolumeImageSrc(90, true)).toEqual("../../assets/images/volume/volume-5-white");
        });

        it("Should return the 'medium-high volume' image Url", function()
        {
            expect(this.service.getVolumeImageSrc(70, false)).toEqual("../../assets/images/volume/volume-4-blue");
            expect(this.service.getVolumeImageSrc(70, true)).toEqual("../../assets/images/volume/volume-4-white");
        });

        it("Should return the 'medium volume' image Url", function()
        {
            expect(this.service.getVolumeImageSrc(50, false)).toEqual("../../assets/images/volume/volume-3-blue");
            expect(this.service.getVolumeImageSrc(50, true)).toEqual("../../assets/images/volume/volume-3-white");
        });

        it("Should return the 'medium-low volume' image Url", function()
        {
            expect(this.service.getVolumeImageSrc(30, false)).toEqual("../../assets/images/volume/volume-2-blue");
            expect(this.service.getVolumeImageSrc(30, true)).toEqual("../../assets/images/volume/volume-2-white");
        });

        it("Should return the 'low volume' image Url", function()
        {
            expect(this.service.getVolumeImageSrc(10, false)).toEqual("../../assets/images/volume/volume-1-blue");
            expect(this.service.getVolumeImageSrc(10, true)).toEqual("../../assets/images/volume/volume-1-white");
        });
    });

    describe('initializeVolume()', function()
    {
        beforeEach(function() { this.aVolumeSetable = { setVolume: jasmine.createSpy('setVolume') }; });

        describe('if volume is muted', function()
        {
            beforeEach(function()
            {
                spyOn(this.service, 'isMuted').and.returnValue(true);
            });

            it('sets volume to 0', function()
            {
                this.service.initializeVolume(this.aVolumeSetable);
                expect(this.aVolumeSetable.setVolume).toHaveBeenCalledWith(0);
            });
        });

        describe('if volume is not muted', function()
        {
            beforeEach(function()
            {
                spyOn(this.service, 'isMuted').and.returnValue(false);
                spyOn(this.service, 'pullNonZeroVolumeFromStorage').and.returnValue(13);
            });

            it('sets volume to whatever is pulled from localStorage', function()
            {
                this.service.initializeVolume(this.aVolumeSetable);
                expect(this.aVolumeSetable.setVolume).toHaveBeenCalledWith(13);
            });
        });
    });

    describe('mute()', function()
    {
        it('sets mute value in local storage to whatever', function()
        {
            this.storageService.setItem.calls.reset();
            this.service.mute(true);
            expect(this.storageService.setItem).toHaveBeenCalledWith(StorageKeyConstant.IS_MUTED, 'true');
        });

        xit('sets volume to zero on media player', function()
        {
            spyOn(mediaPlayerMock, 'setVolume');
            this.service.mute(true);
            expect(mediaPlayerMock.setVolume).toHaveBeenCalledWith(0);
        });
    });

    describe('adjustVolume()', function()
    {
        beforeEach(function() { spyOn(this.service, 'pushVolumeToStorage'); });

        describe('when persist flag is true', function()
        {
            it('if volume 0 it does not push volume to storage', function()
            {
                this.service.adjustVolume(0, true, true);
                expect(this.service.pushVolumeToStorage).not.toHaveBeenCalled();
            });

            it('volume > 0 it pushes volume to storage', function()
            {
                this.service.adjustVolume(1, true, true);
                expect(this.service.pushVolumeToStorage).toHaveBeenCalledWith(1);
            });

            it('sets volume on mediaPlayer', function()
            {
                spyOn(mediaPlayerMock, 'setVolume');
                this.service.adjustVolume(14, true, true);
                expect(mediaPlayerMock.setVolume).toHaveBeenCalledWith(14);
            });
        });

        describe('when persist flag is false', function()
        {
            it('if volume > 0 it does not push volume to storage', function()
            {
                this.service.adjustVolume(0, false, true);
                expect(this.service.pushVolumeToStorage).not.toHaveBeenCalled();
            });

            xit('sets volume on mediaPlayer', function()
            {
                spyOn(mediaPlayerMock, 'setVolume');
                this.service.adjustVolume(0, true, true);
                expect(this.service.pushVolumeToStorage).toHaveBeenCalled();
            });
        });
    });

    describe('pushVolumeToStorage()', function()
    {
        it('calls setItem with IS_MUTED set to false', function()
        {
            this.service.pushVolumeToStorage(10);
            expect(this.storageService.setItem).toHaveBeenCalledWith(StorageKeyConstant.IS_MUTED, 'false');
        });
    });
});
