import { VideoPlayerService } from "./video-player.service";
import { mock } from "ts-mockito";
import { of as observableOf } from "rxjs";

import { TuneService } from "../../tune/tune.service";
import { NoopService } from "../../noop/noop.service";
import { BitrateHistoryLogService } from "./bitrate-history/bitrate-history-log.service";
import { ErrorService } from "../../error/error.service";
import { AppMonitorService } from "../../app-monitor/app-monitor.service";
import { InitializationService } from "../../initialization/initialization.service";
import { CurrentlyPlayingService } from "../../currently-playing/currently.playing.service";
import { PlayheadTimestampService } from "../playhead-timestamp.service";
import { StorageService } from "../../storage/storage.service";
import { IAppConfig } from "../../config/interfaces/app-config.interface";
import { ChromecastService } from "../../chromecast/chromecast.service";
import { SessionTransferService } from "../../sessiontransfer/session.transfer.service";
import { MediaTimeLineService } from "../../media-timeline/media.timeline.service";
import { VideoPlayerEventMonitor } from "./video-player.event-monitor";
import { VideoPlayerConfigFactory } from "./video-player.config.factory";
import { VideoPlayerBufferUnderflowMonitor } from "./video-player.buffer-underflow.monitor";
import WebVideoPlayer  from "sxm-video-player";
import { VideoPlayerConstants } from "./video-player.consts";
import { MediaTimeLine } from "../../tune/tune.interface";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { InitializationStatusCodes } from "../../initialization/initialization.const";
import { PlayerTypes } from "../../service/types/content.types";

describe("Video Player Service Unit Test Suite: ", function()
{
    beforeEach(function()
    {
        this.videoPlayer = new WebVideoPlayer();
        this.tuneServiceMock = mock(TuneService);
        this.noopServiceMock = mock(NoopService);
        this.bitrateHistoryLogService = mock(BitrateHistoryLogService);
        this.errorService = mock(ErrorService);
        this.appMonitoreServiceMock  =  mock(AppMonitorService);
        this.initServiceMock = mock(InitializationService);
        this.currentlyPlayingServiceMock = mock(CurrentlyPlayingService);
        this.playheadTimestampServiceMock = mock(PlayheadTimestampService);
        this.storageService = mock(StorageService);
        this.chromecastServiceMock = mock(ChromecastService);
        this.sessionTransferServiceMock = mock(SessionTransferService);
        this.mediaTimeLineServiceMock = mock(MediaTimeLineService);
        this.videoPlayerEventMonitor = new VideoPlayerEventMonitor(this.bitrateHistoryLogService);
        this.videoPlayerConfigFactory = mock(VideoPlayerConfigFactory);
        this.videoPlayerBufferUnderflowMonitor = mock(VideoPlayerBufferUnderflowMonitor);

        this.configServiceMock= {
            deviceInfo : {
                appRegion: "US",
                browser: "chrome",
                browserVersion: "11.22"
            }
        } as IAppConfig;

        this.currentlyPlayingServiceMock.currentlyPlayingData = {
            subscribe: (result: Function, fault: Function) =>
            {
                result(null);
            },
            pipe: () => observableOf("")
        };

        this.mediaTimeLineServiceMock.mediaTimeLine= new BehaviorSubject(null);
        this.mediaTimeLineServiceMock.getPausePointVideoStartTime = () => 1580313600000;

        this.initServiceMock.initState = new BehaviorSubject<string>(null);

        this.chromecastServiceMock.autoplay$ = new BehaviorSubject<boolean>(null);

        this.playHeadMock = {
                                currentTime: {
                                    milliseconds: 35105,
                                    seconds: 35.105,
                                    zuluMilliseconds: 1580313600000,
                                    zuluSeconds: 1580313600
                                },
                                startTime: {
                                    milliseconds: 0,
                                    seconds: 0,
                                    zuluMilliseconds: 0,
                                    zuluSeconds: 0
                                },
                                id: "",
                                type: "",
                                isBackground: false
                            };
        this.playheadTimestampServiceMock.playhead = new BehaviorSubject(this.playHeadMock);

        this.service = new VideoPlayerService(
            this.videoPlayer,
            this.tuneServiceMock ,
            this.noopServiceMock,
            this.bitrateHistoryLogService,
            this.errorService,
            this.appMonitoreServiceMock,
            this.initServiceMock,
            this.currentlyPlayingServiceMock,
            this.playheadTimestampServiceMock,
            this.storageService,
            this.configServiceMock,
            this.chromecastServiceMock,
            this.sessionTransferServiceMock,
            this.mediaTimeLineServiceMock,
            this.videoPlayerEventMonitor,
            this.videoPlayerConfigFactory,
            this.videoPlayerBufferUnderflowMonitor
        );
        spyOn(this.service, "sessionTransfer").and.returnValue(observableOf(""));
    });

    it("Should VPS to be defined", function()
    {
        expect(this.service).toBeDefined();
    });

    it("Set state as Playing", function()
    {
        this.videoPlayerEventMonitor.playing$.next(WebVideoPlayer.EVENTS.PLAYING);
        expect(this.service.state).toBe(VideoPlayerConstants.PLAYING);
    });

    it("Check state as playing while playhead update", function()
    {
        this.videoPlayerEventMonitor.paused$.next(WebVideoPlayer.EVENTS.PAUSED);
        spyOn(this.service, "convertZeroBasedSecondsToZulu").and.returnValue(this.playHeadMock.currentTime.zuluMilliseconds);
        this.videoPlayerEventMonitor.playhead$.next(this.playHeadMock);

        expect(this.service.state).toBe(VideoPlayerConstants.PLAYING);
    });

    describe("Initializing Video Player", function()
    {
        let mediaTimeLineData;
        const mediaTrigger = {
            isNewMediaId: true,
            isNewMediaType: true,
            isNewPlayerType: false
        };

        beforeEach(function()
        {
            mediaTimeLineData = {
                mediaId: "e35ea97e-6685-3f54-9489-0bd307fce554",
                mediaType: "vod",
                playerType: "local",
                isDataComeFromResume: true,
                isDataComeFromResumeWithDeepLink: false,
                mediaEndPoints: [
                    {
                        name: "hls",
                        url: "https://siriusxm-priuatvideo.siriusxm.com/V0cWs4ajE6ooPSTHtlilreLAeFkzjosd/1/ts/1.m3u8"
                    }]
            } as MediaTimeLine;

        });

        it("On New Media with local media type", function()
        {
            const spy = spyOn(this.service, "onNewMedia").and.callThrough();
            const spyOnStartVideo = spyOn(this.service, "startVideo");
            this.mediaTimeLineServiceMock.mediaTimeLine.next(mediaTimeLineData);

            expect(spyOnStartVideo).toHaveBeenCalled();
            expect(spy).toHaveBeenCalledWith(mediaTimeLineData, mediaTrigger);

        });

        it("Should stop playing Video before start new", function()
        {
            mediaTimeLineData.mediaType = "aod";
            this.videoPlayerEventMonitor.playing$.next(WebVideoPlayer.EVENTS.PLAYING);

            spyOn(this.service, "onNewMedia").and.callThrough();
            const spyOnStopVideo = spyOn(this.service, "stop").and.callThrough();
            this.mediaTimeLineServiceMock.mediaTimeLine.next(mediaTimeLineData);

            expect(spyOnStopVideo).toHaveBeenCalled();
        });

        it("On New Media with Remote media type", function()
        {
            mediaTimeLineData.playerType = "remote";
            const spy = spyOn(this.service, "onNewMedia").and.callThrough();
            this.mediaTimeLineServiceMock.mediaTimeLine.next(mediaTimeLineData);

            expect(VideoPlayerConstants.IDLE).toBe(this.service.state);
        });

        it("Start Video by new media time line with no autoplay", function()
        {
            spyOn(this.initServiceMock.initState,"pipe").and.returnValue(observableOf(""));

            this.service.mediaTimeLine = mediaTimeLineData;
            const spy = spyOn(this.service, "startVideo").and.callThrough();

            this.service.startVideo(false,0);
            this.initServiceMock.initState.next(InitializationStatusCodes.RUNNING);

            expect(this.service.state).toBe(VideoPlayerConstants.PRELOADING);

            this.videoPlayerEventMonitor.playbackReady$.next({willAutoplay: false});

            expect(this.service.state).toBe(VideoPlayerConstants.PAUSED);
        });

        it("Start Video by new media time line with autoplay", function()
        {
            spyOn(this.initServiceMock.initState,"pipe").and.returnValue(observableOf(""));
            spyOn(this.service, "startVideo").and.callThrough();

            this.service.mediaTimeLine = mediaTimeLineData;
            const spy = spyOn(this.service.videoPlayer, "play");

            this.service.startVideo(true,0);
            this.initServiceMock.initState.next(InitializationStatusCodes.RUNNING);

            this.videoPlayerEventMonitor.playbackReady$.next({willAutoplay: false});

            expect(spy).toHaveBeenCalled();
        });
    });

    describe("Toggle Play/Pause: ", function()
    {

        it("Should call video resume method", function()
        {
            spyOn(this.service, "togglePausePlay").and.callThrough();

            this.videoPlayerEventMonitor.paused$.next(WebVideoPlayer.EVENTS.PAUSED);

            const spy = spyOn(this.service.videoPlayer, "resume");
            this.service.togglePausePlay().subscribe();

            expect(spy).toHaveBeenCalled();
        });

        it("Should call video pause method", function()
        {
            spyOn(this.service, "togglePausePlay").and.callThrough();

            this.videoPlayerEventMonitor.paused$.next(WebVideoPlayer.EVENTS.PLAYING);

            const spy = spyOn(this.service.videoPlayer, "pause");
            this.service.togglePausePlay().subscribe();

            expect(spy).toHaveBeenCalled();
        });

        it("Should call start video from Idle state", function()
        {
            this.service.state = VideoPlayerConstants.ERROR;
            this.service.playerType = PlayerTypes.REMOTE;
            this.service.setPlaybackStateFromEvent(VideoPlayerConstants.IDLE);

            spyOn(this.service, "togglePausePlay").and.callThrough();

            const spy = spyOn(this.service, "startVideo");
            this.service.togglePausePlay().subscribe();

            expect(spy).toHaveBeenCalled();
        });

        it("Should call retune if state is error", function()
        {
            this.service.state = VideoPlayerConstants.ERROR;

            spyOn(this.service, "togglePausePlay").and.callThrough();

            const spy = spyOn(this.service, "retune");
            this.service.togglePausePlay();

            expect(spy).toHaveBeenCalled();
        });
    });

    it("Should call video sdk seek and remain old state", function()
    {
        const spy = spyOn(this.service.videoPlayer, "seek");
        this.service.seek(1580313618621).subscribe();

        expect(this.service.state).toBe(VideoPlayerConstants.SEEKING);
        expect(spy).toHaveBeenCalled();
    });

    it("Should be error/paused state if playback failed/Auto playback blocked", function()
    {

        this.videoPlayerEventMonitor.paused$.next(WebVideoPlayer.EVENTS.PLAYING);
        spyOn(this.service, "playbackFailed").and.callThrough();
        this.service.playbackFailed();
        expect(this.service.state).toBe(VideoPlayerConstants.ERROR);

        this.videoPlayerEventMonitor.paused$.next(WebVideoPlayer.EVENTS.PLAYING);
        this.videoPlayerEventMonitor.autoplayBlocked$.next({});

        expect(this.service.state).toBe(VideoPlayerConstants.PAUSED);
    });
});

