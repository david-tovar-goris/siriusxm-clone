import {Observable}   from "rxjs";
export interface IVideoPlayerPlayheadEvent
{
    bitRate: string;
    bufferComplete: false;
    buffered: number;
    duration: number;
    error: any;
    id: string;
    isLive: boolean;
    playbackType: string;
    playhead: number;
    playheadZulu: number;
    started: number;
    startedZulu: number;
    timeIntoPlaylist: number;
    type: string;
    zuluBaseline: number;
    currentTarget: any;
    target: any;
}

export interface IStartVideo
{
    url: string;
    autoPlay: boolean;
    startTime: number;
    type: string;
}

/**
 * TODO : Will remove this once we import this from the web-video-player
 * This is the interface of the core video player.
 */
export interface IVideoPlayer
{
    hls: any;

    playhead$: Observable<any>;

    buffering$: Observable<boolean>;

    finished$: Observable<boolean>;

    getPlayheadTime(): number;

    init(videoWrapperElement: any, config?: any): void;

    loadAsset(asset:any, config:any): void;

    addEventListener(event: string, callback: Function): void;

    play(): Observable<string>;

    setVolume(volume: number): void;

    pause(): Observable<string>;

    resume(): Observable<string>;

    seek(timestamp: number): void;

    stop(id?: string | number);

    getVolume(): number;

    isPlaying():boolean;

    isLive():boolean;

    isPaused():boolean;

    isFinished():boolean;

    isPaused(): boolean;

    isPlaying(): boolean;

    isStopped(): boolean;

    isLive(): boolean;

    isHlsSupported():boolean;
}
