import  { VideoPlayerEventMonitor } from "./video-player.event-monitor";
import { BitrateHistoryLogService } from "./bitrate-history/bitrate-history-log.service";

import { mock } from "ts-mockito";

describe ("Video Player Event Monitor Test Suite:", function()
{

    beforeEach(function()
    {
        this.videoPlayer = {
            subscribe: function(event, callback)
            {
                callback({playhead:0});
                return null;
            }
        };
        this.bitrateHistoryLogService = mock(BitrateHistoryLogService);

        this.service = new VideoPlayerEventMonitor(this.bitrateHistoryLogService);
    });

    it("Should service instance to be defined", function()
    {
        spyOn(this.service, "startMonitor").and.callThrough();
        this.service.startMonitor(this.videoPlayer);

        expect(this.service).toBeDefined();
    });

});
