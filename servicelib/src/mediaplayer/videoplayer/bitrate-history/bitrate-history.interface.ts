export interface IBitrateHistoryRecord {
    unixTimestamp: number;
    streamObject: IStreamObject;
}

// When video bitrate changes, hls calls the Hls.Events.LEVEL_SWITCHED handler
// with a Level (stream) object.  It should include the following properties:
export interface IStreamObject {
    // The bitrate in bits per second.
    bitrate: number;

    // The vertical resolution of the stream.
    height: number;

    // The horizontal resolution of the stream.
    width: number;

    // A unique identifier for the stream.
    id: string;
}
