import { VideoPlayerConfigFactory } from "./video-player.config.factory";
import { ConfigService } from "../../config/config.service";
import { SettingsService }          from "../../settings/settings.service";
import {
    ICdnAccessToken,
    ResponseInterceptor
} from "../../http/http.provider.response.interceptor";
import { IAppConfig } from "../../config/interfaces/app-config.interface";

import { of as observableOf} from "rxjs";
import { mock } from "ts-mockito";
import { MediaTimeLine } from "../../tune/tune.interface";
import { VideoPlayerConstants } from "./video-player.consts";

describe ("Video Player Config Factory Test Suite:", function()
{

    beforeEach(function(this)
    {

        this.serviceConfigMock= {
            deviceInfo : {
                appRegion: "US",
                browser: "chrome",
                browserVersion: "11.22"
            }
        } as IAppConfig;

        this.configServiceMock = mock(ConfigService);
        this.settingServiceMock = mock(SettingsService);
        this.responseInterceptorMock = {
            cdnAccessTokens: observableOf("")
        };

        this.configServiceMock.relativeUrlConfiguration = observableOf("");

        this.service = new VideoPlayerConfigFactory(this.configServiceMock,
                                                    this.settingServiceMock,
                                                    this.responseInterceptorMock,
                                                    this.serviceConfigMock);

        this.service.mediaTimeLine = {
            mediaId: "e35ea97e-6685-3f54-9489-0bd307fce554",
            mediaType: "vod",
            playerType: "local",
            isDataComeFromResume: true,
            isDataComeFromResumeWithDeepLink: false,
            mediaEndPoints: [
                {
                    name: "hls",
                    url: "https://siriusxm-priuatvideo.siriusxm.com/V0cWs4ajE6ooPSTHtlilreLAeFkzjosd/1/ts/1.m3u8"
                }]
        } as MediaTimeLine;
    });

    it("Should Video player config instance to be defined", function()
    {
        expect(this.service).toBeDefined();
    });

    it("Should return player config", function()
    {
        let config = this.service.getConfig();
        expect(config).not.toBeNull();
    });

    it("Should determine autoplay from media timeline", function()
    {
        expect(this.service.getAutoPlay()).toBeFalsy();
    });

    it("should return playback type", function()
    {
        expect(VideoPlayerConstants.VOD).toBe(this.service.getPlaybackType());
    });

    it("Should return video url from media endpoints", function()
    {
        const url = this.service.getVideoUrl("hls",1);
        expect(url).toBe(this.service.mediaTimeLine.mediaEndPoints[0].url);
    });

});
