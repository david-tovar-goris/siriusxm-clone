/**
 * This is known as a barrel file. It allows developers to package and rollup multiple exports
 * into a single file and thus single import/export; e.g., `export * from "./authentication/service/";`
 */
export { BitrateHistoryLogService } from "./bitrate-history/bitrate-history-log.service";
export * from "./video-player.interface";
export { IStreamObject } from "./bitrate-history/bitrate-history.interface";
export { VideoPlayerConstants } from "./video-player.consts";
export { VideoPlayerService } from "./video-player.service";
export { VideoPlayerUtil } from "./video-player.util";
