import { VideoPlayerBufferUnderflowMonitor } from "./video-player.buffer-underflow.monitor";
import { filter, switchMap, skip} from "rxjs/operators";
import { AppMonitorService } from "../../app-monitor";
import { ErrorService } from "../../error";
import { mock } from "ts-mockito";
import { VideoPlayerServiceMock} from "../../test/mocks/mediaplayer/video/video-player.service.mock";
import { VideoPlayerConstants } from "./video-player.consts";

describe("Video Player Buffer Underflow Monitor Test Suite", function()
{
    const appMonitoreService  =  mock(AppMonitorService);
    const errorService = mock(ErrorService);

    beforeEach(function()
    {
        this.service = new VideoPlayerBufferUnderflowMonitor( errorService,
                                                              appMonitoreService);
        this.videoPlayerService = new VideoPlayerServiceMock();
        this.service.setupUnderflowMonitor(this.videoPlayerService);

    });

    it("Removing Error Toast by Video Recovery", function()
    {
        const underflowErrorStateValue = {
            videoRecoveryTimeoutSystemErrorState: false,
            noVideoSystemErrorState: false
        };

        this.service.underflowErrorState$.subscribe( state =>
        {
            expect(state).toEqual(jasmine.objectContaining(underflowErrorStateValue));
        });

        this.videoPlayerService.playbackStateSubject.next(VideoPlayerConstants.PLAYING);
    });

    it("Slow Attempt to play while seeking", function()
    {

        this.service.underflowErrorState$.pipe(
           filter(() => this.videoPlayerService.playbackStateSubject.getValue() === VideoPlayerConstants.SEEKING)
        ).subscribe( val =>
        {
          expect(val.noVideoSystemErrorState).toBeTruthy();

          this.videoPlayerService.playbackStateSubject.next(VideoPlayerConstants.PLAYING);
        }).unsubscribe();

        this.videoPlayerService.playbackStateSubject.next(VideoPlayerConstants.SEEKING);
    });

    it("Buffer ran issue while playing", function()
    {
        let spy = spyOn(this.service, "noVideoSystemError").and.callThrough();
        this.videoPlayerService.stalled$.pipe(
            skip(1),
            filter(val => !!val),
            switchMap(val =>
            {
                return this.service.underflowErrorState$;
            })
        ).subscribe( val =>
        {
            expect(spy).toHaveBeenCalled();
            this.videoPlayerService.playbackStateSubject.next(VideoPlayerConstants.PLAYING);
        }).unsubscribe();

        this.videoPlayerService.stalled$.next(true);
    });


    it("Video Recovery System Error", function()
    {
        let spy = spyOn(this.service, "videoRecoveryTimeoutSystemError").and.callThrough();
        this.service.underflowErrorState$.next({
            videoRecoveryTimeoutSystemErrorState: true,
            noVideoSystemErrorState: false
        });

        expect(spy).toHaveBeenCalled();
    });

    it("No Video System Error", function()
    {
        let spy = spyOn(this.service, "noVideoSystemError").and.callThrough();
        this.service.underflowErrorState$.next({
            videoRecoveryTimeoutSystemErrorState: false,
            noVideoSystemErrorState: true
        });

        expect(spy).toHaveBeenCalled();
    });
});

