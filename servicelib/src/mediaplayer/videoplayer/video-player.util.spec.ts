/// <reference path="../../../node_modules/@types/jasmine/index.d.ts" />

import { VideoPlayerUtil } from "./video-player.util";

describe("Video Player Util Test Suite >>", function()
{
    describe("Infrastructure >> ", function()
    {
        it("Should be defined", function()
        {
            expect(VideoPlayerUtil).toBeDefined();
        });
    });
});
