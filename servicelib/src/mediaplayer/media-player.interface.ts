import { Observable } from "rxjs";
import { IMediaAssetMetadata } from "./media-asset-metadata.interface";
import { IPlayhead } from "./playhead.interface";

export interface IMediaPlayer
{
    // Playback control methods
    play(): void;
    // pause(): Observable<string>;
    pause();
    resume();
    togglePausePlay();
    seek(timestamp: number): void;
    stop(id?: string | number);

    playbackState: Observable<string>;
    playhead$: Observable<IPlayhead>;
    playbackComplete$: Observable<boolean>;
    restart$?: Observable<boolean>;
    bufferEmpty$?: Observable<any>;

    // Getters/Setters
    getId(): string;
    getDuration(): number;
    getDurationInSeconds(): number;
    // getPlayheadTime(): number;
    // getPlayheadTimeInSeconds(): number;
    getPlayheadZuluTime(): number;
    // getPlayheadStartTime(): number;
    getPlayheadStartZuluTime(): number;
    getPlaybackType(): string;
    getJumpActionCuts();
    getState(): string;
    getType(): string;
    getLastSeekTime(): number;
    getVolume(): number;
    setVolume(volume: number): void;
    mute(): void;
    unmute(): void;

    isCurrent: boolean;

    // TODO: BMR: 10/12/2017: We may not need this but in refactoring the playhead controls to be reusable it's necessary for video right now.
    getMediaAssetMetadata(): IMediaAssetMetadata;

    // Creational methods
    destroy: Function;
    warmUp();

    // State methods
    isPlaying(): boolean;
    isPaused(): boolean;
    isStopped(): boolean;
    isFinished(): boolean;
    isLive(): boolean;
}

export interface IMultiTrackMediaPlayer extends IMediaPlayer
{
    skip();
    rewind();
}


export interface IMediaTrigger
{
    isNewMediaId: boolean;
    isNewMediaType: boolean;
    isNewPlayerType: boolean;
}
