import * as _ from "lodash";

import { IClip, IMediaEndPoint, ICrossfade, ITime } from "../../tune";
import { MultiTrackList } from "./multi-track-list";
import { mockTracksList } from "../../test/mocks/mediaplayer/multi-track-list.mock";
import { MultiTrackConstants } from "./multi-track.consts";

describe("MultiTrackList Test Suite", function()
{
    beforeEach(function()
    {
        this.channelGUID            = "channelGUID";
        this.mockAppendTrack     = [{
            assetGUID: "assetGUID4",
            albumName: "albumName",
            artistName: "artistName",
            assetType: "assetType",
            category: "category",
            clipImageUrl: "clipImageUrl",
            consumptionInfo: "consumptionInfo",
            mediaEndPoints: {} as IMediaEndPoint[],
            duration: 200,
            title: "title",
            status: "FUTURE",
            contentUrlList: "contentUrlList",
            layer: "layer",
            times: {} as ITime,
            index: 0,
            crossfade:{} as ICrossfade

        }
        ];
        this.mockTracks                     = mockTracksList;
        this.multiTrackList = new MultiTrackList(this.channelGUID,this.mockTracks);
    });

    describe('constructor()', function()
    {
        it('sets crossFade to -1 if next track is interstitial with shorter duration than crossFade', function()
        {
            let clipList = _.cloneDeep(mockTracksList);
            clipList[0].crossfade.crossFade = 13000;
            clipList[1].duration = 9.234;
            this.multiTrackList = new MultiTrackList(this.channelGUID, clipList);
            expect((this.multiTrackList as any).tracks[0].crossfade.crossFade).toBe(-1);
        });
    });

    describe("appendTracks", function()
    {
        it("Should add the new track list item", function()
        {
            const currentMultiTrackList = this.multiTrackList.appendTracks(this.channelGUID, this.mockAppendTrack);
            const currentTracks         = currentMultiTrackList.toArray();
            expect(currentTracks.length).toEqual(4);
            expect(currentTracks[ 3 ]).toEqual(this.mockAppendTrack[ 0 ]);
        });

        it("Should not add the new track item if already exists.", function()
        {
            const appendTrackList       = [this.mockTracks[ 0 ]];
            const currentMultiTrackList = this.multiTrackList.appendTracks(this.channelGUID, appendTrackList);
            const currentTracks         = currentMultiTrackList.toArray();
            expect(currentTracks.length).toEqual(3);
        });

        it("Should not add the new track item with consume status", function()
        {
            const initialTrackListLen = this.multiTrackList.toArray().length;

            let appendTrackList         = _.cloneDeep(this.mockAppendTrack);
            appendTrackList[ 0 ].status = MultiTrackConstants.ENDED;

            const currentMultiTrackList = this.multiTrackList.appendTracks(this.channelGUID, appendTrackList);
            const currentTracks         = currentMultiTrackList.toArray();
            const isAppended            = currentTracks.some((track) =>
            {
                return track.status === MultiTrackConstants.ENDED;
            });
            expect(isAppended).toBe(false);
            expect(currentTracks.length).toEqual(initialTrackListLen);
        });

        it("Should not add the new track item with error status", function()
        {
            const initialTrackListLen = this.multiTrackList.toArray().length;

            let appendTrackList         = _.cloneDeep(this.mockAppendTrack);
            appendTrackList[ 0 ].status = MultiTrackConstants.ERROR;

            const currentMultiTrackList = this.multiTrackList.appendTracks(this.channelGUID, appendTrackList);
            const currentTracks         = currentMultiTrackList.toArray();
            const isAppended            = currentTracks.some((track) =>
            {
                return track.status === MultiTrackConstants.ERROR;
            });
            expect(isAppended).toBe(false);
            expect(currentTracks.length).toEqual(initialTrackListLen);
        });

        it("Should not add the new track item with skip status", function()
        {
            const initialTrackListLen = this.multiTrackList.toArray().length;

            let appendTrackList         = _.cloneDeep(this.mockAppendTrack);
            appendTrackList[ 0 ].status = MultiTrackConstants.SKIP;

            const currentMultiTrackList = this.multiTrackList.appendTracks(this.channelGUID, appendTrackList);
            const currentTracks         = currentMultiTrackList.toArray();
            const isAppended            = currentTracks.some((track) =>
            {
                return track.status === MultiTrackConstants.SKIP;
            });
            expect(isAppended).toBe(false);
            expect(currentTracks.length).toEqual(initialTrackListLen);
        });
    });

    describe("firstTrack", function()
    {
        it("should return the first track item while no error tracks", function()
        {
            expect(this.multiTrackList.firstTrack()).toEqual(this.mockTracks[ 0 ]);
        });

        it("should return the first track after the" +
            " last error if error track exists", function()
        {
            let currentList         = _.cloneDeep(this.mockTracks);
            currentList[ 0 ].status = MultiTrackConstants.ERROR;

            this.multiTrackList = new MultiTrackList(this.channelGUID, currentList);

            expect(this.multiTrackList.firstTrack()).toEqual(currentList[ 1 ]);
        });

        it("should return there are no tracks in this track list", function()
        {
            this.multiTrackList = new MultiTrackList(this.channelGUID, []);
            try
            {
                this.multiTrackList.firstTrack();
            }
            catch (exception)
            {
                expect(exception).toEqual("there are no tracks in this track list");
            }
        });

        it("should return there are no tracks if all tracks are error tracks", function()
        {
            let currentList = _.cloneDeep(this.mockTracks);
            currentList     = _.map(currentList, (track) =>
            {
                track.status = MultiTrackConstants.ERROR;
                return track;
            });
            this.multiTrackList  = new MultiTrackList(this.channelGUID, currentList);
            try
            {
                this.multiTrackList.firstTrack();
            }
            catch (exception)
            {
                expect(exception).toEqual("there are no tracks in this track list");
            }
        });
    });

    describe("toArray", function()
    {
        it("should return the tracks", function()
        {
            expect(this.multiTrackList.toArray()).toEqual(this.mockTracks);
        });
    });

    describe("getChannelGuid", function()
    {
        it("should return the channelGuid", function()
        {
            expect(this.multiTrackList.getChannelIdentifier()).toEqual(this.channelGUID);
        });
    });

    describe("isPristine()", function()
    {
        it("should return false if no preload, current or skip status", function()
        {
            expect(this.multiTrackList.isPristine()).toEqual(true);
        });

        it("should return true if preload status", function()
        {
            let currentTrackList         = _.cloneDeep(this.mockTracks);
            currentTrackList[ 0 ].status = MultiTrackConstants.PRELOAD;

            this.multiTrackList = new MultiTrackList(this.channelGUID, currentTrackList);
            expect(this.multiTrackList.isPristine()).toBe(false);
        });

        it("should return true if current status", function()
        {
            let currentTrackList         = _.cloneDeep(this.mockTracks);
            currentTrackList[ 0 ].status = MultiTrackConstants.CURRENT;

            this.multiTrackList = new MultiTrackList(this.channelGUID, currentTrackList);
            expect(this.multiTrackList.isPristine()).toBe(false);
        });

        it("should return true if skip status", function()
        {
            let currentTrackList         = _.cloneDeep(this.mockTracks);
            currentTrackList[ 0 ].status = MultiTrackConstants.SKIP;

            this.multiTrackList = new MultiTrackList(this.channelGUID, currentTrackList);
            expect(this.multiTrackList.isPristine()).toBe(false);
        });
    });

    describe("getCurrentTrack", function()
    {
        it("should return track with current status ", function()
        {
            let currentTrackList         = _.cloneDeep(this.mockTracks);
            currentTrackList[ 1 ].status = MultiTrackConstants.CURRENT;

            this.multiTrackList = new MultiTrackList(this.channelGUID, currentTrackList);
            expect(this.multiTrackList.getCurrentTrack()).toEqual(currentTrackList[ 1 ]);
        });
    });

    describe("nextForPreload", function()
    {
        it("should return track with future status ", function()
        {
            let currentTrackList         = _.cloneDeep(this.mockTracks);
            currentTrackList[ 0 ].status = MultiTrackConstants.CURRENT;

            this.multiTrackList = new MultiTrackList(this.channelGUID, currentTrackList);
            expect(this.multiTrackList.nextForPreload()).toEqual(currentTrackList[ 1 ]);
        });
    });

    describe("listAfterSkip", function()
    {
        it("should change current track status to skip and next track status to current " +
            "if next track exists", function()
        {
            let currentTracks         = _.cloneDeep(this.mockTracks);
            currentTracks[ 0 ].status = MultiTrackConstants.CURRENT;

            this.multiTrackList = new MultiTrackList(this.channelGUID, currentTracks);

            const spyGetCurrentTrack = spyOn(this.multiTrackList, "getCurrentTrack").and.returnValue(currentTracks[ 0 ]);

            const resultMultiTrackList = this.multiTrackList.listAfterSkip();
            expect(spyGetCurrentTrack).toHaveBeenCalled();

            const resultTracks = resultMultiTrackList.toArray();
            expect(resultTracks[ 0 ].status).toEqual(MultiTrackConstants.SKIP);
            expect(resultTracks[ 1 ].status).toEqual(MultiTrackConstants.CURRENT);
        });

        it("should change current track status to skip if next track do not exists", function()
        {
            let currentTracks         = _.cloneDeep(this.mockTracks);
            currentTracks[ 2 ].status = MultiTrackConstants.CURRENT;

            this.multiTrackList = new MultiTrackList(this.channelGUID, currentTracks);

            const spyGetCurrentTrack = spyOn(this.multiTrackList, "getCurrentTrack").and.returnValue(currentTracks[ 2 ]);

            const resultMultiTrackList = this.multiTrackList.listAfterSkip();
            expect(spyGetCurrentTrack).toHaveBeenCalled();

            const resultTracks = resultMultiTrackList.toArray();
            expect(resultTracks[ 0 ].status).toEqual(MultiTrackConstants.FUTURE);
            expect(resultTracks[ 1 ].status).toEqual(MultiTrackConstants.FUTURE);
            expect(resultTracks[ 2 ].status).toEqual(MultiTrackConstants.SKIP);
        });
    });

    describe("listAfterError", function()
    {
        it("should return track status as error for the assestguid and all other status to future",
            function()
            {
                let resTrackList = this.multiTrackList.listAfterError("assetGUID2").toArray();
                expect(resTrackList[ 1 ].status).toEqual(MultiTrackConstants.ERROR);
                expect(resTrackList[ 0 ].status).toEqual(MultiTrackConstants.FUTURE);
                expect(resTrackList[ 2 ].status).toEqual(MultiTrackConstants.FUTURE);
            });

        it("should return if no assetGUID found", function()
        {
            expect(this.multiTrackList.listAfterError("assetGUID4")).toBeUndefined();
        });
    });

    describe("listAfterStarted", function()
    {
        it("should change the status to current if asesetguid exist", function()
        {
            let cloneTrackList = _.cloneDeep(this.mockTracks);
            this.multiTrackList     = new MultiTrackList(this.channelGUID, cloneTrackList);

            let resTrackList = this.multiTrackList.listAfterStarted("assetGUID2").toArray();
            expect(resTrackList[ 1 ].status).toEqual(MultiTrackConstants.CURRENT);
        });

        it("should return if asesetguid doesnt exist", function()
        {
            let resMultiTrackList = this.multiTrackList.listAfterStarted("assetGUID4");
            expect(resMultiTrackList).toBeUndefined();
        });
    });

    describe("listAfterFinish", function()
    {
        it("should change the status to consume if asesetguid exist", function()
        {
            let cloneTrackList = _.cloneDeep(this.mockTracks);
            this.multiTrackList     = new MultiTrackList(this.channelGUID, cloneTrackList);
            let resTrackList   = this.multiTrackList.listAfterFinish("assetGUID2").toArray();
            expect(resTrackList[ 1 ].status).toEqual(MultiTrackConstants.ENDED);
            expect(resTrackList[ 2 ].status).toEqual(MultiTrackConstants.CURRENT);
        });

        it("should change the current track status to consume if next track doesnt exist ", function()
        {
            let cloneTrackList = _.cloneDeep(this.mockTracks);
            this.multiTrackList     = new MultiTrackList(this.channelGUID, cloneTrackList);
            let resTrackList   = this.multiTrackList.listAfterFinish("assetGUID3").toArray();
            expect(resTrackList[ 2 ].status).toEqual(MultiTrackConstants.ENDED);
        });

        it("should return if asesetguid doesnt exist", function()
        {
            expect(this.multiTrackList.listAfterFinish("assetGUID4")).toBeUndefined();
        });
    });

    describe("listAfterResume", function()
    {
        it("should return the same track list if current", function()
        {
            let currentTrackList         = _.cloneDeep(this.mockTracks);
            currentTrackList[ 1 ].status = MultiTrackConstants.CURRENT;

            this.multiTrackList      = new MultiTrackList(this.channelGUID, currentTrackList);
            expect(this.multiTrackList.listAfterResume().toArray()).toEqual(currentTrackList);
        });

        it("should change the preload track status to current if no current track found ", function()
        {
            let currentTrackList         = _.cloneDeep(this.mockTracks);
            currentTrackList[ 1 ].status = MultiTrackConstants.PRELOAD;

            this.multiTrackList   = new MultiTrackList(this.channelGUID, currentTrackList);
            expect( this.multiTrackList.listAfterResume().toArray()[1].status).toEqual(MultiTrackConstants.CURRENT);
        });
    });
});

