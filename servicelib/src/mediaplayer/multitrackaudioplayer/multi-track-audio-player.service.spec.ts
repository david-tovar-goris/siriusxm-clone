/// <reference path="../../../node_modules/@types/jasmine/index.d.ts" />

import * as AudioPlayer from "sxm-audio-player";
import { MultiTrackAudioPlayerService } from "./multi-track-audio-player.service";
import { ICurrentlyPlayingMedia } from "../../tune/tune.interface";
import { BehaviorSubject ,  Subject ,  of as observableOf } from "rxjs";
import { take } from 'rxjs/operators';
import { MediaTimeLine } from "../../tune/tune.interface";
import { IPlayhead, Playhead } from "../playhead.interface";
import { AudioPlayerConstants } from "../audioplayer/audio-player.consts";
import { AudioPlayerEventTypes } from "../audioplayer/audio-player.event-types";
import { MockAppConfig }    from "../../test/mocks/app.config.mock";
import {ContentTypes} from "../../service/types";
import { mock } from "ts-mockito";
import { AppMonitorService, IAppByPassState } from "../../app-monitor";

describe("Multi Track Audio Player Service Test Suite >>", function()
{
    beforeEach(function()
    {
        this.audioPlayer = new AudioPlayer();
        this.initializationService = {};
        this.tuneService = {};

        this.currentlyPlayingService = {
            currentlyPlayingData: new BehaviorSubject<ICurrentlyPlayingMedia>({} as ICurrentlyPlayingMedia)
        };

        this.noopService = {};
        this.multiTrackAudioPlayerConfigFactory = {};
        this.chromecastService = {};
        this.sessionTransferService = {};

        this.mediaTimeLineService = {
            mediaTimeLine: new BehaviorSubject<MediaTimeLine>({mediaType:ContentTypes.ADDITIONAL_CHANNELS} as MediaTimeLine)
        };

        this.refreshTracksService = {};

        this.audioPlayerEventMonitor = {
            playhead$: new BehaviorSubject<IPlayhead>(new Playhead()),
            trackFailed$: new Subject<any>()
        };

        this.playheadTimeStampService = {
            playhead$: new BehaviorSubject<IPlayhead>(new Playhead())
        };

        this.appConfig = new MockAppConfig();
        this.appMonitorServiceMock = mock(AppMonitorService);
        this.bypassMonitorService = {
            bypassErrorState: observableOf({} as IAppByPassState)
        };
        function construct()
        {
            this.service = new MultiTrackAudioPlayerService(
                this.audioPlayer,
                this.appConfig,
                this.initializationService,
                this.tuneService,
                this.currentlyPlayingService,
                this.playheadTimeStampService,
                this.noopService,
                this.multiTrackAudioPlayerConfigFactory,
                this.chromecastService,
                this.appMonitorServiceMock,
                this.sessionTransferService,
                this.mediaTimeLineService,
                this.refreshTracksService,
                this.audioPlayerEventMonitor,
                this.bypassMonitorService
            );
        }

        this.construct = construct;

        this.construct();
    });

    describe('constructor()', function()
    {
        it('subscribes to playbackState observable', function(done)
        {
            this.service.playbackStateSubject.next(AudioPlayerConstants.FINISHED);
            this.service.playbackState.pipe(take(1)).subscribe(() =>
            {
                expect(this.service.currentPlaybackState).toEqual(AudioPlayerConstants.FINISHED);
                done();
            });
        });

        it('subscribes to finished events from the audio player event monitor', function()
        {
            spyOn(this.service, 'onFinished');
            const playhead = new Playhead();
            playhead.type = AudioPlayerEventTypes.FINISHED;
            this.audioPlayerEventMonitor.playhead$.next(playhead);
            expect(this.service.onFinished).toHaveBeenCalledWith(playhead);
        });
    });
});
