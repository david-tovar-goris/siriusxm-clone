import { IMediaVideo } from "../tune/tune.interface";
import { IMediaSegment } from "../tune";

export interface IMediaAssetMetadata
{
    mediaId: string;
    seriesName: string;
    episodeTitle: string;

    // view model / client API for the table of segments
    // that appears in the apron under video on the "Now Playing" screen
    apronSegments: Array<IApronSegment>;
}

// represents one row in the table of segments
// that appears in the apron under video on the "Now Playing" screen
export interface IApronSegment
{
    title: string;
    videoMarker?: IMediaVideo;
    videoStartTime?: number;
    timeDisplay: string;
    startTimeInSeconds: number;
    endTimeInSeconds: number;
    secondsFromBeginningOfEpisode: number;
    mediaSegment: IMediaSegment;
}
