import { Subject, BehaviorSubject } from 'rxjs';
import { MediaTimestampService } from "./media-timestamp.service";
import { ICurrentlyPlayingMedia, IMediaSegment } from "../tune";
import { of as observableOf } from 'rxjs';

describe('MediaTimestampService', function()
{
    beforeEach(function()
    {
        this.mockDependencies = function()
        {
            this.mockLiveTimeService = {
                liveTime$: new BehaviorSubject({ zuluMilliseconds: 0 })
            };
            this.mockCurrentlyPlayingService = {
                currentlyPlayingData: observableOf({})
            };
            this.mockMediaPlayerService = {
                isNewMedia: jasmine.createSpy('isNewMedia'),
                mediaPlayer: {
                    getDurationInSeconds: jasmine.createSpy('getDurationInSeconds')
                },
                playheadSubFunctions: []
            };

            this.mockPlayheadTimestampService = {
                playhead: new Subject()
            };
        };

        this.createService = function()
        {
            this.service = new MediaTimestampService(
                this.mockLiveTimeService,
                this.mockMediaPlayerService,
                this.mockCurrentlyPlayingService,
                this.mockPlayheadTimestampService
            );
        };

        this.setPayload = function(data) {this.mockCurrentlyPlayingService.currentlyPlayingData = observableOf(data);};

        this.mockDependencies();
        this.createService();
    });

    describe('observePlayheadTime()', function()
    {
        beforeEach(function()
        {
            let payload = {
                mediaType: "live",
                episode: {
                    times: {
                        zuluStartTime: 1525456800000 // 6:00 PM GMT
                    }
                }
            } as ICurrentlyPlayingMedia;
            this.setPayload(payload);
            this.createService();
        });

        describe('when dragging scrubball', function()
        {
            beforeEach(function()
            {
                this.service.isDraggingScrubBall = true;
                this.service.playheadTimestamp = 10;
                this.service.playheadTimestamp$.next(10);
            });

            it('does not update playheadTimestamp', function()
            {
                this.mockPlayheadTimestampService.playhead.next({ currentTime: { seconds: 11 } });
                expect(this.service.playheadTimestamp).toEqual(10);
            });

            it('does not update playheadTimestamp$', function()
            {
                this.mockPlayheadTimestampService.playhead.next({ currentTime: { seconds: 11 } });
                expect(this.service.playheadTimestamp$.getValue()).toBe(10);
            });
        });

        describe('when not dragging scrubball', function()
        {
            beforeEach(function()
            {
                this.service.isDraggingScrubBall = false;
            });

            it('does update playheadTimestamp', function()
            {
                this.service.playheadTimestamp = 10;
                this.mockPlayheadTimestampService.playhead.next({ currentTime: { seconds: 11 } });
                expect(this.service.playheadTimestamp).toEqual(11);
            });

            it('updates playheadTime$', function()
            {
                this.mockPlayheadTimestampService.playhead.next({ currentTime: { seconds: 11 } });
                let sub = this.service.playheadTimestamp$.subscribe((val) =>
                {
                    expect(val).toBe(11);
                });
                sub.unsubscribe();
            });
        });
    });

    describe('setPlayheadTimestamp()', function()
    {
        it('sets playheadTimestamp', function()
        {
            this.service.playheadTimestamp = 999;
            this.service.setPlayheadTimestamp(1000);
            expect(this.service.playheadTimestamp).toBe(1000);
        });
    });

    describe('getDurationTimestamp()', function()
    {
        it('returns 0 if the returned value from GetDurationInSeconds is falsey', function()
        {
            this.mockMediaPlayerService.mediaPlayer.getDurationInSeconds.and.returnValue(null);
            expect( this.service.durationTimestamp).toEqual(0);
        });

        it('sets the durationTimestamp', function()
        {
            this.mockMediaPlayerService.mediaPlayer.getDurationInSeconds.and.returnValue(100);
            expect( this.service.durationTimestamp).toEqual(0);
        });
    });

    describe('resetDurationTimestamp()', function()
    {
        it('sets the durationTimestamp back to 0', function()
        {
            expect(this.service.resetDurationTimestamp()).toEqual(0);
        });
    });

    describe('resetLiveTimestamp()', function()
    {
        it('sets the livePointTimestamp back to 0', function()
        {
            expect(this.service.resetLiveTimestamp()).toEqual(0);
        });
    });

    describe('resetPlayheadTimestamp()', function()
    {
        it('resets the PlayheadTimestamp back to 0', function()
        {
            this.service.playheadTimestamp = 1000;
            expect(this.service.resetPlayheadTimestamp()).toEqual(0);
        });
    });

    describe('resetSegmentMarkerTimestamps', function()
    {
        it('resets the SegmentMarkerTimestamps back to an empty array', function()
        {
            this.service.segmentMarkers = [{times: { zeroStartTime: 1}}] as IMediaSegment[];
            expect(this.service.resetSegmentMarkers()).toEqual([]);
        });
    });

    describe('resetTimestamps()', function()
    {
        it('resets the Duration Timestamp', function()
        {
            spyOn(this.service, 'resetDurationTimestamp');
            this.service.resetTimestamps();
            expect(this.service.resetDurationTimestamp).toHaveBeenCalled();
        });

        it('resets the Live Timestamp', function()
        {
            spyOn(this.service, 'resetLiveTimestamp');
            this.service.resetTimestamps();
            expect(this.service.resetLiveTimestamp).toHaveBeenCalled();
        });

        it('resets the Playhead Timestamp', function()
        {
            spyOn(this.service, 'resetPlayheadTimestamp');
            this.service.resetTimestamps();
            expect(this.service.resetPlayheadTimestamp).toHaveBeenCalled();

        });

        it('resets the Segment Marker Timestamps', function()
        {
            spyOn(this.service, 'resetSegmentMarkers');
            this.service.resetTimestamps();
            expect(this.service.resetSegmentMarkers).toHaveBeenCalled();
        });
    });

    describe('observeLiveTime()', function()
    {
        describe('when dmca data is live media type', function()
        {
            beforeEach(function()
            {
                let payload = {
                    mediaType: "live",
                    episode: {
                        times: {
                            zuluStartTime: 1525456800000 // 6:00 PM GMT
                        }
                    }
                } as ICurrentlyPlayingMedia;

                this.mockMediaPlayerService.mediaPlayer.getDurationInSeconds.and.returnValue(3600);
                this.mockLiveTimeService.liveTime$.next({ zuluMilliseconds: 1525458600000 });  // 30 min after start of show.
                this.setPayload(payload);
                this.createService();
            });

            it('sets livepoint timestamp to zero based time', function()
            {
                expect(this.service.livePointTimestamp).toEqual(1800);
            });

            it('sets livepoint timestamp to zero based time even after duration of episode', function()
            {
                this.mockLiveTimeService.liveTime$.next({ zuluMilliseconds: 1525464000000 });  // 2 hrs after start of show.
                expect(this.service.livePointTimestamp).toEqual(7200);
            });
        });

        describe('when dmca data is not live media type', function()
        {
            beforeEach(function()
            {
                let payload = {
                    mediaType: "aod",
                    episode: {
                        times: {
                            zuluStartTime: 1525456800000 // 6:00 PM GMT
                        }
                    }
                } as ICurrentlyPlayingMedia;

                this.mockMediaPlayerService.mediaPlayer.getDurationInSeconds.and.returnValue(3600);
                this.mockLiveTimeService.liveTime$.next({ zuluMilliseconds: 1525458600000 });  // 30 min after start of show.
                this.setPayload(payload);
                this.createService();
            });

            it('sets livepoint timestamp to duration if within duration', function()
            {
                expect(this.service.livePointTimestamp).toEqual(3600);
            });

            it('sets livepoint timestampe to duration if outside duration', function()
            {
                this.mockLiveTimeService.liveTime$.next({ zuluMilliseconds: 1525464000000 });  // 2 hrs after start of show.
                expect(this.service.livePointTimestamp).toEqual(3600);
            });
        });
    });
});
