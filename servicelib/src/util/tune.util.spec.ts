/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />/*  describe("getAlbumNameForCut >> ", () =>
import { findMediaItemByTimestamp, getAlbumArtForCut, getAlbumNameForCut, getArtistNameForCut } from "./tune.util";
import { CreativeArtsTypes } from "../service/types/creative.arts.item";
import { IMediaItem, ITime } from "../tune/tune.interface";

describe("findMediaItemByTimestamp >> ", function()
{
    it("Infrastructure", function()
    {
        expect(findMediaItemByTimestamp).toBeDefined();
    });

    describe("Execution >> ", function()
    {
        it("When TimeStamp > start time of the episode  and media item returned", function()
        {
            const timeStamp                = 105;
            const mediaItems: IMediaItem[] = [
                {
                    assetGUID: "assetGuid",
                    times    :
                        {
                            zuluStartTime: 100,
                            zuluEndTime  : 90
                        } as ITime,
                    layer    : "episode"
                },
                {
                    assetGUID: "assetGuid1",
                    times    :
                        {
                            zuluStartTime: 150,
                            zuluEndTime  : 200
                        } as ITime,
                    layer    : "episode"
                }
            ];
            expect(findMediaItemByTimestamp(timeStamp, mediaItems)).toEqual(mediaItems[ 0 ]);
        });

        it("When TimeStamp < start time of the episode and returned second item", function()
        {
            const timeStamp                = 105;
            const mediaItems: IMediaItem[] = [
                {
                    assetGUID: "assetGuid",
                    times    :
                        {
                            zuluStartTime: 110,
                            zuluEndTime  : 200
                        } as ITime,
                    layer    : "episode"
                },
                {
                    assetGUID: "assetGuid",
                    times    :
                        {
                            zuluStartTime: 100,
                            zuluEndTime  : 300
                        } as ITime,
                    layer    : "episode"
                }
            ];
            expect(findMediaItemByTimestamp(timeStamp, mediaItems)).toEqual(mediaItems[ 1 ]);
        });

        it("When TimeStamp < start time of the episode not found in the list and returns the first item", function()
        {
            const timeStamp                = 100;
            const mediaItems: IMediaItem[] = [
                {
                    assetGUID: "assetGuid",
                    times    :
                        {
                            zuluStartTime: 110,
                            zuluEndTime  : 200
                        } as ITime,
                    layer    : "episode"
                },
                {
                    assetGUID: "assetGuid",
                    times    :
                        {
                            zuluStartTime: 200,
                            zuluEndTime  : 300
                        } as ITime,
                    layer    : "episode"
                }
            ];
            expect(findMediaItemByTimestamp(timeStamp, mediaItems)).toEqual(mediaItems[ 0 ]);
        });

        it("Returns First item when times null", function()
        {
            const timeStamp                = 100;
            const mediaItems: IMediaItem[] = [ {
                assetGUID: "assetGuid",
                times    : null,
                layer    : "episode"
            }
            ];
            expect(findMediaItemByTimestamp(timeStamp, mediaItems)).toEqual(mediaItems[ 0 ]);
        });
    });
});

describe("getArtistNameForCut >> ", function()
{
    it("Infrastructure", function()
    {
        expect(getArtistNameForCut).toBeDefined();
    });

    describe("Execution >> ", function()
    {
        it("Returns album name", function()
        {
            const artistTitle = "Artist Title";
            const cut         = {
                assetGUID      : "assetGuid",
                times          : null,
                album          : null,
                artists        : [ {
                    name: artistTitle
                }
                ],
                consumptionInfo: "",
                duration       : 10,
                legacyId       : null,
                title          : "",
                layer          : '',
                cutContentType : "",
                contentType    : ""
            };
            expect(getArtistNameForCut(cut)).toEqual(artistTitle);
        });
    });
});

describe("getAlbumNameForCut >> ", function()
{
    it("Infrastructure", function()
    {
        expect(getAlbumNameForCut).toBeDefined();
    });

    describe("Execution >> ", function()
    {
        it("Returns album name", function()
        {
            const albumTitle = "Album Title";
            const cut        = {
                assetGUID      : "assetGuid",
                times          : null,
                album          : {
                    type : CreativeArtsTypes.IMAGE,
                    title: albumTitle
                },
                artists        : null,
                consumptionInfo: "",
                duration       : 10,
                legacyId       : null,
                title          : "",
                layer          : '',
                cutContentType : "",
                contentType    : ""
            };
            expect(getAlbumNameForCut(cut)).toEqual(albumTitle);
        });
    });
});

describe("getAlbumArtForCut >> ", function()
{
    it("Infrastructure", function()
    {
        expect(getAlbumArtForCut).toBeDefined();
    });

    describe("Execution >> ", function()
    {
        let cut  = null;
        let arts = null;
        beforeEach(function()
        {
            const albumTitle = "Album Title";
            arts             = [ {
                encrypted: false,
                type     : CreativeArtsTypes.IMAGE,
                size     : "medium",
                name     : "image1",
                platform : "",
                url      : "testUrl1",
                width    : 30,
                height   : 30
            },
                {
                    encrypted: false,
                    type     : CreativeArtsTypes.IMAGE,
                    size     : "large",
                    name     : "image2",
                    platform : "",
                    width    : 30,
                    height   : 30
                },
                {
                    encrypted: false,
                    type     : CreativeArtsTypes.IMAGE,
                    name     : "image2",
                    platform : "",
                    width    : 30,
                    height   : 30
                }
            ];
            cut              = {
                assetGUID      : "assetGuid",
                times          : null,
                album          : {
                    title       : albumTitle,
                    creativeArts: arts
                },
                artists        : null,
                consumptionInfo: "",
                duration       : 10,
                legacyId       : null,
                title          : "",
                cutContentType : ""
            };
        });

        it("Should Returns album name", function()
        {
            expect(getAlbumArtForCut(cut)).toEqual(arts[ 0 ].url);
        });

        it("Should Not Returns album name", function()
        {
            expect(getAlbumArtForCut(cut, "large1")).toEqual('');
        });

        it("Should Not Returns album name when artwork size not defined", function()
        {
            expect(getAlbumArtForCut(cut, "large1")).toEqual('');
        });
    });
});
