/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />
import { DateUtil } from "./date.util";
import * as moment from "moment";


describe("DateUtil class", () =>
 {
     it('localizeDateTime input is the empty string, should return an empty string', () =>
     {
         let input: string = '';
         expect(DateUtil.localizeDateTime(input)).toEqual(input);
     });

     it('localizeDateTime input does not have any formatting info, should return the original string', () =>

     {
         let input:string = 'timestamp';
         expect(DateUtil.localizeDateTime(input)).toEqual(input);
     });

     it("localizeDateTime input has LOCAL_SHORT_DAY with current date, should return Today", () =>
     {
         let formattedString = "Today";
         let today = new Date();
         let input = "{{LOCAL_SHORT_DAY" + ':' + today.getTime()/1000 + '}}';
         expect(DateUtil.localizeDateTime(input)).toEqual(formattedString);
     });

     it("localizeDateTime input has LOCAL_SHORT_DAY with yesterday's date, should return Yesterday along with if any suffix string is provided", () =>
     {
         let formattedString = "Yesterday 2h 6m";
         let dateYesterday:Date = new Date();
         dateYesterday.setDate(dateYesterday.getDate() - 1);
         let input = "{{LOCAL_SHORT_DAY" + ':' + dateYesterday.getTime()/1000 + '}} 2h 6m';
         expect(DateUtil.localizeDateTime(input)).toEqual(formattedString);
     });

     it("localizeDateTime input has LOCAL_SHORT_DAY with tomorrow's date, should return Yesterday along with if any suffix string is provided", () =>
     {
         let formattedString = "Tomorrow 2h 6m";
         let dateTomorrow:Date = new Date();
         dateTomorrow.setDate(dateTomorrow.getDate() + 1);
         let input = "{{LOCAL_SHORT_DAY" + ':' + dateTomorrow.getTime()/1000 + '}} 2h 6m';
         expect(DateUtil.localizeDateTime(input)).toEqual(formattedString);
     });

     it("localizeDateTime input has LOCAL_SHORT_DAY with date in past 6 days, should return day of the week", () =>
     {
         let dateThreeDaysBack = new Date();
         dateThreeDaysBack.setDate(dateThreeDaysBack.getDate()-3);
         let formattedString = DateUtil.getDayOfTheWeek(dateThreeDaysBack) + " 2h 6m";
         let input = "{{LOCAL_SHORT_DAY" + ':' + dateThreeDaysBack.getTime()/1000 + '}} 2h 6m';
         expect(DateUtil.localizeDateTime(input)).toEqual(formattedString);
     });

     it("localizeDateTime input has LOCAL_SHORT_TIME, should return time in short form per device 12/24 hour setting", () =>
     {
         let timeNow = new Date();
         let formattedString = timeNow.toLocaleTimeString().toLocaleLowerCase().replace(/:\d{2}\s/,' ') + " 2h 6m";
         let input = "{{LOCAL_SHORT_TIME" + ':' + timeNow.getTime()/1000 + '}} 2h 6m';
         expect(DateUtil.localizeDateTime(input)).toEqual(formattedString);
     });

     it("localizeDateTime input has LOCAL_SHORT_DATE, should return Today", () =>
     {
         let formattedString = "Today 2h 6m";
         let today = new Date();
         let input = "{{LOCAL_SHORT_DATE" + ':' + today.getTime()/1000 + '}} 2h 6m';
         expect(DateUtil.localizeDateTime(input)).toEqual(formattedString);
     });

     it('localizeDateTime input has LOCAL_SHORT_DATE, should return day of the week if within past 6 days', () =>
     {
         let dateTwoDaysBack = new Date();
         dateTwoDaysBack.setDate(dateTwoDaysBack.getDate()-2);
         let formattedString = DateUtil.getDayOfTheWeek(dateTwoDaysBack) + " 2h 6m";
         let input = "{{LOCAL_SHORT_DATE" + ':' + dateTwoDaysBack.getTime()/1000 + '}} 2h 6m';
         const output = DateUtil.localizeDateTime(input);
         expect(output).toEqual(formattedString);
     });

     it('localizeDateTime input has LOCAL_SHORT_DATE, should return day of the week because date is within past 6 days', () =>
     {
         let dateSixDaysBack = new Date();
         dateSixDaysBack.setUTCDate(dateSixDaysBack.getUTCDate()-6);
         let formattedString = DateUtil.getDayOfTheWeek(dateSixDaysBack) + " 2h 6m";
         let input = "{{LOCAL_SHORT_DATE" + ':' + dateSixDaysBack.getTime()/1000 + '}} 2h 6m';
         const output = DateUtil.localizeDateTime(input);
         expect(output).toEqual(formattedString);
     });

     it('localizeDateTime input has LOCAL_SHORT_DATE, should return date based on locale format because date is not within past 6 days', () =>
     {
         let dateSevenDaysBack = new Date();
         dateSevenDaysBack.setDate(dateSevenDaysBack.getDate()-7);
         let formattedString = moment(dateSevenDaysBack).format('ll') + " 2h 6m";
         let input = "{{LOCAL_SHORT_DATE" + ':' + dateSevenDaysBack.getTime()/1000 + '}} 2h 6m';
         expect(DateUtil.localizeDateTime(input)).toEqual(formattedString);
     });

     it('localizeDateTime input has LOCAL_SHORT_DATE, should return date based on locale format because date is in future', () =>
     {
         let tomorrow = new Date();
         tomorrow.setDate(tomorrow.getDate()+1);
         let formattedString =  moment(tomorrow).format('ll') + " 2h 6m";
         let input = "{{LOCAL_SHORT_DATE" + ':' + tomorrow.getTime()/1000 + '}} 2h 6m';
         expect(DateUtil.localizeDateTime(input)).toEqual(formattedString);
     });

    it('convertIsoToTimeFromEpoch has empty input, should return a zero', () =>
    {
        let input: string = '';
        expect(DateUtil.convertIsoToTimeFromEpoch(input)).toEqual(input.length);
    });

     it("convertIsoToTimeFromEpoch has VALID input of Today's Date, should return milliseconds", () =>
     {
         let input: string = new Date().toString();
          let millitime: string = input.replace(DateUtil.ISO_TIME_SEARCH_VALUE, DateUtil.ISO_TIME_REPLACE_VALUE);
          expect(DateUtil.convertIsoToTimeFromEpoch(input)).toEqual(Date.parse(millitime));
     });

     it("convertDateToISO8601Format1 has VALID input of Today's Date, should return date and time as an ISO date string", () =>
     {
         let date = new Date('December 21, 2018 6:00:00 AM GMT-00:00');
         expect(DateUtil.convertDateToISO8601Format1(date)).toEqual('2018-12-21T06:00:00.000Z');
     });

     it('convertDateToISO8601Format1() adds Z on end', () =>
     {
         const date = new Date(1587108836234);
         expect(DateUtil.convertDateToISO8601Format1(date)).toEqual('2020-04-17T07:33:56.234Z');
     });

     it('convertDateToISO8601Format2() adds +0000 on end', () =>
     {
         const date = new Date(1587108836234);
         expect(DateUtil.convertDateToISO8601Format2(date)).toEqual('2020-04-17T07:33:56.234+0000');
     });

     it('convertDateToISO8601Format2() outputs in UTC equivalent', () =>
     {
         const date = new Date('Friday, November 17, 1998 4:33:56 PM GMT-06:00');
         expect(DateUtil.convertDateToISO8601Format2(date)).toEqual('1998-11-17T22:33:56.000+0000');
     });
 });
