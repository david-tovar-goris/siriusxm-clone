import {
    parseNavigationInfo,
    parseQueryParams,
    getCurrentUrlQueryParams,
    getQueryParameterValue,
    secondsToMs,
    msToSeconds,
    convertToBoolOrString,
    generateUUID,
    isSafari,
    inProduction,
    inCompanyBeta,
    inBeta,
    inQA,
    inDev,
    relativeUrlToAbsoluteUrl,
    translateRelativeUrlsForTheList, isSafari11
} from './utilities';

import { IRelativeUrlSetting } from "../config/interfaces/settings-item.interface";

describe("Utilities - Parse URL and Query Params",function()
{
    it("parseNavigationInfo should return empty object when empty url is provided as input", function()
    {
        const urlToParse = '';
        expect(parseNavigationInfo(urlToParse)).toEqual({path: '', queryParams: {}});
    });

    it('parseNavigationInfo to return an object with path and queryParams', function()
    {
        const urlToParse = "http://example.com/foo;key1=value1?key2=value2&key3=1234";
        expect(parseNavigationInfo(urlToParse)).toEqual({ path: "http://example.com/foo;key1=value1",
                                                                    queryParams: {key2: 'value2', key3: '1234'}});
    });

    it("parseQueryParams should return empty object when a url with no query params is provided as input", function()
    {
        const urlToParse = 'http://example.com/foo;key1=value1';
        expect(parseQueryParams(urlToParse)).toEqual({});
    });
});

describe('Utilities - get URL and Query Params', function()
{
    it("getCurrentUrlQueryParams should fetch and return query parameters from window.location.href", function()
    {
        const href = window.location.href;
        expect(getCurrentUrlQueryParams()).toEqual(parseQueryParams(href));
    });

    it("getQueryParameterValue should return an empty string if the requested query param does not exist", function()
    {
        expect(getQueryParameterValue('name')).toEqual('');
    });

    it("getQueryParameterValue should return value of query parameter if present for the provided query parameter name input", function()
    {
        expect(getQueryParameterValue( "sourceid")).toEqual('');
    });
});

describe("Utilities - seconds to milli-seconds conversion and vice-versa",function()
{
    it("secondsToMs should return 0 when we provide 0 as input", function()
    {
        const secs = 0;
        expect(secondsToMs(secs)).toEqual(secs);
    });

    it("secondsToMs should return should return 25000 when  25 is provided as input", function()
    {
        const secs = Math.random() * 100;
        expect(secondsToMs(secs)).toEqual(Math.floor(secs * 1000));
    });

    it("msToSeconds should return 0 when we provide 0 as input", function()
    {
        const ms = 0;
        expect(msToSeconds(ms)).toEqual(ms);
    });

    it("msToSeconds should return a output with decimals for provided input when we don't provide round parameter", function()
    {
        const ms = Math.random() * 100000;
        expect(msToSeconds(ms)).toEqual(ms / 1000);
    });

    it("msToSeconds should return output with no decimals for provided input when we provide round parameter as 'true'", function()
    {
        const ms = Math.random() * 100000;
        expect(msToSeconds(ms, true)).toEqual((Math.floor(ms / 1000)));
    });
});

describe("Utilities - convert to Boolean Or String",function()
{
    it("convertToBoolOrString should return a boolean when we provide valid bool string as input", function()
    {
        const boolStr: string = 'false';
        expect(convertToBoolOrString(boolStr)).toEqual(false);
    });

    it("convertToBoolOrString should return a boolean when we provide valid bool string as input", function()
    {
        const boolStr: string = 'True';
        expect(convertToBoolOrString(boolStr)).toEqual(true);
    });

    it("convertToBoolOrString should return provided string input when the string is not a boolean value", function()
    {
        const boolStr: string = 'xyz';
        expect(convertToBoolOrString(boolStr)).toEqual(boolStr);
    });
});

describe("Utilities - generate UUID",function()
{
    it("generateUUID should return proper UUID based on timestamp", function()
    {
        expect(generateUUID().length).toEqual(36);
        expect(generateUUID().charAt(14)).toEqual('4');
        expect(generateUUID()).not.toEqual(generateUUID());
    });
});

describe("Utilities - is browser Safari",function()
{
    const deviceInfo: any = {
        browser: 'safari',
        browserVersion: '11.09.32'
    };

    it("isSafari should return true if device browser is Safari", function()
    {
        expect(isSafari(deviceInfo)).toBeTruthy();
    });

    it("isSafari11 should return true if Safari browser version is 11 or higher", function()
    {
        expect(isSafari11(deviceInfo)).toBeTruthy();
    });
});

describe("Utilities - Application running environment",function()
{
    it("inProduction should return false if application running environment is not production", function()
    {
        const endpoint: string = 'https://local-dev.siriusxm.com/';
        expect(inProduction(endpoint)).toBeFalsy();
    });

    it("inProduction should return true if application running environment is production", function()
    {
        const endpoint: string = 'https://player.siriusxm.com/';
        expect(inProduction(endpoint)).toBeTruthy();
    });

    it("inCompanyBeta should return false if application running environment is not Company Beta", function()
    {
        const endpoint: string = 'https://player.siriusxm.com/';
        expect(inCompanyBeta(endpoint)).toBeFalsy();
    });

    it("inCompanyBeta should return true if application running environment is Company Beta", function()
    {
        const endpoint: string = 'https://companybeta.siriusxm.com/';
        expect(inCompanyBeta(endpoint)).toBeTruthy();
    });

    it("inBeta should return false if application running environment is not Beta", function()
    {
        const endpoint: string = 'https://companybeta.siriusxm.com/';
        expect(inBeta(endpoint)).toBeFalsy();
    });

    it("inBeta should return true if application running environment is Beta", function()
    {
        const endpoint: string = 'https://beta.siriusxm.com/';
        expect(inBeta(endpoint)).toBeTruthy();
    });

    it("inQA should return false if application running environment is not QA", function()
    {
        const endpoint: string = 'https://companybeta.siriusxm.com/';
        expect(inQA(endpoint)).toBeFalsy();
    });

    it("inQA should return true if application running environment is QA", function()
    {
        const endpoint: string = 'https://k2qa.siriusxm.com/';
        expect(inQA(endpoint)).toBeTruthy();
    });

    it("inDev should return true if application running environment is Dev", function()
    {
        const endpoint: string = 'https://local-dev.siriusxm.com/';
        expect(inDev(endpoint)).toBeTruthy();
    });
});

describe("Utilities - translate Relative urls",function()
{
    const relativeUrls: Array<IRelativeUrlSetting> = [
        {
            name: 'howard-stern-aod',
            url: 'howardstern2020/audio-on-demand'
        }, {
            name: 'howard-stern-vod',
            url: 'howardstern2020/video-on-demand'
        }, {
            name: 'fox-news',
            url: 'news/foxnews/america'
        }];

    it("relativeUrlToAbsoluteUrl should return relative URL tag replaced with actual value from the map if found", function()
    {
        const target: string = 'http://example.com/foo/%howard-stern-aod%/';
        expect(relativeUrlToAbsoluteUrl(target, relativeUrls)).toEqual('http://example.com/foo/howardstern2020/audio-on-demand/');
    });

    it("relativeUrlToAbsoluteUrl should return relative URL tag replaced with actual value from the map if found, else original target value is returned", function()
    {
        const target: string = 'http://example.com/foo/%cnn-news%/glue';
        expect(relativeUrlToAbsoluteUrl(target, relativeUrls)).toEqual('http://example.com/foo/%cnn-news%/glue');
    });

    it("relativeUrlToAbsoluteUrl should only replace single instance of relative url in the target", function()
    {
        const target: string = 'http://example.com/foo/%howard-stern-vod%/boo/%cnn-news%/glue/%howard-stern-vod%';
        expect(relativeUrlToAbsoluteUrl(target, relativeUrls)).toEqual('http://example.com/foo/howardstern2020/video-on-demand/boo/%cnn-news%/glue/%howard-stern-vod%');
    });

    it("For media content (HLS or mp4), we must get the content from an https endpoint", function()
    {
        const target: string = 'http://example.com/foo/%howard-stern-vod%/ts/1_6_1280_720_2000_main.m3u8';
        expect(relativeUrlToAbsoluteUrl(target, relativeUrls)).toEqual('https://example.com/foo/howardstern2020/video-on-demand/ts/1_6_1280_720_2000_main.m3u8');
    });

    it("translateRelativeUrlsForTheList should return absolute url's if found, for all url's in a list ", function()
    {
        const targetList = [
            {
                relativeUrl: 'http://example.com/%fox-news%/foo',
                urlFromApi: '',
                url: ''
            },
            {
                relativeUrl: 'http://example.com/%howard-stern-vod%/boo',
                urlFromApi: '',
                url: ''
            },
            {
                relativeUrl: 'http://example.com/%howard-stern-aod%/foo/%howard-stern-vod%',
                urlFromApi: '',
                url: ''
            }
        ];

        const translatedUrls = translateRelativeUrlsForTheList(targetList, relativeUrls);
        expect(translatedUrls.length).toEqual(targetList.length);
        expect(translatedUrls[0].url).toEqual('http://example.com/news/foxnews/america/foo');
        expect(translatedUrls[1].url).toEqual('http://example.com/howardstern2020/video-on-demand/boo');
        expect(translatedUrls[2].url).toEqual('http://example.com/howardstern2020/audio-on-demand/foo/%howard-stern-vod%');
    });
});


