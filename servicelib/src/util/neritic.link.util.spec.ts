import { parseNeriticLink } from "./neritic.link.util";

describe("Neritic link type APP test suite", function()
 {
    it('Parsing proper APP neritic link should return parsed object', function()
    {
        const appNeriticLink: string = 'App:carousel:video_landing';
        const parsedLink = parseNeriticLink(appNeriticLink);
        expect(parsedLink.linkType).toEqual('App');
        expect(parsedLink.actionType).toEqual('carousel');
        expect(parsedLink.contentType).toEqual('video_landing');
    });

    it('Parsing neritic link with no ":" should return null ', function()
    {
        const apiNeriticLink: string = 'App';
        const parsedLink = parseNeriticLink(apiNeriticLink);
        expect(parsedLink).toEqual(null);
    });

     it('Parsing neritic link with action type Archive should return content-type "channel"', function()
     {
         const appNeriticLink: string = 'App:Archive';
         const parsedLink = parseNeriticLink(appNeriticLink);
         expect(parsedLink.linkType).toEqual('App');
         expect(parsedLink.actionType).toEqual('Archive');
         expect(parsedLink.contentType).toEqual('channel');
     });

     it('Parsing neritic link with action type ViewVODEpisodes should return content-type "show"', function()
     {
         const appNeriticLink: string = 'App:ViewVODEpisodes';
         const parsedLink = parseNeriticLink(appNeriticLink);
         expect(parsedLink.linkType).toEqual('App');
         expect(parsedLink.actionType).toEqual('ViewVODEpisodes');
         expect(parsedLink.contentType).toEqual('show');
     });

     it('Parsing neritic link with action type ArchiveViewEpisodes should return content-type "show"', function()
     {
         const appNeriticLink: string = 'App:ArchiveViewEpisodes';
         const parsedLink = parseNeriticLink(appNeriticLink);
         expect(parsedLink.linkType).toEqual('App');
         expect(parsedLink.actionType).toEqual('ArchiveViewEpisodes');
         expect(parsedLink.contentType).toEqual('show');
     });

     it('Parsing neritic link with action type ViewAODEpisodes should return content-type "show"', function()
     {
         const appNeriticLink: string = 'App:ViewAODEpisodes';
         const parsedLink = parseNeriticLink(appNeriticLink);
         expect(parsedLink.linkType).toEqual('App');
         expect(parsedLink.actionType).toEqual('ViewAODEpisodes');
         expect(parsedLink.contentType).toEqual('show');
     });

     it('Parsing neritic link with link  should return url', function()
     {
         const appNeriticLink: string = 'App:carousel:video_landing:test.com';
         const parsedLink = parseNeriticLink(appNeriticLink);
         expect(parsedLink.url).toEqual('video_landing:test.com');
         expect(parsedLink.contentType).toEqual('video_landing');
     });
 });

describe("Neritic link type API test suite", function()
 {
    it('Parsing proper API neritic link should return parsed object', function()
    {
        const apiNeriticLink: string = 'Api:tune:liveAudio:siriushits1';
        const parsedLink = parseNeriticLink(apiNeriticLink);
        expect(parsedLink.linkType).toEqual('Api');
        expect(parsedLink.actionType).toEqual('tune');
        expect(parsedLink.contentType).toEqual('liveAudio');
        expect(parsedLink.channelId).toEqual('siriushits1');
    });

    it('Parsing neritic link with no ":" should return null ', function()
    {
        const apiNeriticLink: string = 'Api';
        const parsedLink = parseNeriticLink(apiNeriticLink);
        expect(parsedLink).toEqual(null);
    });

     it('Parsing neritic link which neither App nor Api should return null ', function()
     {
         const apiNeriticLink: string = 'test:tune:vod:howardstern100:f6983819-5a97-420b-aa0a-c293f3d134aa::af7721ad-abb1-4e83-e3c6-3dc6046ee648';
         const parsedLink = parseNeriticLink(apiNeriticLink);
         expect(parsedLink).toEqual(null);
     });

    it('Parsing neritic link with "::" should return startTime as zero if date is invalid/undefined ', function()
    {
        const appNeriticLink: string = "Api:tune:vod:howardstern100:f6983819-5a97-420b-aa0a-c293f3d134aa::af7721ad-abb1-4e83-e3c6-3dc6046ee648";
        const parsedLink = parseNeriticLink(appNeriticLink);
        expect(parsedLink.contentType).toEqual('vod');
        expect(parsedLink.channelId).toEqual('howardstern100');
        expect(parsedLink.startTime).toEqual(0);
    });

     it('Parsing neritic link with "::" should return startTime as zero if date is invalid/undefined ', function()
     {
         const appNeriticLink: string = "Api:tune:vod:howardstern100:f6983819-5a97-420b-aa0a-c293f3d134aa::1576267727747";
         const parsedLink = parseNeriticLink(appNeriticLink);
         expect(parsedLink.contentType).toEqual('vod');
         expect(parsedLink.channelId).toEqual('howardstern100');
         expect(parsedLink.startTime).toBeTruthy();
     });
 });


