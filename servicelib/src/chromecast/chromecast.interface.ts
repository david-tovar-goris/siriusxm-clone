import { DeviceInfo } from "../config/device.info";

export interface ChromecastTransferSessionReceiverData
{
    // The token transfer url.
    transferSessionUrl: string;

    // The api endpoint that the sender is using.
    apiEnvironment: string;

    // The deviceInfo object used in API calls like resume.
    deviceInfo: DeviceInfo;

    // NEW for Everest: The current playback type for the sender: "live", "aod"; "vod".
    playbackType: string;

    // NEW for Everest: GUP ID of the session the sender is using.
    gupId: string;

    // Channel ID of the content the receiver is to play.
    channelId: string;

    // Asset GUID of the content the receiver is to play.
    assetGuid: string;

    mediaType: string;
}

export interface IChromecastData
{
    deviceName: string;
    castStatusMessage: string; // This Value is localized key
}

export interface IMetaData
{
    channelId: string;
    assetGuid: string;
    mediaType: string;
    sequencerId?: string;
    stationId?: string;
    tracks? : any;
    tuneResponse? : any;
    skipLimitReached? : boolean;
}
