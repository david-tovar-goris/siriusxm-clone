import { ChromecastUtil } from "./chromecast.util";
import * as Helper from "../index";
import { ApiLayerTypes } from "../service/consts";
import { DeviceInfo, IAppConfig } from "../index";

describe("ChromecastUtil Test Suite", function()
{
    const mockChromeCastUtilUS = {
        domainName : "domainName",
        deviceInfo : { appRegion: "US" } as DeviceInfo
    } as IAppConfig;

    const mockChromeCastUtilCA = {
        domainName : "domainName",
        deviceInfo : { appRegion: "CA" } as DeviceInfo
    } as IAppConfig;

    describe("isValidUrl", function()
    {
        it("should return false when no http and no https", function()
        {
            expect(ChromecastUtil.isValidUrl('www.siriusxm.com')).toBe(false);
        });

        it("should return true when http", function()
        {
            expect(ChromecastUtil.isValidUrl('http://www.siriusxm.com')).toBe(true);
        });

        it("should return true when https", function()
        {
            expect(ChromecastUtil.isValidUrl('https://www.siriusxm.com')).toBe(true);
        });
    });

    describe("getAppId", function()
    {
        describe("Url has chromecastappid ", function()
        {
            let spyGetQueryParam;
            beforeEach(function()
            {
                spyGetQueryParam = spyOn(Helper, "getQueryParameterValue").and.returnValue("935FCC60");
            });

            it("should call getQueryParameterValue with chromecastAppId as parameter value", function()
            {
                ChromecastUtil.getAppId();
                expect(spyGetQueryParam).toHaveBeenCalled();
                expect(spyGetQueryParam).toHaveBeenCalledWith("chromecastAppId");
            });

            it("should return chromecastappid as string ", function()
            {
                const appId = ChromecastUtil.getAppId();
                expect(appId).toEqual("935FCC60");
            });
        });

        describe("Url doesnot have chromecastappid ", function()
        {
            let spyGetQueryParam;
            beforeEach(function()
            {
                spyGetQueryParam = spyOn(Helper, "getQueryParameterValue").and.returnValue("");
            });

            it("should return '4156CAC9' as appId with parameter value empty string", function()
            {
                const appId = ChromecastUtil.getAppId("");
                expect(appId).toEqual("4156CAC9");
            });

            it("should return '936D2C8C' as appId with parameter value as us region-ca", function()
            {
                const appId = ChromecastUtil.getAppId(ApiLayerTypes.REGION_CA);
                expect(appId).toEqual("936D2C8C");
            });

            it("should return '4156CAC9' as appId with parameter value as us region-us", function()
            {
                const appId = ChromecastUtil.getAppId(ApiLayerTypes.REGION_US);
                expect(appId).toEqual("4156CAC9");
            });
        });
    });

    describe("getApiEnv", function()
    {
        let spyGetQueryParam;
        it("should return config domainname when api paramvalue is empty", function()
        {
            spyGetQueryParam = spyOn(Helper, "getQueryParameterValue").and.returnValue("");
            const domainName = ChromecastUtil.getApiEnv(mockChromeCastUtilUS);
            expect(domainName).toEqual(mockChromeCastUtilUS.domainName);
        });

        it("should return api string when api doesnot contain local-dev", function()
        {
            const apiString = "siriusxm.com";
            spyGetQueryParam = spyOn(Helper, "getQueryParameterValue").and.returnValue(apiString);
            const domainName = ChromecastUtil.getApiEnv(mockChromeCastUtilUS);
            expect(domainName).toEqual(apiString);
        });

        it("should return related domain string when api contain local-dev and region not as CA",
            function()
            {
                spyGetQueryParam = spyOn(Helper, "getQueryParameterValue").and.returnValue(
                    "local-dev.siriusxm.com");
                expect(ChromecastUtil.getApiEnv(mockChromeCastUtilUS)).toEqual(
                    "api-k2qa4.mountain.siriusxm.com");
            });

        it("should return related domain string when api contain local-dev and region as CA",
            function()
            {
                spyGetQueryParam = spyOn(Helper, "getQueryParameterValue").and.returnValue(
                    "local-dev.siriusxm.com");
                expect(ChromecastUtil.getApiEnv(mockChromeCastUtilCA)).toEqual(
                    "api-k2qa4.mountain.siriusxm.ca");
            });
    });
});
