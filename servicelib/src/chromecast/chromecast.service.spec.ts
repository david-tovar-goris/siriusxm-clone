import { of as observableOf } from 'rxjs';

import { ChromecastService } from "./chromecast.service";
import { IAppConfig } from "../config/interfaces/app-config.interface";
import {
    ApiDelegate,
    AuthenticationService,
    AppMonitorService, ChromecastPlayerConsts, IHttpResponse, ChromecastModel
} from "../index";

import { SessionTransferService } from "../sessiontransfer/session.transfer.service";
import { ChromecastPlayerService } from "../mediaplayer/chromecastplayer/chromecast-player.service";
import { ChromecastMessageBus } from "./chromecast.message-bus";
import { ResumeService } from "../resume/resume.service";
import { CurrentlyPlayingService } from "../currently-playing/currently.playing.service";
import { TuneService } from "../tune/tune.service";
import { InitializationService } from "../initialization/initialization.service";
import { ChromecastConst } from "./chromecast.const";
import { mock } from "ts-mockito";

import { MockAppConfig }    from "../test/mocks/app.config.mock";
import { AuthenticationServiceMock }      from "../test/mocks/authetication.service.mock";
import { currentlyPlayingServiceMock } from "../test/mocks/currently-playing.service.mock";
import  * as ChromecastConstMock  from "../test/mocks/chromecast/chromecast.const.mock";
import { chromeCastModelMock } from "../test/mocks/chromecast/chromecast.model.mock";

import {
    aodTuneResponseMock,
    liveTuneResponseMock,
    vodTuneResponseMock
} from "../test/index";


interface ThisContext {
    chromecastService: ChromecastService;
    chromecastServiceChrome:ChromecastService;
    appConfig: IAppConfig;
    chromecastPlayerServiceMock: ChromecastPlayerService;
    chromecastMessageBus: ChromecastMessageBus;
    appMonitorServiceMock: AppMonitorService;
    apiDelegateMock:ApiDelegate;
    authenticationServiceMock:AuthenticationService;
    sessionTransferServiceMock:SessionTransferService;
    chromecastMessageBusMock:any;
    currentlyPlayingServiceMock:CurrentlyPlayingService;
    resumeServiceMock:ResumeService;
    tuneServiceMock:TuneService;
    initializationServiceMock:InitializationService;
}


describe("ChormeCast Service Test Suite: ", function(this: ThisContext)
{

    beforeEach(function(this: ThisContext)
    {
        this.appConfig = new MockAppConfig();
        this.chromecastPlayerServiceMock = mock(ChromecastPlayerService);
        this.appMonitorServiceMock = mock(AppMonitorService);

        this.authenticationServiceMock = <AuthenticationService><unknown>new AuthenticationServiceMock();
        this.currentlyPlayingServiceMock = <CurrentlyPlayingService>new currentlyPlayingServiceMock();

        ChromecastConst.CF = ChromecastConstMock.CF;
        ChromecastConst.CC = ChromecastConstMock.CC;

        this.apiDelegateMock = {
            addApiCodeHandler: function (SESSION_IN_CASTING_MODE,handler)
            {
                let res:IHttpResponse;
                handler([],[],res);
            }
        } as ApiDelegate;

        this.sessionTransferServiceMock =  {
            castStatusChange: (status)=>{return observableOf("");},
            "transferSessionToken": observableOf("urltoken"),
            transferSession:()=>{return null;}
        } as SessionTransferService;

        this.chromecastMessageBusMock = {
            message$:observableOf([{type:ChromecastPlayerConsts.CHROMECAST_RECEIVER_EVENT.TRANSFER_SESSION_COMPLETE}]),
            sendMessage:(eve:any)=>{return eve;}
        };

        const aODResponse  = JSON.parse(JSON.stringify(aodTuneResponseMock));
        const resumeMediaMock = {
            data           : aODResponse,
            updateFrequency: 10,
            type           : "live"
        };

        // @ts-ignore
        this.resumeServiceMock = {
            profileData:{
                pipe: ()=> observableOf(""),
                resumeMedia: observableOf(resumeMediaMock)
            }
        } as ResumeService;

        this.tuneServiceMock = {
            tune:(payload)=> {return observableOf("");},
            isPlayingContentSupported:(val, playerType)=>
            {
                return true;
            },
            getMediaType: ()=>
            {
                return "Media_Type";
            }
        } as unknown as TuneService;

        this.initializationServiceMock = {
            initState:{
                pipe:()=>observableOf("running")
            }
        } as InitializationService;

    });

    describe("",function(this: ThisContext)
    {
        beforeEach(function (this: ThisContext)
        {
            this.chromecastService = new ChromecastService( this.appConfig,
                                                            this.apiDelegateMock,
                                                            chromeCastModelMock,
                                                            this.sessionTransferServiceMock,
                                                            this.authenticationServiceMock,
                                                            this.chromecastPlayerServiceMock,
                                                            this.chromecastMessageBusMock,
                                                            this.resumeServiceMock,
                                                            this.currentlyPlayingServiceMock,
                                                            this.tuneServiceMock,
                                                            this.initializationServiceMock,
                                                            this.appMonitorServiceMock);
        });

        describe("Chrome cast Connection ", function (this: ThisContext)
        {
            it("should be false chrome cast not connected", function (this: ThisContext)
            {
                let result: any = this.chromecastService.isChromeCastConnected();
                expect(result).toBe(false);
            });

            it("should be true chrome cast connection established", function (this: ThisContext)
            {
                let spy = spyOn(this.chromecastService, "createChromecastContext").and.callThrough();
                this.chromecastService.createChromecastContext();
                let result: any = this.chromecastService.isChromeCastConnected();
                expect(result).toBe(true);
            });
        });

        describe("Remote Player and Controller ", function (this: ThisContext)
        {
            it("should create player instance", function (this: ThisContext)
            {
                let spy = spyOn(this.chromecastService, "createRemotePlayerAndController").and.callThrough();
                this.chromecastService.createRemotePlayerAndController();
                expect(spy).toHaveBeenCalledTimes(1);
            });

            it("should be false player is not paused ", function (this: ThisContext)
            {
                let result = this.chromecastService.isPaused();
                expect(result).toBe(false);
            });

            it("Should be Stopped Audio and Casting", function (this: ThisContext)
            {
                let spy = spyOn(this.chromecastService, "stopAudioAndCasting").and.callThrough();
                this.chromecastService.stopAudioAndCasting();
                expect(spy).toHaveBeenCalled();
            });

            it("should be success for data transmission to chromecast", function (this: ThisContext)
            {
                let spy = spyOn(this.chromecastService, "updateAffinity").and.callThrough();
                this.chromecastService.updateAffinity(3);
                expect(spy).toHaveBeenCalledWith(3);
            });

            it("getting cast context", function (this: ThisContext)
            {
                let result = this.chromecastService.castContext;
                expect(result).toBeNull();
            });

        });
    });

    describe("Casting Not In Progress: ", function(this: ThisContext)
    {
        let chromecastServiceNoCasting:ChromecastService;

        chromeCastModelMock.remotePlayer = false;

        beforeEach(function(this: ThisContext)
        {
            this.tuneServiceMock = {
                tune:(payload)=> {return observableOf("");},
                isPlayingContentSupported:(val, playerType)=>
                {
                    return false;
                },
                getMediaType: ()=>
                {
                    return "Media_Type";
                }
            } as unknown as TuneService;
              chromecastServiceNoCasting = new ChromecastService(   this.appConfig,
                                                                    this.apiDelegateMock,
                                                                    chromeCastModelMock,
                                                                    this.sessionTransferServiceMock,
                                                                    this.authenticationServiceMock,
                                                                    this.chromecastPlayerServiceMock,
                                                                    this.chromecastMessageBusMock,
                                                                    this.resumeServiceMock,
                                                                    this.currentlyPlayingServiceMock,
                                                                    this.tuneServiceMock,
                                                                    this.initializationServiceMock,
                                                                    this.appMonitorServiceMock);
        });

        it("should create a new chromecast session",function()
        {
            expect(chromecastServiceNoCasting).toBeDefined();
        });

        it("should establish new chrome cast connection", function()
        {
            chromecastServiceNoCasting.createChromecastContext();
            let connectionStatus = chromecastServiceNoCasting.isChromeCastConnected();
            expect(connectionStatus).toBeTruthy();
        });
    });

    describe("ChromeCast for chrome Browser", function(this: ThisContext)
    {
        beforeEach(function(this: ThisContext)
        {

            this.appConfig.deviceInfo.isChromeBrowser = true;
            this.appConfig.deviceInfo.appRegion = "US";

            this.chromecastServiceChrome = new ChromecastService(   this.appConfig,
                                                                    this.apiDelegateMock,
                                                                    chromeCastModelMock,
                                                                    this.sessionTransferServiceMock,
                                                                    this.authenticationServiceMock,
                                                                    this.chromecastPlayerServiceMock,
                                                                    this.chromecastMessageBusMock,
                                                                    this.resumeServiceMock,
                                                                    this.currentlyPlayingServiceMock,
                                                                    this.tuneServiceMock,
                                                                    this.initializationServiceMock,
                                                                    this.appMonitorServiceMock);
        });

        it("Should be chromecast established for chrome browser", function(this: ThisContext)
        {
           expect(this.chromecastServiceChrome).toBeDefined();
        });
    });
});
