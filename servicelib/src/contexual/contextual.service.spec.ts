/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />


import {throwError as observableThrowError,  Observable, of as observableOf } from 'rxjs';
import * as _ from "lodash";
import { mock } from "ts-mockito";
import { ContextualService } from "./contextual.service";
import { ContextualDelegate } from "./contextual.delegate";
import { IAppConfig } from "../config/interfaces/app-config.interface";
import { DeepLinkType } from "./contextual.interface";
import { ContextualUtilities } from "./contextual.utilities";
import { ContextualConstants } from "./contextual.constants";

describe("Contextual service test suite", function()
{
    const deepLinkId: string         = "deepLinkId";
    const deepLinkType: DeepLinkType = "aod";
    const contextualDefaultData      = {
        imageUrl       : "",
        title          : "landing.defaultTitle",
        title2          : "landing.defaultTitle2",
        subTitle       : "",
        description    : "landing.defaultDescription",
        originalAirDate: "",
        expirationDate : "",
        buttonText     : "landing.listenNow",
        isSpecial      : false,
        deepLinkType   : 'live'
    };

    beforeEach(function()
    {
        this.contextualDelegate    = mock(ContextualDelegate);
        this.appConfig        = null;
        this.appConfig   = {
            deviceInfo    : {
                appRegion: "appRegion"
            },
            contextualInfo: {
                userAgent  : "useAgent",
                queryString: "queryString",
                host       : "host",
                hostName   : "hostName",
                protocol   : "protocol"
            }
        } as IAppConfig;
        this.testSubject = new ContextualService(this.contextualDelegate,this.appConfig);
    });

    afterEach(function()
    {
        this.testSubject = null;
        this.appConfig   = null;
    });

    describe("Infrastructure >>", function()
    {
        it("Should have test subject", function()
        {
            expect(this.testSubject).toBeDefined();
        });

        it("Constructor", function()
        {
            expect(this.testSubject).toBeDefined();
            expect(this.testSubject instanceof ContextualService).toEqual(true);
        });

    });

    describe("getContextualPDT() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(_.isFunction(this.testSubject.getContextualPDT)).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the response", function()
            {
                spyOn(this.contextualDelegate, "getContextualPDT").and
                                                             .callFake((deepLinkId, deepLinkTyoe) =>
                                                             {
                                                                 expect(deepLinkId).toEqual(deepLinkId);
                                                                 expect(deepLinkType).toEqual(deepLinkType);

                                                                 return observableOf(contextualDefaultData);
                                                             });

                this.appConfig.contextualInfo.type = "aod";
                this.appConfig.contextualInfo.id   = "335919";
                this.testSubject.getContextualPDT();
                this.testSubject.contextualData.subscribe((data) =>
                {
                    expect(data).toEqual(contextualDefaultData);
                });
            });

            it("Should return the if deep link not exists", function()
            {
                this.appConfig.contextualInfo.type = "";
                this.appConfig.contextualInfo.id   = "";
                this.testSubject.getContextualPDT();
                this.testSubject.contextualData.subscribe((data) =>
                {
                    expect(data).toEqual(null);
                });

            });
        });

        describe("Exception handling >>>", function()
        {
            it("Should log an error and return empty object when delegate throws exception.", function()
            {
                spyOn(this.contextualDelegate, "getContextualPDT").and
                                                             .callFake((deepLinkId, deepLinkTyoe) =>
                                                             {
                                                                 expect(deepLinkId).toEqual(deepLinkId);
                                                                 expect(deepLinkType).toEqual(deepLinkType);

                                                                 return observableThrowError("error");
                                                             });

                this.appConfig.contextualInfo.type = "aod";
                this.appConfig.contextualInfo.id   = "335919";
                this.testSubject.getContextualPDT();
                this.testSubject.contextualData.subscribe((data) =>
                {
                    expect(data).toEqual(null);
                });
            });
        });
    });

    describe("initializeContextualLanding() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(_.isFunction(this.testSubject.initializeContextualLanding)).toBeTruthy();
            });

            it("Should return an observable.", function()
            {
                expect(this.testSubject.initializeContextualLanding() instanceof Observable).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the response - when contentType & channelName empty", function()
            {
                const appInfo       = {
                    appImageSrc      : "appImageSrc",
                    gtmId            : "gtmId",
                    accessibiltyLabel: "accessibiltyLabel"
                };
                const iosVersion    = 9;
                const universalLink = "universalLink";

                spyOn(ContextualUtilities, "getQueryParameterValue").and.returnValue("");
                spyOn(ContextualUtilities, "getIOSVersion").and.returnValue(iosVersion);
                spyOn(ContextualUtilities, "getUniversalLink").and.returnValue(universalLink);
                spyOn(ContextualUtilities, "getContextualAppInfo").and.returnValue(appInfo);

                this.testSubject.initializeContextualLanding().subscribe((data) =>
                {
                    const expected = {
                        contentType             : "",
                        channelName             : "",
                        iosVersion              : iosVersion,
                        universalLink           : universalLink,
                        storeUrl                : "",
                        appDeepLink             : this.appConfig.contextualInfo.protocol + "//" + this.appConfig.contextualInfo.host + "/" ,
                        operatingSystem         : ContextualConstants.UNKNOWN,
                        contextualLandingAppInfo: appInfo,
                        contextualData          : null,
                        actionButtonText        : 'landing.downloadApp',
                        actionButtonUrl         : "",
                        isIOS                   : false
                    };

                    expect(data).toEqual(expected);
                });
            });

            it("Should return the response - when contentType & channelName not empty", function()
            {
                const appInfo        = {
                    appImageSrc      : "appImageSrc",
                    gtmId            : "gtmId",
                    accessibiltyLabel: "accessibiltyLabel"
                };
                const iosVersion     = 9;
                const universalLink  = "universalLink";
                const contextualData = {
                    imageUrl       : "",
                    title          : "",
                    subTitle       : "",
                    description    : "",
                    originalAirDate: "",
                    expirationDate : "",
                    buttonText     : ""
                };
                const storeUrl       = "storeUrl";
                const value          = "true";
                spyOn(this.contextualDelegate, "getContextualPDT").and
                                                             .callFake((deepLinkId, deepLinkTyoe) =>
                                                             {
                                                                 return observableOf(contextualData);
                                                             });

                spyOn(ContextualUtilities, "getQueryParameterValue").and.returnValue(value);
                spyOn(ContextualUtilities, "getStoreUrl").and.returnValue(storeUrl);
                spyOn(ContextualUtilities, "getIOSVersion").and.returnValue(iosVersion);
                spyOn(ContextualUtilities, "getUniversalLink").and.returnValue(universalLink);
                spyOn(ContextualUtilities, "getContextualAppInfo").and.returnValue(appInfo);

                this.testSubject.initializeContextualLanding().subscribe((data) =>
                {
                    const expected = {
                        contentType             : value,
                        channelName             : value,
                        iosVersion              : iosVersion,
                        universalLink           : universalLink,
                        storeUrl                : storeUrl,
                        appDeepLink             :
                        this.appConfig.contextualInfo.protocol + "//" +
                        this.appConfig.contextualInfo.host + "/" +  "?type=" + value + "&id=" + value,
                        operatingSystem         : ContextualConstants.UNKNOWN,
                        contextualLandingAppInfo: appInfo,
                        contextualData          : contextualData,
                        actionButtonText        : 'landing.downloadApp',
                        actionButtonUrl         : "",
                        isIOS                   : false
                    };

                    expect(data).toEqual(expected);
                });
            });

            it("Should return the response - when contentType & channelName and expired deepLink", function()
            {
                const appInfo       = {
                    appImageSrc      : "appImageSrc",
                    gtmId            : "gtmId",
                    accessibiltyLabel: "accessibiltyLabel"
                };
                const iosVersion    = 9;
                const universalLink = "universalLink";
                const storeUrl      = "storeUrl";
                const value         = "true";
                spyOn(this.contextualDelegate, "getContextualPDT").and
                                                             .callFake((deepLinkId, deepLinkTyoe) =>
                                                             {
                                                                 return observableOf(null);
                                                             });

                spyOn(ContextualUtilities, "getQueryParameterValue").and.returnValue(value);
                spyOn(ContextualUtilities, "getStoreUrl").and.returnValue(storeUrl);
                spyOn(ContextualUtilities, "getIOSVersion").and.returnValue(iosVersion);
                spyOn(ContextualUtilities, "getUniversalLink").and.returnValue(universalLink);
                spyOn(ContextualUtilities, "getContextualAppInfo").and.returnValue(appInfo);

                this.testSubject.initializeContextualLanding().subscribe((data) =>
                {
                    const expected = {
                        contentType             : value,
                        channelName             : value,
                        iosVersion              : iosVersion,
                        universalLink           : universalLink,
                        storeUrl                : storeUrl,
                        appDeepLink             :
                            this.appConfig.contextualInfo.protocol + "//" +
                            this.appConfig.contextualInfo.host + "/" + "?type=" + value + "&id=" + value,
                        operatingSystem         : ContextualConstants.UNKNOWN,
                        contextualLandingAppInfo: appInfo,
                        contextualData          : null,
                        actionButtonText: 'landing.downloadApp',
                        actionButtonUrl         : "",
                        isIOS                   : false
                    };
                    expect(data).toEqual(expected);
                });
            });
        });

        describe("Exception handling >>>", function()
        {
            it("Should log an error and return default contextualdata when delegate throws exception.", function()
            {
                const appInfo       = {
                    appImageSrc      : "appImageSrc",
                    gtmId            : "gtmId",
                    accessibiltyLabel: "accessibiltyLabel"
                };
                const iosVersion    = 9;
                const universalLink = "universalLink";
                const storeUrl      = "storeUrl";
                const value         = "true";
                spyOn(ContextualUtilities, "getQueryParameterValue").and.returnValue(value);
                spyOn(ContextualUtilities, "getStoreUrl").and.returnValue(storeUrl);
                spyOn(ContextualUtilities, "getIOSVersion").and.returnValue(iosVersion);
                spyOn(ContextualUtilities, "getUniversalLink").and.returnValue(universalLink);
                spyOn(ContextualUtilities, "getContextualAppInfo").and.returnValue(appInfo);

                spyOn(this.contextualDelegate, "getContextualPDT").and
                                                             .callFake((deepLinkId, deepLinkTyoe) =>
                                                             {
                                                                 expect(deepLinkId).toEqual(deepLinkId);
                                                                 expect(deepLinkType).toEqual(deepLinkType);

                                                                 return observableThrowError({});
                                                             });

                this.testSubject.initializeContextualLanding().subscribe((data) =>
                {
                    const expected = {
                        contentType             : value,
                        channelName             : value,
                        iosVersion              : iosVersion,
                        universalLink           : universalLink,
                        storeUrl                : storeUrl,
                        appDeepLink             :
                            this.appConfig.contextualInfo.protocol + "//" +
                            this.appConfig.contextualInfo.host + "/" +  "?type=" + value + "&id=" + value,
                        operatingSystem         : ContextualConstants.UNKNOWN,
                        contextualLandingAppInfo: appInfo,
                        contextualData          : null,
                        actionButtonText        : 'landing.downloadApp',
                        actionButtonUrl         : "",
                        isIOS                   : false
                    };

                    expect(data).toEqual(expected);
                });
            });
        });
    });

    describe("getDeepLinkUrl() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(_.isFunction(this.testSubject.getDeepLinkUrl)).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the deep link URL - ios Version is less than 9", function()
            {
                spyOn(ContextualUtilities, "getQueryParameterValue").and.returnValue("value");
                spyOn(ContextualUtilities, "getIOSVersion").and.returnValue(8);
                spyOn(ContextualUtilities, "getDeviceType").and.returnValue(ContextualConstants.MOBILE);

                expect(this.testSubject.getDeepLinkUrl()).toEqual("SIRIUSXM://player.siriusxm.com/value/value");
            });

            it("Should return the deep link URL - ios Version is >= 9", function()
            {
                spyOn(ContextualUtilities, "getQueryParameterValue").and.returnValue("value");
                spyOn(ContextualUtilities, "getIOSVersion").and.returnValue(9);
                spyOn(ContextualUtilities, "getDeviceType").and.returnValue(ContextualConstants.MOBILE);

                expect(this.testSubject.getDeepLinkUrl()).toEqual("");
            });

            it("Should return the deep link URL - desktop when channelName and contentType is empty", function()
            {
                spyOn(ContextualUtilities, "getQueryParameterValue").and.returnValue("");
                spyOn(ContextualUtilities, "getIOSVersion").and.returnValue(9);
                spyOn(ContextualUtilities, "getDeviceType").and.returnValue(ContextualConstants.DESKTOP);

                expect(this.testSubject.getDeepLinkUrl())
                    .toEqual(this.appConfig.contextualInfo.protocol + "//" + this.appConfig.contextualInfo.host + "/");
            });

            it("Should return the deep link URL - desktop when channelName and contentType is not empty", function()
            {
                const value = "value";

                spyOn(ContextualUtilities, "getQueryParameterValue").and.returnValue(value);
                spyOn(ContextualUtilities, "getIOSVersion").and.returnValue(9);
                spyOn(ContextualUtilities, "getDeviceType").and.returnValue(ContextualConstants.DESKTOP);

                expect(this.testSubject.getDeepLinkUrl())
                    .toEqual(this.appConfig.contextualInfo.protocol + "//" + this.appConfig.contextualInfo.host +
                        "/" +"?type=" + value + "&id=" + value);
            });

            it("Should return the deep link URL - desktop when channelName and contentType is not empty and with UTM Medium", function()
            {
                const value               = "value";
                const cleanedSearchString = "?utm_source=sampleutmsource&utm_campaign=samplecampaign&utm_medium=samplemedium";

                spyOn(ContextualUtilities, "getQueryParameterValue").and.returnValue(value);
                spyOn(ContextualUtilities, "getCleanedSearchString").and.returnValue(cleanedSearchString);
                spyOn(ContextualUtilities, "getIOSVersion").and.returnValue(9);
                spyOn(ContextualUtilities, "getDeviceType").and.returnValue(ContextualConstants.DESKTOP);

                expect(this.testSubject.getDeepLinkUrl())
                    .toEqual(this.appConfig.contextualInfo.protocol + "//" +
                        this.appConfig.contextualInfo.host + "/" + cleanedSearchString + "&type=" + value + "&id=" + value);
            });
        });
    });
});
