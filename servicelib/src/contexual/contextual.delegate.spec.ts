/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import * as _ from "lodash";
import { Observable, of as observableOf } from "rxjs";
import { mock } from "ts-mockito";
import { HttpProvider } from "../http/http.provider";

import { ContextualDelegate } from "./contextual.delegate";
import { DeepLinkType } from "./contextual.interface";
import { NAME_COLOR_CHANNEL_LOGO, NAME_LIST_VIEW_SHOW_LOGO } from "../service/types/image.interface";
import { DateUtil } from "../util/date.util";

describe("Contextual delegate test suite", function()
{
    const deepLinkId: string                = "deepLinkId";
    const deepLinkLiveType: DeepLinkType    = "live";
    const deepLinkAodType: DeepLinkType     = "aod";
    const deepLinkChannelType: DeepLinkType = "channel";

    beforeEach(function()
    {
        this.http = mock(HttpProvider);
        this.emptyData       = {
            imageUrl       : "",
            title          : "",
            subTitle       : "",
            description    : "",
            originalAirDate: "",
            expirationDate : "",
            buttonText     : "",
            isSpecial      : false,
            deepLinkType   : 'live'
        };
        this.liveResponse    = {
            moduleDetails: {
                liveChannelResponse:
                    {
                        liveChannelResponses: [ {} ]
                    }
            }
        };
        this.channelResponse = {
            contentData: {
                channelListing:
                    {
                        channels: [ {} ]
                    }
            }
        };
        this.aodResponse     = {
            contentData: {}
        };
        this.testSubject     = new ContextualDelegate( this.http);

    });

    afterEach(function()
    {
        this.testSubject     = null;
        this.liveResponse    = null;
        this.channelResponse = null;
        this.aodResponse     = null;
        this.emptyData       = null;
    });

    describe("Infrastructure >>", function()
    {
        it("Should have test subject", function()
        {
            expect(this.testSubject).toBeDefined();
        });

        it("Constructor", function()
        {
            expect(this.testSubject).toBeDefined();
            expect(this.testSubject instanceof ContextualDelegate).toEqual(true);
        });

    });

    describe("getContextualPDT() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.testSubject).toBeDefined();
                expect(_.isFunction(this.testSubject.getContextualPDT)).toBeTruthy();
            });

            it("Should return an observable.", function()
            {
                this.http.get =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               return observableOf({});
                           });

                expect(this.testSubject.getContextualPDT(deepLinkId, deepLinkLiveType) instanceof Observable).toBeTruthy();
            });

        });

        describe("Execution >>>", function()
        {
            it("When API returned empty response object for live .", function()
            {
                this.http.get =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               return observableOf(this.liveResponse);
                           });

                this.testSubject.getContextualPDT(deepLinkId, deepLinkLiveType).subscribe(data =>
                {
                    this.emptyData.buttonText = "landing.listenNow";
                    expect(data).toEqual(this.emptyData);
                });
            });

            it("When API returned response object for live .", function()
            {
                const cutTitle      = "cutTitle";
                const cutArtistName = "cutArtistName";
                const showLongTitle = "showLongTitle";
                this.http.get            =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               this.liveResponse.moduleDetails.liveChannelResponse.liveChannelResponses = [ {
                                   markerLists: [
                                       {
                                           markers: [ {
                                               cut: {
                                                   title  : cutTitle,
                                                   artists: [ {
                                                       name: cutArtistName
                                                   }
                                                   ]
                                               }
                                           }
                                           ]
                                       },
                                       {
                                           markers: [ {
                                               episode: {
                                                   show: {
                                                       longTitle: showLongTitle
                                                   }
                                               }
                                           }
                                           ]
                                       }
                                   ]
                               }
                               ];
                               return observableOf(this.liveResponse);
                           });

                this.testSubject.getContextualPDT(deepLinkId, deepLinkLiveType).subscribe(data =>
                {
                    this.emptyData.buttonText = "landing.listenNow";
                    this.emptyData.title      = cutArtistName + " - " + cutTitle;
                    this.emptyData.subTitle   = showLongTitle;
                    this.emptyData.deepLinkType = 'live';
                    expect(data).toEqual(this.emptyData);
                });
            });

            it("When API returned empty response object for channel .", function()
            {
                this.http.get =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               return observableOf(this.channelResponse);
                           });

                this.testSubject.getContextualPDT(deepLinkId, deepLinkChannelType).subscribe(data =>
                {
                    this.emptyData.buttonText = "landing.viewNow";
                    this.emptyData.deepLinkType = "channel";
                    expect(data).toEqual(this.emptyData);
                });
            });

            it("When API returned response object for channel .", function()
            {
                const channelName      = "channelName";
                const shortDescription = "shortDescription";
                const imageUrl         = "imageUrl";
                const width            = 400;
                const height           = 320;

                this.http.get               =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               this.channelResponse.contentData.channelListing.channels = [ {
                                   name            : channelName,
                                   shortDescription: shortDescription,
                                   images          : {
                                       images: [ {
                                           width : 270,
                                           height: 270,
                                           name  : NAME_COLOR_CHANNEL_LOGO,
                                           url   : imageUrl,
                                           relativeUrl   : imageUrl
                                       },
                                           {
                                               width : width,
                                               height: height,
                                               name  : NAME_COLOR_CHANNEL_LOGO,
                                               url   : imageUrl,
                                               relativeUrl   : imageUrl
                                           }
                                       ]
                                   }
                               }
                               ];

                               return observableOf(this.channelResponse);
                           });

                this.testSubject.getContextualPDT(deepLinkId, deepLinkChannelType).subscribe(data =>
                {
                    this.emptyData.buttonText  = "landing.viewNow";
                    this.emptyData.title       = channelName;
                    this.emptyData.description = shortDescription;
                    this.emptyData.imageUrl    = imageUrl +"?width=" + width + "&height=" + height + "preserveAspect=true";
                    this.emptyData.deepLinkType= 'channel';
                    expect(data).toEqual(this.emptyData);
                });
            });

            it("When API returned empty response object for AOD .", function()
            {
                this.http.get =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               return observableOf(this.aodResponse);
                           });

                this.testSubject.getContextualPDT(deepLinkId, deepLinkAodType).subscribe(data =>
                {
                    this.emptyData.buttonText = "landing.listenNow";
                    this.emptyData.deepLinkType = 'aod';
                    expect(data).toEqual(this.emptyData);
                });
            });

            it("When API returned response object for AOD .", function()
            {
                const episodeLongTitle        = "episodeLongTitle";
                const episodeMediumTitle      = "episodeMediumTitle";
                const showLongTitle           = "showLongTitle";
                const showMediumTitle         = "showMediumTitle";
                const longDescription         = "longDescription";
                const shortDescription        = "shortDescription";
                const imageUrl                = "imageUrl";
                const convertedTime           = "2018-01-06T22:00:00.000Z";
                const width                   = 150;
                const height                  = 150;

                this.http.get =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               this.aodResponse.contentData = {
                                   aodEpisodes: [ {
                                       episodes: [ {
                                           longTitle       : episodeLongTitle,
                                           mediumTitle     : episodeMediumTitle,
                                           longDescription : longDescription,
                                           shortDescription: shortDescription,
                                           originalAirDate : "2018-01-06T22:00:00.000+0000",
                                           publicationInfo : {
                                               expiryDate: "2018-01-06T22:00:00.000+0000"
                                           },
                                           special         : true
                                       }
                                       ]
                                   }
                                   ],
                                   aodShows   : [ {
                                       shows: [ {
                                           showDescription: {
                                               longTitle   : showLongTitle,
                                               mediumTitle : showMediumTitle,
                                               creativeArts: [ {
                                                   width : width,
                                                   height: height,
                                                   name  : NAME_LIST_VIEW_SHOW_LOGO,
                                                   url   : imageUrl,
                                                   relativeUrl   : imageUrl
                                               }
                                               ]
                                           }
                                       }
                                       ]
                                   }
                                   ]
                               };
                               return observableOf(this.aodResponse);
                           });

                this.testSubject.getContextualPDT(deepLinkId, deepLinkAodType).subscribe(data =>
                {
                    this.emptyData.buttonText      = "landing.listenNow";
                    this.emptyData.imageUrl        = imageUrl +"?width=" + width + "&height=" + height + "preserveAspect=true";
                    this.emptyData.title           = episodeLongTitle;
                    this.emptyData.subTitle        = showLongTitle;
                    this.emptyData.description     = longDescription;
                    this.emptyData.expirationDate  = convertedTime;
                    this.emptyData.originalAirDate = convertedTime;
                    this.emptyData.isSpecial       = true;
                    this.emptyData.deepLinkType    = 'aod';
                    expect(data).toEqual(this.emptyData);
                });
            });
        });

        describe("Exception handling >>>", function()
        {
            it("Should Return empty object - Live .", function()
            {
                this.http.get =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               return observableOf({});
                           });

                this.testSubject.getContextualPDT(deepLinkId, deepLinkLiveType).subscribe(data =>
                {
                    expect(data).toEqual(this.emptyData);
                });
            });

            it("Should Return empty object - AOD .", function()
            {
                this.emptyData.deepLinkType = 'aod';
                this.http.get =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               return observableOf({});
                           });

                this.testSubject.getContextualPDT(deepLinkId, deepLinkAodType).subscribe(data =>
                {
                    expect(data).toEqual(this.emptyData);
                });
            });

            it("Should Return empty object - Channel .", function()
            {
                this.emptyData.deepLinkType = 'channel';
                this.http.get =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               return observableOf({});
                           });

                this.testSubject.getContextualPDT(deepLinkId, deepLinkChannelType).subscribe(data =>
                {
                    expect(data).toEqual(this.emptyData);
                });
            });

            it("Should Throw error when the deep link type is unkown .", function()
            {
                this.http.get =
                    jasmine.createSpy("get")
                           .and
                           .callFake((url, request, config) =>
                           {
                               return observableOf({});
                           });

                this.testSubject.getContextualPDT(deepLinkId, "" as DeepLinkType).subscribe(null, error =>
                {
                    expect(error).toEqual("Unknown deepLink Type");
                });
            });
        });
    });
});
