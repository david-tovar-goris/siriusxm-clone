/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import * as _ from "lodash";
import { ContextualUtilities } from "./contextual.utilities";
import { ContextualConstants } from "./contextual.constants";
import { ContextualUrlConstants } from "../service/consts/contextual.url.constants";

describe("Contextual Utilities test suite", function()
{
    const userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0_2 like Mac OS X) " +
        "AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12A366 Safari/600.1.4";

    describe("getStoreUrl() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined.", function()
            {
                expect(_.isFunction(ContextualUtilities.getStoreUrl)).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the store url - Android & US", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.ANDROID);

                expect(ContextualUtilities.getStoreUrl("", "US")).toEqual(ContextualUrlConstants.urls.ANDROID.US);
            });

            it("Should return the store url - Android & CA", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.ANDROID);

                expect(ContextualUtilities.getStoreUrl("", "CA")).toEqual(ContextualUrlConstants.urls.ANDROID.CA);
            });

            it("Should return the store url - IOS & US and IOS version is greater than 9", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.IOS);

                expect(ContextualUtilities.getStoreUrl("", "US")).toEqual(ContextualUrlConstants.urls.IOS.US);
            });

            it("Should return the store url - WINDOWS & US", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.WINDOWS);

                expect(ContextualUtilities.getStoreUrl("", "US")).toEqual(ContextualUrlConstants.urls.WINDOWS.US);
            });

            it("Should return the store url - WINDOWS & CA", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.WINDOWS);

                expect(ContextualUtilities.getStoreUrl("", "CA")).toEqual(ContextualUrlConstants.urls.WINDOWS.CA);
            });

            it("Should return the store url - UNKOWN", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.UNKNOWN);

                expect(ContextualUtilities.getStoreUrl("", "")).toEqual("");
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the store url - Android & US", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.ANDROID);

                expect(ContextualUtilities.getActionButtonUrl("", "US", "", false)).toEqual(ContextualUrlConstants.urls.ANDROID.US);
            });

            it("Should return the store url - Android & CA", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.ANDROID);

                expect(ContextualUtilities.getActionButtonUrl("", "CA", "", false)).toEqual(ContextualUrlConstants.urls.ANDROID.CA);
            });

            it("Should return the store url - IOS & US and IOS version is greater than 9", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.IOS);

                expect(ContextualUtilities.getActionButtonUrl("", "US", "", false)).toEqual(ContextualConstants.SIGN_UP_URL_USA);
            });

            it("Should return the store url - IOS & US and IOS version is greater than 9 with queryString", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.IOS);

                const queryString = "contentType=live&channelName=Howard100&DTOK=123456&utm_source=sampleSource&" +
                    "utm_medium=samplemedium&utm_campaign=sampleCampain";
                const expectedValue = ContextualConstants.SIGN_UP_URL_USA + "?" +
                    "DTOK=123456&utm_source=sampleSource&utm_medium=samplemedium&utm_campaign=sampleCampain";
                expect(ContextualUtilities.getActionButtonUrl("", "US", queryString, false)).toEqual(expectedValue);
            });

            it("Should return the store url - IOS & US and IOS version is greater than 9 with inavlid values in queryString ", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.IOS);

                const queryString = "contentType=live&channelName=Howard100&DTOK={123456}&utm_source=sampleSource&" +
                    "utm_medium=samplemedium&utm_campaign=sampleCampain";
                const expectedValue = ContextualConstants.SIGN_UP_URL_USA + "?" +
                    "utm_source=sampleSource&utm_medium=samplemedium&utm_campaign=sampleCampain";
                expect(ContextualUtilities.getActionButtonUrl("", "US", queryString, true)).toEqual(expectedValue);
            });

            it("Should return the Signup url - IOS & CA and IOS version is greater than 9", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.IOS);

                expect(ContextualUtilities.getActionButtonUrl("", "CA", "", false)).toEqual(ContextualConstants.SIGN_UP_URL_CA_ENGLISH);
            });

            it("Should return the Signup French Url- IOS & CA and IOS version is greater than 9", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.IOS);

                expect(ContextualUtilities.getActionButtonUrl("", "CA", "", true)).toEqual(ContextualConstants.SIGN_UP_URL_CA_FRENCH);
            });

            it("Should return the Signup url - IOS & CA and IOS version is greater than 9 with queryString", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.IOS);

                const queryString = "contentType=live&channelName=Howard100&DTOK=123456&utm_source=sampleSource&" +
                    "utm_medium=samplemedium&utm_campaign=sampleCampain";
                expect(ContextualUtilities.getActionButtonUrl("", "CA", queryString, false)).toEqual(ContextualConstants.SIGN_UP_URL_CA_ENGLISH);
            });

            it("Should return the store url - WINDOWS & US", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.WINDOWS);

                expect(ContextualUtilities.getActionButtonUrl("", "US", "", false)).toEqual(ContextualUrlConstants.urls.WINDOWS.US);
            });

            it("Should return the store url - WINDOWS & CA", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.WINDOWS);

                expect(ContextualUtilities.getActionButtonUrl("", "CA", "", false)).toEqual(ContextualUrlConstants.urls.WINDOWS.CA);
            });

            it("Should return the store url - UNKOWN", function()
            {
                spyOn(ContextualUtilities, "getOperatingSystem").and.returnValue(ContextualConstants.UNKNOWN);

                expect(ContextualUtilities.getActionButtonUrl("", "", "", false)).toEqual("");
            });
        });
    });

    describe("getUniversalLink() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined.", function()
            {
                expect(_.isFunction(ContextualUtilities.getUniversalLink)).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the universal link for local dev player ", function()
            {
                expect(ContextualUtilities.getUniversalLink("local-dev.siriusxm.com",
                        "https",
                        "contentType=live&channelName=ESPNRadio&goToStore=true",
                        "live",
                        "ESPNRadio"))
                    .toEqual("https//local-landing.siriusxm.com/deepLink.html/live/ESPNRadio?goToStore=true&");
            });

            it("Should return the universal link for local dev landing ", function()
            {
                expect(ContextualUtilities.getUniversalLink("local-landing.siriusxm.com",
                    "https",
                    "contentType=live&channelName=ESPNRadio&goToStore=true",
                    "live",
                    "ESPNRadio"))
                    .toEqual("https//local-dev.siriusxm.com/deepLink.html/live/ESPNRadio?goToStore=true&");
            });

            it("Should return the universal link for primary player ", function()
            {
                expect(ContextualUtilities.getUniversalLink("player.siriusxm.com",
                    "https",
                    "contentType=live&channelName=ESPNRadio&goToStore=true",
                    "live",
                    "ESPNRadio"))
                    .toEqual("https//deeplink.siriusxm.com/live/ESPNRadio?goToStore=true&");
            });

            it("Should return the universal link for primary landing ", function()
            {
                expect(ContextualUtilities.getUniversalLink("deeplink.siriusxm.com",
                    "https",
                    "contentType=live&channelName=ESPNRadio&goToStore=true",
                    "live",
                    "ESPNRadio"))
                    .toEqual("https//player.siriusxm.com/live/ESPNRadio?goToStore=true&");
            });

            it("Should return the universal link for beta player ", function()
            {
                expect(ContextualUtilities.getUniversalLink("playerbeta.siriusxm.com",
                    "https",
                    "contentType=live&channelName=ESPNRadio&goToStore=true",
                    "live",
                    "ESPNRadio"))
                    .toEqual("https//deeplinkbeta.mountain.siriusxm.com/live/ESPNRadio?goToStore=true&");
            });

            it("Should return the universal link for beta landing ", function()
            {
                expect(ContextualUtilities.getUniversalLink("deeplinkbeta.mountain.siriusxm.com",
                    "https",
                    "contentType=live&channelName=ESPNRadio&goToStore=true",
                    "live",
                    "ESPNRadio"))
                    .toEqual("https//playerbeta.siriusxm.com/live/ESPNRadio?goToStore=true&");
            });

            it("Should return the universal link for beta landing without any params ", function()
            {
                expect(ContextualUtilities.getUniversalLink("deeplinkbeta.mountain.siriusxm.com",
                    "https",
                    "contentType1=live&channelName=ESPNRadio",
                    "",
                    ""))
                    .toEqual("https//playerbeta.siriusxm.com/?contentType1=live&goToStore=true");
            });

            it("Should return the universal link for default case - find player and replaces to deeplink", function()
            {
                expect(ContextualUtilities.getUniversalLink("player.mountain.siriusxm",
                    "https",
                    "contentType1=live&channelName=ESPNRadio",
                    "",
                    ""))
                    .toEqual("https//deeplink.mountain.siriusxm/?contentType1=live&goToStore=true");
            });

            it("Should return the universal link for default case - find deeplink and replaces to player", function()
            {
                expect(ContextualUtilities.getUniversalLink("deeplink.mountain.siriusxm",
                    "https",
                    "contentType1=live&channelName=ESPNRadio",
                    "",
                    ""))
                    .toEqual("https//player.mountain.siriusxm/?contentType1=live&goToStore=true");
            });

            it("Should return the universal link for default case - url have player and not mountain", function()
            {
                expect(ContextualUtilities.getUniversalLink("deeplink.player.siriusxm",
                    "https",
                    "contentType1=live&channelName=ESPNRadio",
                    "",
                    ""))
                    .toEqual("https//deeplink.mountain.player/?contentType1=live&goToStore=true");
            });

            it("Should return the universal link for default case - url have player and not mountain", function()
            {
                expect(ContextualUtilities.getUniversalLink("siriusxm.com",
                    "https",
                    "contentType1=live&channelName=ESPNRadio",
                    "",
                    ""))
                    .toEqual("https//siriusxm.com/?contentType1=live&goToStore=true");
            });
        });
    });

    describe("getCleanedSearchString() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined.", function()
            {
                expect(_.isFunction(ContextualUtilities.getCleanedSearchString)).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the search string if new parameters other than contentType/channelName/goToStore ", function()
            {
                expect(ContextualUtilities
                    .getCleanedSearchString("contentType1=live&channelName1=ESPNRadio&goToStore=true"))
                    .toEqual("?contentType1=live&channelName1=ESPNRadio");
            });

            it("Should return empty if querystring have contentType/channelName/goToStore", function()
            {
                expect(ContextualUtilities
                    .getCleanedSearchString("contentType=live&channelName=ESPNRadio&goToStore=true")).toEqual("");
            });

            it("Should return empty if querystring have utm medium values", function()
            {
                const mockQueryString = "contentType=live&channelName=Howard100&DTOK=123456&utm_source=sampleSource&" +
                                         "utm_medium=samplemedium&utm_campaign=sampleCampain";
                const expectedValue = "?DTOK=123456&utm_source=sampleSource&utm_medium=samplemedium&utm_campaign=sampleCampain";
                expect(ContextualUtilities.getCleanedSearchString(
                    mockQueryString,
                    true)).toEqual(expectedValue);
            });

            it("Should return empty if querystring have utm medium values with some invalid params", function()
            {
                const mockQueryString = "contentType=live&channelName=Howard100&DTOK=123456&utm_source={sampleSourc}e&" +
                    "utm_medium=samplemedium&utm_campaign=sampleCampain";
                const expectedValue = "?DTOK=123456&utm_medium=samplemedium&utm_campaign=sampleCampain";
                expect(ContextualUtilities.getCleanedSearchString(
                    mockQueryString,
                    true)).toEqual(expectedValue);
            });
        });
    });

    describe("getOperatingSystem() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined.", function()
            {
                expect(_.isFunction(ContextualUtilities.getOperatingSystem)).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the IOS if userAgent have iphone/ipad/ipod", function()
            {
                expect(ContextualUtilities
                    .getOperatingSystem(userAgent)).toEqual(ContextualConstants.IOS);
            });

            it("Should return WINDOWS if userAgent have Windows", function()
            {
                expect(ContextualUtilities
                    .getOperatingSystem("Windows")).toEqual(ContextualConstants.WINDOWS);
            });

            it("Should return ANDROID when useragent have Android", function()
            {
                expect(ContextualUtilities
                    .getOperatingSystem("ANDROID")).toEqual(ContextualConstants.ANDROID);
            });

            it("Should return UNKNOWN ", function()
            {
                expect(ContextualUtilities
                    .getOperatingSystem("Test")).toEqual(ContextualConstants.UNKNOWN);
            });

        });
    });

    describe("getDeviceType() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined.", function()
            {
                expect(_.isFunction(ContextualUtilities.getDeviceType)).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the MOBILE if userAgent have iphone/ipad/ipod", function()
            {
                expect(ContextualUtilities
                    .getDeviceType(userAgent)).toEqual(ContextualConstants.MOBILE);
            });

            it("Should return DESKTOP if userAgent not have iphone/ipad/ipod/FBAV/FBAN/Android", function()
            {
                expect(ContextualUtilities
                    .getDeviceType("Mozilla/5.0")).toEqual(ContextualConstants.DESKTOP);
            });

            it("Should return FB_BROWSER_ANDRIOD when useragent have (FBAV/FBAN) and Android", function()
            {
                expect(ContextualUtilities
                    .getDeviceType("FBAV-Android")).toEqual(ContextualConstants.FB_BROWSER_ANDRIOD);
            });
        });
    });

    describe("getIOSVersion() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined.", function()
            {
                expect(_.isFunction(ContextualUtilities.getIOSVersion)).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the version number", function()
            {
                expect(ContextualUtilities
                    .getIOSVersion(userAgent)).toEqual(8);
            });

            it("Should return null if userAgent not have iphone/ipad/ipod", function()
            {
                expect(ContextualUtilities
                    .getIOSVersion("Mozilla/5.0")).toEqual(null);
            });

            it("Should return null if userAgent have iphone/ipad/ipod and not having version", function()
            {
                expect(ContextualUtilities
                    .getIOSVersion("iphone")).toEqual(null);
            });
        });
    });

    describe("getQueryParameterValue() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined.", function()
            {
                expect(_.isFunction(ContextualUtilities.getQueryParameterValue)).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the value for contentType", function()
            {
                expect(ContextualUtilities
                    .getQueryParameterValue("contentType=aod&channelName=335919", "contentType")).toEqual("aod");
            });

            it("Should return the value for channelName", function()
            {
                expect(ContextualUtilities
                    .getQueryParameterValue("contentType=aod&channelName=335919", "channelName")).toEqual("335919");
            });

            it("Should return empty value if key not found", function()
            {
                expect(ContextualUtilities
                    .getQueryParameterValue("contentType=aod&channelName=335919", "test")).toEqual("");
            });
        });
    });

    describe("getContextualAppInfo() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined.", function()
            {
                expect(_.isFunction(ContextualUtilities.getContextualAppInfo)).toBeTruthy();
            });
        });

        describe("Execution >>>", function()
        {
            it("Should return the contextual App info - Android", function()
            {
                expect(ContextualUtilities.getContextualAppInfo(ContextualConstants.ANDROID))
                    .toEqual({
                        appImageSrc      : "../assets/images/playstore-app-icon.svg",
                        gtmId            : "downloadApp-Android",
                        accessibiltyLabel: "landing.accessibility.googlePlayStore"
                    });
            });


            it("Should return the contextual App info - IOS", function()
            {
                expect(ContextualUtilities.getContextualAppInfo(ContextualConstants.IOS))
                    .toEqual({
                        appImageSrc      : "../assets/images/appstore-app-icon.svg",
                        gtmId            : "downloadApp-iOS",
                        accessibiltyLabel: "landing.accessibility.appleStore"
                    });
            });

            it("Should return the contextual App info -  WINDOWS", function()
            {
                expect(ContextualUtilities.getContextualAppInfo(ContextualConstants.WINDOWS))
                    .toEqual({
                        appImageSrc      : "../assets/images/win-10-app-icon.svg",
                        gtmId            : "downloadApp-Windows",
                        accessibiltyLabel: "landing.accessibility.windowsStore"
                    });
            });

            it("Should return the empty contextual App info -  UNKOWN", function()
            {
                expect(ContextualUtilities.getContextualAppInfo(ContextualConstants.UNKNOWN))
                    .toEqual({});
            });
        });
    });
});
