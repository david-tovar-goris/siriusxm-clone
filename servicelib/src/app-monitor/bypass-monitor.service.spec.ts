/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import {
    ApiDelegate,
    EventBus,
    BypassMonitorService
}                         from "../index";
import { ServiceFactory } from "../service/service.factory";

interface ThisContext
{
    apiDelegate : any;
    service : BypassMonitorService;
}

import { reduce } from 'rxjs/operators';

/**
 * @todo
 */
describe('BypassMonitorService >>', () =>
{
    beforeEach(function(this:ThisContext)
    {
        this.apiDelegate = new ApiDelegate(new EventBus);

        spyOn(this.apiDelegate, 'addApiCodeHandler');

        this.service = new BypassMonitorService(this.apiDelegate);
    });

    describe('Infrastructure >>', function(this:ThisContext)
    {
        it("Should be able to create the service and all its dependencies",function(this:ThisContext)
        {
            expect(ServiceFactory.getInstance(BypassMonitorService)).toBeDefined();
        });

        it('emits when a bypass error happens', function(this:ThisContext)
        {
            this.service.bypassErrors.subscribe(code => { expect(code).toBe(702); });
            this.service.handleByPassState(702);
        });

        it('emits multiple when a bypass error happens', function(this:ThisContext)
        {
            let answers = [702,700,704];
            let count = 0;

            this.service.bypassErrors.subscribe(code =>
            {
                expect(code).toBe(answers[count]);
                count++;
            });

            this.service.handleByPassState(702);
            this.service.handleByPassState(700);
            //this one won't emit
            this.service.handleByPassState(702);
            this.service.handleByPassState(704);
        });


        it('emits bypassErrorStates that change correctly based on api calls and noop calls',function(this:ThisContext)
        {

            let list = [];

            this.service.bypassErrorState.pipe(reduce( (acc, value, index) =>
            {
                acc.push(value);
                return acc;
            }, list)).subscribe(states =>
            {
                const beforeNoop = states[3];
                const afterfirstNoop = states[4];
                const end = states[6];

                expect(beforeNoop.AOD_BYPASS).toBe(true);
                expect(beforeNoop.GUP_BYPASS).toBe(true);
                expect(beforeNoop.GUP_BYPASS2).toBe(true);
                expect(beforeNoop.SEARCH_BYPASS).toBe(false);
                expect(beforeNoop.IT_BYPASS).toBe(false);
                expect(beforeNoop.VOD_BYPASS).toBe(false);

                expect(afterfirstNoop.AOD_BYPASS).toBe(true);
                expect(afterfirstNoop.GUP_BYPASS).toBe(true);
                expect(afterfirstNoop.GUP_BYPASS2).toBe(false);
                expect(afterfirstNoop.SEARCH_BYPASS).toBe(false);
                expect(afterfirstNoop.IT_BYPASS).toBe(false);
                expect(afterfirstNoop.VOD_BYPASS).toBe(false);

                expect(end.AOD_BYPASS).toBe(false);
                expect(end.GUP_BYPASS).toBe(false);
                expect(end.GUP_BYPASS2).toBe(false);
                expect(end.SEARCH_BYPASS).toBe(false);
                expect(end.IT_BYPASS).toBe(false);
                expect(end.VOD_BYPASS).toBe(false);
            });

            this.service.handleByPassState(702);
            this.service.handleByPassState(700);
            this.service.handleByPassState(704);
            this.service.handleNoopMessage([700, 100, 702]);
            this.service.handleByPassState(700);
            this.service.handleNoopMessage([100]);
            this.service.bypassErrorState.complete();
        });

    });

});
