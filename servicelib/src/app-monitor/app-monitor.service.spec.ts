/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { Observable, EMPTY }                     from "rxjs";
import {
    ApiDelegate,
    EventBus,
    IAppConfig,
    SessionMonitorService
}                                         from "../index";
import { AppMonitorService} from "./app-monitor.service";
import { ServiceFactory }                 from "../service/service.factory";

interface ThisContext
{
    apiDelegate : any;
    sessionMonitorService : any;
    appConfig : IAppConfig;
    service : AppMonitorService;
    bypassMonitorService : any;
}

/**
 * @todo
 */
describe('AppMonitorService >>', () =>
{
    beforeEach(function(this:ThisContext)
    {
        this.appConfig = {
            deviceInfo    : {
                appRegion: "appRegion"
            },
            inPrivateBrowsingMode: null,
            contextualInfo: {
                userAgent  : "useAgent",
                queryString: "queryString",
                host       : "host",
                hostName   : "hostName",
                protocol   : "protocol",
                id         : "id",
                type       : "type"
            }
        } as IAppConfig;

        this.apiDelegate = new ApiDelegate(new EventBus);
        this.bypassMonitorService = { bypassErrors: EMPTY };
        this.sessionMonitorService = new SessionMonitorService(this.apiDelegate, this.appConfig);
        this.service = new AppMonitorService(this.sessionMonitorService,
                                             this.bypassMonitorService,
                                             this.apiDelegate,
                                             this.appConfig);
    });

    describe('Infrastructure >>', function(this:ThisContext)
    {
        it("Should be able to create the service and all its dependencies",function(this:ThisContext)
        {
            expect(ServiceFactory.getInstance(AppMonitorService)).toBeDefined();
        });

        it('should exist as a service',function(this:ThisContext)
        {
            expect(this.service).toBeTruthy();
        });
    });
});
