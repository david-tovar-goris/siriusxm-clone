import { ExploreService } from "./explore.service";
import { VideoPlayerServiceMock } from "../test/mocks/mediaplayer/video/video-player.service.mock";
import { kochavaAnalyticsConstants } from "./kochava-analytics.constants";

describe('Explore Service', function()
{
    let videoPlayerServiceMock: any = new VideoPlayerServiceMock();
    let kochavaAnalyticsServiceMock: any = {
        recordPageView: () => {},
        recordAction: () => {}
    };

    let exploreService = new ExploreService(videoPlayerServiceMock, kochavaAnalyticsServiceMock);

    it("should instantiate the service", () =>
    {
        expect(exploreService).toBeDefined();
    });

    it("can load the FT teaser", () =>
    {
        spyOn(videoPlayerServiceMock, "stop");
        spyOn(exploreService, "onTeaseReelClose");
        exploreService.initiateTeaser(null);
        expect(videoPlayerServiceMock.stop).toHaveBeenCalled();
        expect(exploreService.onTeaseReelClose).toHaveBeenCalled();
    });

    it("can close the tease reel", () =>
    {
        spyOn(kochavaAnalyticsServiceMock, "recordAction");
        spyOn(videoPlayerServiceMock, "pause");
        spyOn(videoPlayerServiceMock, "stop");

        exploreService.onTeaseReelClose();

        expect(kochavaAnalyticsServiceMock.recordAction).toHaveBeenCalledWith(
            kochavaAnalyticsConstants.SCREEN_NAMES.REEL,
            kochavaAnalyticsConstants.FLOWS.EXPLORE,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.CLOSE,
            kochavaAnalyticsConstants.EVENT_NAMES.EXPLORE_REEL_CLOSE
        );

        expect(videoPlayerServiceMock.pause).toHaveBeenCalled();
        expect(videoPlayerServiceMock.stop).toHaveBeenCalled();
    });

    it("can load the video asset", () =>
    {
        spyOn(kochavaAnalyticsServiceMock, "recordPageView");
        spyOn(videoPlayerServiceMock, "loadAsset");

        exploreService.initiateTeaser("http://fake.url");

        expect(kochavaAnalyticsServiceMock.recordPageView).toHaveBeenCalledWith(
            kochavaAnalyticsConstants.SCREEN_NAMES.REEL,
            kochavaAnalyticsConstants.FLOWS.EXPLORE,
            kochavaAnalyticsConstants.EVENT_TYPES.LOAD,
            kochavaAnalyticsConstants.EVENT_ACTIONS.LOAD,
            kochavaAnalyticsConstants.EVENT_NAMES.EXPLORE_REEL_LOAD
        );
        expect(videoPlayerServiceMock.loadAsset).toHaveBeenCalled();
    });
});
