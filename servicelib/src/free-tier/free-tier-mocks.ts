export const welcomeScreenInfo = [{
    screens: [{
        id: "iap_welcome_screen",
        fields: [
            {
                name: "sxm-logo",
                type: "image",
                value: "%Image%/images/screen/sxm-logo.png"
            },
            {
                "name": "img-group",
                "fields": [
                    {
                        "name": "sxm-logo1",
                        "type": "image",
                        "value": "%Image%/images/screen/image-1.jpg"
                    },
                    {
                        "name": "sxm-logo2",
                        "type": "image",
                        "value": "%Image%/images/screen/image-2.jpg"
                    },
                    {
                        "name": "sxm-logo3",
                        "type": "image",
                        "value": "%Image%/images/screen/image-3.jpg"
                    }
                ]
            },
            {
                name: "button1",
                supportedLocale: "US,CA",
                actionNeriticLink: "AppScreen:screen:iap_subscribe_screen:flowType=subscribe",
                type: "input-button",
                value: "SUBSCRIBE TO THE SXM APP",
                supportedPlatform: "ios,web,android"
            },
            {
                name: "button2",
                supportedLocale: "US,CA",
                actionNeriticLink: "AppScreen:screen:iap_subscribe_screen:flowType=explore",
                type: "input-button",
                value: "TRY IT OUT",
                supportedPlatform: "ios,web,android"
            },
            {
                name: "bg-img",
                type: "image",
                value: "%Image%/images/screen/background.png"
            },
            {
                name: "footer",
                type: "sub-screen",
                value: "AppScreen:screen:iap_footer_welcome_screen"
            }
        ]
    }]
}];

export const subscribeScreenInfo = [{
    screens: [{
        id: "iap_subscribe_screen",
        fields: [
            {
                name: "sxm-logo",
                type: "image",
                value: "%Image%/images/screen/sxmLogo.png"
            },
            {
                name: "header",
                type: "text",
                value: "Register to start streaming, get personalized recommendations, and save your favorites."
            },
            {
                name: "error_email_invalid",
                type: "text",
                value: "Sorry, email address is not valid"
            },
            {
                name: "error_account_exists",
                type: "text",
                value: "please login, you already have a streaming account."
            },
            {
                name: "enter_email",
                type: "input-text",
                value: "email"
            },
            {
                name: "enter_password",
                type: "input-text",
                value: "password"
            },
            {
                name: "tc_checkbox",
                type: "input-checkbox",
                value: "I accept and agree to SiriusXM’s <a href='http://www.siriusxm.com/pdf/siriusxm_customeragreement_eng.pdf' target='_blank'>Customer Agreement</a> and <a href='http://www.siriusxm.com/pdf/siriusxm_privacypolicy_eng.pdf' target='_blank'>Privacy Policy.</a>"
            },
            {
                name: "button1",
                actionNeriticLink: "App:iap_auth",
                type: "input-button",
                value: "CONTINUE"
            },
            {
                name: "bg_img",
                type: "image",
                value: "%Image%/images/screen/background.png"
            },
            {
                name: "footer",
                type: "sub-screen",
                value: "AppScreen:screen:iap_footer_subscribe_flow"
            }
        ]
    }]
}];

export const upsellScreenInfo = [{
    screens: [
        {
            id: "error_122",
            fields: [
                {
                    "name": "header",
                    "type": "text",
                    "value": "Subscribe to the SXM App to keep listening"
                },
                {
                    "name": "description",
                    "type": "text",
                    "value": "Your free preview has ended."
                },
                {
                    "name": "button_filled",
                    "actionNeriticLink": "AppScreen:screen:iap_select_package",
                    "type": "input-button",
                    "value": "CONTINUE"
                },
                {
                    "name": "text1",
                    "type": "text",
                    "value": "Already a subscriber or have a trial in your car?"
                },
                {
                    "name": "button1",
                    "actionNeriticLink": "AppScreen:screen:iap_accessNow_screen:flowType=accessnow",
                    "type": "input-button",
                    "value": "Sign In "
                }
            ]
        }
    ]
}];

export const footerSubscribeScreenInfo = [{
    screens: [
        {
            id: "iap_footer_subscribe_flow",
            fields: [
                {
                    "name": "text1",
                    "type": "text",
                    "value": "Already a subscriber or have a trial in your car? The SXM App is included."
                },
                {
                    "name": "button_link",
                    "actionNeriticLink": "AppScreen:screen:iap_accessNow_screen:flowType=accessnow",
                    "type": "input-button",
                    "value": "Sign In"
                },
                {
                    "name": "ccpa_request",
                    "url": "https://www.siriusxm.com/ccparequest_DoNotSellMyInfo",
                    "type": "link",
                    "value": "Do Not Sell My Personal Information",
                    "supportedPlatform": "web"
                }
            ]
        }
    ]
}];

export const footerAccessNowScreenInfo = [{
    screens: [{
        id: "iap_footer_accessNow_flow",
        fields: [
            {
                "name": "text1",
                "type": "text",
                "value": "The SXM app is included in your car radio subscription or trial."
            },
            {
                "name": "button_outline",
                "actionNeriticLink": "App:flepz",
                "type": "input-button",
                "value": "GET LOGIN"
            },
            {
                "name": "customer_agreement",
                "url": "http://www.siriusxm.com/pdf/siriusxm_customeragreement_eng.pdf",
                "type": "link",
                "value": "Customer Agreement",
                "supportedPlatform": "web"
            },
            {
                "name": "privacy_policy",
                "url": "http://www.siriusxm.com/pdf/siriusxm_privacypolicy_eng.pdf",
                "type": "link",
                "value": "Privacy Policy",
                "supportedPlatform": "web"
            },
            {
                "name": "ccpa_request",
                "url": "https://www.siriusxm.com/ccparequest_DoNotSellMyInfo",
                "type": "link",
                "value": "Do Not Sell My Personal Information",
                "supportedPlatform": "web"
            }
        ]
    }]
}];

export const upsellBannerScreenInfo = [{
    screens: [{
        id: "iap_upsell_banner_subscribe",
        fields: [
            {
                "name": "text1",
                "type": "text",
                "value": "Your preview expires in {HH:mm:ss}."
            },
            {
                "name": "button1",
                "actionNeriticLink": "AppScreen:screen:iap_select_package",
                "type": "input-button",
                "value": "SUBSCRIBE TO THE SXM APP"
            }
        ]
    }]
}];
