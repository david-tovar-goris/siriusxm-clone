import { FreeTierConstants, FreeTierService } from "./";
import { AuthenticationService } from "../authentication";
import { mock } from "ts-mockito";
import { BehaviorSubject, Observable, of as observableOf } from "rxjs";
import {
    welcomeScreenInfo,
    footerAccessNowScreenInfo,
    footerSubscribeScreenInfo,
    upsellBannerScreenInfo,
    subscribeScreenInfo,
    upsellScreenInfo
} from "./free-tier-mocks";

describe('FreeTierService', () =>
{
    let service: FreeTierService;

    let relativeUrlSettings = [
        {
            "name": "Image",
            "url": "https://siriusxm-art-dd.akamaized.net"
        },
        {
            "name": "Album_Art",
            "url": "https://siriusxm-art-dd.akamaized.net"
        },
        {
            "name": "Live_Primary_HLS",
            "url": "https://siriusxm-priuatlive.akamaized.net"
        }
    ];
    let configServiceMock: any = {
        getRelativeUrlSettings: jasmine.createSpy("getRelativeUrlSettings").and.returnValue(relativeUrlSettings)
    };
    let authenticationServiceMock = mock(AuthenticationService);
    let SERVICE_CONFIG_MOCK: any = {};

    // free-tier delegate mocks for each screen
    let welcomeDelegateMock: any = {
        screensInfo$: observableOf(welcomeScreenInfo),
        getFreeTierScreenInfo: () => {}
    };

    let subscribeDelegateMock: any = {
        screensInfo$: observableOf(subscribeScreenInfo),
        getFreeTierScreenInfo: () => {}
    };

    let upsellDelegateMock: any = {
        screensInfo$: observableOf(upsellScreenInfo),
        getFreeTierScreenInfo: () => {}
    };

    let footerSubscribeDelegateMock: any = {
        screensInfo$: observableOf(footerSubscribeScreenInfo),
        getFreeTierScreenInfo: () => {}
    };

    let footerAccessNowDelegateMock: any = {
        screensInfo$: observableOf(footerAccessNowScreenInfo),
        getFreeTierScreenInfo: () => {}
    };

    let upsellBannerDelegateMock: any = {
        screensInfo$: observableOf(upsellBannerScreenInfo),
        getFreeTierScreenInfo: () => {}
    };

    // resume model mock
    let resumeModelMock: any = {
        globalSettings$: observableOf(true),
        globalSettingsSubject: new BehaviorSubject(null),
        resumeComplete$: observableOf(true)
    };

    // create the service with screen specific mock
    function createService(configMock)
    {
        service = new FreeTierService(
            configMock,
            configServiceMock,
            resumeModelMock,
            authenticationServiceMock,
            SERVICE_CONFIG_MOCK
        );
    }

    it('should be created', () =>
    {
        createService(welcomeDelegateMock);
        expect(service).toBeTruthy();
    });

    it('can get screen info', () =>
    {
        createService(welcomeDelegateMock);
        let screenInfo = service.getScreenInfo(FreeTierConstants.PAGE_NAME.WELCOME_SCREEN);
        expect(screenInfo).toBeTruthy();
    });

    it('can normalize welcome screen info', () =>
    {
        createService(welcomeDelegateMock);
        let screenInfo = service.getScreenInfo(FreeTierConstants.PAGE_NAME.WELCOME_SCREEN);
        expect(screenInfo).toEqual({
            logo: 'https://siriusxm-art-dd.akamaized.net/images/screen/sxm-logo.png',
            carouselImages: [
                'https://siriusxm-art-dd.akamaized.net/images/screen/image-1.jpg',
                'https://siriusxm-art-dd.akamaized.net/images/screen/image-2.jpg',
                'https://siriusxm-art-dd.akamaized.net/images/screen/image-3.jpg'
            ],
            backgroundImg: 'https://siriusxm-art-dd.akamaized.net/images/screen/background.png',
            buttonOne: {
                    text: 'SUBSCRIBE TO THE SXM APP',
                    neriticAction: { app: '', videoUrl: '', screen: 'iap_subscribe_screen', flowType: 'subscribe' },
                    url: null
            },
            buttonTwo: {
                    text: 'TRY IT OUT',
                    neriticAction: {
                        app: '',
                        videoUrl: '',
                        screen: 'iap_subscribe_screen',
                        flowType: 'explore'
                    },
                    url: null
            },
            subScreenName: 'iap_footer_welcome_screen'
        });
        expect(screenInfo).toBeDefined();
    });

    it('can normalize registration overlay info', () =>
    {
        createService(subscribeDelegateMock);
        let screenInfo = service.getScreenInfo(FreeTierConstants.PAGE_NAME.SUBSCRIBE_SCREEN);
        expect(screenInfo).toEqual({
            logo: 'https://siriusxm-art-dd.akamaized.net/images/screen/sxmLogo.png',
            backgroundImg: 'https://siriusxm-art-dd.akamaized.net/images/screen/background.png',
            registerNow: 'Register to start streaming, get personalized recommendations, and save your favorites.',
            username: 'email',
            emailError: 'Sorry, email address is not valid',
            ftNotEligibleError: 'please login, you already have a streaming account.',
            password: 'password',
            forgotPassword: undefined,
            subScreenName: 'iap_footer_subscribe_flow',
            checkbox: "I accept and agree to SiriusXM’s <a href='http://www.siriusxm.com/pdf/siriusxm_customeragreement_eng.pdf' target='_blank'>Customer Agreement</a> and <a href='http://www.siriusxm.com/pdf/siriusxm_privacypolicy_eng.pdf' target='_blank'>Privacy Policy.</a>",

            submitBtn: {
                    text: 'CONTINUE',
                    neriticAction:
                        { app: undefined, videoUrl: '', screen: '', flowType: null },
                     url: null
                    }
            });
    });

    it('can normalize upsell banner subscribe info', () =>
    {
        createService(upsellBannerDelegateMock);
        let screenInfo = service.getScreenInfo(FreeTierConstants.PAGE_NAME.UPSELL_BANNER_SUBSCRIBE);
        expect(screenInfo).toBeDefined();
    });

    it('can normalize upsell screen info', () =>
    {
        createService(upsellDelegateMock);
        let screenInfo = service.getScreenInfo(FreeTierConstants.PAGE_NAME.UPSELL_SCREEN);
        expect(screenInfo).toBeDefined();
    });

    it('can normalize footer subscribe info', () =>
    {
        createService(footerSubscribeDelegateMock);
        let screenInfo = service.getScreenInfo(FreeTierConstants.PAGE_NAME.FOOTER_SUBSCRIBE);
        expect(screenInfo).toBeDefined();
    });

    it('can normalize footer access now info', () =>
    {
        createService(footerAccessNowDelegateMock);
        let screenInfo = service.getScreenInfo(FreeTierConstants.PAGE_NAME.FOOTER_ACCESS_NOW);
        expect(screenInfo).toBeDefined();
    });
});
