/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import * as _                                           from "lodash";
import {Observable, of as observableOf }                                     from "rxjs";
import {AuthenticationDelegate, AuthenticationResponse} from "./authentication.delegate";
import {testObservable}                                 from "../test/test.util";

import {
    IDeviceSetting,
    IGlobalSetting,
    IAuthenticationData,
    IClientConfiguration,
    IAuthenticationResponse
}                                                       from "../index";
import { ServiceFactory }                               from "../service/service.factory";
import { AuthenticationConstants } from "./authentication.constants";

interface ThisContext
{
    authenticationDelegate : AuthenticationDelegate;
    authenticationData : IAuthenticationData;
    authenticationLogoutResponse : any;
    clientConfiguration : IClientConfiguration;
    mockHttp : any;
    username : string;
    password : string;
    deviceSettingList : Array<IDeviceSetting>;
    globalSettingList : Array<IGlobalSetting>;
}

describe("Authentication Delegate Test Suite", () =>
{
    beforeEach(function(this:ThisContext)
    {
        this.username = "username";
        this.password = "password";

        this.deviceSettingList = [
            {
                deviceId : "testDeviceId-1",
                name     : "testDeviceName-1",
                value    : "testDeviceValue-1"
            },
            {
                deviceId : "testDeviceId-2",
                name     : "testDeviceName-2",
                value    : "testDeviceValue-2"
            }
        ];

        this.globalSettingList = [
            {
                settingName  : "testGlobalSettingName-1",
                settingValue : "testGlobalSettingValue-1"
            },
            {
                settingName  : "testGlobalSettingName-2",
                settingValue : "testGlobalSettingValue-2"
            }
        ];

        this.authenticationData = {
            remainingLockoutSeconds : 10,
            remainingLockoutMinutes : 11,
            sessionID               : "testSessionId",
            username                : this.username
        };

        this.clientConfiguration =
            {
                activeListeningGracePeriodInMins  : 1,
                authenticationGracePeriodInDays   : 2,
                cachedContentDeletionPeriodInDays : 3,
                clientDeviceId                    : "1000",
                channelLineupId                   : 4,
                dataPlan                          : true,
                facebookAppId                     : "testFacebookAppId",
                familyFriendly                    : false,
                freeToAirPeriodState              : true,
                faqurl                            : "testFaqurl",
                fbLinkStatus                      : "testfbLinkStatus",
                maximumAutoDownloads              : 4,
                onDemandExpiryDays                : 5,
                openAccessPeriodState             : false,
                radioAuthorizationDelayDuration   : 0,
                radioAuthorizationRequired        : false,
                seededRadioSubscribed             : true,
                streamingAccess                   : 1,
                userDaysListened                  : 0
            };

        const authenticationResponse = {
            deviceSettingList   : {
                deviceSettings : this.deviceSettingList
            },
            globalSettingList   : {
                globalSettings : this.globalSettingList
            },
            authenticationData  : this.authenticationData,
            clientConfiguration : this.clientConfiguration
        };

        const authenticationLogoutResponse = {
            messages : [ "testMessage" ],
            isLogout : false
        };

        this.authenticationLogoutResponse = authenticationLogoutResponse;

        this.mockHttp =
            {
                get               : function ( url, config )
                {
                    return observableOf( authenticationLogoutResponse );
                },
                post              : function ( url, data, config )
                {
                    return observableOf( authenticationResponse );
                },
                postModuleRequest : function ()
                {
                    return observableOf( authenticationResponse );
                }
            };

        this.authenticationDelegate = new AuthenticationDelegate( this.mockHttp );
    });

    describe("Infrastructure >>",function(this:ThisContext)
    {
        it("Should be able to create the service and all its dependencies",function(this:ThisContext)
        {
            expect(ServiceFactory.getInstance(AuthenticationDelegate)).toBeDefined();
        });

        it("Constructor",function(this:ThisContext)
        {
            expect(this.authenticationDelegate).toBeDefined();
            expect(this.authenticationDelegate instanceof AuthenticationDelegate).toEqual(true);
        });
    });

    describe("`login()` >> ",function(this:ThisContext)
    {
        describe("Infrastructure >> ",function(this:ThisContext)
        {
            it("Should have method defined on the test subject.",function(this:ThisContext)
            {
                expect(this.authenticationDelegate.login).toBeDefined();
                expect(_.isFunction(this.authenticationDelegate.login)).toBeTruthy();
            });

            it("Should return an observable.",function(this:ThisContext)
            {
                expect(this.authenticationDelegate.login(this.username, this.password, null) instanceof Observable).toBeTruthy();
            });
        });

        describe("Execution >> ",function(this:ThisContext)
        {
            it("Should return an IAuthenticationResponse after successful login.",function(this:ThisContext)
            {
                const expectations : Function = function (response)
                {
                    expect(response instanceof AuthenticationResponse).toEqual(true);
                };

                testObservable(this.authenticationDelegate.login(this.username, this.password, null), expectations);
            });

            it("Should set deviceSettingList in response after successful login.", function(this:ThisContext)
            {
                const deviceSettingList = this.deviceSettingList;

                this.authenticationDelegate
                    .login(this.username, this.password, null)
                    .subscribe(function(response: IAuthenticationResponse)
                    {
                        expect(response.deviceSettingList).toEqual(deviceSettingList);
                    });
            });

            it("Should set globalSettingList in response after successful login.", function(this:ThisContext)
            {
                const globalSettingList = this.globalSettingList;

                this.authenticationDelegate
                    .login(this.username, this.password, null)
                    .subscribe(function(response : IAuthenticationResponse)
                    {
                        expect(response.globalSettingList).toEqual(globalSettingList);
                    });
            });


            it ('should call auth with nu detect parameters if passed in', function()
            {
                spyOn(this.mockHttp, 'postModuleRequest').and.callThrough();

                const self = this;

                this.authenticationDelegate
                    .login(this.username, this.password, null,{
                        ndPayload: { a: '1' },
                        ndSessionId: 'sessionid'
                    })
                    .subscribe(function()
                    {
                        expect(self.mockHttp.postModuleRequest
                            .calls.argsFor(0)[1].nuDetect.ndSpmd).toEqual(JSON.stringify({a: '1'}));
                        expect(self.mockHttp.postModuleRequest
                            .calls.argsFor(0)[1].nuDetect.ndSessionId).toEqual('sessionid');

                        expect(self.mockHttp.postModuleRequest
                            .calls.argsFor(0)[1].nuDetect.ndPlacement).toEqual(AuthenticationConstants.NUDETECT_PLACEMENT);
                        expect(self.mockHttp.postModuleRequest
                            .calls.argsFor(0)[1].nuDetect.ndPlacementPage).toEqual(AuthenticationConstants.NUDETECT_PLACEMENT_PAGE);

                        expect(self.mockHttp.postModuleRequest
                            .calls.argsFor(0)[1].nuDetect.ndReportingSegment).toEqual(AuthenticationConstants.NUDETECT_REPORTING_SEGMENT);
                    });

            });
        });
    });

    describe("logout >>",function(this:ThisContext)
    {
        describe("Infrastructure >> ",function(this:ThisContext)
        {
            it("Should have method defined on the test subject.",function(this:ThisContext)
            {
                expect(this.authenticationDelegate.logout).toBeDefined();
                expect(_.isFunction(this.authenticationDelegate.logout)).toBeTruthy();
            });

            it("Should return an observable.",function(this:ThisContext)
            {
                expect(this.authenticationDelegate.logout(true) instanceof Observable).toBeTruthy();
            });
        });

        describe("Execution >> ",function(this:ThisContext)
        {
            it("Should return an IAuthenticationLogoutResponse after successful logout.",function(this:ThisContext)
            {
                const expectations : Function = function (this:ThisContext,response)
                {
                    expect(response).toEqual(this.authenticationLogoutResponse);
                };

                testObservable(this.authenticationDelegate.logout(true), expectations.bind(this));
            });

            it("Should return an isLogout to be true after successful logout when appExit true.",
               function(this:ThisContext)
               {
                   this.authenticationDelegate
                       .logout(true)
                       .subscribe(function (response)
                       {
                         expect(response.isLogout).toEqual(true);
                       },
                       function (err) {});
               });

            it("Should return an isLogout to be true after successful logout when appExit false.",
               function(this:ThisContext)
               {
                   this.authenticationDelegate
                       .logout(false)
                       .subscribe(function (response)
                       {
                           expect(response.isLogout).toEqual(true);
                       },
                       function (err) {});
               });
        });
    });
});
