/**
 * @MODULE:     service-lib
 * @CREATED:    07/19/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *      Defines the interface for response data returned by the authentication API.
 */
import {IClientConfiguration} from "../config";

export interface IAuthenticationResponse
{
    deviceSettingList: Array<IDeviceSetting>;
    globalSettingList: Array<IGlobalSetting>;
    authenticationData: IAuthenticationData;
    clientConfiguration: IClientConfiguration;
}

export interface IAuthenticationLogoutResponse
{
    isLogout : boolean;
    messages ?: any[];
}

export interface IAuthenticationData
{
    remainingLockoutMinutes: number;
    remainingLockoutSeconds: number;
    sessionID: string;
    username: string;
}

export interface IDeviceSetting
{
    deviceId: string;
    name: string;
    value: string;
}

export interface IGlobalSetting
{
    settingName: string;
    settingValue: string;
}
