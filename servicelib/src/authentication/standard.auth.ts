import { ModuleRequest } from "../http";
import {
    NuDetectPayload,
    AuthenticationConstants,
    IScreenFlow
} from "./authentication.constants";
import { EFreeTierFlow } from "../free-tier";

export class StandardAuth extends ModuleRequest
{
    public standardAuth: {
        username : string,
        password : string
    };

    public nuDetect : NuDetectPayload;
    public screenFlow: IScreenFlow;

    constructor(username: string, password: string, screenName:EFreeTierFlow, nuDetectPayload?: NuDetectPayload)
    {
        super();

        this.standardAuth = {
            username: username,
            password: password
        };

        if (nuDetectPayload)
        {
            this.nuDetect = {
                ndSpmd : JSON.stringify(nuDetectPayload.ndPayload),
                ndSessionId : nuDetectPayload.ndSessionId,
                ndPlacement : AuthenticationConstants.NUDETECT_PLACEMENT,
                ndPlacementPage : AuthenticationConstants.NUDETECT_PLACEMENT_PAGE,
                ndReportingSegment : AuthenticationConstants.NUDETECT_REPORTING_SEGMENT
            };
        }

        if(screenName)
        {
            this.screenFlow = {
                name: screenName
            };
        }
    }

}

