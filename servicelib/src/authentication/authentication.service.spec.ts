/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />


import {
    throwError as observableThrowError,
    Observable,
    ReplaySubject,
    of as observableOf,
    BehaviorSubject
} from 'rxjs';
import * as _ from "lodash";
import { mock } from "ts-mockito";

import { testObservable } from "../test/test.util";

import { ServiceFactory } from "../service/service.factory";

import {
    IAuthenticationData,
    IAuthenticationResponse,
    IDeviceSetting,
    IGlobalSetting,
    IAuthenticationLogoutResponse
} from "./authentication.response.interface";

import {
    AuthenticationService,
    Session
} from "./authentication.service";

import
{
    ApiCodes,
    ApiDelegate,
    EventBus,
    IClientConfiguration,
    AppMonitorService,
    SessionMonitorService,
    authLoginCodes
} from "../index";

import { openAccessStatus } from "./session.interface";
import { MockAppConfig }    from "../test/mocks/app.config.mock";
import { IAppConfig }       from "../config";

interface ThisContext
{
    authDelegate : any;
    apiDelegate : ApiDelegate;
    sessionMonitorService : any;
    appConfig : IAppConfig;
    authenticationService : AuthenticationService;
    unathenticationResponse : any;
    authenticationResponse : any;
    authenticationLogoutResponse : IAuthenticationLogoutResponse;
    clientConfiguration : IClientConfiguration;
    sessionMock : Session;
    username : string;
    password : string;
    appMonitorServiceMock : any;
    sxmAnalyticsServiceMock: any;
    authModelMock: any;
    initModelMock: any;
}

describe("AuthenticationService Test Suite >>", () =>
{
    beforeEach(function(this:ThisContext)
    {
        this.username    = "foo";
        this.password    = "bar";
        this.sessionMock = mock( Session );
        this.apiDelegate = new ApiDelegate( new EventBus );

        this.authModelMock =
            {
                authenticating$: new BehaviorSubject(false) as Observable<any>,
                setAuthenticatingValue :  jasmine.createSpy( 'setAuthenticatingValue' )
            };

        this.initModelMock =
            {
                initializationState$: new BehaviorSubject("") as Observable<any>,
                setInitializationState :  jasmine.createSpy( 'setInitializationState' )
            };

        this.appMonitorServiceMock = {
            ...mock( AppMonitorService ),
            handleMessage      : jasmine.createSpy( 'handleMessage' ),
            checkIfErrorIsHttp : jasmine.createSpy( 'checkIfErrorIsHttp' )
        };

        this.unathenticationResponse = {
            status     : 200,
            statusText : "OK",
            headers    : [],
            config     : {},
            data       : { ModuleListResponse : { messages : [ { code : ApiCodes.AUTH_REQUIRED, message : "" } ] } }
        };

        const deviceSettingList: Array<IDeviceSetting> = [
            {
                deviceId: "deviceId-100",
                name: "name-100",
                value: "value-100"
            },
            {
                deviceId: "deviceId-200",
                name: "name-200",
                value: "value-200"
            }
        ];
        const globalSettingList: Array<IGlobalSetting> = [
            {
                settingName: "settingName-100",
                settingValue: "settingValue-100"
            },
            {
                settingName: "settingName-200",
                settingValue: "settingValue-200"
            }
        ];
        const authenticationData: IAuthenticationData = {
            remainingLockoutMinutes: 100,
            remainingLockoutSeconds: 200,
            sessionID: "sessionID",
            username: this.username
        };
        const clientConfiguration = {
            activeListeningGracePeriodInMins: 100,
            authenticationGracePeriodInDays: 200,
            cachedContentDeletionPeriodInDays: 300,
            channelLineupId : 10,
            clientDeviceId: "10",
            dataPlan : false,
            facebookAppId: "facebookAppId",
            faqurl: "faqurl",
            freeToAirPeriodState : false,
            fbLinkStatus: "fbLinkStatus",
            maximumAutoDownloads: 400,
            onDemandExpiryDays: 500,
            openAccessPeriodState: false,
            streamingAccess : 1
        };

        const authenticationResponse = {
            deviceSettingList: deviceSettingList,
            globalSettingList: globalSettingList,
            authenticationData: authenticationData,
            clientConfiguration: clientConfiguration
        };

        const authenticationLogoutResponse = {
            messages : ["testMessage"],
            isLogout : false
        };

        this.authenticationResponse       = authenticationResponse;
        this.authenticationLogoutResponse = authenticationLogoutResponse;

        this.authDelegate = {
            login: function (username, password)
            {
                return observableOf(authenticationResponse);
            },
            logout: function ()
            {
                return observableOf(authenticationLogoutResponse);
            }
        };

        this.appConfig = new MockAppConfig();

        this.appConfig.nuDetect = {
            getHiddenValue: jasmine.createSpy('getHiddenValue').and.returnValue('{"nudetectattr":1}'),
            sessionId: 'sessionid'
        } as any;

        (this.appConfig.inPrivateBrowsingMode as ReplaySubject<boolean>).next(false);

        this.sessionMonitorService = new SessionMonitorService(this.apiDelegate,this.appConfig);
        this.authenticationService = new AuthenticationService(this.appMonitorServiceMock,
                                                               this.authDelegate,
                                                               this.appConfig,
                                                               this.sessionMonitorService,
                                                               this.authModelMock,
                                                               this.initModelMock);
    });

    describe("Infrastructure >> ",function(this:ThisContext)
    {
        it("Should have a test subject.",function(this:ThisContext)
        {
            expect(this.authenticationService).toBeDefined();
        });

        it("Should expose the expected public properties.",function(this:ThisContext)
        {
            expect(this.authenticationService.userSession).toBeDefined();
        });
    });

    describe("`login()` >> ",function(this:ThisContext)
    {
        describe("Infrastructure >> ",function(this:ThisContext)
        {
            it("Should have method defined on the test subject.",function(this:ThisContext)
            {
                expect(this.authenticationService.login).toBeDefined();
                expect(_.isFunction(this.authenticationService.login)).toBeTruthy();
            });

            it("Should return an observable.",function(this:ThisContext)
            {
                expect(this.authenticationService.login(this.username, this.password) instanceof Observable).toBeTruthy();
            });
        });

        describe("Execution >> ",function(this:ThisContext)
        {
            it("Should return an IAuthenticationResponse after successful login.",function(this:ThisContext)
            {
                const authenticationResponse = this.authenticationResponse;

                const expectations: Function = function(response)
                {
                    expect(response).toEqual(authenticationResponse);
                };

                testObservable(this.authenticationService.login(this.username,this.password), expectations);
            });

            it("Should save the session object on the service after successful login.",function(this:ThisContext)
            {
                this.authenticationService.login(this.username, this.password);

                this.authenticationService.userSession.subscribe(
                        function (session)
                        {
                            const authData = this.authenticationResponse.authenticationData;

                            Object.keys(authData)
                                  .forEach((key : string) => { expect(session[key]).toBe(authData[key]); });
                        }.bind(this),
                        function (err)
                        {
                            //console.log(`Error: ${JSON.stringify(err)}`);
                        });
            });

            it("Should set login required to be false after successful login.",function(this:ThisContext)
            {
                this.appConfig.loginRequired = true;
                this.authenticationService.login(this.username, this.password).subscribe((success) =>
                {
                    expect(this.appConfig.loginRequired).toEqual(false);
                });

                this.authenticationService.userSession.subscribe(
                    function (session)
                    {
                        const authData = this.authenticationResponse.authenticationData;
                        Object.keys(authData)
                              .forEach((key : string) => { expect(session[key]).toBe(authData[key]); });

                    }.bind(this));
            });

            it("Should return an error after failure login.",function(this:ThisContext)
            {
                const error = {
                    code    : 401,
                    message : "Bad UserName/Password"
                };
                this.appConfig.loginRequired = true;
                spyOn(this.authDelegate, "login").and.returnValue(observableThrowError(error));

                const expectations: Function = function (response)
                {
                    expect(response).toEqual(error);
                    expect(this.appConfig.loginRequired).toEqual(true);
                };

                testObservable(this.authenticationService.login(this.username, this.password), expectations);
            });

            it("Should not reset login required flag if failure login.",function(this:ThisContext)
            {
                const error = {
                    code    : 401,
                    message : "Bad UserName/Password"
                };

                spyOn(this.authDelegate, "login").and.returnValue(observableThrowError(error));

                const expectations: Function = function (response)
                {
                    expect(response).toEqual(error);
                };

                testObservable(this.authenticationService.login(this.username, this.password), expectations);
            });


            it('should pass along nudetect payload', function()
            {
                spyOn(this.authDelegate, 'login').and.returnValue(observableOf({
                    authenticationData: {}
                }));

                const expectations = () =>
                {
                    expect(this.authDelegate.login).toHaveBeenCalledWith('foo',
                                                                         'bar',
                                                                          null,
                                                                         {
                                                                             ndSessionId : 'sessionid',
                                                                             ndPayload   : { nudetectattr : 1 }
                                                                         });
                };

                testObservable(this.authenticationService.login(this.username, this.password), expectations);
            });
        });
    });

    describe("`logout()` >> ",function(this:ThisContext)
    {
        describe("Infrastructure >> ",function(this:ThisContext)
        {
            it("Should have method defined on the test subject.",function(this:ThisContext)
            {
                expect(this.authenticationService.logout()).toBeDefined();
                expect(_.isFunction(this.authenticationService.logout)).toBeTruthy();
            });

            it("Should return an observable.",function(this:ThisContext)
            {
                expect(this.authenticationService.logout() instanceof Observable).toBeTruthy();
            });
        });

        describe("Execution >> ",function(this:ThisContext)
        {
            it("Should return an IAuthenticationLogoutResponse after successful logout.",function(this:ThisContext)
            {
                const expectations: Function = function(response)
                {
                    expect(response).toEqual(this.authenticationLogoutResponse);
                }.bind(this);

                testObservable(this.authenticationService.logout(), expectations);
            });
        });
    });

    describe("`isAuthenticated()` >> ",function(this:ThisContext)
    {
        describe("Infrastructure >> ",function(this:ThisContext)
        {
            it("Should have method defined on the test subject.",function(this:ThisContext)
            {
                expect(this.authenticationService.isAuthenticated()).toBeDefined();
                expect(_.isFunction(this.authenticationService.isAuthenticated)).toBeTruthy();
            });

            it("Should return an observable.",function(this:ThisContext)
            {
                expect(_.isBoolean(this.authenticationService.isAuthenticated())).toBeTruthy();
            });
        });

        describe("Execution >> ",function(this:ThisContext)
        {
            it("Should return false when the user when login required.",function(this:ThisContext)
            {
                this.appConfig.loginRequired = true;
                this.sessionMock.authenticated = true;
                this.authenticationService.session = this.sessionMock;

                expect(this.authenticationService.isAuthenticated()).toEqual(false);
            });

            it("Should return true when the user is authenticated.",function(this:ThisContext)
            {
                this.sessionMock.authenticated = true;
                this.authenticationService.session = this.sessionMock;
                this.appConfig.loginRequired = false;
                expect(this.authenticationService.isAuthenticated()).toEqual(true);
            });

            it("Should return false when the user is not authenticated.",function(this:ThisContext)
            {
                let session = new Session();
                session.authenticated = false;
                this.appConfig.loginRequired = false;
                this.authenticationService.session = session;
                expect(this.authenticationService.isAuthenticated()).toEqual(false);
            });
        });
    });

    describe("`openAccessLogin()` >> ",function(this:ThisContext)
    {
        describe("Infrastructure >> ",function(this:ThisContext)
        {
            it("Should have method defined on the test subject.",function(this:ThisContext)
            {
                expect(_.isFunction(this.authenticationService.openAccessLogin)).toBeTruthy();
            });
        });

        describe("Execution >> ",function(this:ThisContext)
        {
            it("Should set authenticated to be true when the user is open access eligible.",function(this:ThisContext)
            {
                this.sessionMock.openAccessStatus = openAccessStatus.ELIGIBLE;
                this.authenticationService.session = this.sessionMock;
                this.authenticationService.session.authenticated = false;
                this.authenticationService.openAccessLogin();
                expect(this.authenticationService.session.authenticated).toEqual(true);
            });

            it("when the user is not open access eligible.",function(this:ThisContext)
            {
                this.sessionMock.openAccessStatus = openAccessStatus.INELIGIBLE;
                this.authenticationService.session = this.sessionMock;
                this.authenticationService.session.authenticated = false;
                this.authenticationService.openAccessLogin();
                expect(this.authenticationService.session.authenticated).toEqual(false);
            });
        });
    });

    describe("`updateOpenAccessSession()` >> ",function(this:ThisContext)
    {
        describe("Infrastructure >> ",function(this:ThisContext)
        {
            it("Should have method defined on the test subject.",function(this:ThisContext)
            {
                expect(_.isFunction(this.authenticationService.updateOpenAccessSession)).toBeTruthy();
            });
        });

        describe("Execution >> ",function(this:ThisContext)
        {
            it("Should set authenticated to be true when the user is openAccessPeriodState is true.",function(this:ThisContext)
            {
                this.authenticationService.session.authenticated = false;
                const clientConfig = {
                    openAccessPeriodState : true
                } as any as IClientConfiguration;
                this.authenticationService.updateOpenAccessSession(clientConfig);
                expect(this.authenticationService.session.authenticated).toEqual(true);
            });

            it("Should set authenticated to be false when the user is openAccessPeriodState is false.",function(this:ThisContext)
            {
                this.authenticationService.session.authenticated = false;
                const clientConfig = {
                    activeListeningGracePeriodInMins : 1
                } as IClientConfiguration;
                this.authenticationService.updateOpenAccessSession(clientConfig);
                expect(this.authenticationService.session.authenticated).toEqual(false);
            });
        });
    });

    describe("`ApiDelegate code handlers()` >> ",function(this:ThisContext)
    {
        it("Should set the session to unauthenticated when AUTH_REQUIRED is triggered",function(this:ThisContext)
        {
            const unauthenticated = {
                status : 200,
                statusText : "OK",
                headers : [],
                config : {},
                data : { ModuleListResponse : { messages : [{code : ApiCodes.AUTH_REQUIRED, message : ""}]}}
            };

            try
            {
                this.apiDelegate.checkApiResponse(unauthenticated);
            }
            catch (error)
            {
                //console.log(error);
                //console.log(JSON.stringify(error));
                expect(error.code).toEqual(ApiCodes.AUTH_REQUIRED);
            }

            expect(this.authenticationService.session.authenticated).toEqual(false);
        });

        it("Should set the session to unauthenticated when INVALID_CREDENTIALS is triggered",function(this:ThisContext)
        {
            this.unathenticationResponse.data.ModuleListResponse.messages[0].code = ApiCodes.INVALID_CREDENTIALS;

            try
            {
                this.apiDelegate.checkApiResponse(this.unathenticationResponse);
            }
            catch (error)
            {
                expect(error.code).toEqual(ApiCodes.INVALID_CREDENTIALS);
            }

            expect(this.authenticationService.session.authenticated).toEqual(true);
        });

        it("Should set the session to unauthenticated when ACCOUNT_LOCKED is triggered",function(this:ThisContext)
        {
            this.unathenticationResponse.data.ModuleListResponse.messages[0].code = ApiCodes.ACCOUNT_LOCKED;

            try
            {
                this.apiDelegate.checkApiResponse(this.unathenticationResponse);
            }
            catch (error)
            {
                expect(error.code).toEqual(ApiCodes.ACCOUNT_LOCKED);
            }

            expect(this.authenticationService.session.authenticated).toEqual(true);
            expect(this.authenticationService.session.accountLocked).toEqual(true);
        });

        it("Should set the session to unauthenticated when EXPIRED_SUBSCRIPTION is triggered",function(this:ThisContext)
        {
            this.unathenticationResponse.data.ModuleListResponse.messages[0].code = ApiCodes.EXPIRED_SUBSCRIPTION;

            try
            {
                this.apiDelegate.checkApiResponse(this.unathenticationResponse);
            }
            catch (error)
            {
                expect(error.code).toEqual(ApiCodes.EXPIRED_SUBSCRIPTION);
            }

            expect(this.authenticationService.session.authenticated).toEqual(true);
            expect(this.authenticationService.session.accountExpired).toEqual(true);
        });

        it("Should set the session to unauthenticated when EXPIRED_PROSPECT_TRIAL_ACCOUNT is triggered",
           function(this:ThisContext)
        {
            this.unathenticationResponse.data.ModuleListResponse.messages[0].code = ApiCodes.EXPIRED_PROSPECT_TRIAL_ACCOUNT;

            try
            {
                this.apiDelegate.checkApiResponse(this.unathenticationResponse);
            }
            catch (error)
            {
                expect(error.code).toEqual(ApiCodes.EXPIRED_PROSPECT_TRIAL_ACCOUNT);
            }

            expect(this.authenticationService.session.authenticated).toEqual(true);
            expect(this.authenticationService.session.trialAccountExpired).toEqual(true);
        });

        it("Should set the session to open access eligible when OPEN_ACCESS_ELIGIBLE is triggered",function(this:ThisContext)
        {
            this.unathenticationResponse.data.ModuleListResponse.messages = [
                { code: ApiCodes.OPEN_ACCESS_ELIGIBLE, message: "" },
                { code: ApiCodes.AUTH_REQUIRED, message: "" }
            ];
            try
            {
                this.apiDelegate.checkApiResponse(this.unathenticationResponse);
            }
            catch (error)
            {
                expect(error.code).toEqual(ApiCodes.OPEN_ACCESS_ELIGIBLE);
            }

            expect(this.authenticationService.session.openAccessStatus).toEqual(openAccessStatus.ELIGIBLE);
        });

        it("Should set the session to open access eligible when OPEN_ACCESS_ELIGIBLE is triggered and on loginrequired",function(this:ThisContext)
        {
            this.appConfig.loginRequired = true;
            this.unathenticationResponse.data.ModuleListResponse.messages = [
                { code: ApiCodes.OPEN_ACCESS_ELIGIBLE, message: "" },
                { code: ApiCodes.AUTH_REQUIRED, message: "" }
            ];
            try
            {
                this.apiDelegate.checkApiResponse(this.unathenticationResponse);
            }
            catch (error)
            {
                expect(error.code).toEqual(ApiCodes.OPEN_ACCESS_ELIGIBLE);
            }

            expect(this.authenticationService.session.openAccessStatus).toEqual(openAccessStatus.UNAVAILABLE);
        });
    });

    describe("`setting the private browsing status on the session model`",function(this:ThisContext)
    {
      beforeEach(function(this:ThisContext)
      {
          this.unathenticationResponse.data.ModuleListResponse.messages[ 0 ].code = ApiCodes.AUTH_REQUIRED;

          try
          {
              this.apiDelegate.checkApiResponse( this.unathenticationResponse );
          }
          catch (e)
          {
              expect( e.code ).toEqual( ApiCodes.AUTH_REQUIRED );
          }
      });

        describe( "when the checkPrivateBrowsingMode observable emits true", function ( this : ThisContext )
        {
            it( "updates the session.isInPrivateBrowsingMode to true", function ( this : ThisContext )
            {
                this.authenticationService.session.isInPrivateBrowsingMode = false;
                (this.appConfig.inPrivateBrowsingMode as ReplaySubject<boolean>).next( true );
                testObservable( this.authenticationService.userSession, ( sess ) =>
                {
                    expect( sess.isInPrivateBrowsingMode ).toBe( true );
                } );
            } );
        } );

        describe( "when the checkPrivateBrowsingMode observable emits false", function ( this : ThisContext )
        {
            it( "updates the session.isInPrivateBrowsingMode to false", function ( this : ThisContext )
            {
                this.authenticationService.session.isInPrivateBrowsingMode = true;
                (this.appConfig.inPrivateBrowsingMode as ReplaySubject<boolean>).next(false);
                testObservable( this.authenticationService.userSession, ( sess ) =>
                {
                    expect( sess.isInPrivateBrowsingMode ).toBe( false );
                } );
            } );
        } );
    });
});
