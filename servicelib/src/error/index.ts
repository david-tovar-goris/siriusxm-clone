/**
 * This is known as a barrel file. It allows developers to package and rollup multiple exports
 * into a single file and thus single import/export; e.g., `export * from "./authentication/service/";`
 */
export { EError } from "./error.enum";
export * from "./error.interface";
export { ErrorService } from "./error.service";
