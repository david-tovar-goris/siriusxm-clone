import { EError } from "./error.enum";

export interface IErrorInformation {
    isInErrorState: boolean;
    errorType: EError;
}
