/**
 * This is known as a barrel file. It allows developers to package and rollup multiple exports
 * into a single file and thus single import/export; e.g., `export * from "./authentication/service/";`
 */
export { DmcaService } from "./dmca.service";
export { IrNavClassConstants } from "./ir-nav-class.const";
export { IDmcaInfo, IDrmInfo, IDmcaInfoItem, SkipInfo } from './dmca.interface';
