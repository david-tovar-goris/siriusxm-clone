
export interface IDmcaInfo
{
    backSkipDur: number;
    fwdSkipDur: number;
    maxSkipDur: number;
    channelContentType: string;
    irNavClass: string;
    maxBackSkips: number;
    maxFwdSkips: number;
    maxTotalSkips: number;
    playOnSelect: string;
}

export interface IDrmInfo
{
    recordRestriction?: string;
    sharedFlag: boolean;
}

export interface SkipInfo {
    mediaId: string;
    secondaryMediaId?: string;  // could be a stationId for seeded
    mediaType: string;
    numSkipsBack: number;
    numSkipsForward: number;
    skipTimestamp: string;
    maxTotalSkips: number;
}

export interface SkipInfoDictionary {
    [prop: string]: SkipInfo;
}

export interface IDmcaInfoItem {
    dmcaInfo: IDmcaInfo;
    mediaType: string;
    mediaId: string;
}
