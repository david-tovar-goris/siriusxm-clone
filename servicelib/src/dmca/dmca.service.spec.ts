/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { DmcaService } from "./dmca.service";
import { IDmcaInfoItem } from "./dmca.interface";
import { SkipInfo } from "./dmca.interface";
import { BehaviorSubject } from "rxjs";
import { SettingsConstants } from "../settings/settings.const";

describe('DMCAService', function()
{
    beforeEach(function()
    {
        jasmine.clock().uninstall();
        jasmine.clock().install();

        this.settingsService = {
            settings: new BehaviorSubject(null),
            getGlobalSettingValue: jasmine.createSpy('getGlobalSettingValue'),
            setGlobalSettingValue: jasmine.createSpy('setGlobalSettingValue')
        };
        this.service = new DmcaService(this.settingsService);

        const val = {
            g23e: {
                mediaId: 'g23e',
                mediaType: 'live',
                numSkipsBack: 1,
                numSkipsForward: 2,
                maxTotalSkips: 5
            }
        };

        this.settingsService.getGlobalSettingValue.and.returnValue(
            JSON.stringify(val)
        );

        this.settingsService.settings.next({
            deviceSettings: null,
            globalSettings: {
               SkipDmcaDictionary: val
            }
        });

    });

    afterEach(function()
    {
       jasmine.clock().uninstall();
    });


    describe('constructor()', function()
    {
        describe('when settings observable emits', function()
        {
            it('retrieves value from gup', function()
            {
                expect(this.service.skipInfoDictionary$.getValue())
                    .toEqual({
                        g23e: {
                            mediaId: 'g23e',
                            mediaType: 'live',
                            numSkipsBack: 1,
                            numSkipsForward: 2,
                            maxTotalSkips: 5
                        }
                    });
            });
        });


        describe('when skip info dictionary emits', function()
        {
            beforeEach(function()
            {
                this.skipInfo = {
                    mediaId: 'g23e',
                    mediaType: 'live',
                    numSkipsBack: 1,
                    numSkipsForward: 3,
                    maxTotalSkips: 5
                } as SkipInfo;

                this.service.skipInfoDictionary$.next({
                    g23e: this.skipInfo
                });

                jasmine.clock().tick(4000);
            });

            it('sets gup', function()
            {
                expect(this.settingsService.setGlobalSettingValue).toHaveBeenCalledWith(
                    '{"g23e":{"mediaId":"g23e","mediaType":"live","numSkipsBack":1,"numSkipsForward":3,"maxTotalSkips":5}}',
                    SettingsConstants.SKIP_DMCA_DICTIONARY
                );
            });
        });
    });

    describe('hasSkipsRemaining()', function()
    {
        describe('when back + fwd equal max', function()
        {
            beforeEach(function()
            {
                this.skipInfo = {
                    mediaId: 'g23e',
                    mediaType: 'aod',
                    numSkipsBack: 3,
                    numSkipsForward: 2,
                    maxTotalSkips: 5
                } as SkipInfo;

                this.service.skipInfoDictionary$.next({
                    g23e: this.skipInfo
                });
            });

            it('returns false', function()
            {
                expect(this.service.hasSkipsRemaining(this.skipInfo)).toBe(false);
            });
        });

        describe('when back + fwd greater than max', function()
        {
            beforeEach(function()
            {
                this.skipInfo = {
                    mediaId: 'g23e',
                    mediaType: 'aod',
                    numSkipsBack: 3,
                    numSkipsForward: 4,
                    maxTotalSkips: 5
                } as SkipInfo;

                this.service.skipInfoDictionary$.next({
                    g23e: this.skipInfo
                });
            });

            it('returns false', function()
            {
                expect(this.service.hasSkipsRemaining(this.skipInfo)).toBe(false);
            });
        });

        describe('when back + fwd less than max', function()
        {
            beforeEach(function()
            {
                this.skipInfo = {
                    mediaId: 'g23e',
                    mediaType: 'aod',
                    numSkipsBack: 0,
                    numSkipsForward: 3,
                    maxTotalSkips: 5
                } as SkipInfo;

                this.service.skipInfoDictionary$.next({
                    g23e: this.skipInfo
                });
            });

            it('returns false', function()
            {
                expect(this.service.hasSkipsRemaining(this.skipInfo)).toBe(true);
            });
        });
    });


    describe('skipFwdWasSuccessful()', function()
    {
        describe('when skips maxed out', function()
        {
            beforeEach(function()
            {
                this.skipInfo = {
                    mediaId: 'g23e',
                    mediaType: 'aod',
                    numSkipsBack: 0,
                    numSkipsForward: 5,
                    maxTotalSkips: 5
                } as SkipInfo;

                this.service.skipInfoDictionary$.next({
                    g23e: this.skipInfo
                });

                this.dmcaInfoItem = {
                    mediaId: 'g23e',
                    mediaType: 'live',
                    dmcaInfo: {
                        maxBackSkips: 1,
                        maxFwdSkips: 5,
                        maxTotalSkips: 5
                    }
                };

                spyOn(this.service, 'resetSkips');
            });

            it('does not increment numSkipsForward', function()
            {
                this.service.skipFwdWasSuccessful(this.dmcaInfoItem);
                expect(this.service.getSkipInfo(this.dmcaInfoItem).numSkipsForward).toBe(5);
            });

            it('does not set a new skip timestamp', function()
            {
                this.service.getSkipInfo(this.dmcaInfoItem).skipTimestamp = 'old';
                this.service.skipFwdWasSuccessful(this.dmcaInfoItem);
                expect(
                    this.service.getSkipInfo(this.dmcaInfoItem).skipTimestamp
                ).toEqual('old');
            });

            it('does not call resetSkips()', function()
            {
                this.service.skipFwdWasSuccessful(this.dmcaInfoItem);
                expect(this.service.resetSkips).not.toHaveBeenCalled();
            });
        });

        describe('when skips not maxed out', function()
        {
            beforeEach(function()
            {
                this.skipInfo = {
                    mediaId: 'g23e',
                    mediaType: 'live',
                    numSkipsBack: 0,
                    numSkipsForward: 4,
                    maxTotalSkips: 5
                } as SkipInfo;

                this.service.skipInfoDictionary$.next({
                    g23e: this.skipInfo
                });

                this.dmcaInfoItem = {
                    mediaId: 'g23e',
                    mediaType: 'live',
                    dmcaInfo: {
                        maxBackSkips: 1,
                        maxFwdSkips: 5,
                        maxTotalSkips: 5
                    }
                };

                spyOn(this.service, 'resetSkips');
            });

            it('increments numSkipsForward', function()
            {
                this.service.skipFwdWasSuccessful(this.dmcaInfoItem);
                expect(this.service.getSkipInfo(this.dmcaInfoItem).numSkipsForward).toBe(5);
            });

            it('sets a new skipTimestamp', function()
            {
                this.service.getSkipInfo(this.dmcaInfoItem).skipTimestamp = 'old';
                this.service.skipFwdWasSuccessful(this.dmcaInfoItem);
                expect(
                    this.service.getSkipInfo(this.dmcaInfoItem).skipTimestamp
                ).not.toEqual('old');
            });

            it('calls resetSkips()', function()
            {
                this.service.skipFwdWasSuccessful(this.dmcaInfoItem);
                expect(this.service.resetSkips).toHaveBeenCalled();
            });
        });
    });


    describe('findEarliestKey()', function()
    {
        beforeEach(function()
        {
            this.skipInfo1 = {
                mediaId: 'g23e',
                mediaType: 'aod',
                skipTimestamp: 'Mon, 05 Aug 2019 19:17:17 GMT'
            } as SkipInfo;

            this.skipInfo2 = {
                mediaId: 'w34f',
                mediaType: 'live',
                skipTimestamp: 'Mon, 05 Aug 2019 10:17:17 GMT'
            };

            this.service.skipInfoDictionary$.next({
                g23e: this.skipInfo1,
                w34f: this.skipInfo2
            });
        });

        it('returns earliest key', function()
        {
            expect(this.service.findEarliestKey(this.service.skipInfoDictionary$.getValue())).toEqual('w34f');
        });
    });
});
