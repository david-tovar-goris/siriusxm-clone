export const TRACK_TOKEN = "2333a76a-59b8-7551-1989-a4ceeee6cabf";

export const successResponseMock = {
    "ModuleListResponse": {
        "messages": [{
            "messages": "Successful",
            "code": 100
        }],
        "status": 1
    }
};

export const invalidRequestResponseMock = {
    "ModuleListResponse": {
        "messages": [{
            "messages": "Invalid Request",
            "code": 301
        }],
        "status": 0
    }
};
