export const mockSearchResults = {
    "sxmSearch": [
        {
            "searchParams": {
                "channelLineupId": "400",
                "status": 0,
                "totalCount": 316,
                "searchString": "love",
                "start": 0,
                "responseTime": "36",
                "rows": 14
            },
            "searchResultList": {
                "searchResult": [
                    {
                        "assetType": "show",
                        "irNavClassValue": "Unrestricted0",
                        "domain": "AOD",
                        "channelPopularity": "470745.0",
                        "irNavClass": "UNRESTRICTED_0",
                        "searchScore": 7.9781704,
                        "searchAsset": {
                            "show": {
                                "host": [
                                    ""
                                ],
                                "mediumTitle": "Dr Laura",
                                "episodeCount": 3,
                                "channels": [
                                    {
                                        "isMature": true,
                                        "xmChannelNumber": "109",
                                        "channelGuid": "813d656c-9af5-7f51-bb48-d724327d9289",
                                        "isAvailable": false,
                                        "channelLineUp": [

                                        ],
                                        "alternateDescription": "",
                                        "siriusServiceId": "156",
                                        "superCategories": {
                                            "superCategory": [
                                                "Talk"
                                            ],
                                            "categories": {
                                                "category": [
                                                    "Entertainment"
                                                ]
                                            }
                                        },
                                        "streamingName": "",
                                        "xmServiceId": "204",
                                        "isMySxm": false,
                                        "name": "SiriusXM Stars",
                                        "isFavorite": false,
                                        "siriusChannelNumber": "109",
                                        "channelNumber": "109",
                                        "longDescription": "109",
                                        "creativeArts":[],
                                        "mediumDescription": "siriusstars",
                                        "channelId": "siriusstars",
                                        "sortOrder": 0,
                                        "spanishContent": "http://www.siriusxm.com/siriusxmstars",
                                        "url": "http://www.siriusxm.com/siriusxmstars",
                                        "shortDescription": "Jenny McCarthy/Dr Laura/MoreXL",
                                        "isBizMature": true,
                                        "contextualBanner": ""
                                    }
                                ],
                                "futureAiring": [],
                                "disableRecommendations": [],
                                "hostGuid": [
                                    ""
                                ],
                                "showGuid": "5aa081a3-623b-4609-9edd-0fa7e2b4be95",
                                "legacyIds": {
                                    "shortId": "438"
                                },
                                "shortDescription": "Dr. Laura",
                                "longTitle": "Dr. Laura",
                                "longDescription": "One of the most popular talk show hosts in radio history, D.",
                                "creativeArts": [],
                                "programType": "Talk Show",
                                "contextualBanner": "Popular"
                            }
                        }
                    },
                    {
                        "assetType": "episode",
                        "irNavClassValue": "Unrestricted0",
                        "domain": "AOD",
                        "channelPopularity": "470745.0",
                        "irNavClass": "UNRESTRICTED_0",
                        "searchScore": 6.9276576,
                        "searchAsset": {
                            "episode": {
                                "episodeStartTime": "2017-11-02T18:00:00.000+0000",
                                "contentValue": "",
                                "episodeGUID": "b9391071-0682-ab75-5dab-7d88fffdb4db",
                                "show": {
                                    "host": [
                                        ""
                                    ],
                                    "mediumTitle": "Dr Laura",
                                    "episodeCount": 3,
                                    "channels": [
                                        {
                                            "isMature": true,
                                            "xmChannelNumber": "109",
                                            "channelGuid": "813d656c-9af5-7f51-bb48-d724327d9289",
                                            "isAvailable": false,
                                            "channelLineUp": [
                                                100,
                                                5000
                                            ],
                                            "alternateDescription": "",
                                            "siriusServiceId": "156",
                                            "superCategories": {
                                                "superCategory": [
                                                    "Talk"
                                                ],
                                                "categories": {
                                                    "category": [
                                                        "Entertainment"
                                                    ]
                                                }
                                            },
                                            "streamingName": "",
                                            "xmServiceId": "204",
                                            "isMySxm": false,
                                            "name": "SiriusXM Stars",
                                            "isFavorite": false,
                                            "siriusChannelNumber": "109",
                                            "channelNumber": "109",
                                            "longDescription": "df",
                                            "creativeArts": [],
                                            "mediumDescription": "siriusstars",
                                            "channelId": "siriusstars",
                                            "sortOrder": 0,
                                            "spanishContent": "http://www.siriusxm.com/siriusxmstars",
                                            "url": "http://www.siriusxm.com/siriusxmstars",
                                            "shortDescription": "Jenny McCarthy/Dr Laura/MoreXL",
                                            "isBizMature": true,
                                            "contextualBanner": ""
                                        }
                                    ],
                                    "futureAiring": [],
                                    "disableRecommendations": [],
                                    "hostGuid": [
                                        ""
                                    ],
                                    "showGuid": "5aa081a3-623b-4609-9edd-0fa7e2b4be95",
                                    "legacyIds": {
                                        "shortId": "438"
                                    },
                                    "longTitle": "Dr. Laura",
                                    "shortDescription": "Dr. Laura",
                                    "longDescription": "Dr. Laura",
                                    "creativeArts":[],
                                    "programType": "Talk Show",
                                    "contextualBanner": "Popular"
                                },
                                "featured": false,
                                "highlighted": false,
                                "alternateDescription": "",
                                "drmInfo": {
                                    "recordRestrictionDistribution": [],
                                    "recordRestriction": "NONE",
                                    "sharedFlag": false
                                },
                                "channelListRecommendationOverrides": [
                                    ""
                                ],
                                "nowPlayingRecommendationOverrides": [
                                    ""
                                ],
                                "aodEpisodeGUID": "0a98a7a9-a60f-b68e-00c7-1c5355ff31ca",
                                "longDescription": "0a98a7a9-a60f-b68e-00c7-1c5355ff31ca",
                                "episodeScheduleGuid": "null",
                                "sidePanelRecommendationOverrides": [
                                    ""
                                ],
                                "creativeArts": [],
                                "programType": "Talk Show",
                                "dmcaInfo": {
                                    "irNavClass": "UNRESTRICTED_0"
                                },
                                "publicationInfo": {
                                    "creationDate": "2017-11-02T22:12:15.000+0000",
                                    "expiryDate": "2017-11-06T07:59:59.000+0000",
                                    "accessControlIdentifier": "1433a55b-cb00-47d6-90e7-31a83e6dccdb",
                                    "publicationDate": "2017-11-02T22:12:15.000+0000"
                                },
                                "featuredPriority": "",
                                "host": [
                                    ""
                                ],
                                "mediumTitle": "Dr Laura",
                                "percentConsumed": 0,
                                "legacyIds": {
                                    "shortId": "3872703"
                                },
                                "valuable": false,
                                "duration": 8820,
                                "shortDescription": "Adoption, Weddings, and more",
                                "originalAirDate": "2017-11-02T18:00:00.000+0000",
                                "longTitle": "Dr. Laura",
                                "onAirContactInformation": [
                                    ""
                                ],
                                "guest": [
                                    ""
                                ],
                                "hot": false,
                                "special": false,
                                "contextualBanner": ""
                            }
                        }
                    },
                    {
                        "assetType": "channel",
                        "irNavClassValue": "Restricted2",
                        "domain": "AOD",
                        "channelPopularity": "928546.0",
                        "irNavClass": "RESTRICTED_2",
                        "searchScore": 2.2731113,
                        "searchAsset": {
                            "channel": {
                                "isMature": false,
                                "xmChannelNumber": "2",
                                "channelGuid": "194adbca-34d6-cb94-b153-3488ee563308",
                                "isAvailable": false,
                                "channelLineUp": [

                                ],
                                "alternateDescription": "",
                                "siriusServiceId": "2",
                                "superCategories": {
                                    "superCategory": [
                                        "Music"
                                    ],
                                    "categories": {
                                        "category": [
                                            "Pop"
                                        ]
                                    }
                                },
                                "streamingName": "",
                                "xmServiceId": "13",
                                "isMySxm": true,
                                "name": "SiriusXM Hits 1",
                                "isFavorite": false,
                                "siriusChannelNumber": "2",
                                "channelNumber": "2",
                                "longDescription": "From pop to hip-hop and rock to R&B, SiriusXM Hits 1 plays The Most Hit Music!",
                                "creativeArts": [],
                                "mediumDescription": "siriushits1",
                                "channelId": "siriushits1",
                                "sortOrder": 0,
                                "spanishContent": "http://www.siriusxm.com/siriusxmhits1",
                                "url": "http://www.siriusxm.com/siriusxmhits1",
                                "shortDescription": "Today's Pop Hits",
                                "isBizMature": false,
                                "contextualBanner": "Staff Pick"
                            }
                        }
                    }

                ]
            }
        }
    ]
};

export const mockAutoCompleteSearchResults = {
    "sxmSearch": [
        {
            "searchParams": {
                "channelLineupId": "400",
                "status": 0,
                "totalCount": 31,
                "searchString": "love",
                "start": 0,
                "responseTime": "0",
                "rows": 15
            },
            "searchResultList": {
                "searchResult": [
                    {
                        "suggestion": "love songs"
                    },
                    {
                        "suggestion": "lovely"
                    }
                ]
            }
        }
    ]
};

export const mockRecentSearchResults = {
    "profileData": {
        "searchList": {
            "count": 3,
            "lastItem": "howard:2017-10-23T15:39:09.455Z",
            "searches": [
                {
                    "searchString": "howard",
                    "searchDateTime": "2017-10-23T15:39:09.455Z"
                },
                {
                    "searchString": "pop",
                    "searchDateTime": "2017-10-23T14:09:27.471Z"
                },
                {
                    "searchString": "h",
                    "searchDateTime": "2017-10-20T19:28:48.776Z"
                }
            ]
        }
    }
};
