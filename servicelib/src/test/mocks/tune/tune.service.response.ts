import {
    connectInfo,
    times,
    zuluEndTime0,
    zuluEndTime1,
    zuluEndTime2,
    zuluStartTime0,
    zuluStartTime1,
    zuluStartTime2
} from "./tune.delegate.response";
import { ContentTypes } from "../../../service/types/content.types";

export let liveCuePoint =
               {
                   assetGUID: 'null_INSTANTANEOUS',
                   event    : 'INSTANTANEOUS',
                   times    : times
               };


export let cuts = [
    {
        assetGUID      : "11-a81ef7f1-4ea2-8517-518c-f0a574f93f91",
        consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f91,channelId=8254,sir",
        duration       : 300,
        title          : "cutTitle1",
        legacyId       : { pid: "testPid" },
        times          : {
            zuluStartTime: zuluStartTime0,
            isoStartTime: "2017-09-22T15:42:35.000+00:00",
            zuluEndTime: zuluEndTime0,
            isoEndTime: "2017-09-22T15:49:01.000Z",
            zeroStartTime: 0,
            zeroEndTime: 0
        },
        cutContentType : "song",
        contentType : "song"
    },
    {
        assetGUID      : "11-a81ef7f1-4ea2-8517-518c-f0a574f93f92",
        consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f92,channelId=8254,sir",
        legacyId       : {
            siriusXMId: "1b1742d3"
        },
        title          : "First Take1",
        artists        : [ { name: "First Take1" } ],
        album          : {},
        duration       : 400,
        times          : {
            zuluStartTime: zuluStartTime1,
            isoStartTime: "2017-09-22T15:42:35.000+00:00",
            zuluEndTime: zuluEndTime1,
            isoEndTime: "2017-09-22T15:49:01.000Z",
            zeroStartTime: 0,
            zeroEndTime: 0
        },
        cutContentType : "song",
        contentType : "song"
    },
    {
        assetGUID      : "22-a81ef7f1-4ea2-8517-518c-f0a574f93f93",
        consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f93,channelId=8254,sir",
        title          : "First Take2",
        artists        : [ { name: "First Take2" } ],
        album          : {},
        duration       : 500,
        legacyId       : { siriusXMId: "1b1742d3" },
        times          : {
            zuluStartTime: zuluStartTime2,
            isoStartTime: "2017-09-22T15:42:35.000+00:00",
            zuluEndTime: zuluEndTime2,
            isoEndTime: "2017-09-22T15:49:01.000Z",
            zeroStartTime: 0,
            zeroEndTime: 0
        },
        cutContentType : "ext",
        contentType : "song"
    }
];

export let segments = [
    {
        assetGUID       : "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        duration        : 386,
        legacyId        : {
            pid: 1
        },
        type            : "SOFT",
        times           : times,
        longDescription : "TestLongDescription1",
        shortDescription: "TestShortDescription1"
    },
    {
        assetGUID       : "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        duration        : 386,
        type            : "SOFT",
        times           : times,
        longDescription : "TestLongDescription2",
        shortDescription: "TestShortDescription1"
    }
];

export let episodes = [
    {
        assetGUID         : "a8dc4ab1-efff-e97e-430d-543716af311f",
        duration          : 7200,
        legacyId          : { shortId: "3811191" },
        dmcaInfo          : {
            irNavClass        : "UNRESTRICTED_0",
            playOnSelect      : "REALTIME_6",
            channelContentType: "T_3",
            maxFwdSkips       : 5,
            backSkipDur       : 1,
            maxBackSkips      : 1,
            fwdSkipDur        : 1,
            maxSkipDur        : 1,
            maxTotalSkips     : 1
        },
        drmInfo           : {
            recordRestriction: false,
            sharedFlag       : false
        },
        highlighted       : false,
        hot               : false,
        longDescription   : "Long Description",
        longTitle         : "Long Title",
        mediumTitle       : "Medium Title",
        originalIsoAirDate:
            "2017-09-22T14:00:00.000+0000",
        shortDescription  : "Short Description",
        show              : {
            assetGUID             : "assetGuid",
            mediumTitle           : "First Take",
            longTitle             : "First Take",
            aodEpisodeCount       : 1,
            disableRecommendations: false,
            futureAirings         : "future Airings",
            images                : [],
            legacyId              : {},
            longDescription       : "TestLongDescription",
            shortDescription      : "TestShotDescription",
            times                 :
                {
                    zuluStartTime: 1506088800000,
                    isoStartTime : '2017-09-22T14:00:00.000+0000',
                    zuluEndTime  : 0,
                    isoEndTime   : '',
                    zeroStartTime: 0,
                    zeroEndTime: 0
                }
        },
        valuable          : false,
        times             :
            {
                zuluStartTime: 1506088800000,
                isoStartTime : '2017-09-22T14:00:00.000+0000',
                zuluEndTime  : 0,
                isoEndTime   : '',
                zeroStartTime: 0,
                zeroEndTime: 0
            }
    }
];

export let shows = [
    {
        assetGUID             : "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        duration              : 386,
        times                 :
            {
                zuluStartTime: 1506094955000,
                isoStartTime : '2017-09-22T15:42:35.000+0000',
                zuluEndTime  : 0,
                isoEndTime   : '',
                zeroStartTime: 0,
                zeroEndTime: 0
            },
        aodEpisodeCount       : 1,
        connectInfo           : {},
        disableRecommendations: false,
        futureAirings         : "future airings",
        images                : []
    }
];

const livePrimaryChunks = [
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:35.782+0000",
        url             : "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274735782_00222665_v3.aac"

    },
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:45.535+0000",
        url             : "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274745535_00222666_v3.aac"
    }
];

let liveSecondaryChunks = [
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:35.782+0000",
        url             : "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274735782_00222665_v3.aac"

    },
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:45.535+0000",
        url             : "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274745535_00222666_v3.aac"
    }
];

const aodPrimaryChunks = [
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%OnDemand_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:35.782+0000",
        url             : "%OnDemand_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274735782_00222665_v3.aac"

    },
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%OnDemand_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:45.535+0000",
        url             : "%OnDemand_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274745535_00222666_v3.aac"
    }
];

const vodPrimaryChunks = [
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%OnDemand_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:35.782+0000",
        url             : "%OnDemand_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274735782_00222665_v3.aac"

    },
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%OnDemand_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:45.535+0000",
        url             : "%OnDemand_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274745535_00222666_v3.aac"
    }
];

let aodSecondaryChunks = [
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%OnDemand_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:35.782+0000",
        url             : "%OnDemand_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274735782_00222665_v3.aac"

    },
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%OnDemand_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:45.535+0000",
        url             : "%OnDemand_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274745535_00222666_v3.aac"
    }
];

let vodSecondaryChunks = [
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%OnDemand_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:35.782+0000",
        url             : "%OnDemand_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274735782_00222665_v3.aac"

    },
    {
        key             : "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl          : "%OnDemand_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp    : "2017-09-22T20:45:45.535+0000",
        url             : "%OnDemand_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274745535_00222666_v3.aac"
    }
];

export const liveAudioInfoPrimary =
                 {
                     name: "primary",
                     size: "SMALL",
                     url : "%Live_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8"
                 };

export const liveAudioInfoSecondary =
                 {
                     name: "secondary",
                     size: "SMALL",
                     url : "%Live_Secondary_HLS%/AAC_Data/8254/8254_variant_medium_v3.m3u8"
                 };

export const aodAudioInfoPrimary =
                 {
                     name: "primary",
                     size: "SMALL",
                     url : "%OnDemand_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8"
                 };

export const vodAudioInfoPrimary =
                 {
                     name: "primary",
                     size: "SMALL",
                     url : "%OnDemand_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8"
                 };

export const aodAudioInfoSecondary =
                 {
                     name: "secondary",
                     size: "SMALL",
                     url : "%OnDemand_Secondary_HLS%/AAC_Data/8254/8254_variant_medium_v3.m3u8"
                 };

export const vodAudioInfoSecondary =
                 {
                     name: "secondary",
                     size: "SMALL",
                     url : "%OnDemand_Secondary_HLS%/AAC_Data/8254/8254_variant_medium_v3.m3u8"
                 };

export const liveMediaEndPoints = [
    {
        name            : 'primary',
        size            : 'SMALL',
        url             : '%Live_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8',
        position        : {
            isoTimestamp: '2017-09-22T20:45:35.782+0000',
            positionType: 'testPosition'
        },
        mediaFirstChunks: livePrimaryChunks,
        manifestFiles   : [ liveAudioInfoPrimary ]
    },
    {
        name            : 'secondary',
        size            : 'SMALL',
        url             : '%Live_Secondary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8',
        position        : {
            isoTimestamp: '2017-09-22T20:45:35.782+0000',
            positionType: 'testPosition'
        },
        mediaFirstChunks: liveSecondaryChunks,
        manifestFiles   : [ liveAudioInfoSecondary ]
    }
];

export const aodMediaEndPoints = [
    {
        name            : 'primary',
        size            : 'LARGE',
        url             : '%OnDemand_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8',
        position        : {
            isoTimestamp: '2017-09-22T20:45:35.782+0000',
            positionType: 'testPosition'
        },
        mediaFirstChunks: aodPrimaryChunks,
        manifestFiles   : [ aodAudioInfoPrimary ]
    },
    {
        name            : 'secondary',
        size            : 'LARGE',
        url             : '%OnDemand_Secondary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8',
        position        : {
            isoTimestamp: '2017-09-22T20:45:35.782+0000',
            positionType: 'testPosition'
        },
        mediaFirstChunks: aodSecondaryChunks,
        manifestFiles   : [ aodAudioInfoSecondary ]
    }
];

export const vodMediaEndPoints = [
    {
        name            : 'primary',
        size            : 'LARGE',
        url             : '%OnDemand_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8',
        position        : {
            isoTimestamp: '2017-09-22T20:45:35.782+0000',
            positionType: 'testPosition'
        },
        mediaFirstChunks: vodPrimaryChunks,
        manifestFiles   : [ vodAudioInfoPrimary ]
    },
    {
        name            : 'secondary',
        size            : 'LARGE',
        url             : '%OnDemand_Secondary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8',
        position        : {
            isoTimestamp: '2017-09-22T20:45:35.782+0000',
            positionType: 'testPosition'
        },
        mediaFirstChunks: vodSecondaryChunks,
        manifestFiles   : [ vodAudioInfoSecondary ]
    }
];

export let mediaTimeLineLiveMock =
               {
                   mediaType           : ContentTypes.LIVE_AUDIO,
                   mediaId             : "liveId",
                   connectInfo         : connectInfo,
                   liveCuePoint        : liveCuePoint,
                   cuts                : cuts,
                   segments            : segments,
                   episodes            : episodes,
                   futureEpisodes      : episodes,
                   shows               : shows,
                   scoreUpdates        : [],
                   mediaEndPoints      : liveMediaEndPoints,
                   hlsConsumptionInfo  : "",
                   aodEpisodeCount     : 10,
                   isDataComeFromResume: true
               };

export let mediaTimeLineAodMock =
               {
                   mediaType           : ContentTypes.AOD,
                   mediaId             : "aodId",
                   connectInfo         : connectInfo,
                   liveCuePoint        : liveCuePoint,
                   cuts                : cuts,
                   segments            : segments,
                   episodes            : episodes,
                   futureEpisodes      : episodes,
                   shows               : shows,
                   scoreUpdates        : [],
                   mediaEndPoints      : aodMediaEndPoints,
                   hlsConsumptionInfo  : "",
                   aodEpisodeCount     : 10,
                   isDataComeFromResume: false
               };

export let mediaTimeLineVodMock =
               {
                   mediaType           : ContentTypes.VOD,
                   mediaId             : "vodId",
                   connectInfo         : connectInfo,
                   liveCuePoint        : liveCuePoint,
                   cuts                : cuts,
                   segments            : segments,
                   episodes            : episodes,
                   futureEpisodes      : episodes,
                   shows               : shows,
                   scoreUpdates        : [],
                   mediaEndPoints      : vodMediaEndPoints,
                   hlsConsumptionInfo  : "",
                   isDataComeFromResume: false
               };

export let liveTuneResponseMock =
               {
                   updateFrequency     : 100,
                   mediaType           : ContentTypes.LIVE_AUDIO,
                   mediaId             : "liveId",
                   connectInfo         : connectInfo,
                   liveCuePoint        : liveCuePoint,
                   cuts                : cuts,
                   segments            : segments,
                   episodes            : episodes,
                   futureEpisodes      : episodes,
                   shows               : shows,
                   scoreUpdates        : [],
                   mediaEndPoints      : liveMediaEndPoints,
                   hlsConsumptionInfo  : "",
                   aodEpisodeCount     : 10,
                   isDataComeFromResume: true
               };


export let aodTuneResponseMock =
               {
                   updateFrequency     : 100,
                   mediaType           : ContentTypes.AOD,
                   mediaId             : "aodId",
                   connectInfo         : connectInfo,
                   liveCuePoint        : liveCuePoint,
                   cuts                : cuts,
                   segments            : segments,
                   episodes            : episodes,
                   futureEpisodes      : episodes,
                   shows               : shows,
                   scoreUpdates        : [],
                   mediaEndPoints      : aodMediaEndPoints,
                   hlsConsumptionInfo  : "",
                   aodEpisodeCount     : 10,
                   isDataComeFromResume: false
               };

export let vodTuneResponseMock =
               {
                   updateFrequency     : 100,
                   mediaType           : ContentTypes.VOD,
                   mediaId             : "vodId",
                   connectInfo         : connectInfo,
                   liveCuePoint        : liveCuePoint,
                   cuts                : cuts,
                   segments            : segments,
                   episodes            : episodes,
                   futureEpisodes      : episodes,
                   shows               : shows,
                   scoreUpdates        : [],
                   mediaEndPoints      : vodMediaEndPoints,
                   hlsConsumptionInfo  : "",
                   isDataComeFromResume: false
               };

