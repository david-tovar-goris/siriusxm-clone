import {ActionTypes, ContentTypes} from "../../../service/types";
import { ApiLayerTypes } from "../../../index";

export const hlsConsumptionInfoLive = "?consumer=Live";
export const hlsConsumptionInfoAod = "?consumer=Aod";
export const hlsConsumptionInfoVod = "?consumer=Vod";
export const hlsConsumptionInfoAic = "?consumer=Aic";
export const duration = 386;
const zuluTimeEnd = 1506094955000;
const zuluTimeStart = 1506088800000;
const liveAbsoluteTime = "2017-09-22T20:45:35.782+0000";
const normalizedLiveAbsTime = "2017-09-22T20:45:35.782+00:00";
const cuePointAbsoluteTime = "2017-09-22T14:00:00.000+0000";
const siriusXMLegacyId = "1b1742d3";

export let mockClientConfigurationResponse = {
    openAccessPeriodState: false
};

export const connectInfo =
    {
        email: "testEmail",
        facebook: "testFacebook",
        phone: "testPhone",
        twitter: "twitter"
    };

export const liveCuePointFromAPI = {
    active: true,
    assetGUID: "null_INSTANTANEOUS",
    event: "INSTANTANEOUS",
    layer: "livepoint",
    time: 1506113135782,
    timestamp: { absolute: liveAbsoluteTime }
};

export const cuePoints = [
    {
        assetGUID: "cuePoint1AssetGuid",
        event: "START",
        layer: "show",
        markerGuid: "cuePoint1MarkerGuid",
        time: zuluTimeStart,
        timestamp: { absolute: cuePointAbsoluteTime }
    },
    liveCuePointFromAPI
];

export const livePointTimes =
    {
        zuluStartTime: liveCuePointFromAPI.time,
        isoStartTime: normalizedLiveAbsTime,
        zuluEndTime: undefined,
        isoEndTime: "",
        zeroEndTime: undefined,
        zeroStartTime: undefined
    };

export const zuluStartTime0 = zuluTimeEnd;
export const zuluEndTime0 =   zuluTimeEnd + (duration * 1000) - 1;
export const zuluStartTime1 = zuluEndTime0 + 1;
export const zuluEndTime1 = zuluStartTime1 + 10000 - 1;
export const zuluStartTime2 = zuluEndTime1 + 1;
export const zuluEndTime2 = zuluStartTime2 + 1000 - 1;

export const times =
    {
        zuluStartTime: zuluStartTime0,
        isoStartTime: new Date(zuluStartTime0).toISOString(),
        zuluEndTime: zuluEndTime0,
        isoEndTime: new Date(zuluEndTime0).toISOString(),
        zeroEndTime: undefined,
        zeroStartTime: undefined
    };

export const cutMarkers = [
    {
        assetGUID: "cutMarkerAssetGuid1",
        consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f92,channelId=8254,sir",
        containerGUID: "containerGuid1",
        duration: duration,
        time: zuluTimeEnd,
        layer: "cut",
        timestamp:
            {
                absolute: "2017-09-22T15:42:35.000+0000"
            }
    },
    {
        assetGUID: "cutMarkerAssetGuid2",
        consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f92,channelId=8254,sir",
        containerGUID: "containerGuid2",
        cut: {
            legacyIds: { siriusXMId: siriusXMLegacyId },
            title: "First Take1",
            artists: [{ name: "First Take1" }],
            album: {},
            cutContentType: "song"
        },
        duration: duration,
        time: zuluTimeEnd,
        timestamp:
            {
                absolute: "2017-09-22T15:42:35.000+0000"
            }
    },
    {
        assetGUID: "cutMarkerAssetGuid3",
        consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f92,channelId=8254,sir",
        containerGUID: "containerGuid3",
        cut: {
            title: "First Take2",
            artists: [{ name: "First Take2" }]
        },
        duration: duration,
        time: zuluTimeEnd,
        timestamp:
            {
                absolute: "2017-09-22T15:42:35.000+0000"
            }
    },
    {
        assetGUID: "cutMarkerAssetGuid3",
        consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f92,channelId=8254,sir",
        containerGUID: "containerGuid3",
        cut: {
            title: "First Take2",
            artists: [{ name: "First Take2" }],
            legacyIds: { pid: "^I" }
        },
        duration: 20,
        time: zuluTimeEnd,
        timestamp:
            {
                absolute: "2017-09-22T15:42:35.000+0000"
            }
    },
    {
        assetGUID: "cutMarkerAssetGuid2",
        consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f92,channelId=8254,sir",
        containerGUID: "containerGuid2",
        cut: {
            legacyIds: { pid: "$O" },
            title: "First Take1",
            album: {}
         },
        duration: duration,
        time: zuluTimeEnd,
        timestamp:
            {
                absolute: "2017-09-22T15:42:35.000+0000"
            }
    },
    {
        assetGUID: "cutMarkerAssetGuid5",
        consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f92,channelId=8254,sir",
        containerGUID: "containerGuid5",
        cut: {
            legacyIds: {  pid: "$O" },
            title: "First Take1",
            artists: [{ name: "First Take1" }],
            album: {},
            cutContentType: "Song"
        },
        duration: duration,
        time: zuluTimeEnd,
        timestamp:
            {
                absolute: "2017-09-22T15:42:35.000+0000"
            }
    }];

export let normalizedCuts = [
    {
        assetGUID: cutMarkers[0].assetGUID,
        consumptionInfo: cutMarkers[0].consumptionInfo,
        duration: cutMarkers[0].duration,
        times: times,
        cutContentType: undefined,
        contentType: "ext"
    },
    {
        assetGUID: cutMarkers[1].assetGUID,
        consumptionInfo: cutMarkers[1].consumptionInfo,
        legacyId: {
            siriusXMId: siriusXMLegacyId,
            pid: undefined,
            shortId: undefined
        },
        title: "First Take1",
        artists: [{ name: "First Take1" }],
        album: {},
        duration: 386,
        times: times,
        cutContentType: "song",
        contentType: "song"
    },
    {
        assetGUID: cutMarkers[2].assetGUID,
        consumptionInfo: cutMarkers[2].consumptionInfo,
        title: "First Take2",
        artists: [{ name: "First Take2" }],
        duration: 386,
        times: times,
        cutContentType: undefined,
        contentType: "ext"
    },
    {
        assetGUID: cutMarkers[2].assetGUID,
        consumptionInfo: cutMarkers[2].consumptionInfo,
        title: "First Take2",
        artists: [{ name: "First Take2" }],
        duration: 20,
        times: {
            zuluStartTime: 1506094955000,
            isoStartTime: "2017-09-22T15:42:35.000+00:00",
            zuluEndTime: 1506094975000,
            isoEndTime: "2017-09-22T15:42:55.000Z",
            zeroEndTime: undefined,
            zeroStartTime: undefined
        },
        legacyId: {
            siriusXMId: undefined,
            pid: "^I",
            shortId: undefined
        },
        cutContentType: undefined,
        contentType: "ext"
    },
    {
        assetGUID: cutMarkers[1].assetGUID,
        consumptionInfo: cutMarkers[1].consumptionInfo,
        legacyId: {
            siriusXMId: undefined,
            pid: "$O",
            shortId: undefined
        },
        title: "First Take1",
        album: {},
        duration: 386,
        times: times,
        cutContentType: undefined,
        contentType: "song"
    },
    {
        assetGUID: cutMarkers[5].assetGUID,
        consumptionInfo: cutMarkers[5].consumptionInfo,
        legacyId: {
            siriusXMId: undefined,
            pid: "$O",
            shortId: undefined
        },
        title: "First Take1",
        artists: [{ name: "First Take1" }],
        album: {},
        duration: 386,
        times: times,
        cutContentType: "Song",
        contentType: "song"
    }
];

export const episodeMarkers = [
    {
        assetGUID: "a8dc4ab1-efff-e97e-430d-543716af311f",
        layer: "episode",
        time: 1506088800000,
        duration: 7200,
        episode: {
            legacyIds: { shortId: "3811191" },
            dmcaInfo: {
                irNavClass: "UNRESTRICTED_0",
                playOnSelect: "REALTIME_6",
                channelContentType: "T_3",
                maxFwdSkips: 5
            },
            drmInfo: { sharedFlag: false },
            episodeGUID: "a8dc4ab1-efff-e97e-430d-543716af311f",
            highlighted: false,
            hot: false,
            longDescription: "Long Description",
            longTitle: "Long Title",
            mediumTitle: "Medium Title",
            originalAirDate:
                "2017-09-22T14:00:00.000+0000",
            shortDescription: "Short Description",
            show: {
                mediumTitle: "First Take",
                longTitle: "First Take"
            }
        },
        valuable: false,
        timestamp: { absolute: "2017-09-22T14:00:00.000+0000" }
    },
    {
        assetGUID: "1a8dc4ab1-efff-e97e-430d-543716af311f",
        layer: "episode",
        time: 1506088800000,
        duration: 720,
        valuable: false,
        timestamp: { absolute: "2017-09-22T14:00:00.000+0000" },
        episode: {
            show: {}
        }
    },
    {
        assetGUID: "a8dc4ab1-efff-e97e-430d-543716af311f",
        layer: "episode",
        time: 1506088800000,
        duration: 7200,
        episode: {
            legacyIds: { shortId: "3811191" },
            dmcaInfo: {
                irNavClass: "UNRESTRICTED_0",
                playOnSelect: "REALTIME_6",
                channelContentType: "T_3",
                maxFwdSkips: 5
            },
            drmInfo: { sharedFlag: false },
            episodeGUID: "a8dc4ab1-efff-e97e-430d-543716af311f",
            highlighted: false,
            hot: false,
            longDescription: "Long Description",
            longTitle: "Long Title",
            mediumTitle: "Medium Title",
            originalAirDate:
                "2017-09-22T14:00:00.000+0000",
            shortDescription: "Short Description",
            show: {}
        },
        valuable: false
    },
    {
        assetGUID: "50cc07dc-29db-1fe9-b72e-e7dde92f326f",
        layer: "episode",
        time: 1522355400000,
        timestamp: { absolute: "2018-03-29T20:30:00.000+0000" },
        duration: 9000,
        episode: {
            legacyIds: { shortId: "11389001" },
            dmcaInfo: {
                irNavClass: "RESTRICTED_2",
                playOnSelect: "NEWEST_0",
                channelContentType: "T_2",
                maxFwdSkips: 5
            },
            drmInfo: { sharedFlag: false },
            episodeGUID: "50cc07dc-29db-1fe9-b72e-e7dde92f326f",
            highlighted: false,
            hot: false,
            longDescription: "The Beatles were.........",
            longTitle: "The Beatles Channel",
            mediumTitle: "BeatlesChannel",
            originalAirDate:
                "2018-03-30T00:30:00.000+0000",
            shortDescription: "The Beatles 24/8!",
            show: { isPlaceholderShow: true }
        },
        valuable: false
    }

];

export let normalizedEpisodeMarkers = [
    {
        assetGUID: "a8dc4ab1-efff-e97e-430d-543716af311f",
        duration: 7200,
        legacyId: {
            shortId: "3811191",
            pid: undefined,
            siriusXMId: undefined
        },
        dmcaInfo: {
            irNavClass: "UNRESTRICTED_0",
            playOnSelect: "REALTIME_6",
            channelContentType: "T_3",
            maxFwdSkips: 5
        },
        drmInfo: {
            sharedFlag: false
        },
        highlighted: false,
        hot: false,
        longDescription: "Long Description",
        longTitle: "Long Title",
        mediumTitle: "Medium Title",
        originalIsoAirDate:
            "2017-09-22T14:00:00.000+0000",
        shortDescription: "Short Description",
        show: {
            assetGUID: undefined,
            mediumTitle: "First Take",
            longTitle: "First Take",
            aodEpisodeCount: undefined,
            connectInfo: undefined,
            disableRecommendations: undefined,
            futureAirings: undefined,
            images: undefined,
            legacyId: { siriusXMId: '', pid: '', shortId: '' },
            longDescription: undefined,
            shortDescription: undefined,
            isPlaceholderShow: undefined,
            isLiveVideoEligible:undefined,
            type: 'show',
            channelId: '123'
        },
        valuable: false,
        times:
            {
                zuluStartTime: 1506088800000,
                isoStartTime: '2017-09-22T14:00:00.000+00:00',
                zuluEndTime: 1506096000000,
                isoEndTime: '2017-09-22T16:00:00.000Z',
                zeroEndTime: undefined,
                zeroStartTime: undefined
            },
        images: []
    },
    {
        assetGUID: "1a8dc4ab1-efff-e97e-430d-543716af311f",
        duration: 720,
        valuable: false,
        originalIsoAirDate: undefined,
        dmcaInfo: {
            irNavClass: "UNRESTRICTED"
        },
        show: {
            assetGUID: undefined,
            mediumTitle: undefined,
            longTitle: undefined,
            aodEpisodeCount: undefined,
            connectInfo: undefined,
            disableRecommendations: undefined,
            futureAirings: undefined,
            images: undefined,
            legacyId: { siriusXMId: '', pid: '', shortId: '' },
            longDescription: undefined,
            shortDescription: undefined,
            isPlaceholderShow: undefined,
            isLiveVideoEligible:undefined,
            type: 'show',
            channelId: '123'
        },
        times:
            {
                zuluStartTime: 1506088800000,
                isoStartTime: '2017-09-22T14:00:00.000+00:00',
                zuluEndTime: 1506089520000,
                isoEndTime: '2017-09-22T14:12:00.000Z',
                zeroEndTime: undefined,
                zeroStartTime: undefined
            },
        images: []
    },
    {
        assetGUID: "a8dc4ab1-efff-e97e-430d-543716af311f",
        duration: 7200,
        legacyId: { siriusXMId: undefined, pid: undefined, shortId: "3811191" },
        dmcaInfo: {
            irNavClass: "UNRESTRICTED_0",
            playOnSelect: "REALTIME_6",
            channelContentType: "T_3",
            maxFwdSkips: 5
        },
        drmInfo: {
            sharedFlag: false
        },
        highlighted: false,
        hot: false,
        longDescription: "Long Description",
        longTitle: "Long Title",
        mediumTitle: "Medium Title",
        originalIsoAirDate:
            "2017-09-22T14:00:00.000+0000",
        shortDescription: "Short Description",
        valuable: false,
        show: {
            assetGUID: undefined,
            mediumTitle: undefined,
            longTitle: undefined,
            aodEpisodeCount: undefined,
            connectInfo: undefined,
            disableRecommendations: undefined,
            futureAirings: undefined,
            images: undefined,
            legacyId: { siriusXMId: '', pid: '', shortId: '' },
            longDescription: undefined,
            shortDescription: undefined,
            isPlaceholderShow: undefined,
            isLiveVideoEligible:undefined,
            type: 'show',
            channelId: '123'
        },
        times:
            {
                zuluStartTime: NaN,
                isoStartTime: '',
                zuluEndTime: undefined,
                isoEndTime: '',
                zeroEndTime: undefined,
                zeroStartTime: undefined
            },
        images: []
    },
    {
        assetGUID: "50cc07dc-29db-1fe9-b72e-e7dde92f326f",
        duration: 3600,
        legacyId: { siriusXMId: undefined, pid: undefined, shortId: "11389001" },
        dmcaInfo: {
            irNavClass: "RESTRICTED_2",
            playOnSelect: "NEWEST_0",
            channelContentType: "T_2",
            maxFwdSkips: 5
        },
        drmInfo: {
            sharedFlag: false
        },
        highlighted: false,
        hot: false,
        longDescription: "The Beatles were.........",
        longTitle: "The Beatles Channel",
        mediumTitle: "BeatlesChannel",
        originalIsoAirDate:
            "2018-03-30T00:30:00.000+0000",
        shortDescription: "The Beatles 24/8!",
        valuable: false,
        show: {
            assetGUID: undefined,
            mediumTitle: undefined,
            longTitle: undefined,
            aodEpisodeCount: undefined,
            connectInfo: undefined,
            disableRecommendations: undefined,
            futureAirings: undefined,
            images: undefined,
            legacyId: { siriusXMId: '', pid: '', shortId: '' },
            longDescription: undefined,
            shortDescription: undefined,
            isPlaceholderShow: true,
            isLiveVideoEligible:false,
            type: 'show',
            channelId: '123'
        },
        times:
            {
                zuluStartTime: 1522355400000,
                isoStartTime: '2018-03-29T20:30:00.000Z',
                zuluEndTime: 1522359000000,
                isoEndTime: '2018-03-29T21:30:00.000Z',
                zeroEndTime: undefined,
                zeroStartTime: undefined
            },
        images: []
    },
    {
        assetGUID: "50cc07dc-29db-1fe9-b72e-e7dde92f326f",
        duration: 3600,
        legacyId: { siriusXMId: undefined, pid: undefined, shortId: "11389001" },
        dmcaInfo: {
            irNavClass: "RESTRICTED_2",
            playOnSelect: "NEWEST_0",
            channelContentType: "T_2",
            maxFwdSkips: 5
        },
        drmInfo: {
            sharedFlag: false
        },
        highlighted: false,
        hot: false,
        longDescription: "The Beatles were.........",
        longTitle: "The Beatles Channel",
        mediumTitle: "BeatlesChannel",
        originalIsoAirDate:
            "2018-03-30T00:30:00.000+0000",
        shortDescription: "The Beatles 24/8!",
        valuable: false,
        show: {
            assetGUID: undefined,
            mediumTitle: undefined,
            longTitle: undefined,
            aodEpisodeCount: undefined,
            connectInfo: undefined,
            disableRecommendations: undefined,
            futureAirings: undefined,
            images: undefined,
            legacyId: { siriusXMId: '', pid: '', shortId: '' },
            longDescription: undefined,
            shortDescription: undefined,
            isPlaceholderShow: true,
            isLiveVideoEligible:false,
            type: 'show',
            channelId: '123'
        },
        times:
            {
                zuluStartTime: 1522359000000,
                isoStartTime: '2018-03-29T21:30:00.000Z',
                zuluEndTime: 1522362600000,
                isoEndTime: '2018-03-29T22:30:00.000Z',
                zeroEndTime: undefined,
                zeroStartTime: undefined
            },
        images: []
    },
    {
        assetGUID: "50cc07dc-29db-1fe9-b72e-e7dde92f326f",
        duration: 1800,
        legacyId: { siriusXMId: undefined, pid: undefined, shortId: "11389001" },
        dmcaInfo: {
            irNavClass: "RESTRICTED_2",
            playOnSelect: "NEWEST_0",
            channelContentType: "T_2",
            maxFwdSkips: 5
        },
        drmInfo: {
            sharedFlag: false
        },
        highlighted: false,
        hot: false,
        longDescription: "The Beatles were.........",
        longTitle: "The Beatles Channel",
        mediumTitle: "BeatlesChannel",
        originalIsoAirDate:
            "2018-03-30T00:30:00.000+0000",
        shortDescription: "The Beatles 24/8!",
        valuable: false,
        show: {
            assetGUID: undefined,
            mediumTitle: undefined,
            longTitle: undefined,
            aodEpisodeCount: undefined,
            connectInfo: undefined,
            disableRecommendations: undefined,
            futureAirings: undefined,
            images: undefined,
            legacyId: { siriusXMId: '', pid: '', shortId: '' },
            longDescription: undefined,
            shortDescription: undefined,
            isPlaceholderShow: true,
            isLiveVideoEligible:false,
            type: 'show',
            channelId: '123'
        },
        times:
            {
                zuluStartTime: 1522362600000,
                isoStartTime: '2018-03-29T22:30:00.000Z',
                zuluEndTime: 1522364400000,
                isoEndTime: '2018-03-29T23:00:00.000Z',
                zeroEndTime: undefined,
                zeroStartTime: undefined
            },
        images: []
    }
];

export const segmentMarkersFromApi = [
    {
        assetGUID: "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        containerGUID: "e253bdca-601f-2b68-52fc-3f6a9fa33bb9",
        duration: 386,
        layer: "segment",
        segment: { legacyIds: {}, segmentType: "SOFT" },
        legacyIds: {},
        segmentType: "SOFT",
        time: 1506094955000,
        timestamp: { absolute: "2017-09-22T15:42:35.000+0000" }
    },
    {
        assetGUID: "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        containerGUID: "e253bdca-601f-2b68-52fc-3f6a9fa33bb9",
        duration: 386,
        layer: "NO segment",
        segmentType: "SOFT",
        time: 1506094955000,
        timestamp: { absolute: "2017-09-22T15:42:35.000+0000" }
    }];

export let normalizedSegmentMarkers = [
    {
        assetGUID: "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        duration: 386,
        legacyId: {
            pid: undefined,
            shortId: undefined,
            siriusXMId: undefined
        },
        type: "SOFT",
        times: times
    },
    {
        assetGUID: "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        duration: 386,
        type: "SOFT",
        times: times
    }];

export const showMarkers = [
    {
        assetGUID: "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        containerGUID: "e253bdca-601f-2b68-52fc-3f6a9fa33bb9",
        duration: 386,
        layer: "show",
        segmentType: "SOFT",
        time: 1506094955000,
        timestamp: { absolute: "2017-09-22T15:42:35.000+0000" }
    },
    {
        assetGUID: "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        containerGUID: "e253bdca-601f-2b68-52fc-3f6a9fa33bb9",
        duration: 386,
        layer: "show",
        show: { legacyIds: {}, segmentType: "SOFT" },
        legacyIds: {},
        segmentType: "SOFT",
        time: 1506094955000,
        timestamp: { absolute: "2017-09-22T15:42:35.000+0000" }
    }];

export let normalizedShowMarkers = [
    {
        assetGUID: "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        duration: 386,
        segmentType: "SOFT",
        times: {
            zuluStartTime: 1506094955000,
            isoStartTime: '2017-09-22T15:42:35.000+00:00',
            zuluEndTime: 1506095341000,
            isoEndTime: "2017-09-22T15:49:01.000Z",
            zeroEndTime: undefined,
            zeroStartTime: undefined
        }
    },
    {
        assetGUID: "6bba1743-1b0a-eb8c-60e2-61cacde084e0",
        duration: 386,
        legacyId: { siriusXMId: undefined, pid: undefined, shortId: undefined },
        segmentType: "SOFT",
        times: {
            zuluStartTime: 1506094955000,
            isoStartTime: '2017-09-22T15:42:35.000+00:00',
            zuluEndTime: 1506095341000,
            isoEndTime: "2017-09-22T15:49:01.000Z",
            zeroEndTime: undefined,
            zeroStartTime: undefined
        }
    }];

export const markerLists = [
    {
        layer: "cut",
        markers: cutMarkers
    },
    {
        layer: "episode",
        markers: episodeMarkers
    },
    {
        layer: "future-episode"
    },
    {
        layer: "segment",
        markers: segmentMarkersFromApi
    },
    {
        layer: "show",
        markers: showMarkers
    },
    {
        layer: "companioncontent"
    }
];

export const primaryChunks = [
    {
        key: "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl: "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        offset: 0,
        timestamp: "2017-09-22T20:45:35.782+0000",
        url: "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274735782_00222665_v3.aac"

    },
    {
        key: "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl: "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        offset: 0,
        timestamp: "2017-09-22T20:45:45.535+0000",
        url: "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274745535_00222666_v3.aac"
    }];

export const secondaryChunks = [
    {
        key: "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl: "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        offset: 0,
        timestamp: "2017-09-22T20:45:35.782+0000",
        url: "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274735782_00222665_v3.aac"

    },
    {
        key: "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl: "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        offset: 0,
        timestamp: "2017-09-22T20:45:45.535+0000",
        url: "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274745535_00222666_v3.aac"
    }];

export const customAudioInfos =
    [
        {
            chunks: {
                chunks: primaryChunks
            },
            name: "primary",
            size: "SMALL",
            url: "%Live_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8",
            position: {
                timestamp: "2017-09-22T20:45:35.782+0000",
                position: "testPosition"
            }
        },
        {
            chunks: {
                chunks: secondaryChunks
            },
            name: "secondary",
            size: "SMALL",
            url: "%Live_Secondary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8"
        }
    ];

export const customVideoInfos =
    [
        {
            chunks: {
                chunks: primaryChunks
            },
            name: "primary",
            size: "SMALL",
            url: "%Live_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8",
            position: {
                timestamp: "2017-09-22T20:45:35.782+0000",
                position: "testPosition"
            }
        },
        {
            chunks: {
                chunks: secondaryChunks
            },
            name: "secondary",
            size: "SMALL",
            url: "%Live_Secondary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8"
        }
    ];


export const customAICInfos =
    [
        {
            chunks: {
                chunks: primaryChunks
            },
            name: "primary",
            size: "SMALL",
            url: "%Live_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8",
            position: {
                timestamp: "2017-09-22T20:45:35.782+0000",
                position: "testPosition"
            }
        },
        {
            chunks: {
                chunks: secondaryChunks
            },
            name: "secondary",
            size: "SMALL",
            url: "%Live_Secondary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8"
        }
    ];


export const hlsAudioInfoPrimary =
    {
        name: "primary",
        size: "SMALL",
        url: "%Live_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8"
    };

export const hlsAudioInfoSecondary =
    {
        name: "secondary",
        size: "SMALL",
        url: "%Live_Secondary_HLS%/AAC_Data/8254/8254_variant_medium_v3.m3u8"
    };

export const hlsAudioInfos =
    [hlsAudioInfoPrimary, hlsAudioInfoSecondary];


export const hlsVideoInfoPrimary =
    {
        name: "primary",
        size: "SMALL",
        url: "%Live_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8"
    };

export const hlsVideoInfoSecondary =
    {
        name: "secondary",
        size: "SMALL",
        url: "%Live_Secondary_HLS%/AAC_Data/8254/8254_variant_medium_v3.m3u8"
    };

export const hlsVideoInfos =
    [hlsVideoInfoPrimary, hlsVideoInfoSecondary];


export const hlsAICInfoPrimary =
    {
        name: "primary",
        size: "SMALL",
        url: "%Live_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8"
    };

export const hlsAICInfoSecondary =
    {
        name: "secondary",
        size: "SMALL",
        url: "%Live_Secondary_HLS%/AAC_Data/8254/8254_variant_medium_v3.m3u8"
    };

export const hlsAICInfos =
    [hlsAICInfoPrimary, hlsAICInfoSecondary];


export const normalizedPrimaryChunks = [
    {
        key: "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl: "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp: "2017-09-22T20:45:35.782+0000",
        url: "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274735782_00222665_v3.aac"

    },
    {
        key: "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl: "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp: "2017-09-22T20:45:45.535+0000",
        url: "%Live_Primary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274745535_00222666_v3.aac"
    }];

export let normalizedSecondaryChunks = [
    {
        key: "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl: "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp: "2017-09-22T20:45:35.782+0000",
        url: "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274735782_00222665_v3.aac"

    },
    {
        key: "0Nsco7MAgxowGvkUT8aYag==",
        keyUrl: "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/key/1",
        interChunkOffset: 0,
        isoTimestamp: "2017-09-22T20:45:45.535+0000",
        url: "%Live_Secondary_HLS%/AAC_Data/8254/HLS_8254_32k_v3/8254_32k_1_092274745535_00222666_v3.aac"
    }];

export const normalizedMediaEndPoints = [
    {
        name: 'primary',
        size: 'SMALL',
        url: '%Live_Primary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8',
        position: {
            isoTimestamp: '2017-09-22T20:45:35.782+0000',
            positionType: 'testPosition'
        },
        mediaFirstChunks: normalizedPrimaryChunks,
        manifestFiles: [hlsAudioInfoPrimary]
    },
    {
        name: 'secondary',
        size: 'SMALL',
        url: '%Live_Secondary_HLS%/AAC_Data/8254/8254_variant_small_v3.m3u8',
        position: {
            isoTimestamp: '2017-09-22T20:45:35.782+0000',
            positionType: 'testPosition'
        },
        mediaFirstChunks: normalizedSecondaryChunks,
        manifestFiles: [hlsAudioInfoSecondary]
    }
];

export let normalizedLiveCuePoint =
    {
        assetGUID: 'null_INSTANTANEOUS',
        event: 'INSTANTANEOUS',
        times: livePointTimes
    };

export let liveChannelData =
    {
        channelId: "123",
        connectInfo: connectInfo,
        cuePointList: {
            cuePoints: cuePoints
        },
        hlsConsumptionInfo: hlsConsumptionInfoLive,
        customAudioInfos: customAudioInfos,
        hlsAudioInfos: hlsAudioInfos,
        markerLists: markerLists
    };

export let aodData =
    {
        channelId: "12356",
        connectInfo: connectInfo,
        cuePointList: {
            cuePoints: cuePoints
        },
        hlsConsumptionInfo: hlsConsumptionInfoAod,
        customAudioInfos: customAudioInfos,
        hlsAudioInfos: hlsAudioInfos,
        markerLists: markerLists
    };

export let vodData =
    {
        channelId: "12356",
        connectInfo: connectInfo,
        cuePointList: {
            cuePoints: cuePoints
        },
        hlsConsumptionInfo: hlsConsumptionInfoVod,
        customVideoInfos: customVideoInfos,
        hlsVideoInfos: hlsVideoInfos,
        markerLists: markerLists
    };

export let aicData =
    {
        channel : {
            channelGuid: "channelGuid"
        },
        clipList : {
            clips:[{
                artistName: "artistName",
                assetGuid: "assetGuid",
                assetType: ContentTypes.ADDITIONAL_CHANNELS,
                clipImageUrl: "clipImageUrl",
                consumptionInfo: "consumptionInfo",
                duration: 100,
                category: ApiLayerTypes.TRACK_CONTENT_TYPE_MUSIC,
                contentUrlList: {
                    contentUrls:[{
                        url:"contentUrl"
                    }]
                },
                title:"title"
            }]
        },
        sequencerSessionId: "sequencerSessionId"
    };

export let seededRadioData =
    {
        channelId: "12356",
        connectInfo: connectInfo,
        cuePointList: {
            cuePoints: cuePoints
        },
        hlsConsumptionInfo: hlsConsumptionInfoVod,
        customVideoInfos: customVideoInfos,
        hlsVideoInfos: hlsVideoInfos,
        markerLists: markerLists,
        channel: {
            channelImageUrl : "channelImageUrl",
            dmcaInfo : {},
            stationFactory : "stationFactory"
        },
        clipList : {
            clips:[{
                trackToken : "trackToken",
                stationFactory :"stationFactory",
                index : 0,
                artistName: "artistName",
                albumName: "albumName",
                assetType: ContentTypes.SEEDED_RADIO,
                clipImageUrl: "clipImageUrl",
                consumptionInfo: "consumptionInfo",
                duration: 100,
                category: ApiLayerTypes.TRACK_CONTENT_TYPE_MUSIC,
                contentUrlList: {
                    contentUrls:[{
                        url:"contentUrl"
                    }]
                },
                title:"title",
                affinity : 1
            },
                {
                    trackToken : "trackToken1",
                    stationFactory :"stationFactory1",
                    index : 1,
                    artistName: "artistName",
                    albumName: "albumName",
                    assetType: ContentTypes.SEEDED_RADIO,
                    clipImageUrl: "clipImageUrl",
                    consumptionInfo: "consumptionInfo",
                    duration: 100,
                    category: ApiLayerTypes.TRACK_CONTENT_TYPE_MUSIC,
                    contentUrlList: {
                        contentUrls:[{
                            url:"contentUrl"
                        }]
                    },
                    status : "status",
                    crossfade : {
                        crossFade: 11,
                        fade : 10,
                        fadeDownDuration : 100,
                        fadeUpDuration : 101,
                        fadeUpPos : 102,
                        outroPos : 104,
                        fadeDownPos : 105,
                        introPos : 106
                    },
                    title:"title",
                    affinity : -1
                },
                {
                    trackToken : "trackToken2",
                    stationFactory :"stationFactory2",
                    index : 2,
                    artistName: "artistName",
                    albumName: "albumName",
                    assetType: ContentTypes.SEEDED_RADIO,
                    clipImageUrl: "clipImageUrl",
                    consumptionInfo: "consumptionInfo",
                    duration: 100,
                    category: ApiLayerTypes.TRACK_CONTENT_TYPE_MUSIC,
                    contentUrlList: {
                        contentUrls:[{
                            url:"contentUrl"
                        }]
                    },
                    status : "status",
                    crossfade : {
                        crossFade: 11,
                        fade : 10,
                        fadeDownDuration : 100,
                        fadeUpDuration : 101,
                        fadeUpPos : 102,
                        outroPos : 104,
                        fadeDownPos : 105,
                        introPos : 106
                    },
                    title:"title",
                    affinity : 0
                },
                {
                    trackToken : "trackToken3",
                    stationFactory :"stationFactory3",
                    index : 3,
                    artistName: "artistName",
                    albumName: "albumName",
                    assetType: ContentTypes.SEEDED_RADIO,
                    clipImageUrl: "clipImageUrl",
                    consumptionInfo: "consumptionInfo",
                    duration: "100",
                    category: ApiLayerTypes.TRACK_CONTENT_TYPE_MUSIC,
                    contentUrlList: {
                        contentUrls:[{
                            url:"contentUrl"
                        }]
                    },
                    status : "status",
                    crossfade : {
                        crossFade: 11,
                        fade : 10,
                        fadeDownDuration : 100,
                        fadeUpDuration : 101,
                        fadeUpPos : 102,
                        outroPos : 104,
                        fadeDownPos : 105,
                        introPos : 106
                    },
                    title:"title",
                    affinity : ""
                }]
        }
    };


export let mockAllProfilesData = {
    profileData: [
        {
            gupId: '123456',
            profileName: 'test',
            avatar: '001'
        }
    ]
};

export let mockNowPlayingResponse = {
    wallClockRenderTime: "2018-04-13T20:52:29.688+0000",
    moduleType: ContentTypes.LIVE_AUDIO,
    updateFrequency: 100,
    aodEpisodeCount: 10,
    channelId: "123",
    liveChannelData: liveChannelData,
    clientConfiguration: mockClientConfigurationResponse,
    getAllProfilesData: mockAllProfilesData
};

export let mockAODResponse = {
    wallClockRenderTime: "2018-04-13T20:52:29.688+0000",
    moduleType: ContentTypes.AOD,
    updateFrequency: 1002,
    aodData: aodData,
    clientConfiguration: mockClientConfigurationResponse,
    getAllProfilesData: mockAllProfilesData
};

export let mockVODResponse = {
    wallClockRenderTime: "2018-04-13T20:52:29.688+0000",
    moduleType: ContentTypes.VOD,
    updateFrequency: 1002,
    vodData: vodData,
    clientConfiguration: mockClientConfigurationResponse
};

export let mockVODResumeActionResponse = {
    wallClockRenderTime: "2018-04-13T20:52:29.688+0000",
    moduleType: ContentTypes.VOD,
    updateFrequency: 1002,
    vodData: vodData,
    clientConfiguration: mockClientConfigurationResponse,
    resumeAction: {
        actionNeriticLink: "Api:tune:aod:big80s:49d63668-ab3a-4fb3-92e7-794f2d4db35e",
        actionType: ActionTypes.TUNE
    }

};

export let mockAICResponse = {
    wallClockRenderTime: "2018-04-13T20:52:29.688+0000",
    moduleType: ContentTypes.ADDITIONAL_CHANNELS,
    updateFrequency: 1002,
    additionalChannelData: aicData,
    resumeAction: { }
};

export let mockSRResponse = {
    wallClockRenderTime: "2018-04-13T20:52:29.688+0000",
    moduleType: ContentTypes.SEEDED_RADIO,
    updateFrequency: 1002,
    seededRadioData: seededRadioData
};
