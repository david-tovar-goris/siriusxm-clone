export class MockPromise
{
    public then = jasmine.createSpy( "then" )
                   .and.callFake( (handler) =>
                                  {
                                      if (handler) { handler(); }
                                      return this;
                                  } );
    public catch = jasmine.createSpy( "catch" ).and.callFake( () => this );
}
