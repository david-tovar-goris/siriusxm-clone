import { AudioPlayerConstants } from "../../mediaplayer/audioplayer/audio-player.consts";

export let relativeUrls = [ {
    name: AudioPlayerConstants.LIVE_PRIMARY_URL,
    url: "LiveRelativeUrl1"
},
    {
        name: AudioPlayerConstants.LIVE_SECONDARY_URL,
        url: "LiveRelativeUrl2"
    } ,
    {
        name: AudioPlayerConstants.AOD_PRIMARY_URL,
        url: "AodRelativeUrl1"
    },
    {
        name: AudioPlayerConstants.AOD_SECONDARY_URL,
        url: "AodRelativeUrl2"
    }];

export let normalizedRelativeUrlsResponse =
               {
                   locale: 'en_US',
                   settings: relativeUrls,
                   errors: [ {
                       name: 'error1'
                   }, {
                       name: 'error2'
                   } ]
               };

export let configurationResponse = {
    components: [
        {
            name: 'relativeUrls',
            locale: 'en_US',
            settings: [ {
                relativeUrls: relativeUrls
            } ],
            errors: [ {
                name: 'error1'
            }, {
                name: 'error2'
            } ]
        }
    ]
};
