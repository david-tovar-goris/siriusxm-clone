import * as _ from "lodash";
import { ReplaySubject }                          from "rxjs";
import { IAppConfig }                             from "../../config/interfaces/app-config.interface";
import { IAllProfilesData, IClientConfiguration } from "../../config";
import { ISuperCategory } from "../../channellineup";

export class MockAppConfig implements IAppConfig
{
    static goodHash    = "8";
    static goodVersion = "9";
    static goodBuild   = "10";

    public apiEndpoint : string = "unittest";
    public domainName  : string = "unitTestDomain";
    public appId : string = "test";
    public resultTemplate : string = "test";
    public defaultSuperCategory : ISuperCategory =
               {
                   key:"defaultKey"
               } as ISuperCategory;
    public deviceInfo: any =
    {
        platform         : "web",
        appRegion        : "appRegion",
        sxmGitHashNumber : MockAppConfig.goodHash,
        sxmAppVersion    : MockAppConfig.goodVersion + '.' + MockAppConfig.goodBuild,
        supportsVideoSdkAnalytics : false
    };

    public clientConfiguration: IClientConfiguration =
    {
        activeListeningGracePeriodInMins  : 10,
        authenticationGracePeriodInDays   : 10,
        cachedContentDeletionPeriodInDays : 10,
        channelLineupId                   : 10,
        clientDeviceId                    : "test",
        dataPlan                          : false,
        facebookAppId                     : "test",
        familyFriendly                    : false,
        faqurl                            : "test",
        fbLinkStatus                      : "test",
        freeToAirPeriodState              : false,
        maximumAutoDownloads              : 10,
        onDemandExpiryDays                : 10,
        openAccessPeriodState             : false,
        radioAuthorizationDelayDuration   : 0,
        radioAuthorizationRequired        : false,
        seededRadioSubscribed             : true,
        streamingAccess                   : 10,
        userDaysListened                  : 0
    };

    public allProfilesData : IAllProfilesData =
    {
        messages : [],
        profileData : [],
        status : "test"
    };

    public contextualInfo: any =
    {
        userAgent   : "useAgent",
        queryString : "queryString",
        host        : "host",
        hostName    : "hostName",
        protocol    : "protocol",
        id          : "id",
        type        : "type",
        deepLink    : true
    };

    youbora : any = {
        api : null,
        getVideoId : function() { return document.getElementsByTagName("video")[0].id; }
    };

    isFreeTierEnable = false;
    initialPathname = "";
    loginRequired = false;
    public inPrivateBrowsingMode : ReplaySubject<boolean> = new ReplaySubject(1);
    public restart : Function = jasmine.createSpy("restart");
    public uniqueSessionId : string = "";
    constructor() {}
}
