import { of as observableOf } from "rxjs";

export class ChromecastRemotePlayerMock
{
    public state$ = observableOf("DISCONNECTED");

    constructor()
    {
    }
}
