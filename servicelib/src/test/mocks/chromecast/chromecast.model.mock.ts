import { of as observableOf, BehaviorSubject } from "rxjs";
import { ContentTypes, PlayerTypes } from "../../../service/types/content.types";
import { ChromecastModel } from "../../../chromecast/chromecast.model";

export const chromeCastModelMock = {
                                isDeviceVideoSupported: false,
                                isDisconnecting: false,
                                state$: new BehaviorSubject<string>(""),
                                playerType: PlayerTypes.LOCAL,
                                castSessionState$: null,
                                castState$: null,
                                remotePlayer: {
                                    isConnected: true,
                                    isPaused: false
                                },
                                remotePlayerController: false,
                                getSession: () =>
                                {
                                    return {
                                        endSession: (mode) => { return true; }
                                    };
                                },
                                triggerChromcastFatalError: () =>
                                {
                                    return null;
                                },
                                tuneChanged$: observableOf({
                                    channelId: 'NBC',
                                    mediaType: ContentTypes.AOD
                                }),
                                chromecastPlayer: {
                                    setState: (sta:any) =>
                                    {
                                        return sta;
                                    }
                                },
                                chromcastFatalError$: observableOf([]),
                                resetChromcastFatalError:()=>{}

                            } as ChromecastModel;
