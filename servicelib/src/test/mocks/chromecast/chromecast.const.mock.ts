

   export const CC = {
        AutoJoinPolicy:{
            ORIGIN_SCOPED:"ORGIN_SCOPED"
        }
    };

    export const CF = {
        CastContext: {
            getInstance:()=>
            {
                return {
                    setOptions: (opts)=>
                    {
                        return opts;
                    },
                    addEventListener: (state, handler)=>
                    {

                           handler({ castState: "Session_Resumed", sessionState: "Session_Ended" });
                           handler({ castState: "No_Device_Available", sessionState: "Session_Start_Failed" });
                           handler({ castState: "Connecting",sessionState: "Session_Resumed" });
                           handler({ castState: "Connected",sessionState: "Session_Resumed" });

                    }
                };
            }
        },
        CastContextEventType: {
            CAST_STATE_CHANGED: "CAST_STATE_CHANGED",
            SESSION_STATE_CHANGED: "SESSION_STATE_CHANGED"
        },
        CastState: {
            CONNECTING : "Connecting",
            NO_DEVICES_AVAILABLE: "No_Device_Available",
            NOT_CONNECTED : "NOT CONNECTED",
            SESSION_RESUMED: "Session_Resumed",
            CONNECTED: "Connected"

        },
        SessionState: {
            SESSION_RESUMED: "Session_Resumed",
            SESSION_ENDED: "Session_Ended",
            SESSION_START_FAILED: "Session_Start_Failed"
        },
        RemotePlayerEventType: {
            IS_CONNECTED_CHANGED: "Is Connected Changed",
            PLAYER_STATE_CHANGED: "Player State Changed",
            IS_PAUSED_CHANGED: "Is Pause Changed"
        },
        RemotePlayer: ()=> {return true;},
        RemotePlayerController: (remotePlayer)=>
        {
            return {
                addEventListener: (eventType, handler)=>
                {
                    let eve = {
                        value: null
                    };
                    handler(eve);
                }
            };
        }
    };
