/// <reference path="../../../node_modules/@types/jasmine/index.d.ts" />


import {of as observableOf,  BehaviorSubject ,  Observable } from 'rxjs';
import { mockHttpHeader } from "../mocks/app-messaging/messaging-delegate.mock";
import { notificationResponseMock } from "../mocks/data/notification-response.mock";

export class MockHttpProvider
{
    constructor(one, two, three, four) {}

    private sessionValidSubject = jasmine.createSpyObj('sessionValidSubject', [
        "next"
    ]);

    public sessionValid = jasmine.createSpyObj('sessionValid', [
        "subscribe"
    ]);

    private httpHeaderObservables = jasmine.createSpyObj('httpHeaderObservables', [
        "push",
        "pop"
    ]);

    private httpHeaderSubscription = jasmine.createSpyObj('httpHeaderSubscription', [
        "unsubscribe"
    ]);

    /**
     * Maximum number of times that the http service will retry an API call for generic HTTP and API Failures.
     * @type {number}
     */
    public static MAX_RETRIES = 1;

    /**
     * NUmber of milliseconds that http service will wait before retrying an API call that failued
     * @type {number}
     */
    public static RETRY_WAIT_TIME_MS = 2000;

    public get = jasmine.createSpy('get').and.returnValue(observableOf(notificationResponseMock));

    private onHttpHeader = jasmine.createSpy('onHttpHeader');

    private http = jasmine.createSpy('http');

    public post = jasmine.createSpy('post');

    public postModuleRequest = jasmine.createSpy('postModuleRequest');

    public postModuleAreaRequest = jasmine.createSpy('postModuleAreaRequest');

    public postModuleAreaRequests = jasmine.createSpy('postModuleAreaRequests');

    public addHttpHeaderObservable = jasmine.createSpy('addHttpHeaderObservable')
        .and
        .returnValue(observableOf(mockHttpHeader));

    public removeHttpHeaderObservable = jasmine.createSpy('removeHttpHeaderObservable');

}
