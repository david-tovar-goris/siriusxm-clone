export let mediaTimeLineMock =
                 {
                     mediaType: "live",
                     mediaId: "liveId",
                     connectInfo: {
                         email: "testEmail",
                         facebook: "testFacebook",
                         phone: "testPhone",
                         twitter: "testTwitter"
                     },
                     liveCuePoint: {
                         assetGUID: "assetGuid",
                         event: "event",
                         times: {
                             zuluStartTime: 100,
                             isoStartTime: "isoStartTime",
                             zuluEndTime: 200,
                             isoEndTime: "isoEndTime"
                         }
                     },
                     cuts: [
                         {
                             assetGUID: "cutAssetGuid1",
                             album: [],
                             artists: [],
                             consumptionInfo: "consumptionInfo1",
                             duration: 50,
                             legacyId: {},
                             title: "title1",
                             times: {
                                 zuluStartTime: 300,
                                 isoStartTime: "cutIsoStartTime1",
                                 zuluEndTime: 400,
                                 isoEndTime: "cutIsoEndTime1"
                             }
                         },
                         {
                             assetGUID: "cutAssetGuid2",
                             album: [],
                             artists: [],
                             consumptionInfo: "consumptionInfo2",
                             duration: 50,
                             legacyId: {},
                             title: "title2",
                             times: {
                                 zuluStartTime: 500,
                                 isoStartTime: "cutIsoStartTime2",
                                 zuluEndTime: 600,
                                 isoEndTime: "cutIsoEndTime2"
                             }
                         }
                     ],
                     segments: [],
                     episodes: [],
                     futureEpisodes: [],
                     shows: [],
                     scoreUpdates: [],
                     mediaEndPoints: [],
                     hlsConsumptionInfo: "",
                     aodEpisodeCount: 10,
                     isDataComeFromResume: false
                 };
