/// <reference path="../../../node_modules/@types/jasmine/index.d.ts" />

import { BehaviorSubject } from "rxjs";

export class MockRequestInterceptor
{
    public onRequest     = jasmine.createSpy( "onRequest" );
    public onPostRequest = jasmine.createSpy( "onPostRequest" );
    public onGetRequest  = jasmine.createSpy( "onGetRequest" );
}

export class MockResponseInterceptor
{
    public onResponse = jasmine.createSpy( "onResponse" );
    public handleHttpHeader = jasmine.createSpy("handleHttpHeader");
    public httpHeaders = new BehaviorSubject(null);
}
