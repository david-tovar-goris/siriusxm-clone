export const mockRecentlyPlayedList = [
    {
        deviceGuid: "ed7764f5-6375-4ab0-96c1-7a40defacb93",
        channelGuid: "9389",
        startDateTime: "2017-10-02T20:48:58.076Z",
        endDateTime: "2017-10-02T20:59:07.488Z",
        gupId: "D7F5B6220B7026542E3E63C6D2E90B54",
        recentPlayType: "live",
        assetGUID: "9389",
        assetType: "channel",
        startStreamTime: "",
        endStreamTime: "",
        startStreamDateTime: "2017-10-02T20:46:03.000+0000",
        endStreamDateTime: "2017-10-02T20:56:12.001+0000",
        incognito: false,
        aodPercentConsumed: 0,
        aodDownload: false,
        recentlyPlayedGuid: "e6cfda45-e606-4842-8086-ab5e8d0e87ac"
    },
    {
        deviceGuid: "ed7764f5-6375-4ab0-96c1-7a40defacb93",
        channelGuid: "jamon",
        startDateTime: "2017-10-02T19:43:51.917Z",
        endDateTime: "2017-10-02T20:47:51.969Z",
        gupId: "D7F5B6220B7026542E3E63C6D2E90B54",
        recentPlayType: "live",
        assetGUID: "jamon",
        assetType: "channel",
        startStreamTime: "",
        endStreamTime: "",
        startStreamDateTime: "2017-10-02T19:25:09.000+0000",
        endStreamDateTime: "2017-10-02T20:28:45.561+0000",
        incognito: false,
        aodPercentConsumed: 0,
        aodDownload: false,
        recentlyPlayedGuid: "30314a30-aec9-4271-afba-4d51fe6f0e42"
    },
    {
        deviceGuid: "ed7764f5-6375-4ab0-96c1-7a40defacb93",
        channelGuid: "coffeehouse",
        startDateTime: "2017-10-02T19:43:51.917Z",
        endDateTime: "2017-10-11T15:37:29.370Z",
        gupId: "D7F5B6220B7026542E3E63C6D2E90B54",
        recentPlayType: "aod",
        assetGUID: "5cc5727e-e464-4022-a4f4-e04ed4535af6",
        assetType: "aodEpisode",
        startStreamTime: "00:00:00.000Z",
        endStreamTime: "00:01:51.230Z",
        startStreamDateTime: "2017-10-11T15:35:36.404Z",
        endStreamDateTime: "2017-10-02T20:28:45.561+0000",
        incognito: false,
        aodPercentConsumed: 20,
        aodDownload: false,
        recentlyPlayedGuid: "241b94c4-f409-4c51-9276-e330487aa61b",
        showTitle: "Art & Soul w/ Caroline Jones",
        episodeTitle: "Intimate conversations & performance",
        shortEpisodeDescription: "Intimate conversations & performance"
    },

    {
        deviceGuid: "4c9332da-abab-477a-a346-360b38c94f20",
        channelGuid: "siriushits1",
        startDateTime: "2017-10-11T15:31:32.146Z",
        endDateTime: "2017-10-11T15:31:52.926Z",
        gupId: "B4B616B855C6818785222BD2FAAD23C6",
        recentPlayType: "aod",
        assetGUID: "c5ab9cf3-acf5-4023-a288-f9086c9ea8d1",
        assetType: "",
        startStreamTime: "00:00:00.000Z",
        endStreamTime: "00:00:20.000Z",
        incognito: false,
        aodPercentConsumed: 2,
        aodDownload: false,
        drmInfo: {
            recordRestriction: "None",
            sharedFlag: false
        },
        aodEpisodeExpiryDate: "2017-10-13T06:59:59.000+0000",
        recentlyPlayedGuid: "b74104a7-40a2-4f67-9ce0-4e84d751d30f",
        showTitle: "The Morning Mash Up",
        episodeTitle: "Anthony Bourdain/Adrianne Palicki",
        shortEpisodeDescription: "Anthony Bourdain/Adrianne Palicki",
        showLogos: [
            {
                encrypted: false,
                width: 150,
                height: 150,
                type: "IMAGE",
                url: "http://pri.art.prod.streaming.siriusxm.com/images/chan/8f/2bf707-c5c6-4f0d-0396-8cfd3fb284e1.png",
                name: "show logo on dark"
            },
            {
                encrypted: false,
                width: 150,
                height: 150,
                type: "IMAGE",
                url: "http://pri.art.prod.streaming.siriusxm.com/images/chan/8f/2bf707-c5c6-4f0d-0396-8cfd3fb284e1.png",
                name: "show logo on light"
            },
            {
                encrypted: false,
                width: 150,
                height: 150,
                type: "IMAGE",
                url: "http://pri.art.prod.streaming.siriusxm.com/images/chan/b7/deffaa-5e05-1347-fc87-e092bb53e9f4.png",
                name: "show logo white"
            }
        ]
    },
    {
        deviceGuid: "4c9332da-abab-477a-a346-360b38c94f20",
        channelGuid: "8238",
        startDateTime: "2017-10-12T14:34:09.545Z",
        endDateTime: "2017-10-12T14:34:46.001Z",
        gupId: "B4B616B855C6818785222BD2FAAD23C6",
        recentPlayType: "aod show",
        assetGUID: "dfc463a3-8eb3-41c9-9503-193b993bac9c",
        assetType: "vodEpisode",
        startStreamTime: "00:00:00.000Z",
        endStreamTime: "00:10:17.830Z",
        incognito: false,
        aodPercentConsumed: 10,
        aodDownload: false,
        drmInfo: {
            recordRestriction: "RESTRICTED_RECORDABLE",
            sharedFlag: false
        },
        aodEpisodeCount: 10,
        aodEpisodeExpiryDate: "2017-10-18T06:59:59.000+0000",
        recentlyPlayedGuid: "1b3d337f-9fc7-445e-9193-3207de381ee2",
        showTitle: "The Laura Coates Show",
        episodeTitle: "The Laura Coates Show",
        shortEpisodeDescription: "The Laura Coates Show",
        showLogos: [
            {
                encrypted: false,
                width: 150,
                height: 150,
                type: "IMAGE",
                url: "http://pri.art.prod.streaming.siriusxm.com/images/chan/57/0b6fe3-35e2-76af-7108-44fd960697c8.png",
                name: "show logo on dark"
            },
            {
                encrypted: false,
                width: 150,
                height: 150,
                type: "IMAGE",
                url: "http://pri.art.prod.streaming.siriusxm.com/images/chan/57/f6aa46-17c8-6d4b-b6c2-8b37cc4aa393.png",
                name: "show logo on light"
            },
            {
                encrypted: false,
                width: 150,
                height: 150,
                type: "IMAGE",
                url: "http://pri.art.prod.streaming.siriusxm.com/images/chan/21/faaf7c-2293-801d-fbc4-dec37856f093.png",
                name: "show logo white"
            }
        ]
    }
];

export const mockRecentlyPlayedData = {
    recentlyPlayedData: {
        recentlyPlayeds: mockRecentlyPlayedList
    }
};
