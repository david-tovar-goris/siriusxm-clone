import { IBaseCategory } from "../../channellineup/categories.interfaces";

export const mockDiscoverChannelListResponse = {
    updateFrequency: 100,
    moduleDetails  : {
        liveChannelResponse: {
            liveChannelResponses: []
        }
    }
};

export const mockCategoryChannels = [
    {
        aodEpisodeCount: 5,
        channelGuid    : "8001",
        channelId: "8001",
        isGeoRestircted: false
    },
    {
        isGeoRestricted: true,
        aodEpisodeCount: 6,
        channelId: "8002",
        channelGuid    : "8002"
    }
];

export const onDemandShowMock = {
    type: '',
    disableAllBanners: true,
    episodeCount: 0,
    newEpisodeCount: 0,
    showDescription: "",
    aodEpisodes: [],
    vodEpisodes: [],
    isFavorite: true
};

export const offlinePlaybackMock = {
    highProfile: 0,
    lowProfile: 0,
    mediumProfile: 0
};

export const onDemandEpisodeMock = {
    disableAllBanners: true,
    episodeGuid: '',
    show: {},
    longDescription: '',
    shortDescription : '',
    longTitle: '',
    mediumTitle: '',
    isFavorite: true
};

export const vodEpisodeMock = {
    ...onDemandEpisodeMock,
    allowDownload: true,
    disableAutoBanners: true,
    disableRecommendations: true,
    hosts: '',
    hostList: [''],
    topics: '',
    topicList: [''],
    rating: '',
    offlinePlayback: offlinePlaybackMock
};

export const channelMock = {
    channelGuid: "8001",
    channelNumber: 0,
    clientBufferDuration: 0,
    disableRecommendations: true,
    geoRestrictions: 0,
    aodShowCount: 0,
    inactivityTimeout: 10000,
    isAvailable: true,
    isBizMature: true,
    isFavorite: true,
    isMature: true,
    isMySxm: true,
    isPersonalized: true,
    isPlayByPlay: true,
    mediumDescription: '',
    shortDescription: '',
    siriusChannelNumber: 0,
    sortOrder: 0,
    spanishContent: true,
    streamingName: '',
    url: '',
    xmChannelNumber: 0,
    xmServiceId: 0,
    satOnly: true
};

export const aodEpisodeMock = {
    ...onDemandEpisodeMock,
    type: '',
    aodEpisodeGuid: '1234',
    baseTime: '',
    expiringSoon: true,
    featured: true,
    featuredPriority: true,
    highlighted: true,
    hot: true,
    originalAirDate: '',
    percentConsumed: 1,
    programType: '',
    special: true,
    valuable: true,
    channels: [channelMock],
    contentUrlList: [''],
    contextualBanners: [''],
    dmcaInfo: {},
    drmInfo: {},
    hosts: [''],
    publicationInfo: {},
    assetGuid : "assetGuid"
};

export const mockSuperCategories = [
    {
        categoryGuid: "supcat1",
        isPrimary   : true,
        key         : "supcat1",
        name        : "supcat1",
        aodShowCount: 1,
        description : "supcat1",
        images      : { images: [] },
        sortOrder   : 1,
        url         : "supcat1",
        categories  : {
            categories: [{
                categoryGuid: "cat1",
                isPrimary   : true,
                key         : "cat1",
                name        : "cat1",
                aodShowCount: 1,
                description : "cat1",
                images      : { images: [] },
                sortOrder   : 40,
                url         : "cat1",
                channels    : { channels: [mockCategoryChannels[0], mockCategoryChannels[1]] },
                aodShows    : []
            }, {
                categoryGuid: "cat2",
                isPrimary   : true,
                key         : "cat2",
                name        : "cat2",
                aodShowCount: 1,
                description : "cat2",
                images      : { images: [] },
                sortOrder   : 30,
                url         : "cat2",
                channels    : { channels: [mockCategoryChannels[0], mockCategoryChannels[1]] },
                aodShows    : []
            }, {
                categoryGuid: "cat3",
                isPrimary   : true,
                key         : "cat3",
                name        : "cat3",
                aodShowCount: 1,
                description : "cat3",
                images      : { images: [] },
                sortOrder   : 10,
                url         : "cat3",
                channels    : { channels: [] },
                aodShows    : []
            }]
        }
    },
    {
        categoryGuid: "supcat2",
        isPrimary   : true,
        key         : "supcat2",
        name        : "supcat2",
        aodShowCount: 1,
        description : "supcat2",
        images      : { images: [] },
        sortOrder   : 1,
        url         : "supcat2",
        imageList   : [],
        categories  : {
            categories: [{
                categoryGuid: "cat1",
                isPrimary   : true,
                key         : "cat1",
                name        : "cat1",
                aodShowCount: 1,
                description : "cat1",
                images      : { images: [] },
                sortOrder   : 1,
                url         : "cat1",
                channels    : { channels: [mockCategoryChannels[0], mockCategoryChannels[1]] },
                aodShows    : []
            }]
        }
    }
];

export const mockImages = [
    {
        name: "background",
        height: 10,
        width: 10,
        platform: "whatever",
        url: "getithere"
    },
    {
        name: "background",
        height: 20,
        width: 20,
        platform: "whatever",
        url: "getithere"
    },
    {
        name: "background what a long name this is",
        height: 10,
        width: 10,
        platform: "whatever",
        url: "getithere"
    },
    {
        name: "background what a long name this is (with parens now (deal) with that)",
        height: 10,
        width: 10,
        platform: "whatever",
        url: "getithere"
    }
];

export const mockChannelLineup = [
    {
        channelId             : "8001",
        name                  : "8001",
        channelGuid           : "8001",
        channelNumber         : 1,
        clientBufferDuration  : 1000,
        disableRecommendations: false,
        geoRestrictions       : 1,
        aodShowCount          : 5,
        inactivityTimeout     : 1000,
        isAvailable           : true,
        isBizMature           : true,
        isFavorite            : true,
        isMature              : true,
        isMySxm               : false,
        isPersonalized        : false,
        isPlayByPlay          : false,
        mediumDescription     : "8001 8001 8001",
        shortDescription      : "8001 8001",
        siriusChannelNumber   : 8001,
        sortOrder             : 0,
        subscribed            : true,
        spanishContent        : false,
        streamingName         : "8001",
        url                   : "8001",
        xmChannelNumber       : 8001,
        xmServiceId           : 8001,
        satOnly               : false,
        categories            : {
            categories: [
                { categoryGuid: "cat1", isPrimary: true, key: "cat1", name: "cat1", sortOrder : 10 },
                { categoryGuid: "cat2", isPrimary: false, key: "cat2", name: "cat2", sortOrder : 1 }
            ]
        },
        images                : { images: mockImages }
    },
    {
        channelId: "8002",
        name: "8002",
        channelGuid: "8002",
        channelNumber: 1,
        clientBufferDuration: 1000,
        disableRecommendations: false,
        geoRestrictions: 1,
        aodShowCount: 5,
        inactivityTimeout: 1000,
        isAvailable: true,
        isBizMature: true,
        isFavorite: true,
        isMature: true,
        isMySxm: false,
        isPersonalized: false,
        isPlayByPlay: false,
        mediumDescription: "8002 8002 8002",
        shortDescription: "8002 8002",
        siriusChannelNumber: 8002,
        sortOrder: 0,
        subscribed : true,
        spanishContent: false,
        streamingName: "8002",
        url: "8002",
        xmChannelNumber: 8002,
        xmServiceId: 8002,
        satOnly: false,
        categories: {
            categories: [
                {categoryGuid: "cat1", isPrimary: false, key: "cat1", name: "cat1", sortOrder : 20},
                {categoryGuid: "cat2", isPrimary: true, key: "cat2", name: "cat2", sortOrder : 10 }
            ]
        },
        images: { images: mockImages }
    }
];

export const mockMarker = {
    assetGUID: "guid1",
    duration : 1000,
    time     : 0,
    cut      : {
        album        : { title: "album1" },
        title        : "cut1",
        clipGUID     : "guid1",
        artists      : [
            { name: "name1" },
            { personGuid: "person1" }
        ],
        galaxyAssetId: "assetId1",
        legacyIds    : { siriusXMId: "siriusxmid1", pid: "pid1" }
    },
    timestamp: {
        absolute: "timestamp1"
    }
};

export const mockLiveChannelListing = [
    {
        aodEpisodeCount: 5,
        channelId      : "8001",
        markerLists    : [
            { layer: "cut", markers: [mockMarker, mockMarker] },
            { layer: "episode", markers: [mockMarker, mockMarker] }
        ]
    },
    {
        aodEpisodeCount: 6,
        channelId      : "8002",
        markerLists    : [
            { layer: "cut", markers: [mockMarker, mockMarker] },
            { layer: "episode", markers: [mockMarker, mockMarker] }
        ]
    }
];

export const mockLiveChannels = {
    updateFrequency: 100,
    channels       : mockLiveChannelListing
};

export const mockChannelListing = {
    channelChangeId: "id",
    superCategories: mockSuperCategories,
    channels       : mockChannelLineup
};

export const superCategoriesMock = [
    {
        categoryGuid: "supcat1",
        isPrimary   : true,
        key         : "supcat1",
        name        : "supcat1",
        aodShowCount: 1,
        description : "supcat1",
        images      : { images: [] },
        sortOrder   : 1,
        url         : "supcat1",
        categoryList: [ {
            categoryGuid: "cat1",
            isPrimary   : true,
            key         : "cat1",
            name        : "cat1",
            aodShowCount: 1,
            description : "cat1",
            images      : { images: mockImages },
            sortOrder   : 30,
            url         : "cat1",
            channels    : { channels: [ mockCategoryChannels[ 0 ], mockCategoryChannels[ 1 ] ] },
            aodShows    : []
        }, {
            categoryGuid: "cat2",
            isPrimary   : true,
            key         : "cat2",
            name        : "cat2",
            aodShowCount: 1,
            description : "cat2",
            images      : { images: mockImages },
            sortOrder   : 20,
            url         : "cat2",
            channels    : { channels: [ mockCategoryChannels[ 0 ], mockCategoryChannels[ 1 ] ] },
            aodShows    : []
        }, {
            categoryGuid: "cat3",
            isPrimary   : true,
            key         : "cat3",
            name        : "cat3",
            aodShowCount: 1,
            description : "cat3",
            images      : { images: mockImages },
            sortOrder   : 10,
            url         : "cat3",
            channels    : { channels: [] },
            aodShows    : []
        }
        ]
    },
    {
        categoryGuid: "supcat2",
        isPrimary   : true,
        key         : "supcat2",
        name        : "supcat2",
        aodShowCount: 1,
        description : "supcat2",
        images      : { images: mockImages },
        sortOrder   : 1,
        url         : "supcat2",
        categoryList: [ {
            categoryGuid: "cat1",
            isPrimary   : true,
            key         : "cat1",
            name        : "cat1",
            aodShowCount: 1,
            description : "cat1",
            images      : { images: mockImages },
            sortOrder   : 1,
            url         : "cat1",
            channels    : { channels: [ mockCategoryChannels[ 0 ], mockCategoryChannels[ 1 ] ] },
            aodShows    : []
        }
        ]
    }
];

export const baseCategoryMock: IBaseCategory = {
    categoryGuid: "cat1",
    isPrimary: true,
    key: "subcat1",
    name: "subcat1"
};
