import { alertsList } from './../alerts/alert.response';
import { IProfileAvatar } from "../../../profile/profile.interface";

export const mockProfileRecentlyPlayedData =
                 [
                     {
                         aodDownload        : false,
                         aodPercentConsumed : 0,
                         assetGUID          : "thebeat",
                         assetType          : "channel",
                         channelGuid        : "thebeat",
                         contentType        : "audio",
                         deviceGuid         : "deviceGuid1",
                         endDateTime        : "2018-03-20T16:33:43.511Z",
                         endStreamDateTime  : "2018-03-20T16:11:03.075+0000",
                         endStreamTime      : "",
                         gupId              : "gupId1",
                         incognito          : false,
                         recentPlayType     : "live",
                         recentlyPlayedGuid : "recentlyPlayedGuid1",
                         startDateTime      : "2018-03-20T16:27:06.268Z",
                         startStreamDateTime: "2018-03-20T16:23:06.368+0000",
                         startStreamTime    : ""
                     },
                     {
                         aodDownload       : false,
                         aodPercentConsumed: 0,
                         assetGUID         : "thebeat",
                         assetType         : "channel",
                         channelGuid       : "thebeat",
                         contentType       : "audio",
                         deviceGuid        : "deviceGuid2",
                         endStreamTime     : "",
                         gupId             : "gupId2",
                         incognito         : false,
                         recentPlayType    : "live",
                         recentlyPlayedGuid: "recentlyPlayedGuid2",
                         startStreamTime   : ""
                     }
                 ];
export const mockProfilePausePointData     =
                 [ {
                     aodDownload       : false,
                     aodPercentConsumed: 57,
                     assetGUID         : "assetGuid1",
                     assetType         : "aodEpisode1",
                     channelGuid       : "9408",
                     contentType       : "audio",
                     deviceGuid        : "deviceGuid1",
                     endDateTime       : "2018-03-19T21:39:14.991Z",
                     endStreamTime     : "01:33:26.948Z",
                     gupId             : "gupId",
                     incognito         : false,
                     pausePointsGuid   : "pausePointGuid1",
                     recentPlayType    : "aod",
                     startDateTime     : "2018-03-19T21:17:51.432Z",
                     startStreamTime   : "01:23:48.823Z"
                 },
                     {
                         aodDownload       : false,
                         aodPercentConsumed: 57,
                         assetGUID         : "assetGuid2",
                         assetType         : "aodEpisode2",
                         channelGuid       : "9408",
                         contentType       : "audio",
                         deviceGuid        : "deviceGuid2",
                         endStreamTime     : "01:33:26.948Z",
                         gupId             : "gupId",
                         incognito         : false,
                         pausePointsGuid   : "pausePointGuid",
                         recentPlayType    : "aod",
                         startStreamTime   : "01:23:48.823Z"
                     }
                 ];

export const mockProfileFavorites =
                 [ {
                     assetGUID      : "assetGuid",
                     assetName      : "Asset Name",
                     assetType      : "channel",
                     channelId      : "channelId",
                     comingSortOrder: 2,
                     contentType    : "live",
                     tabSortOrder   : 1
                 }
                 ];

export const mockProfileAlerts =
                 [ {
                     active                 : true,
                     alertId                : "alertId",
                     alertType              : "1",
                     assetGUID              : "assetGuid",
                     consumedAlertList      : [ { } ],
                     deviceId               : "deviceId",
                     gupId                  : "gupId",
                     legacyId1              : "legacyId1",
                     legacyId2              : "legacyId2",
                     locationId             : "locationId",
                     showImages             : [ { } ],
                     showName               : "showName"
                 }
                 ];

export const mockProfileResponseFromApi = {
    moduleDetails     : {
        favoritesList:
            {
                favorites: mockProfileFavorites
            },
        alertList:
            {
                alerts: mockProfileAlerts
            }
    },
    pausePointTypes   : mockProfilePausePointData,
    recentlyPlayedData: {
        recentlyPlayeds: mockProfileRecentlyPlayedData
    }
};

export const mockProfileResponseFromService = {
    alerts        : mockProfileAlerts,
    favorites     : mockProfileFavorites,
    pausePoints   : mockProfilePausePointData,
    recentlyPlayed: mockProfileRecentlyPlayedData
};

export const mockUpdateProfileNameResponseFromService = {
    gupid     : "423J2H3J2H3J23HJ23HJ23HJ2",
    profilename   : "testprofile3",
    avatarid: "002"
};

export const mockAvatars: IProfileAvatar[] = [
    {
        url: 'https://siriusxm.avataer001.jpg',
        avatarId: '002'
    }
];

export const mockAvatarResponseFromService = {
    avatarImages: {
        images: mockAvatars
    }
};

