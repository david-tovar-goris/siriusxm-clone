import {from as observableFrom,  Observable } from 'rxjs';
import { FavoriteDelegateMock } from "./favorite.delegate";

export class FavoriteServiceMock
{
    favorites = observableFrom([]);

    public updateFavorite: object;
    public getFavorites: object;
    private favoriteDelegate: object;

    constructor()
    {
        this.getFavorites = jasmine.createSpy("getFav");
        this.updateFavorite = jasmine.createSpy("updateFav");
        this.favoriteDelegate = new FavoriteDelegateMock();
    }
}
