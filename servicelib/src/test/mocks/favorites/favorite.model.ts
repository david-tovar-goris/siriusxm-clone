import {from as observableFrom } from 'rxjs';

export class FavoriteModelMock
{
    favorites$ = observableFrom([]);

    public setFavorites: object;

    constructor()
    {
        this.setFavorites = jasmine.createSpy("setFavorites");
    }

}
