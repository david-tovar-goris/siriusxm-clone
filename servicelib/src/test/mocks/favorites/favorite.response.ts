import {
    IFavoriteItem,
    FavoriteUpdateItem,
    FavoriteContentType,
    IGroupedFavorites
} from "../../../favorite/favorite.interface";
import { ICarouselDataByType } from "../../../carousel/carousel.interface";
import { CarouselTypeConst } from "../../../carousel/carousel.const";

export const favList: IFavoriteItem[] = [
    { assetGUID: "01", assetType: "live", channelId: "01", showImages: [], showLogos: [],
        assetName: "assetName01", contentType:"contentType01", episodeCount: 0 },
    { assetGUID: "02", assetType: "show", channelId: "02", showImages: [], showLogos: [],
        assetName: "assetName021", contentType:"contentType02", episodeCount: 1 },
    { assetGUID: "03", assetType: "live", channelId: "03", showImages: [], showLogos: [] ,
        assetName: "assetName03", contentType:"contentType03", episodeCount: 2 }
];

export const favResponse =
{
    favoritesList : { favorites : favList }
};


const buildFavUpdateArray = (): FavoriteUpdateItem[] =>
{
    return favList.map(item =>
    {
        return new FavoriteUpdateItem(item.channelId, item.contentType as FavoriteContentType , item.assetType,
            'update',item.assetGUID, 0);
    });
};

export const favUpdate: FavoriteUpdateItem[] = buildFavUpdateArray();

export const favGroupedList: IGroupedFavorites = {
    channels:[{
        line1: "Ch 26",
        tileContentType: "channel",
        tileContentSubType: "nowplaying",
        tileAssetInfo: {
            aodEpisodecaId: "",
            episodeGuid: "",
            channelId: "classicvinyl",
            channelGuid: "5ad8659a-414a-9e26-b973-f5a229d788dd",
            showGuid: "",
            vodEpisodeGuid: "",
            categoryGuid: "",
            categoryKey: "",
            recentPlayGuid: "",
            recentPlayAssetGuid: "",
            recentPlayType: "",
            alertGuid: "",
            favAssetGuid: "5ad8659a-414a-9e26-b973-f5a229d788dd",
            showName: "",
            isLiveVideoEligible: null,
            channelName: "Classic Vinyl",
            sequencerTuneChannelId: "",
            tabSortOrder: 1,
            create: false,
            isPlaceholderShow: false,
            stationId: "",
            hideFromChannelList: false
        }
    },
    {
        line1: "Ch 22",
        tileContentType: "channel",
        tileContentSubType: "live",
        tileAssetInfo:{
            aodEpisodecaId: "",
            episodeGuid: "",
            channelId: "classicvinyl",
            channelGuid: "6bd8659a-414a-9e26-b973-f5a229d788dd",
            showGuid: "",
            vodEpisodeGuid: "",
            categoryGuid: "",
            categoryKey: "",
            recentPlayGuid: "",
            recentPlayAssetGuid: "",
            recentPlayType: "",
            alertGuid: "",
            favAssetGuid: "6bd8659a-414a-9e26-b973-f5a229d788dd",
            showName: "",
            isLiveVideoEligible: null,
            channelName: "My Favorite",
            sequencerTuneChannelId: "",
            tabSortOrder: 2,
            create: false,
            isPlaceholderShow: false,
            stationId: "",
            hideFromChannelList: false
        }
    }],
    shows: [{
        tileContentType: "show",
        tileContentSubType: "aod",
        tileAssetInfo: {
            aodEpisodecaId: "",
            episodeGuid: "",
            channelId: "thebridge",
            channelGuid: "1898923d-071d-0abd-1a62-ed0d2702d4f1",
            showGuid: "d471e554-e116-47be-8333-dc68ca3e9b09",
            vodEpisodeGuid: "",
            categoryGuid: "",
            categoryKey: "",
            recentPlayGuid: "",
            recentPlayAssetGuid: "",
            recentPlayType: "",
            alertGuid: "",
            favAssetGuid: "d471e554-e116-47be-8333-dc68ca3e9b09",
            showName: "The Village Folk Show with Mary Sue Twohy",
            isLiveVideoEligible: false,
            channelName: "The Bridge",
            sequencerTuneChannelId: "",
            tabSortOrder: 1,
            create: false,
            isPlaceholderShow: false,
            stationId: "",
            hideFromChannelList: false
        }
    }],
    episodes: []
} as IGroupedFavorites;

export const carouselDataByType = {
    selectors: [{
        class: CarouselTypeConst.CONTENT_TOGGLE_SELECTOR,
        segments: [
            {
                title: "CHANNELS",
                key: "CHANNELS",
                class: "channels",
                carousels: [
                    {
                        tiles: favGroupedList.channels
                    }
                ]
            }
        ]
    }]
} as ICarouselDataByType;
