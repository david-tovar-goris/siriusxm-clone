export class FavoriteDelegateMock
{
    public fetchFavorites: object;
    public updateFavorite: object;

    constructor()
    {
        this.fetchFavorites = jasmine.createSpy("fetchFavs");
        this.updateFavorite = jasmine.createSpy("updateFav");
    }
}
