
import {of as observableOf,  Observable } from 'rxjs';
import { IMediaPlayer } from "../../mediaplayer/media-player.interface";
import { IMediaCut } from "../../index";

export  const consumeServiceMock = {
    tuneIn : function (mediaPlayer: IMediaPlayer, mediaType: string)
    {
        return true;
    },

    tuneOut : function (mediaPlayer: IMediaPlayer, mediaType: string)
    {
        return observableOf(true);
    },

    pause : function (mediaPlayer: IMediaPlayer, mediaType: string)
    {
        return observableOf(true);
    },

    resume : function (mediaPlayer: IMediaPlayer, mediaType: string)
    {
        return observableOf(true);
    },

    cutChange : function (mediaPlayer: IMediaPlayer, mediaType: string, oldCut: IMediaCut, newCut: IMediaCut)
    {
        return observableOf(true);
    }
};
