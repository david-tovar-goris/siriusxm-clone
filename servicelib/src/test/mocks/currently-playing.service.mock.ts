import { of } from "rxjs";
import {
    ILegacyId,
    IDmcaInfo,
    IMediaEpisode,
    IMediaShow,
    IMediaCut
} from "../../index";

const trackName: string       = "trackName";
const artistName: string      = "Eric Church";
const albumTitle: string      = "The Outsiders";

export const currentlyPlayingMedia = {
    cut      : {
        assetGUID      : "assetGUID",
        times          : {
            zuluStartTime: 0,
            isoStartTime : "isoStartTime"
        },
        consumptionInfo: "consumptionInfo",
        duration       : 0,
        legacyId       : {} as ILegacyId,
        title          : trackName,
        layer          : "",
        artists        : [
            {
                name: artistName
            }
        ],
        album          : {
            title: albumTitle
        },
        cutContentType : ""
    },
    mediaType: "mediaType",
    mediaId  : "mediaId",
    channelId : "channelId",
    dmca     : {} as IDmcaInfo,
    episode  : {} as IMediaEpisode,
    show     : {} as IMediaShow,
    firstCut : {} as IMediaCut,
    duration : 0,
    youJustHeard   : [
        {
            albumImageUrl: "imageUrl",
            title        : "title",
            artistName   : "artistName",
            cut          : {} as IMediaCut
        }
    ]
};

export class currentlyPlayingServiceMock
{
    public currentlyPlayingData;

    constructor()
    {
        this. currentlyPlayingData = {
            subscribe: (result: Function, fault: Function) => result(currentlyPlayingMedia),
            flatMap  : (mapResult: Function) => mapResult(currentlyPlayingMedia),
            pipe: () => of("")
        };
    }
}

