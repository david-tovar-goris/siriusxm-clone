import { IAlert, INotificationAlert } from './../../../alerts/alert.interface';

export const mockAlert: IAlert = {
        active: true,
        alertId: 'alertId',
        alertType: 'alertType',
        assetGuid: 'assetGuid',
        consumedAlerts: [],
        deviceId: 'deviceId',
        gupId: 'gupId',
        legacyId1: 'legacyId1',
        legacyId2: 'legacyId2',
        showImages: [],
        showName: 'showName'
    } as IAlert;

export const mockAlerts: IAlert[] = [mockAlert];

export const mockNotificationAlert:  INotificationAlert[] = [
    {
        alertType: 'alertType',
        assetGuid: 'assetGuid',
        episodeGuid: 'episodeGuid',
        zuluStartTime: 0,
        episodeStartDateTime: '',
        episodeEndDateTime: '',
        channelNumber: 'channelNumber',
        title: 'title',
        channelName: 'channelName'
    }
];

export const mockgupAlertsCreateRequests =  [
    {
        "alert": {
            "active"    : true,
            "alertType" : 1,
            "assetGUID" : 'assetGuid',
            "deviceId"  : "",
            "legacyId1" : 'channelId',
            "legacyId2" : 'channelId',
            "locationId": "0"
        }
    }
];

export const mockgupAlertsUpdateRequests =  [
    {
        "alert": {
            "alertType" : "1",
            "active"    : false,
            "alertId"   : 'alertId',
            "assetGUID" : 'assetGuid',
            "deviceId"  : "",
            "locationId": "0"
        }
    }
];

export const mockNormalizeNotificationHeader:  INotificationAlert[] = [
    {
        alertType: '1',
        assetGuid: 'assetGuid',
        episodeGuid: 'episodeGuid',
        zuluStartTime: 0,
        episodeStartDateTime: '',
        episodeEndDateTime: '',
        channelNumber: 'channelNumber',
        title: 'title',
        channelName: 'channelName'
    }
];

// tslint:disable-next-line:max-line-length
export const mockNotification = "[type='1',assetGUID='fb203d33-8157-0005-ccb4-30a6adbc99cb',episodeGUID='14d734b7-5b4f-a7b6-4f13-72733c3ca80c',episodeStartDateTime='2018-05-17T14:00:00.000Z',episodeEndDateTime='2018-05-17T17:00:00.000Z',channel='9457',title='Pac-12+This+Morning',channelName='SiriusXM+Pac-12+Radio']~";

export const mockNotificationList = [
    {
        alertType: '1',
        assetGuid: 'fb203d33-8157-0005-ccb4-30a6adbc99cb',
        episodeGuid: '14d734b7-5b4f-a7b6-4f13-72733c3ca80c',
        episodeStartDateTime: '2018-05-17T14:00:00.000Z',
        episodeEndDateTime: '2018-05-17T17:00:00.000Z',
        channelNumber: '9457',
        title: 'Pac-12 This Morning',
        channelName: 'SiriusXM Pac-12 Radio'
    }
];
