import { IAlert } from './../../../alerts/alert.interface';

export const alertsList: IAlert[] = 
                    [
                        {   
                            active: true, 
                            alertId: "alertId01",
                            alertType: "alertType01",
                            assetGuid: "assetGuid01",
                            consumedAlerts: [],
                            deviceId: "deviceId01",
                            gupId: "gupId01",
                            legacyId1: "legacyId101",
                            legacyId2: "legacyId201",
                            showImages: [],
                            showName: "showName01" 
                        },
                        { 
                            active: true, 
                            alertId: "alertId02",
                            alertType: "alertType02",
                            assetGuid: "assetGuid02",
                            consumedAlerts: [],
                            deviceId: "deviceId02",
                            gupId: "gupId02",
                            legacyId1: "legacyId102",
                            legacyId2: "legacyId202",
                            showImages: [],
                            showName: "showName02" 
                        },
                        { 
                            active: true, 
                            alertId: "alertId03",
                            alertType: "alertType03",
                            assetGuid: "assetGuid03",
                            consumedAlerts: [],
                            deviceId: "deviceId03",
                            gupId: "gupId03",
                            legacyId1: "legacyId103",
                            legacyId2: "legacyId203",
                            showImages: [],
                            showName: "showName03"
                        }
                    ];

export const alertResponse =
{
    alertsList : { alerts : alertsList }
};
