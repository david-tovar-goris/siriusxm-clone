
export class CarouselDelegateMock
{
    public getCarouselsBySuperCategory;
    public getCarouselsByPage;
    public getCarouselsBySubCategory;
    public getFavoritesCarousel;

    constructor()
    {
        this.getCarouselsBySuperCategory = jasmine.createSpy("getCarouselsBySuperCategoryType");
        this.getCarouselsBySubCategory = jasmine.createSpy("getCarouselsBySubCategory");
        this.getCarouselsByPage = jasmine.createSpy("getCarouselsByPage");
        this.getFavoritesCarousel = jasmine.createSpy("getFavoritesCarousel");
    }
}
