export const mockServiceConfig = {
    apiEndpoint    : "myApi",
    appId          : "myApp",
    resultTemplate : "web",
    deviceInfo     : {
        appRegion        : "region",
        browser          : "unittest",
        browserVersion   : "version",
        clientDeviceId   : "id",
        clientDeviceType : "type",
        deviceModel      : "model",
        osVersion        : "OS",
        platform         : "platform",
        player           : "player",
        sxmAppVersion    : "version"
    }
};
