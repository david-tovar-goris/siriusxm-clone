import { IClip, ICrossfade, IMediaEndPoint, ITime, TrackStatus } from "../../../tune";

export const mockTracksList: IClip[] = [
    {
        assetGUID: "assetGUID1",
        albumName: "albumName",
        artistName: "artistName",
        assetType: "assetType",
        category: "category",
        clipImageUrl: "clipImageUrl",
        consumptionInfo: "consumptionInfo",
        mediaEndPoints: {} as IMediaEndPoint[],
        duration: 200,
        title: "title",
        status: "FUTURE",
        contentUrlList: "contentUrlList",
        layer: "layer",
        times: {} as ITime,
        index: 0,
        crossfade:{} as ICrossfade
    },
    {
        assetGUID: "assetGUID2",
        albumName: "albumName",
        artistName: "artistName",
        assetType: "assetType",
        category: "category",
        clipImageUrl: "clipImageUrl",
        consumptionInfo: "consumptionInfo",
        mediaEndPoints: {} as IMediaEndPoint[],
        duration: 200,
        title: "title",
        status: "FUTURE",
        contentUrlList: "contentUrlList",
        layer: "layer",
        times: {} as ITime,
        index: 1,
        crossfade:{} as ICrossfade
    },
    {
        assetGUID: "assetGUID3",
        albumName: "albumName",
        artistName: "artistName",
        assetType: "assetType",
        category: "category",
        clipImageUrl: "clipImageUrl",
        consumptionInfo: "consumptionInfo",
        mediaEndPoints: {} as IMediaEndPoint[],
        duration: 200,
        title: "title",
        status: "FUTURE",
        contentUrlList: "contentUrlList",
        layer: "layer",
        times: {} as ITime,
        index: 2,
        crossfade:{} as ICrossfade
    }
];
