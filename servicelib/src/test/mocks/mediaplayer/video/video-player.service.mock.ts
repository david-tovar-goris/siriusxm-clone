import { BehaviorSubject, Subject, Observable, of } from "rxjs";
import { VideoPlayerConstants } from "../../../../mediaplayer/videoplayer/video-player.consts";
import { IPlayhead, IPlayheadTime } from "../../../../mediaplayer/playhead.interface";


export class VideoPlayerServiceMock{

    public playbackStateSubject = new BehaviorSubject(VideoPlayerConstants.PRELOADING);

    public playbackState = new BehaviorSubject(VideoPlayerConstants.PRELOADING);

    public startVideo$ = new BehaviorSubject(null);

    public ftPlaybackComplete$ = new BehaviorSubject(null);

    public stalled$: Subject<string> = new Subject();

    public playhead$: Observable<any> = of({
                                                    currentTime: 0,
                                                    startTime: 0,
                                                    id: "string",
                                                    type: "vod",
                                                    isBackground: true});

    public playbackFailed = () => {};
    public pause = () => {};
    public stop = () => {};
    public loadAsset = () => {};
    public videoPlayer = {};
    public init = () => {};
}
