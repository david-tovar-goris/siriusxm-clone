
import {throwError as observableThrowError,  of as observableOf,  BehaviorSubject } from 'rxjs';

export class AuthenticationServiceMock
{
    private static session = {
        username: "",
        authenticated: true,
        forgotPassword: false,
        startTrial: false,
        loggedOut: false,
        loginAttempts: 0,
        loginFaultCode: 0,
        loginFaultMessages: null,
        timeStamp: Date.now(),
        sessionID: "12345",
        accountLocked: false,
        remainingLockoutMinutes: 5,
        remainingLockoutSeconds: 20,
        activeOpenAccessSession: false,
        exited: false,
        openAccessStatus: "unavailable",
        activeFreeTierSession: false
    };

    public userSession = new BehaviorSubject([AuthenticationServiceMock.session]);
    public updateProfileData = jasmine.createSpy("updateProfileData");
    private static _spy = jasmine.createSpyObj("authenticationServiceSpy", [
        "login",
        "logout",
        "session",
        "isAuthenticated"
    ]);

    public static loginErrorMsg: string = "Login error";

    public static getSpy(): any
    {
        this._spy.session = AuthenticationServiceMock.session;
        this._spy.userSession = observableOf(AuthenticationServiceMock.session);

        return this._spy;
    }

    public static loginSuccess(): void
    {
        AuthenticationServiceMock.getSpy()
            .login.and.returnValue(observableOf(AuthenticationServiceMock.session));
    }

    public static loginFault(error: {}): void
    {
        error = error || AuthenticationServiceMock.loginErrorMsg;
        AuthenticationServiceMock.getSpy()
            .login.and.returnValue(observableThrowError(error));
    }
}
