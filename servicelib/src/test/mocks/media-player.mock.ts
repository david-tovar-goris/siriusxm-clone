import { Observable, of as observableOf } from "rxjs";
import { IMediaAssetMetadata } from "../../mediaplayer/media-asset-metadata.interface";
import { IMediaPlayer } from "../../mediaplayer/media-player.interface";
import { generateUUID } from "../../util/utilities";
import { playheadMock } from "./data/playhead.mock";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";

export const mediaPlayerMock: IMediaPlayer = {

    playbackState: Observable.create(),
    playhead$: observableOf(playheadMock),
    playbackComplete$: observableOf(false),
    isCurrent: true,


    play: () =>
    {
    },
    pause: (): void =>
    {
    },
    resume: () =>
    {
    },
    togglePausePlay: () =>
    {
        return new BehaviorSubject({});
    },
    seek: (timestamp: number) =>
    {
    },
    stop: (id?: string) =>
    {
    },
    getId: (): string =>
    {
        return generateUUID();
    },
    getDuration: (): number =>
    {
        return 0;
    },
    getDurationInSeconds: (): number =>
    {
        return 0;
    },
    getPlayheadZuluTime: (): number =>
    {
        return 0;
    },
    getPlayheadStartZuluTime: (): number =>
    {
        return 0;
    },
    getPlaybackType: (): string =>
    {
        return "";
    },
    getState: (): string =>
    {
        return "";
    },
    getType: (): string =>
    {
        return "";
    },
    getLastSeekTime: (): number =>
    {
        return 0;
    },
    getVolume: (): number =>
    {
        return 0;
    },
    setVolume: (value: number): void =>
    {
    },
    mute: (): void =>
    {
    },
    unmute: (): void =>
    {
    },
    getMediaAssetMetadata: (): IMediaAssetMetadata =>
    {
        return {
            mediaId:"",
            seriesName: "",
            episodeTitle: "",
            apronSegments: []
        };
    },
    getJumpActionCuts: () =>
    {
        return {
            from: {},
            to: {}
        };
    },
    destroy: () =>
    {
    },
    isPlaying: (): boolean =>
    {
        return true;
    },
    isPaused: (): boolean =>
    {
        return true;
    },
    isStopped: (): boolean =>
    {
        return true;
    },
    isFinished: (): boolean =>
    {
        return true;
    },
    isLive: (): boolean =>
    {
        return true;
    },
    warmUp: (): Observable<String> =>
    {
        return observableOf("");
    }
};
