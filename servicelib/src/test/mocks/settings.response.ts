export let tuneStartValue = {
    "name" : "TuneStart",
    "value": "off"
};

export const globalSettingsList = [
    {
        "settingName" : "AutoPlay",
        "settingValue": "off"
    },
    {
        "settingName" : "SearchDeletionTimestamp",
        "settingValue": "2017-03-07T21:00:11.875Z"
    },
    {
        "settingName" : "TuneStart",
        "settingValue": "off"
    }
];

export const globalSettingsListToBeUpdate = [
    {
        "settingName" : "AutoPlay",
        "settingValue": "off"
    },
    {
        "settingName" : "SearchDeletionTimestamp",
        "settingValue": "2017-03-07T21:00:11.875Z"
    },
    {
        "settingName" : "TuneStart",
        "settingValue": "off"
    }
];

export const deviceSettingsListToBeUpdate = [
    {
        "name" : "AutoPlay",
        "value": "off"
    },
    {
        "name" : "Crossfade",
        "value": "on"
    }
];

export const deviceSettingsListToBeExpected = [
    {
        "name" : "AutoPlay",
        "value": "off"
    },
    {
        "name" : "Crossfade",
        "value": "on"
    }
];

export const globalSettingsListToBeExpected = [
    {
        "name" : "AutoPlay",
        "value": "off"
    },
    {
        "name" : "SearchDeletionTimestamp",
        "value": "2017-03-07T21:00:11.875Z"
    },
    {
        "name" : "TuneStart",
        "value": "off"
    }
];

export const normalizedGlobalSettingsList = [
    {
        "name" : "AutoPlay",
        "value": "off"
    },
    {
        "name" : "SearchDeletionTimestamp",
        "value": "2017-03-07T21:00:11.875Z"
    },
    tuneStartValue
];

export const deviceSettingsList = [
    {
        "name" : "AutoPlay",
        "value": "off"
    },
    {
        "name" : "Crossfade",
        "value": "on"
    }
];

export const mockSettingsListResponseFromApi = [
    {
        "globalSettingList":
            {
                "globalSettings": globalSettingsList
            }
    },
    {
        "deviceSettingList":
            {
                "deviceSettings": deviceSettingsList
            }
    }
];

export const mockSettingsListResponseFromUpdateApi = {
    "globalSettingList":
        {
            "globalSettings": globalSettingsListToBeUpdate
        },
    "deviceSettingList":
        {
            "deviceSettings": deviceSettingsListToBeExpected
        }
};

export const mockSettingListResponse = {
    globalSettings: globalSettingsListToBeExpected,
    deviceSettings: deviceSettingsListToBeExpected
};

export const mockSettingsToBeUpdated = {
    globalSettings: normalizedGlobalSettingsList,
    deviceSettings: deviceSettingsListToBeUpdate
};
