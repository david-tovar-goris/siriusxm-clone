// tslint:disable max-line-length

import { INotification } from "../../../messaging/notification";

export const notificationMock: INotification = {
    notificationKey: {
        name: "FreeListening",
        version: "V1",
        guid: "a742bf57-3558-42a1-9357-63b7dd39d34b"
    },
    required: false,
    inAppOnly: false,
    immediate: false,
    confirmationUrl: "rest/v3/notification/confirm?action=FreeListening&version=V1&guid=a742bf57-3558-42a1-9357-63b7dd39d34b",
    bodyText: "You can now listen to exclusive commercial free music, plus sports, news talk and comedy now through September 5th, 2017",
    outOfAppText: "Listen for free until Sept 5, 2017",
    icon: "http://mountain-demo.s3-website-us-east-1.amazonaws.com/Sirius_Logo_Media%20SourcesMenu.png",
    titleText: "Listen to SiriusXM free for two weeks!",
    channelName: "",
    buttons: {
        primaryButton:{
            neriticLink:"App:SignUp",
            label:"SUBSCRIBE",
            analyticsTag:"",
            metricEventCode:"S"
         },
         secondaryButton:{
            neriticLink:"App:LinearTuner",
            label:"LISTEN NOW",
            analyticsTag:"",
            metricEventCode:"L"
         },
         tertiaryButton:{
            label:"REMIND ME LATER",
            analyticsTag:"",
            metricEventCode:"R"
         },
         dismissButton:{
            analyticsTag:"",
            metricEventCode:"X"
         }
    },
    leadKeyId: "LKJaSDF87234weNA99DPQMC99",
    displayed: false,
    messageType: "MARKETING",
    expirationDate: "2019-09-07T06:00:00.000Z",
    priority: 1,
    isMarketingMessage: true
};
