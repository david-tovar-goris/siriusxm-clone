
import { of as observableOf } from "rxjs";

export class MockMessagingService
{
    constructor(one, two) {}

    private messages = jasmine.createSpyObj('messages', [
        "push",
        "sort",
        "filter"
    ]);

    private headerSubscription = jasmine.createSpyObj('headerSubscription', [
        'unsubscribe'
    ]);

    public httpHeader = jasmine.createSpyObj('httpHeader', [
        "subscribe"
    ]);

    private notificationSubject = jasmine.createSpyObj('notificationSubject', [
        "next"
    ]);

    public notification = jasmine.createSpyObj('notification', [
        "subscribe"
    ]);

    public observeHeader = jasmine.createSpy('observeHeader');

    getNotification = jasmine.createSpy('getNotification');

    public subscribeToNotifications = jasmine.createSpy('subscribeToNotifications');

    public sendConfirm = jasmine.createSpy('sendConfirm').and.returnValue(observableOf({}));

    public sendFeedback = jasmine.createSpy('sendFeedback').and.returnValue(observableOf({}));

    public currentNotificationRequest = jasmine.createSpyObj('currentNotificationRequest', [
        "subscribe"
    ]);

}

export const messagingServiceMock = {
    messages: jasmine.createSpyObj('messages', [
        "push",
        "sort",
        "filter"
    ]),
    headerSubscription: jasmine.createSpyObj('headerSubscription', [
        'unsubscribe'
    ]),
    httpHeader: jasmine.createSpyObj('httpHeader', [
        "subscribe"
    ]),
    notificationSubject: jasmine.createSpyObj('notificationSubject', [
        "next"
    ]),
    notification: jasmine.createSpyObj('notification', [
        "subscribe"
    ]),
    observeHeader: jasmine.createSpy('observeHeader'),
    getNotification: jasmine.createSpy('getNotification'),
    subscribeToNotifications: jasmine.createSpy('subscribeToNotifications'),
    sendConfirm: jasmine.createSpy('sendConfirm').and.returnValue(observableOf({})),
    sendFeedback: jasmine.createSpy('sendFeedback').and.returnValue(observableOf({})),
    currentNotificationRequest: jasmine.createSpyObj('currentNotificationRequest', [
        "subscribe"
    ]),
    getButtonFromNotification: jasmine.createSpy('getButtonFromNotification')
};
