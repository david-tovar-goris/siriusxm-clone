/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
// tslint:disable max-line-length

import { IHttpHeader } from "../../../http/http.provider.response.interceptor";
import { of as observableOf } from "rxjs";
import { notificationResponseMock } from "../../../test/mocks/data/notification-response.mock";

export const mockHttpHeaderPayload: string = "NotificationHeaderVersion=1;Action=FreeListening;Required=false;Immediate=true;URL=rest%2Fv3%2Fnotification%2Fget%3Faction%3DFreeListening%26version%3DV1%26guid%3Da19a84d1-cbc2-4204-94a3-d13baa655b9f";

export const mockHttpHeader: IHttpHeader = {
    name: "X-SiriusXM-Notification",
    payload: mockHttpHeaderPayload
};

export class MockMessagingDelegate
{

    constructor(one) {}

    public notificationSubject = jasmine.createSpyObj('notificationSubject', [
        'next'
    ]);

    public notification = jasmine.createSpyObj('notification', [
        'subscribe'
    ]);

    public requestNotification = jasmine.createSpy('requestNotification')
        .and
        .returnValue(observableOf(mockHttpHeader));

    public normalizeResponse = jasmine.createSpy('normalizeResponse');

}
