export class MockStorageService
{
    public id = "1234567";

    public getItem = jasmine.createSpy( "getItem" ).and.callFake( () => this.id );
}
