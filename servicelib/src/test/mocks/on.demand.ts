export const mockAodShows = [{
    disableAllBanners: false,
    episodeCount     : 0,
    newEpisodeCount  : 1,
    showDescription  : {
        guid             : "guid",
        relatedChannelIds: [],
        creativeArts     : [],
        legacyIds        : { shortId: "yup ... short" }
    },
    episodes         : [{
        episodes: [{
            aodEpisodeGuid: "guid",
            channels      : [{
                channels: {
                    inactivityTimeout : undefined,
                    inactivityTimeouts: [{ platform: "web", value: 10000 }]
                }
            }],
            originalAirDate : "",
            duration      : "P0HR5M6S8.100",
            contentUrlList: { contentUrls: ["url "] },
            publicationInfo: {
                accessControlIdentifier: "422322323232"
            }
        }, {
            duration : undefined,
            aodEpisodeGuid: "guid2",
            originalAirDate : "",
            channels      : [{
                channels: {
                    inactivityTimeout : undefined,
                    inactivityTimeouts: [{ platform: "web", value: 10000 }]
                }
            }],
            contentUrlList: { contentUrls: ["url "] },
            publicationInfo: {
                accessControlIdentifier: "422322323232"
            }
        }
        ]
    }]
}];

export const mockVodEpisodes = [{
    vodEpisodeGuid : "guid",
    humanHostList : "FirstHost,SecondHost,ThirdHost",
    humanTopicList : "FirstTopic,SecondTopic,ThirdTopic"
}];

export const mockDiscoverAodRespoonse = { contentData: { aodShows: [{ shows: mockAodShows }] } };
export const mockDiscoverAssetsResponse = { contentData : { vodEpisodes : mockVodEpisodes } };
