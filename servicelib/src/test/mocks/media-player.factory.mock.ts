import { mediaPlayerMock } from "./media-player.mock";

export class MediaPlayerFactoryMock
{
    getMediaPlayer = () =>
    {
        return mediaPlayerMock;
    }
}
