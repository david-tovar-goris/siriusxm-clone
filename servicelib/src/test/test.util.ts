export const testObservable: Function = function(observable, expectations)
{
    observable
        .subscribe(
            function (response)
            {
                expectations(response);
            },
            function (err)
            {
                console.log("Error: " + err);
            });
};