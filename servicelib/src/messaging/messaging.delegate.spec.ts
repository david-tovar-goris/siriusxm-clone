/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { MessagingDelegate } from "./messaging.delegate";
import { MockHttpProvider } from "../test/mocks/http-provider.mock";
import {
    MockRequestInterceptor,
    MockResponseInterceptor
} from "../test/mocks/interceptors";
import { MockStorageService } from "../test/mocks/storage.service";
import { mockServiceConfig } from "../test/mocks/service.config";
import { IHttpHeader } from "../http/http.provider.response.interceptor";
import { Observable } from "rxjs";
import { notificationResponseMock } from "../test/mocks/data/notification-response.mock";
import { mockHttpHeader } from "../test/mocks/app-messaging/messaging-delegate.mock";
import { notificationMock } from "../test/mocks/app-messaging/notification.mock";
import { INotification } from "./notification";
import { ServiceEndpointConstants } from "../index";

describe('MessagingDelegate', function()
{
    beforeEach(function()
    {
        this.mockHttpProvider = new MockHttpProvider(
            MockRequestInterceptor,
            MockResponseInterceptor,
            MockStorageService,
            mockServiceConfig
        );

        this.delegate = new MessagingDelegate(this.mockHttpProvider);
    });

    afterEach(function()
    {
        this.delegate = null;
        this.mockHttpProvider = null;
    });

    it("Should exist", function()
    {
        expect(this.delegate).toBeTruthy();
    });

    describe("requestNotification()", function()
    {
        it("should be exposed", function()
        {
            expect(this.delegate.requestNotification).toBeDefined();
            expect(typeof this.delegate.requestNotification).toEqual('function');
        });

        it("should return an observable", function()
        {
            spyOn(this.delegate, 'requestNotification').and.callThrough();

            function getNotificationURL(header: IHttpHeader): string
            {
                const payloadArray: Array<string> = header.payload.split(";");
                const urlSection: string = payloadArray.find((item) => item.indexOf("URL") !== -1);
                const url: string = decodeURIComponent(urlSection.split("=")[1]);
                return (urlSection) ? url.slice(0, url.indexOf(",")) : ServiceEndpointConstants.endpoints.NOTIFICATION.V3_GET_NOTIFICATION;
            }

            let url: string = getNotificationURL(mockHttpHeader);

            const headerObservable = this.delegate.requestNotification(url);

            expect(headerObservable instanceof Observable).toEqual(true);
        });
    });

    describe("normalizeResponse()", function()
    {
        it("should be exposed", function()
        {
            expect(this.delegate.normalizeResponse).toBeDefined();
            expect(typeof this.delegate.normalizeResponse).toEqual('function');
        });

        it("should normalize response data", function()
        {
            const normalizedData: Array<INotification> = this.delegate.normalizeResponse(
                notificationResponseMock.ModuleListResponse.moduleList.modules[0].moduleResponse
            );

            expect(normalizedData[0]).toEqual(notificationMock);
        });
    });

});
