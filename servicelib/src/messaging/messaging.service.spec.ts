/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { MessagingService } from "./messaging.service";
import { BehaviorSubject } from "rxjs";
import { INotification } from "./notification";

describe('MessagingService', function()
{
    beforeEach(function()
    {
        this.headerSubject = new BehaviorSubject(null);
        this.delegateResponse = [];
        this.responseObserver = new BehaviorSubject(null);

        this.httpProvider = {
            addHttpHeaderObservable: jasmine.createSpy('addHttpHeaderObservable')
                .and
                .returnValue(
                    this.headerSubject
                )
        };
        this.messagingDelegate = {
            requestNotification: jasmine.createSpy('requestNotification')
                .and
                .returnValue(this.responseObserver)
        };

        this.messagingService = new MessagingService(
            this.httpProvider,
            this.messagingDelegate
        );
        spyOn(this.messagingService, 'emitMarketingMessage').and.callThrough();
    });

    describe('observeHeader()', function()
    {
        describe('when nothing expired', function()
        {
            beforeEach(function()
            {
                this.delegateResponse = [
                    {
                        isMarketingMessage: true,
                        titleText: 'Free Service 1',
                        expirationDate: '2031-09-05T20:07:41.919Z'
                    },
                    {
                        isMarketingMessage: false,
                        titleText: 'Howard Video Alert'
                    }
                ];

                this.responseObserver.next(this.delegateResponse);

                this.headerSubject.next({
                    name: 'x-siriusxm-notification',
                    payload: `NotificationHeaderVersion=1;
                          Action=ContinueListening;
                          Required=false;
                          Immediate=true;
                          URL=rest%2Fv3%2Fnotification%2Fget%3Faction%3DContinueListening%26version%3DV1%26guid%3Dd00fbb33`
                });
            });

            it('sets headerGuidCache to guid returned in url', function ()
            {
                expect(this.messagingService.headerGuidCache)
                    .toEqual(['d00fbb33']);
            });

            it('calls MessagingDelegate.requestNotification', function ()
            {
                expect(this.messagingDelegate.requestNotification)
                    .toHaveBeenCalledWith('rest/v3/notification/get?action=ContinueListening&version=V1&guid=d00fbb33');
            });

            it('does not make request if call with same guid has already been made', function ()
            {
                this.messagingDelegate.requestNotification.calls.reset();
                this.headerSubject.next({
                    name: 'x-siriusxm-notification',
                    payload: `NotificationHeaderVersion=1;
                          Action=ContinueListening;
                          Required=false;
                          Immediate=true;
                          URL=rest%2Fv3%2Fnotification%2Fget%3Faction%3DContinueListening%26version%3DV1%26guid%3Dd00fbb33`
                });
                expect(this.messagingDelegate.requestNotification).not.toHaveBeenCalled();
            });

            it('calls emitMarketingMessage()', function ()
            {
                expect(this.messagingService.emitMarketingMessage).toHaveBeenCalled();
            });

            it('emits an marketing message out of the observable', function (done)
            {
                this.messagingService.notification.subscribe((notification: INotification) =>
                {
                    expect(notification.titleText).toEqual('Free Service 1');
                    done();
                });
            });
        });

        describe('when some notifications have expired', function()
        {
            beforeEach(function()
            {
                this.delegateResponse = [
                {
                    isMarketingMessage: true,
                    titleText: 'Free Service 1',
                    expirationDate: '2017-09-05T20:07:41.919Z',
                    priority: 1
                },
                {
                    isMarketingMessage: true,
                    titleText: 'Free Service 2',
                    expirationDate: '2031-09-05T20:07:41.919Z',
                    priority: 2
                },
                {
                    isMarketingMessage: true,
                    titleText: 'Free Service 3',
                    expirationDate: '2017-09-05T20:07:41.919Z',
                    priority: 3
                },
                {
                    isMarketingMessage: false,
                    titleText: 'Howard Rebroadcast',
                    expirationDate: '2017-09-05T20:07:41.919Z'
                },
                {
                    isMarketingMessage: false,
                    titleText: 'Howard Video Alert',
                    expirationDate: '2031-09-05T20:07:41.919Z'
                },
                {
                    isMarketingMessage: false,
                    titleText: 'Howard Wrap Up Show',
                    expirationDate: '2017-09-05T20:07:41.919Z'
                }];
            });

            it('only emits unexpired show reminders', function(done)
            {
                this.messagingService.showRemindersSubject.subscribe((showReminder: INotification) =>
                {
                    expect(showReminder.titleText).toEqual('Howard Video Alert');
                    done();
                });

                this.responseObserver.next(this.delegateResponse);

                this.headerSubject.next({
                    name: 'x-siriusxm-notification',
                    payload: `NotificationHeaderVersion=1;
                      Action=ContinueListening;
                      Required=false;
                      Immediate=true;
                      URL=rest%2Fv3%2Fnotification%2Fget%3Faction%3DContinueListening%26version%3DV1%26guid%3Dd00fbb33`
                });
            });

            it('only emits unexpired marketing messages', function(done)
            {
                this.responseObserver.next(this.delegateResponse);

                this.headerSubject.next({
                    name: 'x-siriusxm-notification',
                    payload: `NotificationHeaderVersion=1;
                      Action=ContinueListening;
                      Required=false;
                      Immediate=true;
                      URL=rest%2Fv3%2Fnotification%2Fget%3Faction%3DContinueListening%26version%3DV1%26guid%3Dd00fbb33`
                });

                this.messagingService.notification.subscribe((marketingMsg: INotification) =>
                {
                    expect(marketingMsg.titleText).toEqual('Free Service 2');
                    done();
                });
            });
        });
    });
});
