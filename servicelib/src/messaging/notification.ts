export interface INotification
{
    notificationKey?: INotificationKey;
    required?: boolean;
    inAppOnly?: boolean;
    immediate?: boolean;
    confirmationUrl?: string;
    bodyText?: string;
    outOfAppText?: string;
    icon?: string;
    titleText?: string;
    channelName?: string;
    channelId?: string;
    buttons?: IButtons;
    leadKeyId?: string;
    displayed?: boolean;
    messageType?: string;
    expirationDate?: string;
    priority?: number;
    isMarketingMessage: boolean;
}

export interface INotificationKey
{
    name?: string;
    version?: string;
    guid?: string;
}

export interface IButtons
{
    primaryButton?: IButton;
    secondaryButton?: IButton;
    tertiaryButton?: IButton;
    dismissButton?: IButton;
}

export interface IButton
{
    neriticLink?: string;
    label?: string;
    analyticsTag?: string;
    metricEventCode?: string;
}

export interface INotificationFeedback
{
    leadKeyId?: string;
    metricEventCode?: string;
    guid?: string;
    timeStamp?: string;
    inApp?: boolean;
    push?: boolean;
}

export interface INotificationInformation {
    open: boolean;
    data: INotification;
}
