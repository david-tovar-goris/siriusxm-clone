/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import * as _ from "lodash";
import { Observable ,  BehaviorSubject, of as observableOf } from "rxjs";
import { take } from 'rxjs/operators';
import { mock } from "ts-mockito";
import { CurrentlyPlayingService } from "../currently-playing/currently.playing.service";
import {LiveTimeService, MediaPlayerConstants, AuthenticationService} from '../index';
import { MediaPlayerFactory } from "../mediaplayer/media-player.factory";
import { IMediaPlayer } from "../mediaplayer/media-player.interface";
import { ProfileService } from "../profile/profile.service";
import { SettingsService } from "../settings/settings.service";
import { AuthenticationServiceMock } from "../test/mocks/authetication.service.mock";
import { mediaPlayerMock } from "../test/mocks/media-player.mock";
import { TuneService } from "../tune/tune.service";
import { DateUtil } from "../util/date.util";
import { ConsumeActionConsts } from "./consume-action.const";
import { ConsumeEventConsts } from "./consume-event.const";
import { ConsumeDelegate } from "./consume.delegate";
import { IConsumeRequest } from "./consume.interface";
import { ConsumeService } from "./consume.service";
import { IPlayhead } from "../mediaplayer";
import { ChromecastService } from "../chromecast/chromecast.service";
import { MediaTimeLineService } from "../media-timeline/media.timeline.service";
import { ContentTypes } from "../index";

describe("Consume service test suite", function()
{
    const live: string = "live";
    const playheadTime: number = 1000;
    const cutEntryTimestamp: number = 1000;
    const playheadStartTime: number = 2000;
    const zuluStartTime: number = 3000;
    const zuluEndTime: number = 4000;
    const consumptionInfo: string = "consumptionInfo";
    const time: string = "time";
    const mediaType: string = "live";
    const mediaId: string = "12134213423";
    const oldCut: any = {
        times: {
            zuluStartTime: zuluStartTime,
            zuluEndTime: zuluEndTime
        }
    };
    const newCut: any = {
        times: {
            zuluStartTime: zuluStartTime,
            zuluEndTime: zuluEndTime
        }
    };

    beforeEach(function()
    {
        this.consumeService = mock(ConsumeService);
        this.consumeDelegate = mock(ConsumeDelegate);
        this.settingsService = mock(SettingsService);
        this.mediaTimeLineService = mock(MediaTimeLineService);
        this.currentlyPlayingService = mock(CurrentlyPlayingService);
        this.authenticationService = new AuthenticationServiceMock();
        this.mediaPlayerFactory = mock(MediaPlayerFactory);
        this.liveTimeService = mock(LiveTimeService);
        this.mediaPlayer= mediaPlayerMock;
        this.chromecastService = mock(ChromecastService);
        this.mockProfileService = null;

        const nowPlaying                             = {
            mediaType:mediaType,
            cut: {
                consumptionInfo: consumptionInfo
            }
        };
        this.currentlyPlayingService.currentlyPlayingData = observableOf(nowPlaying);
        this.mediaTimeLineService.mediaTimeLine                = Observable.create();
        this.mediaTimeLineService.mediaTimeLine                = observableOf({ mediaType: mediaType, mediaId: mediaId });

        this.mediaPlayer.getPlaybackType = jasmine.createSpy("getPlaybackType")
            .and
            .callFake(() =>
            {
                return live;
            });

        this.mediaPlayer.playbackState = observableOf(MediaPlayerConstants.STOPPED);

        this.mediaPlayer.getPlayheadStartZuluTime = jasmine.createSpy("getPlayheadStartZuluTime")
            .and
            .callFake(() =>
            {
                return playheadStartTime;
            });

        this.mediaPlayer.isLive = jasmine.createSpy("isLive")
            .and
            .callFake(() =>
            {
                return true;
            });

        this.mockProfileService = mock(ProfileService);
        this.mockProfileService.profileData = {
            subscribe: (result: Function, fault: Function) => result({

            })
        };


        this.mediaPlayerFactory.currentMediaPlayer = observableOf(this.mediaPlayer);

        this.consumeDelegate.consume = jasmine.createSpy("consume")
            .and
            .callFake((consumeRequest: IConsumeRequest) =>
            {
                return observableOf(true);
            });

        this.chromecastService.state$ = Observable.create(function() { });

        this.consumeService = new ConsumeService(this.consumeDelegate,
            this.settingsService,
            this.mediaTimeLineService,
            this.currentlyPlayingService,
            this.authenticationService,
            this.mediaPlayerFactory,
            this.mockProfileService,
            this.liveTimeService,
            this.chromecastService);
    });

    describe("tuneIn() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(_.isFunction(this.consumeService.tuneIn)).toBeTruthy();
            });

            it("Should return an observable.", function()
            {
                expect(this.consumeService.tuneIn(this.mediaPlayer, mediaType) instanceof Observable).toBeTruthy();
            });

        });

        describe("Execution >>>", function()
        {
            it("Should pass the delegate a consume request with event = `markerStart` and action = `tuneStart`.", function()
            {
                this.mediaPlayer.isLive = jasmine.createSpy("isLive")
                    .and
                    .callFake(() =>
                    {
                        return true;
                    });

                this.settingsService.isGlobalSettingOn = jasmine.createSpy("isGlobalSettingOn")
                    .and
                    .callFake(() =>
                    {
                        return true;
                    });

                this.consumeDelegate.consume = jasmine.createSpy("consume")
                    .and
                    .callFake((consumeRequests: IConsumeRequest[]) =>
                    {
                        let consumeRequest = consumeRequests[0];
                        expect(consumeRequest.consumeEvent.event).toEqual(ConsumeEventConsts.MARKER_START);
                        expect(consumeRequest.consumeEvent.action).toEqual(ConsumeActionConsts.TUNE_START);
                        expect(consumeRequest.consumptionInfo).toEqual(consumptionInfo);
                        expect(consumeRequest.mediaType).toEqual(mediaType);

                        return observableOf(true);
                    });

                this.testSubject = new ConsumeService(this.consumeDelegate,
                    this.settingsService,
                    this.mediaTimeLineService,
                    this.currentlyPlayingService,
                    this.authenticationService,
                    this.mediaPlayerFactory,
                    this.mockProfileService,
                    this.liveTimeService,
                    this.chromecastService);

                this.testSubject.tuneIn(this.mediaPlayer, mediaType);
            });

            it("Should pass the delegate a consume request with event = `start` and action = `tuneIn`.", function()
            {
                this.mediaPlayer.isLive = jasmine.createSpy("isLive")
                    .and
                    .callFake(() =>
                    {
                        return false;
                    });

                this.settingsService.isGlobalSettingOn = jasmine.createSpy("isGlobalSettingOn")
                    .and
                    .callFake(() =>
                    {
                        return false;
                    });

                this.consumeDelegate.consume = jasmine.createSpy("consume")
                    .and
                    .callFake((consumeRequests: IConsumeRequest[]) =>
                    {
                        let consumeRequest = consumeRequests[0];
                        expect(consumeRequest.consumeEvent.event).toEqual(ConsumeEventConsts.START);
                        expect(consumeRequest.consumeEvent.action).toEqual(ConsumeActionConsts.TUNE_IN);
                        expect(consumeRequest.consumptionInfo).toEqual(consumptionInfo);
                        expect(consumeRequest.mediaType).toEqual(mediaType);

                        return observableOf(true);
                    });

                this.testSubject = new ConsumeService(this.consumeDelegate,
                    this.settingsService,
                    this.mediaTimeLineService,
                    this.currentlyPlayingService,
                    this.authenticationService,
                    this.mediaPlayerFactory,
                    this.mockProfileService,
                    this.liveTimeService,
                    this.chromecastService);

                this.testSubject.tuneIn(this.mediaPlayer, mediaType);
            });

            it("Should pass the delegate a consume request with event = `markerStart` and action = `tuneIn`.", function()
            {
                this.mediaPlayer.isLive = jasmine.createSpy("isLive")
                    .and
                    .callFake(() =>
                    {
                        return false;
                    });

                this.settingsService.isGlobalSettingOn = jasmine.createSpy("isGlobalSettingOn")
                    .and
                    .callFake(() =>
                    {
                        return false;
                    });

                this.mediaPlayer.getPlayheadStartZuluTime = jasmine.createSpy("getPlayheadStartZuluTime")
                    .and
                    .callFake(() =>
                    {
                        return 0;
                    });

                this.consumeDelegate.consume = jasmine.createSpy("consume")
                    .and
                    .callFake((consumeRequests: IConsumeRequest[]) =>
                    {
                        let consumeRequest = consumeRequests[0];
                        expect(consumeRequest.consumeEvent.event).toEqual(ConsumeEventConsts.MARKER_START);
                        expect(consumeRequest.consumeEvent.action).toEqual(ConsumeActionConsts.TUNE_IN);
                        expect(consumeRequest.consumptionInfo).toEqual(consumptionInfo);
                        expect(consumeRequest.mediaType).toEqual(mediaType);

                        return observableOf(true);
                    });

                this.testSubject = new ConsumeService(this.consumeDelegate,
                    this.settingsService,
                    this.mediaTimeLineService,
                    this.currentlyPlayingService,
                    this.authenticationService,
                    this.mediaPlayerFactory,
                    this.mockProfileService,
                    this.liveTimeService,
                    this.chromecastService);
                this.testSubject.tuneIn(this.mediaPlayer, mediaType);
            });
        });

        describe("Exception handling >>>", function()
        {
            beforeEach(function()
            {
                // Create the test subject.
                this.testSubject = new ConsumeService(this.consumeDelegate, this.settingsService, this.mediaTimeLineService,
                    this.currentlyPlayingService, this.authenticationService, this.mediaPlayerFactory, this.mockProfileService,
                    this.liveTimeService, this.chromecastService);
            });

            it("Should throw an error when the media player is falsy.", function()
            {
                const expected: string = ConsumeService.ERROR_INVALID_MEDIA_PLAYER;

                expect(() =>
                {
                    this.testSubject.tuneIn(null, mediaType);
                }).toThrowError(expected);
            });

            it("Should throw an error when the media type is falsy.", function()
            {
                const expected: string = ConsumeService.ERROR_INVALID_MEDIA_TYPE;

                expect(() =>
                {
                    this.testSubject.tuneIn(this.mediaPlayer, null);
                }).toThrowError(expected);
            });
        });
    });

    describe("pause() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                // Create the test subject.
                this.testSubject = new ConsumeService(this.consumeDelegate,
                    this.settingsService,
                    this.mediaTimeLineService,
                    this.currentlyPlayingService,
                    this.authenticationService,
                    this.mediaPlayerFactory,
                    this.mockProfileService,
                    this.liveTimeService,
                    this.chromecastService);

                expect(_.isFunction(this.testSubject.pause)).toBeTruthy();
            });

            it("Should return an observable.", function()
            {
                this.consumeDelegate.consume = jasmine.createSpy("consume")
                    .and
                    .callFake((consumeRequests: IConsumeRequest[]) =>
                    {
                        return observableOf(true);
                    });

                this.testSubject = new ConsumeService(this.consumeDelegate,
                    this.settingsService,
                    this.mediaTimeLineService,
                    this.currentlyPlayingService,
                    this.authenticationService,
                    this.mediaPlayerFactory,
                    this.mockProfileService,
                    this.liveTimeService,
                    this.chromecastService);

                expect(this.testSubject.pause(this.mediaPlayer, mediaType) instanceof Observable).toBeTruthy();
            });

        });

        describe("Execution >>>", function()
        {
            it("Should pass the delegate a consume request with event = `stop` and action = `pause`.", function()
            {
                this.mediaPlayer.isLive = jasmine.createSpy("isLive")
                    .and
                    .callFake(() =>
                    {
                        return true;
                    });

                this.settingsService.isGlobalSettingOn = jasmine.createSpy("isGlobalSettingOn")
                    .and
                    .callFake(() =>
                    {
                        return true;
                    });

                this.consumeDelegate.consume = jasmine.createSpy("consume")
                    .and
                    .callFake((consumeRequests: IConsumeRequest[]) =>
                    {
                        let consumeRequest = consumeRequests[0];
                        expect(consumeRequest.consumeEvent.event).toEqual(ConsumeEventConsts.STOP);
                        expect(consumeRequest.consumeEvent.action).toEqual(ConsumeActionConsts.PAUSE);
                        expect(consumeRequest.consumptionInfo).toEqual(consumptionInfo);
                        expect(consumeRequest.mediaType).toEqual(mediaType);

                        return observableOf(true);
                    });

                this.testSubject = new ConsumeService(this.consumeDelegate,
                    this.settingsService,
                    this.mediaTimeLineService,
                    this.currentlyPlayingService,
                    this.authenticationService,
                    this.mediaPlayerFactory,
                    this.mockProfileService,
                    this.liveTimeService,
                    this.chromecastService);

                this.testSubject.pause(this.mediaPlayer, mediaType);
            });
        });

        describe("Exception handling >>>", function()
        {
            beforeEach(function()
            {
                // Create the test subject.
                this.testSubject = new ConsumeService(this.consumeDelegate,
                    this.settingsService,
                    this.mediaTimeLineService,
                    this.currentlyPlayingService,
                    this.authenticationService,
                    this.mediaPlayerFactory,
                    this.mockProfileService,
                    this.liveTimeService,
                    this.chromecastService);
            });

            it("Should throw an error when the media player is falsy.", function()
            {
                const expected: string = ConsumeService.ERROR_INVALID_MEDIA_PLAYER;

                expect(() =>
                {
                    this.testSubject.pause(null, null);
                }).toThrowError(expected);
            });

        });
    });

    describe("resume() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                // Create the test subject.
                this.testSubject = new ConsumeService(this.consumeDelegate,
                    this.settingsService,
                    this.mediaTimeLineService,
                    this.currentlyPlayingService,
                    this.authenticationService,
                    this.mediaPlayerFactory,
                    this.mockProfileService,
                    this.liveTimeService,
                    this.chromecastService);

                expect(_.isFunction(this.testSubject.resume)).toBeTruthy();
            });

            it("Should return an observable.", function()
            {
                this.consumeDelegate.consume = jasmine.createSpy("consume")
                    .and
                    .callFake((consumeRequest: IConsumeRequest) =>
                    {
                        return observableOf(true);
                    });

                this.testSubject = new ConsumeService(this.consumeDelegate, this.settingsService, this.mediaTimeLineService,
                    this.currentlyPlayingService, this.authenticationService, this.mediaPlayerFactory,  this.mockProfileService,
                    this.liveTimeService, this.chromecastService);
                expect(this.testSubject.resume(this.mediaPlayer, mediaType) instanceof Observable).toBeTruthy();
            });

        });

        describe("Execution >>>", function()
        {
            beforeEach(function()
            {
                spyOn(DateUtil, 'nowInISO8601').and.returnValue('now');
            });

            it("Should call consume.tuneIn() since it hasn't been called yet for this player ID.", function()
            {
                this.mediaPlayer.isLive = jasmine.createSpy("isLive")
                    .and
                    .callFake(() =>
                    {
                        return true;
                    });

                this.mediaPlayer.playhead$ = new BehaviorSubject({ currentTime: { zuluMilliseconds: 1481737300000 } } as IPlayhead);

                this.settingsService.isGlobalSettingOn = jasmine.createSpy("isGlobalSettingOn")
                    .and
                    .callFake(() =>
                    {
                        return true;
                    });

                this.consumeDelegate.consume = jasmine.createSpy("consume")
                    .and
                    .callFake((consumeRequest: IConsumeRequest) =>
                    {
                        return observableOf(true);
                    });

                this.testSubject = new ConsumeService(this.consumeDelegate, this.settingsService,this.mediaTimeLineService,
                    this.currentlyPlayingService, this.authenticationService, this.mediaPlayerFactory,  this.mockProfileService,
                    this.liveTimeService, this.chromecastService);
                spyOn(this.testSubject, "tuneIn").and.callThrough();

                this.testSubject.resume(this.mediaPlayer, mediaType);
                expect(this.testSubject.tuneIn).toHaveBeenCalledWith(this.mediaPlayer, mediaType);
            });

            it("Should pass the delegate a consume request with event = `start` and action = `resume`.", function()
            {
                this.mediaPlayer.isLive = jasmine.createSpy("isLive")
                    .and
                    .callFake(() =>
                    {
                        return true;
                    });

                this.mediaPlayer.playhead$ = new BehaviorSubject({ currentTime: { zuluMilliseconds: 1481737300000 } } as IPlayhead);

                this.mediaPlayer.getId = jasmine.createSpy("getId")
                    .and
                    .callFake(() =>
                    {
                        return "ID";
                    });

                this.settingsService.isGlobalSettingOn = jasmine.createSpy("isGlobalSettingOn")
                    .and
                    .callFake(() =>
                    {
                        return true;
                    });

                this.consumeDelegate.consume = jasmine.createSpy("consume")
                    .and
                    .callFake((consumeRequest: IConsumeRequest) =>
                    {
                        let consumeRequest1 = consumeRequest[0];
                        expect(consumeRequest1.consumeEvent.event).toEqual(consumeRequestResult.consumeEvent.event);
                        expect(consumeRequest1.consumeEvent.action).toEqual(consumeRequestResult.consumeEvent.action);
                        expect(consumeRequest1.consumptionInfo).toEqual(consumeRequestResult.consumptionInfo);
                        expect(consumeRequest1.consumeDateTime).toEqual(consumeRequestResult.consumeDateTime);
                        expect(consumeRequest1.consumeStreamDateTime).toEqual(consumeRequestResult.consumeStreamDateTime);
                        expect(consumeRequest1.mediaType).toEqual(consumeRequestResult.mediaType);

                        return observableOf(true);
                    });

                const consumeRequestResult = {
                    consumeEvent: {
                        event: ConsumeEventConsts.START,
                        action: ConsumeActionConsts.RESUME
                    },
                    consumptionInfo: consumptionInfo,
                    consumeDateTime: 'now',
                    consumeStreamDateTime: '2016-12-14T17:41:40.000Z',
                    mediaType: mediaType
                };

                this.testSubject = new ConsumeService(this.consumeDelegate, this.settingsService, this.mediaTimeLineService,
                    this.currentlyPlayingService, this.authenticationService, this.mediaPlayerFactory,  this.mockProfileService,
                    this.liveTimeService, this.chromecastService);

                expect(this.testSubject.tuneIn(this.mediaPlayer, mediaType) instanceof Observable).toBeTruthy();
                expect(this.testSubject.resume(this.mediaPlayer, mediaType) instanceof Observable).toBeTruthy();

            });
        });

        describe("Exception handling >>>", function()
        {
            beforeEach(function()
            {
                // Create the test subject.
                this.testSubject = new ConsumeService(this.consumeDelegate, this.settingsService, this.mediaTimeLineService,
                    this.currentlyPlayingService, this.authenticationService, this.mediaPlayerFactory, this.mockProfileService,
                    this.liveTimeService, this.chromecastService);
            });

            it("Should throw an error when the media player is falsy.", function()
            {
                const expected: string = ConsumeService.ERROR_INVALID_MEDIA_PLAYER;

                expect(() =>
                {
                    this.testSubject.resume(null, null);
                }).toThrowError(expected);
            });

        });
    });

    describe("tuneOut() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                // Create the test subject.
                this.testSubject = new ConsumeService(this.consumeDelegate, this.settingsService, this.mediaTimeLineService,
                    this.currentlyPlayingService, this.authenticationService, this.mediaPlayerFactory, this.mockProfileService,
                    this.liveTimeService, this.chromecastService);

                expect(_.isFunction(this.testSubject.tuneOut)).toBeTruthy();
            });

            it("Should return an observable.", function()
            {
                this.consumeDelegate.consume = jasmine.createSpy("consume")
                    .and
                    .callFake((consumeRequest: IConsumeRequest) =>
                    {
                        return observableOf(true);
                    });

                this.testSubject = new ConsumeService(this.consumeDelegate, this.settingsService, this.mediaTimeLineService,
                    this.currentlyPlayingService, this.authenticationService, this.mediaPlayerFactory, this. mockProfileService,
                    this.liveTimeService, this.chromecastService);
                expect(this.testSubject.tuneOut(this.mediaPlayer, mediaType) instanceof Observable).toBeTruthy();
            });

        });

        describe("Execution >>>", function()
        {
            it("Should pass the delegate a consume request with event = `end` and action = `tuneOut`.", function()
            {
                this.mediaPlayer.isLive = jasmine.createSpy("isLive")
                    .and
                    .callFake(() =>
                    {
                        return true;
                    });

                this.settingsService.isGlobalSettingOn = jasmine.createSpy("isGlobalSettingOn")
                    .and
                    .callFake(() =>
                    {
                        return true;
                    });

                this.consumeDelegate.consume = jasmine.createSpy("consume")
                    .and
                    .callFake((consumeRequests: IConsumeRequest[]) =>
                    {
                        let consumeRequest = consumeRequests[0];
                        expect(consumeRequest.consumeEvent.event).toEqual(ConsumeEventConsts.END);
                        expect(consumeRequest.consumeEvent.action).toEqual(ConsumeActionConsts.TUNE_OUT);
                        expect(consumeRequest.consumptionInfo).toEqual(consumptionInfo);
                        expect(consumeRequest.mediaType).toEqual(mediaType);

                        return observableOf(true);
                    });

                this.testSubject = new ConsumeService(this.consumeDelegate,
                    this.settingsService,
                    this.mediaTimeLineService,
                    this.currentlyPlayingService,
                    this.authenticationService,
                    this.mediaPlayerFactory,
                    this.mockProfileService,
                    this.liveTimeService,
                    this.chromecastService);

                this.testSubject.tuneOut(this.mediaPlayer, mediaType);
            });
        });

        describe("Exception handling >>>", function()
        {
            beforeEach(function()
            {
                // Create the test subject.
                this.testSubject = new ConsumeService(this.consumeDelegate, this.settingsService, this.mediaTimeLineService,
                    this.currentlyPlayingService, this.authenticationService, this.mediaPlayerFactory,  this.mockProfileService,
                    this.liveTimeService, this.chromecastService);
            });

            it("Should throw an error when the media player is falsy.", function()
            {
                const expected: string = ConsumeService.ERROR_INVALID_MEDIA_PLAYER;

                expect(() =>
                {
                    this.testSubject.tuneOut(null, null);
                }).toThrowError(expected);
            });

        });
    });

    describe("runConsume()", function()
    {
        describe("when no opts passed", function()
        {
            beforeEach(function()
            {
                spyOn(this.mediaPlayer, 'getPlayheadZuluTime').and.returnValue(1481737300000);
                this.consumeDelegate.consume = jasmine.createSpy('consume');
                this.consumeService.runConsume(
                    [ ConsumeEventConsts.END + "/" + ConsumeActionConsts.FORWARD
                        , ConsumeEventConsts.MARKER_START + "/" + ConsumeActionConsts.FORWARD ]
                    , this.mediaPlayer
                    , mediaType
                );
            });

            it('first request event/action is correct', function()
            {
                let requests = this.consumeDelegate.consume.calls.argsFor(0)[0];
                expect(requests[0].consumeEvent).toEqual({
                    event: ConsumeEventConsts.END,
                    action: ConsumeActionConsts.FORWARD
                });
            });

            it('second request event/action is correct', function()
            {
                let requests = this.consumeDelegate.consume.calls.argsFor(0)[0];
                expect(requests[1].consumeEvent).toEqual({
                    event: ConsumeEventConsts.MARKER_START,
                    action: ConsumeActionConsts.FORWARD
                });
            });

            it('sends consumption info', function()
            {
                let requests = this.consumeDelegate.consume.calls.argsFor(0)[0];
                expect(requests[0].consumptionInfo).toEqual(consumptionInfo);
                expect(requests[1].consumptionInfo).toEqual(consumptionInfo);
            });

            it('sends consumeStreamDateTime to media player time', function()
            {
                let requests = this.consumeDelegate.consume.calls.argsFor(0)[0];
                expect(requests[0].consumeStreamDateTime).toEqual("2016-12-14T17:41:40.000Z");
                expect(requests[0].consumeStreamDateTime).toEqual("2016-12-14T17:41:40.000Z");
            });
        });

        describe("when opts passed", function()
        {
            beforeEach(function()
            {
                spyOn(this.mediaPlayer, 'getPlayheadZuluTime').and.returnValue(1481737345000);
                this.consumeDelegate.consume = jasmine.createSpy('consume');
            });

            it('when from option passed, first request consumeStreamDateTime is from value', function()
            {
                this.consumeService.runConsume(["end/fwd", "markerStart/fwd"], this.mediaPlayer, mediaType, { from: 1481737300000 });
                let requests = this.consumeDelegate.consume.calls.argsFor(0)[0];
                expect(requests[0].consumeStreamDateTime).toEqual("2016-12-14T17:41:40.000Z");
            });

            it('when from option passed, second request consumeStreamDateTime is mediaPlayer.getPlayheadZuluTime value', function()
            {
                this.consumeService.runConsume(["end/fwd", "markerStart/fwd"], this.mediaPlayer, mediaType, { from: 1481737300000 });
                let requests = this.consumeDelegate.consume.calls.argsFor(0)[0];
                expect(requests[1].consumeStreamDateTime).toEqual("2016-12-14T17:42:25.000Z");
            });
        });
    });

    describe("checkForPassiveCutChange()", function()
    {
        beforeEach(function()
        {
            spyOn(this.consumeService, 'runConsume').and.returnValue(observableOf(true));
            this.data1 = {
                cut: {
                    mediaId : "channel",
                    consumptionInfo: 'info1',
                    lastPlaybackTimestamp : 1500,
                    times: { zuluStartTime: 1000, zuluEndTime: 2000 },
                    assetGUID: 'assetGUID1',
                    duration: 200
                },
                cutEntryTimestamp: 1999,
                mediaType: 'aod'

            };
            this.data2 = {
                cut: {
                    mediaId : "channel",
                    consumptionInfo: 'info2',
                    lastPlaybackTimestamp : 2500,
                    times: { zuluStartTime: 2000, zuluEndTime: 3000 },
                    assetGUID: 'assetGUID2',
                    duration: 200
                },
                cutEntryTimestamp: 2000,
                mediaType: 'aod'
            };
        });

        describe("when called once", function()
        {
            beforeEach(function()
            {
                this.consumeService.checkForPassiveCutChange(this.data1, this.mediaPlayer);
            });

            it("does not call runConsume", function() { expect(this.consumeService.runConsume).not.toHaveBeenCalled(); });
        });

        describe("when one set of data is null", function()
        {
            it("null first does not call runConsume", function()
            {
                this.consumeService.checkForPassiveCutChange(null, this.mediaPlayer);
                this.consumeService.checkForPassiveCutChange(this.data1, this.mediaPlayer);
                expect(this.consumeService.runConsume).not.toHaveBeenCalled();
            });

            it("null second does not call runConsume", function()
            {
                this.consumeService.checkForPassiveCutChange(this.data1, this.mediaPlayer);
                this.consumeService.checkForPassiveCutChange(null, this.mediaPlayer);
                expect(this.consumeService.runConsume).not.toHaveBeenCalled();
            });
        });

        describe("when cutEntryTimestamp difference is greater than MAX_DIFFERENCE", function()
        {
            beforeEach(function()
            {
                this.data2.cut.lastPlaybackTimestamp =
                    this.data1.cut.lastPlaybackTimestamp + ConsumeService.MAXIMUM_PASSIVE_CUT_CHANGE_TIME_DIFFERENCE + 1;
                this.consumeService.checkForPassiveCutChange(this.data1, this.mediaPlayer);
                this.consumeService.checkForPassiveCutChange(this.data2, this.mediaPlayer);
            });

            it("does not call runConsume", function() { expect(this.consumeService.runConsume).not.toHaveBeenCalled(); });
        });

        describe("when cutEntryTimestamp is small enough but assetGUIDS are the same", function()
        {
            beforeEach(function()
            {
                this.data2.cut.assetGUID = this.data1.cut.assetGUID;
                this.consumeService.checkForPassiveCutChange(this.data1, this.mediaPlayer);
                this.consumeService.checkForPassiveCutChange(this.data2, this.mediaPlayer);
            });

            it("does not call runConsume", function() { expect(this.consumeService.runConsume).not.toHaveBeenCalled(); });
        });

        describe("when assetGUIDS are different and time difference small enough", function()
        {
            beforeEach(function()
            {
                this.consumeService.checkForPassiveCutChange(this.data1, this.mediaPlayer);
                this.consumeService.checkForPassiveCutChange(this.data2, this.mediaPlayer);
            });

            it("calls runConsume with correct events and actions", function()
            {
                expect(this.consumeService.runConsume.calls.argsFor(0)[0]).toEqual([
                    `${ ConsumeEventConsts.MARKER_END }/${ ConsumeActionConsts.PASSIVE }`,
                    `${ ConsumeEventConsts.MARKER_START }/${ ConsumeActionConsts.PASSIVE }`
                ]);
            });

            it("calls runConsume with correct mediaType", function()
            {
                expect(this.consumeService.runConsume.calls.argsFor(0)[2]).toEqual('aod');
            });

            it("calls runConsume with correct opts", function()
            {
                expect(this.consumeService.runConsume.calls.argsFor(0)[3]).toEqual({
                    from: 2000,
                    to: 2000,
                    fromInfo: 'info1',
                    toInfo: 'info2'
                });
            });
        });

        describe('when additional channels', function()
        {
            beforeEach(function()
            {
                this.data1 = {
                    cut: {
                        mediaId : "gwgw-1",
                        consumptionInfo: 'info1',
                        lastPlaybackTimestamp : 249500,
                        assetGUID: 'assetGUID1',
                        duration: 250
                    },
                    cutEntryTimestamp: 1999,
                    mediaType: ContentTypes.ADDITIONAL_CHANNELS

                };

                this.data2 = {
                    cut: {
                        mediaId : "gwgw-1",
                        consumptionInfo: 'info2',
                        lastPlaybackTimestamp : 13000,
                        assetGUID: 'assetGUID2',
                        duration: 300
                    },
                    cutEntryTimestamp: 2000,
                    mediaType: ContentTypes.ADDITIONAL_CHANNELS
                };
            });

            it('calls runConsume when at least one asset is different than jump action assets', function()
            {
                spyOn(this.mediaPlayer, 'getJumpActionCuts').and.returnValue({
                    from: { assetGUID: 'assetGUID3'},
                    to: this.data2.cut
                });
                this.consumeService.checkForPassiveCutChange(this.data1, this.mediaPlayer);
                this.consumeService.checkForPassiveCutChange(this.data2, this.mediaPlayer);

                expect(this.consumeService.runConsume.calls.argsFor(0)[0]).toEqual([
                    `${ ConsumeEventConsts.MARKER_END }/${ ConsumeActionConsts.PASSIVE }`,
                    `${ ConsumeEventConsts.MARKER_START }/${ ConsumeActionConsts.PASSIVE }`
                ]);
            });

            it('does not call runConsume when assets are same as jumpActionAssets', function()
            {
                spyOn(this.mediaPlayer, 'getJumpActionCuts').and.returnValue({
                    from: this.data1.cut,
                    to: this.data2.cut
                });
                this.consumeService.checkForPassiveCutChange(this.data1, this.mediaPlayer);
                this.consumeService.checkForPassiveCutChange(this.data2, this.mediaPlayer);

                expect(this.consumeService.runConsume).not.toHaveBeenCalled();
            });



            it('does not call runConsume when assets are the same', function()
            {
                this.data2.cut.assetGUID = 'assetGUID1';
                this.consumeService.checkForPassiveCutChange(this.data1, this.mediaPlayer);
                this.consumeService.checkForPassiveCutChange(this.data2, this.mediaPlayer);

                expect(this.consumeService.runConsume).not.toHaveBeenCalled();
            });
        });
    });


    describe("consume on change in mediaPlayer plabackstate", function()
    {
        it("tuneout if the state is Finished", function()
        {
            spyOn(this.consumeService, 'tuneOut').and.returnValue(observableOf(true));
            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.FINISHED, mediaType);
            expect(this.consumeService.tuneOut).toHaveBeenCalled();
        });

        it("tuneIn if the state is PLAYING and Previous state is BUFFERING", function()
        {
            spyOn(this.consumeService, 'tuneIn').and.returnValue(observableOf(true));

            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.BUFFERING, mediaType);
            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PLAYING, mediaType);
            expect(this.consumeService.tuneIn).toHaveBeenCalled();

        });

        it("tuneIn if the state is PLAYING and Previous state is PRELOADING", function()
        {
            spyOn(this.consumeService, 'tuneIn').and.returnValue(observableOf(true));

            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PRELOADING,mediaType);
            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PLAYING, mediaType);
            expect(this.consumeService.tuneIn).toHaveBeenCalled();

        });

        it("tuneIn if the state is PLAYING and Previous state is FINISHED - retune to same content", function()
        {
            spyOn(this.consumeService, 'tuneIn').and.returnValue(observableOf(true));

            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.FINISHED, mediaType);
            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PLAYING, mediaType);
            expect(this.consumeService.tuneIn).toHaveBeenCalled();

        });

        it("tuneIn if the state is PLAYING and Previous state is PLAY - on app load", function()
        {
            spyOn(this.consumeService, 'tuneIn').and.returnValue(observableOf(true));

            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PLAY, mediaType);
            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PLAYING, mediaType);
            expect(this.consumeService.tuneIn).toHaveBeenCalled();

        });

        it("resume if the state is PLAYING and Previous state is PAUSED - on resume", function()
        {
            spyOn(this.consumeService, 'resume').and.returnValue(observableOf(true));

            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PAUSED, mediaType);
            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PLAYING, mediaType);
            expect(this.consumeService.resume).toHaveBeenCalled();

        });

        it("Don't make consume call on initial PAUSED state with PRELOADING previous state", function()
        {
            spyOn(this.consumeService, 'pause').and.returnValue(observableOf(true));

            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PRELOADING, mediaType);
            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PAUSED, mediaType);

            expect(this.consumeService.pause).not.toHaveBeenCalled();

        });

        it("pause if the state is PAUSED - on pause", function()
        {
            spyOn(this.consumeService, 'pause').and.returnValue(observableOf(true));

            this.consumeService.handlePlaybackState(this.mediaPlayer, MediaPlayerConstants.PAUSED, mediaType);
            expect(this.consumeService.pause).toHaveBeenCalled();

        });

    });


    describe("tuneout on on session exited/webplayer closed", function()
    {
        it("tuneout on  session exited/webplayer closed", function()
        {
            spyOn(this.consumeService, 'tuneOut').and.returnValue(observableOf(true));

            this.authenticationService.userSession.pipe(take(1)).subscribe((userSession) =>
                                                                {
                                                                    userSession.exited = true;
                                                                    this.authenticationService.userSession.next(userSession);
                                                                });
            expect(this.consumeService.tuneOut).toHaveBeenCalled();
        });

    });
});
