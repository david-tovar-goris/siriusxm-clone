export interface IConsumeEvent
{
    event: string;
    action: string;
}

export interface IConsumeConfig
{
    playbackType: string;
    playhead: number;
    playheadStart: number;
    isLive: boolean;
    consumeEvent?: IConsumeEvent;
    consumptionInfo?: string;
    consumeDateTime?: string;
    consumeStreamDateTime?: string;
    mediaType?: string;
}

export interface IConsumeRequest
{
    consumeEvent: IConsumeEvent;
    consumptionInfo: string;
    consumeDateTime: string;
    consumeStreamDateTime: string;
    mediaType: string;
    aodDownload?: boolean;
    offline?: boolean;
}
