/**
 * This is known as a barrel file. It allows developers to package and rollup multiple exports
 * into a single file and thus single import/export; e.g., `export * from "./authentication/service/";`
 */
export * from "./consume.delegate";
export * from "./consume.service";
export * from "./consume.interface";
export * from "./consume-action.const";
export * from "./consume-event.const";
