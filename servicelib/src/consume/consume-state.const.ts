export class ConsumeStateConsts
{
    public static START: string = "start";
    public static STOP: string = "stop";
    public static PAUSE: string = "pause";
    public static RESUME: string = "resume";
    public static MARKER_START: string = "markerStart";
    public static MARKER_END: string = "markerEnd";
}
