/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import * as _ from "lodash";
import { Observable, of as observableOf } from "rxjs";
import { mock } from "ts-mockito";
import { HttpProvider } from "../http/http.provider";
import { ConsumeConsts } from "../service/consts/api.request.consts";
import { ContentTypes } from "../service/types/content.types";
import { ConsumeDelegate } from "./consume.delegate";
import { IConsumeRequest } from "./consume.interface";

describe("Consume delegate test suite", function()
{
    const event: string = "event";
    const action: string = "action";
    const consumptionInfo: string = "consumptionInfo";
    const consumeDateTime: string = "consumeDateTime";
    const consumeStreamDateTime: string = "consumeStreamDateTime";
    const aodDownload: boolean = false;
    const offline: boolean = false;
    const unknownMediaType: string = "unknownMediaType";

    beforeEach(function()
    {
        this.http = mock(HttpProvider);
        this.consumeRequests = [];
        this.configService = {
            consumeConfiguration: { enableV2Consume: true, enableV4Consume: false }
        };

        this.consumeRequests = [{
            consumeEvent: {
                event: "event",
                action: "action"
            },
            consumptionInfo: "consumptionInfo",
            consumeDateTime: "consumeDateTime",
            consumeStreamDateTime: "consumeStreamDateTime",
            mediaType: ContentTypes.LIVE_AUDIO
        }];

        this.testSubject = new ConsumeDelegate(this.http, this.configService);
    });

    afterEach(function()
    {
        this.testSubject = null;
        this.consumeRequests = null;
    });

    describe("Infrastructure >>", function()
    {
        it("Should have test subject", function()
        {
            expect(this.testSubject).toBeDefined();
        });

        it("Constructor", function()
        {
            expect(this.testSubject).toBeDefined();
            expect(this.testSubject instanceof ConsumeDelegate).toEqual(true);
        });

    });

    describe("consume() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.testSubject).toBeDefined();
                expect(_.isFunction(this.testSubject.consume)).toBeTruthy();
            });

            it("Should return an observable.", function()
            {
                this.http.postModuleAreaRequest =
                    jasmine.createSpy("postModuleAreaRequest")
                        .and
                        .callFake((url, request, config) =>
                        {
                            return observableOf(true);
                        });

                expect(this.testSubject.consume(this.consumeRequests) instanceof Observable).toBeTruthy();
            });

        });

        describe("Execution >>>", function()
        {
            it("Should return the API response object without any changes.", function()
            {
                this.http.postModuleAreaRequest =
                    jasmine.createSpy("postModuleAreaRequest")
                        .and
                        .callFake((url, request, config) =>
                        {
                            return observableOf(true);
                        });

                this.testSubject.consume(this.consumeRequests).subscribe(data =>
                {
                    expect(data).toEqual(true);
                });
            });

            it("Should create the expected API request object for live audio.", function()
            {
                let consumeRequest = this.consumeRequests[0];
                consumeRequest.consumeEvent = {
                    event: event,
                    action: action
                };
                consumeRequest.consumptionInfo = consumptionInfo;
                consumeRequest.consumeDateTime = consumeDateTime;
                consumeRequest.consumeStreamDateTime = consumeStreamDateTime;
                consumeRequest.mediaType = ContentTypes.LIVE_AUDIO;
                consumeRequest.aodDownload = aodDownload;
                consumeRequest.offline = offline;

                this.http.postModuleAreaRequest =
                    jasmine.createSpy("postModuleAreaRequest")
                        .and
                        .callFake((url, request, config) =>
                        {
                            const consumeRequest: any = _.get(request, "moduleRequest.consumeRequests[0]");

                            expect(request.moduleArea).toEqual(ConsumeConsts.MODULE_AREA);
                            expect(request.moduleType).toEqual(ConsumeConsts.MODULE_TYPE_LIVE);
                            expect(consumeRequest.consumeEvent).toEqual(event);
                            expect(consumeRequest.consumeEventAction).toEqual(action);
                            expect(consumeRequest.consumptionInfo).toEqual(consumptionInfo);
                            expect(consumeRequest.consumeDateTime).toEqual(consumeDateTime);
                            expect(consumeRequest.consumeStreamDateTime).toEqual(consumeStreamDateTime);
                            expect(consumeRequest.aodDownload).toEqual(aodDownload);
                            expect(consumeRequest.offline).toEqual(offline);

                            return observableOf(true);
                        });

                this.testSubject.consume([consumeRequest]);
            });

            it("Should create the expected API request object for on demand audio.", function()
            {
                let consumeRequest = this.consumeRequests[0];
                consumeRequest.consumeEvent = {
                    event: event,
                    action: action
                };
                consumeRequest.consumptionInfo = consumptionInfo;
                consumeRequest.consumeDateTime = consumeDateTime;
                consumeRequest.consumeStreamDateTime = consumeStreamDateTime;
                consumeRequest.mediaType = ContentTypes.AOD;
                consumeRequest.aodDownload = aodDownload;
                consumeRequest.offline = offline;

                this.http.postModuleAreaRequest =
                    jasmine.createSpy("postModuleAreaRequest")
                        .and
                        .callFake((url, request, config) =>
                        {
                            const consumeRequest: any = _.get(request, "moduleRequest.consumeRequests[0]");

                            expect(request.moduleArea).toEqual(ConsumeConsts.MODULE_AREA);
                            expect(request.moduleType).toEqual(ConsumeConsts.MODULE_TYPE_AOD);
                            expect(consumeRequest.consumeEvent).toEqual(event);
                            expect(consumeRequest.consumeEventAction).toEqual(action);
                            expect(consumeRequest.consumptionInfo).toEqual(consumptionInfo);
                            expect(consumeRequest.consumeDateTime).toEqual(consumeDateTime);
                            expect(consumeRequest.consumeStreamDateTime).toEqual(consumeStreamDateTime);
                            expect(consumeRequest.aodDownload).toEqual(aodDownload);
                            expect(consumeRequest.offline).toEqual(offline);

                            return observableOf(true);
                        });

                this.testSubject.consume([consumeRequest]);
            });

            it("Should create the expected API request object for live video.", function()
            {
                let consumeRequest = this.consumeRequests[0];
                consumeRequest.consumeEvent = {
                    event: event,
                    action: action
                };
                consumeRequest.consumptionInfo = consumptionInfo;
                consumeRequest.consumeDateTime = consumeDateTime;
                consumeRequest.consumeStreamDateTime = consumeStreamDateTime;
                consumeRequest.mediaType = ContentTypes.LIVE_VIDEO;
                consumeRequest.aodDownload = aodDownload;
                consumeRequest.offline = offline;

                this.http.postModuleAreaRequest =
                    jasmine.createSpy("postModuleAreaRequest")
                        .and
                        .callFake((url, request, config) =>
                        {
                            const consumeRequest: any = _.get(request, "moduleRequest.consumeRequests[0]");

                            expect(request.moduleArea).toEqual(ConsumeConsts.MODULE_AREA);
                            expect(request.moduleType).toEqual(ConsumeConsts.MODULE_TYPE_VLIVE);
                            expect(consumeRequest.consumeEvent).toEqual(event);
                            expect(consumeRequest.consumeEventAction).toEqual(action);
                            expect(consumeRequest.consumptionInfo).toEqual(consumptionInfo);
                            expect(consumeRequest.consumeDateTime).toEqual(consumeDateTime);
                            expect(consumeRequest.consumeStreamDateTime).toEqual(consumeStreamDateTime);
                            expect(consumeRequest.aodDownload).toEqual(aodDownload);
                            expect(consumeRequest.offline).toEqual(offline);

                            return observableOf(true);
                        });

                this.testSubject.consume([consumeRequest]);
            });

            it("Should create the expected API request object for on demand video.", function()
            {
                let consumeRequest = this.consumeRequests[0];
                consumeRequest.consumeEvent = {
                    event: event,
                    action: action
                };
                consumeRequest.consumptionInfo = consumptionInfo;
                consumeRequest.consumeDateTime = consumeDateTime;
                consumeRequest.consumeStreamDateTime = consumeStreamDateTime;
                consumeRequest.mediaType = ContentTypes.VOD;
                consumeRequest.aodDownload = aodDownload;
                consumeRequest.offline = offline;

                this.http.postModuleAreaRequest =
                    jasmine.createSpy("postModuleAreaRequest")
                        .and
                        .callFake((url, request, config) =>
                        {
                            const consumeRequest: any = _.get(request, "moduleRequest.consumeRequests[0]");

                            expect(request.moduleArea).toEqual(ConsumeConsts.MODULE_AREA);
                            expect(request.moduleType).toEqual(ConsumeConsts.MODULE_TYPE_VOD);
                            expect(consumeRequest.consumeEvent).toEqual(event);
                            expect(consumeRequest.consumeEventAction).toEqual(action);
                            expect(consumeRequest.consumptionInfo).toEqual(consumptionInfo);
                            expect(consumeRequest.consumeDateTime).toEqual(consumeDateTime);
                            expect(consumeRequest.consumeStreamDateTime).toEqual(consumeStreamDateTime);
                            expect(consumeRequest.aodDownload).toEqual(aodDownload);
                            expect(consumeRequest.offline).toEqual(offline);

                            return observableOf(true);
                        });

                this.testSubject.consume([consumeRequest]);
            });
        });

        describe("Exception handling >>>", function()
        {
            it("Should throw an error when the media type is unknown.", function()
            {
                const expected: string = ConsumeDelegate.ERROR_UNKNOWN_MEDIA_TYPE + unknownMediaType;
                let consumeRequest = this.consumeRequests[0];

                consumeRequest.consumeEvent = {
                    event: event,
                    action: action
                };
                consumeRequest.consumptionInfo = consumptionInfo;
                consumeRequest.consumeDateTime = consumeDateTime;
                consumeRequest.consumeStreamDateTime = consumeStreamDateTime;
                consumeRequest.mediaType = unknownMediaType;
                consumeRequest.aodDownload = aodDownload;
                consumeRequest.offline = offline;

                expect(() =>
                {
                    this.testSubject.consume([consumeRequest]);
                }).toThrowError(expected);
            });

            it("Should throw an error when the request object can't be created due to null consumeEvent.", function()
            {
                const expected: string = ConsumeDelegate.ERROR_CANNOT_CREATE_REQUEST;
                let consumeRequest = this.consumeRequests[0];

                consumeRequest.consumeEvent = null;
                consumeRequest.consumptionInfo = consumptionInfo;
                consumeRequest.consumeDateTime = consumeDateTime;
                consumeRequest.consumeStreamDateTime = consumeStreamDateTime;
                consumeRequest.mediaType = ContentTypes.VOD;
                consumeRequest.aodDownload = aodDownload;
                consumeRequest.offline = offline;

                expect(() =>
                {
                    this.testSubject.consume([consumeRequest]);
                }).toThrowError(expected);
            });

        });

        describe('when xtra channel ', function()
        {
            beforeEach(function()
            {
                this.consumeRequests[0].mediaType = ContentTypes.ADDITIONAL_CHANNELS;
                this.http.postModuleAreaRequest = jasmine
                    .createSpy('postModuleAreaRequest')
                    .and.returnValue(observableOf(true));
            });

            it('sends aic module type in request', function(done)
            {
                this.testSubject.consume(this.consumeRequests)
                    .subscribe(() =>
                    {
                        expect(this.http.postModuleAreaRequest.calls.argsFor(0)[1].moduleType)
                            .toEqual(
                                ConsumeConsts.MODULE_TYPE_AIC
                            );
                        done();
                });
            });
        });
    });
});
