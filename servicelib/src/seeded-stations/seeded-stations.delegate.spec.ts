import { SeededStationsDelegate } from './seeded-stations.delegate';
import { of as observableOf } from "rxjs";
import { SeededStationUpdateItem } from "./seeded-stations.interface";
import { ServiceEndpointConstants } from "../service/consts/service.endpoint.constants";
import { IHttpRequestConfig } from "../http/types/http.types";

describe('Seeded Stations Deligate Test Suite >> ', function()
{
    const sampleSeededStation: SeededStationUpdateItem = {
        stationId: 'testStationId',
        stationFactory: 'testStationFactory',
        changeType: 'testChangeType'
    };
    const gupId =  "testGupId";

    beforeEach( function()
    {
        this.mockHttp =
            {
                postModuleAreaRequest : jasmine.createSpy( "postModuleAreaRequest" )
            };
        this.seededStationsDelegate = new SeededStationsDelegate( this.mockHttp );
    });

    describe( "Infrastructure >> ", function()
    {
        it( "Constructor", function()
        {
            expect( this.seededStationsDelegate instanceof SeededStationsDelegate ).toBe( true );
        });
    });

    describe( "removeSeededStation method >> ", function()
    {
        it("should call removeSeededStations method", function()
        {
            spyOn(this.seededStationsDelegate, 'removeSeededStations');
            this.seededStationsDelegate.removeSeededStation(gupId, sampleSeededStation);
            expect(this.seededStationsDelegate.removeSeededStations).toHaveBeenCalledWith(gupId, [sampleSeededStation]);
        });
    });

    describe( "removeSeededStations() >> ", function()
    {
        it("calls to removeSeededStations method", function()
        {
            this.mockHttp.postModuleAreaRequest = jasmine.createSpy( "postModuleAreaRequest" )
                       .and
                       .callFake((url, data, config) =>
                       {
                           const mockConfig: IHttpRequestConfig = {
                               params: {
                                   gupID : gupId
                               }
                           };
                           const mockRequest = {
                               stationsList: [sampleSeededStation]
                           };
                           expect(url).toBe(ServiceEndpointConstants.endpoints.SEEDED_RADIO.V4_REMOVE_SEEDED_STATION);
                           expect(data.moduleArea).toEqual("Profile");
                           expect(data.moduleType).toEqual("seededStations");
                           expect(data.moduleRequest).toEqual(mockRequest);
                           expect(config).toEqual(mockConfig);
                           return observableOf([sampleSeededStation]);
                       });
            this.seededStationsDelegate.removeSeededStations(gupId, [sampleSeededStation]).subscribe(x => {});
            expect( this.mockHttp.postModuleAreaRequest ).toHaveBeenCalled();
        });
    });
});
