import { of as observableOf } from 'rxjs';
import { SeededStationsService } from './seeded-stations.service';
import { throwError } from "rxjs/internal/observable/throwError";

describe("Seeded Stations Service Test Suite >> ", function()
{
    const testStationId = 'testStationId';
    const testStationFactoryId = 'resetSeededStationRemovedData';

    beforeEach( function()
    {
        this.mockSeededStationsDelegate = {
            removeSeededStation: () =>
            {
                return observableOf([]);
            },
            removeSeededStations: () =>
            {
                return observableOf([]);
            }
        };
        this.mockProfileService = {
            getGupId: () =>
            {
                return 'testGupID';
            }
        };
        this.seededStationsService = new SeededStationsService(this.mockSeededStationsDelegate, this.mockProfileService);
    });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.seededStationsService instanceof SeededStationsService).toEqual(true);
        });
    });

    describe("removeSeededStation >> ", function()
    {
        it("removeSeededStation on incorrect inputs / Error", function()
        {
            this.seededStationsService.removeSeededStation('', testStationFactoryId).subscribe((response) =>
            {
                expect(response).toEqual(false);
            });
        });

        it('removeSeededStation on success', function()
        {
            spyOn(this.mockSeededStationsDelegate, "removeSeededStation")
                .and.callFake(() =>
                {
                    return observableOf(true);
                });
            let processSeededStationsResponse =  spyOn<any>(this.seededStationsService, 'processSeededStationsResponse')
                .and.callFake((response, stationId, removeAllStations) =>
                {
                    response.subscribe(data =>
                    {
                        expect(data).toEqual(true);
                    });
                    expect(stationId).toEqual(testStationId);
                    expect(removeAllStations).toEqual(false);
                    return observableOf(true);
                });
            this.seededStationsService.removeSeededStation(testStationId, testStationFactoryId).subscribe((response) =>
            {
                expect(response).toEqual(true);
            });
            expect(this.mockSeededStationsDelegate.removeSeededStation).toHaveBeenCalled();
            expect(processSeededStationsResponse).toHaveBeenCalled();
        });
    });

    describe("removeAllSeededStations >> ", function()
    {
        it('removeAllSeededStations on Success', function()
        {
            spyOn(this.mockSeededStationsDelegate, "removeSeededStations")
                .and.callFake(() =>
            {
                return observableOf(true);
            });
            let processSeededStationsResponse =  spyOn<any>(this.seededStationsService, 'processSeededStationsResponse')
                .and.callFake((response, stationId, removeAllStations) =>
                {
                    response.subscribe(data =>
                    {
                        expect(data).toEqual(true);
                    });
                    expect(stationId).toEqual("");
                    expect(removeAllStations).toEqual(true);
                    return observableOf(true);
                });
            this.seededStationsService.removeAllSeededStations([]).subscribe((response) =>
            {
                expect(response).toEqual(true);
            });
            expect(processSeededStationsResponse).toHaveBeenCalled();
            expect(this.mockSeededStationsDelegate.removeSeededStations).toHaveBeenCalled();
        });
    });

    describe("processSeededStationsResponse >> ", function()
    {
        it("on success response from removeSeededStation()", function()
        {
            spyOn(this.mockSeededStationsDelegate, "removeSeededStation")
                .and.callFake(() =>
            {
                return observableOf(true);
            });
            this.seededStationsService.removeSeededStation(testStationId, testStationFactoryId).subscribe((response) =>
            {
                expect(response).toEqual(true);
            });
        });

        it("on error response from removeSeededStation method", function()
        {
            spyOn(this.mockSeededStationsDelegate, "removeSeededStation")
                .and.callFake(() =>
            {
                return throwError('ERROR');
            });
            this.seededStationsService.removeSeededStation(testStationId, testStationFactoryId).subscribe((response) =>
            {
                expect(response).toEqual(false);
            });
        });
    });

    describe("resetSeededStationRemovedData >> ", function()
    {
        it("initial seededStationRemovedSubject should be null", function()
        {
            expect(this.seededStationsService.seededStationRemovedSubject.getValue()).toEqual(null);
        });

        it("seededStationRemovedSubject should be reset after calling resetSeededStationRemovedData method", function()
        {
            this.seededStationsService.seededStationRemovedSubject.next({
                stationId: testStationId,
                removeAllSeededStations: true
            });
            this.seededStationsService.resetSeededStationRemovedData();
            expect(this.seededStationsService.seededStationRemovedSubject.getValue()).toEqual({
                stationId: "",
                removeAllSeededStations: false
            });
        });
    });
});
