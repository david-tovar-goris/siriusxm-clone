/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { ResumeDelegate } from "./resume.delegate";
import {
    ApiLayerTypes,
    ContentTypes, DeepLinkTypes, IAppConfig
} from "../index";
import {mockServiceConfig} from "../test/mocks/service.config";
import { chromeCastModelMock } from "../test/mocks/chromecast/chromecast.model.mock";

describe( "ResumeDelegate Test Suite >>", function()
{
    const pausePointVOD = {
        contentType : ContentTypes.VOD,
        channelName : "ChannelName",
        channelId   : "id",
        timestamp   : 1,
        caId        : "caid"
    };

    const pausePointAOD = {
        contentType : ContentTypes.AOD,
        channelName : "ChannelName",
        channelId   : "id",
        timestamp   : 1,
        publicInfoIdentifier: "publicInfoIdentifier",
        caId: "publicInfoIdentifier"
    };

    const pausePointLA = {
        contentType : ContentTypes.LIVE_AUDIO,
        channelName : "ChannelName",
        channelId   : "id",
        timestamp   : 1,
        caId        : "caid"
    };

    const pausePointLV = {
        contentType : DeepLinkTypes.LIVE_VIDEO,
        channelName : "ChannelName",
        channelId   : "id",
        timestamp   : 1
    };

    const pausePointSeeded = {
        contentType : ContentTypes.SEEDED_RADIO,
        channelName : "ChannelName",
        channelId   : "id"
    };

    const deepLinkAOD = {
        id : pausePointAOD.publicInfoIdentifier,
        type : ContentTypes.AOD
    };

    const deepLinkLA = {
        id : pausePointLA.caId,
        type : ContentTypes.LIVE_AUDIO
    };

    const deepLinkVOD = {
        id: pausePointVOD.caId,
        type: ContentTypes.VOD
    };

    const deepLinkLV = {
        id: pausePointLV.channelName,
        type: DeepLinkTypes.LIVE_VIDEO
    };

    const deepLinkSeeded = {
        id: 'id',
        type: DeepLinkTypes.PANDORA
    };

    const deepLinkAIC = {
        id: pausePointSeeded.channelName,
        type: DeepLinkTypes.AIC
    };

    beforeEach( function()
                {
                    this.mockHttp =
                    {
                        get               : jasmine.createSpy( "get" ),
                        post              : jasmine.createSpy( "post" ),
                        postModuleRequest : jasmine.createSpy( "postModuleRequest" )
                    };
                    this.appConfig = JSON.parse(JSON.stringify(mockServiceConfig));
                    this.delegate = new ResumeDelegate( this.mockHttp, this.appConfig, chromeCastModelMock );
                } );

    describe( "Infrastructure >> ", function()
    {
        it( "Should have a test subject.", function()
        {
            expect( this.delegate instanceof ResumeDelegate ).toBe( true );
        } );
    } );

    describe( "`resume()` >> ", function()
    {
        describe( "Infrastructure >> ", function()
        {
            it( "Should forward the call to http", function()
            {
                expect( this.delegate.resume ).toBeDefined();
            } );
        } );

        describe( "calls to resume >> ", function()
        {
            it ("with invalid pause point, no deeplink should set url params NO CONTENT", function()
            {
                this.mockHttp.postModuleRequest =
                    jasmine.createSpy( "post" )
                        .and
                        .callFake((url, data, config) =>
                        {
                            expect(config.params[ApiLayerTypes.CONTENT_TYPE])
                                .toEqual(ApiLayerTypes.NO_CONTENT);
                            expect(config.params[ApiLayerTypes.TIMESTAMP])
                                .toEqual(undefined);
                            expect(config.params[ApiLayerTypes.CONDITIONAL_ACCESS_ID])
                                .toEqual(undefined);
                            expect(config.params[ApiLayerTypes.CHANNEL_ID])
                                .toEqual(undefined);
                        });

                this.delegate.resume(undefined, undefined, undefined, false);

                expect( this.mockHttp.postModuleRequest ).toHaveBeenCalled();
            });

            it ("with SEEDED_RADIO should set url param on the request", function()
            {
                this.mockHttp.postModuleRequest =
                    jasmine.createSpy( "post" )
                        .and
                        .callFake((url, data, config) =>
                        {
                            expect(config.params[ApiLayerTypes.CONTENT_TYPE])
                                .toEqual(pausePointSeeded.contentType);
                            expect(config.params[ApiLayerTypes.CONDITIONAL_ACCESS_ID])
                                .toEqual(undefined);
                            expect(config.params[ApiLayerTypes.TIMESTAMP])
                                .toEqual(undefined);
                            expect(config.params[ApiLayerTypes.CHANNEL_ID])
                                .toEqual(undefined);
                        });
                this.delegate.resume( undefined, false, deepLinkSeeded, true );

                expect( this.mockHttp.postModuleRequest ).toHaveBeenCalled();
            });

            it ("with DeepLink of AIC pause point should set url param on the request", function()
            {
                this.mockHttp.postModuleRequest =
                    jasmine.createSpy( "post" )
                        .and
                        .callFake((url, data, config) =>
                        {
                            expect(config.params[ApiLayerTypes.CONTENT_TYPE])
                                .toEqual(ContentTypes.AIC);
                            expect(config.params[ApiLayerTypes.CONDITIONAL_ACCESS_ID])
                                .toEqual(undefined);
                            expect(config.params[ApiLayerTypes.TIMESTAMP])
                                .toEqual(undefined);
                            expect(config.params[ApiLayerTypes.CHANNEL_ID])
                                .toEqual(undefined);
                        });
                this.delegate.resume( undefined, false, deepLinkAIC, true );

                expect( this.mockHttp.postModuleRequest ).toHaveBeenCalled();
            });

            it( "with AOD pause point should set the pause point url param on the request", function()
            {
                this.mockHttp.postModuleRequest =
                    jasmine.createSpy( "post" )
                        .and
                        .callFake((url, data, config) =>
                        {
                            expect(config.params[ApiLayerTypes.CONTENT_TYPE])
                                .toEqual(pausePointAOD.contentType);
                            expect(config.params[ApiLayerTypes.TIMESTAMP])
                                .toEqual(pausePointAOD.timestamp);
                            expect(config.params[ApiLayerTypes.CONDITIONAL_ACCESS_ID])
                                .toEqual(pausePointAOD.publicInfoIdentifier);
                            expect(config.params[ApiLayerTypes.CHANNEL_ID])
                                .toEqual(pausePointAOD.channelId);
                        });
                this.delegate.resume( pausePointAOD, false, deepLinkAOD, true );

                expect( this.mockHttp.postModuleRequest ).toHaveBeenCalled();
            } );

            it( "with LA pause point should set the pause point url param on the request", function()
            {
                this.mockHttp.postModuleRequest =
                    jasmine.createSpy( "post" )
                           .and
                           .callFake((url, data, config) =>
                                     {
                                         expect(config.params[ApiLayerTypes.CONTENT_TYPE])
                                             .toEqual(pausePointLA.contentType);
                                         expect(config.params[ApiLayerTypes.TIMESTAMP])
                                             .toEqual(pausePointLA.timestamp);
                                         expect(config.params[ApiLayerTypes.CHANNEL_ID])
                                             .toEqual(pausePointLA.channelId);
                                         expect(config.params[ApiLayerTypes.CONDITIONAL_ACCESS_ID])
                                             .toBe(undefined);
                                     });
                this.delegate.resume( pausePointLA );

                expect( this.mockHttp.postModuleRequest ).toHaveBeenCalled();
            } );

            it( "with VOD pause point should set the pause point url param on the request", function()
            {
                this.mockHttp.postModuleRequest =
                    jasmine.createSpy( "post" )
                        .and
                        .callFake((url, data, config) =>
                        {
                            expect(config.params[ApiLayerTypes.CONTENT_TYPE])
                                .toEqual(pausePointVOD.contentType);
                            expect(config.params[ApiLayerTypes.TIMESTAMP])
                                .toEqual(pausePointVOD.timestamp);
                            expect(config.params[ApiLayerTypes.CONDITIONAL_ACCESS_ID])
                                .toEqual(pausePointVOD.caId);
                            expect(config.params[ApiLayerTypes.CHANNEL_ID])
                                .toEqual(pausePointVOD.channelId);
                        });
                this.delegate.resume( pausePointVOD, false, deepLinkVOD, true );

                expect( this.mockHttp.postModuleRequest ).toHaveBeenCalled();
            } );

            it( "with LV pause point should set the pause point url param on the request", function()
            {
                this.mockHttp.postModuleRequest =
                    jasmine.createSpy( "post" )
                        .and
                        .callFake((url, data, config) =>
                        {
                            expect(config.params[ApiLayerTypes.CONTENT_TYPE])
                                .toEqual(pausePointLV.contentType);
                            expect(config.params[ApiLayerTypes.TIMESTAMP])
                                .toEqual(pausePointLV.timestamp);
                            expect(config.params[ApiLayerTypes.CONDITIONAL_ACCESS_ID])
                                .toBe(undefined);
                            expect(config.params[ApiLayerTypes.CHANNEL_ID])
                                .toEqual(pausePointLV.channelId);
                        });
                this.delegate.resume( pausePointLV, false, deepLinkLV, true );

                expect( this.mockHttp.postModuleRequest ).toHaveBeenCalled();
            } );

            it( "with deep link should set the deep link url param on the request", function()
            {
                this.mockHttp.postModuleRequest = jasmine.createSpy( "post" )
                                                    .and
                                                    .callFake( ( url, data, config ) =>
                                                               {
                                                                   expect( config.params[ ApiLayerTypes.DEEP_LINK_ID ] )
                                                                       .toEqual( deepLinkLA.id );
                                                                   expect( config.params[ ApiLayerTypes.CONTENT_TYPE ] )
                                                                       .toEqual( deepLinkLA.type );
                                                               } );

                this.delegate.resume( pausePointLA, null, deepLinkLA, false );
            } );

            it( "with deep link should set the deep link url param on the request", function()
            {
                this.mockHttp.postModuleRequest = jasmine.createSpy( "post" )
                    .and
                    .callFake( ( url, data, config ) =>
                    {
                        expect( config.params[ ApiLayerTypes.DEEP_LINK_ID ] )
                            .toEqual( deepLinkLA.id );
                        expect( config.params[ ApiLayerTypes.CONTENT_TYPE ] )
                            .toEqual( deepLinkLA.type );
                    } );

                this.delegate.resume( pausePointLA, null, deepLinkLA, false );
            } );

            it( "with open access should set the open access url param on the request", function()
            {
                this.mockHttp.postModuleRequest = jasmine.createSpy( "post" )
                                                    .and
                                                    .callFake( ( url, data, config ) =>
                                                               {
                                                                   expect( config.params[ ApiLayerTypes.OPEN_ACCESS_TRIAL ] )
                                                                       .toEqual( true );
                                                               } );
                this.delegate.resume( null, true, null );

                expect( this.mockHttp.postModuleRequest ).toHaveBeenCalled();
            } );

            it("sets adsEligible param to true based on SERVICE_CONFIG's adsWizzSupported property and if player type is local", function()
            {
                this.mockHttp.postModuleRequest = jasmine.createSpy( "post" )
                                                         .and
                                                         .callFake( ( url, data, config ) =>
                                                         {
                                                             expect(config.params.adsEligible)
                                                                 .toEqual(this.delegate.SERVICE_CONFIG.adsWizzSupported);
                                                         });
                this.delegate.SERVICE_CONFIG.adsWizzSupported = true;
                this.delegate.resume( pausePointLA, null, deepLinkLA, false );
            });

            it("adsEligible param does not exist in config.params if SERVICE_CONFIG's adsWizzSupported property is false", function()
            {
                this.mockHttp.postModuleRequest = jasmine.createSpy( "post" )
                                                         .and
                                                         .callFake( ( url, data, config ) =>
                                                         {
                                                             expect(Object.keys(config.params).includes('adsEligible')).toEqual(false);
                                                         });
                this.delegate.SERVICE_CONFIG.adsWizzSupported = false;
                this.delegate.resume( pausePointLA, null, deepLinkLA, false );
            });
        } );
    } );
} );
