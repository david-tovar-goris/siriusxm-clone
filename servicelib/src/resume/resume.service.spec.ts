import {
    throwError as observableThrowError,
    of as observableOf,
    BehaviorSubject,
    Observable
} from 'rxjs';
import { map, first, takeLast } from 'rxjs/operators';
import {
    mockNowPlayingResponse,
    mockAODResponse, mockAICResponse, mockSRResponse, mockVODResumeActionResponse
} from "../test/";

import { IResumeMedia }  from "./resume.interface";
import { ResumeService } from "./resume.service";
import { ResumeModel }   from "./resume.model";

import {
    ApiCodes,
    ApiDelegate, APP_ERROR_STATE_CONST, AppErrorCodes,
    EventBus, HTTP_ERROR_MESSAGE, HTTP_NETWORK_ERROR,
    openAccessStatus
} from "../index";
import { AuthenticationServiceMock }      from "../test/mocks/authetication.service.mock";
import { mockAllProfilesData }            from "../test/mocks/tune/tune.delegate.response";
import { AppMonitorService }              from "../app-monitor/app-monitor.service";
import { mock }                           from "ts-mockito";
import { HttpProvider }                   from "../http";
import { MockAppConfig }                  from "../test/mocks/app.config.mock";
import { IAppConfig }                     from "../config";

describe("ResumeService Test Suite >>", function()
{
    const goodHash = "8";
    const goodVersion = "9";
    const goodBuild = "10";

    beforeEach(function()
    {
        this.service = null;
        this.baseTime = new Date();

        this.version = { version : goodVersion, hash : goodHash, build : goodBuild };
        this.diffVersion = { version : "10", hash : "9", build : "11" };

        this.setupSessionMock = (activeOpenAccessStatus, itDown = false, dupeLogin = false, authenticated = false) =>
        {
            this.sessionMock = {
                authenticated: authenticated,
                openAccessStatus : openAccessStatus.INELIGIBLE,
                activeOpenAccessSession: activeOpenAccessStatus,
                duplicateLogin: dupeLogin,
                itDown: itDown
            };

            this.appMonitorServiceMock = {
                ...mock(AppMonitorService),
                handleMessage: jasmine.createSpy('handleMessage'),
                triggerFaultError: jasmine.createSpy('triggerFaultError')
            };

            this.authenticationService.userSession.next(this.sessionMock);
        };

        this.initResumeDelegate = (values: any[] = []) =>
        {
            const defaultVals = [observableOf(mockNowPlayingResponse)];
            const valuesFiltered = !!values && values.length > 0 ? values : defaultVals;

            spyOn(this.resumeDelegate, 'resume').and.returnValues(
                ...valuesFiltered
            );
        };

        this.mockDependencies = () =>
        {
            this.appConfig = new MockAppConfig();

            this.apiDelegate = new ApiDelegate(new EventBus);
            this.authenticationService = new AuthenticationServiceMock();

            this.httpProvider = {
                get : function() { return observableOf(this.version); }
            } as any as HttpProvider;

            // values setup above in `initResumeDelegate`
            this.resumeDelegateSpy = jasmine.createSpy('resume');
            this.resumeDelegate = {
                resume: () => this.resumeDelegateSpy
            };

            this.sessionMonitorService = {
                updateSimultaneousListen: jasmine.createSpy('updateSimultaneousListen'),
                updateITDown: jasmine.createSpy('updateITDown')
            };
        };

        this.resumeModel = mock(ResumeModel);

        this.createService = () =>
        {
            this.service = new ResumeService(
                this.resumeDelegate,
                this.resumeModel,
                this.apiDelegate,
                this.authenticationService,
                this.appMonitorServiceMock,
                this.sessionMonitorService,
                this.httpProvider,
                this.appConfig
            );
        };

        this.mockDependencies();
        jasmine.clock().install();
        jasmine.clock().mockDate(this.baseTime);
    });

    afterEach(function()
    {
        jasmine.clock().uninstall();
    });

    describe("Infrastructure >> ", function()
    {
        it("Should have a test subject.", function()
        {
            expect(this.service).toBeDefined();
        });

        it("Should be able to recover session after itDown", function()
        {
            this.setupSessionMock(false, true);
            this.initResumeDelegate();
            this.service = new ResumeService( this.resumeDelegate,
                this.resumeModel,
                this.apiDelegate,
                this.authenticationService,
                this.appMonitorServiceMock,
                this.sessionMonitorService,
                this.httpProvider,
                this.appConfig);

            // initial value, we are not reclaiming a session
            this.authenticationService.userSession.pipe(first())
                .subscribe(response =>
                {
                    // if we are not reclaiming a session, resume is not called
                    expect(this.resumeDelegate.resume).not.toHaveBeenCalled();
                    expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
                    expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
                });

            this.sessionMock.itDown = false;
            this.authenticationService.userSession.next(this.sessionMock);

            // recover session after itDown
            this.authenticationService.userSession.pipe(first()).subscribe(response =>
            {
                expect(this.resumeDelegate.resume).toHaveBeenCalled();
                expect(this.sessionMonitorService.updateSimultaneousListen).toHaveBeenCalled();
                expect(this.sessionMonitorService.updateITDown).toHaveBeenCalled();
            });
        });

        xit("Should be able to recover session after itDown with resume false", function()
        {
            this.setupSessionMock(false, true);
            this.initResumeDelegate();

            this.service = new ResumeService(this.resumeDelegate,
                this.resumeModel,
                this.apiDelegate,
                this.authenticationService,
                this.appMonitorServiceMock,
                this.sessionMonitorService,
                this.httpProvider,
                this.appConfig);

            spyOn(this.service, 'resume').and.returnValue(observableOf(false));

            // initial value, we are not reclaiming a session
            this.authenticationService.userSession.pipe(first())
                .subscribe(response =>
                {
                    // if we are not reclaiming a session, resume is not called
                    expect(this.service.resume).not.toHaveBeenCalled();
                    expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
                    expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
                });

            this.sessionMock.itDown = false;
            this.authenticationService.userSession.next(this.sessionMock);

            // recover session after itDown
            this.authenticationService.userSession.pipe(first()).subscribe(response =>
            {
                expect(this.service.resume).toHaveBeenCalled();
                expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
                expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
            });
        });

        it("Should be able to reclaim session with duplicate login and resume", function()
        {
            this.setupSessionMock(false, false, true);

            let hasRun = false;
            const resumeDelegateSpy = jasmine.createSpy('resume');
            this.resumeDelegate = {
                resume: resumeDelegateSpy.and.callFake( () =>
                {
                    if (!hasRun)
                    {
                        hasRun = true;
                        return observableOf(mockNowPlayingResponse);
                    }

                    return observableThrowError(false);
                })
            };

            this.service = new ResumeService(this.resumeDelegate,
                this.resumeModel,
                this.apiDelegate,
                this.authenticationService,
                this.appMonitorServiceMock,
                this.sessionMonitorService,
                this.httpProvider,
                this.appConfig);

            this.sessionMock.duplicateLogin = false;
            this.authenticationService.userSession.next(this.sessionMock);
            jasmine.clock().tick(ResumeService.RESUME_DEBOUNCE_TIME + 1);

            this.authenticationService.userSession.pipe(first()).subscribe(() =>
            {
                expect(this.resumeDelegate.resume).toHaveBeenCalled();
                expect(this.sessionMonitorService.updateSimultaneousListen).toHaveBeenCalled();
                expect(this.sessionMonitorService.updateITDown).toHaveBeenCalled();

                this.authenticationService.userSession.next(this.sessionMock);
                jasmine.clock().tick(ResumeService.RESUME_DEBOUNCE_TIME + 1);

                this.authenticationService.userSession.pipe(takeLast(1)).subscribe(() =>
                {
                    expect(this.resumeDelegate.resume).toHaveBeenCalled();
                    expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
                    expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
                });
            });
        });
    });

    describe("`resume()` >> ", function()
    {
        beforeEach( function()
        {
            this.initResumeDelegate();
            this.createService();
        });

        describe("Infrastructure >> ", function()
        {
            it("Should call the resume delegate without open Access", function()
            {
                this.setupSessionMock(false);
                this.service = new ResumeService(this.resumeDelegate,
                    this.resumeModel,
                    this.apiDelegate,
                    this.authenticationService,
                    this.appMonitorServiceMock,
                    this.sessionMonitorService,
                    this.httpProvider,
                    this.appConfig);

                this.service.resume(undefined, true);

                expect(this.resumeDelegate.resume).toHaveBeenCalledWith(undefined, false, {type: 'type', id: 'id'}, true);
            });

            it("Should call the resume delegate with open Access", function()
            {
                this.setupSessionMock(true);
                this.service = new ResumeService(this.resumeDelegate,
                    this.resumeModel,
                    this.apiDelegate,
                    this.authenticationService,
                    this.appMonitorServiceMock,
                    this.sessionMonitorService,
                    this.httpProvider,
                    this.appConfig);

                this.service.resume(undefined, true);

                expect(this.resumeDelegate.resume).toHaveBeenCalledWith(undefined, true, {type: 'type', id: 'id'}, true);
            });

            it(`Should only allow one resume every ${ResumeService.RESUME_DEBOUNCE_TIME} milliseconds`, function()
            {
                this.service.resume();

                // Second resume will get a timer to schedule a resume for 5 seconds in the future
                let obs2 = this.service.resume();
                let obs3 = this.service.resume();

                // Only one timer for the future will get spawned, so subsequent calls to resume will return
                // the observable that will be resolved when the resume backoff time expires and a new resume
                // goes out.
                expect(obs3).toBe(obs2);

                expect(this.resumeDelegate.resume.calls.count()).toBe(1);

                jasmine.clock().tick(ResumeService.RESUME_DEBOUNCE_TIME + 1);

                expect(this.resumeDelegate.resume.calls.count()).toBe(2);

                obs2 = this.service.resume();
                obs3 = this.service.resume();

                // Only one timer for the future will get spawned, so subsequent calls to resume will return
                // the observable that will be resolved when the resume backoff time expires and a new resume
                // goes out.
                expect(obs3).toBe(obs2);

                expect(this.resumeDelegate.resume.calls.count()).toBe(2);
            });

            it(`Should handle resume failures properly`, function()
            {
                // Make the resume delegate return a failure, then success
                spyOn(this.service, "resume").and.returnValues(
                    observableThrowError(false),
                    observableOf(true)
                );

                const onFailure = jasmine.createSpy("onFailure");
                const onSuccess = jasmine.createSpy("onSuccess");

                // First resume will fail, failure handler should be called, not success handler
                let obs1 = this.service.resume().subscribe({next: onSuccess, error: onFailure});

                expect(onFailure).toHaveBeenCalled();
                expect(onSuccess).not.toHaveBeenCalled();

                // Success test

                let obs2 = this.service.resume().subscribe({next:onSuccess,error:onFailure});

                // expect second try to be ResumeService.RESUME_DEBOUNCE_TIME
                expect(obs1).not.toBe(obs2);
                expect(onSuccess).toHaveBeenCalled();


                jasmine.clock().tick(ResumeService.RESUME_DEBOUNCE_TIME + 1);

                expect(onSuccess).toHaveBeenCalled();
                expect(onFailure.calls.count()).toBe(1);
            });
        });
    });

    describe("`resume delegate error handles, `callResume` >> `OnResumeFailure`` >> ", function()
    {
        beforeEach( function()
        {
            // Helpers specific to this block
            this.genericError = new Error('server error');

            this.setResumeDelegateError = (error?: any) =>
            {
                this.initResumeDelegate([
                    // passed error or generic error
                    !!error ? observableThrowError(error) : observableThrowError(this.genericError)
                ]);
            };

            // force recover session so callResume is called
            this.forceRecoverSession = () =>
            {
                this.sessionMock.itDown = false;
                this.authenticationService.userSession.next(this.sessionMock);
            };
            this.setupSessionMock(true, true, false, true);
        });

        it(`Should handle empty response from delegate that hits success handler`, function()
        {
            this.initResumeDelegate([
                observableOf(false)
            ]);
            this.createService();
            this.forceRecoverSession();

            // session subscription should not update sessionMonitorService
            expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
            expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
        });

        it(`Should handle generic error with no code`, function()
        {
            this.setResumeDelegateError();
            this.createService();
            spyOn(JSON, 'stringify');
            this.forceRecoverSession();

            expect(JSON.stringify).toHaveBeenCalledWith(this.genericError);
            // session subscription should not update sessionMonitorService
            expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
            expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
        });

        it(`Should handle HTTP_ERROR_MESSAGE error`, function()
        {
            const error = { message: HTTP_ERROR_MESSAGE };
            this.setResumeDelegateError(error);
            this.createService();
            spyOn(JSON, 'stringify');
            this.forceRecoverSession();

            expect(JSON.stringify).toHaveBeenCalledWith(error);
            expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
            expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
        });

        it(`Should handle HTTP_NETWORK_ERROR error`, function()
        {
            const error = { message: HTTP_NETWORK_ERROR };
            this.setResumeDelegateError(error);
            this.createService();
            spyOn(JSON, 'stringify');
            this.forceRecoverSession();

            expect(JSON.stringify).toHaveBeenCalledWith(error);
            expect(this.appMonitorServiceMock.triggerFaultError)
                .toHaveBeenCalledWith({ faultCode : AppErrorCodes.FLTT_RESUME_ERROR });
            expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
            expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
        });

        it(`Should handle Tune error`, function()
        {
            const error = {
                code: ApiCodes.AOD_EXPIRED_CONTENT,
                message: 'AOD_EXPIRED_CONTENT',
                modules: { }
            };
            this.setResumeDelegateError(error);
            this.createService();
            spyOn(JSON, 'stringify');
            this.forceRecoverSession();

            expect(JSON.stringify).toHaveBeenCalledWith(error);
            expect(this.sessionMonitorService.updateSimultaneousListen)
                .toHaveBeenCalledWith(APP_ERROR_STATE_CONST.SIMULTANEOUS_LISTEN_DISABLED);
            expect(this.sessionMonitorService.updateITDown).toHaveBeenCalledWith(false);
        });

        it(`Should handle INVALID_REQUEST error`, function()
        {
            const error = {
                code: ApiCodes.INVALID_REQUEST
            };
            this.setResumeDelegateError(error);
            this.createService();
            spyOn(JSON, 'stringify');
            this.forceRecoverSession();

            expect(JSON.stringify).toHaveBeenCalledWith(error);
            expect(this.appMonitorServiceMock.triggerFaultError).toHaveBeenCalledWith({
               apiCode: ApiCodes.INVALID_REQUEST,
               faultCode: AppErrorCodes.FLTT_INVALID_REQUEST
            });
            expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
            expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
        });

        it(`Should handle empty error`, function()
        {
            this.setResumeDelegateError();
            this.createService();
            spyOn(JSON, 'stringify');
            this.forceRecoverSession();

            expect(JSON.stringify).toHaveBeenCalled();
            expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
            expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
        });

        it(`Should NOT log on "resumeFault" if auth required error code is present`, function()
        {
            const error = {
                code: ApiCodes.AUTH_REQUIRED
            };

            this.setResumeDelegateError(error);
            this.createService();
            spyOn(JSON, 'stringify');

            this.forceRecoverSession();
            expect(JSON.stringify).not.toHaveBeenCalled();
            expect(this.sessionMonitorService.updateSimultaneousListen).not.toHaveBeenCalled();
            expect(this.sessionMonitorService.updateITDown).not.toHaveBeenCalled();
        });
    });

    describe("'resumeMedia Observable' >>", function()
    {
        beforeEach( function()
        {
            this.initResumeDelegate();
            this.createService();
        });

        it(`Should invoke resumeMedia Observable for AOD `, function()
        {
            this.resumeDelegate.resume
                = jasmine.createSpy("resume").and.callFake(() =>
            {
                return observableOf(mockAODResponse);
            });

            this.service.resume();

            this.service.resumeMedia.subscribe((resumeData: IResumeMedia) =>
            {
                expect(resumeData.type).toEqual(mockAODResponse.moduleType);
                expect(resumeData.data).toEqual(mockAODResponse.aodData);
                expect(resumeData.updateFrequency).toEqual(mockAODResponse.updateFrequency);
            });
        });

        it(`Should invoke resumeMedia Observable for VOD `, function()
        {
            this.resumeDelegate.resume
                = jasmine.createSpy("resume").and.callFake(() =>
            {
                return observableOf(mockVODResumeActionResponse);
            });

            this.service.resume();

            this.service.resumeMedia.subscribe((resumeData: IResumeMedia) =>
            {
                expect(resumeData.type).toEqual(mockVODResumeActionResponse.moduleType);
                expect(resumeData.data).toEqual(mockVODResumeActionResponse.vodData);
                expect(resumeData.updateFrequency).toEqual(mockVODResumeActionResponse.updateFrequency);
            });
        });

        it(`Should invoke resumeMedia Observable for SEEDEDRADIO `, function()
        {
            this.resumeDelegate.resume
                = jasmine.createSpy("resume").and.callFake(() =>
            {
                return observableOf(mockSRResponse);
            });

            this.service.resume();

            this.service.resumeMedia.subscribe((resumeData: IResumeMedia) =>
            {
                expect(resumeData.type).toEqual(mockSRResponse.moduleType);
                expect(resumeData.data).toEqual(mockSRResponse.seededRadioData);
                expect(resumeData.updateFrequency).toEqual(mockSRResponse.updateFrequency);
            });
        });

        it(`Should invoke resumeMedia Observable for ADDITIONAL CHANNELS `, function()
        {
            this.resumeDelegate.resume
                = jasmine.createSpy("resume").and.callFake(() =>
            {
                return observableOf(mockAICResponse);
            });

            this.service.resume();

            this.service.resumeMedia.subscribe((resumeData: IResumeMedia) =>
            {
                expect(resumeData.type).toEqual(mockAICResponse.moduleType);
                expect(resumeData.data).toEqual(mockAICResponse.additionalChannelData);
                expect(resumeData.updateFrequency).toEqual(mockAICResponse.updateFrequency);
            });
        });

        it(`Should invoke resumeMedia Observable for Live `, function()
        {
            this.service.resume();

            this.service.resumeMedia.subscribe((resumeData: IResumeMedia) =>
            {
                expect(resumeData.type).toEqual(mockNowPlayingResponse.moduleType);
                expect(resumeData.data).toEqual(mockNowPlayingResponse.liveChannelData);
                expect(resumeData.updateFrequency).toEqual(mockNowPlayingResponse.updateFrequency);
            });
        });

        it(`Should not invoke resumeMedia Observable if no aod data in response `, function()
        {
            this.resumeDelegate.resume
                = jasmine.createSpy("resume").and.callFake(() =>
            {
                const response = {
                    moduleType: "aod",
                    updateFrequency: 1002,
                    clientConfiguration: {
                        openAccessPeriodState: false
                    },
                    getAllProfilesData: mockAllProfilesData
                };
                return observableOf(response);
            });

            this.service.resume();

            this.service.resumeMedia.subscribe((resumeData: IResumeMedia) =>
            {
                expect(resumeData).toEqual(null);
            });
        });
    });

    describe("`ApiDelegate code handlers()` >> ", function()
    {
        beforeEach( function()
        {
            this.initResumeDelegate();
            this.createService();
        });

        it("Should trigger a call to resume when SESSION_RESUME is triggered", function()
        {
            spyOn(this.service, "resume");

            const unauthenticated =
                {
                    status: 200,
                    statusText: "OK",
                    headers: [],
                    config: {},
                    data: { ModuleListResponse: { messages: [{ code: ApiCodes.SESSION_RESUME, message: "" }] } }
                };

            try
            {
                this.apiDelegate.checkApiResponse(unauthenticated);
            }
            catch (error)
            {
                expect(error.code).toEqual(ApiCodes.SESSION_RESUME);
            }

            expect(this.service.resume).toHaveBeenCalled();
        });

        it("Should clear deep link if resume fails(deepLink expired/not good deepLink)", function()
        {
            this.setupSessionMock(false);
            this.service = new ResumeService(this.resumeDelegate,
                this.resumeModel,
                this.apiDelegate,
                this.authenticationService,
                this.appMonitorServiceMock,
                this.sessionMonitorService,
                this.httpProvider,
                this.appConfig);

             const channelNotFound =
                      {
                          status: 200,
                          statusText: "OK",
                          headers: [],
                          config: {},
                          data: {
                              ModuleListResponse: {
                                  messages: [{
                                      code: ApiCodes.CHANNEL_NOT_AVAILABLE,
                                      message: ""
                                  }]
                              }
                          }
                      };

            try
            {
                this.apiDelegate.checkApiResponse(channelNotFound);
            }
            catch (error)
            {
                expect(error.code).toEqual(ApiCodes.CHANNEL_NOT_AVAILABLE);
            }

            this.service.resume();

            expect(this.resumeDelegate.resume).toHaveBeenCalledWith(undefined, false, null, true);
        });

        it("should trigger an API fault when resume response has nothing for playback)", function()
        {
            this.setupSessionMock(false);

            spyOn(this.appMonitorServiceMock, "triggerFaultApiError");

            this.resumeDelegate.resume
                = jasmine.createSpy("resume").and.callFake(() =>
            {
                return observableOf({
                    messages: [ {
                        code   : ApiCodes.CHANNEL_NOT_AVAILABLE,
                        message: ""
                    } ]
                });
            });


            this.service = new ResumeService(this.resumeDelegate,
                this.resumeModel,
                this.apiDelegate,
                this.authenticationService,
                this.appMonitorServiceMock,
                this.sessionMonitorService,
                this.httpProvider,
                this.appConfig);

            this.service.resume();

            expect(this.appMonitorServiceMock.triggerFaultApiError).toHaveBeenCalledWith(ApiCodes.UNAVAILABLE_RESUME_CONTENT,
                null);
        });


        it("should not trigger an API fault when resume response has nothing for playback)", function()
        {
            this.setupSessionMock(false);

            spyOn(this.appMonitorServiceMock, "triggerFaultApiError");

            this.resumeDelegate.resume
                = jasmine.createSpy("resume").and.callFake(() =>
            {
                const response = {
                    moduleType: "live",
                    updateFrequency: 1002,
                    clientConfiguration: {
                        openAccessPeriodState: false
                    },
                    getAllProfilesData: mockAllProfilesData
                };
                return observableOf(response);
            });

            this.service = new ResumeService(this.resumeDelegate,
                this.resumeModel,
                this.apiDelegate,
                this.authenticationService,
                this.appMonitorServiceMock,
                this.sessionMonitorService,
                this.httpProvider,
                this.appConfig);

            this.service.resume();

            expect(this.appMonitorServiceMock.triggerFaultApiError).not.toHaveBeenCalled();
        });
    });

    describe("`Version checking()` >> ", function()
    {
        beforeEach( function()
        {
            this.initResumeDelegate();
            this.createService();
        });

        it("Should not trigger a call to reload the app when the versions match", function()
        {
            this.httpProvider = {
                get : () => { return observableOf(this.version); }
            } as any as HttpProvider;

            this.setupSessionMock(false);
            this.service = new ResumeService(this.resumeDelegate,
                this.resumeModel,
                this.apiDelegate,
                this.authenticationService,
                this.appMonitorServiceMock,
                this.sessionMonitorService,
                this.httpProvider,
                this.appConfig);

            this.service.resume();

            expect(this.appConfig.restart).not.toHaveBeenCalled();
        });

        it("Should not trigger a call to reload the app it cannot get version from the remote endpoint", function()
        {
            this.httpProvider = {
                get : () => { return observableOf(undefined); }
            } as any as HttpProvider;

            this.setupSessionMock(false);
            this.service = new ResumeService(this.resumeDelegate,
                this.resumeModel,
                this.apiDelegate,
                this.authenticationService,
                this.appMonitorServiceMock,
                this.sessionMonitorService,
                this.httpProvider,
                this.appConfig);

            this.service.resume();

            expect(this.appConfig.restart).not.toHaveBeenCalled();
        });

        it("Should trigger a call to reload the app if the versions do not match", function()
        {
            this.httpProvider = {
                get : () => { return observableOf(this.diffVersion); }
            } as any as HttpProvider;

            this.setupSessionMock(false);
            this.service = new ResumeService(this.resumeDelegate,
                this.resumeModel,
                this.apiDelegate,
                this.authenticationService,
                this.appMonitorServiceMock,
                this.sessionMonitorService,
                this.httpProvider,
                this.appConfig);

            this.service.resume();

            expect(this.appConfig.restart).toHaveBeenCalled();
        });

    });
});
