/**
 * This is known as a barrel file. It allows developers to package and rollup multiple exports
 * into a single file and thus single import/export; e.g., `export * from "./authentication/service/";`
 */
export * from "./resume.service";
export * from "./resume.delegate";
export * from "./resume.interface";
export * from "./resume.model";
