// NOTE: Keep this here so your IDE doesn't complain about unknown Jasmine methods. I expected to not need this line
// by adding Jasmines types as a node module and/or specifying Jasmine as a type in the tsconfig.json...alas,
// this was the only way to make WebStorm happy about Jasmine (even though the TS compiled via Karma without
// any issues).
//
/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import {StorageService}          from "./storage.service";
import {StorageKeyConstant}      from "./storage.constants";
import {ServiceFactory}        from "../service/service.factory";

describe("Storage Service Test Suite", () =>
{
    let storageService: StorageService;
    let username: string = "testuser";
    var store = {};

    beforeEach(() =>
    {
        storageService = new StorageService();
    });

    describe("Infrastructure >>", () =>
    {
        it("Should be able to create AuthenticationService and all its dependencies",() =>
        {
            expect(ServiceFactory.getInstance(StorageService)).toBeDefined();
        });

        it("Constructor", () =>
        {
            expect(storageService).toBeDefined();
            expect(storageService instanceof StorageService).toEqual(true);
        });
    });

    describe("Local Storage >>", () =>
    {
        beforeEach(() =>
        {
            spyOn(localStorage, "getItem").and.callFake(function (key)
            {
                return store[key];
            });
            spyOn(localStorage, "setItem").and.callFake(function (key, value)
            {
                store[key] = value;
            });
            spyOn(localStorage, "clear").and.callFake(function (key)
            {
                store = {};
            });
            spyOn(localStorage, "removeItem").and.callFake(function (key)
            {
                delete store[key];
            });
        });

        it("setItem", () =>
        {
            storageService.setItem(StorageKeyConstant.USER, username);
            expect(storageService.username).toEqual(username);
            expect(localStorage.setItem).toHaveBeenCalledWith(StorageKeyConstant.USER, username);
            expect(store[StorageKeyConstant.USER]).toEqual(username);
        });

        it ("getItem - default values are returned when requested item does not exist",() =>
        {
            expect(storageService.getItem("badkey")).toBeUndefined();
            expect(storageService.getItem("badkey","keywasbad")).toEqual("keywasbad");
        });

        it("getItem", () =>
        {
            storageService.setItem(StorageKeyConstant.USER, username);
            storageService.setItem(StorageKeyConstant.CONSUME, "TestConsume");
            expect(storageService.getItem(StorageKeyConstant.CONSUME)).toEqual("TestConsume");
            expect(localStorage.getItem).toHaveBeenCalledWith(username + StorageKeyConstant.CONSUME);
            expect(store[username + StorageKeyConstant.CONSUME]).toEqual("TestConsume");
        });

        it("removeItem - When key is USER", () =>
        {
            storageService.setItem(StorageKeyConstant.USER, username);
            storageService.removeItem(StorageKeyConstant.USER);
            expect(localStorage.removeItem).toHaveBeenCalledWith(StorageKeyConstant.USER);
            expect(storageService.getItem(StorageKeyConstant.USER)).toBeUndefined();
            expect(store[StorageKeyConstant.USER]).toBeUndefined();
        });

        it("removeItem - When key is not USER", () =>
        {
            storageService.setItem(StorageKeyConstant.USER, username);
            storageService.setItem(StorageKeyConstant.CONSUME, "TestConsume");
            storageService.removeItem(StorageKeyConstant.CONSUME);
            expect(localStorage.removeItem).toHaveBeenCalledWith(username + StorageKeyConstant.CONSUME);
            expect(storageService.getItem(StorageKeyConstant.CONSUME)).toBeUndefined();
            expect(store[StorageKeyConstant.CONSUME]).toBeUndefined();
        });

        it("clear everything except clientDeviceId", () =>
        {
            storageService.clearAll();
            expect(localStorage.clear).toHaveBeenCalled();
            expect(store).toEqual({clientDeviceId: null, username: null});
        });

        it("getParsedItem - when the value is json stored as string", () =>
        {
            const mockData = {
                channelId: "8965",
                contentType: "live"
            };
            storageService.setItem(StorageKeyConstant.PAUSEPOINT, JSON.stringify(mockData));
            expect(storageService.getParsedItem(StorageKeyConstant.PAUSEPOINT)).toEqual(mockData);
        });
    });
});
