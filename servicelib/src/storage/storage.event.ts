export class StorageEvent
{
    public static UPDATE = "storage.update.passive";
    public static CLEAR = "storage.clear.passive";
    public static LOADED = "storage.loaded.passive";
    public static LOAD = "storage.load.user.passive";
}
