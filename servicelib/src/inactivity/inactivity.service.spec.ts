/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />
import { InactivityService } from "./inactivity.service";
import { channelMock } from "../test/mocks/channel.lineup";
import { currentlyPlayingServiceMock } from "../test/mocks/currently-playing.service.mock";
import { ChannelLineupService } from "../channellineup/channel.lineup.service";
import { providers } from "../index";
import { MediaPlayerFactoryMock } from "../test/mocks/media-player.factory.mock";
import { IAppConfig } from "../config/interfaces/app-config.interface";
import { mock } from "ts-mockito";
import { mockChannelLineup } from "../test/mocks/channel.lineup";
import { of as observableOf } from "rxjs";

describe("InactivityService", () =>
{
    let inactivityService,
        currentlyPlayingServiceMock,
        channelLineupServiceMock,
        resetTimer,
        findChannelByIdSpy,
        throttleInterval,
        mediaPlayerFactoryMock,
        appConfig;

    beforeEach(() =>
    {
        appConfig   = {
            deviceInfo    : {
                appRegion: "appRegion"
            },
            contextualInfo: {
                userAgent  : "useAgent",
                queryString: "queryString",
                host       : "host",
                hostName   : "hostName",
                protocol   : "protocol"
            }
        } as IAppConfig;

        currentlyPlayingServiceMock = new currentlyPlayingServiceMock();
        channelLineupServiceMock = mock(ChannelLineupService);
        mediaPlayerFactoryMock = new MediaPlayerFactoryMock();
        channelLineupServiceMock.channelLineup = { channels : observableOf([channelMock,channelMock]) };
        findChannelByIdSpy = spyOn(channelLineupServiceMock, "findChannelById").and.returnValue(channelMock);
        throttleInterval = 60000;
        jasmine.clock().install();
        jasmine.clock().mockDate();
        this.appMonitorService = {};

        inactivityService = new InactivityService(currentlyPlayingServiceMock,
                                                  mediaPlayerFactoryMock,
                                                  this.appMonitorService,
                                                  appConfig);
    });


    afterEach(() =>
    {
        inactivityService = null;
        jasmine.clock().uninstall();
    });

    /*describe("Infrastructure", () =>
    {
        it("has a descriptor property so it is AJS injectable", () =>
        {
            const provider = providers.find(provider =>
            {
                return (provider.provide === InactivityService && provider.useClass === InactivityService);
            });

            expect(provider).toBeDefined();
            expect(provider.deps.length).toEqual(4);
        });
    });

    describe("Constructor", () =>
    {
        it("should call channelLineupService and set inactivity interval based on channel", () =>
        {
            expect(findChannelByIdSpy).toHaveBeenCalled();
            // inactivityTimeout is set to 10000 on mockChannel
            expect(InactivityService.USER_INACTIVITY_INTERVAL).toEqual(10000);
        });
    });

    describe("Timer and client handler", () =>
    {
        it("should call client handler after timeout interval has expired", () =>
        {
            const clientInactivityHandlerSpy = spyOn(inactivityService, "clientInactivityHandler").and.callThrough();
            const throttledTimerSpy = spyOn(inactivityService, "throttledInactivityTimer").and.callThrough();

            // Call method AFTER the throttle interval expires.
            jasmine.clock().tick(throttleInterval);
            throttledTimerSpy();
            // Set clock so timeout interval expires.
            jasmine.clock().tick(InactivityService.USER_INACTIVITY_INTERVAL);

            expect(clientInactivityHandlerSpy).toHaveBeenCalled();
        });

        it("should NOT call client handler if the timeout interval has not expired", () =>
        {
            const clientInactivityHandlerSpy = spyOn(inactivityService, "clientInactivityHandler").and.callThrough();
            const throttledTimerSpy = spyOn(inactivityService, "throttledInactivityTimer").and.callThrough();

            // Call method AFTER the throttle interval expires.
            jasmine.clock().tick(throttleInterval);
            throttledTimerSpy();
            // Set clock to just BEFORE the timeout interval expires.
            jasmine.clock().tick(InactivityService.USER_INACTIVITY_INTERVAL - 1);

            expect(clientInactivityHandlerSpy).not.toHaveBeenCalled();
        });

        it("should reset timer if throttle interval has expired", () =>
        {
            const setInactivityTimerSpy = spyOn(inactivityService, "setInactivityTimer").and.callThrough();
            const throttledTimerSpy = spyOn(inactivityService, "throttledInactivityTimer").and.callThrough();

            // Call method AFTER the throttle interval expires.
            jasmine.clock().tick(throttleInterval);
            throttledTimerSpy();

            expect(setInactivityTimerSpy).toHaveBeenCalled();
        });

        it("should NOT reset the timer if throttle interval has NOT expired", () =>
        {
            const setInactivityTimerSpy = spyOn(inactivityService, "setInactivityTimer").and.callThrough();
            const throttledTimerSpy = spyOn(inactivityService, "throttledInactivityTimer").and.callThrough();

            // Call method BEFORE the throttle interval expires.
            jasmine.clock().tick(throttleInterval - 1000);
            throttledTimerSpy();

            expect(setInactivityTimerSpy).not.toHaveBeenCalled();
        });
    });*/
});
