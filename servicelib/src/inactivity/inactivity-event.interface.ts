export interface IInactivityEvent
{
    target: any;
    type: string;
}
