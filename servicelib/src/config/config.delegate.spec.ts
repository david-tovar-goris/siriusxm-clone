
import {throwError as observableThrowError, Observable, of as observableOf} from 'rxjs';
/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />
import * as _ from "lodash";

import {ConfigDelegate} from "./config.delegate";
import {IAppConfig}     from "./interfaces/app-config.interface";
import {catchError} from "rxjs/operators";

describe("Configuration delegate test suite", function()
{
    const resultTemplate: string = 'result';
    const appRegion: string = 'US';
    const config = { deviceInfo : { player : "web "} };

    beforeEach( function()
    {

        this.configurationResponse = {
            configuration : {
                components: [{
                    name: 'Configuration',
                    locale: 'en_US',
                    settings: [{
                        name: 'setting1'
                    }, {
                        name: 'setting2'
                    }],

                    errors: [{
                        name: 'error1'
                    }, {
                        name: 'error2'
                    }]
                }]
            }
        };

        this.http = {
            get: () =>
            {
                return observableOf(this.configurationResponse);
            }
        };



        this.configDelegate = new ConfigDelegate(this.http,config as IAppConfig);
    });

    describe("Infrastructure >>", function()
    {
        it('should have test subject', function()
        {
            expect(this.configDelegate).toBeDefined();
        });

        it("Constructor", function()
        {
            expect(this.configDelegate).toBeDefined();
            expect(this.configDelegate instanceof ConfigDelegate).toEqual(true);
        });

    });

    describe('getConfig() >>', function()
    {
        describe('Infrastructure >>>', function()
        {
            it('should have method defined on the test subject', function()
            {
                expect(this.configDelegate).toBeDefined();
                expect(_.isFunction(this.configDelegate.getConfig)).toBeTruthy();
            });

            it("Should return an observable.", function()
            {
                expect(this.configDelegate.getConfig() instanceof Observable).toBeTruthy();
            });

        });

        describe('Execution >>>', function()
        {
            it('should return the response object', function()
            {
                this.configDelegate.getConfig().subscribe(data =>
                {
                    expect(data).toEqual(this.configurationResponse.configuration);
                });
            });
        });

        describe('Exception handling >>>', function()
        {
            beforeEach(function()
            {
                this.configurationResponse = undefined;
                this.http = {
                    get: () =>
                    {
                        return observableThrowError(new Error('Server error'));
                    }
                };
                this.configDelegate = new ConfigDelegate(this.http,config as IAppConfig);
            });

            it("Should return an error when configuration object is undefined.", function()
            {
                expect(() =>
                {
                    this.configDelegate.getConfig(resultTemplate, appRegion)
                                  .subscribe( ( ) => {}, error => expect(error.message).toBe('Server error'));
                });
            });

        });

    });
});
