
import {throwError as observableThrowError,  Observable, of as observableOf } from 'rxjs';
/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />
import * as _ from "lodash";

import {
    mockConfigResponse,
    normalizedRelativeUrls
} from "./mocks/mock.config.response";

import { mock } from "ts-mockito";

import {
    IConfig,
    IRelativeUrlConfig
} from "./interfaces/config.interface";

import { ConfigService }      from "./config.service";
import { ConfigConstants }    from "./config.constants";
import { MockAppConfig }      from "../test/mocks/app.config.mock";
import { ConfigDelegate }     from "./config.delegate";
import { AssetLoaderService } from "../asset-loader";

describe("ConfigService Service Test Suite", function()
{
    beforeEach(function()
    {
        this.mockConfigAPIResponse = JSON.parse(JSON.stringify(mockConfigResponse));
        this.getConfig             =  () => {return observableOf(this.mockConfigAPIResponse);};

        this.configDelegate = mock(ConfigDelegate);
        spyOn(this.configDelegate, "getConfig").and.callFake(this.getConfig);

        const assetLoaderService = mock(AssetLoaderService);
        spyOn(assetLoaderService, "addExternalAsset").and.callFake(() => {});
        spyOn(assetLoaderService, "addInternalAsset").and.callFake(() => {});

        this.configService = new ConfigService(this.configDelegate,assetLoaderService, new MockAppConfig());
    });

    describe('Infrastructure >>', function()
    {

        it('should have test subject', function()
        {
            expect(this.configService).toBeDefined();
        });

        it('should have public properties defined..', function ()
        {
            expect(this.configService.configurationData).toBeDefined();
            expect(this.configService.configurationData).toBe(null);
        });
    });

    describe('getConfig() >>', function()
    {
        describe('Infrastructure >>>', function()
        {
            it('should have method defined on the test subject', function()
            {
                expect(this.configService).toBeDefined();
                expect(_.isFunction(this.configService.getConfig)).toBeTruthy();
            });

            it("Should return an observable.", function()
            {
                expect(this.configService.getConfig() instanceof Observable).toBeTruthy();
            });
        });

        describe('Execution >>>', function()
        {
            it('should save the response object in configuration & not in relativeUrlConfigData...', function()
            {
                this.mockConfigAPIResponse = { components: [ {name  : 'configuration', locale: 'en_US'}]};

                this.configService.getConfig()
                             .subscribe(response =>
                {
                    expect(response).toEqual(true);
                });

                this.configService.configuration
                             .subscribe((config: IConfig) =>
                {
                    expect(config).toEqual(this.mockConfigAPIResponse);
                });

                this.configService.relativeUrlConfiguration.subscribe((config: IRelativeUrlConfig) =>
                {
                    expect(config).toEqual(null);
                });
            });

            it('should save the data to configuration and relativeUrlConfigData...', function()
            {
                this.configService
                    .getConfig()
                    .subscribe(response =>
                    {
                        expect(response).toEqual(true);
                    });

                this.configService.configuration.subscribe((config: IConfig) =>
                {
                    expect(config).toEqual(this.mockConfigAPIResponse);
                });

                this.configService.relativeUrlConfiguration.subscribe((config: IRelativeUrlConfig) =>
                {
                    expect(config).toEqual(normalizedRelativeUrls);
                });
            });

            it('should except fault error', function()
            {
                const errorMessage = 'Config Error';

                this.configDelegate.getConfig = function() { return observableThrowError(errorMessage); };

                this.configService
                    .getConfig()
                    .subscribe(response =>
                    {
                        expect(response).toEqual(false);
                    });

                this.configService.configuration.subscribe((config: IConfig) =>
                {
                    expect(config).toEqual(null);
                });

                this.configService.relativeUrlConfiguration.subscribe((config: IRelativeUrlConfig) =>
                {

                    expect(config).toEqual(null);
                });
            });

            it('should return false if config response is empty', function()
            {

                this.configDelegate.getConfig = function() { return observableOf({}); };

                this.configService
                    .getConfig()
                    .subscribe(response =>
                               {
                                   expect(response).toEqual(false);
                               });

                this.configService.configuration.subscribe((config: IConfig) =>
                                                      {
                                                          expect(config).toEqual(null);
                                                      });

                this.configService.relativeUrlConfiguration.subscribe((config: IRelativeUrlConfig) =>
                                                                 {

                                                                     expect(config).toEqual(null);
                                                                 });
            });

        });
    });

    describe("getOpenAccessCopy()", function()
    {
        const SIGN_UP = "sign up";
        const SIGN_IN = "sign in";

        beforeEach(function()
        {
            const mockConfigResponseWithOpenAccess = {
                components: [{
                    name: ConfigConstants.OPEN_ACCESS,
                    settings: [{
                        stringSetting: {
                            name: "npOpenAccessSignInButtonCopy",
                            value: SIGN_IN
                        }
                    },
                    {
                        stringSetting: {
                            name: "npOpenAccessSignUpButtonCopy",
                            value: SIGN_UP
                        }
                    }]
                }]
            };

            this.configDelegate.getConfig = function()
            {
                return observableOf(mockConfigResponseWithOpenAccess);
            };

            this.configService.getConfig();
        });

        it('returns sign up copy if sign up param passed', function()
        {
            expect(this.configService.getOpenAccessCopy("npOpenAccessSignUpButtonCopy")).toEqual(SIGN_UP);
        });

        it('returns sign in copy if sign in param passed', function()
        {
            expect(this.configService.getOpenAccessCopy("npOpenAccessSignInButtonCopy")).toEqual(SIGN_IN);
        });
    });


    describe("getAppDownloadUrls()",function()
    {
        it('returns the download urls from the config response', function()
        {
            this.configService.getConfig();

            const downloadUrls = this.configService.getAppDownloadUrls();

            expect(downloadUrls.apple).toEqual("https://itunes.apple.com/us/app/siriusxm-internet-radio/id317951436?mt=8");
            expect(downloadUrls.android).toEqual("https://play.google.com/store/apps/details?id=com.sirius");
            expect(downloadUrls.windows).toEqual("https://www.microsoft.com/store/apps/9nblggh67pjk?ocid=badge");
        });

        it('returns the nothing if there are no download urls in the config response;', function()
        {
            this.mockConfigAPIResponse.components
                                 .forEach((component) =>
            {
                if (component.name === "AppDownloadUrl")
                {
                    delete component.settings;
                }
            });


            this.configDelegate.getConfig = () =>
            {
                return observableOf(this.mockConfigAPIResponse);
            };

            this.configService.getConfig();

            const downloadUrls = this.configService.getAppDownloadUrls();

            expect(downloadUrls.apple).toEqual("");
            expect(downloadUrls.android).toEqual("");
            expect(downloadUrls.windows).toEqual("");
        });
    });

    describe("getOnBoardingSettings()",function()
    {
        it('returns the onboarding urls from the config response', function()
        {
            this.configService.getConfig();

            const onBoardingUrls = this.configService.getOnBoardingSettings("en");

            expect(onBoardingUrls.authUrl).toBe("https://player.siriusxm.com");
            expect(onBoardingUrls.customerAgreementUrl).toBe("http://www.siriusxm.com/pdf/siriusxm_customeragreement_eng.pdf");
            expect(onBoardingUrls.faqUrl).toBe("https://listenercare.siriusxm.com/app/answers/list/p/908");
            expect(onBoardingUrls.feedBackUrl).toBe("https://listenercare.siriusxm.com/app/feedback/incidents.c$media_player_version/620");
            expect(onBoardingUrls.getStartedUrl).toBe("https://streaming.siriusxm.com/?src=everestplayer");
            expect(onBoardingUrls.passwordRecoveryUrl).toBe("https://streaming.siriusxm.com/?src=everestplayer&recoverlogin=true");
            expect(onBoardingUrls.privacyPolicyUrl).toBe("http://www.siriusxm.com/pdf/siriusxm_privacypolicy_eng.pdf");
            expect(onBoardingUrls.forgotLinkEnabled).toBe(true);
            expect(onBoardingUrls.onboardingEnabled).toBe(true);
        });

        it('Will report properly of onboarding links are disabled;', function()
        {
            this.mockConfigAPIResponse.components
                                 .forEach((component) =>
                                          {
                                              if (component.name === ConfigConstants.ON_BOARDING)
                                              {
                                                  component.settings[0].disableOnboarding = true;
                                              }
                                              if (component.name === ConfigConstants.FORGOT_LINK)
                                              {
                                                  component.settings[0].enableForgotLink = false;
                                              }
                                          });


            this.configDelegate.getConfig = () =>
            {
                return observableOf(this.mockConfigAPIResponse);
            };

            this.configService.getConfig();

            const onBoardingUrls = this.configService.getOnBoardingSettings("en");
            expect(onBoardingUrls.forgotLinkEnabled).toBe(false);
            expect(onBoardingUrls.onboardingEnabled).toBe(false);
        });

        it('returns empty onboarding urls and turns off all onboarding if configs are missing from the config response', function()
        {
            this.mockConfigAPIResponse.components
                                 .forEach((component) =>
                                          {
                                              if (component.name === ConfigConstants.AUTH_URL
                                                  || component.name === ConfigConstants.ON_BOARDING
                                                  || component.name === ConfigConstants.FORGOT_LINK
                                                  || component.name === ConfigConstants.CUSTOMER_AGREEMENT
                                                  || component.name === ConfigConstants.GET_STARTED
                                                  || component.name === ConfigConstants.FAQ
                                                  || component.name === ConfigConstants.FEEDBACK
                                                  || component.name === ConfigConstants.ON_BOARDING
                                                  || component.name === ConfigConstants.PRIVACY_POLICY)
                                              {
                                                  delete component.settings;
                                              }
                                          });

            this.configService.getConfig();

            const onBoardingUrls = this.configService.getOnBoardingSettings("en");

            expect(onBoardingUrls.authUrl).toBe("");
            expect(onBoardingUrls.customerAgreementUrl).toBe("");
            expect(onBoardingUrls.faqUrl).toBe("");
            expect(onBoardingUrls.feedBackUrl).toBe("");
            expect(onBoardingUrls.getStartedUrl).toBe("");
            expect(onBoardingUrls.passwordRecoveryUrl).toBe("");
            expect(onBoardingUrls.privacyPolicyUrl).toBe("");
            expect(onBoardingUrls.forgotLinkEnabled).toBe(false);
            expect(onBoardingUrls.onboardingEnabled).toBe(false);
        });
    });


});
