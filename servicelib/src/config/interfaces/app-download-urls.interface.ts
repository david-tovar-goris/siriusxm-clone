export interface AppDownloadUrls
{
    apple : string;
    android : string;
    windows : string;
}