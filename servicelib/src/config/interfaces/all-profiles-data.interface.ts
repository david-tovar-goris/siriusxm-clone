export interface IProfileMessage
{
    code : string;
    message : string;
}

export interface IProfileData
{
    credentialFlag : string;
    gupId : string;
    primaryFlag : boolean;
    profileStatus : string;
    createdDateTime?: Date;
    modifiedDateTime?: Date;
    avatar?: string;
    profileName?: string;
}

export interface IAllProfilesData
{
    messages : Array<IProfileMessage>;
    profileData : Array<IProfileData>;
    status : string;
}
