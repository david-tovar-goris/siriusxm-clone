/**
 * @MODULE:     service-lib
 * @CREATED:    07/19/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *      Defines the interface for the config object.
 */
import { IRelativeUrlSetting } from "./settings-item.interface";

export interface IConfig
{
    components: Array<IComponentConfig>;
}

export interface IComponentConfig
{
    name: string;
    locale?: string;
    settings: Array<any>;
    errors: Array<Object>;
}

export interface IRelativeUrlConfig
{
    settings: Array<IRelativeUrlSetting>;
}

export interface IConsumeSettings
{
    enableV4Consume?: boolean;
    enableV2Consume?: boolean;
}
