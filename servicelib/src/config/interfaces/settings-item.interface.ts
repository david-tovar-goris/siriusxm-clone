interface BaseSettingItem
{
    locale: string;
    platform: string;
}

export interface IntSettingItem extends BaseSettingItem
{
    intSetting: IntSetting;
}

export interface UrlSettingsItem extends BaseSettingItem
{
    urlSettings: Array<UrlSetting>;
}

export interface StringSettingItem extends BaseSettingItem
{
    stringSetting: StringSetting;
}

export interface IntSetting {
    value: number;
}

export interface StringSetting {
    name: string;
    value: string;
}

export interface UrlSetting {
    name: string;
    value: string;
    type: string;
    disableOnboarding?: boolean;
}

export interface IRelativeUrlSetting
{
    name: string;
    url: string;
}