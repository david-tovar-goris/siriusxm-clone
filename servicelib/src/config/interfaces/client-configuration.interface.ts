export interface IClientConfiguration
{
    activeListeningGracePeriodInMins  : number;
    authenticationGracePeriodInDays  : number;
    cachedContentDeletionPeriodInDays : number;
    channelLineupId : number;
    clientDeviceId : string;
    dataPlan : boolean;
    facebookAppId : string;
    familyFriendly : boolean;
    faqurl : string;
    fbLinkStatus : string;
    freeToAirPeriodState : boolean;
    maximumAutoDownloads : number;
    onDemandExpiryDays : number;
    openAccessPeriodState : boolean;
    radioAuthorizationDelayDuration : number;
    radioAuthorizationRequired : boolean;
    seededRadioSubscribed : boolean;
    streamingAccess : number;
    userDaysListened: number;
}
