export type OpenAccessCopyName =
    "welcomeScreenCopy" |
    "watchAndListenNowButtonCopy" |
    "privacyPolicy" |
    "appStoreBadges" |
    "appStoreBadgesCopy" |
    "disclaimerTextCopy" |
    "loginMessageButton" |
    "loginMessageCopy" |
    "npOpenAccessSignInButtonCopy" |
    "npOpenAccessSignUpButtonCopy" |
    "npShowButtons";
