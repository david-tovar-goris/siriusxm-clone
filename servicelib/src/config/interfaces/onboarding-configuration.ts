export interface OnBoardingSettings
{
    onboardingEnabled : boolean;
    forgotLinkEnabled : boolean;
    getStartedUrl : string;
    passwordRecoveryUrl : string;
    privacyPolicyUrl : string;
    customerAgreementUrl : string;
    faqUrl : string;
    feedBackUrl : string;
    authUrl : string;
    dataPrivacyUrl : string;
}
