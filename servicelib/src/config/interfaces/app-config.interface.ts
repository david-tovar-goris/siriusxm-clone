import { DeviceInfo }           from "../device.info";
import { IClientConfiguration } from "./client-configuration.interface";
import { IAllProfilesData }     from "./all-profiles-data.interface";
import { Observable }        from "rxjs";
import { ISuperCategory } from "../../channellineup";

export interface IAppConfig
{
    apiEndpoint : string;
    appId : string;
    resultTemplate : string;
    domainName : string;
    deviceInfo : DeviceInfo;
    adsWizzSupported ?: Boolean;
    defaultSuperCategory : ISuperCategory;
    clientConfiguration : IClientConfiguration;
    allProfilesData : IAllProfilesData;
    contextualInfo : IContextualInfo;
    inPrivateBrowsingMode : Observable<boolean>;
    restart : Function;
    nuDetect?: {
        api: any,
        ndsReady: boolean,
        ndsReadyCallback?: Function,
        beginBehavioralMonitoring: Function,
        stopBehavioralMonitoring: Function,
        requestCall: Function,
        getHiddenValue: Function,
        sessionId: string
    };
    loginRequired: boolean;
    uniqueSessionId: string;
    isFreeTierEnable: boolean;
    initialPathname: string;
}

export interface IContextualInfo
{
    userAgent : string;
    queryString : string;
    host : string;
    hostName : string;
    protocol : string;
    id? : string;
    type? : string;
    deepLink? : boolean;
}
