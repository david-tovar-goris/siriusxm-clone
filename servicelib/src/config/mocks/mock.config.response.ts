/* tslint:disable */

export const normalizedRelativeUrls = {
    "settings" : [{"name" : "Live_Primary_HLS", "url" : "https://siriusxm-priprodlive.akamaized.net"},
                  {"name" : "Live_Secondary_HLS", "url" : "https://siriusxm-secprodlive.akamaized.net"},
                  {"name" : "OnDemand_Primary_HLS", "url" : "https://siriusxm-priprodaod.akamaized.net"},
                  {"name" : "OnDemand_Secondary_HLS", "url" : "https://siriusxm-priprodaod.akamaized.net"},
                  {"url" : "https://siriusxm-priprodlivevideo.siriusxm.com"}]
};

export const mockConfigResponse = {
    "components" : [
        {
            "name"   : "Default",
            "locale" : "en"
        },
        {
            "name"     : "ResourceBundle",
            "settings" : [
                {
                    "resourceURLs" : [
                        {
                            "url"    : "https://player.siriusxm.com/Resource/web/locales_all",
                            "locale" : "en"
                        },
                        {
                            "url"    : "https://player.siriusxm.com/Resource/web/locales_all",
                            "locale" : "es"
                        }
                    ],
                    "platform"     : "WEB"
                }
            ]
        },
        {
            "name"     : "OnBoarding",
            "settings" : [
                {
                    "urlSettings"       : [
                        {
                            "name"  : "FLEPZ",
                            "value" : "https://streaming.siriusxm.com/?src=everestplayer"
                        },
                        {
                            "name"  : "Recovery",
                            "value" : "https://streaming.siriusxm.com/?src=everestplayer&recoverlogin=true"
                        }
                    ],
                    "disableOnboarding" : false,
                    "platform"          : "WEB",
                    "locale"            : "en"
                }
            ],
            "errors"   : [
                {
                    "errors" : [
                        {
                            "code"    : "5200",
                            "message" : "https://care.siriusxm.com/nonpay"
                        },
                        {
                            "code"    : "5230",
                            "message" : "https://care.siriusxm.com/expiredsxir"
                        },
                        {
                            "code"    : "103",
                            "message" : "https://care.siriusxm.com/expiredsxir"
                        },
                        {
                            "code"    : "default",
                            "message" : "https://care.siriusxm.com/login_view.action"
                        },
                        {
                            "code"    : "5260",
                            "message" : "https://www.siriusxm.com/siriusxmforbusiness"
                        },
                        {
                            "code"    : "5140",
                            "message" : "https://www.siriusxm.com/expiredtriallogin"
                        }
                    ],
                    "locale" : "en"
                }
            ]
        },
        {
            "name"     : "ForgotLink",
            "settings" : [
                {
                    "enableForgotLink" : true,
                    "platform"         : "WEB"
                }
            ]
        },
        {
            "name"     : "FAQURL",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://listenercare.siriusxm.com/app/answers/list/p/908",
                            "type"  : "PAGE"
                        }
                    ],
                    "platform"    : "WEB",
                    "locale"      : "en"
                }
            ]
        },
        {
            "name"     : "AppDownloadUrl",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://play.google.com/store/apps/details?id=com.sirius"
                        }
                    ],
                    "platform"    : "ANDROID"
                },
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://www.microsoft.com/store/apps/9nblggh67pjk?ocid=badge"
                        }
                    ],
                    "platform"    : "WINDOWS"
                },
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://itunes.apple.com/us/app/siriusxm-internet-radio/id317951436?mt=8"
                        }
                    ],
                    "platform"    : "IOS"
                }
            ]
        },
        {
            "name"     : "FacebookAppId",
            "settings" : [
                {
                    "facebookAppIds" : [
                        {
                            "id" : "1408209929443929"
                        }
                    ]
                }
            ]
        },
        {
            "name"     : "SplashScreen",
            "settings" : [
                {
                    "lastUpdatedTimestamp" : "2016-05-31T12:34:46.000+0000",
                    "urlSettings"          : [
                        {
                            "name"  : "4000x2000",
                            "value" : "https://player.siriusxm.com/images/Web-4000x2000.jpg"
                        }
                    ],
                    "platform"             : "WEB"
                }
            ]
        },
        {
            "name"     : "MySXMSliderScreen",
            "settings" : [
                {
                    "lastUpdatedTimestamp" : "2014-10-28T07:02:46.000+0000",
                    "urlSettings"          : [
                        {
                            "name"  : "web-1x",
                            "value" : "https://player.siriusxm.com/images/desktop_bgnd1280_custom_mix_1X.jpg"
                        },
                        {
                            "name"  : "web-2x",
                            "value" : "https://player.siriusxm.com/images/desktop_bgnd1280_custom_mix_2X.jpg"
                        }
                    ],
                    "platform"             : "WEB"
                }
            ]
        },
        {
            "name"     : "FeedbackUrl",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://listenercare.siriusxm.com/app/feedback/incidents.c$media_player_version/620"
                        }
                    ],
                    "locale"      : "en"
                },
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://listenercare.siriusxm.com/app/feedback/incidents.c$media_player_version/620"
                        }
                    ],
                    "locale"      : "es"
                }
            ]
        },
        {
            "name"     : "MiniPlayer",
            "settings" : [
                {
                    "urlSettings"   : [
                        {
                            "name"  : "URL",
                            "value" : "http://www.siriusxm.com"
                        }
                    ],
                    "stringSetting" : {
                        "name"  : "miniPlayerEnabled",
                        "value" : "true"
                    }
                }
            ]
        },
        {
            "name"     : "relativeUrls",
            "settings" : [
                {
                    "relativeUrls" : [
                        {
                            "name" : "Live_Primary_HLS",
                            "url"  : "https://siriusxm-priprodlive.akamaized.net"
                        },
                        {
                            "name" : "Live_Secondary_HLS",
                            "url"  : "https://siriusxm-secprodlive.akamaized.net"
                        },
                        {
                            "name" : "OnDemand_Primary_HLS",
                            "url"  : "https://siriusxm-priprodaod.akamaized.net"
                        },
                        {
                            "name" : "OnDemand_Secondary_HLS",
                            "url"  : "https://siriusxm-priprodaod.akamaized.net"
                        },
                        {
                            "url" : "https://siriusxm-priprodlivevideo.siriusxm.com"
                        }
                    ],
                    "platform"     : "WEB"
                }
            ]
        },
        {
            "name"     : "UpSellUrl",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "http://www.siriusxm.com/upgradetoallaccess"
                        }
                    ],
                    "platform"    : "WEB",
                    "locale"      : "en"
                }
            ]
        },
        {
            "name"     : "getStarted",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://streaming.siriusxm.com/?src=everestplayer"
                        }
                    ],
                    "platform"    : "WEB",
                    "locale"      : "en"
                }
            ]
        },
        {
            "name"     : "ITUrl",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://api-360L-ext-pdev.siriusxm.com",
                            "uri"   : [
                                {
                                    "name"     : "GetDeviceCalls_v2_0",
                                    "endpoint" : "rest/v1/subscription/devices/device/account/event/ivsm"
                                },
                                {
                                    "name"     : "getDeviceDisplayProfileV2_0",
                                    "endpoint" : "/rest/v1/subscription/devices/device/account/profile/ivsm"
                                },
                                {
                                    "name"     : "getDeviceInformationNonPII",
                                    "endpoint" : "/rest/v1/subscription/devices/device/nonpii"
                                },
                                {
                                    "name"     : "GetDeviceIVSMCalls",
                                    "endpoint" : "/rest/v1/vehicle/ivsm/device"
                                },
                                {
                                    "name"     : "UpdateDeviceCapabilities",
                                    "endpoint" : "/rest/v1/subscription/devices/device/capabilities"
                                },
                                {
                                    "name"     : "UpdateDeviceProvisionInfo",
                                    "endpoint" : "/rest/v1/vehicle/ivsm/device/refresh"
                                },
                                {
                                    "name"     : "TokenDeviceAuthenticate",
                                    "endpoint" : "/rest/v1/authenticate/deviceTokenAuthenticate"
                                },
                                {
                                    "name"     : "GetChallenge",
                                    "endpoint" : "/rest/v1/authenticate/getChallenge"
                                },
                                {
                                    "name"     : "VerifyChallenge",
                                    "endpoint" : "/rest/v1/authenticate/verifyChallenge"
                                },
                                {
                                    "name"     : "StreamingAuthorize",
                                    "endpoint" : "/rest/v1/authorize/streamingAuthorize"
                                },
                                {
                                    "name"     : "StreamingValidate",
                                    "endpoint" : "/rest/v1/authorize/streamingValidate"
                                },
                                {
                                    "name"     : "AddGuestProfile",
                                    "endpoint" : "/rest/v1/profiles/addGuestProfiles"
                                },
                                {
                                    "name"     : "CreateProfile",
                                    "endpoint" : "/rest/v1/profiles/createProfile"
                                },
                                {
                                    "name"     : "DeleteProfile",
                                    "endpoint" : "/rest/v1/profiles/deleteProfile"
                                },
                                {
                                    "name"     : "GetAllProfiles",
                                    "endpoint" : "/rest/v4/profiles/getAllProfiles"
                                },
                                {
                                    "name"     : "ModifyProfile",
                                    "endpoint" : "/rest/v1/profiles/modifyProfile"
                                },
                                {
                                    "name"     : "RemoveGuestProfile",
                                    "endpoint" : "/rest/v1/profiles/removeGuestProfiles"
                                },
                                {
                                    "name"     : "ResetProfiles",
                                    "endpoint" : "/rest/v1/profiles/resetProfiles"
                                }
                            ]
                        },
                        {
                            "name"  : "SECURE_URL",
                            "value" : "https://api-360L-extp-pdev.siriusxm.com",
                            "uri"   : [
                                {
                                    "name"     : "CreateAccount",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/new"
                                },
                                {
                                    "name"     : "createAccountDeferredTransaction",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/event/transaction/data/new"
                                },
                                {
                                    "name"     : "CreatePaymentCollectByCC",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/event/payment/cc"
                                },
                                {
                                    "name"     : "CreateSubscription",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/device/plans/plan/new"
                                },
                                {
                                    "name"     : "GetAccount",
                                    "endpoint" : "/rest/v1/subscription/accounts/account"
                                },
                                {
                                    "name"     : "GetAccountDeferredTransaction",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/events/event/transaction/data"
                                },
                                {
                                    "name"     : "GetAccountV1_0",
                                    "endpoint" : "/rest/v1/subscription/accounts/account"
                                },
                                {
                                    "name"     : "GetSubscriptionQuotes_V1_0",
                                    "endpoint" : "/rest/v1/subscription/devices/device/plans/plan/quote"
                                },
                                {
                                    "name"     : "updateAccountCredentials",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/profile/credential"
                                },
                                {
                                    "name"     : "UpdateAccountForACSC",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/acsc"
                                },
                                {
                                    "name"     : "UpdateAccountManageSubscription_v1_0",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/device/plan"
                                },
                                {
                                    "name"     : "UpdateCNA_v1_1",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/cna"
                                },
                                {
                                    "name"     : "UpdatePaymentDetails",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/payments/payinfo"
                                },
                                {
                                    "name"     : "UpdateSubscriptionPlan_v1_0",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/device/plans/plan"
                                },
                                {
                                    "name"     : "CreateAccountForgotCredentialsEmail_v1_0",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/notification/email/credential"
                                },
                                {
                                    "name"     : "LPZAuthenticate",
                                    "endpoint" : "/rest/v2/authenticate/lpzAuthenticate"
                                },
                                {
                                    "name"     : "UN-PWAuthenticate",
                                    "endpoint" : "/rest/v2/authenticate/usernameAuthenticate"
                                },
                                {
                                    "name"     : "GetSubscriptionSCEligible",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/device/servicecontinuity"
                                },
                                {
                                    "name"     : "CreateDeviceCall_v2_0",
                                    "endpoint" : "/rest/v1/subscription/devices/device/account/event/ivsm/phone/new"
                                },
                                {
                                    "name"     : "GetAccountEligibleSCPlans_V1_0",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/device/servicecontinuity"
                                },
                                {
                                    "name"     : "CreateAccountInteractions_v1_0",
                                    "endpoint" : "/rest/v1/accounts/ivsm/interactions/new"
                                },
                                {
                                    "name"     : "GetAccountEligibleAddOnPlans_V1_0",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/device/plans/addon"
                                },
                                {
                                    "name"     : "GetAccountEligiblePlans_V1_0",
                                    "endpoint" : "/rest/v1/subscription/accounts/account/device/plans/eligible"
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "name"     : "Encryption",
            "settings" : [
                {
                    "encryptionElements" : [
                        {
                            "encryptedElement" : "accountNumber"
                        },
                        {
                            "encryptedElement" : "accountPassword"
                        },
                        {
                            "encryptedElement" : "accountUsername"
                        },
                        {
                            "encryptedElement" : "cardName"
                        },
                        {
                            "encryptedElement" : "cardNumber"
                        },
                        {
                            "encryptedElement" : "cardType"
                        },
                        {
                            "encryptedElement" : "city"
                        },
                        {
                            "encryptedElement" : "country"
                        },
                        {
                            "encryptedElement" : "deviceId"
                        },
                        {
                            "encryptedElement" : "email"
                        },
                        {
                            "encryptedElement" : "emailAddress"
                        },
                        {
                            "encryptedElement" : "expiryMonth"
                        },
                        {
                            "encryptedElement" : "expiryYear"
                        },
                        {
                            "encryptedElement" : "firstName"
                        },
                        {
                            "encryptedElement" : "last4DigitsOfCc"
                        },
                        {
                            "encryptedElement" : "lastName"
                        },
                        {
                            "encryptedElement" : "newPassword"
                        },
                        {
                            "encryptedElement" : "newUsername"
                        },
                        {
                            "encryptedElement" : "nickname"
                        },
                        {
                            "encryptedElement" : "oldUsername"
                        },
                        {
                            "encryptedElement" : "phone"
                        },
                        {
                            "encryptedElement" : "phoneNumber"
                        },
                        {
                            "encryptedElement" : "postalCode"
                        },
                        {
                            "encryptedElement" : "selectedSubscriptionDeviceId"
                        },
                        {
                            "encryptedElement" : "selfPaidAccountNumber"
                        },
                        {
                            "encryptedElement" : "serviceAddress"
                        },
                        {
                            "encryptedElement" : "serviceEmailAddress"
                        },
                        {
                            "encryptedElement" : "serviceFirstName"
                        },
                        {
                            "encryptedElement" : "serviceLastName"
                        },
                        {
                            "encryptedElement" : "serviceLogin"
                        },
                        {
                            "encryptedElement" : "servicePassword"
                        },
                        {
                            "encryptedElement" : "state"
                        },
                        {
                            "encryptedElement" : "streetAddress"
                        },
                        {
                            "encryptedElement" : "subscriptionDeviceId"
                        }
                    ],
                    "encryptionKey"      : [
                        {
                            "name"  : "PublicKey",
                            "value" : "LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUE4TkdlYWM1dU80blFLTlBTSXBmSwo4dHIweFNvbFNFNjQ2Z3lMbFFGdUFiQm1YejhXRnlLajdrWHpGdVBjOVBER2xmM1ZiVzB4TEtBRFFVVGpGdk9jClBYZVpzaUxzRnZQMk51VmlkVnUrcng0NUNJQ1AwZXo1SVNxMU03STVmTEtHdlMxYkhRK0ROTHJ2TGZCWEU0cTkKM0FxKzJLUHF2aE1pNHp6a0FzNG9Sbmoybm9mMUo5TGMxRzlMYkxjbklGTWRmNmJETHpaYzNTeUwrWitTRWhhUAoyODMwQnBWcGJxcXplRXJ4eWxGUksyUlZrZFVkTDVlN0NPWjRXQ2xBNWRJYk9lQ0FYa1IrUDFoTzJYQzVuZEZhCmdrU3p2c0t4aFNWeTRzVGNQaXN3NnBsNkFrdVdEaEtDM2NGOXprN3RtekFnTTd6TTBFQVF0YXBSNmpvcVFGQXIKa1FJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg==",
                            "index" : "1009"
                        }
                    ]
                }
            ]
        },
        {
            "name"     : "AuthUrl",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://player.siriusxm.com"
                        }
                    ]
                }
            ]
        },
        {
            "name"     : "FordSyncFAQUrl",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://www.siriusxm.com/FordSYNC"
                        }
                    ],
                    "locale"      : "en"
                }
            ]
        },
        {
            "name"     : "SocialLogin",
            "settings" : [
                {
                    "booleanSettings" : [
                        {
                            "name"  : "US",
                            "value" : false
                        }
                    ],
                    "platform"        : "WEB"
                }
            ]
        },
        {
            "name"     : "OAC",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://care.siriusxm.com/login_view.action"
                        }
                    ],
                    "locale"      : "en"
                },
                {
                    "stringSetting" : {
                        "name"  : "maxProfiles",
                        "value" : "5"
                    }
                }
            ]
        },
        {
            "name"     : "PrivacyPolicy",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "http://www.siriusxm.com/pdf/siriusxm_privacypolicy_eng.pdf",
                            "type"  : "PDF"
                        }
                    ],
                    "platform"    : "WEB",
                    "locale"      : "en"
                },
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "http://www.siriusxm.com/pdf/siriusxm_privacypolicy_span.pdf",
                            "type"  : "PDF"
                        }
                    ],
                    "platform"    : "WEB",
                    "locale"      : "es"
                }
            ]
        },
        {
            "name"     : "CustomerAgreement",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "http://www.siriusxm.com/pdf/siriusxm_customeragreement_eng.pdf",
                            "type"  : "PDF"
                        }
                    ],
                    "platform"    : "WEB",
                    "locale"      : "en"
                },
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "http://www.siriusxm.com/pdf/siriusxm_customeragreement_span.pdf",
                            "type"  : "PDF"
                        }
                    ],
                    "platform"    : "WEB",
                    "locale"      : "es"
                }
            ]
        },
        {
            "name"     : "audibleWarnings",
            "settings" : [
                {
                    "stringSetting" : {
                        "name"  : "enabled",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "beepCount",
                        "value" : "2"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "beepType",
                        "value" : "NA"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "voume",
                        "value" : "10"
                    }
                }
            ]
        },
        {
            "name"     : "FordSyncInCar",
            "settings" : [
                {
                    "fordSyncInCar" : false,
                    "platform"      : "WEB"
                }
            ]
        },
        {
            "name"     : "deepLink",
            "settings" : [
                {
                    "availability" : true,
                    "platform"     : "WEB"
                }
            ]
        },
        {
            "name"     : "maxymiser",
            "settings" : [
                {
                    "availability" : true,
                    "platform"     : "WEB"
                }
            ]
        },
        {
            "name"     : "openAccessPeriod",
            "settings" : [
                {
                    "availability" : true,
                    "platform"     : "WEB"
                }
            ]
        },
        {
            "name"     : "myAccountLink",
            "settings" : [
                {
                    "availability" : true,
                    "platform"     : "WEB"
                }
            ]
        },
        {
            "name"     : "oacUrlStatus",
            "settings" : [
                {
                    "availability" : true,
                    "platform"     : "ANDROID"
                },
                {
                    "availability" : true,
                    "platform"     : "IOS"
                }
            ]
        },
        {
            "name"     : "VoiceSearch",
            "settings" : [
                {
                    "silenceDetectTime"    : "1",
                    "maximumListeningTime" : "10",
                    "platform"             : "WEB"
                }
            ]
        },
        {
            "name"     : "audioRecoveryTimeout",
            "settings" : [
                {
                    "intSetting" : {
                        "value" : 0
                    }
                }
            ]
        },
        {
            "name"     : "SatConsumptionCachingPeriod",
            "settings" : [
                {
                    "intSetting" : {
                        "value" : 30
                    }
                }
            ]
        },
        {
            "name"     : "Crashlytics",
            "settings" : [
                {
                    "enableCrashlytics" : false,
                    "platform"          : "WEB"
                }
            ]
        },
        {
            "name"     : "LiveChat",
            "settings" : [
                {
                    "booleanSettings" : [
                        {
                            "name"  : "enabled",
                            "value" : true
                        }
                    ],
                    "stringSetting"   : {
                        "name"  : "liveChatEndpoint",
                        "value" : "https://www.livechat.placeholder.com/web"
                    },
                    "platform"        : "WEB"
                }
            ]
        },
        {
            "name"     : "settingsPanel",
            "settings" : [
                {
                    "stringSetting" : {
                        "name"  : "whiteList",
                        "value" : "siriusxm.com,facebook.com,twitter.com,soundcloud.com,youtube.com,vimeo.com"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "MLTServiceEnabled",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "YMALServiceEnabled",
                        "value" : "false"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "HostBioEnabled",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "EnableTransactionLogging",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "EnableUserFeedback",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "EnableFordInCar",
                        "value" : "false"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "MoreLikeThisOnNowPlayingLive",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "SkipModalPlayHead",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "YouMayAlsoLikeRecentlyPlayed",
                        "value" : "false"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "YouMayAlsoLikeChannelList",
                        "value" : "false"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "moreLikeThisChannelEDP",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "moreLikeThisShowEDP",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "skipRecommendation",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "isOmniture",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "isProd",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "enableAppleCarPlay",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "enableAndroidAuto",
                        "value" : "true"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "moreLikeThisMySxm",
                        "value" : "false"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "un-authenticateRetry",
                        "value" : "4"
                    }
                }
            ]
        },
        {
            "name"     : "MetricsConfigurationUrl",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://player.siriusxm.com/rest/v3/experience/modules/client/configuration/"
                        }
                    ]
                }
            ]
        },
        {
            "name"     : "MetricsBaseUrl",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "URL",
                            "value" : "https://player.siriusxm.com/rest/v3/experience/modules/"
                        }
                    ]
                }
            ]
        },
        {
            "name"     : "contactPhoneNumber",
            "settings" : [
                {
                    "stringSetting" : {
                        "name"  : "contactPhoneNumber",
                        "value" : "1-888-927-7465"
                    }
                }
            ]
        },
        {
            "name"     : "LogTriggers",
            "settings" : [
                {
                    "logTriggers" : {
                        "duration" : 12
                    }
                }
            ]
        },
        {
            "name"     : "ExternalOnlineCheck",
            "settings" : [
                {
                    "specialPlatform"     : "everest",
                    "externalOnlineCheck" : true
                },
                {
                    "specialPlatform"     : "360l",
                    "externalOnlineCheck" : false
                },
                {
                    "specialPlatform"     : "k2",
                    "externalOnlineCheck" : false
                }
            ]
        },
        {
            "name"     : "homepage",
            "settings" : [
                {
                    "stringSetting" : {
                        "value" : "App:carousel:for_you"
                    }
                }
            ]
        },
        {
            "name"     : "resetPassword",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "allPartners",
                            "value" : "https://www.siriusxm.com/forgot"
                        },
                        {
                            "name"  : "apple",
                            "value" : "https://www.siriusxm.com/apasswordreset"
                        }
                    ],
                    "locale"      : "en"
                }
            ]
        },
        {
            "name"     : "OpenAccessParameters",
            "settings" : [
                {
                    "stringSetting" : {
                        "name"  : "welcomeScreenCopy",
                        "value" : "Watch and listen free now to 200+ channels. Stream on."
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "watchAndListenNowButtonCopy",
                        "value" : "Watch and Listen Now"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "privacyPolicy",
                        "value" : "Privacy Policy"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "locatingYou",
                        "value" : "Locating You"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "appStoreBadges",
                        "value" : "ios,android,windows"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "appStoreBadgesCopy",
                        "value" : "Download our app and enjoy SiriusXM anywhere."
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "disclaimerTextCopy",
                        "value" : "By selecting “Get Started” I agree to the following:"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "loginMessageButton",
                        "value" : "Sign In"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "loginMessageCopy",
                        "value" : "Already have a login?"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "npOpenAccessSignInButtonCopy",
                        "value" : "Sign In Now"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "npOpenAccessSignUpButtonCopy",
                        "value" : "Set Up a Streaming Login"
                    }
                },
                {
                    "stringSetting" : {
                        "name"  : "npShowButtons",
                        "value" : "BOTH"
                    }
                }
            ],
            "locale"   : "en"
        },
        {
            "name"     : "audioRecoveryOfflineTimeout",
            "settings" : [
                {
                    "intSetting" : {
                        "value" : 0
                    }
                }
            ]
        },
        {
            "name"     : "accountSignupURL",
            "settings" : [
                {
                    "urlSettings" : [
                        {
                            "name"  : "samsung",
                            "value" : "http://www.siriusxm.com/samsung"
                        },
                        {
                            "name"  : "lg",
                            "value" : "http://www.siriusxm.com/lg"
                        },
                        {
                            "name"  : "sony",
                            "value" : "http://www.siriusxm.com/sony"
                        },
                        {
                            "name"  : "roku",
                            "value" : "http://www.siriusxm.com/roku"
                        },
                        {
                            "name"  : "apple",
                            "value" : "http://www.siriusxm.com/appletv"
                        },
                        {
                            "name"  : "microsoft",
                            "value" : "http://www.siriusxm.com/xbox"
                        },
                        {
                            "name"  : "amazon",
                            "value" : "http://www.siriusxm.com/amazon"
                        },
                        {
                            "name"  : "androidtv",
                            "value" : "http://www.siriusxm.com/androidtv"
                        },
                        {
                            "name"  : "sonos",
                            "value" : "http://www.siriusxm.com/sonos"
                        },
                        {
                            "name"  : "grace",
                            "value" : "http://streaming.siriusxm.com"
                        },
                        {
                            "name"  : "control4",
                            "value" : "http://streaming.siriusxm.com"
                        },
                        {
                            "name"  : "siriusxm",
                            "value" : "http://streaming.siriusxm.com"
                        }
                    ],
                    "locale"      : "en"
                }
            ]
        },
        {
            "name"     : "appVersion",
            "settings" : [
                {
                    "stringSetting" : {
                        "value" : "Mountain_1.1.b9684c"
                    }
                }
            ]
        }
    ]
};
