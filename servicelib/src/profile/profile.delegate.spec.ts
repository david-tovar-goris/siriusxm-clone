/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import * as _ from "lodash";
import { of as observableOf } from "rxjs";
import { tap } from "rxjs/operators";
import {
    IHttpRequestConfig,
    ServiceEndpointConstants
} from "../index";
import { ProfileDelegate } from "./profile.delegate";
import {
    mockAvatarResponseFromService,
    mockProfileResponseFromApi,
    mockAvatars
} from "../test/mocks/profile/profile.mock";
import { of } from "rxjs/internal/observable/of";

describe("profile Delegate Test Suite", function()
{
    beforeEach(function()
    {
        this.mockUpdateProfilePayload = {
            profilename:'test',
            avatarid:'002'
        };
        this.profileDataResponse = _.cloneDeep(mockProfileResponseFromApi);
        this.mockHttp            =
            {
                get:  (url: string) =>
                {
                    return observableOf(this.profileDataResponse);
                },
                postModuleAreaRequest:  (url: string,data:any) =>
                {
                    return observableOf(this.mockUpdateProfilePayload);
                }
            };

        this.profileDelegate = new ProfileDelegate(this.mockHttp);
    });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.profileDelegate).toBeDefined();
            expect(this.profileDelegate instanceof ProfileDelegate).toEqual(true);
        });
    });

    describe("Get ProfileData >> ", function()
    {
        it("Should trigger proper API request to get the profile gup data", function()
        {
            spyOn(this.mockHttp, "get").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      expect(url)
                                          .toBe(ServiceEndpointConstants.endpoints.PROFILE.V4_GET_PROFILE_GUP_COLLATION);
                                      expect(data).toBe(null);

                                      expect(config.params[ "gup-type" ]).toBe("all-w-favorites");
                                      expect(config.params[ "result-template" ]).toBe("");
                                      expect(config.params[ "format" ]).toBe("json");
                                      return observableOf(mockProfileResponseFromApi);
                                  });

            this.profileDelegate.getProfileData();
            expect(this.mockHttp.get).toHaveBeenCalled();
        });

        it("Should trigger proper API request to get the empty data", function()
        {
            const mockProfileData = {
                moduleDetails : {
                    favoritesList: {}
                },
                moduleResponse: {
                    pausePointTypes   : [],
                    recentlyPlayedData: {}
                }
            };
            spyOn(this.mockHttp, "get").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      return observableOf(mockProfileData);
                                  });

            this.profileDelegate.getProfileData().subscribe(data =>
            {
                expect(data.pausePoints.length).toEqual(0);
                expect(data.recentlyPlayed.length).toEqual(0);
                expect(data.favorites.length).toEqual(0);
            });

            expect(this.mockHttp.get).toHaveBeenCalled();
        });


        it("Should trigger proper API request to get the data", function()
        {
            this.profileDelegate.getProfileData().subscribe(data =>
            {
                expect(data.favorites.length).toEqual(mockProfileResponseFromApi.moduleDetails.favoritesList.favorites.length);
                expect(data.recentlyPlayed.length).toEqual(mockProfileResponseFromApi.recentlyPlayedData.recentlyPlayeds.length);
                expect(data.pausePoints.length).toEqual(mockProfileResponseFromApi.pausePointTypes.length);

                expect(data.recentlyPlayed[ 0 ][ 'endDateTime' ] instanceof Date).toBeTruthy();
                expect(data.recentlyPlayed[ 0 ][ 'startDateTime' ] instanceof Date).toBeTruthy();

                expect(data.pausePoints[ 0 ][ 'endDateTime' ] instanceof Date).toBeTruthy();
                expect(data.pausePoints[ 0 ][ 'startDateTime' ] instanceof Date).toBeTruthy();
            });
        });
    });
    describe("Update Profile Name and Avatar >> ", function()
    {
        it("Should trigger proper API request to Update Profile", function()
        {
            spyOn(this.mockHttp, "postModuleAreaRequest").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      expect(url).toBe(ServiceEndpointConstants.endpoints.PROFILE.V3_UPDATE_PROFILE);
                                      expect(data.moduleRequest).toBeDefined();
                                      expect(data.moduleRequest.updateProfileRequest).toBeDefined();
                                      expect(data.moduleRequest.updateProfileRequest).toBe(this.mockUpdateProfilePayload);
                                      expect(config).toBe(null);
                                      return observableOf(mockProfileResponseFromApi);
                                  });

            this.profileDelegate.updateProfile(this.mockUpdateProfilePayload);
            expect(this.mockHttp.postModuleAreaRequest).toHaveBeenCalled();
        });

        it("Should trigger proper API request to update the data", function()
        {
            this.profileDelegate.updateProfile(this.mockUpdateProfilePayload).pipe(tap(data =>
            {
                expect(data['profilename']).toEqual(this.mockUpdateProfilePayload.profilename);
                expect(data['avatarid']).toEqual(this.mockUpdateProfilePayload.avatarid);
            }));
        });
    });
    describe("Fetch profile Avatars from API >> ", function()
    {
        it("Should trigger proper API request to fetch profile Avatars", function()
        {
            spyOn(this.mockHttp, "get").and
                .callFake((url, data: any, config: IHttpRequestConfig) =>
                {
                    expect(url).toBe(ServiceEndpointConstants.endpoints.PROFILE.V3_GET_PROFILE_AVATARS);
                    expect(config).toBe(null);

                    return observableOf(mockAvatarResponseFromService);
                });

            this.profileDelegate.getProfileAvatars();
            expect(this.mockHttp.get).toHaveBeenCalled();
        });

        it("Should trigger proper API request to fetch the records", function()
        {
            spyOn(this.mockHttp, "get").and
                .callFake((url, data: any, config: IHttpRequestConfig) =>
                {
                    expect(url).toBe(ServiceEndpointConstants.endpoints.PROFILE.V3_GET_PROFILE_AVATARS);
                    expect(config).toBe(null);

                    return observableOf(mockAvatarResponseFromService);
                });
            this.profileDelegate.getProfileAvatars().subscribe(data =>
            {
                expect(data).toEqual(mockAvatars);
            });
        });
    });
});
