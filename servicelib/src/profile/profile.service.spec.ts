/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />


import { throwError as observableThrowError, of as observableOf } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ProfileService } from "../profile/profile.service";
import {
    mockProfileResponseFromService,
    mockUpdateProfileNameResponseFromService
} from "../test/mocks/profile/profile.mock";
import { IProfileAvatar } from "./profile.interface";
import { IProfileData } from "../config/interfaces/all-profiles-data.interface";
import {IAppByPassState} from "../index";

describe("Profile Service Test Suite", function()
{
    beforeEach(function()
    {
        this.avatars     = [];
        this.profileResponse     = JSON.parse(JSON.stringify(mockProfileResponseFromService));
        this.updateProfileResponse     = JSON.parse(JSON.stringify(mockUpdateProfileNameResponseFromService));
        this.mockProfileDelegate = {
            getProfileData: () =>
            {
                return observableOf(this.profileResponse);
            },
            updateProfile: () =>
            {
                return observableOf(this.updateProfileResponse);
            },
            getProfileAvatars: () =>
            {
                return observableOf(this.avatars);
            }
        };
        this. mockResumeService = {
            profileData: observableOf({} as IProfileData)
        };

        this.mockAppMonitorService = {
            appByPassState : observableOf("")
        };

        this.bypassMonitorService = {
            bypassErrorState: observableOf({} as IAppByPassState)
        };

        this.profileService = new ProfileService(this.mockProfileDelegate, this.mockResumeService, this.mockAppMonitorService,
            this.bypassMonitorService);

    });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.profileService).toBeDefined();
            expect(this.profileService instanceof ProfileService).toEqual(true);
        });
    });

    describe("getProfileData >> ", function()
    {
        it("Infrastructure", function()
        {
            expect(this.profileService.getProfileData).toBeDefined();
        });
        it("Profile data on Success", function()
        {
            spyOn(this.mockProfileDelegate, "getProfileData").and
                                                        .callFake(() =>
                                                        {
                                                            return observableOf(this.profileResponse);
                                                        });

            this.profileService.getProfileData().subscribe(data =>
            {
                expect(data).toEqual(true);
            });

            expect(this.mockProfileDelegate.getProfileData).toHaveBeenCalled();
        });

        it("if profile data is empty on Success", function()
        {
            spyOn(this.mockProfileDelegate, "getProfileData").and
                                                        .callFake(() =>
                                                        {
                                                            return observableOf(undefined);
                                                        });

            this.profileService.getProfileData().subscribe(data =>
            {
                expect(data).toEqual(false);
            });
            expect(this.mockProfileDelegate.getProfileData).toHaveBeenCalled();
        });

        it("Profile data is on Error", function()
        {
            spyOn(this.mockProfileDelegate, "getProfileData").and
                                                        .callFake(() =>
                                                        {
                                                            return observableThrowError("Failure");
                                                        });

            this.profileService.getProfileData().subscribe(response =>
            {
                expect(response).toEqual(false);
            });
        });
    });

    describe("updateProfile >> ", function()
    {
        it("Infrastructure", function()
        {
            expect(this.profileService.updateProfile).toBeDefined();
        });
        it("update Profile  on Success", function()
        {
            spyOn(this.mockProfileDelegate, "updateProfile").and
                .callFake(() =>
                {
                    return observableOf(this.updateProfileResponse);
                });

            let profilename = mockUpdateProfileNameResponseFromService.profilename;
            let avatarid = mockUpdateProfileNameResponseFromService.avatarid;

            this.profileService.updateProfile(profilename, avatarid).pipe(tap(data =>
            {
                expect(data).toEqual(true);
            }));

            expect(this.mockProfileDelegate.updateProfile).toHaveBeenCalled();
        });
    });
    describe("getProfile Avatars >> ", function()
    {
        it("Infrastructure", function()
        {
            expect(this.profileService.getProfileAvatars).toBeDefined();
        });
        it("get Profile Avatars on Success", function()
        {
            spyOn(this.mockProfileDelegate, "getProfileAvatars").and
                .callFake(() =>
                {
                    return observableOf([]);
                });


            this.profileService.getProfileAvatars().subscribe(data =>
            {
                expect(data).toEqual([]);
            });

            expect(this.mockProfileDelegate.getProfileAvatars).toHaveBeenCalled();
        });
    });

});
