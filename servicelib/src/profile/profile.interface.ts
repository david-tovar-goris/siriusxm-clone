import { IFavoriteItem } from "../favorite/favorite.interface";
import { IRecentlyPlayed } from "../recently-played/recently-played.interface";
import { IAlert } from "../alerts/alert.interface";

export interface IProfileResponse
{
    favorites: any;
    recentlyPlayed: any;
    pausePoints: any;
    alerts: Array<IAlert>;
}


export interface IPausePointData
{
    deviceGuid: string;
    channelGuid: string;
    startDateTime: Date;
    endDateTime: Date;
    contentType: string;
    gupId: string;
    recentPlayType: string;
    assetGuid: string;
    assetType: string;
    startStreamTime: Date;
    endStreamTime: Date;
    percentConsumed: number;
    aodDownload: boolean;
    pausePointGuid: string;
}

export interface IProfileAvatar
{
    avatarId: string;
    url: string;
}
