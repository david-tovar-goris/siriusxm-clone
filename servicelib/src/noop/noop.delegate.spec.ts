/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />
import { throwError,  Observable, of } from 'rxjs';
import * as _ from "lodash";
import { NoopDelegate } from "./noop.delegate";

describe("Configuration delegate test suite", function()
{
    beforeEach(function()
    {
        this.delegateResponse = {
            updateFrequency: 5
        };
        this.http = {
            get: () =>
            {
                return of(this.delegateResponse);
            }
        };

        this.testSubject = new NoopDelegate(this.http);
    });

    describe("Infrastructure >>", function()
    {
        it("should have test subject", function()
        {
            expect(this.testSubject).toBeDefined();
        });

        it("Constructor", function()
        {
            expect(this.testSubject).toBeDefined();
            expect(this.testSubject instanceof NoopDelegate).toEqual(true);
        });

    });

    describe("noop() >>", function()
    {
        describe("Infrastructure >>>", function()
        {
            it("should have method defined on the test subject", function()
            {
                expect(this.testSubject).toBeDefined();
                expect(_.isFunction(this.testSubject.noop)).toBeTruthy();
            });

            it("Should return an observable.", function()
            {
                expect(this.testSubject.noop() instanceof Observable).toBeTruthy();
            });

        });

        describe("Execution >>>", function()
        {
            it("should return the response object", function()
            {
                this.testSubject.noop().subscribe(data =>
                {
                    expect(data).toEqual(5000);
                });
            });
        });

        describe("Exception handling >>>", function()
        {
            beforeEach(function()
            {
                this.delegateResponse = undefined;
                this.http = {
                    get: () =>
                    {
                        return throwError(new Error('Server error'));
                    }
                };
                this.testSubject = new NoopDelegate(this.http);
            });

            it("Should return an error when configuration object is undefined.", function(done)
            {
                this.testSubject.noop().subscribe(
                    () => {},
                    (error) =>
                    {
                        expect(error.message).toEqual('Server error');
                        done();
                    }
                );
            });

        });

    });
});
