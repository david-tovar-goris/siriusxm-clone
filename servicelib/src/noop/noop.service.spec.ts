/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />
import { BehaviorSubject, of } from 'rxjs';
import { NoopService } from "./noop.service";
import { ServiceEndpointConstants } from "../service/consts";
import { InitializationStatusCodes } from "../initialization";
import {TuneModel, TuneState} from "../tune/tune.model";


describe('Noop Service', function()
{
   beforeEach(function()
   {
       this.construct = function (initState, tuneModel)
       {
           this.noopDelegate = {
           };

           this.appMonitorService = {

           };

           this.apiDelegate = {
               addApiEndpointReporter: jasmine.createSpy('addApiEndpointReporter')
           };

           this.bypassMonitorService = {

           };

           this.initService = {
               initState: initState
           };

           this.authService = {
               isUserRegistered: function() { return false;}
           };
           this.appConfig = {
               isFreeTierEnable: false
           };

           this.tuneModel = tuneModel || { tuneState: TuneState.IDLE };

           this.service = new NoopService(
               this.noopDelegate,
               this.appMonitorService,
               this.apiDelegate,
               this.bypassMonitorService,
               this.initService,
               this.tuneModel,
               this.authService,
               this.appConfig
           );
       };
   });

   describe('constructor', function()
   {
       beforeEach(function()
       {
           this.initState = new BehaviorSubject(null);
           this.construct(this.initState);
           spyOn(this.service, 'start');
       });

       it('calls addApiEndpointReporter', function()
       {
          expect(this.apiDelegate.addApiEndpointReporter.calls.argsFor(0)[0]).toEqual(ServiceEndpointConstants.endpoints.CHANNEL.V2_NOOP);
       });

       it('does not call start() if init status is not RUNNING', function()
       {
           expect(this.service.start).not.toHaveBeenCalled();
       });

       it('calls start() if init status is RUNNING', function()
       {
           this.initState.next(InitializationStatusCodes.RUNNING);
           expect(this.service.start).toHaveBeenCalled();
       });
   });

   describe('start()', function()
   {
       describe('when tune model is idle', function()
       {
          beforeEach(function ()
          {
              jasmine.clock().install();
              this.initState = new BehaviorSubject(null);
              this.tuneModel = new TuneModel();
              this.construct(this.initState, this.tuneModel);
              spyOn(this.service, 'noop').and.returnValue(of(50000));
          });

          afterEach(function()
          {
             jasmine.clock().uninstall();
          });

          it('calls noop after default seconds', function()
          {
              this.service.start();
              jasmine.clock().tick(NoopService.NOOP_PERIOD);
              expect(this.service.noop).toHaveBeenCalledTimes(1);
          });

          it('does not call noop before default seconds', function()
          {
              this.service.start();
              jasmine.clock().tick(NoopService.NOOP_PERIOD - 1);
              expect(this.service.noop).not.toHaveBeenCalled();
          });
       });

       describe('when tune model is set to tuning', function()
       {
           beforeEach(function ()
           {
               jasmine.clock().install();
               this.initState = new BehaviorSubject(null);
               this.tuneModel = new TuneModel();
               this.construct(this.initState, this.tuneModel);
               spyOn(this.service, 'noop').and.returnValue(of(50000));
           });

           afterEach(function()
           {
               jasmine.clock().uninstall();
           });

           it('does not call noop after default seconds', function()
           {
               this.service.start();
               this.tuneModel.tuneState = TuneState.TUNING;
               jasmine.clock().tick(NoopService.NOOP_PERIOD);
               jasmine.clock().tick(1000);
               expect(this.service.noop).not.toHaveBeenCalled();
           });

           it('does call once idle again', function()
           {
               this.service.start();
               this.tuneModel.tuneState = TuneState.TUNING;
               jasmine.clock().tick(NoopService.NOOP_PERIOD + NoopService.NOOP_PERIOD + 2000);
               this.tuneModel.tuneState = TuneState.IDLE;
               expect(this.service.noop).toHaveBeenCalledTimes(1);
           });
       });
   });
});
