/**
 * @MODULE:     service-lib
 * @CREATED:    07/11/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *      Defines the interface for events used by the EventBus.
 */
export interface IEvent
{
    type: string;
    target?: any;
    currentTarget?: any;
}
