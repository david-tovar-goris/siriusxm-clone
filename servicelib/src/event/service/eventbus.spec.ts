// NOTE: Keep this here so your IDE doesn't complain about unknown Jasmine methods. I expected to not need this line
// by adding Jasmines types as a node module and/or specifying Jasmine as a type in the tsconfig.json...alas,
// this was the only way to make WebStorm happy about Jasmine (even though the TS compiled via Karma without
// any issues).
//
/// <reference path="../../../node_modules/@types/jasmine/index.d.ts" />

import Spy = jasmine.Spy;

import { EventBus } from "./eventbus";
import { IEvent } from "../event.interface";

describe("EventBus Test Suite >>", function()
{
    const fooType: string = "foo";
    const fooEvent: IEvent = {
        type: fooType
    };
    const foo: Function = () =>
    {
    };

    const barType: string = "bar";
    const barEvent: IEvent = {
        type: barType
    };
    const bar: Function = () =>
    {
    };

    const fooBarType: string = "fooBar";
    const fooBarEvent: IEvent = {
        type: fooBarType
    };
    const fooBar: Function = () =>
    {
    };

    beforeEach(function()
    {
        this.eventBus = null;
        this.fooListener = null;
        this.barListener= null;
        this.eventBus = new EventBus();

        this.fooListener = jasmine.createSpy(fooType);
        this.barListener = jasmine.createSpy(barType);
    });

    describe("Infrastructure >>", function()
    {
        it("Constructor", function()
        {
            expect( this.eventBus).toBeDefined();
            expect( this.eventBus instanceof EventBus).toEqual(true);
        });

    });

    // NOTE: We'll limit the tests for `hasEventListener()` to just the method being available as part of the
    // public API since it's used in other tests quite a bit and we'd just be repeating it. See the tests for
    // `addEventListener()`.
    describe("hasEventListener() Test Suite >>", function()
    {
        it("Expect the method to be publicly accessible.", function()
        {
            expect( this.eventBus.hasEventListener).toBeDefined();
        });

    });

    describe("addEventListener() Test Suite >>", function()
    {
        it("Expect the method to be publicly accessible.", function()
        {
            expect( this.eventBus.addEventListener).toBeDefined();
        });

        it("Expect a listener function to exist on the event bus once added.", function()
        {
            expect( this.eventBus.hasEventListener(fooType, foo)).toEqual(false);
            this.eventBus.addEventListener(fooType, foo);
            expect( this.eventBus.hasEventListener(fooType, foo)).toEqual(true);
        });

        it("Expect a listener function to exist on the event bus after other listeners are added.", function()
        {
            this.eventBus.addEventListener(fooType, foo);
            this.eventBus.addEventListener(barType, bar);
            expect( this.eventBus.hasEventListener(fooType, foo)).toEqual(true);
        });

        it("Expect a listener function to exist on the event bus after other duplicate listeners are added.", function()
        {
            this.eventBus.addEventListener(fooType, foo);
            this.eventBus.addEventListener(fooType, foo);
            expect( this.eventBus.hasEventListener(fooType, foo)).toEqual(true);
        });
    });

    describe("removeEventListener() Test Suite >>", function()
    {
        it("Expect the method to be publicly accessible.", function()
        {
            expect( this.eventBus.removeEventListener).toBeDefined();
        });

        it("Expect false since there's no matching listener to remove.", function()
        {
            expect( this.eventBus.removeEventListener(fooType, foo)).toEqual(false);
        });

        it("Expect true since there's a matching listener to remove.", function()
        {
            this.eventBus.addEventListener(fooType, foo);
            expect( this.eventBus.removeEventListener(fooType, foo)).toEqual(true);
        });

        it("Expect true since there is a matching listener among a list of multiple listeners.", function()
        {
            this.eventBus.addEventListener(fooType, foo);
            this.eventBus.addEventListener(barType, bar);
            this.eventBus.addEventListener(fooBarType, fooBar);
            expect( this.eventBus.removeEventListener(fooType, foo)).toEqual(true);
        });

        it("Expect removeEventListener to return false when there is no listener to remove.", function()
        {
            this.eventBus.addEventListener(fooType, foo);
            expect( this.eventBus.removeEventListener(fooType, fooBar)).toEqual(false);
        });

        it("Expect removeEventListener to return true when duplicate types are added .", function()
        {
            this.eventBus.addEventListener(fooType, foo);
            this.eventBus.addEventListener(fooType, bar);
            expect( this.eventBus.removeEventListener(fooType, foo)).toEqual(true);
        });
    });

    describe("dispatch() Test Suite >>", function()
    {
        it("Expect the method to be publicly accessible.", function()
        {
            expect( this.eventBus.dispatch).toBeDefined();
        });

        it("Expect false since there's no matching listener to dispatch.", function()
        {
            expect( this.eventBus.dispatch(fooEvent)).toEqual(false);
            expect( this.fooListener).not.toHaveBeenCalled();
        });

        it("Expect true since there's a matching listener to dispatch.", function()
        {
            this.eventBus.addEventListener(fooType,  this.fooListener);
            expect( this.eventBus.dispatch(fooEvent)).toEqual(true);
            expect( this.fooListener).toHaveBeenCalled();
        });

        it("Expect multiple listeners for the same event to all have been called.", function()
        {
            this.eventBus.addEventListener(fooType,  this.fooListener);
            this.eventBus.addEventListener(barType, this. barListener);

            expect( this.eventBus.dispatch(fooEvent)).toEqual(true);
            expect( this.eventBus.dispatch(barEvent)).toEqual(true);

            expect( this.fooListener).toHaveBeenCalled();
            expect( this.barListener).toHaveBeenCalled();
        });

        it("Expect false since the event type and listener have been removed.", function()
        {
            this.eventBus.addEventListener(fooType,  this.fooListener);
            expect( this.eventBus.dispatch(fooEvent)).toEqual(true);
            expect( this.fooListener).toHaveBeenCalled();

            this.eventBus.removeEventListener(fooType,  this.fooListener);
            expect( this.eventBus.hasEventListener(fooType,  this.fooListener)).toEqual(false);
            expect( this.eventBus.dispatch(fooEvent)).toEqual(false);
        });

    });

});
