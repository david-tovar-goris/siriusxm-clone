/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { throwError as observableThrowError, Observable, of as observableOf, BehaviorSubject } from 'rxjs';
import * as _ from "lodash";
import { mock } from "ts-mockito";

import { InitializationService } from "./";
import { InitializationStatusCodes } from "./initialization.const";

import {
    AuthenticationService,
    AudioPlayerService,
    CarouselService,
    ChannelLineupService,
    ConfigService,
    providers,
    ResumeService,
    SearchService,
    SettingsService, AppMonitorService, IAppByPassState
} from "../index";
import { StorageService } from "../storage/storage.service";
import { ProfileService } from "../profile/profile.service";
import { MockAppConfig } from "../test/mocks/app.config.mock";

describe("InitializationService Test Suite >>", function()
{
    beforeEach(function()
    {
        // Clear out the expected test value.
        this.expected = null;

        // Create mocks for the expected constructor params.
        this.authenticationServiceMock = mock(AuthenticationService);
        this.sessionMock = { authenticated: true, isOpenAccessEligible: false};
        this.channelLineupServiceMock = mock(ChannelLineupService);
        this.carouselServiceMock = mock(CarouselService);
        this.configServiceMock = mock(ConfigService);
        this.profileServiceMock = mock(ProfileService);
        this.resumeServiceMock = mock(ResumeService);
        this.settingsServiceMock = mock(SettingsService);
        this.audioPlayerServiceMock = mock(AudioPlayerService);
        this.storageServiceMock = mock(StorageService);
        this.searchServiceMock = mock(SearchService);
        this.appMonitorServiceMock = mock(AppMonitorService);
        this.appMonitorServiceMock.appByPassState = observableOf({} as IAppByPassState);
        this.authenticationServiceMock.userSession = { //Observable<ISession>
            subscribe: (result: Function, fault: Function) =>
            {
                this.sessionSuccessFunction = result;
                this.sessionFailureFunction = fault;

                result(this.sessionMock);
            }
        };

        this.initModelMock =
            {
                initializationState$: new BehaviorSubject("") as Observable<any>,
                setInitializationState :  jasmine.createSpy( 'setInitializationState' )
            };

        this.bypassMonitorService = {
            bypassErrorState: observableOf({} as IAppByPassState)
        };
        this.resumeServiceMock.onOpenPeriodState$ = observableOf(false);

        this.channelLineupServiceMock.channelLineup = { superCategories : observableOf([{}]) };

        spyOn(this.carouselServiceMock, "preFetchCarousels").and.callFake(() => { return observableOf(true); });

        spyOn(this.configServiceMock, "getConfig").and.callFake(() =>
        {
            return observableOf(true);
        });

        spyOn(this.channelLineupServiceMock, "getChannelLineup").and.callFake(() =>
        {
            return observableOf(true);
        });


        spyOn(this.authenticationServiceMock, "isAuthenticated").and.callFake(() =>
        {
            return this.sessionMock.authenticated;
        });

        spyOn(this.authenticationServiceMock, "isOpenAccessEligible").and.callFake(() =>
        {
            return this.sessionMock.isOpenAccessEligible;
        });

        spyOn(this.resumeServiceMock, "resume").and.callFake(() =>
        {
            return observableOf(true);
        });

        spyOn(this.settingsServiceMock, "retrieveSettings").and.callFake(() =>
        {
            return observableOf(true);
        });

        spyOn(this.profileServiceMock, "getProfileData").and.returnValue(observableOf(true));

        spyOn(this.searchServiceMock, "getRecentSearchResults").and.returnValue(observableOf(true));

        this.appConfig = new MockAppConfig();

        // Create a new test subject.
        this.testSubject = new InitializationService(
            this.authenticationServiceMock,
            this.channelLineupServiceMock,
            this.carouselServiceMock,
            this.configServiceMock,
            this.resumeServiceMock,
            this.settingsServiceMock,
            this.storageServiceMock,
            this.profileServiceMock,
            this.searchServiceMock,
            this.appMonitorServiceMock,
            this.bypassMonitorService,
            this.initModelMock,
            this.appConfig
        );
    });

    describe("Infrastructure >> ", function()
    {
        it("Should have a test subject.", function()
        {
            expect(this.testSubject).toBeDefined();
        });

        it("Should have a provider descriptor so it's available for Angular injection.", function()
        {
            const provider = providers.find(provider =>
            {
                return (provider.provide === InitializationService && provider.useClass === InitializationService);
            });
            expect(provider).toBeDefined();
            expect(provider.deps[ 0 ]).toBe(AuthenticationService);
            expect(provider.deps[ 1 ]).toBe(ChannelLineupService);
            expect(provider.deps[ 2 ]).toBe(CarouselService);
            expect(provider.deps[ 3 ]).toBe(ConfigService);
            expect(provider.deps[ 4 ]).toBe(ResumeService);
            expect(provider.deps[ 5 ]).toBe(SettingsService);
            expect(provider.deps[ 6 ]).toBe(StorageService);
            expect(provider.deps[ 7 ]).toBe(ProfileService);
            expect(provider.deps[ 8 ]).toBe(SearchService);
            expect(provider.deps[ 9 ]).toBe(AppMonitorService);
        });

        it("Should expose the expected public properties.", async() =>
        {
            expect(this.testSubject.state).toBeDefined();
            expect(this.testSubject.state).toEqual(InitializationStatusCodes.RUNNING);

        });
    });

    describe("`init()` >> ", function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.testSubject.init).toBeDefined();
                expect(_.isFunction(this.testSubject.init)).toBeTruthy();
            });

            it("Should return undefined as the function is typed void.", async() =>
            {
                expect(this.testSubject.init(observableOf(true))).toBeUndefined();
            });
        });

        describe("Execution >> ", function()
        {
            it("Should set the state to `uninitialized` when calling the GetConfig API.", function()
            {
                this.testSubject.init(observableOf(true));

                expect(this.configServiceMock.getConfig).toHaveBeenCalled();
            });

            it("Should set the state to `initializing` before checking for user authentication", async()=>
            {
                this.authenticationServiceMock.isAuthenticated = jasmine.createSpy("isAuthenticated")
                    .and.callFake(() =>
                    {
                        expect(this.testSubject.state).toEqual(InitializationStatusCodes.INITIALIZING);
                        return this.sessionMock.authenticated;
                    });

                this.testSubject.init(observableOf(true));

                expect(this.authenticationServiceMock.isAuthenticated).toHaveBeenCalled();
            });

            it("Should set the state to `unauthenticated' if user session is not authenticated", function()
            {
                this.sessionMock.authenticated = false;

                this.sessionSuccessFunction(this.sessionMock);

                expect(this.testSubject.state).toEqual(InitializationStatusCodes.UNAUTHENTICATED);
            });

            it("Should set the state to `resuming` before calling resume.", async() =>
            {
                this.sessionMock.authenticated = true;

                this.resumeServiceMock.resume = jasmine.createSpy("resume")
                    .and.callFake(() =>
                    {
                        expect(this.testSubject.state).toEqual(InitializationStatusCodes.RESUMING);
                        return observableOf(true);
                    });

                this.testSubject.init(observableOf(true));

                expect(this.resumeServiceMock.resume).toHaveBeenCalled();
            });

            it("Should set the state to `running` after full init.", async() =>
            {
                this.testSubject.init(observableOf(true));

                expect(this.testSubject.state).toEqual(InitializationStatusCodes.RUNNING);

                /**
                 * NOTE: if we can pre-fetch carousels then add this back in.  For launch, there is fear that the
                 * the API cannot handle the load, so we are taking it out.  However, being able to prefetch should
                 * make for a better user experience

                expect(carouselServiceMock.preFetchCarousels).toHaveBeenCalled();
                */
            });

            it("Should log warn messages if config fails and set state to `unauthenticated'.", async() =>
            {
                this.configServiceMock.configurationData = null;

                this.configServiceMock.getConfig = jasmine.createSpy("getConfig")
                    .and.callFake(() =>
                    {
                        return observableThrowError("Error");
                    });

                this.testSubject.init(observableOf(false));

                expect(this.testSubject.state).toEqual(InitializationStatusCodes.UNINITIALIZED);
            });

            it("Should log warn messages if Auth fails and set state to `unauthenticated'.", async() =>
            {
                this.sessionMock.authenticated = false;
                this.configServiceMock.configurationData = null;

                this.configServiceMock.getConfig = jasmine.createSpy("getConfig")
                                                     .and.callFake(() =>
                {
                    return observableOf(true);
                });

                this.authenticationServiceMock.userSession = {
                    subscribe: (result: Function, fault: Function) => fault("error")
                };

                this.testSubject.init(observableOf(true));

                expect(this.testSubject.state).toEqual(InitializationStatusCodes.UNAUTHENTICATED_RESTART);
            });

            it("Should cancel the request and set state to `failure'.", async() =>
            {
                this.configServiceMock.configurationData = {
                    subscribe: (result: Function, fault: Function) => result(undefined)
                };

                this.resumeServiceMock.resume = jasmine.createSpy("resume")
                    .and.callFake(() =>
                    {
                        return observableThrowError("error");
                    });

                this.testSubject.init(observableOf(true));

                expect(this.testSubject.state).toEqual(InitializationStatusCodes.UNINITIALIZED);
            });

            it("Should log warn messages if Auth fails and set state to `unauthenticated'.", async() =>
            {
                this.sessionMock.authenticated = false;
                this.configServiceMock.configurationData = null;

                this.configServiceMock.getConfig = jasmine.createSpy("getConfig")
                                                     .and.callFake(() =>
                    {
                        return observableOf(true);
                    });

                this.authenticationServiceMock.userSession = {
                    subscribe: (result: Function, fault: Function) => fault("error")
                };

                this.testSubject.init(observableOf(true));

                expect(this.testSubject.state).toEqual(InitializationStatusCodes.UNAUTHENTICATED_RESTART);
            });

        });
    });
});
