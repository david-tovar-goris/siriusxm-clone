import { AffinityService } from "./affinity.service";
import { AffinityDelegate } from "./affinity.delegate";
import { AffinityConstants } from "./affinity.consts";
import { MediaTimeLineService } from "../media-timeline";
import {
    TRACK_TOKEN,
    successResponseMock,
    invalidRequestResponseMock
} from "../test/mocks/data/affinity.mocks";
import { mock } from "ts-mockito";
import { HttpProvider } from "../http";
import { Observable, of as observableOf } from "rxjs";
import { ChromecastService } from "../chromecast";
describe("AffinityService Test Suite >>", function()
{
    beforeEach(function()
    {
        this.httpService = mock(HttpProvider);
        this.mediaTimeLineService = mock(MediaTimeLineService);
        this.chromecastService = mock(ChromecastService);
        this.affinityDelegate = new AffinityDelegate(this.httpService);
        this.affinityService = new AffinityService(this.affinityDelegate, this.mediaTimeLineService, this.chromecastService);
        spyOn(this.mediaTimeLineService, "getCurrentPlayingTrack").and.returnValue({assetGUID : "assetGUID"});
    });
    describe("Infrastructure >> ", function()
    {
        it("AffinityService should be defined.", function()
        {
            expect(this.affinityService).toBeDefined();
            expect(this.affinityService).toBeTruthy();
        });
    });
    describe("AffinityService method calls", function()
    {
        beforeEach(function()
        {
            spyOn(this.affinityService, 'updateAffinity');
            spyOn(this.httpService, 'post').and.returnValue(observableOf(successResponseMock));
        });
        it("on thumbsUpPlayingTrack call >> updateAffinity method should have been called with value of LIKE.", function()
        {
            this.affinityService.thumbsUpCurrentPlayingTrack();
            expect(this.affinityService.updateAffinity).toHaveBeenCalledWith(AffinityConstants.LIKE);
        });
        it("on thumbsDownPlayingTrack call >> updateAffinity method should have been called with value of DISLIKE.", function()
        {
            this.affinityService.thumbsDownCurrentPlayingTrack();
            expect(this.affinityService.updateAffinity).toHaveBeenCalledWith(AffinityConstants.DISLIKE);
        });
        it("on neutralPlayingTrack call >> updateAffinity method should have been called with value of NEUTRAL.", function()
        {
            this.affinityService.neutralCurrentPlayingTrack();
            expect(this.affinityService.updateAffinity).toHaveBeenCalledWith(AffinityConstants.NEUTRAL);
        });
    });
    describe("API response >> ", function()
    {
        beforeEach(function()
        {
            spyOn(this.httpService, 'post').and.returnValue(observableOf(successResponseMock));
        });
        it("should get success response with valid request payload", function()
        {
            this.affinityService.thumbsUpCurrentPlayingTrack().subscribe(returnVal =>
            {
                expect(returnVal).toEqual(true);
            });
        });
    });
    describe("API call is made without making seededRadio-tune call >> ", function()
    {
        beforeEach(function()
        {
            spyOn(this.httpService, 'post').and.returnValue(observableOf(invalidRequestResponseMock));
        });
        it("should get invalid-request error", function()
        {
            this.affinityService.thumbsUpCurrentPlayingTrack().subscribe(returnVal =>
            {
                expect(returnVal).toEqual(false);
            });
        });
    });
});
