/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />


import { of, Observable, throwError } from 'rxjs';
import { IAppConfig } from "../config/interfaces/app-config.interface";

import { TuneDelegate } from "./tune.delegate";

import {
    mockNowPlayingResponse,
    liveCuePoint,
    mockAODResponse,
    mockVODResponse,
    mockAICResponse,
    liveChannelData,
    normalizedLiveCuePoint,
    mockSRResponse
} from "../test/index";

import { TuneResponse } from "./index";

import {
    ChromecastModel,
    ContentTypes,
    IHttpRequestConfig,
    PlayheadTimestampService,
    RefreshTracksService,
    ServiceEndpointConstants,
    TuneChromecastService
} from "../index";
import { ConfigService } from "../config";
import { TuneModel } from "./tune.model";
import { BehaviorSubject } from "rxjs";
import { mockServiceConfig } from "../test/mocks/service.config";
import { mock } from "ts-mockito";

describe("Now Playing Delegate Test Suite", function()
{
    const channelId                      = "1234";
    const assetGuid                      = "assent12";
    const config                         = { deviceInfo : { player : "web "} };
    const configService = {
        liveVideoEnabled: () => true,
        getRelativeUrlSettings: () => [],
        getRelativeURL: (urlKey:string) => urlKey,
        getSeededRadioBackgroundUrl : () => ""
    };

    const refreshTracksService = {
        refreshingTracks$: of(false)
    };

    beforeEach(function()
    {
        this.chromecastModel = {
            isDeviceVideoSupported: false
        } as ChromecastModel;

        this.refreshTracksModelMock          =
            {
                refreshTracksModel$: new BehaviorSubject({
                    currentMetaData: {},
                    refreshState: "IDLE",
                    refreshStatus: "SUCCESS"
                }) as Observable<any>

            };

        this.tuneModel = {} as TuneModel;

        this.baseTime                        = new Date();
        jasmine.clock().install();
        jasmine.clock().mockDate(this.baseTime);

        this.playheadTimestampServiceMock = mock(PlayheadTimestampService);
        this.nowPlayingResponse = JSON.parse(JSON.stringify(mockNowPlayingResponse));
        this.aODResponse        = JSON.parse(JSON.stringify(mockAODResponse));
        this.vODResponse        = JSON.parse(JSON.stringify(mockVODResponse));
        this.aICResponse        = JSON.parse(JSON.stringify(mockAICResponse));
        this.srResponse         = JSON.parse(JSON.stringify(mockSRResponse));
        this.mockHttp           =
            {
                get: (url: string) =>
                {
                    if (url === ServiceEndpointConstants.endpoints.NOW_PLAYING.V4_LIVE)
                    {
                        return of(this.nowPlayingResponse);
                    }
                    else if (url === ServiceEndpointConstants.endpoints.NOW_PLAYING.V2_AOD)
                    {
                        return of(this.aODResponse);
                    }
                    else if (url === ServiceEndpointConstants.endpoints.NOW_PLAYING.V4_VOD)
                    {
                        return of(this.vODResponse);
                    }
                    else if (url === ServiceEndpointConstants.endpoints.ADDITIONAL_CHANNELS.V4_AIC)
                    {
                        return of(this.aICResponse);
                    }
                    else
                    {
                        return of(this.srResponse);
                    }
                }
            };

        this.streamingAdsId = "CID:STREAMINGADSID";
        this.appConfig = JSON.parse(JSON.stringify(mockServiceConfig));

        this.nowPlayingDelegate = new TuneDelegate(this.mockHttp,
            this.appConfig,
            configService as ConfigService,
            this.tuneChromecastService as TuneChromecastService,
            this.chromecastModel,
            refreshTracksService as RefreshTracksService,
            this.refreshTracksModelMock,
            this.tuneModel,
            this.playheadTimestampServiceMock);
    });

    afterEach(function()
    {
        jasmine.clock().uninstall();
    });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.nowPlayingDelegate).toBeDefined();
            expect(this.nowPlayingDelegate instanceof TuneDelegate).toEqual(true);
        });
    });

    describe("tuneLive >> ", function()
    {
        it("Should send a proper API post request to get do tuneLive ", function()
        {
            spyOn(this.mockHttp, "get").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      expect(url)
                                          .toBe(ServiceEndpointConstants.endpoints.NOW_PLAYING.V4_LIVE);
                                      expect(data).toBe(null);
                                      expect(config.params.marker_mode).toBe("all_separate_cue_points");
                                      expect(config.params.ccRequestType).toBe("AUDIO_VIDEO");
                                      expect(config.params.hls_output_mode).toBe("custom");
                                      expect(config.params.channelId).toBe(channelId);

                                      return of(this.nowPlayingResponse);
                                  });

            this.nowPlayingDelegate.tuneLive(channelId)
                              .subscribe(() => expect(this.mockHttp.get).toHaveBeenCalled());
        });

        it("Sets adsEligible param of tune call based on config's adsWizzSupported property", function()
        {
            spyOn(this.mockHttp, "get").and
                                       .callFake((url, data: any, config: IHttpRequestConfig) =>
                                       {
                                           expect(config.params.adsEligible)
                                               .toEqual(this.nowPlayingDelegate.SERVICE_CONFIG.adsWizzSupported);

                                           return of(this.nowPlayingResponse);
                                       });

            this.appConfig.adsWizzSupported = true;

            this.nowPlayingDelegate.tuneLive(channelId, null, null, true)
                .subscribe(() => expect(this.mockHttp.get).toHaveBeenCalled());
        });

        it("Should parse the API response and return a TuneResponse object ", function()
        {
            this.nowPlayingDelegate.tuneLive(channelId)
                              .subscribe((response: TuneResponse) =>
                              {
                                  expect(response.updateFrequency).toEqual(mockNowPlayingResponse.updateFrequency);
                                  expect(response.mediaType).toEqual(mockNowPlayingResponse.moduleType);
                                  expect(response.hlsConsumptionInfo).toEqual(liveChannelData.hlsConsumptionInfo);
                                  expect(response.liveCuePoint).toEqual(normalizedLiveCuePoint);
                                  expect(response.futureEpisodes).toEqual([]);
                              });
        });

        it("Should parse the API response and return an TuneResponse object if no live cue point ", function()
        {
            spyOn(this.mockHttp, "get").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      this.nowPlayingResponse.liveChannelData.cuePointList.cuePoints = [];
                                      return of(this.nowPlayingResponse);
                                  });

            this.nowPlayingDelegate.tuneLive(channelId)
                              .subscribe((response: TuneResponse) =>
                              {
                                  expect(response.liveCuePoint).toEqual({});
                              });
        });

        it("Should parse the API response and return an TuneResponse object if live channel data is empty"
            , function()
            {
                spyOn(this.mockHttp, "get").and
                                      .callFake((url, data: any, config: IHttpRequestConfig) =>
                                      {

                                          const response = {
                                              moduleType     : "live",
                                              updateFrequency: 100,
                                              aodEpisodeCount: 10,
                                              channelId      : "123",
                                              liveChannelData: {}
                                          };
                                          return of(response);
                                      });

                this.nowPlayingDelegate.tuneLive(channelId)
                                  .subscribe((response: TuneResponse) =>
                                  {
                                      expect(response.liveCuePoint).toEqual({});
                                      expect(response.updateFrequency).toEqual(response.updateFrequency);
                                      expect(response.hlsConsumptionInfo).toEqual(undefined);
                                  });
            });
    });

    describe("updateLive >> ", function()
    {
        it("Should send a proper API post request to get do tuneLive ", function()
        {
            spyOn(this.mockHttp, "get").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      expect(url)
                                          .toBe(ServiceEndpointConstants.endpoints.NOW_PLAYING.V4_LIVE);
                                      expect(data).toBe(null);
                                      expect(config.params.marker_mode).toBe("all_separate_cue_points");
                                      expect(config.params.ccRequestType).toBe("AUDIO_VIDEO");
                                      expect(config.params.hls_output_mode).toBe("none");
                                      expect(config.params.channelId).toBe(channelId);


                                      return of(this.nowPlayingResponse);
                                  });

            this.nowPlayingDelegate.updateLive(channelId)
                              .subscribe(() => expect(this.mockHttp.get).toHaveBeenCalled());
        });

        it("Should parse the API response and return an TuneResponse object ", function()
        {
            this.nowPlayingDelegate.updateLive(channelId)
                              .subscribe((response: TuneResponse) =>
                              {
                                  expect(response.updateFrequency).toEqual(mockNowPlayingResponse.updateFrequency);
                                  expect(response.mediaType).toEqual(mockNowPlayingResponse.moduleType);
                                  expect(response.hlsConsumptionInfo).toEqual(liveChannelData.hlsConsumptionInfo);
                                  expect(response.liveCuePoint.assetGUID).toEqual(liveCuePoint.assetGUID);
                              });
        });

        it('sets streamingAdsId and adsEligible params of tune call based on parameters', function()
        {
            spyOn(this.mockHttp, "get").and
                                       .callFake((url, data: any, config: IHttpRequestConfig) =>
                                       {
                                            expect(config.params.adsEligible)
                                               .toEqual(this.nowPlayingDelegate.SERVICE_CONFIG.adsWizzSupported);

                                            expect(config.params["with-ads"])
                                               .toEqual(this.streamingAdsId);

                                           return of(this.nowPlayingResponse);
                                       });

            this.appConfig.adsWizzSupported = true;

            this.nowPlayingDelegate.updateLive(channelId, this.streamingAdsId, true)
                                .subscribe();
        });
    });

    describe("tuneAOD >> ", function()
    {
        it("Should send a proper API post request to get to tuneLive ", function()
        {
            spyOn(this.mockHttp, "get").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      expect(url)
                                          .toBe(ServiceEndpointConstants.endpoints.NOW_PLAYING.V2_AOD);
                                      expect(data).toBe(null);
                                      expect(config.params.marker_mode).toBe("all_combined_cue_points");
                                      expect(config.params.ccRequestType).toBe("AUDIO_VIDEO");
                                      expect(config.params.hls_output_mode).toBe("custom");
                                      expect(config.params.channelId).toBe(channelId);
                                      expect(config.params.caId).toBe(assetGuid);

                                      return of(this.aODResponse);
                                  });

            this.nowPlayingDelegate.tuneAOD(channelId, assetGuid)
                              .subscribe(() => expect(this.mockHttp.get).toHaveBeenCalled());
        });

        it("Should parse the API response and return an MediaTimeLine object ", function()
        {
            this.nowPlayingDelegate.tuneAOD(channelId, assetGuid)
                              .subscribe((response: TuneResponse) =>
                              {
                                  expect(response.updateFrequency).toEqual(mockAODResponse.updateFrequency);
                                  expect(response.mediaType).toEqual(mockAODResponse.moduleType);
                                  expect(response.hlsConsumptionInfo)
                                      .toEqual(mockAODResponse.aodData.hlsConsumptionInfo);
                                  expect(response.liveCuePoint)
                                      .toEqual(undefined);
                              });
        });

        it("Should parse the API response and return an TuneResponse object if AOD data is empty ", function()
        {
            spyOn(this.mockHttp, "get").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {

                                      const response = {
                                          moduleType     : "live",
                                          updateFrequency: 100,
                                          aodEpisodeCount: 10,
                                          channelId      : "123",
                                          aodData        : {}
                                      };
                                      return of(response);
                                  });

            this.nowPlayingDelegate.tuneAOD(channelId, assetGuid)
                              .subscribe((response: TuneResponse) =>
                              {
                                  expect(response.liveCuePoint).toEqual({});
                                  expect(response.updateFrequency).toEqual(response.updateFrequency);
                                  expect(response.hlsConsumptionInfo).toEqual(undefined);
                              });
        });
    });

   describe("tuneVOD >> ", function() // Section added to extend tune delegate testing
    {
        it("Should send a proper API post request to get to tuneLive ", function()
        {
            spyOn(this.mockHttp, "get").and
                .callFake((url, data: any, config: IHttpRequestConfig) =>
                {
                    expect(url)
                        .toBe(ServiceEndpointConstants.endpoints.NOW_PLAYING.V4_VOD);
                    expect(data).toBe(null);
                    expect(config.params.marker_mode).toBe("all_separate_cue_points");
                    expect(config.params.channelId).toBe(channelId);
                    expect(config.params.assetGUID).toBe(assetGuid);

                    return of(this.vODResponse);
                });

            this.nowPlayingDelegate.tuneVOD(channelId, assetGuid)
                              .subscribe(() => expect(this.mockHttp.get).toHaveBeenCalled());
        });

        it("Should parse the API response and return an MediaTimeLine object ", function()
        {
            this.nowPlayingDelegate.tuneVOD(channelId, assetGuid)
                .subscribe((response: TuneResponse) =>
                {
                    expect(response.updateFrequency).toEqual(mockVODResponse.updateFrequency);
                    expect(response.mediaType).toEqual(mockVODResponse.moduleType);
                    expect(response.hlsConsumptionInfo)
                        .toEqual(mockVODResponse.vodData.hlsConsumptionInfo);
                    expect(response.liveCuePoint)
                        .toEqual(undefined);
                });
        });

        it("Should parse the API response and return an TuneResponse object if VOD data is empty ", function()
        {
            spyOn(this.mockHttp, "get").and
                .callFake((url, data: any, config: IHttpRequestConfig) =>
                {

                    const response = {
                        moduleType     : "live",
                        updateFrequency: 100,
                        aodEpisodeCount: 10,
                        channelId      : "123",
                        vodData        : {}
                    };
                    return of(response);
                });

            this.nowPlayingDelegate.tuneVOD(channelId, assetGuid)
                .subscribe((response: TuneResponse) =>
                {
                    expect(response.liveCuePoint).toEqual({});
                    expect(response.updateFrequency).toEqual(response.updateFrequency);
                    expect(response.hlsConsumptionInfo).toEqual(undefined);
                });
        });
    });

    describe("tuneAIC >> ", function() // Section added to extend tune delegate testing
    {
        it("Should send a proper API post request to get to tuneLive ", function()
        {
            spyOn(this.mockHttp, "get").and
                .callFake((url, data: any, config: IHttpRequestConfig) =>
                {
                    expect(url)
                        .toBe(ServiceEndpointConstants.endpoints.ADDITIONAL_CHANNELS.V4_AIC);
                    expect(data).toBe(null);
                    return of(this.aICResponse);
                });

            this.nowPlayingDelegate.tuneAdditionalChannels(assetGuid)
                              .subscribe(() => expect(this.mockHttp.get).toHaveBeenCalled());
        });

        it("Should parse the API response and return an MediaTimeLine object ", function()
        {
            this.nowPlayingDelegate.tuneAdditionalChannels(assetGuid)
                .subscribe((response: TuneResponse) =>
                {
                    expect(response.mediaType).toEqual(ContentTypes.ADDITIONAL_CHANNELS);
                    expect(response.sequencerSessionId).toEqual(mockAICResponse.additionalChannelData.sequencerSessionId);
                    expect(response.clips.toArray().length).toEqual(mockAICResponse.additionalChannelData.clipList.clips.length);
                });
        });

        it("Should parse the API response and return an TuneResponse object if AIC data is empty ", function()
        {
            spyOn(this.mockHttp, "get").and
                .callFake((url, data: any, config: IHttpRequestConfig) =>
                {

                    const response = {
                        wallClockRenderTime: "2018-04-13T20:52:29.688+0000",
                        moduleType     : ContentTypes.ADDITIONAL_CHANNELS,
                        updateFrequency: 100,
                        channelId: "channelId",
                        additionalChannelData        : {
                        }
                    };
                    return of(response);
                });

            this.nowPlayingDelegate.tuneAdditionalChannels(assetGuid)
                .subscribe((response: TuneResponse) =>
                {
                    expect(response.updateFrequency).toEqual(response.updateFrequency);
                    expect(response.mediaType).toEqual(ContentTypes.ADDITIONAL_CHANNELS);
                });
        });
    });

    describe("tuneSeeded >> ", function() // Section added to extend tune delegate testing
    {
        it("Should send a proper API post request to get to tuneLive ", function()
        {
            spyOn(this.mockHttp, "get").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      expect(url)
                                          .toBe(ServiceEndpointConstants.endpoints.SEEDED_RADIO.V4_TUNE);
                                      expect(data).toBe(null);
                                      return of(this.srResponse);
                                  });

            this.nowPlayingDelegate.tuneSeededRadio(assetGuid)
                              .subscribe(() => expect(this.mockHttp.get).toHaveBeenCalled());
            expect(this.mockHttp.get).toHaveBeenCalled();
        });

        it("Should parse the API response and return an MediaTimeLine object ", function()
        {
            this.nowPlayingDelegate.tuneSeededRadio(assetGuid)
                              .subscribe((response: TuneResponse) =>
                              {
                                  expect(response.mediaType).toEqual(ContentTypes.SEEDED_RADIO);
                                  expect(response.mediaId).toEqual(mockSRResponse.seededRadioData.channel.stationFactory);
                                  expect(response.clips.toArray().length).toEqual(mockSRResponse.seededRadioData.clipList.clips.length);
                              });
        });

        it("Should parse the API response and return an MediaTimeLine object ", function()
        {
            this.nowPlayingDelegate.tuneSeededRadio(assetGuid)
                              .subscribe((response: TuneResponse) =>
                              {
                                  expect(response.mediaType).toEqual(ContentTypes.SEEDED_RADIO);
                                  expect(response.mediaId).toEqual(mockSRResponse.seededRadioData.channel.stationFactory);
                                  expect(response.clips.toArray().length).toEqual(mockSRResponse.seededRadioData.clipList.clips.length);
                              });
        });

        it("NO Tune call made - When refresh state not IDLE ", function()
        {
            this.refreshTracksModelMock.refreshTracksModel$.next({
                currentMetaData: {},
                refreshState: "REFRESHING",
                refreshStatus: ""
            });
            spyOn(this.mockHttp, "get");

            this.nowPlayingDelegate.tuneSeededRadio(assetGuid).subscribe(response =>
            {
            });

            expect(this.mockHttp.get).not.toHaveBeenCalled();
        });

        it("Tune call made - When refresh state  IDLE ", function()
        {
            this.refreshTracksModelMock.refreshTracksModel$.next({
                currentMetaData: {},
                refreshState: "IDLE",
                refreshStatus: ""
            });
             spyOn(this.mockHttp, "get").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                       return of(this.srResponse);
                                  });

            this.nowPlayingDelegate.tuneSeededRadio(assetGuid).subscribe(response =>
            {
            });

            expect(this.mockHttp.get).toHaveBeenCalled();
        });

        it("tune call retries - Multiple times while API returned 315 error until  Maximum limit reached", function()
        {
            this.srResponse = {
                "ModuleListResponse": {
                    "messages": [{ "message": "TRACKS_UNAVAILABLE: No SXM Tracks found", "code": 315 }],
                    "status": 0
                },
                "code": 315
            };

            this.refreshTracksModelMock.refreshTracksModel$.next({
                currentMetaData: {
                    mediaId: "mediaId"
                },
                refreshState: "IDLE",
                refreshStatus: "SUCCESS"
            });

            spyOn(this.mockHttp, "get").and
                    .callFake((url, request) =>
                    {
                        expect(url).toEqual(ServiceEndpointConstants.endpoints.SEEDED_RADIO.V4_TUNE);
                        return throwError(this.srResponse);
                    });

            const onFailure = jasmine.createSpy("onFailure");
            const onSuccess = jasmine.createSpy("onSuccess");

            this.nowPlayingDelegate.tuneSeededRadio(assetGuid)
                    .subscribe(onSuccess, onFailure);
            const SEEDED_RETRIES_ALLOWED         = 2;

            expect(onSuccess).not.toHaveBeenCalled();
            expect(onFailure.calls.count()).toBe(1);
            expect(this.mockHttp.get.calls.count()).toBe(SEEDED_RETRIES_ALLOWED + 1);
        });

        it("Refresh call retries - Multiple times while API returned 315 error until successfull response reached ", function()
        {
            let fail = true;
            this.srResponse = {
                "ModuleListResponse": {
                    "messages": [{ "message": "TRACKS_UNAVAILABLE: No SXM Tracks found", "code": 315 }],
                    "status": 0
                },
                "code": 315
            };

            this.refreshTracksModelMock.refreshTracksModel$.next({
                currentMetaData: {
                    mediaId: "mediaId"
                },
                refreshState: "IDLE",
                refreshStatus: "SUCCESS"
            });

            const SEEDED_RETRIES_ALLOWED         = 2;
            let retries = SEEDED_RETRIES_ALLOWED;

            spyOn(this.mockHttp, "get").and
                                  .callFake((url, request) =>
                                  {
                                      retries -= 1;
                                      if(retries === 0)
                                      {
                                          this.srResponse = {
                                              stationFactory: "stationFactory",
                                              mediaId: "mediaId"
                                          };
                                          fail = false;
                                      }
                                      expect(url).toEqual(ServiceEndpointConstants.endpoints.SEEDED_RADIO.V4_TUNE);

                                      return fail === true
                                          ? throwError(this.srResponse)
                                          : of(this.srResponse);
                                  });

            const onFailure = jasmine.createSpy("onFailure");
            const onSuccess = jasmine.createSpy("onSuccess").and.callFake((response) =>
            {
                expect(response).toEqual(this.srResponse);
            });

            this.nowPlayingDelegate.tuneSeededRadio(assetGuid)
                    .subscribe(onSuccess, onFailure);

            expect(this.mockHttp.get.calls.count()).toBe(SEEDED_RETRIES_ALLOWED);
            expect(onSuccess).toHaveBeenCalled();
            expect(onSuccess).toHaveBeenCalledWith(this.srResponse);
            expect(onSuccess.calls.count()).toBe(1);
            expect(onFailure.calls.count()).toBe(0);
        });
    });
});
