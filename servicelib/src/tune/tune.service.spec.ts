/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />


import {throwError as observableThrowError, of as observableOf, Observable, BehaviorSubject} from 'rxjs';

import {
    liveTuneResponseMock,
    aodTuneResponseMock,
    superCategoriesMock,
    mockCategoryChannels
} from "../test/index";
import { mock } from "ts-mockito";
import { TuneService } from "./index";

import {
    IOnDemandShow,
    EventBus,
    ApiDelegate,
    AppMonitorService,
    PlayerTypes,
    ChromecastModel,
    ClientCodes,
    IAppByPassState,
    ApiCodes
} from "../index";
import { vodTuneResponseMock }     from "../test/mocks/tune/tune.service.response";
import { StorageService }          from "../storage/storage.service";
import { ContentTypes }            from "../service/types/content.types";

describe("Tune Service Test Suite", function()
{
    const channelId                    = "1234";
    const assetGuid                    = "assetTestGuid";
    const vodAssetGuid                       = "vodAssetTestGuid";
    const failureMessage                     = "FailMessage";

    beforeEach(function()
    {
        this.tuneService = null;
        this.mockNowPlayingDelegate    = null;
        this.tuneResponse             = null;
        this.aODResponse                    = null;
        this.vODResponse                    = null;
        this.resumeServiceMock              = null;
        this.mockStorageService             = null;
        this.resumeMediaMock                = null;
        this.channelLineupServiceMock       = null;
        this.apiDelegate                     = null;
        this.apiDelegate = new ApiDelegate(new EventBus);
        this.appMonitorServiceMock = {
            ...mock(AppMonitorService),
            handleMessage: jasmine.createSpy('handleMessage')
        };
        jasmine.clock().install();

        this.tuneResponse = JSON.parse(JSON.stringify(liveTuneResponseMock));
        this.aODResponse        = JSON.parse(JSON.stringify(aodTuneResponseMock));
        this.vODResponse        = JSON.parse(JSON.stringify(vodTuneResponseMock));
        this.resumeMediaMock    = {
            data           : this.aODResponse,
            updateFrequency: 10,
            type           : "live"
        };

        this.resumeServiceMock                 = {};
        this.resumeServiceMock.resumeMedia     = observableOf(this.resumeMediaMock);
        const shows: Array<IOnDemandShow> = [];
        this.tuneModelMock =
            {
                currentMetaData : {}
            };

        this.channelLineupServiceMock = {
            channelLineup: {
                superCategories: {
                    take     : () =>
                    {
                        return {
                            subscribe: (result: Function, fault: Function) => result(superCategoriesMock)
                        };
                    },
                    subscribe: (result: Function, fault: Function) => result(superCategoriesMock),
                    flatMap  : (mapResult: Function) => mapResult(superCategoriesMock)
                }
            },
            discoverAod  : function () { return observableOf(shows); },
            findAodEpisodeByGuid : jasmine.createSpy("findAodEpisodeByGuid")
                                          .and.callFake((channelId : string, guid : string) =>
                {
                    if ( guid === assetGuid )
                    { return observableOf({ type : ContentTypes.AOD }); }
                    return observableOf(null);
                }),

            findVodEpisodeByGuid : jasmine.createSpy("findAodEpisodeByGuid")
                                            .and.callFake((channelId : string, guid : string) =>
            {
              if ( guid === vodAssetGuid ) { return observableOf({ type : ContentTypes.VOD }); }
              return observableOf(null);
            }),

            discoverOnDemandByChannelId : jasmine.createSpy("discoverOnDemandByChannelId").and.returnValue([]),
            findChannelById : jasmine.createSpy("findChannelById").and.callFake((channelId : string) =>
            {
                if (channelId)
                {
                    return mockCategoryChannels[0];
                }
                else
                {
                    return null;
                }
            })
        };

        this.mockNowPlayingDelegate = {
            tuneLive          : () =>
            {
                return observableOf(this.tuneResponse);
            },
            updateLive        : () =>
            {
                return observableOf(this.tuneResponse);
            },
            tuneAOD           : () =>
            {
                return observableOf(this.aODResponse);
            },
            tuneVOD           : () =>
            {
                return observableOf(this.vODResponse);
            }
        };
        this.mockStorageService =  mock(StorageService);

        spyOn(this.mockNowPlayingDelegate, "updateLive").and.callThrough();
        spyOn(this.mockNowPlayingDelegate, "tuneLive").and.callThrough();
        spyOn(this.mockNowPlayingDelegate, "tuneAOD").and.callThrough();
        spyOn(this.mockNowPlayingDelegate, "tuneVOD").and.callThrough();

        // forkJoinSpy = spyOn(Observable, "forkJoin").and.callFake(() => pipe: (mapResult: Function) => mapResult(true) };

        this.geolocationServiceMock = { checkContent: jasmine.createSpy("checkContent").and.returnValue((observableOf(ClientCodes.SUCCESS))) };

        this.configServiceMock = {
            liveVideoEnabled: () => true,
            getRelativeUrlSettings: () => [],
            getRelativeURL: (urlKey) => urlKey,
            getSeededRadioBackgroundUrl : () => ""
        };
        this.sessionTransferServiceMock =  { castStatusChange: jasmine.createSpy("castStatusChange").and.returnValue((observableOf(true))) };
        this.chromecastModelMock = {
            isDeviceVideoSupported: false
        } as ChromecastModel;

        this.bypassMonitorService = {
            bypassErrorState: observableOf({} as IAppByPassState)
        };

        this.SERVICE_CONFIG_MOCK = {
            adsWizzSupported: true
        };

        this.mockMediaPlayerModel = {
            playbackState$ : new BehaviorSubject(''),
            restart$ : new BehaviorSubject(false),
            bufferUnderFlow$ : new BehaviorSubject(false)
        } as any;


        this.tuneService = new TuneService(this.resumeServiceMock, this.channelLineupServiceMock, this.mockNowPlayingDelegate,
            this.mockStorageService, this.geolocationServiceMock, this.appMonitorServiceMock, this.configServiceMock,
            this.sessionTransferServiceMock, this.chromecastModelMock, this.bypassMonitorService, this.tuneModelMock,
            this.SERVICE_CONFIG_MOCK, this.mockMediaPlayerModel);
    });

    afterEach(function()
    {
        jasmine.clock().uninstall();
    });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.tuneService).toBeDefined();
            expect(this.tuneService instanceof TuneService).toEqual(true);
        });

        it("should have public properties defined..", function()
        {
            expect(this.tuneService.tuneMediaTimeLine).toBeDefined();

            expect(TuneService.DEFAULT_LIVE_UPDATE_TUNE_QUERY_PERIOD_MS).toBeDefined();
            expect(TuneService.DEFAULT_LIVE_UPDATE_TUNE_QUERY_PERIOD_MS).toEqual(100000);
        });
    });

    describe("setMediaData >> ", function()
    {
        it("Constructor- with sending resumeMedia as null", function()
        {
            this.resumeServiceMock.resumeMedia = observableOf(null);

            this.tuneService = new TuneService(this.resumeServiceMock,
                this.channelLineupServiceMock,
                this.mockNowPlayingDelegate,
                this.mockStorageService,
                this.geolocationServiceMock,
                this.appMonitorServiceMock,
                this.configServiceMock,
                this.sessionTransferServiceMock,
                this.chromecastModelMock,
                this.bypassMonitorService,
                this.tuneModelMock,
                this.SERVICE_CONFIG_MOCK,
                this.mockMediaPlayerModel);

            expect(this.tuneService).toBeDefined();
            expect(this.tuneService instanceof TuneService).toEqual(true);
        });

        it("Constructor - with sending resumeMedia as aod data ", function()
        {
            this.resumeMediaMock.data          = this.aODResponse;
            this.resumeMediaMock.type          = "aod";

            let tuneServiceLocal = new TuneService(this.resumeServiceMock,
                this.channelLineupServiceMock,
                this.mockNowPlayingDelegate,
                this.mockStorageService,
                this.geolocationServiceMock,
                this.appMonitorServiceMock,
                this.configServiceMock,
                this.sessionTransferServiceMock,
                this.chromecastModelMock,
                this.bypassMonitorService,
                this.tuneModelMock,
                this.SERVICE_CONFIG_MOCK,
                this.mockMediaPlayerModel);
            expect(tuneServiceLocal).toBeDefined();
            expect(tuneServiceLocal instanceof TuneService).toEqual(true);
        });
    });

    describe("tuneLive >> ", function()
    {
        describe("Execution >> ", function()
        {
            afterEach(function()
            {
                // IMPORTANT, don't want this unit test bleeding into other unit tests
                this.tuneService.stopPeriodicLiveUpdateRequests();
                liveTuneResponseMock.updateFrequency = 100;
            });

            it("Returns successful response and sets the periodic live update requests", function()
            {
                const channelIdFromMock = mockCategoryChannels[ 0 ].channelGuid;
                const streamingAdsId = "CID:STREAMINGADSID";
                spyOn(this.tuneService, "startPeriodicLiveUpdateRequests").and.callThrough();

                const tuneLive = jasmine.createSpy("tuneLive")
                                        .and.callFake((response) =>
                    {
                         expect(response).toEqual(ClientCodes.SUCCESS);

                        expect(this.tuneService.startPeriodicLiveUpdateRequests)
                            .toHaveBeenCalledWith(liveTuneResponseMock.updateFrequency, channelIdFromMock, undefined);

                        expect(this.mockNowPlayingDelegate.tuneLive
                                                     .calls.count()).toBe(1);

                        expect(this.mockNowPlayingDelegate.updateLive
                                                     .calls.count()).toBe(0);

                        jasmine.clock().tick(liveTuneResponseMock.updateFrequency * 1000 + 1);

                        expect(this.mockNowPlayingDelegate.updateLive
                                                     .calls.count()).toBe(1);

                        jasmine.clock().tick(liveTuneResponseMock.updateFrequency * 1000 + 1);

                        expect(this.mockNowPlayingDelegate.updateLive
                                                     .calls.count()).toBe(2);

                        this.tuneService.startPeriodicLiveUpdateRequests(liveTuneResponseMock.updateFrequency / 2,
                            channelIdFromMock, streamingAdsId);

                        jasmine.clock().tick((liveTuneResponseMock.updateFrequency / 2) * 1000 + 1);

                        expect(this.mockNowPlayingDelegate.updateLive
                                                     .calls.count()).toBe(3);

                        this.tuneService.startPeriodicLiveUpdateRequests(0, channelIdFromMock, streamingAdsId);

                        jasmine.clock().tick((liveTuneResponseMock.updateFrequency / 2) * 1000 + 1);

                        expect(this.mockNowPlayingDelegate.updateLive
                                                     .calls.count()).toBe(4);
                    });
                const payload = {
                    channelId:mockCategoryChannels[0].channelId,
                    episodeIdentifier:"",
                    contentType: ""
                };
                this.tuneService.tune(payload).subscribe(tuneLive);
            });

            it("Returns failure response  and sets the periodic live update requests", function()
            {
                this.mockNowPlayingDelegate.tuneLive = () => {};

                spyOn(this.mockNowPlayingDelegate, "tuneLive").and
                                                         .callFake(() =>
                                                         {
                                                             return observableThrowError(failureMessage);
                                                         });

                const payload = {
                    channelId:mockCategoryChannels[0].channelId,
                    episodeIdentifier:"",
                    contentType: ""
                };
                this.tuneService.tune(payload).subscribe(null, (response) =>
                {
                    expect(response).toEqual(failureMessage);
                });
            });

            it("Returns successful response and sets the periodic live update requests when " +
                "frequency undefined", function()
            {
                this.resumeMediaMock.data          = this.aODResponse;
                this.resumeMediaMock.type          = "aod";
                this.resumeServiceMock.resumeMedia = {
                    subscribe: (result: Function, fault: Function) => result(this.resumeMediaMock)
                };

                this.mockNowPlayingDelegate.tuneLive = () => {};

                spyOn(this.mockNowPlayingDelegate, "tuneLive").and
                                                         .callFake(() =>
                                                         {
                                                             liveTuneResponseMock.updateFrequency = undefined;

                                                             return observableOf(liveTuneResponseMock);
                                                         });

                spyOn(this.tuneService, "startPeriodicLiveUpdateRequests").and.callThrough();

                const tuneLive = jasmine.createSpy("tuneLive")
                                        .and.callFake((response) =>
                    {
                        expect(response).toEqual(ClientCodes.SUCCESS);

                        expect(this.tuneService.startPeriodicLiveUpdateRequests)
                            .toHaveBeenCalledWith(undefined, mockCategoryChannels[0].channelId, undefined);

                        expect(this.mockNowPlayingDelegate.tuneLive
                                                     .calls.count()).toBe(1);

                        expect(this.mockNowPlayingDelegate.updateLive
                                                     .calls.count()).toBe(0);

                        jasmine.clock().tick(TuneService.DEFAULT_LIVE_UPDATE_TUNE_QUERY_PERIOD_MS + 1);

                        expect(this.mockNowPlayingDelegate.updateLive
                                                     .calls.count()).toBe(10);
                    });
                const payload = {
                    channelId:mockCategoryChannels[0].channelId,
                    episodeIdentifier:"",
                    contentType: ""
                };
                this.tuneService.tune(payload).subscribe(tuneLive);
            });

            it("Returns successful response as false", function()
            {
                const payload = {
                    channelId:undefined,
                    episodeIdentifier:"",
                    contentType: ""
                };
                this.tuneService.tune(payload).subscribe((response: number) =>
                {
                    expect(response).toEqual(ClientCodes.ERROR);
                });
            });
        });
    });

    describe("tuneAOD >> ", function()
    {
        describe("Execution >> ", function()
        {
            it("Returns successful response", function()
            {
                const payload = {
                    channelId:mockCategoryChannels[0].channelId,
                    episodeIdentifier:"assetGuid",
                    contentType: "aod"
                };
                this.tuneService.tune(payload).subscribe((response: number) =>
                {
                    expect(this.mockNowPlayingDelegate.tuneAOD.calls.count()).toBe(1);
                    expect(response).toEqual(ClientCodes.SUCCESS);
                });
            });

            it("Returns successful response as ERROR", function()
            {
                const payload = {
                    channelId:undefined,
                    episodeIdentifier:assetGuid,
                    contentType: "aod"
                };
                this.tuneService.tune(payload).subscribe((response: number) =>
                {
                    expect(this.mockNowPlayingDelegate.tuneAOD.calls.count()).toBe(0);
                    expect(response).toEqual(ClientCodes.ERROR);
                });
            });
        });
    });

    describe("tuneVOD >> ", function()
    {
        describe("Execution >> ", function()
        {
            it("Returns successful response", function()
            {
                const payload = {
                    channelId:mockCategoryChannels[0].channelId,
                    episodeIdentifier:vodAssetGuid,
                    contentType: "vod"
                };
                this.tuneService.tune(payload).subscribe((response: number) =>
                {
                    expect(this.mockNowPlayingDelegate.tuneVOD.calls.count()).toBe(1);
                    expect(response).toEqual(ClientCodes.SUCCESS);
                });
            });

            it("Returns successful response as ERROR", function()
            {
                const payload = {
                    channelId:undefined,
                    episodeIdentifier:vodAssetGuid,
                    contentType: "vod"
                };
                this.tuneService.tune(payload).subscribe((response: number) =>
                {
                    expect(this.mockNowPlayingDelegate.tuneVOD.calls.count()).toBe(0);
                    expect(response).toEqual(ClientCodes.ERROR);
                });
            });
        });
    });

    describe("updateLive >> ", function()
    {
        it("Infrastructure", function()
        {
            expect(this.tuneService.updateLive).toBeDefined();
        });

        describe("Execution >> ", function()
        {
            it("Returns successful response", function()
            {
                this.tuneService.updateLive(channelId).subscribe((response: number) =>
                {
                    expect(response).toEqual(ClientCodes.SUCCESS);
                });
            });

            it("Returns failure response", function()
            {
                this.mockNowPlayingDelegate.updateLive = () => {};

                spyOn(this.mockNowPlayingDelegate, "updateLive").and
                                                           .callFake(() =>
                                                           {
                                                               return observableThrowError(failureMessage);
                                                           });

                this.tuneService.updateLive(channelId).subscribe(null, (response) =>
                {
                    expect(response).toEqual(failureMessage);
                });
            });

            it('force re-tunes if Error is due to ADSWIZZ_FAILOVER with adsWizz set to disabled', function()
            {
                this.mockNowPlayingDelegate.updateLive = () => {};

                spyOn(this.tuneService, 'retune');

                spyOn(this.mockNowPlayingDelegate, "updateLive").and
                                                                .callFake(() =>
                                                                {
                                                                    return observableThrowError({
                                                                        code: ApiCodes.ADSWIZZ_FAILOVER
                                                                    });
                                                                });

                this.tuneService.updateLive(channelId).subscribe(null, (response) =>
                {
                    expect(this.tuneService.retune).toHaveBeenCalled();
                    expect(this.tuneService.forceRetune).toEqual(true);
                    expect(this.tuneService.enableAdsWizz$.getValue()).toEqual(false);
                });
            });
        });
    });
});
