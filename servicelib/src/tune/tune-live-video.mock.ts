import * as _ from "lodash";
import {
    ApiLayerTypes, IMediaVideo,
    ITime,
    MediaPlayerConstants,
    TuneResponse
} from "../index";
import {
    IMediaEpisode,
    IMediaItem
} from "./tune.interface";

export class NowPlayingLiveVideoMock
{
    /**
     * The regex pattern for a timestamp in the live video playlist including the key and value.
     */
    private static PATTERN_LIVE_VIDEO_PROGRAM_TIME = "#EXT-X-PROGRAM-DATE-TIME:([0-9]+)-([0-9]+)-([0-9]+)+T([0-9]+):([0-9]+):([0-9]+).([0-9]+)Z";

    /**
     * The regex pattern for a timestamp in the live video playlist just for the key.
     */
    private static PATTERN_DATE_TIME: string = "#EXT-X-PROGRAM-DATE-TIME:";

    /**
     * The channel ID for Howard 100.
     */
    private static CHANNEL_HOWARD_100: string = "howardstern100";

    /**
     * The channel ID for Howard 101.
     */
    private static CHANNEL_HOWARD_101: string = "howardstern101";

    /**
     * The number of milliseconds to add to non-live, rebroadcast offset start times.
     */
    private static NOT_LIVE_FUDGE_FACTOR: number = 1000 * 20;

    /**
     * The number of milliseconds in one minute.
     * @type {number}
     */
    private static MINUTE: number = 1000 * 60;

    /**
     * Mocks data for live video for the howard stern channels. Specific live video data mocked is:
     *
     *      - Playlist URL
     *      - Marker Point Where Video is Available
     *      - Cut Zero-Based Offset
     *      - Segments Zero-Based Offset
     *
     * @param {TuneResponse} tuneResponse
     * @returns {Promise<any>}
     */
    public static async mock(tuneResponse: TuneResponse): Promise<any>
    {
        return new Promise((resolve, reject) =>
        {
            // TODO this is a mock, once the API gives us video and a video marker layer, we must take this out
            if (NowPlayingLiveVideoMock.isMediaLiveVideoEnabled(tuneResponse.mediaId))
            {
                // TODO: BMR: This value changes daily and we need from OO.
                const videoEventPlaylistUrl: string = "" +
                    "https://siriusxm-no-token-priuatlivevideo.siriusxm.com/video/" +
                    "e7aca86786b84c3baf413baf0ae7b58d/direct/110324/l5dG1wZTE6Bn7nVfw0IT53pwQdGgDQRz/en/d6e5e75afdec4f8fb5b452086b4683a3_1.m3u8";

                const mockVideoEndpoint =
                    {
                        mediaFirstChunks: [],
                        manifestFiles: [ { name: MediaPlayerConstants.HLS, size: "LARGE", url: videoEventPlaylistUrl } ],
                        name: MediaPlayerConstants.HLS,
                        position: undefined,
                        size: "LARGE",
                        url: videoEventPlaylistUrl
                    };

                NowPlayingLiveVideoMock.getUrl(videoEventPlaylistUrl,
                    (manifest) =>
                    {
                        const firstTimestamp: number = NowPlayingLiveVideoMock.getFirstTimestampFromPlaylist(manifest).getTime();
                        NowPlayingLiveVideoMock.addZeroBasedTimes(tuneResponse.episodes, tuneResponse.cuts, firstTimestamp);

                        resolve(true);
                    },
                    (error) =>
                    {
                        console.error("bmr loading video playlist failed", error);
                        resolve(true);
                    }
                );

                if (tuneResponse.cuts.length > 0)
                {
                    // Create a list of video markers that range from 7AM EST to 11AM EST (aka within the first episode)
                    // and put a delay between them so we can see the switch to live video button show and hide (as it
                    // should only when a video marker exists for the current audio timestamp.
                    const today: Date = new Date();
                    const startTime: number = today.setHours(12, 0, 0, 0);
                    const endTime: number = today.setHours(23, 0, 0, 0);
                    const markerLength: number = NowPlayingLiveVideoMock.MINUTE;
                    const delay: number = NowPlayingLiveVideoMock.MINUTE;

                    tuneResponse.videos = NowPlayingLiveVideoMock.createVideoMarkers(3, startTime, endTime, markerLength, delay);
                    tuneResponse.mediaEndPoints.push(mockVideoEndpoint);
                }
            }
            else
            {
                resolve(true);
            }
        });
    }

    public static createVideoMarkers(count: number, startTime: number, endTime: number, markerLength: number = 0, delay: number = 0): IMediaVideo[]
    {
        markerLength = markerLength || NowPlayingLiveVideoMock.MINUTE;

        const result: IMediaVideo[] = [];
        let markerStartTime: number = 0;
        let markerEndTime: number = 0;

        for (let i: number = 0; i < count; i++)
        {
            // Make the marker's start the start time of the episode for the first item then
            // make it the last marker's end time + a delay.
            markerStartTime = (i === 0)
                ? startTime
                : markerEndTime + delay;

            // Make the marker's end time the start time of the episode plus the marker length for the first item then
            // make it the last marker's start time plus the marker length for all items other than the last one, then
            // make the last one the end time for the episode.
            markerEndTime = (i === 0)
                ? startTime + markerLength
                : ((i + 1) >= count)
                    ? endTime
                    : markerStartTime + markerLength;

            result.push(NowPlayingLiveVideoMock.createVideoMarker(markerStartTime, markerEndTime));
        }

        return result;
    }

    public static createVideoMarker(startTime: number, endTime: number): IMediaVideo
    {
        const videoMarker: IMediaVideo = {} as IMediaVideo;
        videoMarker.times = {} as ITime;

        videoMarker.assetGUID = "";
        videoMarker.layer = ApiLayerTypes.VIDEO_LAYER;

        videoMarker.times.zuluStartTime = startTime;
        videoMarker.times.isoStartTime = new Date(startTime).toISOString();

        videoMarker.times.zuluEndTime = endTime;
        videoMarker.times.isoEndTime = new Date(endTime).toISOString();

        // console.log("bmr Video Marker startTime >>", new Date(startTime));
        // console.log("bmr Video Marker endTime >>", new Date(endTime));

        return videoMarker;
    }

    public static isMediaLiveVideoEnabled(mediaId: string): boolean
    {
        return NowPlayingLiveVideoMock.isHoward100(mediaId) || NowPlayingLiveVideoMock.isHoward101(mediaId);
    }

    public static isHoward100(mediaId: string): boolean
    {
        return mediaId === NowPlayingLiveVideoMock.CHANNEL_HOWARD_100;
    }

    public static isHoward101(mediaId: string): boolean
    {
        return mediaId === NowPlayingLiveVideoMock.CHANNEL_HOWARD_101;
    }

    public static getNotLiveFudgeFactor(episodeStartTime: number): number
    {
        // return !NowPlayingLiveVideoMock.isLiveTime(episodeStartTime) ? NowPlayingLiveVideoMock.NOT_LIVE_FUDGE_FACTOR : 0;
        return !NowPlayingLiveVideoMock.isLiveTime(episodeStartTime) ? 5000 : 0;
    }

    public static isLiveTime(timestamp: number): boolean
    {
        const isLiveDay: boolean = NowPlayingLiveVideoMock.isLiveDay(timestamp);
        const isLiveHour: boolean = NowPlayingLiveVideoMock.isLiveHour(timestamp);
        console.log("bmr isLiveTime >>", isLiveDay && isLiveHour);

        return isLiveDay && isLiveHour;
    }

    public static isLiveDay(timestamp: number): boolean
    {
        const day: number = new Date(timestamp).getDay();
        const isMonday: boolean = day === 1;
        const isTuesday: boolean = day === 2;
        const isWednesday: boolean = day === 3;

        return isMonday || isTuesday || isWednesday;
    }

    public static isLiveHour(timestamp: number): boolean
    {
        const liveEpisodeStartHour: number = 7;
        const liveEpisodeEndHour: number = 11;
        const hours: number = new Date(timestamp).getHours();

        return (hours >= liveEpisodeStartHour) && (hours <= liveEpisodeEndHour);
    }

    public static addZeroBasedTimes(episodes: IMediaEpisode[], mediaItems: IMediaItem[], firstTimestamp: number): void
    {
        const today: Date = new Date();

        // TODO: BMR: This value changes daily and we need from SXM, although it should theoretically be 7AM everyday.
        // Hardcoded constant for each day that indicates the start time of the episode; this will change everyday.
        const videoEpisodeStartTime: number = today.setHours(7, 0, 0, 0);

        // The amount of time between the start of the episode and the first timestamp; this value can be positive or negative.
        const preRollOffset: number = videoEpisodeStartTime - firstTimestamp;

        console.log("bmr addZeroBasedTimes: First timestamp >>", new Date(firstTimestamp));
        console.log("bmr addZeroBasedTimes: Episode start >>", new Date(videoEpisodeStartTime));
        console.log("bmr addZeroBasedTimes: Pre-Roll offset ms >>", preRollOffset);

        episodes.forEach((episode: IMediaEpisode, index: number) =>
        {
            const episodeZuluStartTime: number = episode.times.zuluStartTime;
            const episodeZuluEndTime: number = episode.times.zuluEndTime;
            console.log("bmr addZeroBasedTimes: Episode Start Time ms >>", new Date(episodeZuluStartTime));

            // The number of milliseconds to add to non-live, rebroadcast offset start times or zero for live days.
            const notLiveFudgeFactor: number = NowPlayingLiveVideoMock.getNotLiveFudgeFactor(episodeZuluStartTime);
            console.log("bmr addZeroBasedTimes: Not-Live Fudge Factor offset ms >>", notLiveFudgeFactor);

            // Find the first and last media element using a target zulu timestamp.
            const startItem: IMediaItem = NowPlayingLiveVideoMock.findMediaItemByTimestamp(episodeZuluStartTime, mediaItems);
            const endItem: IMediaItem = NowPlayingLiveVideoMock.findMediaItemByTimestamp(episodeZuluEndTime, mediaItems);

            // Find the start and end index for those start and end items.
            const startIndex: number = _.findIndex(mediaItems, startItem);
            const endIndex: number = _.findIndex(mediaItems, endItem);

            // Select the range of items from the list using the start and end index.
            const episodeMediaItems: IMediaItem[] = mediaItems.slice(startIndex, endIndex + 1) as IMediaItem[];

            // Loop through the items and make sure their duration properties are all set correctly.
            episodeMediaItems.forEach((item: IMediaItem, index: number) =>
            {
                // Shortcuts to the cut or segment start and end times.
                const itemStartTime: number = item.times.zuluStartTime;
                const itemEndTime: number = item.times.zuluEndTime;

                // Distance close to the current playing b/c it’s on a cut / segment basis and not the actual audio timestamp.
                // item.times.zeroStartTime = preRollOffset + (itemStartTime - episodeZuluStartTime);
                // item.times.zeroEndTime = preRollOffset + (itemEndTime - episodeZuluStartTime);

                // Distance close to the current playing b/c it’s on a cut / segment basis and not the actual audio timestamp.
                item.times.zeroStartTime = notLiveFudgeFactor + preRollOffset + (itemStartTime - episodeZuluStartTime);
                item.times.zeroEndTime = notLiveFudgeFactor + preRollOffset + (itemEndTime - episodeZuluStartTime);

                // If for some reason the offset start time into the playlist is less than zero, let's just make it zero
                // to force playback from the first timestamp in the playlist.
                if (item.times.zeroStartTime <= 0)
                {
                    item.times.zeroStartTime = 0;
                }
                if (item.times.zeroEndTime <= 0)
                {
                    item.times.zeroEndTime = 0;
                }

                // console.log("bmr item.times.zuluStartTime", new Date(item.times.zuluStartTime));
                // console.log("bmr item.times.zeroStartTime", item.times.zeroStartTime);
            });
        });
    }

    // TODO: BMR: 03/01/2018: We have the same method in TuneService and this was added as a quick test. Consider util class.
    public static findMediaItemByTimestamp(timestamp: number, collection: Array<IMediaItem>, clamp = true): IMediaItem
    {
        if (!_.isEmpty(collection))
        {
            let findMediaItem: any = (item: IMediaItem, index: number, collection: Array<any>): boolean =>
            {
                const startTime: number = item.times.zuluStartTime;
                const endTime: number = getEndTime();

                return (timestamp >= startTime) && (!endTime || timestamp <= endTime);

                function getEndTime(): number
                {
                    const nextItem: IMediaEpisode = collection[ index + 1 ] as IMediaEpisode;
                    return nextItem ? nextItem.times.zuluStartTime - 1 : item.times.zuluEndTime;
                }
            };

            let mediaItem = _.find(collection, findMediaItem);

            if (!mediaItem && clamp === true)
            {
                const firstMediaItem = _.first(collection);
                const lastMediaItem = _.last(collection);
                mediaItem = (timestamp <= firstMediaItem.times.zuluStartTime) ? firstMediaItem : lastMediaItem;
            }

            return mediaItem;
        }
        return null;
    }

    public static getFirstTimestampFromPlaylist(manifest)
    {
        const firstTimeStamp = manifest.match(new RegExp(NowPlayingLiveVideoMock.PATTERN_LIVE_VIDEO_PROGRAM_TIME))[ 0 ] || [];

        const PATTERN_DATE_TIME = NowPlayingLiveVideoMock.PATTERN_DATE_TIME;
        const length = PATTERN_DATE_TIME.length;
        const index = firstTimeStamp.indexOf(PATTERN_DATE_TIME) + length;
        const isoDateString = firstTimeStamp.substring(index);
        const date = new Date(isoDateString);
        console.log("bmr date", date);

        return date;
    }

    public static getUrl(url, success, error)
    {
        // Feature detection
        if (!XMLHttpRequest)
        {
            return;
        }

        // Create new request
        const request = new XMLHttpRequest();

        // Setup callbacks
        request.onreadystatechange = function ()
        {

            // If the request is complete
            if (request.readyState === 4)
            {

                // If the request failed
                if (request.status !== 200)
                {
                    if (error && typeof error === "function")
                    {
                        error(request.responseText, request);
                    }
                    return;
                }

                // If the request succeeded
                if (success && typeof success === "function")
                {
                    success(request.responseText, request);
                }
            }

        };

        // Get the HTML
        request.open("GET", url);
        request.send();
    }
}
