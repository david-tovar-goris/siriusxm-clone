/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />


import {throwError as observableThrowError,  of as observableOf } from 'rxjs';
import { AlertService }                 from './alert.service';
import { ProfileService }               from "../profile/profile.service";
import { IChannel }                     from "../index";
import { mock }                         from "ts-mockito";
import { alertsList }                   from './../test/mocks/alerts/alert.response';
import { ServiceFactory }               from "../service/service.factory";
import { MockHttpProvider }             from "../test/mocks/http-provider.mock";
import { MockRequestInterceptor,
         MockResponseInterceptor }      from "../test/mocks/interceptors";
import { MockStorageService }           from "../test/mocks/storage.service";
import { mockServiceConfig }            from "../test/mocks/service.config";
import { AlertType }                    from "./alert.constants";

interface ThisContext
{
    service : AlertService;
    alertDelegate : any;
    mockProfileService : any;
    mockHttpProvider : any;
    failureMessage : string;
    settingsService: any;
}

describe("AlertService Test Suite", () =>
{
    beforeEach(function(this:ThisContext)
    {
        this.failureMessage = "API Failure";

        this.alertDelegate = {
            getAlerts   : () =>
            {
                return observableOf(alertsList);
            },
            createAlert   : () =>
            {
                return observableOf(alertsList);
            },
            removeAlert   : () =>
            {
                return observableOf(alertsList);
            },
            removeAllAlerts   : () =>
            {
                return observableOf(alertsList);
            },
            muteAlert   : () =>
            {
                return observableOf(alertsList);
            },
            normalizeNotificationHeader   : () =>
            {
                return observableOf(alertsList);
            },
            normalizeAlerts   : () =>
            {
                return observableOf(alertsList);
            }
        };

        this.mockProfileService = mock(ProfileService);
        this.mockProfileService.profileData =  observableOf({
            alerts        : [],
            favorites     : [],
            recentlyPlayed: [],
            pausePoints   : []
        });

        this.mockHttpProvider = new MockHttpProvider(
            MockRequestInterceptor,
            MockResponseInterceptor,
            MockStorageService,
            mockServiceConfig
        );

        this.settingsService = {
            runSubscribeFlow: jasmine.createSpy('runSubscribeFlow')
        };

        this.service = new AlertService(this.mockHttpProvider, this.alertDelegate, this.mockProfileService, this.settingsService);
        this.service.alertsList = alertsList;
    });

    describe("Infrastructure", function(this:ThisContext)
    {
        it("Should be able to create the service and all its dependencies",function(this:ThisContext)
        {
            expect(ServiceFactory.getInstance(AlertService)).toBeDefined();
        });

        it("Should have a test subject.",function(this:ThisContext)
        {
            expect(this.service).toBeDefined();
        });

        it("Should expose the expected public properties.",function(this:ThisContext)
        {
            expect(this.service.alerts).toBeDefined();
        });
    });

    describe("createAlert()",function(this:ThisContext)
    {
        const channel = {
            channelId     : "01",
            type          : "live",
            channelNumber : 1,
            channelGuid   : "assetGuid"
        } as IChannel;

        it("call create alert using alertDelegate.createAlert", function(this:ThisContext)
        {
            spyOn(this.alertDelegate, "createAlert").and.returnValue(observableOf([]));
            this.service.createAlert(channel.channelId, channel.channelGuid, AlertType.SHOW).subscribe(data =>
            {
                expect(data).toEqual(true);
            });
            expect(this.alertDelegate.createAlert).toHaveBeenCalled();
        });

        it("Returns failure response",function(this:ThisContext)
        {
            spyOn(this.alertDelegate, "createAlert").and
                                                   .callFake(function(this:ThisContext)
                                                   {
                                                        return observableThrowError(this.failureMessage);
                                                   });

            this.service
                .createAlert(channel.channelId, channel.channelGuid, AlertType.SHOW)
                .subscribe(null, function(this:ThisContext,response)
            {
                expect(response).toEqual(this.failureMessage);
            });
        });

    });

    describe("removeAlert()",function(this:ThisContext)
    {
        const channel =
                      {
                          channelId    : "01",
                          type         : "live",
                          channelNumber: 1,
                          channelGuid    : "assetGuid"
                      } as IChannel;

        it("call remove alert using alertDelegate.removeAlert",function(this:ThisContext)
        {
            spyOn(this.alertDelegate, "removeAlert").and.returnValue(observableOf([]));
            this.service.removeAlert(channel.channelId, channel.channelGuid, AlertType.SHOW).subscribe(data =>
            {
                expect(data).toEqual(true);
            });
            expect(this.alertDelegate.removeAlert).toHaveBeenCalled();
        });

        it("call remove alert using alertDelegate.removeAlert", function(this:ThisContext)
        {
            spyOn(this.alertDelegate, "removeAlert").and.returnValue(observableOf([]));
            this.service.removeAlert(channel.channelId, channel.channelGuid, AlertType.SHOW).subscribe(data =>
            {
                expect(data).toEqual(true);
            });
            expect(this.alertDelegate.removeAlert).toHaveBeenCalled();
        });

        it("call removeAlert without channelID",function(this:ThisContext)
        {
            spyOn(this.alertDelegate, "removeAlert").and.returnValue(observableOf([]));
            this.service.removeAlert(null, channel.channelGuid, AlertType.SHOW).subscribe(data =>
            {
                expect(data).toEqual(false);
            });
        });

        it("Returns failure response", function(this:ThisContext)
        {
            spyOn(this.alertDelegate, "removeAlert").and
                                                   .callFake(function(this:ThisContext)
                                                   {
                                                        return observableThrowError(this.failureMessage);
                                                   });

            this.service.removeAlert(channel.channelId, channel.channelGuid, AlertType.SHOW).subscribe(null, (response) =>
            {
                expect(response).toEqual(this.failureMessage);
            });
        });

    });

    describe("removeAllAlerts()",function(this:ThisContext)
    {
        it("call remove all alerts using alertDelegate.removeAllAlerts", function(this:ThisContext)
        {
            spyOn(this.alertDelegate, "removeAllAlerts").and.returnValue(observableOf(alertsList));

            this.service.removeAllAlerts()
                .subscribe((result) => { expect(result).toBeTruthy(); });

            expect(this.alertDelegate.removeAllAlerts).toHaveBeenCalled();
        });

        it("Returns failure response", function(this:ThisContext)
        {
            spyOn(this.alertDelegate, "removeAllAlerts").and
                                                   .callFake(function(this:ThisContext)
                                                   {
                                                        return observableThrowError(this.failureMessage);
                                                   });

            this.service.removeAllAlerts().subscribe(null, function(this:ThisContext,response)
            {
                expect(response).toEqual(this.failureMessage);
            });
        });

    });

    describe("muteAlert()",function(this:ThisContext)
    {
        const alertId = "01";

        it("call mute alert using alertDelegate.muteAlert",function(this:ThisContext)
        {
            spyOn(this.alertDelegate, "muteAlert").and.returnValue(observableOf([]));
            this.service.muteAlert(alertId).subscribe(data =>
            {
                expect(data).toEqual(true);
            });
            expect(this.alertDelegate.muteAlert).toHaveBeenCalled();
        });

        it("Returns failure response",function(this:ThisContext)
        {
            spyOn(this.alertDelegate, "muteAlert").and
                                                   .callFake(function(this:ThisContext)
                                                   {
                                                        return observableThrowError(this.failureMessage);
                                                   });

            this.service.muteAlert(alertId).subscribe(null, function(this:ThisContext,response)
            {
                expect(response).toEqual(this.failureMessage);
            });
        });

    });

    describe('observeHeader()',function(this:ThisContext)
    {
        it("Should trigger a subscription",function(this:ThisContext)
        {
            spyOn(this.service.httpHeader, 'subscribe');
            this.service.observeHeader("fake-header");
            expect(this.service.httpHeader.subscribe).toHaveBeenCalled();
        });

    });

    describe("getShowAlert()",function(this:ThisContext)
    {
        it("call get show alert using service.getShowAlert",function(this:ThisContext)
        {
            const assetGuid = "showGuid";
            spyOn(this.service, 'getShowAlert').and.callThrough();
            this.service.getShowAlert(assetGuid, AlertType.SHOW);
            expect(this.service.getShowAlert).toHaveBeenCalled();
        });
    });
});
