/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { of as observableOf }                 from "rxjs";
import { IHttpRequestConfig }         from "../http";
import { ServiceEndpointConstants }   from "../service/consts";
import { AlertDelegate }              from "./alert.delegate";
import { alertsList }                 from './../test/mocks/alerts/alert.response';
import {
    mockAlerts,
    mockgupAlertsUpdateRequests,
    mockgupAlertsCreateRequests,
    mockNotification,
    mockNotificationList
}                                     from './../test/mocks/alerts/alert.mock';
import { mockProfileResponseFromApi } from "../test/mocks/profile/profile.mock";
import { AlertType }                  from "./alert.constants";
import { ServiceFactory }             from "../service/service.factory";

interface ThisContext
{
    alertDelegate : AlertDelegate;
    mockHttp : any;
}

describe("Alert Delegate Test Suite", () =>
{
    beforeEach(function(this:ThisContext)
    {
        this.mockHttp = {
            get                    : function (url : string) { return observableOf(null); },
            postModuleAreaRequest  : function (url : string, data : any) { return observableOf(null); },
            postModuleAreaRequests : function (url : string, data : any) { return observableOf(null); }
        };

        this.alertDelegate = new AlertDelegate(this.mockHttp);
    });

    describe("Infrastructure >>", function(this:ThisContext)
    {
        it("Should be able to create the service and all its dependencies",function(this:ThisContext)
        {
            expect(ServiceFactory.getInstance(AlertDelegate)).toBeDefined();
        });

        it("Constructor", function(this:ThisContext)
        {
            expect(this.alertDelegate).toBeDefined();
            expect(this.alertDelegate instanceof AlertDelegate).toEqual(true);
        });
    });

    describe("getAlerts()", function(this:ThisContext)
    {
        it("should be exposed", function(this:ThisContext)
        {
            expect(this.alertDelegate.getAlerts).toBeDefined();
            expect(typeof this.alertDelegate.getAlerts).toEqual('function');
        });

        it("Should trigger proper API request to get the Alerts data", function(this:ThisContext)
        {
            spyOn(this.mockHttp, "get").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      expect(url)
                                          .toBe(ServiceEndpointConstants.endpoints.PROFILE.V2_GET_PROFILE_GUP_COLLATION);
                                      expect(data).toBe(null);
                                      expect(config.params[ "gup-type" ]).toBe("alerts");
                                      expect(config.params[ "result-template" ]).toBe("");
                                      expect(config.params[ "format" ]).toBe("json");
                                      return observableOf(alertsList);
                                  });

            this.alertDelegate.getAlerts().subscribe(response => {} );
            expect(this.mockHttp.get).toHaveBeenCalled();
        });

    });

    describe("createAlert()", function(this:ThisContext)
    {
        it("should be exposed", function(this:ThisContext)
        {
            expect(this.alertDelegate.createAlert).toBeDefined();
            expect(typeof this.alertDelegate.createAlert).toEqual('function');
        });

        it("Should trigger proper API request to Create Alert", function(this:ThisContext)
        {
            spyOn(this.mockHttp, "postModuleAreaRequest").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      expect(url).toBe(ServiceEndpointConstants.endpoints.PROFILE.V4_CREATE_ALERT);
                                      expect(data.moduleRequest).toBeDefined();
                                      expect(data.moduleRequest.profileUpdateRequest).toBeDefined();
                                      expect(data.moduleRequest.profileUpdateRequest.gupAlertsCreateRequests).toEqual(mockgupAlertsCreateRequests);
                                      expect(config).toBe(null);
                                      return observableOf(mockProfileResponseFromApi);
                                  });

            this.alertDelegate.createAlert('channelId', 'assetGuid', 1).subscribe(response => {} );
            expect(this.mockHttp.postModuleAreaRequest).toHaveBeenCalled();
        });
         
    });

    describe("removeAlert()", function(this:ThisContext)
    {
        
        it("should be exposed", function(this:ThisContext)
        {
            expect(this.alertDelegate.removeAlert).toBeDefined();
            expect(typeof this.alertDelegate.removeAlert).toEqual('function');
        });

        it("Should trigger proper API request to Remove Alert", function(this:ThisContext)
        {
            spyOn(this.mockHttp, "postModuleAreaRequest").and
                                  .callFake((url, data: any, config: IHttpRequestConfig) =>
                                  {
                                      expect(url).toBe(ServiceEndpointConstants.endpoints.PROFILE.V4_REMOVE_ALERT);
                                      expect(data.moduleRequest).toBeDefined();
                                      expect(data.moduleRequest.profileUpdateRequest).toBeDefined();
                                      expect(data.moduleRequest.profileUpdateRequest.gupAlertsUpdateRequests).toEqual(mockgupAlertsUpdateRequests);
                                      expect(config).toBe(null);
                                      return observableOf(mockProfileResponseFromApi);
                                  });

            this.alertDelegate.removeAlert('alertId', 'assetGuid', AlertType.SHOW).subscribe(response => {} );
            expect(this.mockHttp.postModuleAreaRequest).toHaveBeenCalled();
        });
         
    });

    describe("muteAlert()", function(this:ThisContext)
    {
        it("should be exposed", function(this:ThisContext)
        {
            expect(this.alertDelegate.muteAlert).toBeDefined();
            expect(typeof this.alertDelegate.muteAlert).toEqual('function');
        });

        it("Should trigger proper API request to Mute Alert",function(this:ThisContext)
        {
            spyOn(this.mockHttp, "get").and
                                  .callFake((url, config: IHttpRequestConfig, data: any) =>
                                  {
                                      expect(url).toBe(ServiceEndpointConstants.endpoints.PROFILE.V4_MUTE_ALERTS);
                                      expect(data).toBe(null);
                                      expect(config.params[ "ID" ]).toBe("alertId");
                                      expect(config.params[ "result-template" ]).toBe("html5");
                                      return observableOf(alertsList);
                                  });

            this.alertDelegate.muteAlert('alertId').subscribe(response => {} );
            expect(this.mockHttp.get).toHaveBeenCalled();
        });
         
    });

    describe("removeAllAlerts()",function(this:ThisContext)
    {
        it("should be exposed", function(this:ThisContext)
        {
            expect(this.alertDelegate.removeAllAlerts).toBeDefined();
            expect(typeof this.alertDelegate.removeAllAlerts).toEqual('function');
        });

        it("remove all alerts",function(this:ThisContext)
        {
            spyOn(this.mockHttp, "postModuleAreaRequests").and
            .callFake((url, data: any, config: IHttpRequestConfig) =>
            {
                expect(url).toBe(ServiceEndpointConstants.endpoints.PROFILE.V4_REMOVE_ALERT);
                expect(Array.isArray(data)).toEqual(true);
                expect(data[ 0 ].moduleArea).toEqual('Profile');
                expect(data[ 0 ].moduleType).toEqual('GUPCollation');
                expect(config).toEqual(null);                
                return observableOf(mockProfileResponseFromApi);
            });
            
            this.alertDelegate.removeAllAlerts(mockAlerts).subscribe(response =>
            {
            });
            expect(this.mockHttp.postModuleAreaRequests).toHaveBeenCalled();
        });
         
    });

    describe("normalizeNotificationHeader()", () =>
    {
        it("should be exposed", function(this:ThisContext)
        {
            expect(this.alertDelegate.normalizeNotificationHeader).toBeDefined();
            expect(typeof this.alertDelegate.normalizeNotificationHeader).toEqual('function');
        });

        it("should return notificationAlerts", function(this:ThisContext)
        {
            let normalizeNotificationHeader = this.alertDelegate.normalizeNotificationHeader(mockNotification);
            expect(normalizeNotificationHeader).toEqual(mockNotificationList);
        });

        it("should return Empty", function(this:ThisContext)
        {
            let normalizeNotificationHeader = this.alertDelegate.normalizeNotificationHeader('');
            expect(normalizeNotificationHeader).toEqual([]);
        });


    });

});
