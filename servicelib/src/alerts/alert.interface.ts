import { IImage } from "../service/types/image.interface";

export interface IAlert
{
    active: boolean;
    alertId: string;
    alertType: string;
    assetGuid: string;
    consumedAlerts: Array<string>;
    deviceId: string;
    gupId: string;
    legacyId1: string;
    legacyId2: string;
    showImages: Array<IImage>;
    showName: string;
}

export interface INotificationAlert
{
    alertType: string | number;
    assetGuid: string;
    episodeGuid: string;
    zuluStartTime: number;
    episodeStartDateTime: string;
    episodeEndDateTime: string;
    channelNumber: string;
    title: string;
    channelName: string;
}
