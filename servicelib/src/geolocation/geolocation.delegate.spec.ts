
import {throwError as observableThrowError,  of as observableOf } from 'rxjs';
import { GeolocationDelegate } from "./geolocation.delegate";
import { ApiCodes } from "..";

describe("GeolocationDelegate", function()
{
    const httpProviderMock = { get: jasmine.createSpy("get") };

    beforeEach(function()
    {
        this.geolocationDelegate = new GeolocationDelegate(httpProviderMock as any);
    });

    describe("getGeoRestrictionStatusForLocation()", function()
    {
        describe("when an error code of GEO_LOCATION_UNAVAILABLE is received", function()
        {
            beforeEach(function()
            {
                httpProviderMock.get.and.returnValue(observableThrowError({ code: ApiCodes.GEO_LOCATION_UNAVAILABLE }));
            });

            it("returns an Observable of false so the content is not played and the code to determine the error to display", function()
            {
                let result;

                this.geolocationDelegate.getGeoRestrictionStatusForLocation("4", "2")
                    .subscribe(val => result = val)
                    .unsubscribe();

                    expect(result).toEqual([ false, ApiCodes.GEO_LOCATION_UNAVAILABLE ]);
            });
        });

        describe("when an error code of GEO_LOCATION_ERROR is received", function()
        {
            beforeEach(function()
            {
                httpProviderMock.get.and.returnValue(observableThrowError({ code: ApiCodes.GEO_LOCATION_ERROR }));
            });

            it("returns an Observable of false so the content is not played and the code to determine the error to display", function()
            {
                let result;

                this.geolocationDelegate.getGeoRestrictionStatusForLocation("4", "2")
                    .subscribe(val => result = val)
                    .unsubscribe();

                    expect(result).toEqual([ false, ApiCodes.GEO_LOCATION_ERROR ]);
            });
        });

        describe("when an http error code is received", function()
        {
            beforeEach(function()
            {
                httpProviderMock.get.and.returnValue(observableThrowError({ code: 402 }));
            });

            it("it treats the content as unrestricted returning [ true 100 ]", function()
            {
                let result;

                this.geolocationDelegate.getGeoRestrictionStatusForLocation("4", "2")
                    .subscribe(val => result = val)
                    .unsubscribe();

                    expect(result).toEqual([ true, 100 ]);
            });
        });

        describe("when the content is allowed for the users location", function()
        {
            beforeEach(function()
            {
                httpProviderMock.get.and.returnValue(observableOf("yay!"));
            });

            it("it return [ true 100 ] to allow content to be tuned to", function()
            {
                let result;

                this.geolocationDelegate.getGeoRestrictionStatusForLocation("4", "2")
                    .subscribe(val => result = val)
                    .unsubscribe();

                    expect(result).toEqual([ true, 100 ]);
            });
        });
    });
});
