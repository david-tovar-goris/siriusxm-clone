export interface GeolocationParams
{
  params: {
    channelID: string;
    episodeCAID?: string;
  };
}
