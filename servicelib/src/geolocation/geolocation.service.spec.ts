import { GeolocationService, ClientCodes } from "../";
import { Observable, of as observableOf } from "rxjs";

describe("GeolocationService", function()
{
    beforeEach(function()
    {
        this.geolocationDelegateMock = { getGeoRestrictionStatusForLocation: jasmine.createSpy('geoRest') };
        this.geolocationService = new GeolocationService(this.geolocationDelegateMock as any);
    });

    describe("checkContent()", function()
    {
        describe("when the channel is not geo restricted", function()
        {
            it("does not call the api and returns Observable.of(true) to allow tuning", function()
            {
                this.geolocationService.checkContent("channelId", "", false)
                    .subscribe(response =>
                    {
                        expect(response).toEqual(ClientCodes.SUCCESS);
                        expect(this.geolocationDelegateMock.getGeoRestrictionStatusForLocation).not.toHaveBeenCalled();
                    });
            });
        });

        describe("when a channel is geo restricted", function()
        {
            it("calls the api and returns the result", function()
            {
                const geoLocationDelegateResp = observableOf([ true, 100 ]);
                let resp;

                this.geolocationDelegateMock.getGeoRestrictionStatusForLocation.and.returnValue(geoLocationDelegateResp);

                this.geolocationService.checkContent("channelId", "", true)
                .subscribe(val => resp = val);

                expect(resp).toEqual(ClientCodes.SUCCESS);
                expect(this.geolocationDelegateMock.getGeoRestrictionStatusForLocation).toHaveBeenCalled();
            });
        });
    });
});
