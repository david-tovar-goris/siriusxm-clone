/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { BehaviorSubject } from "rxjs";

import { mockCategoryChannels } from "../test/index";
import { mock } from "ts-mockito";

import {
    EventBus,
    ApiDelegate,
    AppMonitorService, IMediaEpisode, TunePayload, IChannel, ContentTypes
} from "../index";
import { StorageService } from "../storage/storage.service";
import { CurrentlyPlayingService } from "./currently.playing.service";
import { TuneService } from "../tune/tune.service";
import { ITime, MediaTimeLine , IMediaShow } from "../tune/tune.interface";
import { Playhead } from "../mediaplayer/playhead.interface";

describe("Currently Playing Service Test Suite", function()
{
    beforeEach(function()
    {
        this.tuneServiceMock                                  = mock(TuneService);
        this.mockStorageService                               = null;
        this.channelLineupServiceMock                         = null;
        this.apiDelegate                                      = null;
        this.appMonitorServiceMock;
        this.mockMediaTimeLine                                = null;
        this.videoAssetGuid                                   = "VideoAssetGuid";
        this.videoZeroStartTime                               = 10;
        this.cut1                                             = null;
        this.cut2                                             = null;

        this.apiDelegate                       = new ApiDelegate(new EventBus);
        this.appMonitorServiceMock             = {
            ...mock(AppMonitorService),
            handleMessage: jasmine.createSpy('handleMessage')
        };

        this.channelLineupServiceMock = {
            findChannelById: jasmine.createSpy("findChannelById").and.callFake((channelId: string) =>
            {
                if (channelId)
                {
                    return mockCategoryChannels[0];
                }
                else
                {
                    return null;
                }
            })
        };
        this.cut1                     = {
            assetGUID      : "11-a81ef7f1-4ea2-8517-518c-f0a574f93f91",
            consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f91,channelId=8254,sir",
            duration       : 50,
            title          : "cutTitle1",
            legacyId       : { pid: "testPid" },
            times          : {
                zuluStartTime: 100,
                isoStartTime : "2017-09-22T15:42:35.000+00:00",
                zuluEndTime  : 150,
                isoEndTime   : "2017-09-22T15:49:01.000Z",
                zeroStartTime: 0,
                zeroEndTime  : 0
            },
            cutContentType : "song",
            contentType    : "song",
            liveGame       : true
        };

        this.cut2 = {
            assetGUID      : "11-a81ef7f1-4ea2-8517-518c-f0a574f93f91",
            consumptionInfo: "assetGUID=a81ef7f1-4ea2-8517-518c-f0a574f93f91,channelId=8254,sir",
            duration       : 50,
            title          : "cutTitle2",
            legacyId       : { pid: "testPid" },
            times          : {
                zuluStartTime: 150,
                isoStartTime : "2017-09-22T15:42:35.000+00:00",
                zuluEndTime  : 200,
                isoEndTime   : "2017-09-22T15:49:01.000Z",
                zeroStartTime: 10,
                zeroEndTime  : 0
            },
            cutContentType : "song",
            contentType    : "song",
            liveGame       : false
        };

        this.mockMediaTimeLine = {
            mediaType: "live",
            mediaId  : "123",
            channelId: "channelId",
            cuts     : [
                this.cut1,
                this.cut2
            ],
            videos   : [
                {
                    assetGUID    : this.videoAssetGuid,
                    layer        : "episode",
                    zeroStartTime: this.videoZeroStartTime,
                    times        :
                        {
                            zuluStartTime: 100,
                            zuluEndTime  : 150,
                            zeroStartTime: this.videoZeroStartTime
                        } as ITime
                }
            ]
        };

        this.mockStorageService            = mock(StorageService);
        this.tuneServiceMock.mediaTimeLine = new BehaviorSubject(this.mockMediaTimeLine as MediaTimeLine);
        this.configService = {
            liveVideoEnabled: () => true,
            getRelativeUrlSettings: () => [],
            getRelativeURL: (urlKey:string) => urlKey,
            getSeededRadioBackgroundUrl : () => ""
        };

        this.currentlyPlayingService = new CurrentlyPlayingService( this.channelLineupServiceMock,
                                                                    this.mockStorageService,
                                                                    this.tuneServiceMock,
                                                                    this.appMonitorServiceMock,
                                                                    this.configService);
    });

    afterEach(function()
    {
        jasmine.clock().uninstall();
    });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.currentlyPlayingService).toBeDefined();
            expect(this.currentlyPlayingService instanceof CurrentlyPlayingService).toEqual(true);
        });

        it("should have public properties defined..", function()
        {
            expect(this.currentlyPlayingService.currentlyPlayingData).toBeDefined();
        });

        it("should set currently Playing data.", function()
        {
            this.currentlyPlayingService.currentlyPlayingData.subscribe(data =>
            {
                expect(data.mediaType).toEqual(this.mockMediaTimeLine.mediaType);
                expect(data.mediaId).toEqual(this.mockMediaTimeLine.mediaId);
                expect(data.channelId).toEqual(this.mockMediaTimeLine.channelId);
            });
        });
    });

    describe("setCurrentPlayingData()", function()
    {
        it("Sets the currently playing data included cuts and video", function(done)
        {
            const playhead = new Playhead();
            playhead.currentTime.zuluMilliseconds = 100;
            this.currentlyPlayingService.setCurrentPlayingData(playhead);
            this.currentlyPlayingService.currentlyPlayingData.subscribe(data =>
            {
                expect(data).toEqual(
                    {
                        cut              : this.mockMediaTimeLine.cuts[0],
                        firstCut         : this.mockMediaTimeLine.cuts[0],
                        episode          : {},
                        cutEntryTimestamp: 100,
                        mediaType        : this.mockMediaTimeLine.mediaType,
                        mediaId          : this.mockMediaTimeLine.mediaId,
                        channelId        : this.mockMediaTimeLine.channelId,
                        show             : {} as IMediaShow,
                        duration         : 50000,
                        dmcaInfo         : {},
                        video            : this.mockMediaTimeLine.videos[0],
                        inactivityTimeOut: 0,
                        youJustHeard     : [],
                        channel          : undefined,
                        stationId        : undefined
                    }
                );
            });
            done();
        });
    });

    describe("getLiveVideoAssetGuid()", function()
    {
        it("return unkown Asset guid if video not found in currentlyPlaying Data", function(done)
        {
            expect(this.currentlyPlayingService.getLiveVideoAssetGuid()).toEqual("Unknown Asset GUID");
            done();
        });

        it("return Asset guid if video found in currentlyPlaying Data", function(done)
        {
            const playhead = new Playhead();
            playhead.currentTime.zuluMilliseconds = 100;
            this.currentlyPlayingService.setCurrentPlayingData(playhead);
            expect(this.currentlyPlayingService.getLiveVideoAssetGuid()).toEqual(this.videoAssetGuid);
            done();
        });
    });

    describe("getDuration()", function()
    {
        it("return 0 if duration not found in currentlyPlaying Data", function(done)
        {
            expect(this.currentlyPlayingService.getDuration()).toEqual(0);
            done();
        });
    });

    describe("getCurrentEpisodeZuluStartTime()", function()
    {
        it("return 0 if zulu start time not found in currentlyPlaying Data", function(done)
        {
            expect(this.currentlyPlayingService.getCurrentEpisodeZuluStartTime()).toEqual(0);
            done();
        });
    });

    describe("reset()", function()
    {
        it("set currentlyPlayingData as null on reset", function()
        {
            this.currentlyPlayingService.reset();
            this.currentlyPlayingService.currentlyPlayingData.subscribe(response =>
            {
                expect(response).toEqual(null);
            });
        });
    });

    describe("isTunedTo()", function()
    {
        describe("when live", function()
        {
            it('returns false if media id is different than id in payload', function()
            {
                const payload: TunePayload = {
                   contentType: ContentTypes.LIVE_AUDIO,
                   channelId: '124'
                };

                expect(this.currentlyPlayingService.isTunedTo(payload)).toBe(false);
            });

            it('returns false if media id is the same as id in payload', function()
            {
                const payload: TunePayload = {
                    contentType: ContentTypes.LIVE_AUDIO,
                    channelId: '123'
                };

                expect(this.currentlyPlayingService.isTunedTo(payload)).toBe(true);
            });
        });

        describe("when payload has an on demand content type", function()
        {
            it('returns false if media id is different than id in payload - aod', function()
            {
                const payload: TunePayload = {
                    contentType: ContentTypes.AOD,
                    channelId: '123',
                    episodeIdentifier: '124'
                };

                expect(this.currentlyPlayingService.isTunedTo(payload)).toBe(false);
            });

            it('returns false if media id is different than id in payload - vod', function()
            {
                const payload: TunePayload = {
                    contentType: ContentTypes.VOD,
                    channelId: '123',
                    episodeIdentifier: '124'
                };

                expect(this.currentlyPlayingService.isTunedTo(payload)).toBe(false);
            });

            it('returns true if media id is the same as id in payload', function()
            {
                const payload: TunePayload = {
                    channel: { channelId: 'channelId' } as IChannel,
                    contentType: ContentTypes.AOD,
                    channelId: '123',
                    episodeIdentifier: '123'
                };

                expect(this.currentlyPlayingService.isTunedTo(payload)).toBe(true);
            });
        });
    });
});
