import { getYouJustHeard, hasSkipped } from "./you-just-heard-util";
import { IMediaCut } from "../tune/tune.interface";

describe("YouJustHeardUtil", function()
{
    it("Infrastructure", function()
    {
        expect(getYouJustHeard).toBeDefined();
    });

    describe("getYouJustHeard", function()
    {
        it("Add song to You just heard list", function()
        {
            const oldYouJustHeard = [];
            const cut: IMediaCut =
                {
                    contentType: "song",
                    assetGUID: "AssetGuid1",
                    lastPlaybackTimestamp: 1000,
                    times:
                        {
                            zuluEndTime: 100
                        }
                } as IMediaCut;

            const result = getYouJustHeard(oldYouJustHeard, cut);

            expect(result.length).toEqual(1);
        });

        it("Should not add duplicate song to You just heard list", function()
        {
            let oldYouJustHeard = [];
            const cut: IMediaCut =
                {
                    contentType: "song",
                    assetGUID: "AssetGuid1",
                    lastPlaybackTimestamp: 1000,
                    times:
                        {
                            zuluEndTime: 100
                        }
                } as IMediaCut;

            oldYouJustHeard = getYouJustHeard(oldYouJustHeard, cut);
            const result = getYouJustHeard(oldYouJustHeard, cut);
            expect(result.length).toEqual(1);
        });

        it("Should add new song to existing You just heard list and unshifts the item", function()
        {
            let oldYouJustHeard = [];
            const cut: IMediaCut =
                {
                    contentType: "song",
                    assetGUID: "AssetGuid1",
                    title: "song1 Title",
                    lastPlaybackTimestamp: 1000,
                    times:
                        {
                            zuluEndTime: 100
                        }
                } as IMediaCut;

            oldYouJustHeard = getYouJustHeard(oldYouJustHeard, cut);

            expect(oldYouJustHeard.length).toEqual(1);
            expect(oldYouJustHeard).toEqual([{
                albumImageUrl: "",
                title: cut.title,
                artistName: "",
                cut: cut
            }
            ]);

            const cut2: IMediaCut =
                {
                    contentType: "song",
                    assetGUID: "AssetGuid2",
                    title: "song2 Title",
                    lastPlaybackTimestamp: 1000,
                    times:
                        {
                            zuluEndTime: 100
                        }
                } as IMediaCut;
            const result = getYouJustHeard(oldYouJustHeard, cut2);

            expect(result.length).toEqual(2);
            expect(result).toEqual([{
                albumImageUrl: "",
                title: cut2.title,
                artistName: "",
                cut: cut2
            },
                {
                    albumImageUrl: "",
                    title: cut.title,
                    artistName: "",
                    cut: cut
                }
            ]);
        });

        it("Should return You just heard list if cut is null", function()
        {
            let oldYouJustHeard = [];
            const cut: IMediaCut =
                {
                    contentType: "song",
                    assetGUID: "AssetGuid1",
                    title: "title",
                    lastPlaybackTimestamp: 1000,
                    times:
                        {
                            zuluEndTime: 100
                        }
                } as IMediaCut;

            oldYouJustHeard = getYouJustHeard(oldYouJustHeard, cut);

            const result = getYouJustHeard(oldYouJustHeard, null);
            expect(result.length).toEqual(1);
        });

        it("Should not add to You Just Heard if content type is not song", function()
        {
            let oldYouJustHeard = [];
            const cut: IMediaCut =
                {
                    contentType: "link",
                    assetGUID: "AssetGuid1",
                    lastPlaybackTimestamp: 1000,
                    times:
                        {
                            zuluEndTime: 100
                        }
                } as IMediaCut;

            const result = getYouJustHeard(oldYouJustHeard, cut);

            expect(result.length).toEqual(0);
        });

        it("Should not add to You Just Heard if skips the song", function()
        {
            let oldYouJustHeard = [];
            const cut: IMediaCut =
                {
                    contentType: "song",
                    assetGUID: "AssetGuid1",
                    lastPlaybackTimestamp: 900,
                    times:
                        {
                            zuluEndTime: 3000
                        }
                } as IMediaCut;

            const result = getYouJustHeard(oldYouJustHeard, cut);

            expect(result.length).toEqual(0);
        });
    });

    describe("hasSkipped", () =>
    {
        describe("when the difference between zulu end time and playback timestamp is greater than 2s", function()
        {
            it("Should return true", () =>
            {
                const cut: IMediaCut =
                    {
                        contentType: "song",
                        assetGUID: "AssetGuid1",
                        lastPlaybackTimestamp: 500,
                        times:
                            {
                                zuluEndTime: 3000
                            }
                    } as IMediaCut;

                expect(hasSkipped(cut)).toEqual(true);
            });
        });

        describe("when the difference between zulu end time and playback timestamp is less than 2s", function()
        {
            it("Should return false", () =>
            {
                const cut: IMediaCut =
                    {
                        contentType: "song",
                        assetGUID: "AssetGuid1",
                        lastPlaybackTimestamp: 2000,
                        times:
                            {
                                zuluEndTime: 3000
                            }
                    } as IMediaCut;

                expect(hasSkipped(cut)).toEqual(false);
            });
        });
    });
});
