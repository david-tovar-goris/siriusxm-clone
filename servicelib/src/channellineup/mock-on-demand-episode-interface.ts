export interface mockEpisodeInterface
{
    longTitle: string;
    originalAirDate: string;
    percentConsumed: number;
    duration: string;
    longDescription: string;
    contextualBanner?: string;
    contextualBannerColor?: string;
}
