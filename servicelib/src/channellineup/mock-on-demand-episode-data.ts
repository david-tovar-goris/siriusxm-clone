export const mockEpisodes = [

        {
            longTitle: "Hurricane Coverage + RIP Jay Thomas",
            originalAirDate: "2017-09-11T11:00:00.000+0000",
            duration: "47m",
            percentConsumed: 20,
            longDescription: "Howard remembers friend of the show Jay Thomas, discusses the news coverage of Hurricane Irma, and much more!",
            contextualBanner: "",
            contextualBannerColor: ""
        },
        {
            longTitle: "Hank Azaria + RIP Chris Cornell",
            originalAirDate: "2017-09-06T11:00:00.000+0000",
            percentConsumed: 92,
            duration: "3hr 12m",
            longDescription: "Howard remembers late musician Chris Cornell. Plus, Hank Azaria stops by, Roger Ailes passed away and much more!",
            contextualBanner: "New",
            contextualBannerColor: "#2ba7ff"
        },
        {
            longTitle : "Jeff the Vomit Guy + More Jimmy" ,
            originalAirDate: "2017-08-23T11:00:00.000+0000",
            duration: "1hr 27m",
            percentConsumed: 0,
            longDescription: "The day is finally here! Jeff the Vomit Guy's dream comes true as he gets vomited on, live in studio.",
            contextualBanner: "",
            contextualBannerColor: ""
        },
        {
            longTitle: "Ronnie Update + Richard's Beer",
            originalAirDate: "2017-08-21T11:00:00.000+0000",
            duration: "2hr 3m",
            percentConsumed: 35,
            longDescription: "Ronnie updates Howard on his relationship with Shuli since the Sandler movie shoot.",
            contextualBanner: "",
            contextualBannerColor: ""
        }

];

export const mockShow = {
        subCategory: "Howard Stern",
        longTitle: "Howard Stern Show",
        imageURL: "http://pri.art.uat.streaming.siriusxm.com/images/chan/85/1e5b18-fbc8-7fb4-e8c3-1ad067335887.png",
        longDescription:"Howard came to Satellite Radio and is bigger than ever; joined by his longtime team including Robin, " +
                        "Fred and Gary, hear the unusual, unexpected and uncensored, including the best celebrity interviews in the business."
};
