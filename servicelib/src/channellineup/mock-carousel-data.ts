import { ISuperCategory } from "./categories.interfaces";
import { ISuperCategoryCarousel } from "../carousel/carousel.interface";

const carouselsByCategory = {
    foryou: <ISuperCategoryCarousel[]> [{type: "hero", id: "0001"},
        {type: "content", id: "0010", header: "Continue Listening", additionalText: "more"},
        {type: "content", id: "0100", header: "Recommended For You", additionalText: "more"}],
    news: <ISuperCategoryCarousel[]> [{type: "hero", id: "0002"},
        {type: "content", id: "0020", header: "Popular Channels", additionalText: "more"},
        {type: "content", id: "0200", header: "Popular On Demand", additionalText: "more"}],
    sports: <ISuperCategoryCarousel[]> [{type: "hero", id: "0003"},
        {type: "content", id: "0030", header: "Popular Channels", additionalText: "more"},
        {type: "content", id: "0300", header: "Popular On Demand", additionalText: "more"}],
    entertainment: <ISuperCategoryCarousel[]> [{type: "hero", id: "0004"},
        {type: "content", id: "0040", header: "Popular Channels", additionalText: "more"},
        {type: "content", id: "0400", header: "Popular On" + " Demand", additionalText: "more"}],
    music: <ISuperCategoryCarousel[]> [{type: "hero", id: "0005"},
        {type: "content", id: "0050", header: "Popular Channels", additionalText: "more"},
        {type: "content", id: "0500", header: "Popular On Demand", additionalText: "more"}]
};

export const addMockCarouselData: (superCategory: ISuperCategory) => ISuperCategory =
                 superCategory => superCategory.carousels = carouselsByCategory[superCategory.key.toLowerCase()];
