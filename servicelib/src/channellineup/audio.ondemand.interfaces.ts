import { IOnDemandEpisode } from "./ondemand.interfaces";
import { IDmcaInfo, IDrmInfo } from "../dmca/dmca.interface";

/**
 * @MODULE:     service-lib
 * @CREATED:    07/19/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *   Defines the interfaces for a channel lineup
 */

export interface IAodEpisode extends IOnDemandEpisode
{
    type: string;
    baseTime: string;
    expiringSoon: boolean;
    featured: boolean;
    featuredPriority: boolean;
    highlighted: boolean;
    hot: boolean;
    originalAirDate: string;
    percentConsumed: number;
    programType: string;
    special: boolean;
    valuable: boolean;
    duration? : string;
    episodeLength? : IAodEpisodeDuration;
    airDate? : IAodEpisodeAirDate;
    contentUrlList: Array<string>; // gets delivered as contentUrlList.contentUrls, normalized to this
    contextualBanners: Array<string>;
    dmcaInfo: IDmcaInfo;
    drmInfo: IDrmInfo;
    hosts: Array<string>; // array of strings containing the guid's of the host personalities for the aodEpisode
    publicationInfo: IPublicationInfo;
    favoriteEpisodeCAId: string;
}

export interface IAodEpisodeDuration
{
    days : number;
    hours : number;
    minutes : number;
    seconds : number;
    milliseconds : number;
    totalSeconds : number;
}

export interface IAodEpisodeAirDate
{
    year : number;
    day : number;
    dayofWeek : number;
    hour : number;
    minutes : number;
    seconds : number;
    milliseconds : number;
}

export interface IPublicationInfo
{
    accessControlIdentifier: string;
    creationDate: string;
    episodeModifier: string;
    expiryDate: string;
    identifier: number;
    publicationDate: string;
    serialNumber: number;
}
