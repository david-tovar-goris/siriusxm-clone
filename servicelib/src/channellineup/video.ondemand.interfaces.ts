import {IOnDemandEpisode} from "./ondemand.interfaces";

export interface IVodEpisode extends IOnDemandEpisode
{
    allowDownload : boolean;
    disableAutoBanners : boolean;
    disableRecommendations : boolean;
    hosts : string; // humanHostList from API, but its a single string, not an array
    hostList : Array<string>;
    topics : string; // humanTopicsList from API, but its a single string, not an array
    topicList : Array<string>;
    rating : string;
    offlinePlayback : IOffLinePlayback;
    percentConsumed : number;
    originalAirDate : string;
    vodEpisodeGUID? : string;
}

export interface IOffLinePlayback
{
    highProfile : number;
    lowProfile : number;
    mediumProfile : number;
}
