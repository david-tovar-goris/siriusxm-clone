import {
    channelMock,
    onDemandShowMock,
    vodEpisodeMock,
    aodEpisodeMock, mockChannelLineup
} from "../test/mocks/channel.lineup";
import { of as observableOf } from "rxjs";

export const channelLineupServiceMock = {
    channelLineup : {
        channels : observableOf(mockChannelLineup)
    },
    findChannelById : jasmine.createSpy("findChannelById").and.returnValue(channelMock),
    findShowByGuid : jasmine.createSpy("findShowByGuid").and.returnValue(observableOf(onDemandShowMock)),
    findVodEpisodeByGuid : jasmine.createSpy("findVodEpisodeByGuid").and.returnValue(observableOf(vodEpisodeMock)),
    findAodEpisodeByGuid : jasmine.createSpy("findAodEpisodeByGuid").and.returnValue(observableOf(aodEpisodeMock))
};
