/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />
import { Observable, of as observableOf } from "rxjs";
import { ChannelLineupDelegate } from "./channel.lineup.delegate";

import {
    mockChannelListing,
    mockSuperCategories,
    mockDiscoverChannelListResponse
} from "../test/mocks/channel.lineup";

import {
    mockDiscoverAodRespoonse,
    mockAodShows,
    mockDiscoverAssetsResponse,
    mockVodEpisodes
} from "../test/mocks/on.demand";

import {
    ISuperCategory,
    ISubCategory
} from "./categories.interfaces";

import {
    IAodEpisode,
    IChannel,
    IImage,
    DiscoverChannelListConsts,
    DiscoverOnDemandConsts,
    DiscoverAssetsConsts,
    ModuleAreaRequest,
    ServiceEndpointConstants
}                        from "../index";
import { IOnDemandShow } from "./ondemand.interfaces";
import { ContentTypes }  from "../service/types/content.types";
import {ServiceFactory}  from "../service/service.factory";

describe("Channel Lineup Delegate Test Suite", function()
{
    beforeEach(function()
    {
        this.discoverChannelListResponse = JSON.parse(JSON.stringify(mockDiscoverChannelListResponse));
        this.discoverAodResponse         = JSON.parse(JSON.stringify(mockDiscoverAodRespoonse));
        this.discoverAssetsResponse      = JSON.parse(JSON.stringify(mockDiscoverAssetsResponse));
        this.superCategories             = JSON.parse(JSON.stringify(mockSuperCategories));
        this.aodShows                    = JSON.parse(JSON.stringify(mockAodShows));
        this.vodEpisodes                 = JSON.parse(JSON.stringify(mockVodEpisodes));

        this.channelLineupResponse = {
            contentData: { channelListing: JSON.parse(JSON.stringify(mockChannelListing)) }
        };

        this.mockHttp =
        {
            get:  (url:string) =>
            {
                if ( url.indexOf(ServiceEndpointConstants.endpoints.ON_DEMAND.V4_GET_SHOWS_EPISODES) < 0 )
                {
                    return observableOf(this.discoverChannelListResponse);
                }
                else
                {
                    return observableOf(this.discoverAodResponse);
                }
            },

            postModuleAreaRequest:  () =>
            {
                return observableOf(this.channelLineupResponse);
            }
        };

        this.appConfig = {
            apiEndpoint   : "myApi",
            appId         : "myApp",
            resultTemplate: "web",
            deviceInfo    : {
                appRegion       : "region",
                browser         : "unittest",
                browserVersion  : "version",
                clientDeviceId  : "id",
                clientDeviceType: "type",
                deviceModel     : "model",
                osVersion       : "OS",
                platform        : "platform",
                player          : "player",
                sxmAppVersion   : "version"
            }
        };

        this.channelLineupDelegate = new ChannelLineupDelegate(this.mockHttp, this.appConfig);
    });

    describe("Infrastructure >> ", function()
    {
        it("Should be able to create the service and all its dependencies",function()
        {
            expect(ServiceFactory.getInstance(ChannelLineupDelegate)).toBeDefined();
        });

        it("Constructor", function()
        {
            expect(this.channelLineupDelegate).toBeDefined();
            expect(this.channelLineupDelegate instanceof ChannelLineupDelegate).toEqual(true);
        });
    });

    describe("GetChannelLineup >> ", function()
    {
        it("Should send a proper API post request to get the lineup and the categories ", function()
        {
            spyOn(this.mockHttp, "postModuleAreaRequest").and
                .callFake((url, request:ModuleAreaRequest) =>
                {
                    expect(url)
                        .toBe(ServiceEndpointConstants.endpoints.CHANNEL.V4_COMMON_GET);
                    expect(request instanceof ModuleAreaRequest)
                        .toBe(true);
                    expect(request.moduleArea).toBe("Discovery");
                    expect(request.moduleType)
                        .toBe("ChannelListing");
                    expect(request.moduleRequest)
                        .toEqual({ resultTemplate: "responsive" });

                    return observableOf(this.channelLineupResponse);
                });

            this.channelLineupDelegate.getChannelLineup();

            expect(this.mockHttp.postModuleAreaRequest).toHaveBeenCalled();
        });

        it("Should parse the API response and return an IChannelLineup object ", function()
        {
            this.channelLineupDelegate.getChannelLineup()
                .subscribe((response) =>
                {
                    expect(response.channelChangeId)
                        .toBe(this.channelLineupResponse.contentData.channelListing.channelChangeId);
                    expect(response.superCategories)
                        .toBe(this.channelLineupResponse.contentData.channelListing.superCategories);
                    expect(response.channels)
                        .toBe(this.channelLineupResponse.contentData.channelListing.channels);
                });
        });

    });

    describe("DiscoverChannelList >> ", function()
    {
        it("Should send a proper API get request to get the live channel list ", function()
        {
            spyOn(this.mockHttp, "get").and
                .callFake((url, data, config) =>
                {
                    expect(url)
                        .toBe(ServiceEndpointConstants.endpoints.CHANNEL.V2_GET_LIST);
                    expect(data).toBe(null);
                    expect(config.params)
                        .toEqual(DiscoverChannelListConsts.PARAMS);

                    return observableOf(this.discoverChannelListResponse);
                });

            this.channelLineupDelegate.discoverChannelList();

            expect(this.mockHttp.get).toHaveBeenCalled();
        });

        it("Should parse the API response and return an ILiveChannels object ", function()
        {
            this.channelLineupDelegate.discoverChannelList()
                .subscribe((response) =>
                {
                    expect(response.updateFrequency)
                        .toBe(this.discoverChannelListResponse.updateFrequency);
                    expect(response.channels)
                        .toBe(this.discoverChannelListResponse.moduleDetails.liveChannelResponse.liveChannelResponses);
                });
        });

    });

    describe("DiscoverAod >> ", function()
    {
        it("Should send a proper API get request to get the aod shows for a given supercategory and subcategory ",
            function()
            {
                spyOn(this.mockHttp, "get").and
                    .callFake((url, data, config) =>
                    {
                        expect(url)
                            .toBe(ServiceEndpointConstants.endpoints.ON_DEMAND.V4_GET_SHOWS_EPISODES);
                        expect(data).toBe(null);
                        expect(config.params[DiscoverOnDemandConsts.DISCOVER_ONDEMAND_SUPERCATEGORY])
                            .toEqual("supercategory");
                        expect(config.params[DiscoverOnDemandConsts.DISCOVER_ONDEMAND_SUBCATEGORY])
                            .toEqual("subcategory");

                        return observableOf(this.discoverAodResponse);
                    });

                const discoverSpy = jasmine.createSpy("discoverSpy")
                    .and.callFake((response) =>
                    {
                        expect(response)
                            .toEqual(this.discoverAodResponse.contentData.aodShows[0].shows);
                    });

                this.channelLineupDelegate.discoverOnDemand("supercategory", "subcategory")
                    .subscribe(discoverSpy);

                expect(this.mockHttp.get).toHaveBeenCalled();
                expect(discoverSpy).toHaveBeenCalled();
            });
    });

    describe("discoverOnDemandAssets",function()
    {
        it("Should send a proper API get request to get assets for an ondemand show",function()
        {
            spyOn(this.mockHttp, "get").and
                .callFake((url, data, config) =>
                {
                    expect(url)
                        .toBe(ServiceEndpointConstants.endpoints.EDP.V4_GET_EDP_DETAILS);
                    expect(data).toBe(null);
                    expect(config.params.assetType).toBe(DiscoverAssetsConsts.DISCOVER_ONDEMAND_SHOW);
                    expect(config.params.assetGUID).toBe("guid");

                    return observableOf(this.discoverAssetsResponse);
                });

            const discoverSpy = jasmine.createSpy("discoverSpy")
                .and.callFake((show) =>
                {
                    expect(show.vodEpisodes).toBeDefined();
                });


            this.channelLineupDelegate.discoverOnDemandAssets({ guid : "guid" } as IOnDemandShow)
                .subscribe(discoverSpy);

            expect(this.mockHttp.get).toHaveBeenCalled();
            expect(discoverSpy).toHaveBeenCalled();
        });
    });

    describe("Normalization >> ", function()
    {
        it("Should clean up the data for supercategories, subcategories and channels", function()
        {
            const normalized : Array<ISuperCategory> = ChannelLineupDelegate.normalizeSuperCategories(this.superCategories);

            normalized.forEach((normalizedSuperCategory : ISuperCategory,catIndex : number) =>
            {
                expect(normalizedSuperCategory.categoryList).toBeDefined();
                expect(normalizedSuperCategory.categories).not.toBeDefined();
                expect(normalizedSuperCategory.imageList).toBeDefined();
                expect(normalizedSuperCategory.images).not.toBeDefined();

                normalizedSuperCategory.categoryList.forEach((normalizedSubCategory : ISubCategory,
                                                              subCatIndex : number) =>
                {
                    expect(normalizedSuperCategory.categoryList[subCatIndex].categoryGuid)
                        .toEqual(mockSuperCategories[catIndex].categories.categories[subCatIndex].categoryGuid);

                    expect(normalizedSubCategory.channelList).toBeDefined();
                    expect(normalizedSubCategory.channels).not.toBeDefined();

                    normalizedSubCategory.channelList.forEach((channel : IChannel, channelIndex : number) =>
                    {
                        expect(channel.channelGuid)
                            .toEqual(mockSuperCategories[catIndex].categories
                                                                  .categories[subCatIndex].channels
                                                                                          .channels[channelIndex].channelGuid);
                    });

                    expect(normalizedSubCategory.imageList).toBeDefined();
                    expect(normalizedSubCategory.images).not.toBeDefined();

                    normalizedSubCategory.imageList.forEach((image : IImage,imageIndex) =>
                    {
                        expect(image.name)
                            .toEqual(mockSuperCategories[catIndex].categories
                                                                  .categories[subCatIndex].images
                                                                                          .images[imageIndex].name);
                    });
                });
            });
        });

        it("Should replace duplicate subcategories with the first instance of the subcategory", function()
        {
            const normalized : Array<ISuperCategory> = ChannelLineupDelegate.normalizeSuperCategories(this.superCategories);

            expect(normalized[1].categoryList[0]).toBe(normalized[0].categoryList[0]);
        });

        it("Should clean up data for VOD episodes",function()
        {
            this.vodEpisodes.forEach((episode : any,index : number) =>
            {
                const show = { guid : "guid"};
                const newEpisode = ChannelLineupDelegate.normalizeVodEpisode(episode, { guid : "guid"} as IOnDemandShow);

                expect(newEpisode.show).toEqual(show);
                expect(newEpisode.type).toEqual(ContentTypes.VOD);
                expect(newEpisode.hosts).toEqual(mockVodEpisodes[index].humanHostList);
                expect(newEpisode.topics).toEqual(mockVodEpisodes[index].humanTopicList);
                expect(newEpisode.hostList).toEqual(newEpisode.hosts.split(","));
                expect(newEpisode.topicList).toEqual(newEpisode.topics.split(","));
            });
        });

        it("Should clean up data for AOD Shows",function()
        {
            this.aodShows.forEach((show : any,showIndex : number) =>
            {
                const normalizedShow:any = ChannelLineupDelegate.normalizeAudioShow(show,"web");

                Object.keys(mockAodShows[showIndex].showDescription).forEach((key : string) =>
                {
                    if (key === "creativeArts")
                    {
                        expect(normalizedShow.images)
                            .toEqual(mockAodShows[showIndex].showDescription.creativeArts);
                    }
                    else if (key === "legacyIds")
                    {
                        expect(normalizedShow.shortId)
                            .toEqual(mockAodShows[showIndex].showDescription.legacyIds.shortId);
                    }
                    else
                    {
                        expect(normalizedShow[key])
                            .toEqual(mockAodShows[showIndex].showDescription[key]);
                    }
                });

                expect(normalizedShow.aodEpisodes).toBeDefined();
                expect(normalizedShow.episodes).not.toBeDefined();

                normalizedShow.aodEpisodes.forEach((episode : IAodEpisode,episodeIndex : number) =>
                {
                    expect(episode.type).toEqual(ContentTypes.AOD);

                    expect(episode.episodeGuid)
                        .toEqual(mockAodShows[showIndex].episodes[0].episodes[episodeIndex].aodEpisodeGuid);

                    const duration : string = mockAodShows[showIndex].episodes[0].episodes[episodeIndex].duration;

                    if (duration)
                    {
                        const numbers : Array<string> = episode.duration.match(/\d+/g);

                        expect(episode.episodeLength.days).toEqual(parseInt(numbers[0]));
                        expect(episode.episodeLength.hours).toEqual(parseInt(numbers[1]));
                        expect(episode.episodeLength.minutes).toEqual(parseInt(numbers[2]));
                        expect(episode.episodeLength.seconds).toEqual(parseInt(numbers[3]));
                        expect(episode.episodeLength.milliseconds).toEqual(parseInt(numbers[4]));
                    }

                    const airDate = new Date(mockAodShows[showIndex].episodes[0].episodes[episodeIndex].originalAirDate);

                    expect(episode.airDate.year).toEqual(airDate.getFullYear());
                    expect(episode.airDate.day).toEqual(airDate.getDate());
                    expect(episode.airDate.dayofWeek).toEqual(airDate.getDay());
                    expect(episode.airDate.hour).toEqual(airDate.getHours());
                    expect(episode.airDate.minutes).toEqual(airDate.getMinutes());
                    expect(episode.airDate.seconds).toEqual(airDate.getSeconds());
                    expect(episode.airDate.milliseconds).toEqual(airDate.getMilliseconds());
                });
            });
        });
    });
});
