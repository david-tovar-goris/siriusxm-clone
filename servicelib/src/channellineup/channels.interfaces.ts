import { IBaseCategory , ISubCategory, ISuperCategory} from "./categories.interfaces";
import { IOnDemandShow } from "./ondemand.interfaces";

import {
    IImage,
    IMarkerList
} from "../index";
import { IDmcaInfo } from "../dmca/dmca.interface";

/**
 * @MODULE:     service-lib
 * @CREATED:    08/01/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *      Defines the interfaces for dealing with Channels
 */

export interface IBaseChannel
{
    channelGuid?: string;
    name?: string;
    aodEpisodeCount?: number;
    channelId?: string;
    markerLists?: Array<IMarkerList>;
    playingArtist?: string; //Will get ripped from markerLists and placed here by lineup service
    playingTitle?: string; //Will get ripped from markerLists and placed here by lineup service
    markerStartZuluTime?: number;
    markerEndZuluTime?: number;
}

export interface IChannel extends IBaseChannel
{
    channelNumber: number;
    clientBufferDuration: number;
    disableRecommendations: boolean;
    geoRestrictions?: number;
    aodShowCount: number;
    inactivityTimeout: number;
    isAvailable: boolean;
    isBizMature: boolean;
    isFavorite: boolean;
    isMature: boolean;
    isMySxm: boolean;
    isPersonalized: boolean;
    isPlayByPlay: boolean;
    mediumDescription: string;
    shortDescription: string;
    siriusChannelNumber: number;
    sortOrder: number;
    spanishContent: boolean;
    streamingName: string;
    tuneMethod : string;
    url: string;
    xmChannelNumber: number;
    xmServiceId: number;
    satOnly: boolean;
    subscribed : boolean;
    type?: string;
    firstSuperCategory? : ISuperCategory;
    firstSubCategory? : ISubCategory;
    categoryList?: Array<IBaseCategory>; // Will get normalized to this
    categories?: { categories: Array<IBaseCategory> }; // API returns this
    imageList?: Array<IImage>; // Will get normalized to this
    images?: { images: Array<IImage> }; // API returns this
    artwork? : any;
    shows?: Array<IOnDemandShow>;
    isGeoRestricted: boolean;
    dmcaInfo?: IDmcaInfo;
    stationFactory?: string;
}




