import { ISuperCategory } from "./categories.interfaces";
import {IChannel, IBaseChannel} from "./channels.interfaces";

/**
 * @MODULE:     service-lib
 * @CREATED:    07/19/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *   Defines the interfaces for a channel lineup
 */

export interface IChannelLineup
{
    channelChangeId : string;
    superCategories : Array<ISuperCategory>;
    channels : Array<IChannel>;
}

export interface ILiveChannels
{
    updateFrequency : number;
    channels : Array<IBaseChannel>;
}
