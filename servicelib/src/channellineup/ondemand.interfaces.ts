import { IImage } from "../service/types";
import {IAodEpisode} from "./audio.ondemand.interfaces";
import {IVodEpisode} from "./video.ondemand.interfaces";
import {
    IConnectInfo,
    IShowImage,
    ISuperCategory,
    ISubCategory
} from "../index";
import { ApiLayerTypes } from "../service/consts/api.types";

export interface IOnDemandShowDescription
{
    connectInfo : IConnectInfo;
    guid : string;
    assetGUID:string;
    images : Array<IShowImage>; // gets delivered as creativeArts, will be normalized to this
    shortId : string; // gets delivered as legacyIds.shortId, will be normalized to this
    longDescription : string;
    shortDescription : string;
    longTitle : string;
    mediumTitle : string;
    programType : string;
    relatedChannelIds : Array<string>;
    artwork : any;
}

export type IOnDemandShow = Partial<IOnDemandShowDescription> &
{
    type : string;
    disableAllBanners : boolean;
    episodeCount : number;
    newEpisodeCount : number;
    showDescription : IOnDemandShowDescription;
    aodEpisodes : Array<IAodEpisode>; // delivered as aodEpisodes[0].aodEpisodes, will be normalized to this
    vodEpisodes : Array<IVodEpisode>; // delivered as aodEpisodes[0].aodEpisodes, will be normalized to this
    firstSuperCategory : ISuperCategory;
    firstSubcategory : ISubCategory;
    isFavorite : boolean;
};

export interface IOnDemandEpisode
{
    disableAllBanners : boolean;
    episodeGuid : string;
    assetGuid : string;
    show : IOnDemandShow;
    longDescription : string;
    shortDescription : string;
    longTitle : string;
    mediumTitle : string;
    isFavorite: boolean;
    type : string;
    images: IImage[];
}

