/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import * as _ from "lodash";

import { mockAodShows } from "../test/mocks/on.demand";
import {
    mockChannelLineup,
    mockLiveChannelListing,
    mockSuperCategories, mockImages
} from "../test/mocks/channel.lineup";

import {
    IChannel,
    IBaseChannel
} from "./channels.interfaces";

import { IOnDemandShow } from "./ondemand.interfaces";
import { ISuperCategory } from "./categories.interfaces";
import { ChannelLineup } from "./channel.lineup";
import { ChannelLineupDelegate } from "./channel.lineup.delegate";
import { mockForYouData } from "./mock-for-you-data";

describe("Channel Lineup Test Suite", function()
{

    beforeEach(function()
               {
                   this.liveChannels = JSON.parse(JSON.stringify(mockLiveChannelListing));
                   this.lineup = JSON.parse(JSON.stringify(mockChannelLineup));
                   this.superCategories = JSON.parse(JSON.stringify(mockSuperCategories));
                   this.shows = JSON.parse(JSON.stringify(mockAodShows));


                   this.lineup = this.lineup.map(ChannelLineupDelegate.normalizeChannel);
                   this.superCategories = ChannelLineupDelegate.normalizeSuperCategories(this.superCategories);
                   this.shows = this.shows.map((show) => ChannelLineupDelegate.normalizeAudioShow(show,"web"));

                   this.channelLineup = new ChannelLineup();
               });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.channelLineup).toBeDefined();
            expect(this.channelLineup instanceof ChannelLineup).toEqual(true);
        });
    });

    describe("setSuperCategories >> ", function()
    {
        function onSuperCategory(superCategories: Array<ISuperCategory>)
        {
            if (superCategories.length === 0)
            { return; }

            expect(superCategories.length).toBe(2);

            Object.keys(superCategories[ 1 ])
                  .forEach((key: string) =>
                  {
                      if (key === "categoryList")
                      {
                          expect(superCategories[ 1 ].categories).toBeUndefined();
                      }
                      else if (key !== "categories")
                      {
                          expect(superCategories[ 1 ][ key ])
                              .toEqual(mockSuperCategories[ 1 ][ key ]);
                      }
                  });
        }

        it("When called with supercategory array, it will trigger the observer", function()
        {
            const onSuperCategorySpy = jasmine.createSpy("onSuperCategorySpy").and.callFake(onSuperCategory);

            this.channelLineup.setSuperCategories(this.superCategories);
            this.channelLineup.superCategories.subscribe(onSuperCategorySpy);

            expect(onSuperCategorySpy).toHaveBeenCalled();
        });
    });

    describe("setLiveChannels >> ", function()
    {
        let channelSwap = false;

        beforeEach(function() { channelSwap = false; });

        function checkChannel(channel, mockChannel, mockLiveChannel)
        {
            Object.keys(channel)
                  .forEach((key : string) =>
                           {
                               if (key === "categoryList")
                               {
                                   const mockCategories = mockChannel.categories.categories;

                                   channel.categoryList.forEach((category,index) =>
                                                                {
                                                                    expect( category.categoryGuid )
                                                                        .toEqual( mockCategories[ index ].categoryGuid );
                                                                });
                               }
                               else if (key === "imageList")
                               {
                                   expect(channel[key]).toEqual(mockChannel.images.images);
                               }
                               else if (key === "categories")
                               {
                                   expect(channel[key]).toBeUndefined();
                               }
                               else if (key != "images" && mockChannel[key] === undefined)
                               {
                                   if (key == "playingTitle")
                                   {
                                       expect(channel[key]).toEqual(mockLiveChannel.markerLists[0]
                                                                        .markers[0]
                                                                        .cut
                                                                        .title);
                                   }
                                   if (key == "playingArtist")
                                   {
                                       expect(channel[key]).toEqual(mockLiveChannel.markerLists[0]
                                                                        .markers[0]
                                                                        .cut
                                                                        .artists[0]
                                                                        .name);
                                   }
                               }
                               else
                               {
                                   if (key !== "images")
                                   {
                                       expect(channel[key]).toEqual(mockChannel[key]);
                                   }
                                   else
                                   {
                                       const images =
                                       {
                                           background : mockImages[1],
                                           backgroundWhatALongNameThisIs : mockImages[2],
                                           backgroundWhatALongNameThisIsWithParensNowDealWithThat : mockImages [3]
                                       };

                                       expect(channel[key]).toEqual(images);
                                   }
                               }
                           });
        }

        function onLiveChannels(channels : Array<IChannel>)
        {
            if (channels.length === 0)
            { return; }

            expect(channels.length).toBe(2);

            channels.forEach((channel : IChannel, index) =>
                             {
                                 const mockIndex = mockChannelLineup.length - 1 - index;
                                 if (channelSwap === false)
                                 {
                                     checkChannel(channel, mockChannelLineup[index], mockLiveChannelListing[index]);
                                 }
                                 else
                                 {
                                     checkChannel(channel, mockChannelLineup[index], mockLiveChannelListing[mockIndex]);
                                 }
                             });
        }

        function onSuperCategory(superCategories : Array<ISuperCategory>)
        {
            if (superCategories.length === 0)
            { return; }

            expect(superCategories.length).toBe(mockSuperCategories.length);

            let scIndex = (superCategories[0].key !== mockForYouData.key) ? 0 : 1;

            checkChannel(superCategories[scIndex].categoryList[0].channelList[0], mockChannelLineup[0], mockLiveChannelListing[0]);
            checkChannel(superCategories[scIndex].categoryList[0].channelList[1], mockChannelLineup[1], mockLiveChannelListing[1]);
        }

        it("When called with live channels and a channel lineup, it will merge the lineup into the channels",
           function()
           {
               const onLiveChannelsSpy = jasmine.createSpy("onLiveChannels").and.callFake(onLiveChannels);

               this.channelLineup.setSuperCategories(this.superCategories);
               this.channelLineup.setChannelLineup(this.lineup);
               this.channelLineup.setLiveChannels(this.liveChannels);
               this.channelLineup.channels.subscribe(onLiveChannelsSpy);

               expect(onLiveChannelsSpy).toHaveBeenCalled();
           });

        it("When called with live channels and a channel lineup, it will sort the subcategories",
            function()
            {
                const onLiveChannelsSpy = jasmine.createSpy("onLiveChannels").and.callFake(onLiveChannels);

                this.channelLineup.setSuperCategories(this.superCategories);
                this.channelLineup.setChannelLineup(this.lineup);
                this.channelLineup.setLiveChannels(this.liveChannels);
                this.channelLineup.channels.subscribe(onLiveChannelsSpy);

                this.channelLineup.superCategories.subscribe((superCategories : Array<ISuperCategory>) =>
                {
                    superCategories.forEach((superCategory : ISuperCategory) =>
                    {
                        if (superCategory.key === "supcat1")
                        {
                            expect(superCategory.categoryList[0].sortOrder < superCategory.categoryList[1].sortOrder);
                        }
                    });
                });
            });


        it("When called with live channels and a channel lineup, it will merge the lineup into the channels, then "
           + "update the merged lineup",
           function()
           {
               const onLiveChannelsSpy = jasmine.createSpy("onLiveChannels").and.callFake(onLiveChannels);

               this.channelLineup.setSuperCategories(this.superCategories);
               this.channelLineup.setChannelLineup(this.lineup);
               this.channelLineup.setLiveChannels(this.liveChannels);
               this.channelLineup.channels.subscribe(onLiveChannelsSpy);

               const oldEpisodeCounts = [];

               this.liveChannels.forEach((channel : IChannel, index : number) =>
                                    {
                                        oldEpisodeCounts.push(channel.aodEpisodeCount);
                                        channel.aodEpisodeCount = 199 + index;
                                    });

               this.channelLineup.setLiveChannels(this.liveChannels);

               this.liveChannels.forEach((channel : IChannel, index : number) =>
                                    {
                                        channel.aodEpisodeCount = oldEpisodeCounts[index];
                                    });

               expect(onLiveChannelsSpy.calls.count()).toBe(2);
           });

        it("When called with live channels and a channel lineup, it will merge the lineup into the channels, then "
           + "update and re-order the merged lineup",
           function()
           {
               const onLiveChannelsSpy = jasmine.createSpy("onLiveChannels").and.callFake(onLiveChannels);

               this.channelLineup.setSuperCategories(this.superCategories);
               this.channelLineup.setChannelLineup(this.lineup);
               this.channelLineup.setLiveChannels(this.liveChannels);
               this.channelLineup.channels.subscribe(onLiveChannelsSpy);

               const oldEpisodeCounts = [];

               this.liveChannels.forEach((channel : IChannel, index : number) =>
                                    {
                                        oldEpisodeCounts.push(channel.aodEpisodeCount);
                                        channel.aodEpisodeCount = 199 + index;
                                    });

               this.channelLineup.setLiveChannels(this.liveChannels);

               this.liveChannels.forEach((channel : IChannel, index : number) =>
                                    {
                                        channel.aodEpisodeCount = oldEpisodeCounts[index];
                                    });

               expect(onLiveChannelsSpy.calls.count()).toBe(2);

           });

        it("When called with live channels, super categories present, and a lineup, it will merge all channels "
           + " from the lineups into the channel lists in the subcategories within the super categories",
           function()
           {
               const onLiveChannelsSpy = jasmine.createSpy("onLiveChannels").and.callFake(onLiveChannels);
               const onSuperCategorySpy = jasmine.createSpy("onSuperCategorySpy").and.callFake(onSuperCategory);

               this.channelLineup.setSuperCategories(this.superCategories);
               this.channelLineup.superCategories.subscribe(onSuperCategorySpy);

               this.channelLineup.setChannelLineup(this.lineup);
               this.channelLineup.setLiveChannels(this.liveChannels);
               this.channelLineup.channels.subscribe(onLiveChannelsSpy);

               expect(onLiveChannelsSpy).toHaveBeenCalled();
               expect(onSuperCategorySpy).toHaveBeenCalled();
           });

    });

    describe("Categories and Supercategories >> ", function()
    {
        it("getSuperCategoryKeys and getSubCategoryKeys should return all the keys ", function()
        {
            this.channelLineup.setSuperCategories(this.superCategories);

            const superCatKeys = this.superCategories.map((superCategory) => superCategory.key);

            expect(this.channelLineup.getSupercategoryKeys()).toEqual(superCatKeys);

            superCatKeys.forEach((superCategoryKey, index) =>
                                 {
                                     const subCatKeys = this.superCategories[index].categoryList
                                                                              .map((subCategory) => subCategory.key);

                                     expect(this.channelLineup.getSubcategoryKeys(superCategoryKey))
                                         .toEqual(subCatKeys);
                                 });
        });

        it("setOnDemandShows should set the shows for a given category/supercategory", function()
        {
            this.channelLineup.setSuperCategories(this.superCategories);
            this.channelLineup.setChannelLineup(this.lineup);

            this.shows.forEach((show) =>
                          {
                              show.showDescription
                                  .relatedChannelIds.push(this.superCategories[0].categoryList[1].channelList[0].channelGuid);
                              show.vodEpisodes = [];
                          });

            this.channelLineup.setOnDemandShows("supcat1", "cat2", this.shows);

            this.channelLineup.superCategories
                         .subscribe((superCats : Array<ISuperCategory>) =>
                                    {
                                        expect(superCats[0].categoryList[0].onDemandShows).toEqual(this.shows);

                                        this.shows.forEach((show) =>
                                                      {
                                                          expect(_.find(superCats[0]
                                                                            .categoryList[1]
                                                                            .channelList[0]
                                                                            .shows,
                                                                        (channelShow) => channelShow === show))
                                                              .toBeDefined();

                                                          expect(_.find(superCats[0]
                                                                            .categoryList[0]
                                                                            .channelList[1]
                                                                            .shows,
                                                                        (channelShow) => channelShow === show))
                                                              .not.toBeDefined();

                                                      });

                                    });
        });

    });

});
