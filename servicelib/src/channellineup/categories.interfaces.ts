import { IImage }                 from "../service/types";
import { ISuperCategoryCarousel } from "../carousel";
import {
    IBaseChannel,
    IChannel
}                                 from "./channels.interfaces";
import { IOnDemandShow }          from "./ondemand.interfaces";

/**
 * @MODULE:     service-lib
 * @CREATED:    08/01/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 * Defines the interfaces for dealing with categories
 */

export interface IBaseCategory
{
    categoryGuid : string;
    isPrimary : boolean;
    key : string;
    name : string;
}

export interface ICategory extends IBaseCategory
{
    aodShowCount : number;
    description : string;
    imageList? : Array<IImage>;
    images? : { images : Array<IImage> };
    sortOrder : number;
    url : string;
}

export interface ISubCategory extends ICategory
{
    duplicate? : boolean; // tells us that the subcategory is a dup so we do not have to setup AOD shows for it
    channelList? : Array<IChannel>; // Will get normalized to this
    channels? : { channels : Array<IBaseChannel> }; // API returns this
    onDemandShows? : Array<IOnDemandShow>; // this will get added in when the aod roster for the subcategory is pulled
    onDemandPullTime : number;
    categoryType? : string;
    order? : number;
}

export interface ISuperCategory extends ICategory
{
    categoryList? : Array<ISubCategory>; // Will get normalized to this
    categories? : { categories : Array<ISubCategory> }; // API returns this
    carousels? : Array<ISuperCategoryCarousel>;
}

