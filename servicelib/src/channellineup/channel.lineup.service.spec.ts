
import {throwError as observableThrowError,  Observable, of as observableOf } from 'rxjs';
/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />
import * as _ from "lodash";

import { mockAodShows } from "../test/mocks/on.demand";
import {
    mockSuperCategories,
    mockChannelListing,
    mockLiveChannels
} from "../test/mocks/channel.lineup";

import { FavoriteServiceMock, FavoriteModelMock }      from "../test/mocks/favorites";
import { IChannel }                 from "./channels.interfaces";
import { ChannelLineupDelegate }    from "./channel.lineup.delegate";
import { ChannelLineupService }     from "./channel.lineup.service";
import { FavoriteService, FavoriteModel }          from "../favorite";
import { HttpProvider }             from "../http";
import { PLATFORM_WEBEVEREST }      from "../service/types";
import { MockAppConfig }            from "../test/mocks/app.config.mock";
import { AppMonitorService }        from "../app-monitor";
import { ServiceEndpointConstants } from "../service/consts";
import { ApiCodes }                 from "../service/consts/api.codes";
import { ConfigService } from "../config";

describe("Channel Lineup Test Suite", () =>
{
    const configService = {
        liveVideoEnabled: () => true,
        getRelativeUrlSettings: () => [],
        getRelativeURL: (urlKey:string) => urlKey,
        getSeededRadioBackgroundUrl : () => ""
    };
    beforeEach(function()
               {
                   jasmine.clock().install();

                   this.mockChannelListingResponse = JSON.parse(JSON.stringify(mockChannelListing));
                   this.mockChannelListingResponse = ChannelLineupDelegate.normalizeLineup(this.mockChannelListingResponse);

                   this.mockLiveChannelsResponse = JSON.parse(JSON.stringify(mockLiveChannels));

                   this.mockAodShowsResponse = JSON.parse(JSON.stringify(mockAodShows));
                   this.mockAodShowsResponse = this.mockAodShowsResponse.map((show) => ChannelLineupDelegate.normalizeAudioShow(show,"web"));

                   this.mockChannelLineupDelegate = {
                       getChannelLineup    : () => { return observableOf(this.ockChannelListingResponse); },
                       discoverChannelList : () => { return observableOf(this.mockLiveChannelsResponse); },
                       discoverOnDemand : () => { return observableOf(this.mockAodShowsResponse); }
                   };

                   this.mockHttpProvider = { addHttpHeaderObservable : function() { return observableOf(undefined); } };

                   this.mockAppMonitor = { ignoreFault : function() {} };

                   spyOn(this.mockAppMonitor,"ignoreFault").and.callThrough();

                   this.channelLineupService = new ChannelLineupService(new FavoriteServiceMock() as any as FavoriteService,
                                                                   this.mockChannelLineupDelegate,
                                                                   this.mockHttpProvider as any as HttpProvider,
                                                                   this.mockAppMonitor as any as AppMonitorService,
                                                                   new MockAppConfig(),
                                                                   configService as any as ConfigService,
                                                                   new FavoriteModelMock() as any as FavoriteModel);

                   expect(this.mockAppMonitor.ignoreFault)
                       .toHaveBeenCalledWith(ApiCodes.UNAVAILABLE_CONTENT,
                                             ServiceEndpointConstants.endpoints.CHANNEL.V2_GET_LIST);

               });

    afterEach(function() { jasmine.clock().uninstall(); });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.channelLineupService).toBeDefined();
            expect(this.channelLineupService instanceof ChannelLineupService).toEqual(true);
        });

        describe("GetChannelLineup >> ", function()
        {
            afterEach(function()
                      {
                          // IMPORTANT, don't want this unit test bleeding into other unit tests
                          this.channelLineupService.stopPeriodicLineupRequests();
                      });

            it("Should set the channelLineup and start periodic channel list requests ", function()
            {
                spyOn(this.mockChannelLineupDelegate,"discoverChannelList").and.callThrough();
                spyOn(this.mockChannelLineupDelegate,"discoverOnDemand").and.callThrough();
                spyOn(this.channelLineupService, "startPeriodicLineupRequests").and.callThrough();
                spyOn(this.channelLineupService.channelLineup,"setChannelLineup").and.callThrough();
                spyOn(this.channelLineupService.channelLineup,"setLiveChannels").and.callThrough();

                const onLineup = jasmine.createSpy("onLineup")
                                        .and.callFake(function(lineup)
                                                      {
                                                          expect(lineup[1]).toEqual(mockChannelListing[0]);

                                                          expect(this.channelLineupService.startPeriodicLineupRequests)
                                                              .toHaveBeenCalledWith(this.mockLiveChannelsResponse.updateFrequency);

                                                          expect(this.mockChannelLineupDelegate.discoverChannelList
                                                                                          .calls.count()).toBe(1);

                                                          jasmine.clock().tick(200 + 1);

                                                          expect(this.channelLineupService.channelLineup.setLiveChannels)
                                                              .toHaveBeenCalledWith(this.mockLiveChannelsResponse.channels);

                                                          jasmine.clock().tick(this.mockLiveChannelsResponse.updateFrequency * 1000 + 1);

                                                          expect(this.channelLineupService.channelLineup.setChannelLineup)
                                                              .toHaveBeenCalledWith(this.mockChannelListingResponse.channels);

                                                          expect(this.channelLineupService.channelLineup.setLiveChannels)
                                                              .toHaveBeenCalledWith(this.mockLiveChannelsResponse.channels);

                                                          expect(this.mockChannelLineupDelegate.discoverChannelList
                                                                                          .calls.count()).toBe(2);
                                                      });

                this.channelLineupService.channelLineup
                    .setSuperCategories(ChannelLineupDelegate.normalizeSuperCategories(JSON.parse(JSON.stringify(mockSuperCategories))));

                this.channelLineupService.getChannelLineup()
                                    .subscribe(onLineup);
            });

            it("Should set the channelLineup and start periodic channel list requests using the default period when "
               + "the period is not present in the lineup response", function()
            {
                this.mockLiveChannelsResponse.updateFrequency = undefined;

                spyOn(this.mockChannelLineupDelegate,"discoverChannelList").and.callThrough();
                spyOn(this.channelLineupService, "startPeriodicLineupRequests").and.callThrough();

                const onLineup = jasmine.createSpy("onLineup")
                                        .and.callFake(function(lineup)
                                                      {
                                                          expect(lineup[1]).toEqual(mockChannelListing[0]);

                                                          expect(this.channelLineupService.startPeriodicLineupRequests)
                                                              .toHaveBeenCalledWith(undefined);

                                                          expect(this.mockChannelLineupDelegate.discoverChannelList
                                                                                          .calls.count()).toBe(1);

                                                          jasmine.clock()
                                                                 .tick(ChannelLineupService.DEFAULT_LIVE_CHANNEL_QUERY_PERIOD_MS + 1);

                                                          expect(this.mockChannelLineupDelegate.discoverChannelList
                                                                                          .calls.count()).toBe(2);
                                                      });

                this.channelLineupService.channelLineup.setSuperCategories(JSON.parse(JSON.stringify(mockSuperCategories)));

                this.channelLineupService.getChannelLineup()
                                    .subscribe(onLineup);
            });

            it("Should be able to start and cancel the periodic channel list requests ", function()
            {
                const period = mockLiveChannels.updateFrequency;

                spyOn(this.mockChannelLineupDelegate,"discoverChannelList").and.callThrough();

                this.channelLineupService.startPeriodicLineupRequests(period);

                jasmine.clock().tick((period * 2 * 1000) + 1);

                expect(this.mockChannelLineupDelegate.discoverChannelList.calls.count()).toBe(2);

                this.channelLineupService.stopPeriodicLineupRequests();

                jasmine.clock().tick((period * 2 * 1000) + 1);

                expect(this.mockChannelLineupDelegate.discoverChannelList.calls.count()).toBe(2);
            });

            it("Should be able to start the periodic channel list requests using the period stored in the service", function()
            {
                spyOn(this.mockChannelLineupDelegate,"discoverChannelList").and.callThrough();

                this.channelLineupService.startPeriodicLineupRequests();

                jasmine.clock().tick((ChannelLineupService.DEFAULT_LIVE_CHANNEL_QUERY_PERIOD_MS * 2) + 1);

                expect(this.mockChannelLineupDelegate.discoverChannelList.calls.count()).toBe(2);
            });

            it("Should change the periodicity of the periodic channel list requests based on the response", function()
            {
                spyOn(this.mockChannelLineupDelegate,"discoverChannelList").and.callThrough();
                spyOn(this.channelLineupService, "startPeriodicLineupRequests").and.callThrough();
                spyOn(this.channelLineupService, "stopPeriodicLineupRequests").and.callThrough();

                this.channelLineupService.startPeriodicLineupRequests();

                this.mockLiveChannelsResponse.updateFrequency = ChannelLineupService.DEFAULT_LIVE_CHANNEL_QUERY_PERIOD_MS / 2000;

                jasmine.clock().tick(ChannelLineupService.DEFAULT_LIVE_CHANNEL_QUERY_PERIOD_MS + 1);

                expect(this.mockChannelLineupDelegate.discoverChannelList.calls.count()).toBe(1);

                jasmine.clock().tick(ChannelLineupService.DEFAULT_LIVE_CHANNEL_QUERY_PERIOD_MS + 1);

                expect(this.channelLineupService.stopPeriodicLineupRequests)
                    .toHaveBeenCalled();

                expect(this.channelLineupService.startPeriodicLineupRequests)
                    .toHaveBeenCalledWith( this.mockLiveChannelsResponse.updateFrequency);

                expect(this.mockChannelLineupDelegate.discoverChannelList.calls.count()).toBe(3);
            });

            it("Should log fault error ", function()
            {
                const errorMessage = "error";
                spyOn(this.mockChannelLineupDelegate, "getChannelLineup").and.returnValue(observableThrowError(errorMessage));
                this.channelLineupService.getChannelLineup()
                                    .subscribe(null, (error) =>
                                    {
                                        expect(error).toEqual(errorMessage);
                                    });
            });
        });
    });

    describe("getChannelImage >>", function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(ChannelLineupService.getChannelImage).toBeDefined();
                expect(_.isFunction(ChannelLineupService.getChannelImage)).toBeTruthy();
            });
        });

        describe("Execution >>", function()
        {
            const channel: IChannel =
                      {
                          isGeoRestricted: false,
                          channelNumber         : 1,
                          clientBufferDuration  : 1000,
                          disableRecommendations: false,
                          geoRestrictions       : 1,
                          aodShowCount          : 5,
                          inactivityTimeout     : 1000,
                          isAvailable           : true,
                          isBizMature           : true,
                          isFavorite            : true,
                          isMature              : true,
                          isMySxm               : false,
                          isPersonalized        : false,
                          isPlayByPlay          : false,
                          mediumDescription     : "8002 8002 8002",
                          shortDescription      : "8002 8002",
                          siriusChannelNumber   : 8002,
                          sortOrder             : 0,
                          spanishContent        : false,
                          streamingName         : "8002",
                          url                   : "8002",
                          xmChannelNumber       : 8002,
                          xmServiceId           : 8002,
                          tuneMethod            : "",
                          satOnly               : false,
                          subscribed            : true,
                          imageList             : [
                              {
                                  name    : "testName",
                                  platform: PLATFORM_WEBEVEREST,
                                  width   : 1000,
                                  height  : 1000,
                                  url     : "imageUrl1"
                              },
                              {
                                  name    : "testName1",
                                  platform: PLATFORM_WEBEVEREST,
                                  width   : 800,
                                  height  : 800,
                                  url     : "imageUr2"
                              }
                          ]
                      };

            it("Should return image url when sending height and width", function()
            {
                const urlExpected = channel.imageList[ 0 ].url + "?width=100&height=100&preserveAspect=true";

                expect(ChannelLineupService.getChannelImage(channel,
                                                            "testName",
                                                            PLATFORM_WEBEVEREST,
                                                            100,
                                                            100)).toEqual(urlExpected);
            });

            it("Should return image url when not sending height and width", function()
            {
                expect(ChannelLineupService.getChannelImage(channel,
                                                            "testName",
                                                            PLATFORM_WEBEVEREST)).toEqual(channel.imageList[ 0 ].url);
            });

            it("Should return image url as empty when image not found", function()
            {
                expect(ChannelLineupService.getChannelImage(channel,
                                                            "testName2",
                                                            PLATFORM_WEBEVEREST,
                                                            150,
                                                            150)).toEqual("");
            });
        });
    });
});
