import { SkipService }           from "./skip.service";

describe('SkipService', function()
{
    beforeEach(function()
    {
        this.seekService = { seekThenPlay: jasmine.createSpy('seekThenPlay') };
        this.mediaPlayerService = {mediaPlayer: {mediaType : 'live'}};

        this.mediaTimestampService = {
            getSegmentMarkers: jasmine.createSpy('getSegmentMarkers'),
            getCutMarkers: jasmine.createSpy('getCutMarkers'),
            playhead: {
                currentTime: {
                    seconds: 13,
                    zuluMilliseconds: 13000
                }
            },
            livePointTimestamp: 15,
            livePointTimestampZulu: 15000
        };

        this.dmcaService = {
            isUnrestricted: jasmine.createSpy('isUnrestricted'),
            isRestricted: jasmine.createSpy('isRestricted'),
            isDisallowed: jasmine.createSpy('isDisallowed')
        };

        this.skipService = new SkipService(this.mediaTimestampService, this.mediaPlayerService, this.seekService, this.dmcaService);

    });

    describe('skipBack()', function()
    {
        describe('when unrestricted', function()
        {
            beforeEach(function()
            {
                this.dmcaService.isUnrestricted.and.returnValue(true);
            });

            it('when within 10 seconds of track it goes to one before', function()
            {
                this.mediaTimestampService.getSegmentMarkers.and.returnValue([
                    { times: { zuluStartTime: 0, zeroStartTime: 0 }},
                    { times: { zuluStartTime: 10000, zeroStartTime: 10 }},
                    { times: { zuluStartTime: 12000, zeroStartTime: 12 }}
                ]);
                this.skipService.skipBack({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(10000, false);
            });

            it('when not within 10 seconds of start track it goes to beginning of same one', function()
            {
                this.mediaTimestampService.getSegmentMarkers.and.returnValue([
                    { times: { zuluStartTime: 0, zeroStartTime: 0 }},
                    { times: { zuluStartTime: 2000, zeroStartTime: 2 }},
                    { times: { zuluStartTime: 18000, zeroStartTime: 18 }}
                ]);

                this.skipService.skipBack({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(2000, false);
            });

            it('when within 10 seconds of track and one segment it goes to only segment', function()
            {
                this.mediaTimestampService.getSegmentMarkers.and.returnValue([
                    { times: { zuluStartTime: 0, zeroStartTime: 0 }}
                ]);
                this.skipService.skipBack({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(0, false);
            });
        });

        describe('when restricted', function()
        {
            beforeEach(function()
            {
                this.dmcaService.isUnrestricted.and.returnValue(false);
            });

            it('when within 10 seconds of track it goes to one before', function()
            {
                this.mediaTimestampService.getCutMarkers.and.returnValue([
                    { times: { zuluStartTime: 0, zeroStartTime: 0 }},
                    { times: { zuluStartTime: 10000, zeroStartTime: 10 }},
                    { times: { zuluStartTime: 12000, zeroStartTime: 12 }}
                ]);

                this.skipService.skipBack({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(10000, false);
            });

            it('when not within 10 seconds of start track it goes to beginning of same one', function()
            {
                this.mediaTimestampService.getCutMarkers.and.returnValue([
                    { times: { zuluStartTime: 0, zeroStartTime: 0 }},
                    { times: { zuluStartTime: 2000, zeroStartTime: 2 }},
                    { times: { zuluStartTime: 18000, zeroStartTime: 18 }}
                ]);
                this.skipService.skipBack({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(2000, false);
            });
        });
    });

    describe('skipForward()', function()
    {
        describe('when unrestricted', function()
        {
            beforeEach(function()
            {
                this.dmcaService.isUnrestricted.and.returnValue(true);
            });

            it('when no more upcoming tracks it goes to livepoint', function()
            {
                this.mediaTimestampService.getSegmentMarkers.and.returnValue([
                    { times: { zuluStartTime: 10000, zeroStartTime: 10 }},
                    { times: { zuluStartTime: 12000, zeroStartTime: 12 }}
                ]);
                this.skipService.skipForward({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(15000, false);
            });

            it('when upcoming tracks but they are past livepoint it goes to the livepoint', function()
            {
                this.mediaTimestampService.getSegmentMarkers.and.returnValue([
                    { times: { zuluStartTime: 12000, zeroStartTime: 12 }},
                    { times: { zuluStartTime: 18000, zeroStartTime: 18 }},
                    { times: { zuluStartTime: 20000, zeroStartTime: 20 }}
                ]);
                this.skipService.skipForward({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(15000, false);
            });

            it('when upcoming but they are not past livepoint it goes to the next track', function()
            {
                this.mediaTimestampService.getSegmentMarkers.and.returnValue([
                    { times: { zuluStartTime: 12000, zeroStartTime: 12 }},
                    { times: { zuluStartTime: 14000, zeroStartTime: 14 }},
                    { times: { zuluStartTime: 20000, zeroStartTime: 20 }}
                ]);
                this.skipService.skipForward({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(14000, false);
            });
        });

        describe('when restricted', function()
        {
            beforeEach(function()
            {
                this.dmcaService.isUnrestricted.and.returnValue(false);
            });

            it('when no more upcoming tracks it goes to livepoint', function()
            {
                this.mediaTimestampService.getCutMarkers.and.returnValue([
                    { times: { zuluStartTime: 10000, zeroStartTime: 10 }},
                    { times: { zuluStartTime: 12000, zeroStartTime: 12 }}
                ]);
                this.skipService.skipForward({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(15000, false);
            });

            it('when upcoming tracks but they are past livepoint it goes to the livepoint', function()
            {
                this.mediaTimestampService.getCutMarkers.and.returnValue([
                    { times: { zuluStartTime: 12000, zeroStartTime: 12 }},
                    { times: { zuluStartTime: 18000, zeroStartTime: 18 }},
                    { times: { zuluStartTime: 20000, zeroStartTime: 20 }}
                ]);
                this.skipService.skipForward({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(15000, false);
            });

            it('when upcoming tracks and not past livepoint it goes to the next track', function()
            {
                this.mediaTimestampService.getCutMarkers.and.returnValue([
                    { times: { zuluStartTime: 12000, zeroStartTime: 12 }},
                    { times: { zuluStartTime: 14000, zeroStartTime: 14 }},
                    { times: { zuluStartTime: 20000, zeroStartTime: 20 }}
                ]);
                this.skipService.skipForward({});
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(14000, false);
            });
        });
    });
});
