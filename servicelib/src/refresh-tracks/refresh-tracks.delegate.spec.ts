import { BehaviorSubject, Observable, of, throwError } from "rxjs";
import { RefreshTracksDelegate } from "./refresh-tracks.delegate";
import { TuneChromecastService } from "../chromecast";
import { mock } from "ts-mockito";
import { ClientCodes, NowPlayingConsts, ServiceEndpointConstants } from "../service/consts";
import { TuneResponse } from "../tune";

describe("RefreshTracksDelegate Test Suite >>", function()
{
    beforeEach(function()
    {
        this.tuneChromecastService           = mock(TuneChromecastService);
        this.baseTime                        = new Date();
        jasmine.clock().install();
        jasmine.clock().mockDate(this.baseTime);

        this.tuneModelMock          =
            {
                tuneModel$: new BehaviorSubject({
                    currentMetaData: {},
                    tuneState: "",
                    tuneStatus: ""
                }) as Observable<any>

            };
        this.refreshTracksModelMock =
            {
                currentMetaData: {
                    mediaId: ""
                },
                refreshModelData: {}
            };

        this.mockHttp =
            {
                post: jasmine.createSpy("post").and.returnValue(of(this.refreshTracksSeededMockResponse))
            };

        this.delegate = new RefreshTracksDelegate(this.mockHttp,
            this.tuneChromecastService,
            this.refreshTracksModelMock,
            this.tuneModelMock);
    });

    afterEach(function()
    {
        jasmine.clock().uninstall();
    });

    describe("Infrastructure >> ", function()
    {
        it("Should have a test subject.", function()
        {
            expect(this.delegate instanceof RefreshTracksDelegate).toBe(true);
        });
    });

    describe("RefreshTracksDelegate >> ", function()
    {
        describe("calls to refreshTracksSeeded api >> ", function()
        {
            it("NO Refresh call made - When Tune state not IDLE ", function()
            {
                this.tuneModelMock.tuneModel$.next({
                    currentMetaData: {},
                    tuneState: "TUNING",
                    tuneStatus: ""
                });
                const payload =
                          {
                              sequencerSessionId: "SequencerId",
                              mediaType: "seededRadio",
                              tracksParam: [],
                              playerType: "local",
                              stationFactory: "stationFactory",
                              stationId: "station Id",
                              isSkipLimitReached: false,
                              mediaId: "mediaId"
                          };

                this.delegate.refreshTracksSeeded(payload).subscribe(response =>
                {
                });

                expect(this.mockHttp.post).not.toHaveBeenCalled();
            });

            it(" Refresh call made - When Tune state IDLE ", function()
            {
                this.tuneModelMock.tuneModel$.next({
                    currentMetaData: {
                        mediaId: "mediaId"
                    },
                    tuneState: "IDLE",
                    tuneStatus: "SUCCESS"
                });
                const payload =
                          {
                              sequencerSessionId: "SequencerId",
                              mediaType: "seededRadio",
                              tracksParam: [],
                              playerType: "local",
                              stationFactory: "stationFactory",
                              stationId: "station Id",
                              isSkipLimitReached: false,
                              mediaId: "mediaId"
                          };

                this.delegate.refreshTracksSeeded(payload).subscribe(response =>
                {
                });

                expect(this.mockHttp.post).toHaveBeenCalled();
            });

            it(" NO Refresh call made - When MediaId not matches ", function()
            {
                this.tuneModelMock.tuneModel$.next({
                    currentMetaData: {
                        mediaId: "mediaId"
                    },
                    tuneState: "IDLE",
                    tuneStatus: "FAILURE"
                });
                const payload =
                          {
                              sequencerSessionId: "SequencerId",
                              mediaType: "seededRadio",
                              tracksParam: [],
                              playerType: "local",
                              stationFactory: "stationFactory",
                              stationId: "station Id",
                              isSkipLimitReached: false,
                              mediaId: "mediaId"
                          };

                this.delegate.refreshTracksSeeded(payload).subscribe(response =>
                {
                    expect(response).toEqual(ClientCodes.ERROR);
                });
            });

            it(" Refresh call Sets refreshState - IDLE & refreshStatus - SUCCESS - When Tune state IDLE & API returned response ", function()
            {
                this.refreshTracksSeededMockResponse = {
                    stationFactory: "stationFactory",
                    mediaId: "mediaId"
                };

                this.tuneModelMock.tuneModel$.next({
                    currentMetaData: {
                        mediaId: "mediaId"
                    },
                    tuneState: "IDLE",
                    tuneStatus: "SUCCESS"
                });
                const payload =
                          {
                              sequencerSessionId: "SequencerId",
                              mediaType: "seededRadio",
                              tracksParam: [],
                              playerType: "local",
                              stationFactory: "stationFactory",
                              stationId: "station Id",
                              isSkipLimitReached: false,
                              mediaId: "mediaId"
                          };

                this.mockHttp.post.and
                        .callFake((url, request) =>
                        {
                            expect(url).toEqual(ServiceEndpointConstants.endpoints.SEEDED_RADIO.V4_REFRESH);
                            expect(request.moduleList.modules[0].moduleRequest.type).toEqual(NowPlayingConsts.SEEDEDRADIOSPARAMS.type);

                            return of(this.refreshTracksSeededMockResponse);
                        });


                this.delegate.refreshTracksSeeded(payload).subscribe(response =>
                {
                    expect(response).toEqual(this.refreshTracksSeededMockResponse);
                });

                expect(this.mockHttp.post).toHaveBeenCalled();
                expect(this.refreshTracksModelMock.refreshModelData.refreshState).toEqual("IDLE");
                expect(this.refreshTracksModelMock.refreshModelData.refreshStatus).toEqual("SUCCESS");
            });

            it(" Refresh call Set to IDLE & refreshStatus - FAILURE - When Tune state IDLE & API returned error ", function()
            {
                this.refreshTracksSeededMockResponse = {
                    stationFactory: "stationFactory",
                    mediaId: "mediaId"
                };

                this.tuneModelMock.tuneModel$.next({
                    currentMetaData: {
                        mediaId: "mediaId"
                    },
                    tuneState: "IDLE",
                    tuneStatus: "SUCCESS"
                });
                const payload =
                          {
                              sequencerSessionId: "SequencerId",
                              mediaType: "seededRadio",
                              tracksParam: [],
                              playerType: "local",
                              stationFactory: "stationFactory",
                              stationId: "station Id",
                              isSkipLimitReached: false,
                              mediaId: "mediaId"
                          };

                this.mockHttp.post.and
                        .callFake((url, request) =>
                        {
                            expect(url).toEqual(ServiceEndpointConstants.endpoints.SEEDED_RADIO.V4_REFRESH);
                            expect(request.moduleList.modules[0].moduleRequest.type).toEqual(NowPlayingConsts.SEEDEDRADIOSPARAMS.type);

                            return throwError(this.refreshTracksSeededMockResponse);
                        });


                this.delegate.refreshTracksSeeded(payload)
                        .subscribe(response =>
                        {
                            expect(response).toEqual(ClientCodes.ERROR);
                        });

                expect(this.mockHttp.post).toHaveBeenCalled();
                expect(this.refreshTracksModelMock.refreshModelData.refreshState).toEqual("IDLE");
                expect(this.refreshTracksModelMock.refreshModelData.refreshStatus).toEqual("FAILURE");
            });

            it("Refresh call retries - Multiple times while API returned 315 error until  Maximum limit reached", function()
            {
                this.refreshTracksSeededMockResponse = {
                    "ModuleListResponse": {
                        "messages": [{ "message": "TRACKS_UNAVAILABLE: No SXM Tracks found", "code": 315 }],
                        "status": 0
                    },
                    "code": 315
                };

                this.tuneModelMock.tuneModel$.next({
                    currentMetaData: {
                        mediaId: "mediaId"
                    },
                    tuneState: "IDLE",
                    tuneStatus: "SUCCESS"
                });
                const payload =
                          {
                              sequencerSessionId: "SequencerId",
                              mediaType: "seededRadio",
                              tracksParam: [],
                              playerType: "local",
                              stationFactory: "stationFactory",
                              stationId: "station Id",
                              isSkipLimitReached: false,
                              mediaId: "mediaId"
                          };

                this.mockHttp.post.and
                        .callFake((url, request) =>
                        {
                            expect(url).toEqual(ServiceEndpointConstants.endpoints.SEEDED_RADIO.V4_REFRESH);
                            expect(request.moduleList.modules[0].moduleRequest.type).toEqual(NowPlayingConsts.SEEDEDRADIOSPARAMS.type);

                            return throwError(this.refreshTracksSeededMockResponse);
                        });

                const onFailure = jasmine.createSpy("onFailure");
                const onSuccess = jasmine.createSpy("onSuccess").and.callFake((response) =>
                {
                    expect(response).toEqual(ClientCodes.ERROR);
                });

                this.delegate.refreshTracksSeeded(payload)
                        .subscribe(onSuccess, onFailure);

                let retries = RefreshTracksDelegate.SEEDED_RETRIES_ALLOWED;
                while (retries > 0)
                {
                    retries -= 1;
                    expect(this.mockHttp.post.calls.count()).toBe(RefreshTracksDelegate.SEEDED_RETRIES_ALLOWED - retries);
                    jasmine.clock().tick(1000);
                }

                expect(onSuccess).toHaveBeenCalled();
                expect(onSuccess).toHaveBeenCalledWith(ClientCodes.ERROR);
                expect(onSuccess.calls.count()).toBe(1);
                expect(onFailure.calls.count()).toBe(0);
            });

            it("Refresh call retries - Multiple times while API returned 315 error until successfull response reached ", function()
            {
                let fail = true;
                this.refreshTracksSeededMockResponse = {
                    "ModuleListResponse": {
                        "messages": [{ "message": "TRACKS_UNAVAILABLE: No SXM Tracks found", "code": 315 }],
                        "status": 0
                    },
                    "code": 315
                };

                this.tuneModelMock.tuneModel$.next({
                    currentMetaData: {
                        mediaId: "mediaId"
                    },
                    tuneState: "IDLE",
                    tuneStatus: "SUCCESS"
                });
                const payload =
                          {
                              sequencerSessionId: "SequencerId",
                              mediaType: "seededRadio",
                              tracksParam: [],
                              playerType: "local",
                              stationFactory: "stationFactory",
                              stationId: "station Id",
                              isSkipLimitReached: false,
                              mediaId: "mediaId"
                          };

                this.mockHttp.post.and
                        .callFake((url, request) =>
                        {
                            expect(url).toEqual(ServiceEndpointConstants.endpoints.SEEDED_RADIO.V4_REFRESH);
                            expect(request.moduleList.modules[0].moduleRequest.type).toEqual(NowPlayingConsts.SEEDEDRADIOSPARAMS.type);

                            return fail === true
                                   ? throwError(this.refreshTracksSeededMockResponse)
                                   : of(this.refreshTracksSeededMockResponse);
                        });

                const onFailure = jasmine.createSpy("onFailure");
                const onSuccess = jasmine.createSpy("onSuccess").and.callFake((response) =>
                {
                    expect(response).toEqual(this.refreshTracksSeededMockResponse);
                });

                this.delegate.refreshTracksSeeded(payload)
                        .subscribe(onSuccess, onFailure);

                let retries = RefreshTracksDelegate.SEEDED_RETRIES_ALLOWED;

                while (retries > 0)
                {
                    retries -= 1;
                    if(retries === 0)
                    {
                        this.refreshTracksSeededMockResponse = {
                            stationFactory: "stationFactory",
                            mediaId: "mediaId"
                        };
                        fail = false;
                    }
                    expect(this.mockHttp.post.calls.count()).toBe(RefreshTracksDelegate.SEEDED_RETRIES_ALLOWED - retries);
                    jasmine.clock().tick(1000);
                }

                expect(onSuccess).toHaveBeenCalled();
                expect(onSuccess).toHaveBeenCalledWith(this.refreshTracksSeededMockResponse);
                expect(onSuccess.calls.count()).toBe(1);
                expect(onFailure.calls.count()).toBe(0);
            });

            it("When Player is - Remote API call should not made ", function()
            {
                this.tuneModelMock.tuneModel$.next({
                    currentMetaData: {
                        mediaId: "mediaId"
                    },
                    tuneState: "IDLE",
                    tuneStatus: "SUCCESS"
                });
                const payload =
                          {
                              sequencerSessionId: "SequencerId",
                              mediaType: "seededRadio",
                              tracksParam: [],
                              playerType: "remote",
                              stationFactory: "stationFactory",
                              stationId: "station Id",
                              isSkipLimitReached: false,
                              mediaId: "mediaId"
                          };

                this.tuneChromecastService.refreshTracksSeeded = () =>
                {
                    return of(this.refreshTracksSeededMockResponse as TuneResponse);
                };

                this.delegate.refreshTracksSeeded(payload).subscribe(response =>
                {
                });

                expect(this.mockHttp.post).not.toHaveBeenCalled();
            });
        });

        describe("calls to refreshTracksAIC api >> ", function()
        {
            it("Verify params to the api call and verify the successful response", function()
            {
                this.refreshTracksAICMockResponse = {
                    moduleType : "",
                    updateFrequency : 50,
                    additionalChannelData : {
                        channelId : "channelId",
                        mediaType : "mediaType",
                        channel : {
                            channelId: "channelId",
                            channelGuid : "channelGuid"
                        }
                    }
                };

                const payload =
                          {
                              sequencerSessionId: "SequencerId",
                              mediaType: "seededRadio",
                              tracksParam: [],
                              playerType: "local",
                              stationFactory: "stationFactory",
                              stationId: "station Id",
                              isSkipLimitReached: false,
                              mediaId: "mediaId",
                              channelGUID : "channelGUID"
                          };

                this.mockHttp.post.and
                        .callFake((url, request) =>
                        {
                            expect(url).toEqual(ServiceEndpointConstants.endpoints.ADDITIONAL_CHANNELS.V4_REFRESH);
                            expect(request.moduleList.modules[0].moduleRequest.type).toEqual(NowPlayingConsts.ADDITIONALCHANNELREFRESH.type);

                            return of(this.refreshTracksAICMockResponse);
                        });

                this.delegate.refreshTracksAIC(payload).subscribe(response =>
                {
                    expect(response.mediaId).toEqual(this.refreshTracksAICMockResponse.additionalChannelData.channel.channelGuid);
                    expect(response.channel).toEqual(this.refreshTracksAICMockResponse.additionalChannelData.channel);
                });

                expect(this.mockHttp.post).toHaveBeenCalled();
            });

            it("Player type is remote - should not make API call ", function()
            {
                this.refreshTracksAICMockResponse = {
                    moduleType : "",
                    updateFrequency : 50,
                    additionalChannelData : {
                        channelId : "channelId",
                        mediaType : "mediaType",
                        channel : {
                            channelId: "channelId",
                            channelGuid : "channelGuid"
                        }
                    }
                };

                const payload =
                          {
                              sequencerSessionId: "SequencerId",
                              mediaType: "seededRadio",
                              tracksParam: [],
                              playerType: "remote",
                              stationFactory: "stationFactory",
                              stationId: "station Id",
                              isSkipLimitReached: false,
                              mediaId: "mediaId",
                              channelGUID : "channelGUID"
                          };

                this.tuneChromecastService.refreshTracks = () =>
                {
                    return of(this.refreshTracksAICMockResponse as TuneResponse);
                };

                this.mockHttp.post.and
                        .callFake((url, request) =>
                        {
                            expect(url).toEqual(ServiceEndpointConstants.endpoints.ADDITIONAL_CHANNELS.V4_REFRESH);
                            expect(request.moduleList.modules[0].moduleRequest.type).toEqual(NowPlayingConsts.ADDITIONALCHANNELREFRESH.type);

                            return of(this.refreshTracksAICMockResponse);
                        });

                this.delegate.refreshTracksAIC(payload).subscribe(response =>
                {
                    expect(response.mediaId).toEqual(this.refreshTracksAICMockResponse.additionalChannelData.channel.channelGuid);
                    expect(response.channel).toEqual(this.refreshTracksAICMockResponse.additionalChannelData.channel);
                });

                expect(this.mockHttp.post).not.toHaveBeenCalled();
            });
        });
    });
});
