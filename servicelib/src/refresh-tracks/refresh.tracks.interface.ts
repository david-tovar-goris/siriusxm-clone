export interface IRefreshTracksRequestPayload
{
    sequencerSessionId? : string;
    mediaType? : string;
    tracksParam?: Array<object>;
    playerType: string;
    stationFactory: string;
    stationId: string;
    isSkipLimitReached: boolean;
    mediaId: string;
}
