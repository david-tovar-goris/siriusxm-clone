import { Observable, of, throwError } from "rxjs";
import { mock } from "ts-mockito";

import { RefreshTracksService } from "./refresh-tracks.service";
import { AppMonitorService, IAppByPassState } from "../app-monitor";
import { MultiTrackList } from "../mediaplayer";
import { IRefreshTracksRequestPayload } from "./refresh.tracks.interface";
import { ClientCodes } from "../service/consts";
import { IClip, MediaTimeLine, TuneDelegate } from "../tune";
import { ContentTypes } from "../service/types";
import { MultiTrackConstants } from "..";

describe("RefreshTracks Service  Test Suite", function()
{
    beforeEach(function()
    {
        this.mockRefreshTracksDelegate = {
            refreshTracksAIC: () =>
            {
                return of(null);
            },
            refreshTracksSeeded: () =>
            {
                return of(null);
            }
        };

        this.configServiceMock = {
            liveVideoEnabled: () => true,
            getRelativeUrlSettings: () => [],
            getRelativeURL: (urlKey) => urlKey,
            getSeededRadioBackgroundUrl: () => ""
        };

        this.bypassMonitorService = {
            bypassErrorState: of({} as IAppByPassState)
        };

        this.appMonitorServiceMock = {
            ...mock(AppMonitorService),
            handleMessage: jasmine.createSpy('handleMessage')
        };
        this.refreshTracksService  = new RefreshTracksService(this.mockRefreshTracksDelegate,
            this.configServiceMock,
            this.bypassMonitorService,
            this.appMonitorServiceMock);

    });

    afterEach(function()
    {
    });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(RefreshTracksService).toBeDefined();
            expect(this.refreshTracksService instanceof RefreshTracksService).toEqual(true);
        });
    });

    describe("refreshTracksAIC Execution ", function()
    {
        it("Success when tracks empty array", function()
        {
            const channelId              = "channelId";
            this.mockRefreshTracksAICResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, [])
            };

            const payload = {
                mediaType: "additionalChannels",
                playerType: "local",
                sequencerSessionId: "sequencerSessionId"
            } as IRefreshTracksRequestPayload;
            const tracks  = new MultiTrackList(channelId, []);
            spyOn(this.mockRefreshTracksDelegate, "refreshTracksAIC").and
                                                                .callFake(() =>
                                                                {
                                                                    return of(this.mockRefreshTracksAICResponse);
                                                                });

            this.refreshTracksService.refreshTracksAIC(tracks, payload).subscribe(response =>
            {
                expect(response).toEqual(ClientCodes.SUCCESS);
            });
            expect(this.mockRefreshTracksDelegate.refreshTracksAIC).toHaveBeenCalled();
        });

        it("Success when we have the tracks", function()
        {
            const channelId  = "channelId";
            const mockTracks = [
                {
                    status: MultiTrackConstants.PRELOAD,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip,
                {
                    status: MultiTrackConstants.FUTURE,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 1
                } as IClip,
                {
                    status: MultiTrackConstants.CURRENT,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 2
                } as IClip,
                {
                    status: MultiTrackConstants.ENDED,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 3
                } as IClip,
                {
                    status: MultiTrackConstants.ERROR,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 4
                } as IClip
            ];

            this.mockRefreshTracksAICResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, [{
                    status: MultiTrackConstants.ENDED,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    }
                } as IClip
                ])
            };

            const payload = {
                mediaType: "additionalChannels",
                playerType: "local",
                sequencerSessionId: "sequencerSessionId"
            } as IRefreshTracksRequestPayload;
            const tracks  = new MultiTrackList(channelId, mockTracks);
            spyOn(this.mockRefreshTracksDelegate, "refreshTracksAIC").and
                                                                .callFake(() =>
                                                                {
                                                                    return of(this.mockRefreshTracksAICResponse);
                                                                });

            this.refreshTracksService.refreshTracksAIC(tracks, payload).subscribe(response =>
            {
                expect(response).toEqual(ClientCodes.SUCCESS);
            });
            expect(this.mockRefreshTracksDelegate.refreshTracksAIC).toHaveBeenCalled();
            const payloadToVerify = {
                channelGUID: "channelId",
                contentType: "additionalChannels",
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                tracksParam: [
                    { assetGuid: "assetGUID", playerEventType: "PLAYLIST" },
                    { assetGuid: "assetGUID", playerEventType: "PLAYLIST" },
                    { assetGuid: "assetGUID", playerEventType: "CONSUME" },
                    { assetGuid: "assetGUID", playerEventType: "CONSUME" },
                    { assetGuid: "assetGUID", playerEventType: "ERROR" }
                ]
            };
            expect(this.mockRefreshTracksDelegate.refreshTracksAIC).toHaveBeenCalledWith(payloadToVerify,
                [], 4);
        });

        it("Failure", function()
        {
            const channelId = "channelId";
            const payload   = {
                mediaType: "additionalChannels",
                playerType: "local",
                sequencerSessionId: "sequencerSessionId"
            } as IRefreshTracksRequestPayload;
            const tracks    = new MultiTrackList(channelId, []);
            const mockError = {
                message: "failed"
            };
            spyOn(this.mockRefreshTracksDelegate, "refreshTracksAIC").and
                                                                .callFake(() =>
                                                                {
                                                                    return throwError(mockError);
                                                                });

            this.refreshTracksService.refreshTracksAIC(tracks, payload)
                                .subscribe(response =>
                                {

                                }, (error) =>
                                {
                                    expect(error).toEqual(mockError);
                                });
            expect(this.mockRefreshTracksDelegate.refreshTracksAIC).toHaveBeenCalled();
        });
    });

    describe("refreshTracksSeeded Execution ", function()
    {
        it("Success when tracks are empty", function()
        {
            const channelId                 = "channelId";
            this.mockRefreshTracksSeededResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, []),
                moduleType: "seededradio",
                seededRadioData: {}
            };

            const payload = {
                mediaType: "seededradio",
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory"
            } as IRefreshTracksRequestPayload;
            const tracks  = new MultiTrackList(channelId, []);

            spyOn(TuneDelegate, "normalizeSeededRadioData").and
                                                           .callFake(() =>
                                                           {
                                                               return this.mockRefreshTracksSeededResponse;
                                                           });

            spyOn(this.mockRefreshTracksDelegate, "refreshTracksSeeded").and
                                                                   .callFake(() =>
                                                                   {
                                                                       return of(this.mockRefreshTracksSeededResponse);
                                                                   });

            this.refreshTracksService.refreshTracksSeeded(tracks, payload).subscribe(response =>
            {
                expect(response).toEqual(ClientCodes.SUCCESS);
            });
            expect(this.mockRefreshTracksDelegate.refreshTracksSeeded).toHaveBeenCalled();
        });

        it("Success when we have the tracks", function()
        {
            const channelId       = "channelId";
            const mockduration    = 100;
            const mockElapsedTime = 150;
            const mockTracks      = [
                {
                    status: MultiTrackConstants.PRELOAD,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip,
                {
                    status: MultiTrackConstants.FUTURE,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 1
                } as IClip,
                {
                    status: MultiTrackConstants.CURRENT,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 2
                } as IClip,
                {
                    status: MultiTrackConstants.ENDED,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 3,
                    duration: mockduration
                } as any as IClip,
                {
                    status: MultiTrackConstants.ERROR,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 4
                } as IClip,
                {
                    status: MultiTrackConstants.SKIP,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 5,
                    duration: mockduration
                } as any as IClip
            ];

            this.mockRefreshTracksAICResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, [{
                    status: MultiTrackConstants.ENDED,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    }
                } as IClip
                ])
            };

            const payload = {
                mediaType: "additionalChannels",
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory",
                isSkipLimitReached: false
            } as IRefreshTracksRequestPayload;
            const tracks  = new MultiTrackList(channelId, mockTracks);

            spyOn(TuneDelegate, "normalizeSeededRadioData").and
                                                           .callFake(() =>
                                                           {
                                                               return this.mockRefreshTracksSeededResponse;
                                                           });

            spyOn(this.mockRefreshTracksDelegate, "refreshTracksSeeded").and
                                                                   .callFake(() =>
                                                                   {
                                                                       return of(this.mockRefreshTracksSeededResponse);
                                                                   });

            this.refreshTracksService.refreshTracksSeeded(tracks, payload, mockElapsedTime).subscribe(response =>
            {
                expect(response).toEqual(ClientCodes.SUCCESS);
            });
            expect(this.mockRefreshTracksDelegate.refreshTracksSeeded).toHaveBeenCalled();

            const payloadToVerify = {
                stationId: 'stationId',
                tracksParam: [
                    { index: 0, playerEventType: 'PLAYLIST', elapsedTime: 0 },
                    { index: 1, playerEventType: 'PLAYLIST', elapsedTime: 0 },
                    { index: 2, playerEventType: 'STARTED', elapsedTime: 0 },
                    { index: 3, elapsedTime: 100, playerEventType: 'ENDED' },
                    { index: 4, playerEventType: 'ERROR', elapsedTime: 0 },
                    { index: 5, playerEventType: 'SKIP', elapsedTime: mockElapsedTime }
                ],
                stationFactory: 'stationFactory',
                playerType: 'local',
                isSkipLimitReached: false,
                mediaId: undefined
            };
            expect(this.mockRefreshTracksDelegate.refreshTracksSeeded).toHaveBeenCalledWith(payloadToVerify);
        });

        it("Failure when stationId or stationFactory empty", function()
        {
            const channelId = "channelId";
            const payload   = {
                mediaType: "additionalChannels",
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId"
            } as IRefreshTracksRequestPayload;
            const tracks    = new MultiTrackList(channelId, []);
            spyOn(this.mockRefreshTracksDelegate, "refreshTracksSeeded").and
                                                                   .callFake(() =>
                                                                   {
                                                                       return of(ClientCodes.ERROR);
                                                                   });

            this.refreshTracksService.refreshTracksSeeded(tracks, payload)
                                .subscribe(response =>
                                {
                                    expect(response).toEqual(ClientCodes.ERROR);

                                });
            expect(this.mockRefreshTracksDelegate.refreshTracksSeeded).not.toHaveBeenCalled();
        });

        it("Failure", function()
        {
            const channelId = "channelId";
            const payload   = {
                mediaType: "additionalChannels",
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory"
            } as IRefreshTracksRequestPayload;
            const tracks    = new MultiTrackList(channelId, []);
            spyOn(this.mockRefreshTracksDelegate, "refreshTracksSeeded").and
                                                                   .callFake(() =>
                                                                   {
                                                                       return of(ClientCodes.ERROR);
                                                                   });

            this.refreshTracksService.refreshTracksSeeded(tracks, payload)
                                .subscribe(response =>
                                {
                                    expect(response).toEqual(ClientCodes.ERROR);

                                });
            expect(this.mockRefreshTracksDelegate.refreshTracksSeeded).toHaveBeenCalled();
        });

        it("Failure when throw exception", function()
        {
            const channelId = "channelId";
            const payload   = {
                mediaType: "additionalChannels",
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory"
            } as IRefreshTracksRequestPayload;
            const tracks    = new MultiTrackList(channelId, []);
            const mockError = {
                message: "failed"
            };
            spyOn(this.mockRefreshTracksDelegate, "refreshTracksSeeded").and
                                                                   .callFake(() =>
                                                                   {
                                                                       return throwError(mockError);
                                                                   });

            this.refreshTracksService.refreshTracksSeeded(tracks, payload)
                                .subscribe(response =>
                                {

                                }, (error) =>
                                {
                                    expect(error).toEqual(mockError);
                                });
            expect(this.mockRefreshTracksDelegate.refreshTracksSeeded).toHaveBeenCalled();
        });
    });

    describe("preRefreshTracks Execution ", function()
    {
        it("When Media type is Seeded radio calls the seeded refresh tracks call", function()
        {
            const channelId                 = "channelId";
            this.mockRefreshTracksSeededResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, []),
                moduleType: "seededradio",
                seededRadioData: {}
            };

            const mediaTimeLine = {
                mediaType: ContentTypes.SEEDED_RADIO,
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory",
                channel: {
                    stationFactory: "stationFactory"
                },
                mediaId: "mediaId"
            } as any as MediaTimeLine;
            const tracks        = new MultiTrackList(channelId, []);

            spyOn(TuneDelegate, "normalizeSeededRadioData").and
                                                           .callFake(() =>
                                                           {
                                                               return this.mockRefreshTracksSeededResponse;
                                                           });

            spyOn(this.mockRefreshTracksDelegate, "refreshTracksSeeded").and
                                                                   .callFake(() =>
                                                                   {
                                                                       return of(this.mockRefreshTracksSeededResponse);
                                                                   });

            this.refreshTracksService.preRefreshTracks(tracks, mediaTimeLine);

            expect(this.mockRefreshTracksDelegate.refreshTracksSeeded).toHaveBeenCalled();
        });

        it("When Media type is AIC calls the AIC refresh tracks call", function()
        {
            const channelId                 = "channelId";
            this.mockRefreshTracksSeededResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, []),
                moduleType: "seededradio",
                seededRadioData: {}
            };

            const mediaTimeLine = {
                mediaType: ContentTypes.ADDITIONAL_CHANNELS,
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory",
                channel: {
                    stationFactory: "stationFactory"
                },
                mediaId: "mediaId"
            } as any as MediaTimeLine;
            const tracks        = new MultiTrackList(channelId, []);

            spyOn(this.mockRefreshTracksDelegate, "refreshTracksAIC").and
                                                                .callFake(() =>
                                                                {
                                                                    return of(this.mockRefreshTracksSeededResponse);
                                                                });

            this.refreshTracksService.preRefreshTracks(tracks, mediaTimeLine);

            expect(this.mockRefreshTracksDelegate.refreshTracksAIC).toHaveBeenCalled();
        });
    });

    describe("trackFinished Execution ", function()
    {
        it("trackFinished makes pre refresh tracks call", function()
        {
            const mockTracks = [
                {
                    status: MultiTrackConstants.CURRENT,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip
            ];

            const channelId = "channelId";
            const tracks    = new MultiTrackList(channelId, mockTracks);

            this.mockRefreshTracksSeededResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, []),
                moduleType: "seededradio",
                seededRadioData: {}
            };

            const mediaTimeLine = {
                mediaType: ContentTypes.SEEDED_RADIO,
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory",
                channel: {
                    stationFactory: "stationFactory"
                },
                mediaId: "mediaId",
                clips: tracks
            } as any as MediaTimeLine;

            spyOn(this.refreshTracksService, "preRefreshTracks").and.callThrough();

            const expectedTracks = [{
                assetGUID: "assetGUID",
                crossfade: { crossFade: 10 },
                index: 0,
                nextTrack: null,
                previousTrack: null,
                status: "ENDED"
            } as IClip
            ];
            const mockResult     = new MultiTrackList("channelId", expectedTracks);
            const result         = this.refreshTracksService.trackFinished("assetGUID", mediaTimeLine);
            expect(result).toEqual(mockResult);
            expect(this.refreshTracksService.preRefreshTracks).toHaveBeenCalled();
        });
    });

    describe("trackFailed Execution ", function()
    {
        it("trackFailed makes pre refresh tracks call", function()
        {
            const mockTracks = [
                {
                    status: MultiTrackConstants.CURRENT,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip
            ];

            const channelId = "channelId";
            const tracks    = new MultiTrackList(channelId, mockTracks);

            this.mockRefreshTracksSeededResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, []),
                moduleType: "seededradio",
                seededRadioData: {}
            };

            const mediaTimeLine = {
                mediaType: ContentTypes.SEEDED_RADIO,
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory",
                channel: {
                    stationFactory: "stationFactory"
                },
                mediaId: "mediaId",
                clips: tracks
            } as any as MediaTimeLine;

            spyOn(this.refreshTracksService, "preRefreshTracks").and.callThrough();

            this.refreshTracksService.trackFailed("assetGUID", mediaTimeLine);

            expect(this.refreshTracksService.preRefreshTracks).toHaveBeenCalled();
        });
    });

    describe("trackSkipped Execution ", function()
    {
        it("trackSkipped makes pre refresh tracks call", function()
        {
            const mockTracks = [
                {
                    status: MultiTrackConstants.CURRENT,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip
            ];

            const channelId = "channelId";
            const tracks    = new MultiTrackList(channelId, mockTracks);

            this.mockRefreshTracksSeededResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, []),
                moduleType: "seededradio",
                seededRadioData: {}
            };

            const mediaTimeLine = {
                mediaType: ContentTypes.SEEDED_RADIO,
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory",
                channel: {
                    stationFactory: "stationFactory"
                },
                mediaId: "mediaId",
                clips: tracks
            } as any as MediaTimeLine;

            spyOn(this.refreshTracksService, "preRefreshTracks").and.callThrough();

            const expectedTracks = [{
                assetGUID: "assetGUID",
                crossfade: { crossFade: 10 },
                index: 0,
                nextTrack: null,
                previousTrack: null,
                status: "SKIP"
            } as IClip
            ];
            const mockResult     = new MultiTrackList("channelId", expectedTracks);
            const result         = this.refreshTracksService.trackSkipped(mediaTimeLine);
            expect(result).toEqual(mockResult);
            expect(this.refreshTracksService.preRefreshTracks).toHaveBeenCalled();
        });
    });

    describe("trackStarted Execution ", function()
    {
        it("trackStarted makes pre refresh tracks call when track count is 1", function()
        {
            const mockTracks = [
                {
                    status: MultiTrackConstants.PRELOAD,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip
            ];

            const channelId = "channelId";
            const tracks    = new MultiTrackList(channelId, mockTracks);

            this.mockRefreshTracksSeededResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, []),
                moduleType: "seededradio",
                seededRadioData: {}
            };

            const mediaTimeLine = {
                mediaType: ContentTypes.SEEDED_RADIO,
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory",
                channel: {
                    stationFactory: "stationFactory"
                },
                mediaId: "mediaId",
                clips: tracks
            } as any as MediaTimeLine;

            spyOn(this.refreshTracksService, "preRefreshTracks").and.callThrough();

            const expectedTracks = [{
                assetGUID: "assetGUID",
                crossfade: { crossFade: 10 },
                index: 0,
                nextTrack: null,
                previousTrack: null,
                status: "CURRENT"
            } as IClip
            ];
            const mockResult     = new MultiTrackList("channelId", expectedTracks);
            const result         = this.refreshTracksService.trackStarted("assetGUID", mediaTimeLine);
            expect(result).toEqual(mockResult);
            expect(this.refreshTracksService.preRefreshTracks).toHaveBeenCalled();
        });

        it("trackStarted should not makes pre refresh tracks call when track count greater than 1", function()
        {
            const mockTracks = [
                {
                    status: MultiTrackConstants.PRELOAD,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip,
                {
                    status: MultiTrackConstants.FUTURE,
                    assetGUID: "assetGUID1",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip
            ];

            const channelId = "channelId";
            const tracks    = new MultiTrackList(channelId, mockTracks);

            this.mockRefreshTracksSeededResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, []),
                moduleType: "seededradio",
                seededRadioData: {}
            };

            const mediaTimeLine = {
                mediaType: ContentTypes.SEEDED_RADIO,
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory",
                channel: {
                    stationFactory: "stationFactory"
                },
                mediaId: "mediaId",
                clips: tracks
            } as any as MediaTimeLine;

            spyOn(this.refreshTracksService, "preRefreshTracks").and.callThrough();
            this.refreshTracksService.trackStarted("assetGUID", mediaTimeLine);

            expect(this.refreshTracksService.preRefreshTracks).not.toHaveBeenCalled();
        });
    });

    describe("trackResumed Execution ", function()
    {
        it("trackResumed makes pre refresh tracks call when track count is 1", function()
        {
            const mockTracks = [
                {
                    status: MultiTrackConstants.PRELOAD,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip
            ];

            const channelId = "channelId";
            const tracks    = new MultiTrackList(channelId, mockTracks);

            this.mockRefreshTracksSeededResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, []),
                moduleType: "seededradio",
                seededRadioData: {}
            };

            const mediaTimeLine = {
                mediaType: ContentTypes.SEEDED_RADIO,
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory",
                channel: {
                    stationFactory: "stationFactory"
                },
                mediaId: "mediaId",
                clips: tracks
            } as any as MediaTimeLine;

            spyOn(this.refreshTracksService, "preRefreshTracks").and.callThrough();

            const expectedTracks = [{
                assetGUID: "assetGUID",
                crossfade: { crossFade: 10 },
                index: 0,
                nextTrack: null,
                previousTrack: null,
                status: "CURRENT"
            } as IClip
            ];
            const mockResult     = new MultiTrackList("channelId", expectedTracks);
            const result         = this.refreshTracksService.trackResumed(mediaTimeLine);
            expect(result).toEqual(mockResult);
            expect(this.refreshTracksService.preRefreshTracks).toHaveBeenCalled();
        });

        it("trackResumed should not make pre refresh tracks call when track count greater than 1 ", function()
        {
            const mockTracks = [
                {
                    status: MultiTrackConstants.PRELOAD,
                    assetGUID: "assetGUID",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip,
                {
                    status: MultiTrackConstants.FUTURE,
                    assetGUID: "assetGUID1",
                    crossfade: {
                        crossFade: 10
                    },
                    index: 0
                } as IClip
            ];

            const channelId = "channelId";
            const tracks    = new MultiTrackList(channelId, mockTracks);

            this.mockRefreshTracksSeededResponse = {
                channelId: "channelId",
                clips: new MultiTrackList(channelId, []),
                moduleType: "seededradio",
                seededRadioData: {}
            };

            const mediaTimeLine = {
                mediaType: ContentTypes.SEEDED_RADIO,
                playerType: "local",
                sequencerSessionId: "sequencerSessionId",
                stationId: "stationId",
                stationFactory: "stationFactory",
                channel: {
                    stationFactory: "stationFactory"
                },
                mediaId: "mediaId",
                clips: tracks
            } as any as MediaTimeLine;

            spyOn(this.refreshTracksService, "preRefreshTracks").and.callThrough();

            const result = this.refreshTracksService.trackResumed(mediaTimeLine);
            expect(this.refreshTracksService.preRefreshTracks).not.toHaveBeenCalled();
        });
    });
});
