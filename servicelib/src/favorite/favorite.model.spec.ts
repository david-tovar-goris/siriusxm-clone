import { of } from "rxjs";
import { FavoriteModel } from "./favorite.model";
import {
    favList,
    favGroupedList
} from "../test/mocks/favorites/favorite.response";

describe("Favorite Model Test Suite:", function()
{

    const favoriteModel: FavoriteModel = new FavoriteModel();

    it("Setter and Getter of Favorite List", function()
    {
        favoriteModel.setFavorites(favList,true);
        expect(favList[0]).toBe(favoriteModel.getFavorite(favList[0].assetGUID));
    });

    it("Get Favorite By Channel", function()
    {
        expect(favoriteModel.getFavoriteForChannel("03")).toBe(favList[2]);
        expect(favoriteModel.getFavoriteForChannel("02")).toBeUndefined();
    });

    it("Set Favorite List from Carousel Api Service", function()
    {
        let favoriteGroup$ = of(favGroupedList);
        let spy = spyOn(favoriteModel, "setFavorites");
        favoriteModel.setFavListFromCarousel(favoriteGroup$);
        expect(spy).toHaveBeenCalled();
    });
});
