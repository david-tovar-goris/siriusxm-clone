/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />
import { of as observableOf } from "rxjs";
import { FavoriteService } from "./favorite.service";
import { FavoriteModel } from "../index";
import { favList, favUpdate} from "../test/mocks/favorites/favorite.response";
import {
    IChannel,
    IOnDemandShow
} from "../index";
import { FavoriteAssetType, FavoriteContentType, FavoriteUpdateItem } from "./favorite.interface";

describe("FavoriteService Test Suite >>", function()
{
    beforeEach(function()
    {
        this.favoriteDelegate = {
            fetchFavorites: jasmine.createSpy('fetchFavs'),
            updateFavorite: jasmine.createSpy('updateFavs'),
            updateFavorites: jasmine.createSpy('updateAllFavs')
        };

        this.mockFavoriteModel = new FavoriteModel();
        this.mockFavoriteModel.setFavorites(favList);
        this.service = new FavoriteService(this.favoriteDelegate, this.mockFavoriteModel);
    });

    describe("Infrastructure >> ", function()
    {
        it("Should have a test subject.", function()
        {
            expect(this.service).toBeDefined();
        });
    });

    describe("Get Favorites >>", function()
    {
        it("getFavorite() ", function()
        {
            expect(this.service.getFavorite("01")).toBe(favList[0]);
            expect(this.service.getFavoriteForChannel("03")).toBe(favList[2]);
            expect(this.service.getFavoriteForChannel("02")).toBeUndefined();
        });
    });

    describe("updateFavorite()", function()
    {
        it("calls updates favorite list on API using favoriteDelegate.updateFavorites - channel", function()
        {
            const channel =
                      {
                          channelId    : "02",
                          type         : "live",
                          channelNumber: 1,
                          channelGuid    : "assetGuid"
                      } as IChannel;
            this.favoriteDelegate.updateFavorite.and.returnValue(observableOf([]));

            const favoriteUpdateItem = new FavoriteUpdateItem(channel.channelId,
                channel.type as FavoriteContentType,
                channel.type as FavoriteAssetType,
                "update",
                channel.channelGuid);
            this.service.updateFavorite(favoriteUpdateItem).subscribe(data =>
            {
                expect(data).toEqual(true);
            });
            expect(this.favoriteDelegate.updateFavorite).toHaveBeenCalled();
        });
        it("calls updates favorite list on API using favoriteDelegate.updateFavorites - show", function()
        {
            const show = { guid: "showGuid", relatedChannelIds: [ "Ch1" ] } as IOnDemandShow;
            this.favoriteDelegate.updateFavorite.and.returnValue(observableOf([]));

            const favoriteUpdateItem = new FavoriteUpdateItem(show.relatedChannelIds[0],
                "show" as FavoriteContentType,
                "show" as FavoriteAssetType,
                "delete",
                show.guid);
            this.service.updateFavorite(favoriteUpdateItem).subscribe(data =>
            {
                expect(data).toEqual(true);
            });
            expect(this.favoriteDelegate.updateFavorite).toHaveBeenCalled();
        });
    });

    describe("updateFavorites()", function()
    {
        it("call updates the full list of favorites using the favoriteDelegate.updateFavorites", function()
        {
            this.favoriteDelegate.updateFavorites.and.returnValue(observableOf(favList));

            this.service.updateFavorites(favUpdate)
                .subscribe((result) =>
                {
                    expect(result).toBeTruthy();
                });
            expect(this.favoriteDelegate.updateFavorites).toHaveBeenCalled();
        });
    });
});
