import { FavoriteServiceUtil } from "./favorite.service.util";
import { FavoriteAssetTypes } from "./favorite.interface";
import {
    favGroupedList
} from "../test/mocks/favorites/favorite.response";

describe("Favorite Service Util Test Suite: ", function()
{
    const favoriteServiceUtil = new FavoriteServiceUtil();

    it("Check the Favorite Tile Type", function()
    {
        expect(FavoriteServiceUtil.isFavoriteTileLive(FavoriteAssetTypes.LIVE)).toBeTruthy();
        expect(FavoriteServiceUtil.isFavoriteTileAodEpisode(FavoriteAssetTypes.EPISODE)).toBeTruthy();
        expect(FavoriteServiceUtil.isFavoriteTileAodShow(FavoriteAssetTypes.SHOW)).toBeTruthy();
    });

    it("Unwrap Grouped favoriteList", function()
    {
        expect(FavoriteServiceUtil.unGroupFavoriteItems(favGroupedList).length).toBe(3);
    });

});
