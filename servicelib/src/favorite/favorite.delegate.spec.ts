/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { of as observableOf } from "rxjs";
import { ServiceEndpointConstants } from "../service/consts/service.endpoint.constants";
import {
    favList,
    favResponse,
    favUpdate
} from "../test/mocks/favorites/favorite.response";
import { FavoriteDelegate } from "./favorite.delegate";
import {
    FavoriteAssetType, FavoriteContentType,
    FavoriteUpdateItem,
    IFavoriteItem
} from "./favorite.interface";

describe("Favorite Delegate Test Suite", function()
{
    const favoriteChannelText: string = "FavoriteChannels";
    const profileText: string = "Profile";
    const likesText: string = "Likes";

    beforeEach(function()
    {
        this.favoriteDelegate = null;
        this.mockHttp = null;
        this.mockFavoriteResponse = null;
        this.mockFavoriteResponse = JSON.parse(JSON.stringify(favResponse));
        this.mockHttp =
            {
                get: (endpoint, request, config) =>
                {
                    return observableOf(this.mockFavoriteResponse);
                },
                postModuleAreaRequest: (endpoint, request, config) =>
                {
                    return observableOf(this.mockFavoriteResponse);
                }
            };

        spyOn(this.mockHttp, "get").and.callThrough();
        spyOn(this.mockHttp, "postModuleAreaRequest").and.callThrough();

        this.favoriteDelegate = new FavoriteDelegate(this.mockHttp);
    });

    describe("Infrastructure >>", function()
    {
        it("Constructor", function()
        {
            expect(this.favoriteDelegate).toBeDefined();
            expect(this.favoriteDelegate instanceof FavoriteDelegate).toEqual(true);
        });
    });

    describe("updateFavorite()", function()
    {
        it("updates the favorite using the preset endpoint by adding a new favorite to the list - IChannel", function()
        {
            this.mockFavoriteResponse.favoritesList.favorites.push(
                {
                    assetGUID: "04",
                    assetName: "assetName",
                    assetType: "live",
                    channelId: "02",
                    contentType: "contentType",
                    episodeCount: 5,
                    showImages: [],
                    showLogos: []
                });

            const favoriteUpdateItem = new FavoriteUpdateItem("02",
                "live" as FavoriteContentType,
                "live" as FavoriteAssetType,
                "add",
                "04");

            this.favoriteDelegate.updateFavorite(favoriteUpdateItem)
                .subscribe((result) =>
                {
                    expect(result).toEqual(this.mockFavoriteResponse.favoritesList.favorites);
                });

            expect(this.mockHttp.postModuleAreaRequest.calls.mostRecent().args)
                .toContain(ServiceEndpointConstants.endpoints.FAVORITES.V4_UPDATE_PRESETS);
        });

        it("updates the favorite using the preset endpoint by adding a new favorite to the list - IChannel", function()
        {
            const addItem: IFavoriteItem = {
                assetGUID: "04",
                assetName: "assetName",
                assetType: "live",
                channelId: "02",
                contentType: "contentType",
                episodeCount: 5,
                showImages: [],
                showLogos: []
            };
            this.mockFavoriteResponse.favoritesList.favorites.push(addItem);

            addItem.guid = addItem.assetGUID;
            addItem.relatedChannelIds = [ addItem.channelId ];

            const favoriteUpdateItem = new FavoriteUpdateItem(addItem.channelId,
                addItem.contentType as FavoriteContentType,
                addItem.assetType as FavoriteAssetType,
                "add",
                addItem.assetGUID);

            this.favoriteDelegate.updateFavorite(favoriteUpdateItem)
                .subscribe((result) =>
                {
                    expect(result).toEqual(this.mockFavoriteResponse.favoritesList.favorites);
                });

            expect(this.mockHttp.postModuleAreaRequest.calls.mostRecent().args)
                .toContain(ServiceEndpointConstants.endpoints.FAVORITES.V4_UPDATE_PRESETS);
        });

        it("updates the favorite using the preset endpoint by removing an item from the list", function()
        {
            const removeItem = favUpdate[ 0 ];
            removeItem.changeType = "delete";

            this.mockFavoriteResponse.favoritesList.favorites.splice(0, 1);

            this.favoriteDelegate.updateFavorite(removeItem)
                .subscribe((result) =>
                {
                    expect(result).toEqual(this.mockFavoriteResponse.favoritesList.favorites);
                });

            expect(this.mockHttp.postModuleAreaRequest.calls.mostRecent().args)
                .toContain(ServiceEndpointConstants.endpoints.FAVORITES.V4_UPDATE_PRESETS);
        });
    });

    describe("updateFavorites()", function()
    {
        it("updates the favorites list using the preset endpoint", function()
        {

            this.favoriteDelegate.updateFavorites(favUpdate)
                .subscribe((result) =>
                {
                    expect(result).toEqual(favList);
                });

            const mock = this.mockHttp.postModuleAreaRequest.calls.mostRecent().args;
            expect(mock).toContain(ServiceEndpointConstants.endpoints.FAVORITES.V4_UPDATE_PRESETS);
        });
    });
});
