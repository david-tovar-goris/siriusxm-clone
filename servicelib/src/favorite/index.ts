/**
 * This is known as a barrel file. It allows developers to package and rollup multiple exports
 * into a single file and thus single import/export; e.g., `export * from "./authentication/service/";`
 */
export * from "./favorite.service";
export * from "./favorite.interface";
export * from "./favorite.delegate";
export * from "./favorite.model";
export * from "./favorite.service.util";
