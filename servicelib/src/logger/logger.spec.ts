import * as _ from "lodash";
import { Logger } from "../index";

describe("Logger Test Suite >>", function()
{
    const args = ["test1", "test2", "test3"];

    beforeEach(function()
               {
                   this.context      = "testContext";
                   this.emptyContext = "";
                   this.testMessage  = "testMessage";
                   this.baseTime              = new Date();

                   this.loggerName   = "testLogger";
                   this.logger       = new Logger(this.context);
                   Logger.level = Logger.LEVEL_ALL;

                   jasmine.clock().install();
                   jasmine.clock().mockDate(this.baseTime);

                   console.info = jasmine.createSpy("info");
               });

    afterEach(function() { jasmine.clock().uninstall(); });

    describe("Infrastructure >> ", function()
    {
        it("Should have a test subject.", function()
        {
            expect(this.logger).toBeDefined();
        });

        it("Should expose the expected public properties.", function()
        {
            expect(Logger.isEnabled).toBeDefined();
            expect(Logger.isEnabled).toEqual(true);

            expect(Logger.LEVEL_ALL).toBeDefined();
            expect(Logger.LEVEL_ALL).toEqual(0);

            expect(Logger.LEVEL_LOG).toBeDefined();
            expect(Logger.LEVEL_LOG).toEqual(2);

            expect(Logger.LEVEL_DEBUG).toBeDefined();
            expect(Logger.LEVEL_DEBUG).toEqual(4);

            expect(Logger.LEVEL_INFO).toBeDefined();
            expect(Logger.LEVEL_INFO).toEqual(6);


            expect(Logger.LEVEL_WARN).toBeDefined();
            expect(Logger.LEVEL_WARN).toEqual(8);


            expect(Logger.LEVEL_ERROR).toBeDefined();
            expect(Logger.LEVEL_ERROR).toEqual(10);

            expect(Logger.LEVEL_FATAL).toBeDefined();
            expect(Logger.LEVEL_FATAL).toEqual(1000);

            expect(Logger.level).toBeDefined();
            expect(Logger.level).toEqual(0);

            expect(this.logger.context).toBeDefined();
            expect(this.logger.context).toEqual(this.context);

        });

        it("Should return context to be empty .", function()
        {
            this.logger = new Logger();

            expect(this.logger.context).toBeDefined();
            expect(this.logger.context).toEqual(this.emptyContext);

        });

    });

    describe("static getLogger  >>", function()
    {
        describe("Infrastructure >>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(Logger.getLogger).toBeDefined();
                expect(_.isFunction(Logger.getLogger)).toBe(true);
            });

            it("Should return a Logger.", function()
            {
                expect(Logger.getLogger(this.loggerName) instanceof Logger).toBe(true);
            });
        });

        describe("Execution >>", function()
        {
            it("Should return a Logger object.", function()
            {
                expect(Logger.getLogger(this.loggerName)).toEqual(new Logger(this.loggerName));
            });

            it("Should return an invalid logger name error .", function()
            {
                try
                {
                    Logger.getLogger(null);
                }
                catch (exception)
                {
                    expect(exception).toEqual(new Error("invalid logger name"));
                }
            });
        });
    });

    describe("instance getLogger  >>", function()
    {
        describe("Infrastructure >>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.logger.getLogger).toBeDefined();
                expect(_.isFunction(this.logger.getLogger)).toBe(true);
            });

            it("Should return a Logger.", function()
            {
                expect(this.logger.getLogger(this.loggerName) instanceof Logger).toBe(true);
            });
        });

        describe("Execution >>", function()
        {
            it("Should return a Logger object.", function()
            {
                expect(this.logger.getLogger(this.loggerName)).toEqual(new Logger(this.loggerName));
            });

            it("Should return an invalid logger name error .", function()
            {
                try
                {
                    this.logger.getLogger(null);
                }
                catch (exception)
                {
                    expect(exception).toEqual(new Error("invalid logger name"));
                }
            });
        });
    });

    describe("getPrintFriendlyLogStatement  >>", function()
    {
        describe("Infrastructure >>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(Logger.getPrintFriendlyLogStatement).toBeDefined();
                expect(_.isFunction(Logger.getPrintFriendlyLogStatement)).toBe(true);
            });
        });

        describe("Execution >>", function()
        {
            it("Should return a log statement when log level is INFO.", function()
            {
                expect(Logger.getPrintFriendlyLogStatement(this.context,
                    Logger.LEVEL_INFO,
                    this.testMessage))
                    .toContain(" [INFO] " + this.context + " - " + this.testMessage);
            });

            it("Should return a log statement when log level is WARN.", function()
            {
                expect(Logger.getPrintFriendlyLogStatement(this.context,
                    Logger.LEVEL_WARN,
                    this.testMessage))
                    .toContain(" [WARN] " + this.context + " - " + this.testMessage);
            });

            it("Should return a log statement when log level is FATAL.", function()
            {
                expect(Logger.getPrintFriendlyLogStatement(this.context,
                    Logger.LEVEL_FATAL,
                    this.testMessage))
                    .toContain(" [ERROR] " + this.context + " - " + this.testMessage);
            });

            it("Should return a log statement when log level is ERROR.", function()
            {
                expect(Logger.getPrintFriendlyLogStatement(this.context,
                    Logger.LEVEL_ERROR,
                    this.testMessage))
                    .toContain(" [ERROR] " + this.context + " - " + this.testMessage);
            });

            it("Should return a log statement when log level is DEBUG.", function()
            {
                expect(Logger.getPrintFriendlyLogStatement(this.context,
                    Logger.LEVEL_DEBUG,
                    this.testMessage))
                    .toContain(" [DEBUG] " + this.context + " - " + this.testMessage);
            });

            it("Should return a log statement when log level is LOG.", function()
            {
                expect(Logger.getPrintFriendlyLogStatement(this.context,
                    Logger.LEVEL_LOG,
                    this.testMessage))
                    .toContain(" [DEBUG] " + this.context + " - " + this.testMessage);
            });
        });
    });

    describe("debug  >>", function()
    {
        describe("Infrastructure >>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.logger.debug).toBeDefined();
                expect(_.isFunction(this.logger.debug)).toBe(true);
            });
        });

        describe("Execution >> ", function()
        {
            beforeEach(function()
            {
                this.consoleLog = console.log.bind(console);
                console.log = jasmine.createSpy("log").and.callFake((text) => { this.consoleLog(text); });
            });

            afterEach(function()
            {
                console.log = this.consoleLog;
            });

            it("Should have been called console.log with no arguments", function()
            {
                this.logger.debug();
                expect(console.log).toHaveBeenCalledWith("[JS] " + Logger.getTimestamp(this.baseTime) + " [DEBUG] "
                    + this.context + " - undefined");
            });

            it("Should have been called console.log with arguments", function()
            {
                this.logger.debug(args);
                    expect(console.log).toHaveBeenCalledWith("[JS] " + Logger.getTimestamp(this.baseTime) + " [DEBUG] "
                    + this.context + " - test1,test2,test3");
            });

            it("Should not have been been called console.log.", function()
            {
                Logger.level = 10000;
                this.logger.debug();
                expect(console.log).not.toHaveBeenCalled();
            });
        });
    });

    describe("info  >>", function()
    {
        describe("Infrastructure >>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.logger.info).toBeDefined();
                expect(_.isFunction(this.logger.info)).toBe(true);
            });
        });

        describe("Execution >> ", function()
        {
            it("Should have been called console.info with no arguments", function()
            {
                this.logger.info();
                expect(console.info).toHaveBeenCalledWith("[JS] " + Logger.getTimestamp(this.baseTime) + " [INFO] "
                    + this.context + " - undefined");
            });

            it("Should have been called console.info with arguments", function()
            {
                this.logger.info(args);
                expect(console.info).toHaveBeenCalledWith("[JS] " + Logger.getTimestamp(this.baseTime) + " [INFO] "
                    + this.context + " - test1,test2,test3");
            });

            it("Should have been not have been called console.info.", function()
            {
                Logger.level = 10000;
                this.logger.info();
                expect(console.info).not.toHaveBeenCalled();
            });

        });
    });

    describe("warn  >>", function()
    {
        describe("Infrastructure >>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.logger.warn).toBeDefined();
                expect(_.isFunction(this.logger.warn)).toBe(true);
            });

        });

        describe("Execution >> ", function()
        {
            beforeEach(function()
            {
                this.warnLog = console.warn.bind(console);
                console.warn = jasmine.createSpy("warn").and.callFake((text) => { this.warnLog(text); });
            });

            afterEach(function()
            {
                console.warn = this.warnLog;
            });

            it("Should have been called console.warn with no arguments", function()
            {
                this.logger.warn();
                expect(console.warn).toHaveBeenCalledWith("[JS] " + Logger.getTimestamp(this.baseTime) + " [WARN] "
                    + this.context + " - undefined");
            });

            it("Should have been called console.warn with arguments", function()
            {
                this.logger.warn(args);
                expect(console.warn).toHaveBeenCalledWith("[JS] " + Logger.getTimestamp(this.baseTime) + " [WARN] "
                    + this.context + " - test1,test2,test3");
            });

            it("Should have been not have been called console.warn.", function()
            {
                Logger.level = 10000;
                this.logger.warn();
                expect(console.warn).not.toHaveBeenCalled();
            });
        });
    });

    describe("error  >>", function()
    {
        describe("Infrastructure >>", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.logger.error).toBeDefined();
                expect(_.isFunction(this.logger.error)).toBe(true);
            });
        });
        xdescribe("Execution >> ", function()
        {
            beforeEach(function()
            {
                this.errorLog = console.error.bind(console);
                console.error = jasmine.createSpy("error").and.callFake((text) => { this.errorLog(text); });
            });

            afterEach(function()
            {
                console.error = this.errorLog;
            });

            it("Should have been called console.error with no arguments", function()
            {
                this.logger.error();
                expect(console.error).toHaveBeenCalledWith("[JS] " + Logger.getTimestamp(this.baseTime) + " [ERROR] "
                    + this.context + " - undefined");
            });

            it("Should have been called console.error with arguments", function()
            {
                this.logger.error(args);
                expect(console.error).toHaveBeenCalledWith("[JS] " + Logger.getTimestamp(this.baseTime) + " [ERROR] "
                    + this.context + " - test1,test2,test3");
            });

            it("Should have been not have been called console.error.", function()
            {
                Logger.level = 10000;
                this.logger.error();
                expect(console.error).not.toHaveBeenCalled();
            });
        });
    });
});
