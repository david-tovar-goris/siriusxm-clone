/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import * as _ from "lodash";
import { Observable, of as observableOf } from "rxjs";
import { mock } from "ts-mockito";
import { mockRecentlyPlayedData } from "../test/mocks/recently-played/recently-played.mock";
import { RecentlyPlayedDelegate } from "./recently-played.delegate";
import {
    IHttpRequestConfig,
    ServiceEndpointConstants
} from "../index";
import { RecentlyPlayedConsts } from "../service/consts/api.request.consts";

describe("Recently Played Delegate Test Suite", function()
{
    const recentAssetGUID = "testRecentAssetGuid";
    const recentPlayType = "live";

    beforeEach(function()
    {
        this.discoverRecentlyPlayedResponse = _.cloneDeep(mockRecentlyPlayedData);
        this.mockHttp =
            {
                get: function (url: string)
                {
                    return (url.indexOf(ServiceEndpointConstants.endpoints.RECENTLY_PLAYED.V4_GET_RECENTLY_PLAYED) > -1) ?
                        observableOf(this.discoverRecentlyPlayedResponse) : observableOf([]);
                },
                postModuleAreaRequests: function (url: string)
                {
                    return (url.indexOf(ServiceEndpointConstants.endpoints.RECENTLY_PLAYED.V4_REMOVE_RECENTLY_PLAYED) > -1) ?
                        observableOf(this.discoverRecentlyPlayedResponse) : observableOf([]);
                }
            };

        this.recentlyPlayedDelegate = new RecentlyPlayedDelegate(this.mockHttp);
    });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.recentlyPlayedDelegate).toBeDefined();
            expect(this.recentlyPlayedDelegate instanceof RecentlyPlayedDelegate).toEqual(true);
        });
    });

    describe("Get RecentlyPlayed >> ", function()
    {
        it("delete Single Recently played Item:", function()
        {
            let recentlyPlayedId = "testRecentId";

            spyOn(this.mockHttp, "postModuleAreaRequests").and
                .callFake((url, data: any, config: IHttpRequestConfig) =>
                {
                    expect(url)
                        .toBe(ServiceEndpointConstants.endpoints.RECENTLY_PLAYED.V4_REMOVE_RECENTLY_PLAYED);
                    expect(Array.isArray(data)).toEqual(true);
                    expect(data[ 0 ].moduleArea).toEqual(RecentlyPlayedConsts.MODULE_AREA);
                    expect(data[ 0 ].moduleType).toEqual(RecentlyPlayedConsts.DELETE_MODULE_TYPE);

                    let request = data[ 0 ].moduleRequest;
                    const recentPlay = request.profileUpdateRequest.gupRecentPlaysUpdateRequests[0].recentPlay;
                    expect(recentPlay.recentPlayId).toEqual(recentlyPlayedId);
                    expect(recentPlay.assetGUID).toEqual(recentAssetGUID);
                    expect(recentPlay.recentPlayType).toEqual(recentPlayType);

                    expect(config).toEqual(null);

                    return observableOf(this.discoverRecentlyPlayedResponse);
                });

            this.recentlyPlayedDelegate.removeRecentlyPlayedItem(recentlyPlayedId, recentAssetGUID, recentPlayType)
                                  .subscribe(response =>
                                  {
                                  });
            expect(this.mockHttp.postModuleAreaRequests).toHaveBeenCalled();
        });

        it("delete Single Recently played Item: - return empty recently played data", function()
        {
             const mockRecentlyPlayedData = {
                recentlyPlayedData: {
                }
            };
            let recentlyPlayedId = "testRecentId";

            spyOn(this.mockHttp, "postModuleAreaRequests").and
                                                     .callFake((url, data: any, config: IHttpRequestConfig) =>
                                                     {
                                                          return observableOf(mockRecentlyPlayedData);
                                                     });

            this.recentlyPlayedDelegate.removeRecentlyPlayedItem(recentlyPlayedId, recentAssetGUID, recentPlayType)
                                  .subscribe(response =>
                                  {
                                      expect(response).toEqual(mockRecentlyPlayedData);
                                  });
            expect(this.mockHttp.postModuleAreaRequests).toHaveBeenCalled();
        });

        it("delete all Recently played Items:", function()
        {
            spyOn(this.mockHttp, "postModuleAreaRequests").and
                .callFake((url, data: any, config: IHttpRequestConfig) =>
                {
                    expect(url)
                        .toBe(ServiceEndpointConstants.endpoints.RECENTLY_PLAYED.V4_REMOVE_RECENTLY_PLAYED);
                    expect(Array.isArray(data)).toEqual(true);
                    expect(data[ 0 ].moduleArea).toEqual(RecentlyPlayedConsts.MODULE_AREA);
                    expect(data[ 0 ].moduleType).toEqual(RecentlyPlayedConsts.DELETE_MODULE_TYPE);
                    let request = data[ 0 ].moduleRequest;
                    const mockRequest = {
                        profileUpdateRequest: {
                            gupRecentPlaysUpdateRequests: [
                                {
                                    recentPlay: RecentlyPlayedConsts.REMOVE_ALL_REQUEST_PAYLOAD
                                }
                            ]
                        }
                    };

                    expect(request).toEqual(mockRequest);

                    expect(config).toEqual(null);
                    return observableOf(this.discoverRecentlyPlayedResponse);
                });

            this.recentlyPlayedDelegate.removeAllRecentlyPlayed().subscribe(response =>
            {
            });
            expect(this.mockHttp.postModuleAreaRequests).toHaveBeenCalled();
        });
   });
});
