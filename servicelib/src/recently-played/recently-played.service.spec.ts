/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />


import {throwError as observableThrowError,  of as observableOf } from 'rxjs';
import { RecentlyPlayedService } from "./recently-played.service";

describe("Recently Played Service Test Suite", function()
{
    const failureMessage = "FailMessage";
    const recentlyPlayedId = "recentPlayId";
    const recentAssetGUID = "testRecentAssetGuid";
    const recentPlayType = "live";

    beforeEach(function()
    {
         this.mockRecentlyPlayedDelegate = {
            discoverRecentlyPlayed: () =>
            {
                return observableOf(this.discoverRecentlyPlayedResponse);
            },
            removeRecentlyPlayedItem: () =>
            {
                return observableOf(this.discoverRecentlyPlayedResponse);
            },
            removeAllRecentlyPlayed: () =>
            {
                return observableOf(this.discoverRecentlyPlayedResponse);
            }
        };

        this.recentlyPlayedService = new RecentlyPlayedService(this.mockRecentlyPlayedDelegate );

    });

    afterEach(function()
    {
        jasmine.clock().uninstall();
    });

    describe("Infrastructure >> ", function()
    {
        it("Constructor", function()
        {
            expect(this.recentlyPlayedService).toBeDefined();
            expect(this.recentlyPlayedService instanceof RecentlyPlayedService).toEqual(true);
        });

    });

    describe("Remove Recently Played Item >> ", function()
    {
        it("Remove a single recently played item>> on Success", function()
        {
            spyOn(this.mockRecentlyPlayedDelegate, "removeRecentlyPlayedItem").and
                .callFake(() =>
                {
                    return observableOf(this.discoverRecentlyPlayedResponse);
                });

            this.recentlyPlayedService.removeRecentlyPlayedItem(recentlyPlayedId, recentAssetGUID, recentPlayType)
                                 .subscribe(data =>
                                 {
                                     expect(data).toEqual(true);
                                 });
            expect(this.mockRecentlyPlayedDelegate.removeRecentlyPlayedItem).toHaveBeenCalled();
        });

        it("Remove a single recently played item>> on Error", function()
        {
            spyOn(this.mockRecentlyPlayedDelegate, "removeRecentlyPlayedItem").and
                .callFake(() =>
                {
                    return observableThrowError(failureMessage);
                });

            this.recentlyPlayedService.removeRecentlyPlayedItem(recentlyPlayedId, recentAssetGUID, recentPlayType)
                                 .subscribe(response =>
                                 {
                                     expect(response).toEqual(false);
                                 });
        });
    });
    describe("Remove All Recently Played Items >> ", function()
    {
        it("Remove all recently played items >> on Success", function()
        {
            spyOn(this.mockRecentlyPlayedDelegate, "removeAllRecentlyPlayed").and
                .callFake(() =>
                {
                    return observableOf(this.discoverRecentlyPlayedResponse);
                });

            this.recentlyPlayedService.removeAllRecentlyPlayed().subscribe(data =>
            {
                expect(data).toEqual(true);
            });

            expect(this.mockRecentlyPlayedDelegate.removeAllRecentlyPlayed).toHaveBeenCalled();
        });

        it("Remove all recently played items>> on Error", function()
        {
             spyOn(this.mockRecentlyPlayedDelegate, "removeAllRecentlyPlayed").and
                .callFake(() =>
                {
                    return observableThrowError(failureMessage);
                });

            this.recentlyPlayedService.removeAllRecentlyPlayed().subscribe(response =>
            {
                expect(response).toEqual(false);
            });
        });
    });
});
