import { IShowImage } from "../index";
import { IChannel } from "../channellineup/channels.interfaces";
import { IOnDemandEpisode, IOnDemandShow } from "../channellineup/ondemand.interfaces";
import { IAodEpisode } from "../channellineup/audio.ondemand.interfaces";
import { IVodEpisode } from "../channellineup/video.ondemand.interfaces";

export interface IRecentlyPlayed
{
    aodDownload: boolean;
    aodPercentConsumed: number;
    assetGuid: string;
    assetType: string;
    channelGuid: string;
    deviceGuid: string;
    endDateTime: Date;
    endStreamDateTime?: Date;
    endStreamTime?: Date;
    gupId: string;
    incognito: boolean;
    recentPlayType: string;
    recentlyPlayedGuid: string;
    startDateTime: Date;
    startStreamDateTime: Date;
    startStreamTime?: Date;
    showTitle?: string;
    episodeTitle?: string;
    shortEpisodeDescription?: string;
    images?: Array<IShowImage>;
    channel? : IChannel;
    aodEpisode? : IAodEpisode;
    vodEpisode? : IVodEpisode;
}
