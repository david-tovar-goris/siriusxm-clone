export class HttpHeaderEvents
{
    public static NOTIFICATION_RECEIVED = "ModalEvent.NOTIFICATION_EVENT";
    public static CONTENT_ALERT = "ContentAlertEvent.CONTENT_ALERT_EVENT";
    public static LOAD_CROSS_DEVICE_PAUSEPOINT = "SettingsEvent.LOAD_CROSS_DEVICE_PAUSE_POINT";
    public static CHANNEL_LINEUP_CHANGE = "ChannelEvent.LINEUP_CHANGE";
}
