/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />
import axios from "axios";
import * as _ from "lodash";

import { mockServiceConfig } from "../test/mocks/service.config";
import { MockPromise } from "../test/mocks/promise";
import {
    MockRequestInterceptor,
    MockResponseInterceptor
} from "../test/mocks/interceptors";

import { MockStorageService } from "../test/mocks/storage.service";

import { HttpProvider } from "./http.provider";
import { RequestInterceptor } from "./http.provider.request.interceptor";
import {
    ResponseInterceptor,
    IHttpHeader
} from "./http.provider.response.interceptor";
import {
    ModuleList,
    ModuleRequest,
    ModuleListRequest,
    ModuleAreaRequest,
    ModuleListAreaRequest
} from "./types/module.list.request";

import
{
    IAppConfig,
    ApiCodes,
    HttpCodes,
    providers
}                                   from "../index";
import { ServiceEndpointConstants } from "../service/consts";
import { IHttpRequestConfig }       from "./types/http.types";
import { BehaviorSubject, Observable } from "rxjs";

describe( "HttpProvider Test Suite >>", function()
{
    const baseTime                = new Date();

    beforeEach( function()
                {
                    jasmine.clock().install();
                    jasmine.clock().mockDate(baseTime);

                    this.requestSpy = spyOn( axios.interceptors.request, "use" );
                    spyOn( axios.interceptors.response, "use" );

                    this.id = "1234567";

                    this.promise = new MockPromise();

                    this.requestInterceptor = new MockRequestInterceptor();
                    this.responseInterceptor = new MockResponseInterceptor();
                    this.storageService = new MockStorageService();

                    this.appConfig = JSON.parse(JSON.stringify(mockServiceConfig));

                    this.authModelMock =
                        {
                            authenticating$: new BehaviorSubject(false) as Observable<any>
                        };

                    this.service = new HttpProvider(this.requestInterceptor, this.responseInterceptor, this.storageService,
                        this.appConfig, this.authModelMock);
                } );

    afterEach(function() { jasmine.clock().uninstall(); });

    describe( "Infrastructure >> ", function()
    {
        beforeEach( function()
                    {
                        spyOn( axios, "get" ).and.callFake( () =>
                                                            {
                                                                return this.promise;
                                                            } );
                        spyOn( axios, "post" ).and.callFake( () =>
                                                             {
                                                                 return this.promise;
                                                             } );
                    } );

        it( "Should have a test subject", function()
        {
            expect( this.service ).toBeDefined();
        } );

        it( "Should set the deviceID for SERVICE_CONFIG from local storage if present", function()
        {
            expect( this.appConfig.deviceInfo.clientDeviceId ).toEqual( this.storageService.id );
        } );

        it( "Should not set the deviceID for SERVICE_CONFIG from local storage if not present", function()
        {
            this.storageService.id = undefined;

            this.appConfig.deviceInfo.clientDeviceId = "id";

            this.service = new HttpProvider(this.requestInterceptor, this.responseInterceptor, this.storageService,
                this.appConfig, this.authModelMock);

            expect( this.appConfig.deviceInfo.clientDeviceId ).toEqual( "id" );
        } );

        it( "Should register request and response interceptors with axios", function()
        {
            expect( axios.interceptors.request.use ).toHaveBeenCalledWith( this.requestInterceptor.onRequest );
            expect( axios.interceptors.response.use ).toHaveBeenCalledWith( this.responseInterceptor.onResponse );
        } );

        it( "Should forward gets to axois", function()
        {
            const config = {};
            const url    = "test";

            this.service.get( url, null, config ).subscribe((response) =>
            {
                expect(axios.get).toHaveBeenCalledWith(url, config, undefined);
            });
        } );

        it( "Should forward posts to axois", function()
        {
            const config = {};
            const data   = {};
            const url    = "test";

            this.service.post(url, data, config).subscribe((response) =>
            {
                expect(axios.post).toHaveBeenCalledWith(url, data, config);
            });
        });

        it( "Should provide a descriptor object to describe dependency injection needed for object construction", function()
        {
            const provider = providers.find(provider =>
            {
                return (provider.provide === HttpProvider && provider.useClass === HttpProvider);
            });

            expect(provider).toBeDefined();
            expect(provider.deps[ 0 ]).toBe(RequestInterceptor);
            expect(provider.deps[ 1 ]).toBe(ResponseInterceptor);
        } );
    } );

    describe( "Get call >> ", function()
    {
        it( "Should add add a payload object to the request if provided", function()
        {
            const config = {};
            const data   = { property : "test " };
            const url    = "test";

            spyOn( axios, "get" ).and.callFake( ( u, c ) =>
                                                {
                                                    expect( u ).toBe( url );
                                                    expect( c.data instanceof ModuleList ).toBe( true );
                                                    expect( c.data.moduleList.modules[ 0 ] ).toBe( data );

                                                    return this.promise;
                                                } );

            this.service.get( url, data, config ).subscribe();
        } );

        it( "Should add add a payload array to the request if provided", function()
        {
            const config = {};
            const data   = [ { property : "test " }, { property2 : "test2" } ];
            const url    = "test";

            spyOn( axios, "get" ).and.callFake( ( u, c ) =>
                                                {
                                                    expect( u ).toBe( url );
                                                    expect( c.data instanceof ModuleList ).toBe( true );
                                                    expect( c.data.moduleList.modules ).toBe( data );

                                                    return this.promise;
                                                } );

            this.service.get( url, data, config ).subscribe();
        } );
    } );

    describe( "postModuleRequest call >> ", function()
    {
        it( "Should add a ModuleRequest to the outgoing post call with the config set properly", function()
        {
            const request = new ModuleRequest();
            const url     = "test";

            spyOn( request, "setConfig" ).and.callThrough();
            spyOn( axios, "post" ).and.callFake( ( u, d, c ) =>
                                                 {
                                                     expect( u ).toBe( url );
                                                     expect( d instanceof ModuleListRequest ).toBe( true );
                                                     expect( d.moduleList.modules[ 0 ].moduleRequest )
                                                         .toEqual( request );
                                                     expect( c ).not.toBeDefined();

                                                     return this.promise;
                                                 } );

            this.service.postModuleRequest( url, request ).subscribe(() =>
            {
                expect(axios.post).toHaveBeenCalled();
                expect( request.setConfig ).toHaveBeenCalledWith( this.appConfig );
                expect( request.resultTemplate ).toBe( this.appConfig.resultTemplate );
                expect( request.deviceInfo ).toBe( this.appConfig.deviceInfo );
            });
        } );
    } );

    describe( "postModuleAreaRequest call >> ", function()
    {
        it( "Should add a ModuleAreaRequest to the outgoing post call with the config set properly", function()
        {
            const request = new ModuleAreaRequest( "area", "type", {} );
            const url     = "test";

            spyOn( axios, "post" ).and.callFake( ( u, d, c ) =>
                                                 {
                                                     expect( u ).toBe( url );
                                                     expect( d instanceof ModuleListAreaRequest ).toBe( true );
                                                     expect( d.moduleList.modules[ 0 ] ).toEqual( request );
                                                     expect( c ).not.toBeDefined();

                                                     return this.promise;
                                                 } );

            this.service.postModuleAreaRequest( url, request ).subscribe(() =>
            {
                expect(axios.post).toHaveBeenCalled();
            });
        } );
    } );

    describe( "postModuleAreaRequests call >> ", function()
    {
        it( "Should add multiple ModuleAreaRequest to the outgoing post call with the config set properly", function()
        {
            const requests = [ new ModuleAreaRequest( "area", "type", {} ), new ModuleAreaRequest( "area", "type", {} ) ];
            const url      = "test";

            spyOn( axios, "post" ).and.callFake( ( u, d, c ) =>
                                                 {
                                                     expect( u ).toBe( url );
                                                     expect( d instanceof ModuleListAreaRequest ).toBe( true );
                                                     expect( d.moduleList.modules[ 0 ] ).toEqual( requests[ 0 ] );
                                                     expect( d.moduleList.modules[ 1 ] ).toEqual( requests[ 1 ] );
                                                     expect( c ).not.toBeDefined();

                                                     return this.promise;
                                                 } );

            this.service.postModuleAreaRequests( url, requests ).subscribe(() =>
            {
                expect(axios.post).toHaveBeenCalled();
            });
        } );
    } );

    describe( "http header subscription management call >> ", function()
    {
        it ( "Should observe specific http headers and trigger their observable when those headers are encountered",function()
        {
            const httpHeader = { name : "testHeader", payload : "testPayload "} as IHttpHeader;
            const obs = this.service.addHttpHeaderObservable("testHeader");
            const onHeader = jasmine.createSpy("onHeader");

            obs.subscribe(onHeader);
            this.responseInterceptor.httpHeaders.next(httpHeader);
            this.responseInterceptor.httpHeaders.next(httpHeader);

            expect(onHeader).toHaveBeenCalledWith(httpHeader);

            expect(onHeader.calls.count()).toBe(3);
        });

        it ( "multiple http header observables should be independent from each other",function()
        {
            const httpHeader1 = { name : "testHeader1", payload : "testPayload1"} as IHttpHeader;
            const httpHeader2 = { name : "testHeader2", payload : "testPayload2"} as IHttpHeader;
            const onHeader1 = jasmine.createSpy("onHeader1");
            const onHeader2 = jasmine.createSpy("onHeader2");

            this.service.addHttpHeaderObservable("testHeader1").subscribe(onHeader1);
            this.service.addHttpHeaderObservable("testHeader2").subscribe(onHeader2);

            this.responseInterceptor.httpHeaders.next(httpHeader1);
            this.responseInterceptor.httpHeaders.next(httpHeader2);

            expect(onHeader1).toHaveBeenCalledWith(httpHeader1);
            expect(onHeader2).toHaveBeenCalledWith(httpHeader2);
            expect(onHeader1).not.toHaveBeenCalledWith(httpHeader2);
            expect(onHeader2).not.toHaveBeenCalledWith(httpHeader1);
        });


        it ( "Should should stop observing http headers when the observables are removed",function()
        {
            const httpHeader = { name : "testHeader", payload : "testPayload "} as IHttpHeader;
            const obs = this.service.addHttpHeaderObservable("testHeader");
            const onHeader = jasmine.createSpy("onHeader");

            obs.subscribe(onHeader);
            this.responseInterceptor.httpHeaders.next(httpHeader);

            expect(onHeader).toHaveBeenCalledWith(httpHeader);

            this.service.removeHttpHeaderObservable("testHeader");

            this.responseInterceptor.httpHeaders.next(httpHeader);

            expect(onHeader.calls.count()).toBe(2);
        });

        it ( "removing one http header observable should not effect any other http header observables",function()
        {
            const httpHeader1 = { name : "testHeader1", payload : "testPayload1"} as IHttpHeader;
            const httpHeader2 = { name : "testHeader2", payload : "testPayload2"} as IHttpHeader;
            const onHeader1 = jasmine.createSpy("onHeader1");
            const onHeader2 = jasmine.createSpy("onHeader2");

            this.service.addHttpHeaderObservable("testHeader1").subscribe(onHeader1);
            this.service.addHttpHeaderObservable("testHeader2").subscribe(onHeader2);

            this.service.removeHttpHeaderObservable("testHeader1");

            this.responseInterceptor.httpHeaders.next(httpHeader1);
            this.responseInterceptor.httpHeaders.next(httpHeader2);

            expect(onHeader1).not.toHaveBeenCalledWith(httpHeader1);
            expect(onHeader2).toHaveBeenCalledWith(httpHeader2);
            expect(onHeader1).not.toHaveBeenCalledWith(httpHeader2);
            expect(onHeader2).not.toHaveBeenCalledWith(httpHeader1);
        });
    });

    describe( "axiosHttp call >> ", function()
    {
        it ( "Should set the session to valid if invalid and the call is successful",function()
        {
            const config = {};
            const data   = { property : "test " };
            const url    = "test";

            spyOn( axios, "get" ).and.callFake( ( u, c ) =>
                                                {
                                                    expect( u ).toBe( url );
                                                    expect( c.data instanceof ModuleList ).toBe( true );
                                                    expect( c.data.moduleList.modules[ 0 ] ).toEqual( data );

                                                    return this.promise;
                                                } );

            this.service.get( url, data, config ).subscribe();

            const spyObserve = jasmine.createSpy("spyObserve");

            const subscription = this.service.sessionValid.subscribe(spyObserve);

            expect(spyObserve).toHaveBeenCalledWith(true);

            subscription.unsubscribe();
        });

        it ( "Should set the session to become invalid if valid and the call returns a SESSION_RESUME error code"
             + " then perform a retry when the session is recovered", function()
        {
            const config       = {};
            const data         = {property : "test "};
            let url            = "test";
            let resumeCallMade = false;

            this.promise = Promise.reject({ code : ApiCodes.SESSION_RESUME });

            const getSpy = jasmine.createSpy("get").and.callFake(( u, c ) =>
                                                                 {
                                                                     expect( u ).toBe( url );
                                                                     expect( c.data instanceof ModuleList ).toBe( true );
                                                                     expect( c.data.moduleList.modules[ 0 ] ).toEqual( data );
                                                                     return this.promise;
                                                                 } );

            spyOn( axios, "get" ).and.callFake( ( u, c ) =>
                                                {
                                                    return getSpy(u,c);
                                                });

            this.service.get(url, data, config).subscribe(() =>
            {
                /* Now there should be 4 calls
                 * 1) Original call the resulted in session becoming invalid
                 * 2) Resume to recover session
                 * 3) Retry of first call that failed after session has been recovered
                 * 4) This call that was held off until session recovered
                 */
                expect(getSpy.calls.count()).toBe(4);
            });

            expect(getSpy.calls.count()).toBe(1);

            const spyObserve = jasmine.createSpy("spyObserve").and.callFake((sessionValid : boolean) =>
            {
                if (sessionValid === false)
                {
                    this.promise = Promise.resolve({code : ApiCodes.API_SUCCESS_CODE});

                    this.service.get(url, data, config).subscribe(() =>
                    {
                        /* Now there should be 4 calls
                         * 1) Original call the resulted in session becoming invalid
                         * 2) Resume to recover session
                         * 3) Retry of first call that failed after session has been recovered
                         * 4) This call that was held off until session recovered
                         */
                        expect(getSpy.calls.count()).toBe(4);
                    });

                    url = ServiceEndpointConstants.endpoints.RESUME.V4_GET_RESUME;

                    this.service.get(url, data, config).subscribe();
                    resumeCallMade = true;

                    // There should be a total of 2 get calls.  One from the first service.get call, a second from the
                    // resume.  The second call to the original url above should be held off until the session is
                    // recovered
                    expect(getSpy.calls.count()).toBe(2);
                }
                else if (resumeCallMade === true)
                {
                    /*
                     * Original call that invalidated the session was made, and the Resume has recovered the session,
                     * so we are back to checking urls against the "test" requests to make sure we have made any
                     * requests that were held off and that we retry the original failed request
                     */
                    url = "test";
                }
            });

            this.service.sessionValid.subscribe(spyObserve);
        });

        it ( "Should perform one retry if call returns Axois HTTP Error BAD_GATEWAY error code and verify retry count is 1", function()
        {
            const data   = { params : {dummyParam:"test "} };
            const url    = "test";

            const originalMakeHttpCall = HttpProvider.makeHttpCall;

            spyOn(HttpProvider,"makeHttpCall").and
                                              .callFake((type : string,
                                                         url : string,
                                                         data? : any,
                                                         config? : IHttpRequestConfig,
                                                         retry? : number) =>
            {
                if (retry > 0)
                {
                    axios.interceptors.request.use((config : IHttpRequestConfig) =>
                    {
                        const configParams = (config) ? config.params : undefined;
                        const dataParams = (data) ? data.params : undefined;

                        if (url === config.url && _.isEqual(configParams, dataParams))
                        {
                            config.retryCount = retry;
                        }
                        return config;
                    });
                }

                return originalMakeHttpCall(type, url, data, config, retry);
            });

            this.promise =
                {
                    then  : jasmine.createSpy( "then" )
                                   .and.callFake( () => this.promise),
                    catch : jasmine.createSpy( "catch" )
                                   .and.callFake( (handler) =>
                        {
                            handler({
                                        response : {
                                            status : HttpCodes.BAD_GATEWAY,
                                            config : { url : url }
                                        }
                            });
                            return this.promise;
                        })
                };

            const getSpy = jasmine.createSpy("get").and.callFake(( u, c ) =>
            {
                expect( u ).toBe( url );
                expect(c.params).toEqual(data.params);
                return this.promise;
            } );

            spyOn( axios, "get" ).and.callFake( ( u, c ) =>
            {
                return getSpy(u,c);
            });

            const useSpy = jasmine.createSpy("use").and.callFake((c) =>
            {
                expect(c.url).toEqual(url);
                expect(c.retryCount).toBeGreaterThan(0);
                expect(c.params).toEqual(data.params);
                return this.promise;
            });

            this.requestSpy.and.callFake((fn) =>
            {
                useSpy(fn({params:data.params,url:url}));
            });

            try
            {
                this.service.get( url, null , data ).subscribe();
            }
            catch (error)
            {
                expect(error.code).toEqual(HttpCodes.BAD_GATEWAY);
            }

            expect(getSpy.calls.count()).toBe(1);

            this.promise =
                {
                    then  : jasmine.createSpy( "then" )
                                   .and.callFake( (handler) =>
                        {
                            handler();
                            return this.promise;
                        } ),
                    catch : jasmine.createSpy( "catch" ).and.callFake( () => this.promise )
                };

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);
        });

        it ( "Should perform one retry if call returns Axois HTTP Error BAD_GATEWAY error code", function()
        {
            const config = {};
            const data   = { property : "test " };
            const url    = "test";

            this.promise =
            {
                then  : jasmine.createSpy( "then" )
                               .and.callFake( () => this.promise),
                catch : jasmine.createSpy( "catch" )
                               .and.callFake( (handler) =>
                                              {
                                                  handler({
                                                              response : {
                                                                  status : HttpCodes.BAD_GATEWAY,
                                                                  config : { url : url }
                                                              }
                                                          });
                                                  return this.promise;
                                              })
            };

            const getSpy = jasmine.createSpy("get").and.callFake(( u, c ) =>
                                                                 {
                                                                     expect( u ).toBe( url );
                                                                     expect( c.data instanceof ModuleList ).toBe( true );
                                                                     expect( c.data.moduleList.modules[ 0 ] ).toEqual( data );
                                                                     return this.promise;
                                                                 } );

            spyOn( axios, "get" ).and.callFake( ( u, c ) =>
                                                {
                                                    return getSpy(u,c);
                                                });


            this.service.get( url, data, config ).subscribe();

            expect(getSpy.calls.count()).toBe(1);

            this.promise =
            {
                then  : jasmine.createSpy( "then" )
                               .and.callFake( (handler) =>
                                              {
                                                  handler();
                                                  return this.promise;
                                              } ),
                catch : jasmine.createSpy( "catch" ).and.callFake( () => this.promise )
            };

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);
        });

        it ( "Should perform one retry if call returns Axois HTTP Error Network TIMEOUT error code", function()
        {
            const config = {};
            const data   = { property : "test " };
            const url    = "test";

            this.promise =
            {
                then  : jasmine.createSpy( "then" )
                               .and.callFake( () => this.promise),
                catch : jasmine.createSpy( "catch" )
                               .and.callFake( (handler) =>
                                              {
                                                  handler({ response : {
                                                          status : undefined,
                                                          config : { url : url }
                                                      } });
                                                  return this.promise;
                                              })
            };

            const getSpy = jasmine.createSpy("get").and.callFake(( u, c ) =>
                                                                 {
                                                                     expect( u ).toBe( url );
                                                                     expect( c.data instanceof ModuleList ).toBe( true );
                                                                     expect( c.data.moduleList.modules[ 0 ] ).toEqual( data );
                                                                     return this.promise;
                                                                 } );

            spyOn( axios, "get" ).and.callFake( ( u, c ) =>
                                                {
                                                    return getSpy(u,c);
                                                });


            this.service.get( url, data, config ).subscribe();

            expect(getSpy.calls.count()).toBe(1);

            this.promise =
            {
                then  : jasmine.createSpy( "then" )
                               .and.callFake( (handler) =>
                                              {
                                                  handler();
                                                  return this.promise;
                                              } ),
                catch : jasmine.createSpy( "catch" ).and.callFake( () => this.promise )
            };

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);
        });

        it ( "Should perform one retry if call returns Axios HTTP Error with no info, and treat as a NETWORK_TIMEOUT", function()
        {
            const config = {};
            const data   = { property : "test " };
            const url    = "test";

            this.promise =
            {
                then  : jasmine.createSpy( "then" )
                               .and.callFake( () => this.promise),
                catch : jasmine.createSpy( "catch" )
                               .and.callFake( (handler) =>
                                              {
                                                  handler({ response : {
                                                          status : undefined,
                                                          config : { url : url }
                                                      } });
                                                  return this.promise;
                                              })
            };

            const getSpy = jasmine.createSpy("get").and.callFake(( u, c ) =>
                                                                 {
                                                                     expect( u ).toBe( url );
                                                                     expect( c.data instanceof ModuleList ).toBe( true );
                                                                     expect( c.data.moduleList.modules[ 0 ] ).toEqual( data );
                                                                     return this.promise;
                                                                 } );

            spyOn( axios, "get" ).and.callFake( ( u, c ) =>
                                                {
                                                    return getSpy(u,c);
                                                });

            this.service.get( url, data, config ).subscribe();

            expect(getSpy.calls.count()).toBe(1);

            this.promise =
            {
                then  : jasmine.createSpy( "then" )
                               .and.callFake( (handler) =>
                                              {
                                                  handler();
                                                  return this.promise;
                                              } ),
                catch : jasmine.createSpy( "catch" ).and.callFake( () => this.promise )
            };

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);
        });

        it ( "Should perform one retry if call returns BAD_GATEWAY error code", function()
        {
            const config = {};
            const data   = { property : "test " };
            const url    = "test";

            this.promise =
            {
                then  : jasmine.createSpy( "then" )
                               .and.callFake( () => this.promise),
                catch : jasmine.createSpy( "catch" )
                               .and.callFake( (handler) =>
                                              {
                                                  handler({ code : ApiCodes.INVALID_REQUEST });
                                                  return this.promise;
                                              })
            };

            const getSpy = jasmine.createSpy("get").and.callFake(( u, c ) =>
            {
                expect( u ).toBe( url );
                expect( c.data instanceof ModuleList ).toBe( true );
                expect( c.data.moduleList.modules[ 0 ] ).toEqual( data );
                return this.promise;
            } );

            spyOn( axios, "get" ).and.callFake( ( u, c ) =>
                                                {
                                                    return getSpy(u,c);
                                                });


            this.service.get( url, data, config ).subscribe();

            expect(getSpy.calls.count()).toBe(1);

            this.promise =
            {
                then  : jasmine.createSpy( "then" )
                               .and.callFake( (handler) =>
                                              {
                                                  handler();
                                                  return this.promise;
                                              } ),
                catch : jasmine.createSpy( "catch" ).and.callFake( () => this.promise )
            };

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);
        });

        it ( "Should perform only one retry if call returns BAD_GATEWAY error code, and retry fails too", function()
        {
            const config = {};
            const data   = { property : "test " };
            const url    = "test";

            this.promise =
            {
                then  : jasmine.createSpy( "then" )
                               .and.callFake( () => this.promise),
                catch : jasmine.createSpy( "catch" )
                               .and.callFake( (handler) =>
                                              {
                                                  const err = { message: 'HTTP status error', code: HttpCodes.BAD_GATEWAY };

                                                  try
                                                  {
                                                      handler({ response: {
                                                          status: HttpCodes.BAD_GATEWAY,
                                                          config : { url : url }
                                                      }});
                                                  }
                                                  catch(error)
                                                  {
                                                      expect(error.code).toEqual(HttpCodes.BAD_GATEWAY);
                                                      expect(error.url).toEqual(url);
                                                  }
                                                  return this.promise;
                                              })
            };

            const getSpy = jasmine.createSpy("get").and.callFake(( u, c ) =>
                                                                 {
                                                                     expect( u ).toBe( url );
                                                                     expect( c.data instanceof ModuleList ).toBe( true );
                                                                     expect( c.data.moduleList.modules[ 0 ] ).toEqual( data );
                                                                     return this.promise;
                                                                 } );

            spyOn( axios, "get" ).and.callFake( ( u, c ) =>
                                                {
                                                    return getSpy(u,c);
                                                });

            this.service.get( url, data, config ).subscribe();

            expect(getSpy.calls.count()).toBe(1);

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);

            jasmine.clock().tick(HttpProvider.RETRY_WAIT_TIME_MS + 1);

            expect(getSpy.calls.count()).toBe(2);
        });
    });
} );
