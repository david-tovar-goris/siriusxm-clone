/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import {
    ApiLayerTypes,
    EventBus,
    HttpUtilities,
    IAppConfig,
    providers,
    StorageService
} from "../index";
import { ApiDelegate } from "./api.delegate";
import { HttpDelegate } from "./http.delegate";
import {
    ICdnAccessToken,
    ResponseInterceptor
} from "./http.provider.response.interceptor";
import { IHttpResponse } from "./types/http.types";
import { MockAppConfig } from "../test/mocks/app.config.mock";

describe("ResponseInterceptor Test Suite >>", function()
{
    beforeEach(function()
    {
        spyOn(HttpDelegate, "checkHTTPResponse");
        spyOn(HttpDelegate, "checkHTTPHeaders").and.callThrough();

        this.apiDelegate = {
            checkApiResponse: jasmine.createSpy("checkApiResponse"),
            getResponseData: jasmine.createSpy("getResponseData").and.returnValue("response")
        };

        this.response = {
            data: {
                ModuleListResponse: {
                    messages: [ { code: 100, message: "Success" } ],
                    moduleList: { modules: [ { moduleResponse: { value: "response" } } ] }
                }
            },
            status: 200,
            statusText: "OK",
            headers: {},
            config: { url: "test" }
        };

        this.config = new MockAppConfig();
        this.storageService = new StorageService();
        spyOn(this.storageService, "setItem");

        this.eventBus = new EventBus();
        this.service = new ResponseInterceptor(this.apiDelegate, this.storageService, this.eventBus, this.config);
    });

    describe("Infrastructure >> ", function()
    {
        it("Should have a test subject", function()
        {
            expect(this.service).toBeDefined();
        });

        it("Should provide a descriptor object to describe dependency injection needed for object construction", function()
        {
            const provider = providers.find(provider =>
            {
                return (provider.provide === ResponseInterceptor && provider.useClass === ResponseInterceptor);
            });

            expect(provider).toBeDefined();
            expect(provider.deps[ 0 ]).toBe(ApiDelegate);
            expect(provider.deps[ 1 ]).toBe(StorageService);
            expect(provider.deps[ 2 ]).toBe(EventBus);
        });

    });

    describe("Response interception", function()
    {
        it("onResponse should use HttpDelegate and API delegate to handle response", function()
        {
            this.response.headers['testHeader'] = 'testPayload';
            const headerSpy = jasmine.createSpy("headerSpy");
            this.service.httpHeaders.subscribe(headerSpy);

            const retvalue = this.service.onResponse(this.response);

            expect(HttpDelegate.checkHTTPResponse).toHaveBeenCalledWith(this.response);
            expect(HttpDelegate.checkHTTPHeaders).toHaveBeenCalledWith(this.response, this.service);

            expect(headerSpy).toHaveBeenCalledWith({ name : 'testHeader',payload : 'testPayload'});
            expect(retvalue).toEqual(this.response.data.ModuleListResponse.moduleList.modules[ 0 ].moduleResponse);
        });

        it("onResponse should set deviceId in SERVICE_CONFIG if it is present", function()
        {
            const id = "1234567";

            this.response.data.ModuleListResponse.moduleList.modules[ 0 ].clientConfiguration = { clientDeviceId: id };

            this.service.onResponse(this.response);

            expect(this.storageService.setItem).toHaveBeenCalledWith(ApiLayerTypes.CLIENT_DEVICE_ID, id);
            expect(this.config.deviceInfo.clientDeviceId).toEqual(id);
        });

        it("onResponse should set clientConfiguration in SERVICE_CONFIG if it is present", function()
        {
            const id = "1234567";

            this.response.data.ModuleListResponse.moduleList.modules[ 0 ].clientConfiguration = { clientDeviceId: id };

            this.service.onResponse(this.response);

            expect(this.config.clientConfiguration)
                .toEqual(this.response.data.ModuleListResponse.moduleList.modules[ 0 ].clientConfiguration);
        });

        it("onResponse should set allProfileData in SERVICE_CONFIG if it is present", function()
        {
            this.response.data.ModuleListResponse.moduleList.modules[ 0 ].getAllProfilesData = { whatever: "dude" };

            this.service.onResponse(this.response);

            expect(this.config.allProfilesData)
                .toEqual(this.response.data.ModuleListResponse.moduleList.modules[ 0 ].getAllProfilesData);
        });

        it("onResponse should set deviceId in SERVICE_CONFIG if it is present in the last of multiple responses", function()
        {
            const id = "1234567";

            this.response.data.ModuleListResponse.moduleList.modules
                .push({
                    moduleResponse: { value: "response" },
                    clientConfiguration: { clientDeviceId: id }
                });

            this.service.onResponse(this.response);

            expect(this.storageService.setItem).toHaveBeenCalledWith(ApiLayerTypes.CLIENT_DEVICE_ID, id);
            expect(this.config.deviceInfo.clientDeviceId).toEqual(id);
        });

        it("onResponse should set cdn Access token", function()
        {
            const akamaiCookieValue = "akamaCookie";
            const limeLightCookieValue = "akamaCookie";
            spyOn(HttpUtilities, "getCookieBody").and.callFake(
                (cookieName) =>
                {
                    if (cookieName === "SXMAKTOKEN")
                    {
                        return akamaiCookieValue;
                    }
                    return limeLightCookieValue;
                }
            );

            this.service.onResponse(this.response);

            this.service.cdnAccessTokens.subscribe((response: ICdnAccessToken) =>
            {
                expect(response.akamai).toEqual(akamaiCookieValue);
                expect(response.limeLight).toEqual(limeLightCookieValue);
            });

            this.service.onResponse(this.response);

            this.service.cdnAccessTokens.subscribe((response: ICdnAccessToken) =>
            {
                expect(response.akamai).toEqual(akamaiCookieValue);
                expect(response.limeLight).toEqual(limeLightCookieValue);
            });
        });
    });
});
