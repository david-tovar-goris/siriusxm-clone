/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { IHttpResponse } from "./types/http.types";
import {
    HttpDelegate,
    IHttpHeaderDelegate
} from "./http.delegate";
import { MockResponseInterceptor } from "../test/mocks/interceptors";

describe('Http delegate for test suite >>', function()
{
    const responseInterceptor = new MockResponseInterceptor as IHttpHeaderDelegate;

    describe('Infrastructure >>>', function()
    {
        it('should have static methods defined', function()
        {
            expect(HttpDelegate.checkHTTPResponse).toBeDefined();
            expect(HttpDelegate.checkHTTPHeaders).toBeDefined();
        });
    });

    describe('CheckHttpResponse() >>>', function()
    {
        let response: any = {};
        describe('Failure execution >>>', function()
        {
            beforeEach(function()
            {
                response.message = "HTTP status error";
                response.status = 400;
                response.config  = { url : "test" };
            });

            it('should throw error', function()
            {
                try
                {
                    HttpDelegate.checkHTTPResponse(response);
                }
                catch (exception)
                {
                    expect(exception.message).toEqual(response.message);
                    expect(exception.code).toEqual(response.status);
                    expect(exception.url).toEqual(response.config.url);
                }
            });
        });

        describe('Success execution >>>', function()
        {
            beforeEach(function()
            {
                response.status = 200;
                response.config  = { url : "test" };
            });

            it('should throw error', function()
            {
                expect(() =>
                {
                    HttpDelegate.checkHTTPResponse(response);
                }).not.toThrowError();
            });
        });

    });

    describe('checkHttpHeaders >>>', function()
    {
        describe('Success execution >>>', function()
        {
            let response: IHttpResponse = {
                data: [''],
                status: 200,
                statusText: 'success',
                headers: {},
                config: {}
            };

            describe('Infrastructure >>>', function()
            {
                beforeEach(() =>
                {
                    HttpDelegate.checkHTTPHeaders(response, responseInterceptor);
                });

                it('should have response headers undefined...', function()
                {
                    expect(response.headers).toBeDefined();
                });
            });

            describe('Execution of x-mountain-notification >>>', function()
            {
                it('should check http headers...', function()
                {
                    const headerName = 'x-mountain-notification';
                    response.headers = {};
                    response.headers[headerName] = "test";
                    HttpDelegate.checkHTTPHeaders(response,responseInterceptor);
                    expect(responseInterceptor.handleHttpHeader).toHaveBeenCalledWith(headerName,
                                                                                      response.headers[headerName]);
                });
            });

            describe('Execution of x-siriusxm-notification >>>', function()
            {
                it('should check http headers...', function()
                {
                    const headerName = 'x-siriusxm-notification';
                    response.headers = {};
                    response.headers[headerName] = "test";
                    HttpDelegate.checkHTTPHeaders(response,responseInterceptor);
                    expect(responseInterceptor.handleHttpHeader).toHaveBeenCalledWith(headerName,
                                                                                      response.headers[headerName]);
                });
            });

            describe('Execution of crossdevicepausepointtime >>>', function()
            {
                it('should check http headers...', function()
                {
                    const headerName = 'crossdevicepausepointtime';
                    response.headers = {};
                    response.headers[headerName] = "test";
                    HttpDelegate.checkHTTPHeaders(response,responseInterceptor);
                    expect(responseInterceptor.handleHttpHeader).toHaveBeenCalledWith(headerName,
                                                                                      response.headers[headerName]);
                });
            });

            describe('Execution of channelchangerefreshflag >>>', function()
            {
                it('should check http headers...', function()
                {
                    const headerName ='channelchangerefreshflag';
                    response.headers = {};
                    response.headers[headerName] = "test";
                    HttpDelegate.checkHTTPHeaders(response,responseInterceptor);
                    expect(responseInterceptor.handleHttpHeader).toHaveBeenCalledWith(headerName,
                                                                                      response.headers[headerName]);
                });
            });
        });

        describe('Failure execution >>>', function()
        {
            let response: any = {
                headers: undefined
            };
            beforeEach(function()
            {
                HttpDelegate.checkHTTPHeaders(response, responseInterceptor);
            });

            it('should have response headers undefined', function()
            {
                expect(response.headers).toBeUndefined();
            });
        });
    });

});
