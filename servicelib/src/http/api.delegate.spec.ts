import * as _ from "lodash";
import createSpy = jasmine.createSpy;

import { ApiDelegate, ApiCodeHandler } from "./api.delegate";
import {
    IApiMessage,
    IModuleListResponse
} from "./types/module.list.response";

import { IQueryStringParam } from "./types/query.string.param.interface";

import {
    IEvent,
    IHttpResponse,
    ApiCodes,
    EventBus
} from "../index";

describe("API Delegate Test Suite", function()
{
    beforeEach(function()
    {
        this.moduleResponse1 = {
            id: "reponseId1"
        };

        this.moduleResponse2 = {
            id: "reponseId2"
        };

        this.clientConfiguration1 = {
            id: "configId1"
        };

        this.clientConfiguration2 = {
            id: "configId1"
        };

        this.module1 = {
            moduleArea: "testArea1",
            moduleType: "testType1",
            moduleResponse:   this.moduleResponse1
        };

        this.module2 = {
            moduleArea: "testArea2",
            moduleType: "testType2",
            moduleResponse:   this.moduleResponse2,
            clientConfiguration:   this.clientConfiguration2
        };

        this.message101 = {
            code: ApiCodes.AOD_BYPASS,
            message: "TestMessage1"
        };

        this.message102 = {
            code: ApiCodes.GUP_BYPASS,
            message: "TestMessage2"
        };

        this.apiMessages = [
            this.message101,
            this.message102,
            { code : ApiCodes.API_SUCCESS_CODE, message : "OK" }
        ];

        this.httpResponse =
            {
                data:
                    {
                        ModuleListResponse:
                            {
                                messages:   this.apiMessages,
                                moduleList: {
                                    modules:
                                        {}
                                }
                            }
                    },
                config: {
                    url: "url"
                }
            };

        this.eventBus = new EventBus();
        this.delegate = new ApiDelegate( this.eventBus);
    });

    describe(" getMessages  >>>", function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(ApiDelegate.getMessages).toBeDefined();
                expect(_.isFunction(ApiDelegate.getMessages)).toBe(true);
            });

            it("Should return an array .", function()
            {
                expect(ApiDelegate.getMessages(  this.httpResponse) instanceof Array).toBe(true);
            });
        });

        describe('Execution >>>', function()
        {
            it("Should return messages object from data.ModuleListResponse.messages.", function()
            {
                expect(ApiDelegate.getMessages(this.httpResponse)).toEqual(this.httpResponse.data.ModuleListResponse.messages);
            });

            it("Should return empty array for malformed response.", function()
            {
                this.httpResponse.data.ModuleListResponse1 = this.httpResponse.data.ModuleListResponse;
                this.httpResponse.data.ModuleListResponse = undefined;

                expect(this.httpResponse.data.ModuleListResponse).toBeUndefined();
                expect(ApiDelegate.getMessages(this.httpResponse)).toEqual([ApiDelegate.MALFORMED_API_RESPONSE]);
            });
        });
    });

    describe(" addApiCodeHandler  >>>", function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.delegate.addApiCodeHandler).toBeDefined();
                expect(_.isFunction(this.delegate.addApiCodeHandler)).toBe(true);
            });
        });

        describe('Execution >>>', function()
        {

            let singleCodeHandler,
                multiCodeHandler;

            beforeEach(() =>
            {
                singleCodeHandler = jasmine.createSpy("singleCodeHandler"),
                multiCodeHandler = jasmine.createSpy("multiCodeHandler");

                singleCodeHandler.calls.reset();
                multiCodeHandler.calls.reset();
            });

            describe("when a single code is registered", function()
            {
                it("should add the handler to the error table and call the handler when a message message with that code is received", function()
                {
                    this.delegate.addApiCodeHandler(ApiCodes.GUP_BYPASS, singleCodeHandler);
                    this.delegate.checkApiResponse(this.httpResponse);

                    expect(singleCodeHandler).toHaveBeenCalled();
                });
            });

            describe("when multiple codes are registered", function()
            {
                it("should add the handler to the error table and call the handler when all registered codes are recieved within the same response",
                function()
                {
                    this.delegate.addApiCodeHandler(
                        [ ApiCodes.AOD_BYPASS, ApiCodes.GUP_BYPASS, ApiCodes.API_SUCCESS_CODE ],
                        multiCodeHandler
                    );

                    this.delegate.checkApiResponse(this.httpResponse);

                    expect(multiCodeHandler).toHaveBeenCalled();
                });
            });
        });
    });

    describe(" checkApiResponse  >>>", function()
    {
        describe("Infrastructure >> ", function()
        {
            afterEach(function()
                      {
                          this.httpResponse.data.ModuleListResponse.messages[2].code = ApiCodes.API_SUCCESS_CODE;
                      });

            it("Should have method defined on the test subject.", function()
            {
                expect(this.delegate.checkApiResponse).toBeDefined();
                expect(_.isFunction(this.delegate.checkApiResponse)).toBe(true);
            });

            it("Should throw an exception if there is no api code 100 present", function()
            {
                this.httpResponse.data.ModuleListResponse.messages[2].code = ApiCodes.GATEWAY_TIMEOUT;

                try
                {
                    this.delegate.checkApiResponse( this.httpResponse );
                }
                catch(error)
                {
                    expect( error.message ).toBe( this.apiMessages[ 0 ].message );
                }
            });

            it("Should not throw an exception if there is an api code 100 present, followed by another non 100 code", function()
            {
                this.httpResponse.data.ModuleListResponse.messages[3] = { code : ApiCodes.AOD_EXPIRED_CONTENT, message : "expired" };

                try
                {
                    this.delegate.checkApiResponse( this.httpResponse );
                }
                catch(error)
                {
                    expect( error.message ).toBe( this.apiMessages[ 0 ].message );
                }
            });
        });
    });

    describe(" addApiCodeEventBroadcast  >>>", function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.delegate.addApiCodeEventBroadcast).toBeDefined();
                expect(_.isFunction(this.delegate.addApiCodeEventBroadcast)).toBe(true);
            });
        });

        describe('Execution >>>', function()
        {
            it("Should dispatch event", function()
            {
                const message: string = "TestMessage1";
                const event: IEvent = {
                    type: message
                };

                const dispatch = jasmine.createSpy("dispatch");

                this.eventBus.dispatch = dispatch;

                let token = this.delegate.addApiCodeEventBroadcast(this.message101.code, event.type);
                this.delegate.checkApiResponse( this.httpResponse );

                expect(this.eventBus.dispatch).toHaveBeenCalledWith(event);

                dispatch.calls.reset();

                token.removeCodeHandler();

                expect(this.eventBus.dispatch).not.toHaveBeenCalledWith(event);
            });
        });
    });

    describe(" getResponseData  >>>", function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(ApiDelegate.getResponseData).toBeDefined();
                expect(_.isFunction(ApiDelegate.getResponseData)).toBe(true);
            });
        });

        describe('Execution >>>', function()
        {
            it("Should return error message when path not existed", function()
            {
                try
                {
                    ApiDelegate.getResponseData(this.httpResponse);
                }
                catch (exception)
                {
                    expect(exception.message)
                        .toEqual(this.httpResponse.data.ModuleListResponse.messages[0].message);
                }

            });

            it("Should return moduleResponse when moduleResoponse array > 1", function()
            {
                this.httpResponse.data.ModuleListResponse.moduleList.modules = [this.module1, this.module2];

                const expectedObject = [
                    {
                        moduleArea : this.module1.moduleArea,
                        moduleType : this.module1.moduleType,
                        id: this.moduleResponse1.id
                    },
                    {
                        moduleArea : this.module2.moduleArea,
                        moduleType : this.module2.moduleType,
                        id: this.moduleResponse2.id,
                        clientConfiguration : this.module2.clientConfiguration
                    }
                ];
                expect(ApiDelegate.getResponseData(this.httpResponse)).toEqual(expectedObject);
            });

            it("Should return clientConfiguration ", function()
            {
                let modules: Array<any> = [{
                    moduleArea: "testArea",
                    moduleType: "testType",
                    moduleResponse : { clientConfiguration : { id: "configId" }}
                }];

                this.httpResponse.data.ModuleListResponse.moduleList.modules = modules;

                let data = ApiDelegate.getResponseData(this.httpResponse);

                expect(data.clientConfiguration).toEqual(modules[0].moduleResponse.clientConfiguration);
                expect(data.id).toBeUndefined();
            });

            it("Should return moduleResponse and clientConfiguration ", function()
            {
                this.httpResponse.data.ModuleListResponse.moduleList.modules = [this.module2];
                let data = ApiDelegate.getResponseData(this.httpResponse);

                expect(data.clientConfiguration).toEqual(this.module2.clientConfiguration);
                expect(data.id).toEqual(this.moduleResponse2.id);
            });

            it("Should return moduleResponse when moduleResponse array >= 1", function()
            {
                this.httpResponse.data.ModuleListResponse.moduleList.modules = [this.module2];

                let data = ApiDelegate.getResponseData(this.httpResponse);
                expect(data.clientConfiguration).toBeDefined();
                expect(data.id).toEqual(this.moduleResponse2.id);
            });
        });
    });

    describe(" replaceQueryStringParams  >>>", function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(ApiDelegate.replaceQueryStringParams).toBeDefined();
                expect(_.isFunction(ApiDelegate.replaceQueryStringParams)).toBe(true);
            });

        });

        describe('Execution >>>', function()
        {
            it("Should return Url without parameters.", function()
            {
                expect(ApiDelegate.replaceQueryStringParams("TestUrl")).toEqual("TestUrl");
            });

            it("Should return Url with parameters.", function()
            {
                const queryStringParamsList: Array<IQueryStringParam> = [
                    {
                        name: "Param1",
                        value: "TestParamValue"
                    },
                    {
                        name: "Param2",
                        value: true
                    },
                    {
                        name: "Param3",
                        value: 10
                    }
                ];

                expect(ApiDelegate.replaceQueryStringParams("TestUrl/{Param1}/{Param2}/{Param3}",
                    queryStringParamsList))
                    .toEqual("TestUrl/TestParamValue/true/10");
            });
        });
    });
});
