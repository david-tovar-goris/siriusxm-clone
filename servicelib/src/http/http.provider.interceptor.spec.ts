import 'jasmine-ajax';

import {
    trapXHR,
    untrapXHR
} from "./http.provider.interceptor";

describe( "XHR Trap Test Suite >>", function()
{
    const requestUrl = "test";

    beforeEach( function()
    {
        this.success = {
            status: 200,
            contentType: 'text/plain',
            responseText: "Oh yeah!"
        };

        this.afterOpenNeverRun = jasmine.createSpy("afterOpenNeverRun");
        this.onSendNeverRun = jasmine.createSpy("onSendNeverRun");
        this.onAfterSendNeverRun = jasmine.createSpy("onAfterSendNeverRun");
        this.onResponseNeverRun = jasmine.createSpy("onResponseNeverRun");
        jasmine.Ajax.install();

        trapXHR("neverrun",
                (url : string) => url.valueOf() == "neverrun",
            this.afterOpenNeverRun,
            this.onSendNeverRun,
            this.onAfterSendNeverRun,
            this.onResponseNeverRun);

        this.afterOpen = jasmine.createSpy("afterOpen")
                           .and
                           .callFake((request : XMLHttpRequest,
                                      method : string,
                                      url : string,
                                      flag : boolean) : void =>
                                     {

                                         expect(url).toEqual(requestUrl);
                                     });

        this.onSend = jasmine.createSpy("onSend")
                        .and
                        .callFake((request : XMLHttpRequest,
                                   method : string,
                                   url : string,
                                   flag : boolean) : boolean =>
                                  {
                                      expect(url).toEqual(requestUrl);
                                      return true;
                                  });

        this.onAfterSend = jasmine.createSpy("onAfterSend")
                             .and
                             .callFake((request : XMLHttpRequest,
                                        method : string,
                                        url : string,
                                        flag : boolean) : void =>
                                       {

                                           expect(url).toEqual(requestUrl);
                                       });

        this.onResponse = jasmine.createSpy("onResponse")
                            .and
                            .callFake((request : XMLHttpRequest,
                                       method : string,
                                       url : string,
                                       ev : Event) : Event =>
                                      {
                                          expect(url).toEqual(requestUrl);
                                          expect(request.responseText).toEqual(this.success.responseText);

                                          (request as any).responseText = "intercepted";

                                          return ev;
                                      });
    });

    afterEach( function()
    {
        try
        {
            untrapXHR("neverrun");
            untrapXHR("test");
        }
        catch(e) {}

        jasmine.Ajax.uninstall();
    });

    it( "should trigger our trap once", function()
    {
        trapXHR("test",
                (url : string) => url.valueOf() == "test",
            this.afterOpen,
            this.onSend,
            this.onAfterSend,
            this.onResponse);

        const xhr = new XMLHttpRequest();

        xhr.open("GET",requestUrl, true);

        const onload = jasmine.createSpy("onResponse")
                              .and
                              .callFake(()  =>
                              {
                                  expect(xhr.responseText).toEqual("intercepted");
                              });

        xhr.onload = onload;

        xhr.send();

        this.request = jasmine.Ajax.requests.mostRecent();
        this.request.respondWith(this.success);

        expect(this.afterOpen.calls.count()).toBe(1);
        expect(this.onSend.calls.count()).toBe(1);
        expect(this.onAfterSend.calls.count()).toBe(1);
        expect(this.onResponse.calls.count()).toBe(1);
        expect(this.afterOpenNeverRun.calls.count()).toBe(0);
        expect(this.onSendNeverRun.calls.count()).toBe(0);
        expect(this.onAfterSendNeverRun.calls.count()).toBe(0);
        expect(this.onResponseNeverRun.calls.count()).toBe(0);
        expect(onload.calls.count()).toBe(1);
        expect(xhr.responseText).toEqual("intercepted");
    } );

       it ( "removing a trap that does not exist should throw an exception", function()
    {
        const exceptionHandler = jasmine.createSpy("exceptionHandler");

        trapXHR("test",
                (url : string) => url.valueOf() == "test",
            this.afterOpen,
            this.onSend,
            this.onAfterSend,
            this.onResponse);

        try
        {
            untrapXHR("badtrap");
        }
        catch (e)
        {
            exceptionHandler();
        }

        expect(exceptionHandler.calls.count()).toBe(1);
    });

    it ( "registering a trap twice should should throw an exception", function()
    {
        const exceptionHandler = jasmine.createSpy("exceptionHandler");

        trapXHR("test",
                (url : string) => url.valueOf() == "test",
            this.afterOpen,
            this.onSend,
            this.onAfterSend,
            this.onResponse);

        try
        {
            trapXHR("test",
                    (url : string) => url.valueOf() == "test",
                    null,
                    null,
                    null,
                    null);
        }
        catch (e)
        {
            exceptionHandler();
        }

        expect(exceptionHandler.calls.count()).toBe(1);
    });


    it ( "should not trigger the trap when the trap is removed", function()
    {
        const xhr = new XMLHttpRequest();

        xhr.open("GET",requestUrl, true);

        const onload = jasmine.createSpy("onResponse")
                              .and
                              .callFake(()  =>
                                        {
                                            expect(xhr.responseText).toEqual(this.success.responseText);
                                        });

        xhr.onload = onload;

        xhr.send();

        this.request = jasmine.Ajax.requests.mostRecent();
        this.request.respondWith(this.success);

        expect(this.afterOpen.calls.count()).toBe(0);
        expect(this.onSend.calls.count()).toBe(0);
        expect(this.onAfterSend.calls.count()).toBe(0);
        expect(this.onResponse.calls.count()).toBe(0);
        expect(this.afterOpenNeverRun.calls.count()).toBe(0);
        expect(this.onSendNeverRun.calls.count()).toBe(0);
        expect(this.onAfterSendNeverRun.calls.count()).toBe(0);
        expect(this.onResponseNeverRun.calls.count()).toBe(0);
        expect(onload.calls.count()).toBe(1);
        expect(xhr.responseText).toEqual(this.success.responseText);
    });

    it ( "afterOpen should be optional", function()
    {
        trapXHR("test",
                (url : string) => url.valueOf() == "test",
                null,
            this.onSend,
            this.onAfterSend,
            this.onResponse);

        const xhr = new XMLHttpRequest();

        xhr.open("GET",requestUrl, true);
        const onload = jasmine.createSpy("onResponse")
                              .and
                              .callFake(()  =>
                                        {
                                            expect(xhr.responseText).toEqual("intercepted");
                                        });

        xhr.onload = onload;

        xhr.send();

        this.request = jasmine.Ajax.requests.mostRecent();
        this.request.respondWith(this.success);

        expect(this.afterOpen.calls.count()).toBe(0);
        expect(this.onSend.calls.count()).toBe(1);
        expect(this.onAfterSend.calls.count()).toBe(1);
        expect(this.onResponse.calls.count()).toBe(1);
        expect(this.afterOpenNeverRun.calls.count()).toBe(0);
        expect(this.onSendNeverRun.calls.count()).toBe(0);
        expect(this.onAfterSendNeverRun.calls.count()).toBe(0);
        expect(this.onResponseNeverRun.calls.count()).toBe(0);
        expect(onload.calls.count()).toBe(1);
        expect(xhr.responseText).toEqual("intercepted");
    });

    it ( "onSend should be optional", function()
    {
        trapXHR("test",
                (url : string) => url.valueOf() == "test",
            this.afterOpen,
                null,
            this.onAfterSend,
            this.onResponse);

        const xhr = new XMLHttpRequest();

        xhr.open("GET",requestUrl, true);
        const onload = jasmine.createSpy("onResponse")
                              .and
                              .callFake(()  =>
                                        {
                                            expect(xhr.responseText).toEqual("intercepted");
                                        });

        xhr.onload = onload;

        xhr.send();

        this.request = jasmine.Ajax.requests.mostRecent();
        this.request.respondWith(this.success);

        expect(this.afterOpen.calls.count()).toBe(1);
        expect(this.onSend.calls.count()).toBe(0);
        expect(this.onAfterSend.calls.count()).toBe(1);
        expect(this.onResponse.calls.count()).toBe(1);
        expect(this.afterOpenNeverRun.calls.count()).toBe(0);
        expect(this.onSendNeverRun.calls.count()).toBe(0);
        expect(this.onAfterSendNeverRun.calls.count()).toBe(0);
        expect(this.onResponseNeverRun.calls.count()).toBe(0);
        expect(onload.calls.count()).toBe(1);
        expect(xhr.responseText).toEqual("intercepted");
    });

    it ( "onAfterSend should be optional", function()
    {
        trapXHR("test",
                (url : string) => url.valueOf() == "test",
            this.afterOpen,
            this.onSend,
                null,
            this.onResponse);

        const xhr = new XMLHttpRequest();

        xhr.open("GET",requestUrl, true);
        const onload = jasmine.createSpy("onResponse")
                              .and
                              .callFake(()  =>
                                        {
                                            expect(xhr.responseText).toEqual("intercepted");
                                        });

        xhr.onload = onload;

        xhr.send();

        this.request = jasmine.Ajax.requests.mostRecent();
        this.request.respondWith(this.success);

        expect(this.afterOpen.calls.count()).toBe(1);
        expect(this.onSend.calls.count()).toBe(1);
        expect(this.onAfterSend.calls.count()).toBe(0);
        expect(this.onResponse.calls.count()).toBe(1);
        expect(this.afterOpenNeverRun.calls.count()).toBe(0);
        expect(this.onSendNeverRun.calls.count()).toBe(0);
        expect(this.onAfterSendNeverRun.calls.count()).toBe(0);
        expect(this.onResponseNeverRun.calls.count()).toBe(0);
        expect(onload.calls.count()).toBe(1);
        expect(xhr.responseText).toEqual("intercepted");
    });

    it ( "onResponse should be optional", function()
    {
        trapXHR("test",
                (url : string) => url.valueOf() == "test",
            this.afterOpen,
            this.onSend,
            this.onAfterSend,
                null);

        const xhr = new XMLHttpRequest();

        xhr.open("GET",requestUrl, true);
        const onload = jasmine.createSpy("onResponse")
                              .and
                              .callFake(()  =>
                                        {
                                            expect(xhr.responseText).toEqual(this.success.responseText);
                                        });

        xhr.onload = onload;

        xhr.send();

        this.request = jasmine.Ajax.requests.mostRecent();
        this.request.respondWith(this.success);

        expect(this.afterOpen.calls.count()).toBe(1);
        expect(this.onSend.calls.count()).toBe(1);
        expect(this.onAfterSend.calls.count()).toBe(1);
        expect(this.onResponse.calls.count()).toBe(0);
        expect(this.afterOpenNeverRun.calls.count()).toBe(0);
        expect(this.onSendNeverRun.calls.count()).toBe(0);
        expect(this.onAfterSendNeverRun.calls.count()).toBe(0);
        expect(this.onResponseNeverRun.calls.count()).toBe(0);
        expect(onload.calls.count()).toBe(1);
        expect(xhr.responseText).toEqual(this.success.responseText);
    });

    it ( "trap should not trigger for a url the trap is not set up for", function()
    {
        trapXHR("test",
                (url : string) => url.valueOf() == "test",
            this.afterOpen,
            this.onSend,
            this.onAfterSend,
                null);

        const xhr = new XMLHttpRequest();

        xhr.open("GET","nottrapped", true);
        const onload = jasmine.createSpy("onResponse")
                              .and
                              .callFake(()  =>
                                        {
                                            expect(xhr.responseText).toEqual(this.success.responseText);
                                        });

        xhr.onload = onload;

        xhr.send();

        this.request = jasmine.Ajax.requests.mostRecent();
        this.request.respondWith(this.success);

        expect(this.afterOpen.calls.count()).toBe(0);
        expect(this.onSend.calls.count()).toBe(0);
        expect(this.onAfterSend.calls.count()).toBe(0);
        expect(this.onResponse.calls.count()).toBe(0);
        expect(this.afterOpenNeverRun.calls.count()).toBe(0);
        expect(this.onSendNeverRun.calls.count()).toBe(0);
        expect(this.onAfterSendNeverRun.calls.count()).toBe(0);
        expect(this.onResponseNeverRun.calls.count()).toBe(0);
        expect(onload.calls.count()).toBe(1);
        expect(xhr.responseText).toEqual(this.success.responseText);
    });

    it ( "trap should can prevent request from being sent", function()
    {
        trapXHR("test",
                (url : string) => url.valueOf() == "test",
            this.afterOpen,
                () => false,
            this.onAfterSend,
                null);

        const xhr = new XMLHttpRequest();

        xhr.open("GET",requestUrl, true);
        const onload = jasmine.createSpy("onResponse");

        xhr.onload = onload;

        xhr.send();

        this.request = jasmine.Ajax.requests.mostRecent();
        this.request.respondWith(this.success);

        expect(this.afterOpen.calls.count()).toBe(1);

        expect(this.onAfterSend.calls.count()).toBe(0);
        expect(this.onResponse.calls.count()).toBe(0);
        expect(this.afterOpenNeverRun.calls.count()).toBe(0);
        expect(this.onSendNeverRun.calls.count()).toBe(0);
        expect(this.onAfterSendNeverRun.calls.count()).toBe(0);
        expect(this.onResponseNeverRun.calls.count()).toBe(0);
        expect(onload.calls.count()).toBe(0);
        expect(xhr.responseText).toEqual(this.success.responseText);
    });


});
