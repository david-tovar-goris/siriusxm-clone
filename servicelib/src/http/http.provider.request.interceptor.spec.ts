import { EmptyRequest } from "../test";

import { RequestInterceptor } from "./http.provider.request.interceptor";
import {
    providers,
    StorageService
} from "../index";
import { IHttpRequestConfig } from "./types/http.types";

import { IAppConfig } from "../index";
import { MockAppConfig } from "../test/mocks/app.config.mock";

describe( "RequestInterceptor Test Suite >>", function()
{
    beforeEach( function()
                {
                    this.requestConfig = {
                        url               : '/user',
                        method            : '',
                        baseURL           : undefined,
                        responseType      : 'json',
                        transformRequest  : [],
                        transformResponse : [],
                        headers           : { 'X-Requested-With' : 'XMLHttpRequest' },
                        timeout           : 1000,
                        withCredentials   : false,
                        maxContentLength  : 2000,
                        maxRedirects      : 5,
                        data              : new EmptyRequest().data
                    };

                    this.config = new MockAppConfig();
                    this.storageService = new StorageService();

                    this.config.restart = jasmine.createSpy("restart");

                    this.service = new RequestInterceptor(  this.config,  this.storageService );
                } );

    describe( "Infrastructure >> ", function()
    {
        it( "should have a test subject", function()
        {
            expect(  this.service ).toBeDefined();
        } );

        it( "Should provide a descriptor object to describe dependency injection needed for object construction", function()
        {
            const provider = providers.find(provider =>
            {
                return (provider.provide === RequestInterceptor && provider.useClass === RequestInterceptor);
            });

            expect(provider).toBeDefined();
        } );

    } );

    describe( "OnRequest call >> ", function()
    {
        it( "should set config.baseUrl to the api endpoint from the config object", function()
        {
            this.requestConfig.data.ModuleList.modules = null;

            this.service.onRequest(  this.requestConfig );
            expect(  this.requestConfig.baseURL ).toBe(  this.config.apiEndpoint );
        } );

        it( "should set a cache bust parameter for the outgoing call with no params", function()
        {
            jasmine.clock().install();
            var baseTime = new Date();
            jasmine.clock().mockDate( baseTime );

            this.service.onRequest(  this.requestConfig );
            expect(  this.requestConfig.params ).toBeDefined();
            expect(  this.requestConfig.params.cacheBuster ).toBe( baseTime.getTime() );

            jasmine.clock().uninstall();
        } );

        it( "should add a cache bust parameter for the outgoing call with existing params", function()
        {
            jasmine.clock().install();
            var baseTime = new Date();
            jasmine.clock().mockDate( baseTime );

            this.requestConfig.params = {
                existingParam : true
            };

            this.service.onRequest(  this.requestConfig );
            expect(  this.requestConfig.params.existingParam ).toBe( true );
            expect(  this.requestConfig.params.cacheBuster ).toBe( baseTime.getTime() );

            jasmine.clock().uninstall();
        } );

        it( "should ensure responseType is set to json", function()
        {
            this.requestConfig.responseType = undefined;

            this.service.onRequest(  this.requestConfig );
            expect(  this.requestConfig.responseType ).toBe( "json" );
        } );

        it( "should set the result-template url parameter with the value from SERVICE_CONFIG", function()
        {
            this.requestConfig.params = { "result-template" : "" };

            this.service.onRequest(  this.requestConfig );
            expect(  this.requestConfig.params[ "result-template" ] ).toBe(  this.config.resultTemplate );
        } );

        it( "should set not the result-template url parameter if the parameter is not an empty string", function()
        {
            this.requestConfig.params = { "result-template" : "value" };

            this.service.onRequest(  this.requestConfig );
            expect(  this.requestConfig.params[ "result-template" ] ).toBe( "value" );
        } );

        it( "should set the app-region url parameter with the value from SERVICE_CONFIG", function()
        {
            this.requestConfig.params = { "app-region" : "" };

            this.service.onRequest(  this.requestConfig );
            expect(  this.requestConfig.params[ "app-region" ] ).toBe(  this.config.deviceInfo.appRegion );
        } );

        it( "should not set the app-region url parameter with the value from SERVICE_CONFIG", function()
        {
            this.requestConfig.params = { "app-region" : "value" };

            this.service.onRequest(  this.requestConfig );
            expect(  this.requestConfig.params[ "app-region" ] ).toBe( "value" );
        } );

        it( "should set the time url parameter with the current time and not cache bust", function()
        {
            jasmine.clock().install();
            var baseTime = new Date();
            jasmine.clock().mockDate( baseTime );

            this.requestConfig.params = { time : "" };

            this.service.onRequest(  this.requestConfig );
            expect(  this.requestConfig.params.time ).toBe( baseTime.getTime() );
            expect(  this.requestConfig.params.cacheNBuster ).not.toBeDefined();

            jasmine.clock().uninstall();
        } );

        it ("should set the clientDeviceID from SERVICE_CONFIG for requests that contain a deviceInfo object", function()
        {
            this.requestConfig.data.ModuleList.modules[0].moduleRequest.deviceInfo = { clientDeviceId : "0" };

            this.service.onRequest(  this.requestConfig );

            expect(  this.requestConfig.data.ModuleList.modules[0].moduleRequest.deviceInfo.clientDeviceId)
                .toEqual( this.config.deviceInfo.clientDeviceId);
        });
    } );
} );
