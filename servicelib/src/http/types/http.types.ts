import {
    AxiosRequestConfig,
    AxiosResponse
} from "axios";

/**
 * @MODULE:     service-lib
 * @CREATED:    07/19/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 * http.types provides some redefentisions of the Axios type to insulate the service layer from Axios
 */

/**
 * Extend the Axios request config object (https://github.com/mzabriskie/axios#request-config)
 */
export interface IHttpRequestConfig extends AxiosRequestConfig
{
    isRaw?: boolean;
    retryCount?: number;
    dataProperties?: Array<string>;
}

/**
 * Extend the Axios response object (https://github.com/mzabriskie/axios#response-schema)
 */
export interface IHttpResponse extends AxiosResponse
{
    dataProperties?: Array<string>;
    config : IHttpRequestConfig;
}

/**
 * Defines the structure for an generic network (HTTP or API) error
 */
export interface INetworkError
{
    message: string;
    code: number;
    url: string;
    detailedMessage: string;
    modules?: { any };
}