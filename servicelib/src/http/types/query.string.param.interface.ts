export interface IQueryStringParam
{
    name: string;
    value: string | number | boolean;
}
