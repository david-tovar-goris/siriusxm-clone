/**
 * @MODULE:     service-lib
 * @CREATED:    07/19/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *      Defines the interface for response data returned by the API.
 */
export interface IModuleListResponse
{
    moduleArea: string;
    moduleType: string;
    moduleResponse: Object;
    clientConfiguration?: any;
    updateFrequency? : number;
}

export interface IApiMessage
{
    code: number;
    message: string;
}
