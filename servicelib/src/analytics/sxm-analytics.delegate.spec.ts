import { ServiceEndpointConstants } from "../service/consts";
import { SxmAnalyticsDelegate }   from "./sxm-analytics.delegate";
import { mock } from "ts-mockito";
import { HttpProvider } from "../http";

describe("Analytics Service", () =>
{
    const mockPayload = { mockValue: "42" };

    beforeEach(function()
    {
        this.httpService = mock(HttpProvider);
        this.analyticsDelegate = new SxmAnalyticsDelegate(this.httpService);
    });

    it("should be defined.", function()
    {
        expect(this.analyticsDelegate).toBeDefined();
        expect(this.httpService).toBeDefined();
    });

    it("can call the endpoint and send the event tag..", function()
    {
        spyOn(this.httpService, 'post');

        this.analyticsDelegate.postAnalyticsCall(mockPayload);

        expect(this.httpService.post).toHaveBeenCalledWith(ServiceEndpointConstants.endpoints.ANALYTICS.V1_ANALYTICS_REPORT, mockPayload);
    });

});
