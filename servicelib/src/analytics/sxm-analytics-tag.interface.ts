import {
    EAction,
    EActionSource,
    EAppCountry,
    EApplication,
    EAssetType,
    EBannerInd,
    EConsumedInd,
    EContentSource,
    EContentType,
    EDeviceType,
    EElementType,
    EFindingMethod,
    EInputType,
    ELanguage,
    EModal,
    EOrientation,
    EPageFrame,
    EPlatform,
    EStreamingType,
    ESubscriptionLevel,
    ETileType,
    EPageName
} from "./sxm-analytics.enum";
import { ITileAssetInfo } from "../carousel";

export interface ISXMGlobalTag
{
    GupId: string;
    DeviceID: string;
    Platform: string; //EPlatform;
    Application: EApplication;
    UiVersion: string;
    DeviceType: string; //EDeviceType;
    DeviceOS: string;
    AppCountry: EAppCountry;
    Language: ELanguage;
    ChannelLineupID: string;
    SubscriptionLevel: ESubscriptionLevel;
    UserProfile: string;
    SessionId: string;
    SchemaVersion: string;
    SchemaClass: string;
    CclVersion: string;
    UserDaysListened: number;

    tags: Array<ISXMTag>;
}

export interface ISXMTag
{
    ActionTime: string;

    MarketingId: string;
    HitId: string;

    TagShortName: string;
    UserPath: string;
    Screen: string;
    PageFrame: EPageFrame;
    CarouselName: string;
    Toast: string;
    Modal: EModal;
    Overlay: string;
    CarouselPosition: number;
    ElementType: EElementType;
    ButtonName: string;
    LinkName: string;
    Text: string;
    ElementPosition: number;
    Action: EAction;
    ActionSource: EActionSource;
    InputType: EInputType;
    ContentSource: EContentSource;
    TileType: ETileType;
    AssetType: EAssetType;
    ContentType: EContentType;
    StreamingType: EStreamingType;
    ChannelGuid: string;
    ShowGuid: string;
    CategoryGuid: string;
    EpisodeGuid: string;
    AodEpisodeGuid: string;
    VodEpisodeGuid: string;
    LiveEventGuid: string;
    TeamGuid: string;
    LeagueGuid: string;
    PlayingStatus: string;
    NpTileType: ETileType;
    NpAssetType: EAssetType;
    NpContentType: EContentType;
    NpStreamingType: EStreamingType;
    NpChannelGuid: string;
    NpShowGuid: string;
    NpCategoryGuid: string;
    NpEpisodeGuid: string;
    NpAodEpisodeGuid: string;
    NpVodEpisodeGuid: string;
    NpLiveEventGuid: string;
    NpTeamGuid: string;
    NpLeagueGuid: string;
    NpStatus: string;
    BannerInd: EBannerInd;
    Orientation: EOrientation;
    ConsumedInd: EConsumedInd;
    SeekTo: string;
    SeekFrom: string;
    CurrentPosition: string;
    FindingMethod: EFindingMethod;
    SearchTerm: string;
    NumSearchResults: number;
    DirectTuneEntry: string;
    NewFavoriteIndex: number;
    ErrorNumber: number;
    LeadKeyId: string;
    MessageType: string;
    MetricEventCode: string;
    PushKeyId: string;
    MessageExpDate: string;
    NullSearch: number;
    Coachmark: string;
    ZoneName: string;
    ZonePosition: number;
    CollectionGuid: string;
    Edp: string;
    PageName: EPageName;
}

export interface ISXMDeviceInfo
{
    GupId: string;
    DeviceID: string;
    Platform: string; //EPlatform;
    Application: EApplication;
    UiVersion: string;
    DeviceType: string; //EDeviceType;
    DeviceOS: string;
    AppCountry: EAppCountry;
    Language: ELanguage;
    ChannelLineupID: string;
    UserProfile: string;
    SessionId: string;
    SchemaVersion: string;
    SchemaClass: string;
    CclVersion: string;
}

export interface ISXMClientTag
{
    type?: string;
    name: string;
    text?: string;
    userPath?: string;
    screen?: string;
    carouselPosition?: number;
    carouselName?: string;
    pageFrame?: string;
    elementType?: string;
    tileType?: string;
    buttonName?: string;
    tileAssetInfo?: ITileAssetInfo;
    linkName?: string;
    tagItemIndex?: number;
    zoneTitle?: string;
    zoneOrder?: number;
    inputType?: string;
    action? : string;
    elementPosition?: number;
    modal?: string;
    actionSource?: string;
    userPathScreenPosition?: number;
    bannerInd?: string;
    consumedPerc?: number;
    tileContentType?: string;
    findingMethod?: string;
    newFavoriteIndex?: number;
    skipActionSource?: boolean;
    edp?: string;
    skipScreenName?: boolean;
    searchTerm?: string;
    numSearchResults?: number;
    skipNpData?: boolean;
    skipCurrentPosition?: boolean;
    skipConsumedPerc?: boolean;
    messageType?: string;
    toast?: string;
    skipContentSource?: boolean;
    skipOrientation?: boolean;
    overlayName?: string;
    errorNumber?: number;
    pageName?: string;
    shim?: string;
    mercuryEvent?: string;
    perfVal1?: string;
    collectionGuid?: string;
}
