import { SxmAnalyticsService } from "./sxm-analytics.service";
import { BehaviorSubject, Observable, of as observableOf, of } from "rxjs";
import { MockAppConfig } from "../test/mocks/app.config.mock";
import { InitializationService } from "../initialization";
import { MediaTimeLine } from "../tune/tune.interface";

describe("Analytics Service", function()
{
    const mockPayload = { mockValue: "42" };
    const initializationServiceMock = { initState: of("") } as InitializationService;

    beforeEach(function()
    {
        this.currentlyPlayingService = {
            currentlyPlayingData: function()
            {
                return of( {} );
            }
        };
        this.authModelMock =
            {
                authenticating$: new BehaviorSubject(false) as Observable<any>,
                authenticated$: new BehaviorSubject(false) as Observable<any>
            };

        this.analyticsDelegate = {
            postAnalyticsCall: function (tag)
            {
                return observableOf(true);
            }
        };

        this.videoPlayerServiceMock =
            {
                stalled$: new BehaviorSubject(false) as Observable<any>,
                playbackStateSubject: {
                    getValue : function (){}
                }
            };

        this.mediaTimeLineServiceMOck =
            {
                mediaTimeLine: new BehaviorSubject({} as MediaTimeLine) as Observable<MediaTimeLine>
            };

        this.appConfigMock = new MockAppConfig();
        this.appConfigMock.deviceInfo.supportsVideoSdkAnalytics = true;

        this.analyticsService = new SxmAnalyticsService(
            this.analyticsDelegate,
            this.appConfigMock,
            this.authModelMock,
            initializationServiceMock,
            this.videoPlayerServiceMock,
            this.mediaTimeLineServiceMOck);
    });

    it("can call the endpoint and send the event tag..", function()
    {
        spyOn(this.analyticsDelegate, 'postAnalyticsCall').and.returnValue(observableOf(true));
        this.analyticsService.logAnalyticsTag(mockPayload);
        expect(this.analyticsDelegate.postAnalyticsCall).toHaveBeenCalled();
    });

    it("when video buffer underrun happens trigger the Ana call", function()
    {
        spyOn(this.analyticsService, 'logAnalyticsTag').and.returnValue(observableOf(true));
        this.videoPlayerServiceMock.stalled$.next(true);
        spyOn(this.videoPlayerServiceMock.stalled$, "getValue").and.returnValue("PLAYING");
        expect(this.analyticsService.logAnalyticsTag).toHaveBeenCalled();
    });

    it("when tuning from vod to other mediaType happens trigger the Ana call", function()
{
    spyOn(this.analyticsService, 'logAnalyticsTag').and.returnValue(observableOf(true));
    this.mediaTimeLineServiceMOck.mediaTimeLine.next({ mediaId: "mediaId", mediaType: "vod"});
    this.mediaTimeLineServiceMOck.mediaTimeLine.next({ mediaId: "mediaId", mediaType: "aod"});
    expect(this.analyticsService.logAnalyticsTag).toHaveBeenCalled();
});

    it("when tuning from aod to other mediaType happens ana call should not be triggered", function()
    {
        spyOn(this.analyticsService, 'logAnalyticsTag').and.returnValue(observableOf(true));
        this.mediaTimeLineServiceMOck.mediaTimeLine.next({ mediaId: "mediaId", mediaType: "aod"});
        this.mediaTimeLineServiceMOck.mediaTimeLine.next({ mediaId: "mediaId", mediaType: "vod"});
        expect(this.analyticsService.logAnalyticsTag).not.toHaveBeenCalled();
    });

    it("when tuning from vod to other vod happens ana call should be triggered", function()
    {
        spyOn(this.analyticsService, 'logAnalyticsTag').and.returnValue(observableOf(true));
        this.mediaTimeLineServiceMOck.mediaTimeLine.next({ mediaId: "mediaId", mediaType: "vod"});
        this.mediaTimeLineServiceMOck.mediaTimeLine.next({ mediaId: "diffMediaId", mediaType: "vod"});
        expect(this.analyticsService.logAnalyticsTag).toHaveBeenCalled();
    });
});
