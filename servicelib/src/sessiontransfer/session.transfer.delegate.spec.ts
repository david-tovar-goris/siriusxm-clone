
import { of as observableOf } from "rxjs";
import { SessionTransferDelegate } from "./session.transfer.delegate";


describe( "SessionTransferDelegate Test Suite >>", function()
{
    beforeEach( function()
    {
        this.mockHttp =
            {
                get : jasmine.createSpy( "get" )
            };

        this.delegate = new SessionTransferDelegate( this.mockHttp );
    } );

    describe( "Infrastructure >> ", function()
    {
        it( "Should have a test subject.", function()
        {
            expect( this.delegate instanceof SessionTransferDelegate ).toBe( true );
        } );
    } );

    describe( "`castStatusChange()` >> ", function()
    {
        describe( "Infrastructure >> ", function()
        {
            it( "Should forward the call to http", function()
            {
                expect( this.delegate.castStatusChange ).toBeDefined();
            } );
        } );

        describe( "calls to castStatusChange >> ", function()
        {
            it ("should return observable of boolean when casestatus change is called", function()
            {
                this.mockHttp.get =
                    jasmine.createSpy( "get" )
                           .and
                           .callFake((url, data, config) =>
                           {
                              return observableOf(true);
                           });

                this.delegate.castStatusChange(true);

                expect( this.mockHttp.get ).toHaveBeenCalled();
            });
        } );
    } );
} );
