import { of as observableOf } from "rxjs";
import { SessionTransferService } from "../sessiontransfer";
import { SessionTransferDelegate } from "./session.transfer.delegate";
import { ChromecastModel } from "../chromecast/chromecast.model";
import { MockRequestInterceptor, MockResponseInterceptor } from "../test/mocks/interceptors";
import { MockStorageService } from "../test/mocks/storage.service";
import { mockServiceConfig } from "../test/mocks/service.config";
import { MockHttpProvider } from "../test/mocks/http-provider.mock";
import { throwError } from "rxjs/internal/observable/throwError";

describe("SessionTransferService", function ()
{
    beforeEach(function()
    {
        this.mockHttpProvider   = new MockHttpProvider(
            MockRequestInterceptor,
            MockResponseInterceptor,
            MockStorageService,
            mockServiceConfig
        );
        this.sessionTransferDelegate = new SessionTransferDelegate(this.mockHttpProvider);

        this.chromecastModel = {
            isDeviceVideoSupported: false,
            state$: {
                getValue: function (){}
            }
        } as ChromecastModel;

        this.apiDelegate = {
            addApiEndpointReporter: {},
            addApiCodeHandler: function (){},
            checkApiResponse: jasmine.createSpy("checkApiResponse"),
            getResponseData: jasmine.createSpy("getResponseData").and.returnValue("response")
        };
    });

    describe("constructor", function ()
    {
        it("Should addApiCodeHandler when constructor is invoked", function ()
        {
            let sessionClaimed     = spyOn(this.apiDelegate, "addApiCodeHandler").and.callFake((key, handler) =>
            {
                handler.call();
            });
            this.sessionTransferService = new SessionTransferService(this.sessionTransferDelegate, this.apiDelegate, this.chromecastModel);
            expect(sessionClaimed).toHaveBeenCalled();
            expect(this.sessionTransferService.sessionClaimed.getValue()).toEqual(false);
        });
    });

    describe("Session transfer", function ()
    {
        beforeEach(function ()
        {
            this.sessionTransferService = new SessionTransferService(this.sessionTransferDelegate, this.apiDelegate, this.chromecastModel);
        });

        afterEach(function ()
        {
            this.sessionTransferService.sessionClaimed.next(true);
        });

        it("test for error scenario", function ()
        {
            let claimStatus   = false;
            let setErrorState = spyOn(this.sessionTransferDelegate, "castStatusChange").and.returnValue(observableOf(Error));
            this.sessionTransferService.castStatusChange(claimStatus).subscribe((response) =>
            {
                expect(setErrorState).toHaveBeenCalled();
                expect(response).toEqual(true);
                expect(this.sessionTransferService["claimStatus"]).toBe(claimStatus);
            });
        });

        it("can set the cast status to true when chromeCast state is 'CASTING_CONNECTED' ", function ()
        {
            let claimStatus   = true;
            let getStateValue = spyOn(this.chromecastModel.state$, "getValue").and.returnValue('CASTING_CONNECTED');
            this.sessionTransferService.sessionClaimed.next(false);
            this.sessionTransferService.castStatusChange(claimStatus).subscribe((response) =>
            {
                expect(getStateValue).toHaveBeenCalled();
                expect(response).toEqual(true);
                expect(this.sessionTransferService["claimStatus"]).toBe(!claimStatus);
            });
        });

        it("can leave cast status alone if the requested status equals the current status", function ()
        {
            let claimStatus      = false;
            let getStateValue    = spyOn(this.chromecastModel.state$, "getValue").and.returnValue(false);
            let castStatusChange = spyOn(this.sessionTransferDelegate, "castStatusChange").and.callFake(() =>
            {
                return throwError(new Error('Fake error'));
            });
            this.sessionTransferService.castStatusChange(claimStatus).subscribe((response) =>
            {
                expect(getStateValue).toHaveBeenCalled();
                expect(response).toEqual(true);
                expect(castStatusChange).toHaveBeenCalled();
                expect(this.sessionTransferService["claimStatus"]).toBe(claimStatus);
            });
        });

        it("can set the cast status to true", function ()
        {
            let claimStatus   = true;
            let getStateValue = spyOn(this.chromecastModel.state$, "getValue").and.returnValue(false);
            this.sessionTransferService.castStatusChange(claimStatus).subscribe((response) =>
            {
                expect(getStateValue).toHaveBeenCalled();
                expect(response).toEqual(true);
                expect(this.sessionTransferService["claimStatus"]).toBe(claimStatus);
            });
        });

        it("can transfer session", function ()
        {
            let next = spyOn(this.sessionTransferService.transferSessionToken, "next");
            this.sessionTransferService.transferSession();
            expect(next).toHaveBeenCalled();
        });

        it("cannot transfer session when transferSenderSession returns an error", function ()
        {
            let error = spyOn(this.sessionTransferDelegate, "transferSenderSession").and.callFake(() =>
            {
                return throwError(new Error('Fake error'));
            });
            this.sessionTransferService.transferSession();
            expect(error).toHaveBeenCalled();
        });
    });
});
