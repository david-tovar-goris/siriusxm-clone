/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />


import {throwError as observableThrowError,  of as observableOf } from 'rxjs';
import { SettingsService } from "./settings.service";
import { SettingsConstants } from "./settings.const";
import { ISettings } from "./globalSettings.interface";
import {
    mockSettingListResponse,
    mockSettingsToBeUpdated
} from "../test/mocks/settings.response";

describe("SettingsService", function()
{
    beforeEach(function()
    {
        this.settingsDelegate = {
            getSettings: function()
            {
                return observableOf(mockSettingListResponse);
            },
            updateSettings: function()
            {
                return observableOf(mockSettingListResponse);
            }
        };

        this.failureMessage = 'API Failure';

        this.appMonitorService = {

        };
        this.settingsService = new SettingsService(this.settingsDelegate, this.appMonitorService);
    });

    describe('`retrieveSettings()` >>', function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.settingsService.retrieveSettings).toBeDefined();
            });
        });

        describe("Execution >> ", function()
        {
            it('retrieve settings', function()
            {
                this.settingsService.retrieveSettings().subscribe(val =>
                {
                    expect(val).toEqual(true);
                });
            });

            it('retrieve settings if settings is empty', function()
            {
                spyOn(this.settingsDelegate, "getSettings").and.returnValue(observableOf(undefined));
                this.settingsService.retrieveSettings().subscribe(val =>
                {
                    expect(val).toEqual(false);
                });
            });

            it('settings observable to retrieve the settings', function()
            {
                this.settingsService.retrieveSettings();
                this.settingsService.settings.subscribe(settings =>
                {
                    expect(settings).toEqual(mockSettingListResponse);
                });
            });

            it("Returns failure response", function()
            {
                spyOn(this.settingsDelegate, "getSettings").and
                                                      .callFake(function()
                                                      {
                                                          return observableThrowError(this.failureMessage);
                                                      });

                this.settingsService.retrieveSettings().subscribe(null, (response) =>
                {
                    expect(response).toEqual(this.failureMessage);
                });
            });
        });
    });

    describe('`updateSettings()` >>', function()
    {
        describe("Infrastructure >> ", function()
        {
            it("Should have method defined on the test subject.", function()
            {
                expect(this.settingsService.updateSettings).toBeDefined();
            });
        });

        describe("Execution >> ", function()
        {
            it('updates the user\'s global settings', function()
            {
                this.settingsService.updateSettings(mockSettingsToBeUpdated).subscribe(val =>
                {
                    expect(val).toEqual(true);
                });
            });

            it("Returns failure response", function()
            {
                spyOn(this.settingsDelegate, "updateSettings").and
                                                      .callFake(function()
                                                      {
                                                          return observableThrowError(this.failureMessage);
                                                      });

                this.settingsService.updateSettings(mockSettingsToBeUpdated).subscribe(null, (response) =>
                {
                    expect(response).toEqual(this.failureMessage);
                });
            });
        });
    });

    describe('`getSettingValue()` >>', function ()
    {
        describe('where there is the provided setting in the provided settings list', function ()
        {
            describe('when the setting is a stringified boolean', function ()
            {
                it('returns the boolean boolean', function ()
                {
                    expect(this.settingsService.getSettingValue([{name: 'mock-setting', value: 'true'}], 'mock-setting')).toBe(true);
                });
            });

            describe('when the setting is NOT a strigified boolean', function ()
            {
                it('returns the string setting', function ()
                {
                    expect(this.settingsService.getSettingValue([{name: 'mock-setting', value: 'off'}], 'mock-setting')).toBe('off');
                });
            });
        });
        describe('where the provided setting is NOT present in the provided settings list', function ()
        {
            describe('when the setting is supposed to default to on', function ()
            {
                it('returns the string constant for ON', function ()
                {
                    expect(this.settingsService.getSettingValue([], SettingsConstants.MINI_PLAY)).toBe(SettingsConstants.ON);
                });
            });
            describe('when the setting does NOT default to on', function ()
            {
                it('returns null', function ()
                {
                    expect(this.settingsService.getSettingValue([], 'mock-setting')).toBe(null);
                });
            });
        });
    });

    describe('switchGlobalSettingOnOrOff()', function()
    {
        beforeEach(function()
        {
            this.settingsService.settingsList = {
                globalSettings: [
                    { name: SettingsConstants.NOTIFICATION_SUBSCRIPTION_CONTENT, value: 'off' }
                ],
                deviceSettings: []
            } as ISettings;

            spyOn(this.settingsService, 'updateSettings');
        });

        describe('when name is not found in list', function()
        {
            describe('when called with true', function()
            {
                it('adds to list and turns on', function()
                {
                    this.settingsService.switchGlobalSettingOnOrOff(
                        true,
                        SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS
                    );

                    expect(this.settingsService.updateSettings).toHaveBeenCalledWith(
                        {
                            globalSettings: [
                                { name: SettingsConstants.NOTIFICATION_SUBSCRIPTION_CONTENT, value: 'off' },
                                { name: SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS, value: 'on'}
                            ],
                            deviceSettings: []
                        }
                    );
                });
            });

            describe('when called with false', function()
            {
                it('adds to list and turns off', function()
                {
                    this.settingsService.switchGlobalSettingOnOrOff(
                        false,
                        SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS
                    );

                    expect(this.settingsService.updateSettings).toHaveBeenCalledWith(
                        {
                            globalSettings: [
                                { name: SettingsConstants.NOTIFICATION_SUBSCRIPTION_CONTENT, value: 'off' },
                                { name: SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS, value: 'off'}
                            ],
                            deviceSettings: []
                        }
                    );
                });
            });
        });

        describe('when name is found in list', function()
        {
            beforeEach(function()
            {
                this.settingsService.settingsList.globalSettings.push(
                    { name: SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS }
                );
            });

            describe('when called with true', function()
            {
                it('keeps in list and turns on', function()
                {
                    this.settingsService.switchGlobalSettingOnOrOff(
                        true,
                        SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS
                    );

                    expect(this.settingsService.updateSettings).toHaveBeenCalledWith(
                        {
                            globalSettings: [
                                { name: SettingsConstants.NOTIFICATION_SUBSCRIPTION_CONTENT, value: 'off' },
                                { name: SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS, value: 'on'}
                            ],
                            deviceSettings: []
                        }
                    );
                });
            });


            describe('when called with false', function()
            {
                it('keeps in list and turns on', function()
                {
                    this.settingsService.switchGlobalSettingOnOrOff(
                        false,
                        SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS
                    );

                    expect(this.settingsService.updateSettings).toHaveBeenCalledWith(
                        {
                            globalSettings: [
                                { name: SettingsConstants.NOTIFICATION_SUBSCRIPTION_CONTENT, value: 'off' },
                                { name: SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS, value: 'off'}
                            ],
                            deviceSettings: []
                        }
                    );
                });
            });
        });
    });
});
