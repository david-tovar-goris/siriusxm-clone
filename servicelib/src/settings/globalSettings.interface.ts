export interface ISetting
{
    name: string;
    value: string;
}

export interface ISettings
{
    deviceSettings: Array<ISetting>;
    globalSettings: Array<ISetting>;
}
