/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { of as observableOf } from "rxjs";
import { IAppConfig }               from "../config/interfaces/app-config.interface";
import { ServiceEndpointConstants } from "../service/consts/service.endpoint.constants";
import {
    deviceSettingsListToBeExpected,
    globalSettingsListToBeExpected,
    mockSettingsListResponseFromApi,
    mockSettingsListResponseFromUpdateApi,
    mockSettingsToBeUpdated
}                                   from "../test/mocks/settings.response";
import { ISettings }                from "./globalSettings.interface";
import { SettingsDelegate }         from "./settings.delegate";
import { MockAppConfig }            from "../test/mocks/app.config.mock";

describe("SettingsService", function()
{
    beforeEach(function()
    {
        this.config = new MockAppConfig();

        this.mockHttp =
            {
                postModuleAreaRequests: function (url, data, config)
                {
                    if (url === ServiceEndpointConstants.endpoints.ME_SETTINGS.V2_UPDATE_SETTINGS_RESULT)
                    {
                        return observableOf(mockSettingsListResponseFromUpdateApi);
                    }
                    else
                    {
                        return observableOf(mockSettingsListResponseFromApi);
                    }
                }
            };

        this.settingsDelegate = new SettingsDelegate(this.mockHttp, this.config);
    });

    describe("getSettings()", function()
    {
        it("retrieves the user's settings", function()
        {
            this.settingsDelegate.getSettings().subscribe((response: ISettings) =>
            {
                const expected = {
                    globalSettings: globalSettingsListToBeExpected,
                    deviceSettings: deviceSettingsListToBeExpected
                };
                expect(response).toEqual(expected);
            });
        });
    });

    describe("updateSettings()", function()
    {
        it("updates the user's settings", function()
        {
            this.settingsDelegate.updateSettings(mockSettingsToBeUpdated).subscribe((response: ISettings) =>
            {
                expect(response.globalSettings).toEqual(globalSettingsListToBeExpected);
                expect(response.deviceSettings).toEqual(deviceSettingsListToBeExpected);
            });
        });
    });
});
