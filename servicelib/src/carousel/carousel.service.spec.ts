/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { of as observableOf } from "rxjs";
import { CarouselDelegateMock } from "../test/mocks/carousel/carousel.delegate.mock";
import { CarouselService }           from "./carousel.service";
import { mockV2NormalizedCarousels } from "./mock-v2-carousel";
import { channelLineupServiceMock }  from "../channellineup/channel.lineup.service.mock";
import { mock }                      from "ts-mockito";
import { ProfileService }            from "../profile/profile.service";
import { LiveTimeService }           from "../livetime";
import { FavoriteService }           from "../favorite/favorite.service";
import { AlertService }              from "../alerts/alert.service";
import { MockAppConfig }             from "../test/mocks/app.config.mock";
import { ServiceFactory }            from "../service/service.factory";
import { CurrentlyPlayingService } from "../currently-playing/currently.playing.service";

xdescribe("CarouselService", () =>
{
    let service,
        delegate,
        profileServiceMock,
        favoriteServiceMock,
        mediaTimeLineServiceMock,
        currentlyPlayingServiceMock,
        alertServiceMock,
        liveTimeService,
        seededStationsServiceMock,
        dmcaService;

    beforeEach(() =>
    {
        delegate = new CarouselDelegateMock();
        profileServiceMock = mock(ProfileService);
        favoriteServiceMock = mock(FavoriteService);
        alertServiceMock = mock(AlertService);
        profileServiceMock.pausePoints =  observableOf([]);
        liveTimeService = { } as LiveTimeService;
        favoriteServiceMock.favorites =  observableOf([]);
        alertServiceMock.alerts = observableOf([]);
        mediaTimeLineServiceMock = {};
        currentlyPlayingServiceMock = mock(CurrentlyPlayingService);
        seededStationsServiceMock = {};
        dmcaService = {
            isUnrestricted: jasmine.createSpy('isUnrestricted'),
            isRestricted: jasmine.createSpy('isRestricted'),
            isDisallowed: jasmine.createSpy('isRestricted')
        };


        service = new CarouselService(delegate as any,
            channelLineupServiceMock as any,
            profileServiceMock,
            favoriteServiceMock,
            liveTimeService,
            mediaTimeLineServiceMock,
            currentlyPlayingServiceMock,
            alertServiceMock,
            seededStationsServiceMock,
            dmcaService,
            new MockAppConfig());
        spyOn(service, "cacheCarousel");
    });

    describe("Infrastructure >>",() =>
    {
        it("Should be able to create the service and all its dependencies",() =>
        {
            expect(ServiceFactory.getInstance(CarouselService)).toBeDefined();
        });
    });

    describe("selectCarouselsBySuperCategory", () =>
    {
        it("retrieves the carousels from the api using the delegate's getCarouselsBySuperCategory method", () =>
        {
            delegate.getCarouselsBySuperCategory.and.returnValue(observableOf(mockV2NormalizedCarousels));
            const superCategory = { key: "music" };

            service.getCarouselsBySuperCategory(superCategory);

            expect(delegate.getCarouselsBySuperCategory).toHaveBeenCalledWith(superCategory.key);
        });
    });

    describe("getCarousel By PageName", () =>
    {
        it("retrieves the carousels from the api using the delegates getCarouselsByPage method", () =>
        {
            delegate.getCarouselsByPage.and.returnValue(observableOf(mockV2NormalizedCarousels));
            const pageName = "profile";

            service.getCarouselsByPage(pageName);

            expect(delegate.getCarouselsByPage).toHaveBeenCalledWith(pageName, []);
        });
    });

    describe("getCarouselBySubCategory()", () =>
    {
        it("retrieves carousels from the api using the selectCarouselsBySubCategory", () =>
        {
            delegate.getCarouselsBySubCategory.and.returnValue(observableOf(mockV2NormalizedCarousels));
            const subCategory = { key: "pop" };

            service.getCarouselsBySubCategory(subCategory);

            expect(delegate.getCarouselsBySubCategory).toHaveBeenCalledWith(subCategory);
        });
    });

    describe("getCarousel By PageName and searchParam", () =>
    {
        it("retrieves the carousels from the api using the delegates getCarouselsByPage method", () =>
        {
            delegate.getCarouselsByPage.and.returnValue(observableOf(mockV2NormalizedCarousels));
            const params = { paramName: "", paramValue: "" };
            const pageName = "search";

            service.getCarouselsByPage(pageName, params);

            expect(delegate.getCarouselsByPage).toHaveBeenCalledWith(pageName, params);
        });
    });
});
