import { CarouselUtil } from "./carousel.util";
import { ITile, CarouselTypeConst} from "../index";

describe("Carousel Util Test Suite: ", function()
{
   it("Should return Additional Channel as content type for channel tile", function()
   {
        const tile = {
            tileContentSubType: CarouselTypeConst.ADDITIONAL_CHANNEL,
            tileContentType: CarouselTypeConst.CHANNEL_TILE
        } as ITile;

        expect(CarouselTypeConst.ADDITIONAL_CHANNEL).toBe(CarouselUtil.getContentType(tile));
   });

    it("Should return AOD as content type for episode tile", function()
    {
        const tile = {
            tileContentSubType: CarouselTypeConst.AOD_EPISODE_TILE,
            tileContentType: CarouselTypeConst.EPISODE_TILE
        } as ITile;

        expect(tile.tileContentSubType).toBe(CarouselUtil.getContentType(tile));
    });

    it("Should return Show as content type for show tile", function()
    {
        const tile = {
            tileContentSubType: CarouselTypeConst.AOD_EPISODE_TILE,
            tileContentType: CarouselTypeConst.SHOW_TILE
        } as ITile;

        expect(tile.tileContentType).toBe(CarouselUtil.getContentType(tile));
    });

});
