export const mockTileData = {
    "0001": [
        {
            subtext:  "Fox News",
            channelNumber:  "Ch 44",
            bgImageUrl: "../../assets/mock-images/img1.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/foxnewschannel-1-31-00-180x180.png"
        },
        {
            subtext:  "MSNBC",
            channelNumber:  "Ch 41",
            bgImageUrl: "../../assets/mock-images/img2.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/8367-1-31-00-180x180.png"
        },
        {
            subtext:  "Fox Business",
            channelNumber:  "Ch 40",
            bgImageUrl: "../../assets/mock-images/img3.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/9410-1-31-00-180x180.png"
        },
        {
            subtext:  "CNN",
            channelNumber:  "Ch 42",
            bgImageUrl: "../../assets/mock-images/img1.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/cnn-1-31-00-180x180.png"
        },
        {
            subtext:  "HLN",
            channelNumber:  "Ch 43",
            bgImageUrl: "../../assets/mock-images/img2.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/cnnheadlinenews-1-31-00-180x180.png"
        },
        {
            subtext:  "Bloomberg",
            channelNumber:  "Ch 45",
            bgImageUrl: "../../assets/mock-images/img3.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/bloombergradio-1-31-00-180x180.png"
        },
        {
            subtext:  "Public Affairs",
            channelNumber:  "Ch 46",
            bgImageUrl: "../../assets/mock-images/img1.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/8237-1-31-00-180x180.png"
        },
        {
            subtext:  "BBC",
            channelNumber:  "Ch 47",
            bgImageUrl: "../../assets/mock-images/img2.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/bbcworld-1-31-00-180x180.png"
        },
        {
            subtext:  "Insight",
            channelNumber:  "Ch 48",
            bgImageUrl: "../../assets/mock-images/img3.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/8183-1-31-00-180x180.png"
        },
        {
            subtext:  "NPR",
            channelNumber:  "Ch 49",
            bgImageUrl: "../../assets/mock-images/img1.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/nprnow-1-31-00-180x180.png"
        }
    ],
    "0010": [
        {
            channelNumber:  "Charlie Rose",
            subtext:  "Fire and Fury",
            airDate: "Friday",
            bgImageUrl: "../../assets/mock-images/img1.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/89/3066e5-da28-6671-9e46-f094adea9550.png"
        },
        {
            channelNumber:  "The Hear wh Dion" ,
            subtext:  "Matt Welch with guest",
            airDate: "Wednesday",
            bgImageUrl: "../../assets/mock-images/img2.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/e9/377861-08a0-2e93-e859-08e5de18af44.jpg"
        },
        {
            channelNumber:  "Tell Me Everything With John" ,
            subtext:  "Jackie Joke Man",
            airDate: "Monday",
            bgImageUrl: "../../assets/mock-images/img3.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/64/70fb2a-62da-1bfa-2dcc-1f31c8582822.png"
        },
        {
            channelNumber:  "Tell Me Everything With John" ,
            subtext:  "Jackie Joke Man",
            airDate: "Monday",
            bgImageUrl: "../../assets/mock-images/img1.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/a1/98d103-9e0b-3a78-aed1-11ff904686e3.jpg"
        },
        {
            channelNumber:  "A Day ithe Sun" ,
            subtext:  "Exploring Central Florida",
            airDate: "Monday",
            bgImageUrl: "../../assets/mock-images/img2.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/e9/07a4a1-a2d1-1c39-fdba-1583af6f8fca.png"
        }
    ],
    "0100": [
        {
            channelNumber:  "A Day ithe Sun" ,
            subtext:  "Exploring Central Florida",
            airDate: "Monday",
            bgImageUrl: "../../assets/mock-images/img2.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/0f/0b4a4d-cf4c-d140-c63c-5357930bab1e.png"
        },
        {
            channelNumber:  "Aspen Ideas TGo" ,
            subtext:  "Roots & the Future of Populism",
            airDate: "08.02.17",
            bgImageUrl: "../../assets/mock-images/img3.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/ab/bbc28a-f29e-2d9d-0000-7e4f0b7e896e.png"
        },
        {
            channelNumber:  "Best of Iight" ,
            subtext:  "Jane Lynch",
            airDate: "Tuesday",
            bgImageUrl: "../../assets/mock-images/img1.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/8b/8383ba-82a4-0cbe-d2b2-40525daa13d7.png"
        },
        {
            channelNumber:  "Great Debates",
            subtext:  "McDonald's vs Michelin",
            airDate: "Monday",
            bgImageUrl: "../../assets/mock-images/img2.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/02/63671d-f702-82db-991e-ab62c281b6c0.png"
        },
        {
            channelNumber:  "Star Talk",
            subtext:  "Black Holes",
            airDate: "Thursday",
            bgImageUrl: "../../assets/mock-images/img3.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/53/8d7664-d736-58e0-e1d9-a70d13bae687.png"
        },
        {
            channelNumber:  "Success without Sess" ,
            subtext:  "Directory",
            airDate: "Monday",
            bgImageUrl: "../../assets/mock-images/img2.jpg",
            fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/chan/53/8d7664-d736-58e0-e1d9-a70d13bae687.png"
        }
    ]
};
