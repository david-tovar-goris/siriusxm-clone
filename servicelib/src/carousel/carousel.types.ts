export interface ICarouselAlgorithmParameter
{
    paramKey: string;
    paramValue: string;
}

export interface ICarouselAlgorithm
{
    algorithmName: string;
    fallbackAlgorithmName?: string;
    algorithmParameters: Array<ICarouselAlgorithmParameter>;
}

export interface ICarouselConditionType
{
    conditionKey: string;
    conditionValue: string;
}

export interface ICarouselTextType
{
    textClass: string;
    textValue: string;
    textField?: string;
    textStyle?: string;
    textColor?: string;
}

export interface ICarouselViewAllType
{
    showViewAll: boolean;
    viewAllLink?: string;
}

export interface ITileImageType
{
    imageClass: string;
    imageLink: string;
    imageAltText: string;
}

export interface ITileActionType
{
    actionType: string;
    actionNeriticLink: string;
    actionAnalyticsTag: string;
}

export interface ISegmentGroupedCarouselType
{
    groupName: ICarouselTextType;
    groupedCarousel: IBaseCarousel;
}

export interface ISelectorSegmentType
{
    segmentTitle: string;
    segmentClass?: string;
    segmentImage?: ITileImageType;
    segmentLink: ITileActionType;
    segmentGroupedCarousel?: ISegmentGroupedCarouselType[];
    segmentCarousel?: IBaseCarousel[];
}

export interface ISelectorType
{
    selectorClass: string;
    selectorTitle?: ICarouselTextType;
    selectorSegment: ISelectorSegmentType[];
}

export interface ITileBannerType
{
    bannerClass: string;
    bannerText: string;
    bannerCondition?: string;
}

export interface ITileMarkupType
{
    tileAction: Array<ITileActionType>;
    tileImage: Array<ITileImageType>;
    tileText: Array<ICarouselTextType>;
    tileBanner?: ITileBannerType;
    backgroundColor?: string;
}

export interface ITileType
{
    tileContentType: string;
    tileContentSubType: string;
    tileAssetInfo: IAssetInfoType[];
    tileContentStatus?: string;
    tileMarkup: ITileMarkupType;
}

export interface IZoneType
{
    zoneClass: string;
    zoneId: string;
    zoneGuid: string;
    zoneTitle: string;
    zoneLink: string;
    heroCarousel: IBaseCarousel[];
    carousel: IBaseCarousel [];
    moreSelector: ISelectorType;
    expiry: string;
    zoneOrder: number;
}

export interface IZoneInformationType
{
    moreZonesAvailable: boolean;
    totalZonesAvailable: number;
    totalZonesReturned: number;
    zoneIdsAvailable: string[];
}

export interface IAssetInfoType
{
    assetInfoKey: string;
    assetInfoValue: string;
    assetInfoNum?: number;
    assetInfoList?: IAssetInfoType[];
}

export interface ICarouselEditableType
{
    clearAll: string;
    modifyModuleType: string;
    remove: string;
    reorder: string;
}

export interface IBaseCarousel
{
    carouselGUID: string;
    carouselAlgorithm?: ICarouselAlgorithm;
    carouselConditions?: ICarouselConditionType [];
    carouselTitle: ICarouselTextType;
    carouselDisplayType: string;
    carouselOrientation: string;
    carouselViewAll: ICarouselViewAllType;
    carouselTiles: ITileType [];
    carouselTileEntities?: any[];
    editable?: ICarouselEditableType;
}

export interface IBaseCarouselResponse
{
    screen: string;
    expiry: string;
    pageTitle?: ICarouselTextType;
    pageLogo?: ITileImageType;
    pageBackground?: ITileImageType;
    pageBackgroundColor?: string;
    pageText?: ICarouselTextType[];
    pageAssetInfo?: IAssetInfoType[];
    pageSelector: ISelectorType;
    moreSelector: ISelectorType;
    pageAction?: ITileActionType[];
    zone?: IZoneType[];
    zoneInformation?: IZoneInformationType;
    heroCarousel: IBaseCarousel;
    carousel: IBaseCarousel [];
    contentToggleSelector?: ISelectorType;
}
