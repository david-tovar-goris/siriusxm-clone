import { FavoriteCarouselNormalizer } from "./favorite-carousel.normalizer";
import { carouselDataByType } from "../../test/mocks/favorites/favorite.response";
import { IGroupedFavorites } from "../../favorite/favorite.interface";

describe( "Favorite Carousel Normalizer Test Suite:", function()
{
    const favoriteCarouselNormalizer = new FavoriteCarouselNormalizer();

    it("Noramalize Favorite Carousel", function()
    {
        let normalizedData: IGroupedFavorites = FavoriteCarouselNormalizer
            .normalizeFavoritesCarousel(carouselDataByType);
        expect(normalizedData.channels.length).toBe(2);
    });

    it("Default Response from Normalizer", function()
    {
        carouselDataByType.selectors[0].class = null;
        let normalizedData: IGroupedFavorites = FavoriteCarouselNormalizer
            .normalizeFavoritesCarousel(carouselDataByType);
        expect(normalizedData.channels.length).toBe(0);
        expect(normalizedData.shows.length).toBe(0);
        expect(normalizedData.episodes.length).toBe(0);
    });
});
