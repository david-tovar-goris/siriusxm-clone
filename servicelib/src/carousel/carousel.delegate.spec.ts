/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { BehaviorSubject ,  Observable, of as observableOf }           from "rxjs";
import { CarouselDelegate }          from "./carousel.delegate";
import {
    IAppByPassState,
    ISuperCategory
}                                    from "../index";
import {IBaseCarouselResponse}       from "./carousel.types";
import { mockV2RawCarouselResponse } from "./mock-v2-carousel";
import { baseCategoryMock }          from "../test/mocks/channel.lineup";
import { ContentTypes }              from "../service/types/content.types";
import { CarouselTypeConst }         from "./carousel.const";
import { ServiceFactory }            from "../service/service.factory";


describe("CarouselDelegate", () =>
{
    let carouselDelegate,
        mockHttp;
    const channelId = "classicrewind";
    const action = { actionNeriticLink: "Api:tune:liveAudio:"+ channelId, actionAnalyticsTag: "", actionPadding: 0 };
    let mockV2Response: IBaseCarouselResponse;


    beforeEach(() =>
    {
        this.bypassMonitorService = {
            bypassErrorState: observableOf({} as IAppByPassState)
        };

        this.configService = {
            liveVideoEnabled: () => false,
            getRelativeUrlSettings: () => [],
            getRelativeURL: (urlKey:string) => urlKey
        };

        mockHttp =
            {
                get: jasmine.createSpy("get").and.returnValue(observableOf([]))
            };

        carouselDelegate = new CarouselDelegate(mockHttp, this.bypassMonitorService, this.configService);

        mockV2Response = {
            ...mockV2RawCarouselResponse
        };

    });

    afterEach(() =>
    {
        mockV2Response = null;
        carouselDelegate = null;
        mockHttp = null;
    });

    describe("Infrastructure >>",() =>
    {
        it("Should be able to create the service and all its dependencies",() =>
        {
            expect(ServiceFactory.getInstance(CarouselDelegate)).toBeDefined();
        });
    });

    describe("getCarouselsBySuperCategory", () =>
    {
        const mockSuperCat = { key: "for_you" } as ISuperCategory;
        const mockParams = {
            params: {
                "page-name": `supercategory_${mockSuperCat.key}`,
                "result-template": "everest|web"
            }
        };

        it("gets the carousels from the endpoint by superCategory key", () =>
        {
            carouselDelegate.getCarouselsBySuperCategory(mockSuperCat.key);
            expect(mockHttp.get.calls.mostRecent().args).toContain(mockParams);
        });

        it("removes any carousels that do not have tiles if tiles are empty array", () =>
        {
            let carousels;
            mockV2Response.carousel[0].carouselTiles = [];

            mockHttp.get.and.returnValue(observableOf(mockV2Response));

            carouselDelegate.getCarouselsBySuperCategory(mockSuperCat.key)
                .subscribe(res => carousels = res);

            expect(carousels.zone[0].content.length).toEqual(0);
        });

        it("removes any carousels that do not have tiles if tiles are null", () =>
        {
            let carousels;
            mockV2Response.carousel[0].carouselTiles = null;

            mockHttp.get.and.returnValue(observableOf(mockV2Response));

            carouselDelegate.getCarouselsBySuperCategory(mockSuperCat.key)
                .subscribe(res => carousels = res);

            expect(carousels.zone[0].content.length).toEqual(0);
        });

        it("removes any carousels that do not have tiles if tiles don't exist at all", () =>
        {
            let carousels;
            delete mockV2Response.carousel[0].carouselTiles;

            mockHttp.get.and.returnValue(observableOf(mockV2Response));

            carouselDelegate.getCarouselsBySuperCategory(mockSuperCat.key)
                .subscribe(res => carousels = res);

            expect(carousels.zone[0].content.length).toEqual(0);
        });

        it("normalizes the carousel tile data and separates them by type", () =>
        {
            let carousels,
                parsedCarousel = {
                    zone:[
                        {
                            hero: [
                                {
                                    screen : "for_you",
                                    algorithm: {
                                        name: "curated",
                                        parameters: [
                                            {
                                                paramKey: "pageName",
                                                paramValue: "for_you"
                                            },
                                            {
                                                paramKey: "name",
                                                paramValue: "SiriusXM Essentials"
                                            }
                                        ]
                                    },
                                    tiles:[
                                        {
                                            neriticLinkData:[
                                                {
                                                    analyticsTag:"",
                                                    functionalGroup:"primaryAction",
                                                    linkType:"Api",
                                                    actionType:"tune",
                                                    contentType: ContentTypes.VOD,
                                                    channelId:"howardstern100",
                                                    assetGuid:"b0c6b833-b88e-4715-8134-db454340f3ca",
                                                    contentSubType:"",
                                                    startTime: 0,
                                                    url: "",
                                                    showGuid: "",
                                                    categoryKey: ""
                                                }
                                            ],
                                            tileAssetInfo:
                                                {
                                                    aodEpisodecaId: "",
                                                    channelGuid   : "",
                                                    channelId     : "",
                                                    episodeGuid   : "",
                                                    showGuid      : "",
                                                    vodEpisodeGuid: "b0c6b833-b88e-4715-8134-db454340f3ca",
                                                    categoryGuid  : "",
                                                    categoryKey   : "",
                                                    recentPlayGuid: "",
                                                    alertGuid:      "",
                                                    favAssetGuid: "",
                                                    showName: "",
                                                    recentPlayAssetGuid: "",
                                                    recentPlayType: "",
                                                    isLiveVideoEligible: null,
                                                    isPlaceholderShow: false,
                                                    channelName : "",
                                                    sequencerTuneChannelId : "",
                                                    tabSortOrder: 0,
                                                    create: false,
                                                    stationId: "",
                                                    hideFromChannelList: false,
                                                    isPodcast: false,
                                                    isPandoraPodcast: false
                                                },

                                            tileAssets : mockV2Response.heroCarousel.carouselTiles[0].tileAssetInfo,
                                            backgroundColor : "",
                                            bgImageUrl:"http://pri.art.uat.streaming.siriusxm.com/images/chan/ec/a4a364" +
                                                "-0f9d-6318-334b-503f1b8e9c24.jpg",
                                            fgImageUrl:"http://pri.art.uat.streaming.siriusxm.com/images/chan/d5/7998af" +
                                                "-ac29-b9ea-367d-dc4ee3bf8c55.png",
                                            fgImageType: "left-top",
                                            iconImageUrl: '',
                                            iconImageType: '',
                                            imageAltText: "show name",
                                            tileShape: 'square',
                                            tileBanner: {
                                                bannerClass: CarouselTypeConst.UNAVAILABLE_BANNER_CLASS,
                                                bannerText : "New Episode",
                                                bannerColor : CarouselTypeConst.UNAVAILABLE_BANNER_COLOR,
                                                display : true
                                            },
                                            ariaText:"2m · 12/8/17",
                                            isTunedTo$: new BehaviorSubject<boolean>(false),
                                            line1:"2m · 12/8/17",
                                            line1$:new BehaviorSubject<string>("2m · 12/8/17"),
                                            line2:"Gary the Conqueror combats the phone flu",
                                            line2$:new BehaviorSubject<string>("Gary the Conqueror combats the phone flu"),
                                            line3: "",
                                            line3$:new BehaviorSubject<string>(""),
                                            line3Prefix: {
                                                textValue: "10 NEW • ",
                                                textStyle: "bold",
                                                textColor: "#2BA7FF",
                                                textClass: "line3-prefix"
                                            },
                                            longDescription: "Yoda, Mad Dog, Juggalos & More!",
                                            isEligibleForLiveUpdates : false,
                                            reminders: {showReminderSet: false, liveVideoReminderSet: false},
                                            primaryNeriticLink:{
                                                analyticsTag:"",
                                                functionalGroup:"primaryAction",
                                                linkType:"Api",
                                                actionType:"tune",
                                                contentType: ContentTypes.VOD,
                                                channelId:"howardstern100",
                                                assetGuid:"b0c6b833-b88e-4715-8134-db454340f3ca",
                                                contentSubType:"",
                                                startTime: 0,
                                                url: "",
                                                showGuid: "",
                                                categoryKey: ""
                                            },
                                            tileContentType: ContentTypes.VOD,
                                            tileContentTypeFromAPI: ContentTypes.VOD,
                                            tileContentSubType : "",
                                            tabSortOrder: 0
                                        }
                                    ],
                                    type:"hero",
                                    guid:"hero",
                                    carouselViewAll: {
                                        showViewAll: false,
                                        viewAllLink: ''
                                    },
                                    carouselOrientation: "horizontal",
                                    title:{
                                        textClass: CarouselTypeConst.CAROUSEL_TITLE,
                                        textValue:""
                                    },
                                    conditions : {
                                        channelId : ""
                                    },
                                    editable: {
                                        clearAll: false,
                                        modifyModuleType: "",
                                        remove: false,
                                        reorder: false
                                    }
                                }
                            ],
                            content: []
                        }
                    ]
                };

            mockHttp.get.and.returnValue(observableOf(mockV2Response));

            carouselDelegate.getCarouselsBySuperCategory(mockSuperCat.key)
                .subscribe(res => carousels = res);

            expect(carousels.zone[0].hero).toEqual(parsedCarousel.zone[0].hero);
        });
    });

    describe("getCarouselsByPage", () =>
    {
        const mockPage = "profile";
        const mockParams = {
            params: {
                "page-name": mockPage,
                "result-template": "everest|web",
                "search-term": undefined
            }
        };

        it("gets the carousels from the endpoint by pageName", () =>
        {
            carouselDelegate.getCarouselsByPage(mockPage, [ { paramName : "search-term", paramValue : undefined } ]);
            expect(mockHttp.get.calls.mostRecent().args).toContain(mockParams);
        });

        /**
         * @todo
         * What was this test trying to accomplish?
         * How do I reconcile this with the new Schema?
         *
         * Ask Charles Christine about this
         *
         * @description
         * In the new Schema:
         *      @const carouselTypeConstants,
         *      @type CarouselType from
         *      @file carousel.interface
         *      don't seem to be used anymore.
         *
         *      @class CarouselConsts from
         *      @file api.request.consts
         *      don't seem to be used anymore.
         *
         *      This test currently seems invalid
         *
         */
        it("removes any carousels that do not have a type of HERO or STANDARD", () =>
        {
            let carousels;
            mockHttp.get.and.returnValue(observableOf([ { carouselType: "NOTVALID" } ]));

            carouselDelegate.getCarouselsByPage(mockPage)
                .subscribe(res => carousels = res);

            expect(carousels.zone.length).toEqual(0);
        });

        it("removes any carousels that do not have tiles if tiles are empty array", () =>
        {
            let carousels;
            mockV2Response.carousel[0].carouselTiles = [];

            mockHttp.get.and.returnValue(observableOf(mockV2Response));

            carouselDelegate.getCarouselsByPage(mockPage)
                .subscribe(res => carousels = res);

            expect(carousels.zone[0].content.length).toEqual(0);
        });

        it("removes any carousels that do not have tiles if tiles are null", () =>
        {
            let carousels;
            mockV2Response.carousel[0].carouselTiles = null;

            mockHttp.get.and.returnValue(observableOf(mockV2Response));

            carouselDelegate.getCarouselsByPage(mockPage)
                .subscribe(res => carousels = res);

            expect(carousels.zone[0].content.length).toEqual(0);
        });

        it("removes any carousels that do not have tiles if tiles don't exist at all", () =>
        {
            let carousels;
            delete mockV2Response.carousel[0].carouselTiles;

            mockHttp.get.and.returnValue(observableOf(mockV2Response));

            carouselDelegate.getCarouselsByPage(mockPage)
                .subscribe(res => carousels = res);

            expect(carousels.zone[0].content.length).toEqual(0);
        });


        /**
         * @todo
         *      Modify mockV2Response so it's a content not hero?
         *      @interface IV2RawCarouselResponse doesn't seem to be able
         *      to hold non-hero easily, how do I mock a content carousel?
         *      Is content supposed to have stuff in it after normalization now?
         *      carousels.content keeps coming back as [] when I use the
         *      existing @interface IV2RawCarouselResponse Mock
         *
         *      Is this spec still relevant?
         */
        xit("normalizes the carousel tile data and separates them by type", () =>
        {
            let carousels,
                parsedCarousel = {
                    zone:[
                        {
                            content: [
                                {
                                    algorithm: {
                                        name: "curated",
                                        aparameters: [
                                            {
                                                paramKey: "pageName",
                                                paramValue: "for_you"
                                            },
                                            {
                                                paramKey: "name",
                                                paramValue: "SiriusXM Essentials"
                                            }
                                        ]
                                    },
                                    tiles: [
                                        {
                                            action: action,
                                            type: "STANDARD",
                                            tileContentType: "CHANNEL",
                                            assetGuid: "totally70s",
                                            bgImageUrl: "",
                                            fgImageUrl: "",
                                            imageAltText: "",
                                            channelNumber: "",
                                            subtext: "",
                                            bannerText: "",
                                            promoTile : false,
                                            tileGuid: "totally70s",
                                            neriticLinkData : {
                                                channelId:channelId,
                                                linkType   : 'Api',
                                                actionType : 'tune',
                                                contentType: "liveAudio",
                                                assetGuid  :  "",
                                                showGuid  :  ""
                                            },
                                            images: [],
                                            texts : [],
                                            background : null
                                        }
                                    ],
                                    type: "content",
                                    guid: "",
                                    title:undefined
                                }
                            ]
                        }
                    ]

                };

            mockHttp.get.and.returnValue(observableOf(mockV2Response));

            carouselDelegate.getCarouselsByPage(mockPage)
                .subscribe(res => carousels = res);

            console.log(`carousels =
                ${JSON.stringify(carousels)}
                hoping to get the entire JSON
            `);

            // expect(carousels.content).toEqual(parsedCarousel.content);
        });
    });

    describe("getCarouselsBySubCategory", () =>
    {
        const mockParams = {
            params: {
                "page-name"      : `category_${baseCategoryMock.key}`,
                "result-template": "everest|web"
            }
        };

        it("gets the carousels from the endpoint by base category", () =>
        {
            carouselDelegate.getCarouselsBySubCategory(baseCategoryMock);
            expect(mockHttp.get.calls.mostRecent().args).toContain(mockParams);
        });

        /**
         * @todo
         *      come up with a valid test case here
         *      may need to modify the mockV2Response
         *      to get actual content
         */
        it("Should retrieve carousels", () =>
        {
            let carousels;
            mockHttp.get.and.returnValue(observableOf(mockV2Response));

            carouselDelegate.getCarouselsBySubCategory(baseCategoryMock)
                .subscribe(res => carousels = res);
        });

    });

    describe("getV1Carousels", () =>
    {

    });

    describe("getV2Carousels", () =>
    {

    });

});
