/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import {
    INeriticLinkData,
    ActionTypes,
    CarouselType,
    CarouselTypeConst,
    V2CarouselNormalizer,
    mockV2RawTileData,
    mockV2RawCarouselData,
    mockNormalizedTile,
    mockNormalizedCarousel,
    mockV2RawCarouselResponse,
    mockV2NormalizedCarousels, IEditable
} from "../index";

import { neriticLinkDataMock } from "../test/mocks/data/neritic-link-data.mock";

xdescribe("V2 Carousel Normalizer Test", () =>
{
    describe("normalizeCarousels", () =>
    {
        it("should return normalized carousels", () =>
        {
            expect(V2CarouselNormalizer.normalizeCarousels(mockV2RawCarouselResponse))
                .toEqual(jasmine.objectContaining(mockV2NormalizedCarousels));

        });

        it("should call V2CarouselNormalizer.normalizeCarousel", () =>
        {
            spyOn(V2CarouselNormalizer, "normalizeCarousel");
            V2CarouselNormalizer.normalizeCarousels(mockV2RawCarouselResponse);
            expect(V2CarouselNormalizer.normalizeCarousel).toHaveBeenCalled();
        });
    });

    describe("normalizeCarousel", () =>
    {
        let type : CarouselType = "content";

        it("should return a normalized carousel", () =>
        {

            expect(V2CarouselNormalizer.normalizeCarousel(mockV2RawCarouselData, type, mockV2RawCarouselData.screen))
                .toEqual(jasmine.objectContaining(mockNormalizedCarousel));
        });

        it("should call V2CarouselNormalizer.normalizeTile", () =>
        {
            spyOn(V2CarouselNormalizer, "normalizeTile");
            V2CarouselNormalizer.normalizeCarousel(mockV2RawCarouselData, type, mockV2RawCarouselData.screen);
            expect(V2CarouselNormalizer.normalizeTile).toHaveBeenCalled();
        });
    });

    describe("normalizeTile", () =>
    {
        it("should return a normalized tile object", () =>
        {
            const editable = {
                clearAll: false,
                modifyModuleType: "",
                remove: false,
                reorder: false
            };
            expect(V2CarouselNormalizer.normalizeTile(mockV2RawTileData,"content", [], editable))
                .toEqual(jasmine.objectContaining(mockNormalizedTile));
        });

        it("should call V2CarouselNormalizer.determineTileType", () =>
        {
            const editable = {
                clearAll: false,
                modifyModuleType: "",
                remove: false,
                reorder: false
            };
            spyOn(V2CarouselNormalizer, "determineTileType");
            V2CarouselNormalizer.normalizeTile(mockV2RawTileData,"content",[], editable);
            expect(V2CarouselNormalizer.determineTileType).toHaveBeenCalled();
        });
    });

    describe("determineTileType", () =>
    {
        describe("when the neritic link data content type is liveaudio", () =>
        {
            neriticLinkDataMock.contentType = CarouselTypeConst.LIVE_AUDIO;

            describe("when the neritic link data action type is tune", () =>
            {
                neriticLinkDataMock.actionType = ActionTypes.TUNE;

                describe("when the tile content type is show", () =>
                {
                    let type = CarouselTypeConst.SHOW_TILE;

                    it("should return a tile content type of live-show", () =>
                    {
                        expect(V2CarouselNormalizer.determineTileType(type, neriticLinkDataMock)).toEqual(CarouselTypeConst.LIVE_SHOW_TILE);
                    });
                });

                describe("when the tile content type is continue-listening", () =>
                {
                    let type = CarouselTypeConst.CONTINUE_LISTENING_TILE;

                    it("should return a tile content type of live-continue-listening", () =>
                    {
                        expect(V2CarouselNormalizer.determineTileType(type, neriticLinkDataMock))
                                        .toEqual(CarouselTypeConst.LIVE_CONTINUE_LISTENING_TILE);
                    });
                });

                describe("when the tile content type is vod", () =>
                {
                    let type = CarouselTypeConst.VOD_EPISODE_TILE;

                    it("should return a tile content type of vod", () =>
                    {
                        expect(V2CarouselNormalizer.determineTileType(type, neriticLinkDataMock))
                                        .toEqual(CarouselTypeConst.VOD_EPISODE_TILE);
                    });
                });
            });
        });
    });
});
