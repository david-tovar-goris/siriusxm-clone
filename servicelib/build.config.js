/**
 * This file/module contains all common configuration constants used for the build process.  Constants contained within
 * this file should be agnostic to the actual build process, and instead should just define values to be used within
 * the build process
 */
module.exports = function() {

  const path = require('path');
  const root = path.join(__dirname, '..');

  return {
    name: "sxmsl",
    rootDir: __dirname,
    srcDir: "./src/",
    buildDir: "./bin/",
    entry: "./src/index.ts",
  }
}();

