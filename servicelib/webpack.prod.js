/**
 * This file contains the production environment specific configuration for webpack.  This is used with the
 * "advanced" configuration approach specified in the following link from the webpack 2 documentation
 *
 * https://webpack.js.org/guides/production/#advanced-approach
 */

module.exports = getExports();

function getExports() {
  const buildConfig = require("./build.config.js");

  return {
    output  : { filename : buildConfig.buildDir + buildConfig.name + '_min.js' },
    plugins : getPlugins()
  };
}

function getPlugins() {
  const webpack = require('webpack');

  const loaderOptions = {
    minimize : true,
    debug    : false
  };

  const uglifyOptions = {
    beautify : false,
    mangle   : {
      screw_ie8   : true,
      keep_fnames : true
    },
    compress : {
      screw_ie8 : true
    },
    comments : false,
  };

  return [
    new webpack.LoaderOptionsPlugin(loaderOptions),
    new webpack.optimize.UglifyJsPlugin(uglifyOptions),
  ]
}


