/**
 * This file contains the development environment specific configuration for webpack.  This is used with the
 * "advanced" configuration approach specified in the following link from the webpack 2 documentation
 *
 * https://webpack.js.org/guides/production/#advanced-approach
 */
module.exports = getExports();

function getExports() {

  const buildConfig = require("./build.config.js");

  return {
    devtool : "eval-source-map",
    output  : { filename : buildConfig.buildDir + buildConfig.name + '.js' }
  }
}
