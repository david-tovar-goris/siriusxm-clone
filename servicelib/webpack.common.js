/**
 * This file contains the common configuration for webpack.  This is used with the "advanced" configuration approach
 * specified in the following link from the webpack 2 documentation
 *
 * https://webpack.js.org/guides/production/#advanced-approach
 */

const buildConfig = require('./build.config.js');

function getExports() {

  return {
    entry   : buildConfig.entry,
    output  : getOutput(),
    resolve : getResolvableExtensions(),
    module  : {
      rules : getRules()
    },
    plugins : getPlugins()
  };
}

function getOutput() {
  return {
    pathinfo       : false,
    library        : buildConfig.name,
    libraryTarget  : 'amd',
    umdNamedDefine : true
  };
}

function getResolvableExtensions() {
  return { // Add `.ts` and `.tsx` as a resolvable extension.
    extensions : [ '.ts', '.tsx', '.js' ]
  };
}

function getRules() {
  // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
  return [
    {
      test    : /\.ts$/,
      enforce : 'pre',
      loader  : 'tslint-loader',
      options : {
        configFile : 'tslint.json',
        emitErrors : true,
        fix        : true,
      }
    },
    {
      test   : /\.tsx?$/,
      loader : 'ts-loader',
    }
  ]
}

function getPlugins() {
  const CleanWebpackPlugin   = require('clean-webpack-plugin');
  const WebpackShellPlugin = require('webpack-shell-plugin');
  const buildConfig          = require('./build.config.js');
  const path                 = require('path');

  // the path(s) that should be cleaned
  let pathsToClean = [
    buildConfig.buildDir,
  ];

  let cleanOptions = {
    root:     buildConfig.rootDir,
    verbose:  true,
    dry:      false
  };

  return [
    new CleanWebpackPlugin(pathsToClean, cleanOptions),
    new WebpackShellPlugin({ onBuildEnd:['npm run copy'], dev : false})
  ];
}

module.exports = getExports();
