const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const path                 = require('path');

function getExports(env) {
    var entryPoints = {};

    switch (env.app) {
        case "client": {
            entryPoints = {
                client: "./src/main.client.prod.ts"
            };
            console.log("Currently running prod server as  - Client");
            break;
        }
        case "landing": {
            entryPoints = {
                deepLink: "./src/main.landing.prod.ts"
            };
            console.log("Currently running prod server as  - Landing ");
            break;
        }
        case "landingCA": {
            entryPoints = {
                deepLinkCA: "./src/main.landing.prod.ts"
            };
            console.log("Currently running prod server as  - Landing ");
            break;
        }
        case "portal": {
            entryPoints = {
                portal: "./src/main.portal.prod.ts"
            };
            console.log("Currently running prod server as  - Portal App");
            break;
        }
        default: {
            throw "Invalid app";
        }
    }

    return {
        resolve: {
            "alias": {
                "sxm-audio-player": path.resolve(__dirname, "node_modules/sxm-audio-player/bin/web-audio-player.min.js"),
            }
        },
        devtool : 'source-map',
        plugins: getPlugins(env),
        entry: entryPoints,
        mode: "production"
    };
}

function getPlugins(env) {
    return [new BundleAnalyzerPlugin( { analyzerMode : "static", generateStatsFile : true }) ];
}

module.exports = env => {
    return getExports(env);
};
