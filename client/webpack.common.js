const webpack                = require('webpack');
const path                   = require('path');
const ProgressPlugin         = require('webpack/lib/ProgressPlugin');
const HtmlWebpackPlugin      = require('html-webpack-plugin');
const CopyWebpackPlugin      = require("copy-webpack-plugin");
const TerserPlugin           = require('terser-webpack-plugin');
const AotPlugin              = require('@ngtools/webpack').AngularCompilerPlugin;
const WebpackGitHash         = require('webpack-git-hash');
const packageJson            = require('./package.json');
const child_process          = require('child_process');
const CreateFileWebPack      = require('create-file-webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WorkboxWebpackPlugin   = require("workbox-webpack-plugin");
const WebpackPwaManifest     = require('./webpack.pwa');

const clientChunks        = [ "inline", "polyfills.client", "sw-register", "styles.client", "vendor.client", "client" ];
const landingChunks       = [ "inline", "polyfills.deepLink", "sw-register", "styles.deepLink", "vendor.deepLink", "deepLink" ];
const landingCAChunks     = [ "inline", "polyfills.deepLinkCA", "sw-register", "styles.deepLinkCA", "vendor.deepLinkCA", "deepLinkCA" ];
const portalChunks        = [ "inline", "polyfills.portal", "styles.portal", "vendor.portal", "portal" ];

const gitHashNumber       = child_process.execSync('git rev-parse --short HEAD ', { encoding : 'utf8' });
const teamCityBuildNumber = process.env.BUILD_NUMBER || '0';
const versionNumber       = packageJson.version;
const usIosAppId          = packageJson.usIosAppId;
const caIosAppId          = packageJson.caIosAppId;
const sxmVersionNumber    = versionNumber.split('.').slice(0, 2).join('.');

// HtmlWebPackPluggin has hard coded type as "text/javascript", below is the polyfill to override value to
// application/javascript
HtmlWebpackPlugin.prototype.generateAssetTags = assetTagPolyFill;

module.exports = env => {
    const config = {
        devServer: getDevServer(env),
        optimization: getOptimization(env),
        plugins: getPlugins(env),
        resolve: {
            "extensions": [".ts", ".js"],
            "modules": ["./node_modules", "./node_modules"],
            "alias": {
                "sxmServices": path.resolve(__dirname, "../servicelib"),
                "jquery": path.resolve(__dirname, "node_modules/jquery/dist/jquery.min.js"),
                "moment": path.resolve(__dirname, "node_modules/moment/moment.js"),
                "lodash": path.resolve(__dirname, "node_modules/lodash/lodash.min.js"),
                "slick-carousel": path.resolve(__dirname, "node_modules/slick-carousel/slick/slick.js"),
                "sxm-audio-player": path.resolve(__dirname, "node_modules/sxm-audio-player/bin/web-audio-player.js"),
            }
        },
        resolveLoader: {
            "modules": ["./node_modules"],
            moduleExtensions: ['dev-int'],
        },
        entry:  getEntryPoints(env),
        output: {
            "path": path.join(process.cwd(), "dist"),
            "filename": "[name]." + "bundle" + ".[githash].js",
            "chunkFilename": "[id]." + "chunk" + ".[githash].js"
        },
        module: {
            "rules": [
                {
                    "test": /\.js$/,
                    "loader": "source-map-loader",
                    "enforce": "pre",
                },
                {
                    "test": /\.json$/,
                    "type": "javascript/auto",
                    "loader": "file-loader"
                },
                {
                    "test": /\.html$/,
                    "loader": "raw-loader",
                    "exclude": getAppExclusions(env)
                },
                {
                    "test": /\.(eot|svg)$/,
                    "loader": "file-loader?name=[name].[hash:20].[ext]"
                },
                {
                    "test": /\.(jpg|png|gif|otf|ttf|woff|woff2|cur|ani)$/,
                    "loader": "url-loader?name=[name].[hash:20].[ext]&limit=10000"
                },
                {
                    "exclude": [path.join(process.cwd(), "src/styles.scss")],
                    "test": /\.css$/,
                    "use": [
                        "exports-loader?module.exports.toString()",
                        {
                            "loader": "css-loader",
                            "options": {"sourceMap": false, "importLoaders": 1}
                        },
                        {
                            "loader": "postcss-loader",
                            "options": {"ident": "postcss", "plugins": postcssPlugins}
                        }
                    ]
                },
                {
                    "exclude": [path.join(process.cwd(), "src/styles.scss")],
                    "test": /\.scss$|\.sass$/,
                    "use": [
                        "exports-loader?module.exports.toString()",
                        {
                            "loader": "css-loader",
                            "options": {"sourceMap": false, "importLoaders": 1}
                        },
                        {
                            "loader": "postcss-loader",
                            "options": {"ident": "postcss", "plugins": postcssPlugins}
                        },
                        {
                            "loader": "sass-loader",
                            "options": {"sourceMap": false, "precision": 8, "includePaths": []}
                        }
                    ]
                },
                {
                    "exclude": [path.join(process.cwd(), "src/styles.scss")],
                    "test": /\.less$/,
                    "use": [
                        "exports-loader?module.exports.toString()",
                        {
                            "loader": "css-loader",
                            "options": {"sourceMap": false, "importLoaders": 1}
                        },
                        {
                            "loader": "postcss-loader",
                            "options": {"ident": "postcss", "plugins": postcssPlugins}
                        },
                        {
                            "loader": "less-loader",
                            "options": {"sourceMap": false}
                        }
                    ]
                },
                {
                    "exclude": [path.join(process.cwd(), "src/styles.scss")],
                    "test": /\.styl$/,
                    "use": [
                        "exports-loader?module.exports.toString()",
                        {
                            "loader": "css-loader",
                            "options": {"sourceMap": false, "importLoaders": 1}
                        },
                        {
                            "loader": "postcss-loader",
                            "options": {"ident": "postcss", "plugins": postcssPlugins}
                        },
                        {
                            "loader": "stylus-loader",
                            "options": {"sourceMap": false, "paths": []}
                        }
                    ]
                },
                {
                    "include": [path.join(process.cwd(), "src/styles.scss")],
                    "test": /\.css$/,
                    "use": [
                        "style-loader",
                        {
                            "loader": "css-loader",
                            "options": {"sourceMap": false, "importLoaders": 1}
                        },
                        {
                            "loader": "postcss-loader",
                            "options": {"ident": "postcss", "plugins": postcssPlugins}
                        }
                    ]
                },
                {
                    "include": [path.join(process.cwd(), "src/styles.scss")],
                    "test": /\.scss$|\.sass$/,
                    "use": [
                        "style-loader",
                        {
                            "loader": "css-loader",
                            "options": {"sourceMap": false, "importLoaders": 1}
                        },
                        {
                            "loader": "postcss-loader",
                            "options": {"ident": "postcss", "plugins": postcssPlugins}
                        },
                        {
                            "loader": "sass-loader",
                            "options": {"sourceMap": false, "precision": 8, "includePaths": []}
                        }
                    ]
                },
                {
                    "include": [
                        path.join(process.cwd(), "src/styles.scss")
                    ],
                    "test": /\.less$/,
                    "use": [
                        "style-loader",
                        {
                            "loader": "css-loader",
                            "options": {"sourceMap": false, "importLoaders": 1}
                        },
                        {
                            "loader": "postcss-loader",
                            "options": {"ident": "postcss", "plugins": postcssPlugins}
                        },
                        {
                            "loader": "less-loader",
                            "options": {"sourceMap": false}
                        }
                    ]
                },
                {
                    "include": [path.join(process.cwd(), "src/styles.scss")],
                    "test": /\.styl$/,
                    "use": [
                        "style-loader",
                        {
                            "loader": "css-loader",
                            "options": {"sourceMap": false, "importLoaders": 1}
                        },
                        {
                            "loader": "postcss-loader",
                            "options": {"ident": "postcss", "plugins": postcssPlugins}
                        },
                        {
                            "loader": "stylus-loader",
                            "options": {"sourceMap": false, "paths": []}
                        }
                    ]
                },
                {
                    test: /\.ts$/,
                    loader: "@ngtools/webpack",
                }
            ]
        },
        node: {
            "fs": "empty",
            "global": true,
            "crypto": "empty",
            "tls": "empty",
            "net": "empty",
            "process": true,
            "module": false,
            "clearImmediate": false,
            "setImmediate": false
        }
    };

    if (env.analyze) {
        config.stats = {
            maxModules: Infinity,
            optimizationBailout: true
        };
    }

    return config;
};

function getAppExclusions(env)
{
    switch (env.app)
    {
        case "client" :
        {
            return /index\.html$/;
        }
        case "landing":
        {
            return /deepLink\.html$/;
        }
        case "landingCA":
        {
            return /deepLinkCA\.html$/;
        }
        case "portal":
        {
            return /portal\.html$/;
        }
        default :
        {
            throw "Invalid App";
        }
    }
}

function getEntryPoints(env)
{
    switch (env.app)
    {
        case "client" :
        {
            return {
                "polyfills.client": ["./src/polyfills.ts"],
                "styles.client": ["./src/styles.scss"]
            };
        }
        case "landing":
        {
            return {
                "polyfills.deepLink": ["./src/polyfills.ts"],
                "styles.deepLink": ["./src/styles.scss"]
            };
        }
        case "landingCA":
        {
            return {
                "polyfills.deepLinkCA": ["./src/polyfills.ts"],
                "styles.deepLinkCA": ["./src/styles.scss"]
            };
        }
        case "portal":
        {
            return {
                "polyfills.portal": ["./src/polyfills.ts"],
                "styles.portal": ["./src/styles.scss"]
            };
        }
        default :
        {
            throw "Invalid App";
        }
    }
}

function getDevServer(env)
{
    return {
        disableHostCheck     : true,
        "proxy"              : require("./proxy.conf.js")(env),
        "historyApiFallback" : true,
        "host"               : "0.0.0.0",
        inline               : false,
        compress             : true,
        http2                : false
    }
}

function getOptimization(env)
{
    let optimization = {
        splitChunks : { chunks : 'async' },
        noEmitOnErrors: true
    };

    if (env.optimize === "true") {

        const terserOptions = {
            ecma: 5,
            warnings: false,
            parse: {},
            parallel : true,
            compress: {
                ecma : 6,
                arguments : true,
                hoist_funs : true,
                passes: 2,
                drop_debugger : true // this is the default, but set to false to help with debugging mangled code
            },
            mangle: {
                toplevel: true,
                module: true
            },
            module: true,
            output: null,
            toplevel: true,
            nameCache: null,
            ie8: false,
            keep_classnames: false,
            keep_fnames: false,
            safari10: false
        }

        optimization.minimizer = [ new TerserPlugin({ terserOptions: terserOptions }) ];
    }

    return optimization;
}

function getPlugins(env)
{
    var app;
    var module;
    var chunks;
    var vendorChunk;
    var vendorChunkName;

    switch (env.app) {
        case "client" : {
            app = "index.html";
            module = "app/app.module#AppModule";
            chunks = clientChunks;
            vendorChunkName = ["vendor.client"];
            vendorChunk = ["client"];
            break;
        }
        case "landing": {
            app = "deepLink.html";
            module = "landing/landing.module#LandingModule";
            chunks = landingChunks;
            vendorChunkName = ["vendor.deepLink"];
            vendorChunk = ["deepLink"];
            break;
        }
        case "landingCA": {
            app = "deepLinkCA.html";
            module = "landing/landing.module#LandingModule";
            chunks = landingCAChunks;
            vendorChunkName = ["vendor.deepLinkCA"];
            vendorChunk = ["deepLinkCA"];
            break;
        }
        case "portal": {
            app = "portal.html";
            module = "portal/portal.module#PortalModule";
            chunks = portalChunks;
            vendorChunkName = ["vendor.portal"];
            vendorChunk = ["portal"];
            break;
        }
        default : {
            throw "Invalid App";
        }
    }

    const plugins = [
        new webpack.ContextReplacementPlugin(
            /moment[\/\\]locale$/,
            /fr|en/
        ),
        new AotPlugin({
            tsConfigPath : "src/tsconfig.app.json",
            entryModule  : path.resolve(__dirname, 'src/' + module),
            skipCodeGeneration : true
        }),
        new CreateFileWebPack({
            path : './dist/assets/data',
            fileName : 'version.json',
            content : JSON.stringify({
                version : sxmVersionNumber,
                sha : gitHashNumber.trim(),
                hash : gitHashNumber.trim(),
                sha : gitHashNumber.trim(),
                build : teamCityBuildNumber
            })
        }),
        new CreateFileWebPack({
                                  path : './dist',
                                  fileName : 'version.conf',
                                  content : 'set $version "'+ gitHashNumber.substring(0,7) + '"'
                              }),
        new webpack.ProvidePlugin(
            {
                $: "jquery"
            }
        ),
        new webpack.DefinePlugin(
            {
                sxmAppVersion          : JSON.stringify(sxmVersionNumber),
                sxmGitHashNumber       : JSON.stringify(gitHashNumber).trim(),
                sxmTeamCityBuildNumber : JSON.stringify(teamCityBuildNumber),
                iosAppId               : JSON.stringify(usIosAppId),
                iosCAAppId             : JSON.stringify(caIosAppId)
            }
        ),
        new WebpackGitHash(),
        new CopyWebpackPlugin([
            {
                from : "src/assets",
                to   : "assets"
            },
            {
                from : "node_modules/sxm-audio-player/bin/web-audio-player.swf",
                to   : ""
            },
            {
                from : "node_modules/sxm-audio-player/bin/web-audio-player-debug.swf",
                to   : ""
            },
            {
                from : "node_modules/sxm-video-player/dist/flashlsChromeless.swf",
                to   : ""
            },
            {
                from : "src/apple-app-site-association",
                to   : ""
            },
            {
                from: "src/{assets,favicon.ico,i18n,fonts}",
                to: "",
                exclude: "**/.gitkeep"
            }
        ]),
        new ProgressPlugin(),
        new HtmlWebpackPlugin({
            "template"       : "./src/" + app,
            "filename"       : "./" + app,
            "hash"           : false,
            "inject"         : true,
            "compile"        : true,
            "favicon"        : false,
            "minify"         : false,
            "cache"          : true,
            "showErrors"     : true,
            "chunks"         : chunks,
            "excludeChunks"  : [ 'dev-int' ],
            "title"          : "Webpack App",
            "xhtml"          : true,
            "chunksSortMode" : function sort(left, right)
            {
                let leftIndex  = chunks.indexOf(left.names[ 0 ]);
                let rightindex = chunks.indexOf(right.names[ 0 ]);
                if (leftIndex > rightindex)
                {
                    return 1;
                }
                else if (leftIndex < rightindex)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }),
        WebpackPwaManifest.getProdPlugin(),
        ...WebpackPwaManifest.getDevPlugins(),
        new WorkboxWebpackPlugin.InjectManifest({
            swSrc: path.join(process.cwd(), 'src/src-sw.js'),
            swDest: "sw.js",
            maximumFileSizeToCacheInBytes: 50,
            include:[
                /offline\.html$/,
            ],
            exclude: [
                /\.map$/,
                /manifest$/,
                /\.htaccess$/,
                /src-sw\.js$/,
                /sw\.js$/,
            ],
        })
    ];

    return plugins;
}

function postcssPlugins()
{
    const postcssUrl   = require('postcss-url');
    const autoprefixer = require('autoprefixer');
    const cssnano      = require('cssnano');

    const minimizeCss = false;
    const baseHref    = "";
    const deployUrl   = "";

    // safe settings based on: https://github.com/ben-eb/cssnano/issues/358#issuecomment-283696193
    const importantCommentRe = /@preserve|@license|[@#]\s*source(?:Mapping)?URL|^!/i;
    const minimizeOptions    = {
        autoprefixer    : false,
        safe            : true,
        mergeLonghand   : false,
        discardComments : { remove : (comment) => !importantCommentRe.test(comment) }
    };
    return [
        postcssUrl({
            url : (URL) =>
            {
                // Only convert root relative URLs, which CSS-Loader won't process into require().
                if (!URL.startsWith('/') || URL.startsWith('//'))
                {
                    return URL;
                }
                if (deployUrl.match(/:\/\//))
                {
                    // If deployUrl contains a scheme, ignore baseHref use deployUrl as is.
                    return `${deployUrl.replace(/\/$/, '')}${URL}`;
                }
                else if (baseHref.match(/:\/\//))
                {
                    // If baseHref contains a scheme, include it as is.
                    return baseHref.replace(/\/$/, '') +
                        `/${deployUrl}/${URL}`.replace(/\/\/+/g, '/');
                }
                else
                {
                    // Join together base-href, deploy-url and the original URL.
                    // Also dedupe multiple slashes into single ones.
                    return `/${baseHref}/${deployUrl}/${URL}`.replace(/\/\/+/g, '/');
                }
            }
        }),
        autoprefixer(),
    ].concat(minimizeCss ? [ cssnano(minimizeOptions) ] : []);
}

function assetTagPolyFill(assets) {
    // Turn script files into script tags
    var scripts = assets.js.map(function (scriptPath) {
        return {
            tagName: 'script',
            closeTag: true,
            attributes: {
                type: 'application/javascript',
                src: scriptPath
            }
        };
    });
    // Make tags self-closing in case of xhtml
    var selfClosingTag = !!this.options.xhtml;
    // Turn css files into link tags
    var styles = assets.css.map(function (stylePath) {
        return {
            tagName: 'link',
            selfClosingTag: selfClosingTag,
            attributes: {
                href: stylePath,
                rel: 'stylesheet'
            }
        };
    });
    // Injection targets
    var head = [];
    var body = [];

    // If there is a favicon present, add it to the head
    if (assets.favicon) {
        head.push({
                      tagName: 'link',
                      selfClosingTag: selfClosingTag,
                      attributes: {
                          rel: 'shortcut icon',
                          href: assets.favicon
                      }
                  });
    }
    // Add styles to the head
    head = head.concat(styles);
    // Add scripts to body or head
    if (this.options.inject === 'head') {
        head = head.concat(scripts);
    } else {
        body = body.concat(scripts);
    }
    return {head: head, body: body};
};
