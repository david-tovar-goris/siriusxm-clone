import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { LandingModule } from "./landing/landing.module";

enableProdMode();

platformBrowserDynamic().bootstrapModule(LandingModule);
