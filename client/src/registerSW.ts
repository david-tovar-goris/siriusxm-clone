import {Logger} from "sxmServices";

export const registerServiceWorker = () =>
{
    const logger = Logger.getLogger("Bootstrap");
    const serviceWorker = 'serviceWorker';

    if (!(serviceWorker in navigator))
    {
        return;
    }

    window.navigator.serviceWorker
        .register(`sw.js`, {scope: '/'})
        .then(registration => logger.info(`Service worker registration succeeded`))
        .catch(error => logger.info(`Service worker registration failed`));
};
