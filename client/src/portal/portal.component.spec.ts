import {
    async,
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { CommonModule } from "@angular/common";
import { PortalComponent } from "./portal.component";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../test/mocks/translate.service";

describe("PortalComponent", () =>
{
    let component: PortalComponent;
    let fixture: ComponentFixture<PortalComponent>;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            imports     : [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
                HttpModule,
                CommonModule,
                TranslateModule
            ],
            declarations: [
                PortalComponent
            ],
            providers   : [
                 { provide: TranslateService, useValue: MockTranslateService.getSpy() }
            ]
        }).compileComponents();
    }));

    beforeEach(() =>
    {
        fixture   = TestBed.createComponent(PortalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () =>
    {
        expect(component instanceof PortalComponent).toBe(true);
    });
});
