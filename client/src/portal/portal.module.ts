import {
    Logger,
    providers,
    injectAppConfig,
    IAppConfig
} from "sxmServices";
import { PortalComponent } from "./portal.component";
import { NgModule, InjectionToken } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { getPortalServiceConfig } from "./portal.service.config.web";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import {
    HttpClient,
    HttpClientModule
} from "@angular/common/http";
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from "@ngx-translate/core";

export const APP_CONFIG   = new InjectionToken<IAppConfig>("app.config");
let serviceConfigProvider = { provide: APP_CONFIG, useValue: getPortalServiceConfig() };

/**
 * Creates the translation loader.
 *
 * NOTE: This method is required to be exported to work with AOT compilation builds.
 *
 * @param {Http} http - The HTTP service to load the JSON translation bundles.
 * @returns {TranslateHttpLoader}
 */
export function createTranslateLoader(http: HttpClient)
{
    return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

injectAppConfig(serviceConfigProvider);

@NgModule({
    declarations: [
        PortalComponent
    ],
    imports     : [
        BrowserModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide   : TranslateLoader,
                useFactory: createTranslateLoader,
                deps      : [ HttpClient ]
            }
        })
    ],
    providers   : providers,
    bootstrap   : [ PortalComponent ],
    exports     : [ PortalComponent ]
})

export class PortalModule
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("PortalModule");

    /**
     * Constructor
     * @param  translate - module used to set up translation
     */
    constructor(private translate: TranslateService)
    {
        translate.addLangs([
            "en",
            "fr",
            "en-ca"
        ]);
        translate.setDefaultLang("en");

        let browserLang = translate.getBrowserLang();
        const lang      = browserLang.match(/en|fr|en-ca/) ? browserLang : "en";
        PortalModule.logger.debug(`Constructor( Current locale: ${lang} )`);
        translate.use(lang);
    }
}
