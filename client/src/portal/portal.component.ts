import {
    Component
} from "@angular/core";
import {
    Logger
} from "sxmServices";

/**
 * @MODULE:     Portal App
 * @CREATED:    07/14/21
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     PortalComponent used to Profile portal.
 */
@Component({
    moduleId   : module.id,
    selector   : "app-sxm-portal-client",
    templateUrl: "portal.component.html",
    styleUrls  : [ "portal.component.scss" ],
    providers  : []
})

export class PortalComponent
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("PortalComponent");

    /**
     * Constructor
     */
    constructor()
    {
    }
}
