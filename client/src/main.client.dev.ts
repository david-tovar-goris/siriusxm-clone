import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { AppModule } from "./app/app.module";
import { registerServiceWorker } from "./registerSW";

enableProdMode();

platformBrowserDynamic().bootstrapModule(AppModule)
    .then(() =>
    {
        registerServiceWorker();
    });
