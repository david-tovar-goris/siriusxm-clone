import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Input
} from "@angular/core";
import { ContextualService, IContextualData, DeepLinkTypes, IRelativeUrlSetting, relativeUrlToAbsoluteUrl } from "sxmServices";
import * as moment from "moment";
import { TranslateService } from "@ngx-translate/core";
import { AutoUnSubscribe } from "app/common/decorator";

/**
 * @MODULE:     client
 * @CREATED:    06/02/18
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     ContextualDataComponent used to load contextual data
 */

@Component({
    selector   : "contextual-data-control",
    templateUrl: "./contextual-data.component.html",
    styleUrls  : [ "./contextual-data.component.scss" ]
})

@AutoUnSubscribe()
export class ContextualDataComponent implements OnInit
{
    /**
     * Emits the boolean value indicates that the deep liink exists or not
     * @type {EventEmitter<boolean>}
     */
    @Output()
    deepLinkExists: EventEmitter<boolean> = new EventEmitter<boolean>();

    @Input()
    relativeUrls: Array<IRelativeUrlSetting>;
    /**
     * Contextual data used to populate the view
     */
    private contextualData: IContextualData = {} as IContextualData;

    public get isLiveVideo() : boolean
    {
        return this.contextualData.deepLinkType === DeepLinkTypes.LIVE_VIDEO;
    }

    /**
     * AutoUnSubscribe will use this to unsubscribe from any observable subscriptions that may prevent the
     * component from being garbage collected
     */
    private subscriptions = [];

     /**
     * Constructor
     * @param {TranslateService} translate
     * @param {ContextualService} contextualService
     */
    constructor(private translate: TranslateService,
                private contextualService: ContextualService)
    {

    }

    /**
     * initialization
     */
    ngOnInit()
    {
        this.contextualService.getContextualPDT();
        this.subscriptions.push(
            this.contextualService.contextualData.subscribe((response: IContextualData) =>
            {
                this.deepLinkExists.emit(response ? true : false);
                this.contextualData = response ? response : {} as IContextualData;

                this.contextualData.imageUrl = this.contextualData.imageUrl ?
                    relativeUrlToAbsoluteUrl(this.contextualData.imageUrl, this.relativeUrls) : "";
            }));
    }

    /**
     * gets the date label for a AOD type
     * @returns {string}
     */
    public getDescriptionPrefix(): string
    {
        const expirationDate  = this.contextualData.expirationDate;
        const originalAirDate = this.contextualData.originalAirDate;
        if (!expirationDate && !originalAirDate)
        {
            return "";
        }
        const specialString = this.translate.instant("landing.special").toUpperCase() + " - ";
        if (this.contextualData.isSpecial)
        {
            return specialString;
        }

        const originalAirDateLabel = this.createAirDateLabel(originalAirDate);
        const expDateLabel         = this.createExpDateLabel(expirationDate);
        if (expDateLabel)
        {
            return expDateLabel + " - ";
        }
        else if (originalAirDateLabel)
        {
            return originalAirDateLabel + " - ";
        }
        else
        {
            return specialString;
        }
    }

    /**
     * Creates the original air date label
     * @param {string} originalAirDate
     * @returns {string}
     */
    private createAirDateLabel(originalAirDate: string): string
    {
        let airDate;

        const date        = moment(originalAirDate),
              presentDate = moment(),
              yesterDay   = moment().subtract(1, 'day'),
              airDateText = this.translate.instant("landing.airDate").toUpperCase();

        if (!originalAirDate)
        {
            return "";
        }

        if (date.isSame(presentDate, 'day'))
        {
            airDate = this.translate.instant("landing.notificationToday");
        }
        else if (date.isSame(yesterDay, 'day'))
        {
            airDate = this.translate.instant("landing.notificationYesterday");
        }
        else if (date.isSame(presentDate, 'week'))
        {
            airDate = date.format('D').toUpperCase();
        }
        else
        {
            airDate = airDateText + " " + date.format('MM/DD/YYYY');
        }

        return airDate.replace(/\//g, "\.");
    }

    /**
     * creates the expiration date label
     * @param {string} expirationDate
     * @returns {string}
     */
    private createExpDateLabel(expirationDate: string): string
    {
        const date         = moment(expirationDate),
              duration     = moment.duration(date.diff(moment())),
              expTimeHours = duration.asHours(),
              expDateText  = this.translate.instant("landing.expire");

        if (!expirationDate)
        {
            return "";
        }

        if (expTimeHours > 0 &&
            expTimeHours <= 24)
        {
            return expDateText + " " + Math.ceil(expTimeHours) + this.translate.instant("landing.hours").toUpperCase();
        }

        return "";
    }
}
