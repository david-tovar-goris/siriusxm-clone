import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { NoResultsModule } from "../no-results/no-results.module";
import { TranslateModule } from "@ngx-translate/core";
import { RouterModule } from "@angular/router";
import { ContextualDataComponent } from "./contextual-data.component";

@NgModule({
    imports     : [
        CommonModule,
        FormsModule,
        HttpModule,
        NoResultsModule,
        TranslateModule,
        RouterModule
    ],
    declarations: [
        ContextualDataComponent
    ],
    exports     : [ ContextualDataComponent ]
})

export class ContextualDataModule
{
}
