import {
    Component,
    Input,
    ViewContainerRef
} from "@angular/core";

import { LoginService }              from "./services/login.service";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

/**
 * @MODULE:     client
 * @CREATED:    07/11/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     LoginComponent used to load login page
 */

@Component({
    selector: "login-component",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"]
})

export class LoginComponent
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    /**
     * Internal model object - used to get data from view
     */
    public model: {
        password: string,
        username: string
    } = {
        password: "",
        username: ""
    };

    @Input() closeLoginOverLayFunction?: Function;
    @Input() viewContainerRef: ViewContainerRef;

    /**
     * Constructor
     * @param loginService
     */
    constructor(public loginService: LoginService){}

}
