import {
    TestBed
} from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { LoginComponent } from "./login.component";
import {
    TranslateModule
} from "@ngx-translate/core";
import { LoginService } from "./services/login.service";
import { LoginServiceMock } from "../../../../test/login.service.mock";
import { By } from "@angular/platform-browser";
import { MockDirective } from "../../../../test/mocks/component.mock";

describe("LoginComponent", function()
{
    beforeEach(function()
    {
        TestBed.configureTestingModule({
            imports: [
                TranslateModule.forRoot(),
                FormsModule
            ],
            declarations: [
                LoginComponent,
                MockDirective({
                    selector: "[sxm-new-analytics]",
                    inputs: [ "tagName", "userPath", "userPathScreenPosition", "pageFrame", "elementType", "elementPosition",
                        "buttonName", "tagText", "skipCurrentPosition", "action", "inputType"]})
            ],
            providers: [
                { provide: LoginService, useClass: LoginServiceMock }
            ]
        });
        this.fixture = TestBed.createComponent(LoginComponent);
        this.component = this.fixture.debugElement.componentInstance;
        this.loginButton = this.fixture.debugElement.query(By.css('.login-button'));
    });

    afterEach(function()
    {
        this.fixture.destroy();
    });

    it('should create the Component and initially, login button is disabled when username or password are empty', function()
    {
        this.fixture.detectChanges();
        expect(this.component).toBeTruthy();
        expect(this.loginButton.nativeElement.disabled).toEqual(true);
    });

    it('login button should be enabled when username and password are provided', function()
    {
        this.fixture.detectChanges();
        this.component.model = { username: 'sample_username', password: 'sample_password'};
        this.fixture.detectChanges();
        expect(this.loginButton.nativeElement.disabled).toEqual(false);
    });

    it('Should call loginService login() method when button is clicked', function()
    {
        this.fixture.detectChanges();
        this.component.model = { username: 'sample_username', password: 'sample_password'};
        this.fixture.detectChanges();
        this.fixture.debugElement.query(By.css('form')).triggerEventHandler('submit', null);
        expect(this.component.loginService.login).toHaveBeenCalled();
    });
});
