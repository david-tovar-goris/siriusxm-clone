import { ApiCodes, ISession, AppErrorCodes, apiToFault, FaultCode }         from "sxmServices";
import { Injectable, ViewContainerRef }                         from "@angular/core";
import { TranslateService }                                     from "@ngx-translate/core";
import { OverlayService }                                       from "../../../common/service/overlay/overlay.service";
import { ModalService }                                         from "../../../common/service/modal/modal.service";
import { FlepzScreenService }                                   from "../../../common/service/flepz/flepz-screen.service";
import * as _ from "lodash";

@Injectable()
export class LoginErrorMessagesService
{
    /**
     * loginErrorMessage displayed if any issues with authenticate the user
     */
    public get getLoginErrorMessage(): string
    {
        return this.loginErrorMessage;
    }

    public set setLoginErrorMessage(message: string)
    {
        this.loginErrorMessage = message || '';
    }

    private loginErrorMessage: string = '';

    constructor(private translate: TranslateService,
                private overlayService: OverlayService,
                private modalService: ModalService,
                private flepzScreenService: FlepzScreenService){}

    /**
     * Checks the API error code and displays the relative message
     */
    public displayErrorMessage(code, viewContainerRef: ViewContainerRef, session: ISession): void
    {
        session.loginAttempts += 1;
        let translationObject :any;
        let modalData :any;
        switch (code)
        {
            case ApiCodes.INVALID_CREDENTIALS:

                this.loginErrorMessage = this.translate.instant('login.errors.invalidLogin');
                if (session.loginAttempts >= 4)
                {
                    modalData = this.translate.instant('login.errors.modals.multipleInvalidAttempts');
                    modalData.errorCode = AppErrorCodes.FLTT_BAD_USERNAME_PASSWORD_AFTER_3.faultCode;
                    this.modalService.addDynamicModal(modalData);
                }
                break;

            case ApiCodes.ACCOUNT_LOCKED:
                translationObject = this.translate.instant('login.errors.modals.accountLocked');
                modalData = {
                        ...translationObject,
                        description: this.replaceMinutesAndSeconds(translationObject, session)
                    };
                modalData.errorCode = _.get(apiToFault.get(code), "faultCode", "");
                modalData.buttonOne.action = this.openForgotUsernamePasswordPage.bind(this);
                this.modalService.addDynamicModal(modalData);
                break;

            case ApiCodes.OAC_PASSWORD:

                modalData = this.translate.instant('login.errors.modals.oacLoginAttempt');
                modalData.errorCode = session.loginAttempts > 3
                    ? AppErrorCodes.FLTT_BAD_USERNAME_PASSWORD_AFTER_3.faultCode
                    : AppErrorCodes.FLTT_BAD_USERNAME_PASSWORD_FIRST_3.faultCode;
                modalData.buttonOne.action = this.openForgotUsernamePasswordPage.bind(this);
                this.modalService.addDynamicModal(modalData);
                break;

            case ApiCodes.EXPIRED_SUBSCRIPTION:
                modalData = this.translate.instant('login.errors.modals.expiredSubscription');
                modalData.errorCode = _.get(apiToFault.get(code), "faultCode", "");
                this.modalService.addDynamicModal(modalData);
                break;

            case ApiCodes.SR_REQ_FORBIDDEN:
                translationObject = this.translate.instant('login.errors.modals.errForbidden');
                modalData = {
                    ...translationObject
                };
                modalData.errorCode = _.get(apiToFault.get(code), "faultCode", "");
                modalData.buttonOne.action = this.openForgotUsernamePasswordPage.bind(this);
                this.modalService.addDynamicModal(modalData);
                break;

            default:
                this.loginErrorMessage = this.translate.instant('login.errors.invalidLogin');
        }
    }

    /**
     * Replaces Minutes and Seconds in the given string
     * @param translation
     * @returns {Object}
     */
    private replaceMinutesAndSeconds(translation, session: ISession): object
    {
        let lockoutSeconds = session.remainingLockoutSeconds === 0 ? "00" : session.remainingLockoutSeconds;
        return translation.description.replace(/{{ minutes }}/g, session.remainingLockoutMinutes)
            .replace(/{{ seconds }}/g, lockoutSeconds);
    }

    private openForgotUsernamePasswordPage(): void
    {
        this.flepzScreenService.enableIframe('forgotUsernamePassword');
    }
}
