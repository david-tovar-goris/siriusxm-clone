import { ISession }         from "sxmServices";
import { ViewContainerRef }                         from "@angular/core";
import { DomSanitizer }                             from '@angular/platform-browser';
import { LoginErrorMessagesService }                            from "./login-error-messages.service";
import { OverlayService }                                       from "../../../common/service/overlay/overlay.service";
import { ModalService }                                         from "../../../common/service/modal/modal.service";
import { FlepzScreenService }                                   from "../../../common/service/flepz/flepz-screen.service";

import { mock } from "ts-mockito";

interface ThisContext {
    loginErrorMessageService: LoginErrorMessagesService;
    iSessionMock:ISession;
    translate: any;
    overlayService: OverlayService;
    modalService: ModalService;
    flepzScreenService: FlepzScreenService;
    viewContainerRef: ViewContainerRef;
}

describe("Login Error Service Test Suite: ", function()
{
    describe("Display Error message for respective errors", function(this: ThisContext)
    {
        beforeEach(function(this: ThisContext)
        {
            this.translate = {
                instant:(key)=>
                {
                    return {
                        buttonOne:{action:""}
                    };
                }
            };
            this.overlayService = new OverlayService();
            this.modalService = mock(ModalService);
            this.flepzScreenService = new FlepzScreenService(mock(DomSanitizer));

            this.iSessionMock = {
                loginAttempts: 2
            } as ISession;

            this.viewContainerRef = mock(ViewContainerRef);
            this.loginErrorMessageService = new LoginErrorMessagesService(  this.translate,
                                                                            this.overlayService,
                                                                            this.modalService,
                                                                            this.flepzScreenService);
        });

        it("should display invalid credential error message", function()
        {
             let spy = spyOn(this.loginErrorMessageService, "displayErrorMessage").and.callThrough();
             this.loginErrorMessageService.displayErrorMessage(101, this.viewContainerRef,this.iSessionMock);
             expect(spy).toHaveBeenCalledWith(101,this.viewContainerRef,this.iSessionMock);
        });

        it("should display invalid attempt overlay", function()
        {
            this.iSessionMock.loginAttempts = 4;
            let spy = spyOn(this.loginErrorMessageService, "displayErrorMessage").and.callThrough();
            this.loginErrorMessageService.displayErrorMessage(101, this.viewContainerRef,this.iSessionMock);
            expect(spy).toHaveBeenCalledWith(101,this.viewContainerRef,this.iSessionMock);
        });

        it("should display Account locked overlay for 403/Forbidden response", function()
        {
            let spy = spyOn(this.loginErrorMessageService, "displayErrorMessage").and.callThrough();
            this.loginErrorMessageService.displayErrorMessage(403, this.viewContainerRef,this.iSessionMock);
            expect(spy).toHaveBeenCalledWith(403,this.viewContainerRef,this.iSessionMock);
        });

        it("should display Account locked overlay for invalid multiple attempt", function()
        {
            spyOn(this.loginErrorMessageService,"replaceMinutesAndSeconds").and.returnValue("");
            let spy = spyOn(this.loginErrorMessageService, "displayErrorMessage").and.callThrough();
            this.loginErrorMessageService.displayErrorMessage(102, this.viewContainerRef,this.iSessionMock);
            expect(spy).toHaveBeenCalledWith(102,this.viewContainerRef,this.iSessionMock);
        });

        it("should display OAC Password error message overlay(greater than 3 attempts)", function()
        {
            this.iSessionMock.loginAttempts = 4;
            let spy = spyOn(this.loginErrorMessageService, "displayErrorMessage").and.callThrough();
            this.loginErrorMessageService.displayErrorMessage(105, this.viewContainerRef,this.iSessionMock);
            expect(spy).toHaveBeenCalledWith(105,this.viewContainerRef,this.iSessionMock);
        });

        it("should display OAC Password error message overlay(less than 3 attempts)", function()
        {
            this.iSessionMock.loginAttempts = 2;
            let spy = spyOn(this.loginErrorMessageService, "displayErrorMessage").and.callThrough();
            this.loginErrorMessageService.displayErrorMessage(105, this.viewContainerRef,this.iSessionMock);
            expect(spy).toHaveBeenCalledWith(105,this.viewContainerRef,this.iSessionMock);
        });

        it("should display Expired Subscription overlay", function()
        {
            let spy = spyOn(this.loginErrorMessageService, "displayErrorMessage").and.callThrough();
            this.loginErrorMessageService.displayErrorMessage(5230, this.viewContainerRef,this.iSessionMock);
            expect(spy).toHaveBeenCalledWith(5230,this.viewContainerRef,this.iSessionMock);
        });

    });
});
