import { Inject, Injectable, ViewContainerRef } from "@angular/core";
import { Location } from "@angular/common";
import { appRouteConstants } from "../../../app.route.constants";
import { APP_CONFIG } from "../../../sxmservicelayer/sxm.service.layer.module";
import { SplashScreenService } from "../../../common/service/splash-screen/splash-screen.service";
import { LoginErrorMessagesService } from "./login-error-messages.service";
import { SubscriptionLike as ISubscription } from "rxjs";
import { filter, take } from "rxjs/operators";
import {
    IAuthenticationResponse,
    AuthenticationService,
    IAppConfig,
    InitializationService,
    InitializationStatusCodes,
    ISession,
    Logger,
    openAccessStatus,
    Session,
    StorageService,
    ConfigService,
    HTTP_ERROR_MESSAGE,
    AppMonitorService,
    OnBoardingSettings
}                             from "sxmServices";
import { TranslateService }   from "@ngx-translate/core";
import {languageConstants} from "../../../common/component/language-toggle/language.consts";

@Injectable()
export class LoginService
{
    private static logger: Logger = Logger.getLogger("LoginService");
    private loading: boolean = false;

    /**
     * Calls the LoginErrorMessageService's getter to retrieve the error message string
     * @returns {string}
     */
    public get loginErrorMessage(): string
    {
        return this.loginErrorMessageService.getLoginErrorMessage;
    }

    /**
     * Getter for the session
     * @returns {ISession}
     */
    public get userSession(): ISession
    {
        return this.session;
    }

    /**
     * Determines whether to use the Open Access translation key or the regular Login page key
     * @returns {string}
     */
    public get signInTranslationKey(): string
    {
        return this.session.openAccessStatus === openAccessStatus.INELIGIBLE ? "openAccess.postOA.signIn" : "login.signIn";
    }

    public customerAgreementUrl:string;
    public forgotPasswordUrl:string;
    public privacyPolicyUrl:string;
    public getStartedUrl:string;
    public dataPrivacyUrl:string;
    public appDownloadUrls = { apple: "", android: "", windows: ""};

    /**
     * Reference to the view container of the component that called `login` in order to create error messages
     */
    public viewContainerRef: ViewContainerRef;

    private closeLoginOverLayFunction: Function;

    constructor(private authenticationService: AuthenticationService,
                private session: Session,
                private splashScreenService: SplashScreenService,
                private initializeService: InitializationService,
                private loginErrorMessageService: LoginErrorMessagesService,
                private location: Location,
                private storageService: StorageService,
                private configService: ConfigService,
                private translate: TranslateService,
                private appMonitorService: AppMonitorService,
                @Inject(APP_CONFIG) public appConfig: IAppConfig)
    {
        this.observeSession();
        this.observeInitService();
    }

    /**
     * login used to authenticate user to login to the application using authentication service
     */
    public login(viewContainerRef: ViewContainerRef, userName: string, password: string, closeLoginOverLayFunction?: Function): void
    {
        this.viewContainerRef = viewContainerRef;
        LoginService.logger.debug(`login( ${userName} )`);
        this.loading = true;
        this.closeLoginOverLayFunction = closeLoginOverLayFunction;

        this.splashScreenService.addDynamicSplashScreen(this.viewContainerRef);

        this.authenticationService.login(userName, password)
            .subscribe(this.onLoginSuccess.bind(this), this.onLoginFailure.bind(this));
    }

    /**
     * Determines whether the login button should be disabled
     * @returns {boolean}
     */
    public disableLoginButton(userName: string, password: string): boolean
    {
        return (!password || !userName)
            || (password.length < 6 || userName.length < 2)
            || this.loading;
    }

    /**
     * Calls the LoginErrorMessageService's setter to clear the error message string
     */
    public clearLoginErrorMessage(): void
    {
        this.loginErrorMessageService.setLoginErrorMessage = '';
    }

    /**
     * determines if the given URL can be opened in iFrame if it is https based else new tab
     */
    public getTarget(url, httpsTarget) : string
    {
        return (url && url.length>0 && url.match(/https:/)) ? httpsTarget:"_blank";
    }

    private onLoginSuccess(response: IAuthenticationResponse)
    {
        this.clearLoginErrorMessage();

        if (response !== null && response.authenticationData)
        {
            this.loading = false;

            if (this.closeLoginOverLayFunction)
            {
                this.closeLoginOverLayFunction();
            }

            // If the user started OA and decide to sign in to the app then we are redirecting Home and refreshing app
            if (this.initializeService.state === InitializationStatusCodes.RUNNING)
            {
                /*
                    If successful login from the open access overlay if there's a query string from a deep link,
                    route there instead of home before refreshing the app. This maintains the deep link. If there's
                    no query string then route to home and refresh the app.
                 */
                const queryString:string = this.appConfig.contextualInfo.queryString;
                const rootWithQueryString:string = "/?" + queryString;

                if (queryString && queryString !== "")
                {
                    this.location.go(rootWithQueryString);
                }
                else
                {
                    this.location.go(appRouteConstants.HOME);
                }

                this.initializeService.reload(() => { location.reload(); });
            }
        }
    }

    private onLoginFailure(error)
    {
        LoginService.logger.warn(`loginFault( Error: ${error.message} )`);
        this.loading = false;
        if(error.message !== HTTP_ERROR_MESSAGE)
        {
            this.loginErrorMessageService.displayErrorMessage(error.code, this.viewContainerRef, this.session);
        }
        this.splashScreenService.closeSplashScreen();
    }

    private observeSession(): void
    {
        this.authenticationService.userSession.subscribe(data =>
        {
            this.session = data;
        });
    }

    private observeInitService(): ISubscription
    {
        this.configService.configuration.pipe(filter(config => !!config),take(1)).subscribe(config =>
        {
            const onBoardingSettings: OnBoardingSettings = this.configService.getOnBoardingSettings(
                this.translate.currentLang === languageConstants.ENGLISH_CANADA ? languageConstants.ENGLISH : this.translate.currentLang
            );

            this.appDownloadUrls      = this.configService.getAppDownloadUrls();
            this.getStartedUrl        = onBoardingSettings.getStartedUrl;
            this.customerAgreementUrl = onBoardingSettings.customerAgreementUrl;
            this.privacyPolicyUrl     = onBoardingSettings.privacyPolicyUrl;
            this.forgotPasswordUrl    = onBoardingSettings.passwordRecoveryUrl;
            this.dataPrivacyUrl         = onBoardingSettings.dataPrivacyUrl;

            if (this.forgotPasswordUrl === "")
            {
                this.forgotPasswordUrl = (this.appConfig.deviceInfo.appRegion === "US")
                    ? "https://streaming.siriusxm.com/?src=everestplayer&recoverlogin=true&hideheader=true"
                    : "http://www.siriusxm.ca/player-activation-help/";
            }
        });

        return this.initializeService.initState.subscribe((state: string) =>
        {
        });
    }
}
