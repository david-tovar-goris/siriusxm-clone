import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { AuthenticationGuard } from "./guard/index";

import { LoginComponent } from "./login/login.component";
import { SharedModule } from "../common/shared.module";
import { TranslationModule } from "../translate/translation.module";
import { Session } from "sxmServices";
import { ContextualDataModule } from "../contextual-data/contextual-data.module";
import { LoginErrorMessagesService } from "./login/services/login-error-messages.service";
import { LoginService } from "./login/services/login.service";
import { SettingsService } from "../common/service/settings/settings.service";
import { LoginPageComponent } from "./login-page/login-page.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        SharedModule,
        TranslationModule,
        ContextualDataModule
    ],
    declarations: [
        LoginComponent,
        LoginPageComponent
    ],
    providers: [
        AuthenticationGuard,
        Session,
        LoginErrorMessagesService,
        LoginService,
        SettingsService
    ],
    exports: [
        LoginComponent
    ]
})
export class AuthenticationModule
{
}
