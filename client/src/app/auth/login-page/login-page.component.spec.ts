/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />

import {
    getTestBed,
    TestBed
} from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { ContextualService} from "sxmServices";
import { LoginPageComponent } from "./login-page.component";
import {
    TranslateModule,
    TranslateService
} from "@ngx-translate/core";
import { OverlayService } from "../../common/service/overlay/overlay.service";
import { OverlayServiceMock } from "../../../../test/mocks/overlay.service.mock";
import { of as observableOf } from "rxjs";
import { MockComponent, MockDirective } from "../../../../test/mocks/component.mock";
import { ContextualServiceMock } from "../../../../test/mocks/contextual.service.mock";
import { FlepzScreenService } from "../../common/service/flepz/flepz-screen.service";
import { FlepzScreenServiceMock } from "../../../../test/mocks/flepz-screen.service.mock";
import { LoginService } from "../login/services/login.service";
import { LoginServiceMock } from "../../../../test/login.service.mock";
import { SettingsService } from "../../common/service/settings/settings.service";
import { SettingsServiceMock } from "../../../../test/mocks/settings.service.mock";
import { APP_CONFIG, SXMServiceLayerModule } from "../../sxmservicelayer/sxm.service.layer.module";
import { By } from "@angular/platform-browser";

describe("LoginPageComponent", function()
{
    beforeEach(function()
    {
        TestBed.configureTestingModule({
            imports: [
                TranslateModule.forRoot(),
                FormsModule,
                SXMServiceLayerModule
            ],
            declarations: [
                LoginPageComponent,
                MockComponent({ selector: "contextual-data-control" }),
                MockComponent({ selector: "language-toggle" }),
                MockComponent({ selector: "login-component", inputs: [ "viewContainerRef", "closeLoginOverLayFunction"]}),
                MockDirective({
                    selector: "[sxm-new-analytics]",
                    inputs: [ "tagName", "userPath", "userPathScreenPosition", "pageFrame", "elementType", "elementPosition",
                        "buttonName", "linkName", "tagText", "skipCurrentPosition", "action", "inputType"]})
            ],
            providers: [
                { provide: OverlayService, useValue: OverlayServiceMock.getSpy() },
                { provide: ContextualService, useValue: ContextualServiceMock.getSpy() },
                { provide: FlepzScreenService, useClass: FlepzScreenServiceMock },
                { provide: LoginService, useClass: LoginServiceMock },
                { provide: SettingsService, useClass: SettingsServiceMock }
            ]
        });
        this.injector = getTestBed();
        this.overlayService = this.injector.get(OverlayService);
        this.translate = this.injector.get(TranslateService);
        this.fixture = TestBed.createComponent(LoginPageComponent);
        this.component = this.fixture.componentInstance;
    });

    afterEach(function()
    {
        this.fixture.destroy();
    });

    it('should create the Component and render all links properly', function()
    {
        this.fixture.detectChanges();
        expect(this.component).toBeTruthy();
        expect(this.fixture.debugElement.queryAll(By.css('.app-store-link')).length).toEqual(3);
    });

    it('render href properties of footer links correctly', function()
    {
        this.fixture.detectChanges();
        this.footerLinks = this.fixture.debugElement.queryAll(By.css('footer a'));
        expect(this.footerLinks.length).toEqual(3);
        expect(this.footerLinks[0].properties.href).toEqual("customerAgreementUrl");
        expect(this.footerLinks[1].properties.href).toEqual("privacyPolicyUrl");
        expect(this.footerLinks[2].properties.href).toEqual("dataPrivacyUrl");
    });

    it('opens Locating You Overlay on calling openOverlay()', function()
    {
        spyOn(this.translate, 'get').and.returnValue(observableOf({
            header: 'Header',
            description: 'description',
            buttonOneText: 'buttonOneText',
            buttonTwoText: null,
            screen: 'locationPolicy'
        }));
        this.component.openOverlay();
        expect(this.component.overlayService.open).toHaveBeenCalled();
    });
});
