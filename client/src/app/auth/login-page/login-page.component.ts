import {
    Component,
    OnInit,
    OnDestroy,
    ViewContainerRef,
    Inject
} from "@angular/core";

import {
    ASSET_NAME_CONST,
    IAppConfig,
    AppErrorCodes,
    AppMonitorService,
    StorageService,
    KochavaAnalyticsService,
    ConfigService,
    OnBoardingSettings
} from "sxmServices";

import { OverlayService }                                                                 from "../../common/service/overlay/overlay.service";
import { OverlayData }                                                                    from "../../common/component/overlay/overlay.interface";
import { TranslateService }                                                               from "@ngx-translate/core";
import { FlepzScreenService }                                                             from "../../common/service/flepz/flepz-screen.service";
import { APP_CONFIG }                from "../../sxmservicelayer/sxm.service.layer.module";
import { LoginService }              from "../login/services/login.service";
import { SettingsService }           from "../../common/service/settings/settings.service";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import {languageConstants} from "app/common/component/language-toggle/language.consts";

/**
 * @MODULE:     client
 * @CREATED:    07/11/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     LoginComponent used to load login page
 */

@Component({
    templateUrl: "./login-page.component.html",
    styleUrls: ["./login-page.component.scss"]
})

export class LoginPageComponent implements OnInit, OnDestroy
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    /**
     * overlay text data
     */
    overlayText: OverlayData = {
        header: '',
        description: '',
        buttonOneText: '',
        buttonTwoText: '',
        screen: ''
    };

    /**
     * flag detects the locating you
     * @type {boolean}
     */
    public locatingYouVisited: boolean = false;

    /**
     * detects the flag if deep link exists or not.
     * @type {boolean}
     */
    public deepLinkDataExists: boolean;

    public onBoardingSettings: OnBoardingSettings;
    /**
     * Constructor
     * @param configService - used to gather configuration settings
     * @param overlayService - used to spawn an overlay
     * @param translate - used to get locale translations
     * @param viewContainerRef - component's view container
     * @param flepzScreenService - used to open/close Flepz screens
     */
    constructor(private overlayService: OverlayService,
                private translate: TranslateService,
                public viewContainerRef: ViewContainerRef,
                public flepzScreenService: FlepzScreenService,
                 public loginService: LoginService,
                public settingsService: SettingsService,
                private configService: ConfigService,
                public storageService: StorageService,
                public appMonitorService: AppMonitorService,
                @Inject(APP_CONFIG) public appConfig: IAppConfig,
                private kochavaService : KochavaAnalyticsService)
    {

    }

    ngOnInit()
    {
        if(this.storageService.getItem('showAuthFailureMessage'))
        {
            this.appMonitorService.triggerFaultError({faultCode: AppErrorCodes.FLTT_AUTHENTICATION_REQUIRED_ERROR});
        }

        this.appConfig.nuDetect.beginBehavioralMonitoring();

        this.configService.configuration.subscribe(configuration =>
        {
            if(configuration)
            {
                const currentLang = this.translate.currentLang;
                const localeKey = currentLang === languageConstants.ENGLISH_CANADA ? languageConstants.ENGLISH : currentLang;
                this.onBoardingSettings = this.configService.getOnBoardingSettings(localeKey);
            }
        });

    }

    /**
     * When this component is destroyed, remove the subscription of the user session from the authentication
     * service so that we do not leak the component
     */
    ngOnDestroy(): void
    {
        this.appConfig.nuDetect.stopBehavioralMonitoring();
    }

    /**
     * Retrieves the username from local storage
     */
    static getLocalStorageUsername(): string
    {
        return localStorage.getItem('username');
    }

    /**
     * gets the boolean value from contextual data component which detects the deepLink exists or not.
     * @param {boolean} deepLinkExists
     */
    getDeepLinkExists(deepLinkExists: boolean)
    {
        this.deepLinkDataExists = deepLinkExists;
    }

    /**
     * Used to open an overlay with text and buttons
     */
    public openOverlay(): void
    {
        this.translate.get('login.locatingYouOverlay').subscribe((data) =>
        {
            this.overlayText = {
                header: data.header,
                description: data.description,
                buttonOneText: data.buttonOneText,
                buttonTwoText: null,
                screen: data.screen
            };

        });

        this.overlayService.open({
            overlayData: this.overlayText
        });
    }

    public updateVisitedLink(type: string): boolean
    {
        switch (type)
        {
            case 'locatingYou':
                return this.locatingYouVisited = true;
        }
    }

    /**
     * enables iFrame only if url is https
     */
    public enableIframe(iFrame, url, httpsTarget) : void
    {
        if (this.loginService.getTarget(url, httpsTarget) === httpsTarget)
        {
            this.flepzScreenService.enableIframe(iFrame);
        }
    }
}
