import { Inject, Injectable } from "@angular/core";
import { Location } from '@angular/common';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    Router,
    RouterStateSnapshot,
    NavigationEnd,
    NavigationStart
} from "@angular/router";
import { Observable, of as observableOf, Subscription } from "rxjs";
import { skipWhile, combineLatest, switchMap, first,  filter, pairwise } from 'rxjs/operators';
import {
    AuthenticationService,
    IAppConfig,
    InitializationService,
    InitializationStatusCodes,
    ISession,
    Logger,
    openAccessStatus,
    ResumeService,
    BypassMonitorService,
    AppErrorCodes,
    StorageService,
    DeepLinkTypes
} from "sxmServices";
import { appRouteConstants } from "../../app.route.constants";
import { APP_CONFIG } from "../../sxmservicelayer/sxm.service.layer.module";
import { NavigationService } from "../../common/service/navigation.service";
import {SplashScreenService} from "../../common/service/splash-screen/splash-screen.service";
import { NeriticLinkService } from "../../neritic-links";

@Injectable()
export class AuthenticationGuard implements CanActivate
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("AuthenticationGuard");

    /**
     * Store the base route here so that we can determine if we are on the login page or not
     */
    private baseRoute : string;

    /**
     * Keep the query parameters on our url here so that if we have to refresh the page to go to the login screen we can preserve
     * our query parameters
     */
    private queryParams : string;

    private lastTwoRouteUrls: string[] = [];
    private lastRouteUrl: string = "";

    /**
     * Used to hold the init subscription
     */
    private initSubscription: Subscription ;

    /**
     * Constructor.
     */
    constructor(private authenticationService: AuthenticationService,
                private router: Router,
                private angularLocation : Location,
                private resumeService: ResumeService,
                private initializationService: InitializationService,
                private storageService: StorageService,
                private navigationService: NavigationService,
                private splashScreenService: SplashScreenService,
                private bypassMonitorService: BypassMonitorService,
                private neriticLinkService: NeriticLinkService,
                @Inject(APP_CONFIG) public appConfig: IAppConfig)
    {
        // Listen to changes on the user's session and if the user is ever unauthenticated reroute them to the login page.
        authenticationService.userSession.subscribe(this.onSessionChange.bind(this));

        this.router.events.pipe(filter( event => event instanceof NavigationEnd))
            .subscribe( (event: NavigationEnd ) =>
            {
                this.lastRouteUrl = event.urlAfterRedirects;
            } );

        this.router.events
            .pipe(
                pairwise(),
                filter(navigationStartAndEndEvents))
                .subscribe(([ { url: navEndUrl }, { url: navStartUrl } ]: [NavigationEnd, NavigationStart]) =>
                {
                    this.lastTwoRouteUrls = [ navEndUrl, navStartUrl ];
                });

        function navigationStartAndEndEvents([ event1, event2 ]: [NavigationEnd, NavigationStart]): boolean
        {
            return event1 instanceof NavigationEnd && event2 instanceof NavigationStart;
        }

        this.initializationService.initState
            .subscribe((initState) =>
            {
                if(initState === InitializationStatusCodes.UNAUTHENTICATED_RESTART)
                {
                   location.reload();
                }
            });
    }

    /**
     * Determines if the user can navigate to the requested route if they are authenticated.
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {boolean}
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>
    {
        return (this.resumeService.resumeIsComplete$.pipe(combineLatest(this.initializationService.initState),
            skipWhile((data : Array<any>) => !appIsReady.bind(this)(data[0],data[1])),
            switchMap(goToRoute.bind(this)),
            first())
        ) as any as Observable<boolean>;

        /**
         * Used to check if we have resumed properly and the app is running, or we are not authenticated and
         * therefore can only go to the welcome/login routes
         *
         * @param resumeIsComplete indicates (true or false) whether resume has completed or not
         * @param initState is the value of the initialization state service
         *
         * @returns {boolean} true if app is ready for routing, false if app is not ready for routing
         */
        function appIsReady(resumeIsComplete : boolean, initState : string): boolean
        {
            return ((resumeIsComplete && initState === InitializationStatusCodes.RUNNING)
                    || initState === InitializationStatusCodes.UNAUTHENTICATED
                    || (resumeIsComplete && initState === InitializationStatusCodes.OPENACCESS && this.appConfig.isFreeTierEnable));

        }

        /**
         * Used to detect boolean value which can be used to launch route or not.
         * @returns {Observable<boolean>}
         */
        function goToRoute(): Observable<boolean>
        {
            const loginRoute = state.url.includes(appRouteConstants.AUTH.LOGIN);
            const freeTierRoute = state.url.includes(appRouteConstants.FT_WELCOME);

            return loginRoute || freeTierRoute ? observableOf(true)
                              : observableOf(this.authenticationService.isAuthenticated());
        }
    }

    private isInRoute(match: string)
    {
        return this.lastRouteUrl.indexOf(match) > -1;
    }

    /**
     * When the user session changes, we check to see of we are unauthenticated and not already on the login route.
     * If so, then we go to login
     * @param session
     */
    private onSessionChange({
      authenticated: newAuthVal,
      exited : exited,
      openAccessStatus: currentOpenAccessStatus,
      isInPrivateBrowsingMode: isInPrivateBrowsingMode
    }: ISession): void
    {
        // If newAuth is false we are unauthenticated
        if (newAuthVal === false)
        {

            if( !exited && this.lastRouteUrl !== ""  && !this.isInRoute("login") )
            {
                //This is the best i could come up with so we can trigger a popup on auth session impolosions
                this.storageService.setItem("showAuthFailureMessage", true);
            }

            if(this.appConfig.isFreeTierEnable)
            {
                this.navigationService.go([ appRouteConstants.FT_WELCOME]);
            }
            // We are eligible for Open Access
            else if ( (currentOpenAccessStatus === openAccessStatus.ELIGIBLE)
                && !isInPrivateBrowsingMode
                && !this.bypassMonitorService.appByPassStateData.IT_BYPASS)
            {
                this.navigationService.go([ appRouteConstants.HOME_FORYOU ]);
            }
            else
            {
                // We are either in post Open Access or just unauthenticated
                this.navigationService.go([ appRouteConstants.AUTH.LOGIN ]);
            }
            if(!this.appConfig.isFreeTierEnable) this.splashScreenService.closeSplashScreen();
            // If we at least two urls we have been inside the app before becoming unauthenticated
            if (!exited && this.lastTwoRouteUrls.length)
            {


                // 1) We know that media playback will automatically be stopped when we bring up the login page
                // 2) If the user logs in with different creds, we don't have to worry about situations where they
                //    use a more limited account that does not have access to all the channels the previously
                //    logged in account had.
                // 3) If the user logs in after a period of time, and the lineup has changed, we don't have to
                //    worry about managing this
                // 4) All "session" cookies will be cleared, which gives a clean API slate to work with.
                this.reload();
            }
        }
        else if (!this.initSubscription)// If newAuth is true we are authenticated
        {
            //initialization service state to be running And if user authenticated
            //and deepLink exists then route NP view otherwise Home screen

            this.initSubscription = this.initializationService.initState.pipe(
                                    skipWhile(state =>
                                        (state !== InitializationStatusCodes.RUNNING)),
                                        first())
                                        .subscribe((state) =>
                                        {
                                            if ((!this.appConfig.isFreeTierEnable ||
                                                (this.authenticationService.isUserRegistered() && !this.authenticationService.isFreeTierExpired()))
                                                 && this.appConfig.contextualInfo.deepLink)
                                            {
                                                const isResumeAction = this.appConfig.contextualInfo.type === DeepLinkTypes.COLLECTION ||
                                                                       this.appConfig.contextualInfo.type === DeepLinkTypes.VIDEOS ||
                                                                       this.appConfig.contextualInfo.type === DeepLinkTypes.SHOW ||
                                                                       this.appConfig.contextualInfo.type === DeepLinkTypes.PODCASTS ||
                                                                       this.appConfig.contextualInfo.type === DeepLinkTypes.CATEGORY;
                                                this.appConfig.contextualInfo.deepLink = false;
                                                this.appConfig.contextualInfo.type = "";
                                                this.appConfig.contextualInfo.id = "";

                                                if(isResumeAction)
                                                {
                                                    this.resumeService.resumeMedia.pipe(
                                                        first())
                                                        .subscribe((resumeMedia) =>
                                                        {
                                                            this.neriticLinkService.takeAction(resumeMedia.resumeAction);
                                                        });
                                                }
                                                else
                                                {
                                                    this.navigationService.go([ appRouteConstants.NOW_PLAYING ]);
                                                }
                                            }
                                            else if(this.authenticationService.isFreeTierExpired())
                                            {
                                                this.navigationService.go([ appRouteConstants.FT_UPSELL ]);
                                            }
                                            else if (this.router.url.includes(appRouteConstants.AUTH.LOGIN)
                                                || this.router.url.includes(appRouteConstants.FT_WELCOME))
                                            {
                                                this.navigationService.go([ appRouteConstants.HOME ]);
                                            }

                                        }) as any as Subscription;
        }
    }

    /**
     * Used to hard reload of the app.
     * @returns {boolean}
     */
    private reload(): boolean
    {
        this.queryParams = this.queryParams ? this.queryParams : '';
        this.angularLocation.go(`${appRouteConstants.AUTH.LOGIN}${this.queryParams}`);
        location.reload();
        return true;
    }
}
