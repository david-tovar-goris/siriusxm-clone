import {
    async,
    TestBed
} from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { Observable, of as observableOf } from "rxjs";
import {
    AuthenticationService,
    ConfigService,
    InitializationService,
    InitializationStatusCodes,
    IProfileData,
    ResumeService
} from "sxmServices";

import { SXMServiceLayerModule } from "../../sxmservicelayer/sxm.service.layer.module";
import { AuthenticationModule }  from "../authentication.module";
import { AuthenticationGuard }   from "./authentication.guard";
import { NavigationService }     from "../../common/service/navigation.service";
import { navigationServiceMock } from "../../../../test/mocks/navigation.service.mock";
import { ConfigServiceMock }     from "../../../../test/mocks/config.service.mock";
import { NeriticLinkService } from "../../neritic-links";

describe("AuthenticationGuard", () =>
{
    let authenticationServiceMock;
    let resumeServiceMock;
    let configServiceMock;
    let neriticLinkServiceMock;
    let mockInitService: InitializationService = ({ initState: observableOf(InitializationStatusCodes.RUNNING) }) as any;

    beforeEach(async (() =>
    {
        authenticationServiceMock = {
            userSession: observableOf({ authenticated: true }),
            isAuthenticated: jasmine.createSpy("isAuth").and.returnValue(false),
            logout: jasmine.createSpy('logout').and.returnValue(observableOf(true)),
            isOpenAccessEligible: jasmine.createSpy('isOpenAccessEligible').and.returnValue(observableOf(true)),
            session: { authenticated: true },
            isUserRegistered: jasmine.createSpy("isAuth").and.returnValue(false),
            isFreeTierExpired: jasmine.createSpy("isFreeTierExpired").and.returnValue(false)
        };

        resumeServiceMock = {
            profileData: observableOf({} as IProfileData),
            resumeIsComplete$: observableOf(false),
            resumeMedia: observableOf(false)
        };

        neriticLinkServiceMock = {

        };

        configServiceMock = new ConfigServiceMock();

        TestBed.configureTestingModule({
            providers: [
                AuthenticationGuard,
                { provide: ConfigService, useValue: configServiceMock },
                { provide: AuthenticationService, useValue: authenticationServiceMock },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: ResumeService, useValue: resumeServiceMock},
                { provide: NeriticLinkService, useValue: neriticLinkServiceMock},
                { provide: InitializationService, useValue: mockInitService}
            ],

            imports: [
                RouterTestingModule,
                SXMServiceLayerModule,
                AuthenticationModule
            ]
        });
    }));

    describe("when the url is not login", () =>
    {
        describe("when the resume has completed", () =>
        {
            beforeEach(() =>
            {
                resumeServiceMock.resumeIsComplete$ = observableOf(true);
                mockInitService.initState = observableOf(InitializationStatusCodes.RUNNING);
            });

            it("returns an observable of true if the url is the music route", (done: DoneFn) =>
            {
                let auth = TestBed.get(AuthenticationGuard);
                auth.canActivate(null, { url: "/login" })
                    .subscribe(result =>
                    {
                        expect(result).toBe(true);
                        done();
                    });
            });

            describe("when isAuthenticated() is true", () =>
            {
                beforeEach(() =>
                {
                    authenticationServiceMock.isAuthenticated.and.returnValue(true);
                });

                it("returns an observable of true", (done: DoneFn) =>
                {
                    let auth = TestBed.get(AuthenticationGuard);
                    auth.canActivate(null, { url: "/not" })
                        .subscribe(result =>
                        {
                            expect(result).toBe(true);
                            done();
                        });
                });
            });

            describe("when isAuthenticated() is false", () =>
            {
                beforeEach(() =>
                {
                    authenticationServiceMock.isAuthenticated.and.returnValue(false);
                });

                it("returns an observable of false", (done: DoneFn) =>
                {
                    let auth = TestBed.get(AuthenticationGuard);
                    auth.canActivate(null, { url: "/not" })
                        .subscribe(result =>
                        {
                            expect(result).toBe(false);
                            done();
                        });
                });
            });
        });
    });
});
