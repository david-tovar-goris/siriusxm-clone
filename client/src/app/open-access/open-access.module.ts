import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { RouterModule } from "@angular/router";
import { ContextualDataModule } from "../contextual-data/contextual-data.module";
import { OpenAccessPopupsModule } from "./popups/open-access-popups.module";
import { OpenAccessButtonsComponent } from "../now-playing/open-access/open-access-buttons.component";
import { SXMServiceLayerModule } from "../sxmservicelayer/sxm.service.layer.module";
import { SharedModule } from "../common/shared.module";

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        RouterModule,
        ContextualDataModule,
        OpenAccessPopupsModule,
        SXMServiceLayerModule,
        SharedModule
    ],
    declarations: [
        OpenAccessButtonsComponent
    ],
    exports: [
        OpenAccessButtonsComponent
    ]
})

export class OpenAccessModule
{
}
