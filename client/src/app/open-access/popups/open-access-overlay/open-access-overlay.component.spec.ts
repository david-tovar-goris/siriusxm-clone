import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { ApplicationRef, DebugElement } from "@angular/core";
import { SharedModule } from "app/common/shared.module";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { SettingsService } from "app/common/service/settings/settings.service";
import { SettingsServiceMock } from "../../../../../test/mocks/settings.service.mock";
import { LoginService } from "app/auth/login/services/login.service";
import { LoginServiceMock } from "../../../../../test/login.service.mock";
import { APP_CONFIG } from "app/sxmservicelayer/sxm.service.layer.module";
import { FocusUtil } from "app/common/util/focus.util";
import { OpenAccessOverlayComponent } from "app/open-access/popups/open-access-overlay/open-access-overlay.component";
import { By } from "@angular/platform-browser";
import { MockTranslateService } from "../../../../../test/mocks/translate.service";
import { OverlayServiceMock } from "../../../../../test/mocks/overlay.service.mock";
import { OverlayService } from "app/common/service/overlay/overlay.service";
import { FlepzScreenServiceMock } from "../../../../../test/mocks/flepz-screen.service.mock";
import { FlepzScreenService } from "app/common/service/flepz/flepz-screen.service";
import { LoginOverlayService } from "app/open-access/popups/login-overlay/login-overlay.service";
import { SxmAnalyticsService } from "sxmServices";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../../test";

describe('Open Access Overlay Component', () =>
{
    let component: OpenAccessOverlayComponent;
    let fixture: ComponentFixture<OpenAccessOverlayComponent>;
    let debugElement: DebugElement;

    let appConfig = {
        nuDetect: {
            beginBehavioralMonitoring: function(){}
        },
        deviceInfo: {
            platform: "web",
            appRegion: "appRegion"
        }
    };

    let loginOverlayServiceMock = {
        open: function() {}
    };

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function() {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    let mockStore = {
        select: () => new BehaviorSubject(null)
    };

    let applicationRef = {
        components: [{ instance: { viewConainterRef: {} } }]
    };

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({

            imports: [
                SharedModule,
                TranslateModule.forRoot()
            ],
            declarations: [
                OpenAccessOverlayComponent
            ],
            providers: [
                { provide: SettingsService, useValue: SettingsServiceMock },
                { provide: LoginService, useClass: LoginServiceMock },
                { provide: APP_CONFIG, useValue: appConfig },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: OverlayService, useValue: OverlayServiceMock.getSpy() },
                { provide: FlepzScreenService, useClass: FlepzScreenServiceMock },
                { provide: LoginOverlayService, useValue: loginOverlayServiceMock },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: Store, useValue: mockStore },
                { provide: Router, useClass: RouterStub },
                { provide: ApplicationRef, useValue: applicationRef }
            ]

        }).compileComponents().then(r => {});

        spyOn(FocusUtil, "setupAccessibleDialog");
        spyOn(FocusUtil, "closeFocusedDialog");

        fixture = TestBed.createComponent(OpenAccessOverlayComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        spyOn(component.renderer, "setStyle");
        fixture.detectChanges();
    }));

    it("creates the component", () =>
    {
        expect(component).toBeTruthy();
    });

    it("can be closed", () =>
    {

        spyOn(component, 'close');
        spyOn(window.sessionStorage, 'setItem');

        fixture.debugElement.query(By.css('.overlay-button-1'))
            .triggerEventHandler('click', event);

        expect(component.close).toHaveBeenCalled();
        expect(window.sessionStorage.setItem).toHaveBeenCalledWith("displayOA", "false");
    });

    it("can be opened", () =>
    {
        component.openOverlay();
        expect(OverlayServiceMock.getSpy().open).toHaveBeenCalled();
    });

    it("can update the visited link", () =>
    {
        component.updateVisitedLink('locatingYou');
        expect(component.locatingYouVisited).toBe(true);
    });

    it("can close the open access overlay", () =>
    {
        spyOn(component, 'close');
        spyOn(window.sessionStorage, 'setItem');
        component.closeOpenAccessOverlay();
        expect(component.close).toHaveBeenCalled();
        expect(window.sessionStorage.setItem).toHaveBeenCalledWith("displayOA", "false");
    });
});

