import { ComponentFactoryResolver, Injectable, ViewContainerRef } from "@angular/core";
import { OpenAccessOverlayComponent } from "./open-access-overlay.component";

@Injectable()
export class OpenAccessOverlayService
{
    constructor(private factoryResolver: ComponentFactoryResolver){}

   /* flag to restrict the preview display on every visit to home component.
   * OpenAccessOverlay should be displayed only once.
   * */
    private isOpenAccessOverlayAddressed: boolean = false;

    public open(viewContainerRef: ViewContainerRef, viewContainerSplashRef?: ViewContainerRef): void
    {
        if(!this.isOpenAccessOverlayAddressed)
        {
            const component = this.factoryResolver.resolveComponentFactory(OpenAccessOverlayComponent),
                openAccessOverlayComponent = viewContainerRef.createComponent(component);
            openAccessOverlayComponent.instance._ref = openAccessOverlayComponent;
            openAccessOverlayComponent.instance.containerSplashRef = viewContainerSplashRef ? viewContainerSplashRef : null;
            this.isOpenAccessOverlayAddressed = true;
        }
    }
}
