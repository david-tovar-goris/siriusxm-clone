import {
    Component,
    OnDestroy,
    OnInit,
    ViewContainerRef,
    Renderer2,
    Inject,
    ApplicationRef
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { LoginService } from "../../../auth/login/services/login.service";
import { SettingsService } from "../../../common/service/settings/settings.service";
import { FlepzScreenService } from "../../../common/service/flepz/flepz-screen.service";
import { OverlayService } from "../../../common/service/overlay/overlay.service";
import { FocusUtil } from "../../../common/util/focus.util";
import { APP_CONFIG } from "../../../sxmservicelayer/sxm.service.layer.module";
import { IAppConfig } from "sxmServices";
import { LoginOverlayService } from "app/open-access/popups/login-overlay/login-overlay.service";

@Component({
    templateUrl: "./locating-you-overlay.component.html",
    styleUrls: ["./locating-you-overlay.component.scss"]
})

export class LocatingYouOverlayComponent implements OnInit, OnDestroy
{
    public locatingYouVisited: boolean = false;
    public _ref: any;
    public model: {
        password: string,
        username: string
    } = {
        password: "",
        username: ""
    };
    public privacyPolicyUrl: string = '';
    public customerAgreementUrl: string = '';
    public forgotPasswordUrl: string = '';
    public dataPrivacyUrl: string = '';

    public viewContainerSplashRef: any;

    constructor(public translate: TranslateService,
                public loginService: LoginService,
                public viewContainerRef: ViewContainerRef,
                public settingsService: SettingsService,
                public flepzScreenService: FlepzScreenService,
                private applicationRef: ApplicationRef,
                private overlayService: OverlayService,
                private loginOverlayService : LoginOverlayService,
                public renderer: Renderer2,
                @Inject(APP_CONFIG) public appConfig: IAppConfig)
    {
    }

    public set containerSplashRef(value: any)
    {
        this.viewContainerSplashRef = value ? value : this.viewContainerRef;
    }

    ngOnInit()
    {
        this.renderer.setStyle(document.documentElement, 'overflow', 'hidden');
        this.renderer.setStyle(document.documentElement, 'msOverflowStyle', 'none');
        FocusUtil.setupAccessibleDialog(document.getElementById('locating-you-overlay'), true);
    }

    ngOnDestroy()
    {
        FocusUtil.closeFocusedDialog();
        this.appConfig.nuDetect.stopBehavioralMonitoring();
    }

    public close(): void
    {
        this._ref.destroy();

        this.renderer.setStyle(document.documentElement, 'overflow-y', 'scroll');
        this.renderer.setStyle(document.documentElement, 'overflowY', 'scroll');
        this.renderer.setStyle(document.documentElement, 'overflow-x', 'visible');
        this.renderer.setStyle(document.documentElement, 'overflowX', 'visible');
        this.renderer.setStyle(document.documentElement, 'msOverflowStyle', 'scrollbar');
    }


}
