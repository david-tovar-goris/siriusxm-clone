import {
    async,
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import { SettingsService } from "app/common/service/settings/settings.service";
import { SettingsServiceMock } from "../../../../../test/mocks/settings.service.mock";
import { LocatingYouOverlayComponent } from "./locating-you-overlay.component";
import { DebugElement } from "@angular/core";
import { SharedModule } from "app/common/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { LoginService } from "app/auth/login/services/login.service";
import { LoginServiceMock } from "../../../../../test/login.service.mock";
import { APP_CONFIG } from "app/sxmservicelayer/sxm.service.layer.module";
import { FocusUtil } from "app/common/util/focus.util";
import { By } from "@angular/platform-browser";
import { OverlayService } from "app/common/service/overlay/overlay.service";
import { OverlayServiceMock } from "../../../../../test/mocks/overlay.service.mock";
import { LoginOverlayService } from "app/open-access/popups/login-overlay/login-overlay.service";

describe('Locating You Overlay Component', () =>
{
    let component: LocatingYouOverlayComponent;
    let fixture: ComponentFixture<LocatingYouOverlayComponent>;
    let debugElement: DebugElement;

    let appConfig = {
        nuDetect: {
            beginBehavioralMonitoring: function(){}
        },
        deviceInfo: {
            platform         : "web",
            appRegion        : "appRegion"
        }
    };

    let loginOverlayServiceMock = {
        open: jasmine.createSpy('open')
    };


    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({

            imports: [
                SharedModule,
                TranslateModule.forRoot()
            ],
            declarations: [
                LocatingYouOverlayComponent
            ],
            providers: [
                { provide: LoginService, useClass: LoginServiceMock },
                { provide: SettingsService, useValue: SettingsServiceMock.getSpy() },
                { provide: OverlayService, useValue: OverlayServiceMock.getSpy() },
                { provide: APP_CONFIG, useValue: appConfig },
                { provide: LoginOverlayService, useValue: loginOverlayServiceMock }
            ]

        }).compileComponents();

        spyOn(FocusUtil,"setupAccessibleDialog");
        spyOn(FocusUtil,"closeFocusedDialog");

        fixture = TestBed.createComponent(LocatingYouOverlayComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        spyOn(component.renderer, "setStyle");
        spyOn(component.appConfig.nuDetect, "beginBehavioralMonitoring");
        fixture.detectChanges();
    }));

    it("creates the component", () =>
    {
        expect(component).toBeTruthy();
        expect(component.renderer.setStyle).toHaveBeenCalled();
        expect(FocusUtil.setupAccessibleDialog).toHaveBeenCalled();
    });

    it("can be closed", () =>
    {
        fixture.debugElement.query(By.css('.overlay-close'))
            .triggerEventHandler('click', event);

        fixture.detectChanges();
        expect(component.renderer.setStyle).toHaveBeenCalled();
    });
});
