import {
    Component,
    OnDestroy,
    OnInit,
    ViewContainerRef,
    Renderer2,
    Inject,
    ApplicationRef,
    ViewChild
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { LoginService } from "../../../auth/login/services/login.service";
import { FlepzScreenService } from "../../../common/service/flepz/flepz-screen.service";
import { OverlayService } from "../../../common/service/overlay/overlay.service";
import { FocusUtil } from "../../../common/util/focus.util";
import { APP_CONFIG } from "../../../sxmservicelayer/sxm.service.layer.module";
import {
    IAppConfig,
    SettingsConstants,
    SettingsService
} from "sxmServices";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    templateUrl: "./cold-start-overlay.component.html",
    styleUrls: ["../open-access-overlay/open-access-overlay.component.scss"]
})

export class ColdStartOverlayComponent implements OnInit, OnDestroy
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    public locatingYouVisited: boolean = false;
    public _ref: any;

    public viewContainerSplashRef: any;
    @ViewChild('csOverlay') overlayBody;

    private overlayText: any = {
        header: '',
        description: '',
        buttonOneText: '',
        buttonTwoText: '',
        analyticsTag: {
            ok: '',
            close: ''
        },
        screen: ''
    };

    constructor(public translate: TranslateService,
                public loginService: LoginService,
                public viewContainerRef: ViewContainerRef,
                public settingsService: SettingsService,
                public flepzScreenService: FlepzScreenService,
                private applicationRef: ApplicationRef,
                private overlayService: OverlayService,
                public renderer: Renderer2,
                @Inject(APP_CONFIG) public appConfig: IAppConfig)
    {
    }

    public set containerSplashRef(value: any)
    {
        this.viewContainerSplashRef = value ? value : this.viewContainerRef;
    }

    ngOnInit()
    {
        this.renderer.setStyle(document.documentElement, 'overflow', 'hidden');
        this.renderer.setStyle(document.documentElement, 'msOverflowStyle', 'none');
        FocusUtil.setupAccessibleDialog(document.getElementById('cold-start-overlay'), true);
        this.appConfig.nuDetect.beginBehavioralMonitoring();
    }

    ngOnDestroy()
    {
        FocusUtil.closeFocusedDialog();
        this.appConfig.nuDetect.stopBehavioralMonitoring();
    }

    public close(): void
    {
        this._ref.destroy();

        this.renderer.setStyle(document.documentElement, 'overflow-y', 'scroll');
        this.renderer.setStyle(document.documentElement, 'overflowY', 'scroll');
        this.renderer.setStyle(document.documentElement, 'overflow-x', 'visible');
        this.renderer.setStyle(document.documentElement, 'overflowX', 'visible');
        this.renderer.setStyle(document.documentElement, 'msOverflowStyle', 'scrollbar');
    }

    /**
     * Used to open an overlay with text and buttons
     */
    public openOverlay(): void
    {
        this.translate.get('login.locatingYouOverlay').subscribe((data) =>
        {
            this.overlayText = {
                header: data.header,
                description: data.description,
                buttonOneText: data.buttonOneText,
                buttonTwoText: null,
                analyticsTag: {
                    close: 'tag41',
                    ok: 'tag79'
                },
                screen: data.screen
            };
        });

        const currHeight = this.overlayBody.nativeElement.offsetHeight;
        const currWidth = this.overlayBody.nativeElement.clientWidth;

        const overlayStyles = {
            height: currHeight+'px',
            width: currWidth+'px'
        };

        this.overlayService.open({
            overlayData: this.overlayText ,
            overlayStyles: overlayStyles
        });
    }

    /**
     * close the cold start overlay
     */
    public closeColdStartOverlay()
    {
        this.close();
        this.settingsService.switchGlobalSettingOnOrOff(false, SettingsConstants.DISPLAY_COLD_START_OVERLAY);
    }
}
