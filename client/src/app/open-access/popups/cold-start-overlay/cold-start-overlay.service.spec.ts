import { ColdStartOverlayService } from "app/open-access/popups/cold-start-overlay/cold-start-overlay.service";

describe("cold start overlay service", () =>
{
    let factoryResolver,
        coldStartOverlayService,
        viewContainerRef;

    beforeEach(() =>
    {
        factoryResolver = {
            resolveComponentFactory: jasmine.createSpy("resolveComponentFactory")
        };
        coldStartOverlayService = new ColdStartOverlayService(factoryResolver as any);
        viewContainerRef = {
            createComponent: jasmine.createSpy("createComponent")
        };
    });

    xit("can open the overlay", () =>
    {
        coldStartOverlayService.open(viewContainerRef as any);
        expect(factoryResolver.resolveComponentFactory).toHaveBeenCalled();
        expect(viewContainerRef.createComponent).toHaveBeenCalled();
    });
});
