import {
    async,
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import { ColdStartOverlayComponent } from "./cold-start-overlay.component";
import { DebugElement } from "@angular/core";
import { SharedModule } from "app/common/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { LoginService } from "app/auth/login/services/login.service";
import { LoginServiceMock } from "../../../../../test/login.service.mock";
import { SettingsService } from "sxmServices";
import { SettingsServiceMock } from "../../../../../test/mocks/settings.service.mock";
import { OverlayService } from "app/common/service/overlay/overlay.service";
import { OverlayServiceMock } from "../../../../../test/mocks/overlay.service.mock";
import { FocusUtil } from "app/common/util/focus.util";
import { APP_CONFIG } from "app/sxmservicelayer/sxm.service.layer.module";
import { By } from "@angular/platform-browser";

xdescribe('ColdStartOverlayComponent', () =>
{
    let component: ColdStartOverlayComponent;
    let fixture: ComponentFixture<ColdStartOverlayComponent>;
    let debugElement: DebugElement;

    let appConfig = {
        nuDetect: {
            beginBehavioralMonitoring: function(){}
        },
        deviceInfo: {
            platform         : "web",
            appRegion        : "appRegion"
        }
    };

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({

            imports: [
                SharedModule,
                TranslateModule.forRoot()
            ],
            declarations: [
                ColdStartOverlayComponent
            ],
            providers: [
                { provide: LoginService, useClass: LoginServiceMock },
                { provide: SettingsService, useValue: SettingsServiceMock.getSpy() },
                { provide: OverlayService, useValue: OverlayServiceMock.getSpy() },
                { provide: APP_CONFIG, useValue: appConfig }
            ]

        }).compileComponents();

        spyOn(FocusUtil,"setupAccessibleDialog");
        spyOn(FocusUtil,"closeFocusedDialog");

        fixture = TestBed.createComponent(ColdStartOverlayComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        spyOn(component.renderer, "setStyle");
        spyOn(component.appConfig.nuDetect, "beginBehavioralMonitoring");
        fixture.detectChanges();
    }));

    it("creates the component", () =>
    {
        expect(component).toBeTruthy();
    });

    it("can be initialized", () =>
    {
        expect(component.renderer.setStyle).toHaveBeenCalled();
        expect(FocusUtil.setupAccessibleDialog).toHaveBeenCalled();
        expect(component.appConfig.nuDetect.beginBehavioralMonitoring).toHaveBeenCalled();
    });

    it("can be opened", () =>
    {
        component.openOverlay();
        fixture.detectChanges();
        expect(OverlayServiceMock.getSpy().open).toHaveBeenCalled();
    });

    it("can be closed", () =>
    {
        spyOn(component, "close");
        SettingsServiceMock.getSpy().settings.subscribe(response =>
        {
            expect(response.stopBehavioralMonitoring).toHaveBeenCalled();
        });

        fixture.debugElement.query(By.css('.overlay-button-1'))
            .triggerEventHandler('click', event);

        fixture.detectChanges();
        expect(component.close).toHaveBeenCalled();
        expect(component.renderer.setStyle).toHaveBeenCalled();
    });

});
