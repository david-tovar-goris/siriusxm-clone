import { ComponentFactoryResolver, Injectable, ViewContainerRef } from "@angular/core";
import { ColdStartOverlayComponent } from "./cold-start-overlay.component";

@Injectable()
export class ColdStartOverlayService
{
    constructor(private factoryResolver: ComponentFactoryResolver){}

    /*
    *   flag to restrict the preview display on every visit to home component.
    *   ColdStartOverlay should be displayed only once.
    */
    private isColdStartOverlayAddressed: boolean = false;

    public open(viewContainerRef: ViewContainerRef, viewContainerSplashRef?: ViewContainerRef): void
    {
        if(!this.isColdStartOverlayAddressed)
        {
            const component = this.factoryResolver.resolveComponentFactory(ColdStartOverlayComponent),
                coldStartOverlayComponent = viewContainerRef.createComponent(component);
                coldStartOverlayComponent.instance._ref = coldStartOverlayComponent;
                coldStartOverlayComponent.instance.containerSplashRef = viewContainerSplashRef ? viewContainerSplashRef : null;
            this.isColdStartOverlayAddressed = true;
        }
    }
}
