import { LoginOverlayComponent } from "./login-overlay/login-overlay.component";
import { LoginOverlayService } from "./login-overlay/login-overlay.service";
import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { AuthenticationModule } from "app/auth/authentication.module";
import { SettingsService } from "app/common/service/settings/settings.service";
import { OpenAccessOverlayComponent } from "./open-access-overlay/open-access-overlay.component";
import { OpenAccessOverlayService } from "./open-access-overlay/open-access-overlay.service";

import { ColdStartOverlayComponent } from "./cold-start-overlay/cold-start-overlay.component";
import { ColdStartOverlayService } from "./cold-start-overlay/cold-start-overlay.service";

import { LocatingYouOverlayComponent } from "app/open-access/popups/locating-you-overlay/locating-you-overlay.component";
import { SharedModule } from "app/common/shared.module";


@NgModule({
    imports: [
        TranslateModule,
        CommonModule,
        AuthenticationModule,
        SharedModule,
        FormsModule
    ],
    providers: [
        LoginOverlayService,
        SettingsService,
        OpenAccessOverlayService,
        ColdStartOverlayService
    ],
    declarations: [
        LoginOverlayComponent,
        OpenAccessOverlayComponent,
        LocatingYouOverlayComponent,
        ColdStartOverlayComponent
    ],
    entryComponents: [
        LoginOverlayComponent,
        OpenAccessOverlayComponent,
        LocatingYouOverlayComponent,
        ColdStartOverlayComponent
    ]
})

export class OpenAccessPopupsModule
{
}
