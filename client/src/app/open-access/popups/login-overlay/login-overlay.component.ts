import {
    Component,
    OnDestroy,
    OnInit,
    ViewContainerRef,
    Renderer2,
    Inject,
    ApplicationRef
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { LoginService } from "../../../auth/login/services/login.service";
import { SettingsService } from "../../../common/service/settings/settings.service";
import { FlepzScreenService } from "../../../common/service/flepz/flepz-screen.service";
import { OverlayService } from "../../../common/service/overlay/overlay.service";
import { FocusUtil } from "../../../common/util/focus.util";
import { APP_CONFIG } from "../../../sxmservicelayer/sxm.service.layer.module";
import { IAppConfig } from "sxmServices";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    templateUrl: "./login-overlay.component.html",
    styleUrls: ["./login-overlay.component.scss"]
})

export class LoginOverlayComponent implements OnInit, OnDestroy
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    public locatingYouVisited: boolean = false;
    public _ref: any;

    public viewContainerSplashRef: any;

    private overlayText: any = {
        header: '',
        description: '',
        buttonOneText: '',
        buttonTwoText: '',
        analyticsTag: {
            ok: '',
            close: ''
        },
        screen: ''
    };

    public set containerSplashRef(value: any)
    {
        this.viewContainerSplashRef = value ? value : this.applicationRef.components[0].instance.viewContainerRef;
    }

    constructor(public translate: TranslateService,
                public loginService: LoginService,
                public viewContainerRef: ViewContainerRef,
                private applicationRef: ApplicationRef,
                public settingsService: SettingsService,
                public flepzScreenService: FlepzScreenService,
                private overlayService: OverlayService,
                public renderer: Renderer2,
                @Inject(APP_CONFIG) public appConfig: IAppConfig)
    {
    }

    ngOnInit()
    {
        this.renderer.setStyle(document.documentElement, 'overflow', 'hidden');
        this.renderer.setStyle(document.documentElement, 'msOverflowStyle', 'none');
        FocusUtil.setupAccessibleDialog(document.getElementById('login-overlay'), true);
        this.appConfig.nuDetect.beginBehavioralMonitoring();
    }

    ngOnDestroy()
    {
        FocusUtil.closeFocusedDialog();
        this.appConfig.nuDetect.stopBehavioralMonitoring();
    }

    public close(): void
    {
        this._ref.destroy();
        this.loginService.clearLoginErrorMessage();

        this.renderer.setStyle(document.documentElement, 'overflow-y', 'scroll');
        this.renderer.setStyle(document.documentElement, 'overflowY', 'scroll');
        this.renderer.setStyle(document.documentElement, 'overflow-x', 'visible');
        this.renderer.setStyle(document.documentElement, 'overflowX', 'visible');
        this.renderer.setStyle(document.documentElement, 'msOverflowStyle', 'scrollbar');
    }

    /**
     * Used to open an overlay with text and buttons
     */
    public openOverlay(): void
    {
        this.translate.get('login.locatingYouOverlay').subscribe((data) =>
        {
            this.overlayText = {
                header: data.header,
                description: data.description,
                buttonOneText: data.buttonOneText,
                buttonTwoText: null,
                analyticsTag: {
                    close: 'tag41',
                    ok: 'tag79'
                },
                screen: data.screen
            };

        });

        this.overlayService.open({
            overlayData : this.overlayText
        });
    }

    public updateVisitedLink(type: string): boolean
    {
        switch (type)
        {
            case 'locatingYou':
                return this.locatingYouVisited = true;
        }
    }

    /**
     * enables iFrame only if url is https
     */
    public enableIframe(iFrame, url, httpsTarget) : void
    {
        if (this.loginService.getTarget(url, httpsTarget) === httpsTarget)
        {
            this.flepzScreenService.enableIframe(iFrame);
            this.close();
        }
    }
}
