import { ComponentFactoryResolver, Injectable, ViewContainerRef } from "@angular/core";
import { LoginOverlayComponent } from "./login-overlay.component";

@Injectable()
export class LoginOverlayService
{
    constructor(private factoryResolver: ComponentFactoryResolver){}

    public open(viewContainerRef: ViewContainerRef, viewContainerSplashRef?: ViewContainerRef): void
    {
        const component = this.factoryResolver.resolveComponentFactory(LoginOverlayComponent),
            loginOverlayComponent = viewContainerRef.createComponent(component);
        loginOverlayComponent.instance._ref = loginOverlayComponent;
        loginOverlayComponent.instance.containerSplashRef = viewContainerSplashRef ? viewContainerSplashRef : null;
    }
}
