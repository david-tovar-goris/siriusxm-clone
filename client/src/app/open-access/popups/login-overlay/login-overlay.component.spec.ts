import { SettingsService } from "app/common/service/settings/settings.service";
import { SettingsServiceMock } from "../../../../../test/mocks/settings.service.mock";
import { LoginOverlayComponent } from "./login-overlay.component";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { DebugElement } from "@angular/core";
import { SharedModule } from "app/common/shared.module";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { LoginService } from "app/auth/login/services/login.service";
import { LoginServiceMock } from "../../../../../test/login.service.mock";
import { APP_CONFIG } from "app/sxmservicelayer/sxm.service.layer.module";
import { FocusUtil } from "app/common/util/focus.util";
import { MockComponent, MockDirective } from "../../../../../test/mocks/component.mock";
import { OverlayService } from "app/common/service/overlay/overlay.service";
import { OverlayServiceMock } from "../../../../../test/mocks/overlay.service.mock";
import { MockTranslateService } from "../../../../../test/mocks/translate.service";
import { FlepzScreenService } from "app/common/service/flepz/flepz-screen.service";
import { FlepzScreenServiceMock } from "../../../../../test/mocks/flepz-screen.service.mock";
import { SxmAnalyticsService } from "sxmServices";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Store } from "@ngrx/store";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../../test";
import { By } from "@angular/platform-browser";

xdescribe('Login Overlay Component', () =>
{
    let component: LoginOverlayComponent;
    let fixture: ComponentFixture<LoginOverlayComponent>;
    let debugElement: DebugElement;

    let appConfig = {
        nuDetect: {
            beginBehavioralMonitoring: function(){}
        },
        deviceInfo: {
            platform         : "web",
            appRegion        : "appRegion"
        }
    };

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    let mockStore = {
        select: () => new BehaviorSubject(null)
    };

    let loginServiceMock = new LoginServiceMock();

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({

            imports: [
                SharedModule,
                TranslateModule.forRoot()
            ],
            declarations: [
                LoginOverlayComponent,
                MockComponent({ selector: "login-component",
                    inputs: ["closeLoginOverLayFunction", "viewContainerRef"]}),
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "userPath",
                        "pageFrame", "carouselName", "buttonName", "actionSource",
                        "elementPosition", "elementType", "action", "inputType","findingMethod",
                        "searchTerm", "tileContentType", "skipCurrentPosition"]
                })
            ],
            providers: [
                { provide: LoginService, useValue: loginServiceMock },
                { provide: SettingsService, useClass: SettingsServiceMock },
                { provide: OverlayService, useValue: OverlayServiceMock.getSpy() },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: Store, useValue: mockStore },
                { provide: Router, useClass: RouterStub },
                { provide: APP_CONFIG, useValue: appConfig },
                { provide: FlepzScreenService, useClass: FlepzScreenServiceMock },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() }
            ]

        }).compileComponents();

        spyOn(FocusUtil,"setupAccessibleDialog");
        spyOn(FocusUtil,"closeFocusedDialog");

        fixture = TestBed.createComponent(LoginOverlayComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;

        component._ref = {
            destroy: function(){}
        };

        spyOn(component.renderer, "setStyle");
        fixture.detectChanges();
    }));

    it("creates the component", () =>
    {
        expect(component).toBeTruthy();
    });

    it("can be closed", () =>
    {
        spyOn(component._ref, "destroy");

        fixture.debugElement.query(By.css('.overlay-close'))
            .triggerEventHandler('click', event);

        expect(component.renderer.setStyle).toHaveBeenCalled();
        expect(loginServiceMock.clearLoginErrorMessage).toHaveBeenCalled();
    });

    it("can be opened", () =>
    {
        component.openOverlay();
        expect(OverlayServiceMock.getSpy().open).toHaveBeenCalled();
    });

    it("can update the visited link", () =>
    {
        component.updateVisitedLink('locatingYou');
        expect(component.locatingYouVisited).toBe(true);
    });
});
