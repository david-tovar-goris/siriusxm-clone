import { Inject, NgModule } from "@angular/core";
import {
    Router,
    RouterModule,
    Routes
} from "@angular/router";
import { AllChannelsComponent } from "./all-channels/all-channels.component";
import { appRouteConstants } from "./app.route.constants";
import { AuthenticationGuard } from "./auth/guard";
import { ViewAllComponent } from "./carousel/content/view-all/view-all.component";
import { OnDemandDetailsResolver } from "./channel-list/component/on-demand-details.resolver";
import { OnDemandEpisodesListComponent } from "./channel-list/component/on-demand-episodes-list.component";
import { FavoritesComponent } from "./favorites/favorites.component";
import { HomeComponent } from "./home";
import { HomeResolver } from "./home/home-resolver";
import { NowPlayingContainer } from "./now-playing/now-playing.container";
import { NowPlayingRouteGaurd } from "./now-playing/now-playing.route-gaurd";
import {
    ApplicationSettingsComponent,
    ReminderSettingsComponent
} from "./profile";
import { RecentlyPlayedComponent } from "./recently-played/recently-played.component";
import { SearchResultsComponent } from "./search/search-results/search-results.component";
import { AllOnDemandComponent } from "./all-on-demand/all-on-demand.component";
import { CategoryPageComponent } from "./category-page/category-page.component";
import { RecentlyPlayedRouteGuard } from "./recently-played/recently-played.route-guard";
import { ManageShowRemindersComponent } from "./profile/manage-show-reminders/manage-show-reminders.component";
import { ShowReminderDetailsComponent } from "./profile/manage-show-reminders/show-reminder-details.component";
import { MessagingSettingsComponent } from "./profile/messaging-settings/messaging-settings.component";
import { ViewAllResolver } from "./carousel/content/view-all/view-all-resolver";
import { CollectionPageComponent } from "./collection-page/collection-page.component";
import { CollectionPageResolver } from "./collection-page/collection-page.resolver";
import { CategoryPageResolver } from "app/category-page/category-page.resolver";
import { EnhancedEdpComponent } from "./enhanced-edp/enhanced-edp.component";
import { EnhancedEdpResolver } from "./enhanced-edp/enhanced-edp.resolver";
import { SeededSettingsComponent } from "./profile/seeded-settings/seeded-settings.component";
import { SeededSettingsResolver } from "./profile/seeded-settings/seeded-settings.resolver";
import { SearchLandingComponent }  from "./search/search-landing/search-landing.component";
import { LoginPageComponent } from "app/auth/login-page/login-page.component";
import { CategoryComponent } from "app/category";
import { CategoryResolver } from "app/category/category-resolver";
import { WelcomePageComponent } from "app/free-tier/welcome/welcome-page.component";
import { UpsellPageComponent } from "app/free-tier/upsell/upsell-page.component";

/**
 * @MODULE:     service-lib
 * @CREATED:    07/17/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  App Routing Module used to configure app & child level routes. Can be used in entire client side app.
 */

export const createCleanRoute: Function = (name: string) => name.toLowerCase().replace(/ /g, "-").replace(/\//g, "-");

export let appRoutes: Routes = [
    {
        path: "",
        canActivate: [ AuthenticationGuard ],
        children: [
            {
                path: appRouteConstants.FT_WELCOME,
                component: WelcomePageComponent
            },
            {
                path: appRouteConstants.FT_UPSELL,
                component: UpsellPageComponent
            },
            {
                path: appRouteConstants.SUBSCRIBE_SCREEN,
                component: WelcomePageComponent
            },
            {
                path: appRouteConstants.EXPLORE,
                component: WelcomePageComponent
            },
            {
                path: appRouteConstants.ACCESS_NOW,
                component: WelcomePageComponent
            },
            {
                path: appRouteConstants.AUTH.LOGIN,
                component: LoginPageComponent
            },
            {
                path: `${appRouteConstants.HOME}/:superCat`,
                component: HomeComponent,
                resolve: {
                    categoryData: HomeResolver
                }
            },
            {
                path: appRouteConstants.HOME,
                component: HomeComponent,
                resolve: {
                    categoryData: HomeResolver
                }
            },
            {
                path: appRouteConstants.CATEGORY,
                component: CategoryComponent
            },
            {
                path: `${appRouteConstants.CATEGORY}/:superCategory/:subCategory/:listView/:channelId`,
                component: CategoryComponent,
                resolve: { categoryData: CategoryResolver }
            },
            {
                path: `${appRouteConstants.CATEGORY}/:superCategory/:subCategory/:listView`,
                component: CategoryComponent,
                resolve: { categoryData: CategoryResolver }
            },
            {
                path: `${appRouteConstants.CATEGORY}/:url/:listView`,
                component: CategoryComponent,
                resolve: { categoryData: CategoryResolver }
            },
            {
                path: appRouteConstants.NOW_PLAYING,
                component: NowPlayingContainer,
                canActivate: [ NowPlayingRouteGaurd ]
            },
            {
                path: `${appRouteConstants.FAVORITES}/:tab`,
                component: FavoritesComponent
            },
            {
                path: `${appRouteConstants.ON_DEMAND.EPISODES_LIST}/:channelId/:showId/:${appRouteConstants.ON_DEMAND.TYPE_PARAM}/:url`,
                component: OnDemandEpisodesListComponent,
                resolve: { onDemandDetails: OnDemandDetailsResolver }
            },
            {
                path: `${appRouteConstants.SEARCH}`,
                component: SearchLandingComponent
            },
            {
                path: `${appRouteConstants.SEARCH}/:keyword`,
                component: SearchResultsComponent
            },
            {
                path: appRouteConstants.RECENTLY_PLAYED,
                component: RecentlyPlayedComponent,
                canActivate: [ RecentlyPlayedRouteGuard ]
            },
            {
                path: `${appRouteConstants.VIEW_ALL}/:carouselGuid`,
                component: ViewAllComponent,
                resolve: { viewAllDetails: ViewAllResolver }
            },
            {
                path: appRouteConstants.ALL_CHANNELS,
                component: AllChannelsComponent
            },
            {
                path: appRouteConstants.ALL_ON_DEMAND,
                component: AllOnDemandComponent
            },
            {
                path: `${appRouteConstants.CATEGORY_PAGE}/:categoryType`,
                component: CategoryPageComponent,
                resolve: { collectionDetails: CategoryPageResolver }
            },
            {
                path: appRouteConstants.APPLICATION_SETTINGS,
                component: ApplicationSettingsComponent
            },
            {
                path: appRouteConstants.REMINDER_SETTINGS,
                component: ReminderSettingsComponent
            },
            {
                path: appRouteConstants.MANAGE_SHOW_REMINDERS,
                component: ManageShowRemindersComponent
            },
            {
                path: `${appRouteConstants.SHOW_REMINDER_DETAILS}/:showGuid`,
                component: ShowReminderDetailsComponent
            },
            {
                path: appRouteConstants.MESSAGING_SETTINGS,
                component: MessagingSettingsComponent
            },
            {
                path: appRouteConstants.SEEDED_SETTINGS,
                component: SeededSettingsComponent,
                resolve: { seededSettings: SeededSettingsResolver }
            },
            {
                path: `${appRouteConstants.COLLECTION}/:url`,
                component: CollectionPageComponent,
                resolve: { collectionDetails: CollectionPageResolver }
            },
            {
                path: `${appRouteConstants.ENHANCED_EDP}/:url`,
                component: EnhancedEdpComponent,
                resolve: { enhancedEdpDetails: EnhancedEdpResolver }
            },
            {
                path: "",
                redirectTo: `${appRouteConstants.HOME}`,
                pathMatch: "full"
            },
            {
                path: "**",
                redirectTo: `${appRouteConstants.HOME}`
            }
        ]
    }
];

@NgModule({
    imports: [

        RouterModule.forRoot(appRoutes,
            {
                enableTracing: false,
                scrollPositionRestoration:'enabled'
            })
    ],
    exports: [ RouterModule ]
})

export class AppRoutingModule
{
    constructor(private router: Router)
    {
    }
}
