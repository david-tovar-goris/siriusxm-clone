import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { StoreModule } from "@ngrx/store";
import {
    InitializationService,
    Logger,
    CurrentlyPlayingService,
    ConnectivityService,
    ChromecastService
} from "sxmServices";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app.routing";
import { AuthenticationModule } from "./auth/authentication.module";
import { CategoryModule } from "./category/category.module";
import { carouselReducer } from "./common/reducer/carousel.reducer";
import { channelsReducer } from "./common/reducer/channel-list.reducer";
import { favoriteListReducer } from "./common/reducer/favorite-list.reducer";
import { nowPlayingReducer } from "./common/reducer/now-playing.reducer";
import { recentlyPlayedListReducer } from "./common/reducer/recently-played-list.reducer";
import { ContextMenuListComponent } from "./context-menu/context-menu-list.component";
import { ShowRemindersComponent } from "./show-reminders/show-reminders.component";
import { ContextMenuModule } from "./context-menu/context-menu.module";
import { FavoritesModule } from "./favorites/favorites.module";
import { HomeModule } from "./home/home.module";
import { ClientErrorService } from "./common/service/error/client-error.service";
import { ClientInactivityService } from "./inactivity/client-inactivity.service";
import { NavigationModule } from "./navigation/navigation.module";
import { NowPlayingModule } from "./now-playing/now-playing.module";
import { SingletonVideoModule } from "./video/singleton-video/singleton-video.module";
import { OverlayModule } from "./common/component/overlay/overlay.module";
import { OverlayService } from "./common/service/overlay/overlay.service";
import { ToastModule } from "./common/component/toast/toast.module";
import { ToastService } from "./common/service/toast/toast.service";
import { AutoplayModule } from "./autoplay/autoplay.module";
import { PlayerControlsModule } from "./player-controls/player-controls.module";
import { ProfileModule } from "./profile/profile.module";
import { RecentlyPlayedModule} from "./recently-played/recently-played.module";
import { SXMServiceLayerModule } from "./sxmservicelayer/sxm.service.layer.module";
import { TranslationModule } from "./translate/translation.module";
import { SearchModule } from "./search/search.module";
import { SharedModule } from "./common/shared.module";
import { OnDemandEpisodeListModule } from "./channel-list/component/on-demand-episode-list.module";
import { HomeResolver } from "./home/home-resolver";
import { CategoryResolver } from "app/category/category-resolver";
import { AllChannelsModule } from "./all-channels/all-channels.module";
import { HowardModule } from "./category/howard/howard.module";
import { OpenAccessModule } from "./open-access/open-access.module";
import { apronSegmentReducer } from "./common/reducer/apron-segments.reducer";
import { OnDemandDetailsResolver } from "./channel-list/component/on-demand-details.resolver";
import { ChannelListStoreService } from "./common/service/channel-list.store.service";
import { NotificationModule } from "./common/component/notification/notification.module";
import { NotificationService } from "./common/service/notification/notification.service";
import { RecentlyPlayedStoreService } from "./common/service/recently-played.store.service";
import { AllOnDemandModule } from "./all-on-demand/all-on-demand.module";
import { CategoryPageModule } from "./category-page/category-page.module";
import { wallClockReducer } from "./common/reducer/wall-clock.reducer";
import { WallClockStoreService } from "./common/service/wall-clock.store.service";
import { AlertClientService } from "./common/service/alert/alert.client.service";
import { NavigationService } from "./common/service/navigation.service";
import { AppErrorMessaging } from "./common/service/error/app-error.messaging";
import { DynamicAssetLoaderService } from "./common/service/dynamic-asset-loader.service";
import { SingletonVideoService } from "./video/singleton-video/singleton-video.service";
import { ViewAllResolver } from "./carousel/content/view-all/view-all-resolver";
import { FaultMap } from "./common/service/error/fault.map";
import { CollectionPageModule } from "./collection-page/collection-page.module";
import { AutoplayService } from "./autoplay/autoplay.service";
import { EnhancedEdpModule } from "./enhanced-edp/enhanced-edp.module";
import { SeededStationsClientService } from "./common/service/seeded-stations/seeded-stations.client.service";
import { A11yModule } from '@angular/cdk/a11y';
import { WINDOW_PROVIDERS } from "./common/service/window/window.service";
import { FreeTierModule } from "app/free-tier/free-tier.module";

@NgModule({
    declarations: [
        ContextMenuListComponent,
        ShowRemindersComponent,
        AppComponent
    ],
    imports: [
        BrowserModule,
        NavigationModule,
        ProfileModule,
        FavoritesModule,
        CategoryModule,
        HomeModule,
        AuthenticationModule,
        NowPlayingModule,
        TranslationModule,
        SXMServiceLayerModule,
        ContextMenuModule,
        OverlayModule,
        SingletonVideoModule,
        ToastModule,
        SearchModule,
        OpenAccessModule,
        NotificationModule,
        AutoplayModule,
        PlayerControlsModule,
        RecentlyPlayedModule,
        OnDemandEpisodeListModule,
        AllChannelsModule,
        AllOnDemandModule,
        CategoryPageModule,
        HowardModule,
        CollectionPageModule,
        EnhancedEdpModule,
        SharedModule,
        A11yModule,
        CategoryModule,
        FreeTierModule,

        // List of reducers to support the NGRX stores.
        StoreModule.forRoot({
            channelStore: channelsReducer,
            favoriteList: favoriteListReducer,
            nowPlayingStore: nowPlayingReducer,
            recentlyPlayedStore: recentlyPlayedListReducer,
            apronSegmentStore: apronSegmentReducer,
            carouselStore: carouselReducer,
            wallClockStore: wallClockReducer
        }),

        // NOTE: Only uncomment this if you want to debug the Redux owned objects, otherwise the performance impact is HUGE and the app crawls!!!
        // Note that you must instrument after importing StoreModule
        // StoreDevtoolsModule.instrument({
        //     maxAge: 25 //  Retains last 25 states
        // }),

        // Note: AppRoutingModule should be last one in the list.
        AppRoutingModule
    ],
    providers: [
        ClientErrorService,
        ClientInactivityService,
        OverlayService,
        ToastService,
        HomeResolver,
        CategoryResolver,
        OnDemandDetailsResolver,
        NotificationService,
        WallClockStoreService,
        AlertClientService,
        NavigationService,
        SingletonVideoService,
        AppErrorMessaging,
        DynamicAssetLoaderService,
        ViewAllResolver,
        FaultMap,
        AutoplayService,
        SeededStationsClientService,
        WINDOW_PROVIDERS
    ],
    bootstrap: [ AppComponent ]
})

export class AppModule
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("AppModule");

    constructor(private initializationService: InitializationService,
                private connectivityService : ConnectivityService,
                private currentlyPlayingService: CurrentlyPlayingService,
                private clientInactivityService: ClientInactivityService,
                private channelListStoreService: ChannelListStoreService,
                private recentlyPlayedStoreService: RecentlyPlayedStoreService,
                private clientErrorService: ClientErrorService,
                private wallClockStoreSerivce: WallClockStoreService,
                private navigationService: NavigationService,
                private appErrorMessaging: AppErrorMessaging,
                private dynamicAssetLoaderService: DynamicAssetLoaderService,
                private alertClientService: AlertClientService,
                private chromecastService: ChromecastService

    )
    {
        this.clientErrorService.init();
        this.dynamicAssetLoaderService.init();
    }
}
