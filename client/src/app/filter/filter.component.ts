import {
    Component,
    EventEmitter,
    Input,
    Output
} from "@angular/core";
import { Logger } from "sxmServices";
import { displayViewType } from "app/channel-list/component/display.view.type";
import {
    AnalyticsCarouselNames,
    AnalyticsElementTypes, AnalyticsInputTypes,
    AnalyticsPageFrames, AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "sxm-filter",
    templateUrl: "./filter.component.html",
    styleUrls: [ "./filter.component.scss" ]
})
export class FilterComponent
{

    /**
     * Analytics Constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("FilterComponent");

    /**
     * Component output that dispatches an event with the new filter. This allows the parent component
     * to listen to the event and act accordingly.
     */
    @Output() filter = new EventEmitter();

    /**
     * Input used to determine the filter type: channel or show
     */
    @Input() filterType: displayViewType;

    /**
     * the current filter being used on the channel list.
     */
    public currentFilter: string = "";

    /**
     * Broadcasts an event as component output for the new filter.
     * @param {string} value
     */
    public filterChannels(value: string = "")
    {
        FilterComponent.logger.debug(`filterChannels( ${value} )`);
        this.currentFilter = value;
        this.filter.emit(value);
    }

    public getCarouselNameAnaInput()
    {
        switch(this.filterType)
        {
            case displayViewType.channels:
                return AnalyticsCarouselNames.CHANNELS_LIST;
            case displayViewType.episodeView:
                return AnalyticsCarouselNames.EPISODES_LIST;
            case displayViewType.ondemand:
                return AnalyticsCarouselNames.ONDEMAND_LIST;
        }
    }
}
