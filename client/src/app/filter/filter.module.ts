import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import {FilterComponent} from "./filter.component";
import {SharedModule} from "../common/shared.module";
import {TranslationModule} from "../translate/translation.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        TranslationModule
    ],
    declarations: [
        FilterComponent
    ],
    exports: [
        FilterComponent
    ]
})
export class FilterModule
{
}
