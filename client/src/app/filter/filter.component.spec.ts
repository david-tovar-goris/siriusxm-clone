import {
    async,
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { FormsModule } from '@angular/forms';
import { TranslationModule } from "../translate/translation.module";

import { FilterComponent } from "./filter.component";
import { MockComponent, MockDirective } from "../../../test/mocks/component.mock";

describe("FilterComponent", () =>
{
    let component: FilterComponent;
    let fixture: ComponentFixture<FilterComponent>;

    const filterValue = "sirius";

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                FormsModule
            ],
            declarations: [
                FilterComponent,
                MockComponent({ selector: "[sxm-sticky]", inputs: ["stickyClass"]}),
                MockDirective({ selector: "[sxm-new-analytics]", inputs: ["tagName", "userPath", "userPathScreenPosition",
                        "carouselName", "pageFrame", "elementPosition", "searchTerm", "buttonName", "elementType", "action", "inputType",
                        "skipActionSource", "skipCurrentPosition"
                    ]})
            ],
            providers: []
        }).compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(FilterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should emit an event with a string", () =>
    {
        spyOn(component.filter, "emit");
        component.filterChannels(filterValue);
        expect(component.currentFilter).toEqual(filterValue);
        expect(component.filter.emit).toHaveBeenCalledWith(filterValue);
    });
});
