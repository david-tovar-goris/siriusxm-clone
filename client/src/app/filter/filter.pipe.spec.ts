import { FilterPipe } from "./filter.pipe";

describe('Filter Pipe', () =>
{
    let filterPipe: FilterPipe;
    let episodeList = [];
    let filterString: string = '';
    let propertyName: string[] = [''];

    beforeEach(() =>
    {
        filterPipe = new FilterPipe;
    });

    afterEach( () =>
    {
        cleanUp();
    });

    describe('when the aodEpisode list is empty', () =>
    {
        episodeList = [ {} ];

        it('returns the original aodEpisode list', () =>
        {
            expect(filterPipe.transform(episodeList, filterString, propertyName)).toEqual(episodeList);
        });
    });

    describe('when the filterString is an empty string', () =>
    {
        filterString = '';

        it('returns the original aodEpisode list', () =>
        {
            episodeList = [ {} ];

            expect(filterPipe.transform(episodeList, filterString, propertyName)).toEqual(episodeList);
        });
    });

    describe('when the aodEpisode property matches the filter string', () =>
    {
        it('returns a new aodEpisode array with the matched episodes', () =>
        {

            episodeList = [
                {
                    longTitle: 'howard'
                },
                {
                    longTitle: 'stern'
                }
            ];


            const filteredList = [
                {
                    longTitle: 'howard'
                }
            ] as any;

            filterString = 'howard';
            propertyName = ['longTitle'];

            expect(filterPipe.transform(episodeList, filterString, propertyName)).toEqual(filteredList as any);
        });
    });

    const cleanUp = () =>
    {
        filterString = '';
        episodeList = [ {} ];
    };
});
