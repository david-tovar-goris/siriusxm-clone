/**
 * @MODULE:     client
 * @CREATED:    09/29/2017
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  Custom Filter Pipe used to filter the channels, shows or on demand episode list
 */

import { Pipe, PipeTransform } from '@angular/core';
import * as _ from "lodash";
import { filterConstants } from "./filter.constants";
import { ITile } from "sxmServices";

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform
{
    /**
     *  Uses the parameters passed to the FilterPipe to return a new array of filtered channel/shows/episodes
     * @param {any[]} list
     * @param {string} filterString
     * @param {string[]} propNames
     * @returns {any[]}
     */
    transform(list: ITile[],
              filterString: string,
              propNames: string[])
    {
        if (list && (list.length === 0 || filterString === ''))
        { return list; }

        if (!list)
        { return []; }

        return list.filter((listItem) =>
        {
            let match = false;

            propNames.forEach(propName =>
            {
                if(match) return;

                //ignore leading zeroes for channel numbers (02 to 09)
                if(propName === filterConstants.LINE1 && filterString.length === 2)
                {
                    filterString = filterString.replace(/^[0]+/g,"");
                }

                const regexp = new RegExp(filterString, 'gi');
                const value = propName === filterConstants.CHANNEL_NAME
                            ? _.get(listItem.tileAssetInfo, propName).toString()
                            :  _.get(listItem, propName).toString();
                match = value ? value.match(regexp) !== null : false;
            });

            return match;
        });
    }
}



