/**
 * @MODULE:     client
 * @CREATED:    05/31/18
 * @COPYRIGHT:  2018 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  filterConstants are property names used in the filter.
 **/

export const filterConstants = {
    //property names
    LINE1: "line1",
    NAME: "name",
    CHANNEL_NAME: "channelName"
};
