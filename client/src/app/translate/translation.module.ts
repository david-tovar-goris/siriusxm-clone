import {
    Inject,
    NgModule
}                              from "@angular/core";
import {
    HttpClient,
    HttpClientModule
}                              from "@angular/common/http";
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
}                              from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { APP_CONFIG }          from "../sxmservicelayer/sxm.service.layer.module";
import {
    IAppConfig,
    Logger,
    StorageService,
    StorageKeyConstant
} from "sxmServices";
import { TranslateConstants } from "./translate.constants";

/**
 * Creates the translation loader.
 *
 * NOTE: This method is required to be exported to work with AOT compilation builds.
 *
 * @param {Http} http - The HTTP service to load the JSON translation bundles.
 * @returns {TranslateHttpLoader}
 */
export function createTranslateLoader(http : HttpClient)
{
    return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

@NgModule({
              imports      : [
                  HttpClientModule,
                  TranslateModule.forRoot({
                                              loader : {
                                                  provide    : TranslateLoader,
                                                  useFactory : createTranslateLoader,
                                                  deps       : [ HttpClient ]
                                              }
                                          })
              ],
              exports      : [
                  TranslateModule
              ],
              declarations : [],
              providers    : [StorageService]
          })

/**
 * @MODULE:     client-lib
 * @CREATED:    07/19/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 * Module to setup localization using the "ngx-translate" library.
 */
export class TranslationModule
{
    /**
     * Internal logger.
     */
    private static logger : Logger = Logger.getLogger("TranslationModule");

    /**
     * Constructor. Sets up translations with the default language and current language
     * based on what the browser believes is current.
     *
     * @param {TranslateService} translate is the angular translation service
     * @param {StorageService} storageService is the local storage service, user language from CA login UI's stored here
     * @param appConfig is the service layer application config object, region for app is available here
     */
    constructor(private translate : TranslateService,
                private storageService : StorageService,
                @Inject(APP_CONFIG) private appConfig : IAppConfig)
    {
        translate.addLangs(TranslateConstants.LANGUAGELIST);

        const browserLang        = translate.getBrowserLang();
        const appRegion : string = this.appConfig.deviceInfo.appRegion;
        const localStorageLang   = storageService.getItem(StorageKeyConstant.LANGUAGE, browserLang);
        const regionLang         = (appRegion === 'CA' && localStorageLang == 'en') ? 'en-ca' : localStorageLang;
        const lang               = localStorageLang.match(/en|fr|en-ca/) ? regionLang : "en";

        TranslationModule.logger.debug(`Constructor( Current locale: ${lang} )`);
        translate.use(lang);
    }
}
