import {
    async,
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { NoResultsComponent } from "../no-results/no-results.component";
import { TranslationModule } from "../translate/translation.module";


describe("NoResultsComponent", () =>
{
    let component: NoResultsComponent;
    let fixture: ComponentFixture<NoResultsComponent>;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [
                NoResultsComponent
            ]
        }).compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(NoResultsComponent);
        component = fixture.componentInstance;
        component.results = [];
        component.filterText = "";
        component.pageName = "favoritesPage";

        fixture.detectChanges();
    });

    it("should be created", () =>
    {
        expect(component instanceof NoResultsComponent).toBe(true);
    });

    describe('noFilteredResults()', () =>
    {
        describe('when the filterText is empty', () =>
        {
            it('returns a boolean value of false', () =>
            {
                component = fixture.componentInstance;
                expect(component.noFilteredResults()).toEqual(false);
            });
        });
    });

    describe("displayFilterText()", () =>
    {
        it("should display the filter text", () =>
        {
            component.pageName = 'episodeView';
            component.filterText = "something";

            expect(component.displayFilterText()).toEqual(false);
        });

        it("should not display the filter text", () =>
        {
            component.pageName = 'channelView';
            component.filterText = "something";

            expect(component.displayFilterText()).toEqual(true);
        });
    });
});
