import {
    Component,
    Input, OnChanges,
    OnInit, SimpleChanges
} from "@angular/core";
import { noResultsPagesConst } from "./no-results.const";

@Component({
    selector: "sxm-no-results",
    templateUrl: "./no-results.component.html",
    styleUrls: [ "./no-results.component.scss" ]
})
export class NoResultsComponent implements OnChanges
{

    /**
     * Input used to capture the results - ex: array of channels, array of favorites, etc.
     */
    @Input() results: any[] = [];

    /**
     * Input used to display filter text on the NoResultsComponent
     */
    @Input() filterText?: string;

    /**
     * Input used to determine which page is using the NoResultsComponent
     */
    @Input() pageName: string;

    /**
     * noResultsPage used to fetch the all data for that screen/page
     */
    public noResultsPage: string;

    /**
     * Sets the boolean value to display "No results found " message
     */
    noFilteredResults(): boolean
    {
       return !!this.filterText;
    }

    /**
     * returns whether to display the filter text in the view
     * @returns {boolean}
     */
    displayFilterText(): boolean
    {
        return this.pageName !== noResultsPagesConst.EPISODE_VIEW
            && this.pageName !== noResultsPagesConst.SHOWS
            && this.pageName !== noResultsPagesConst.EPISODES
            && !!this.filterText;
    }

    ngOnChanges(changes: any)
    {
        if(changes.pageName)
        {
            this.noResultsPage = changes.pageName.currentValue;
        }
    }
}
