/**
 * @MODULE:     service-lib
 * @CREATED:    07/31/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  noResultsPagesConst are constants are used for get the data for different screens/pages.
 **/

export const noResultsPagesConst = {
    CHANNELS :              "channels",
    ONDEMAND:               "ondemand",
    EPISODE_VIEW:           "episodeView",
    SEARCH:                 "search",
    SHOWS:                  "show",
    EPISODES:               "episode",
    MANAGE_SHOW_REMINDERS:  "manageShowReminders",
    MANAGE_SEEDED_STATIONS: "manageSeededStations",
    RECENTLY_PLAYED:        "recentlyPlayed"
};
