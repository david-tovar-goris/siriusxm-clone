import { CommonModule} from "@angular/common";
import { NgModule } from "@angular/core";
import {TranslationModule} from "../translate/translation.module";
import { NoResultsComponent } from "./no-results.component";


@NgModule({
    imports: [
        CommonModule,
        TranslationModule
    ],
    declarations: [
        NoResultsComponent
    ],
    exports: [
        NoResultsComponent
    ]
})

export class NoResultsModule
{
}

