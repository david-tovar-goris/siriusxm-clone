import { combineLatest as observableCombineLatest, Observable } from 'rxjs';
import { map, filter, share } from 'rxjs/operators';
import { Injectable }                    from "@angular/core";
import { Store}                         from "@ngrx/store";
import * as _                          from "lodash";
import {
    IApronSegment,
    IMediaSegment,
    IMediaVideo,
    Logger,
    MediaUtil,
    CurrentlyPlayingService,
    VideoPlayerConstants,
    msToSeconds,
    MediaTimestampService
}                                      from "sxmServices";
import {SetApronSegments}              from "../../common/action/apron-segment.action";
import {
        IAppStore,
        IApronSegmentStore
}                                      from "../../common/store";
import {
    getEpisodeSegments,
    getEpisodeVideoMarkers,
    getMediaType
}                                      from "../../common/store/now-playing.store";
import {FormatSegmentTimestampService} from "./format-segment-timestamp.service";

@Injectable()
export class SegmentsService
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("SegmentsService");

    /**
     * The media type stream.
     */
    public mediaType$: Observable<string> = this.store.select(getMediaType);

    /**
     * The raw episode segments stream.
     */
    public rawSegments$: Observable<IMediaSegment[]> = this.store.select(getEpisodeSegments);

    /**
     * The raw episode video markers.
     */
    public rawVideoMarkers$: Observable<IMediaVideo[]> = this.store.select(getEpisodeVideoMarkers);

    /**
     * List of episode segments that are consumed and used by the UI.
     */
    public segments: IApronSegment[] = null;

    /**
     * The minimum number of segments required to expand and contract the list of segments.
     */
    private static MIN_SEGMENTS_TO_EXPAND: number = 4;

    /**
     * Constructor.
     * @param {MediaTimestampService} mediaTimestampService
     * @param {Store<IAppStore>} store
     * @param {CurrentlyPlayingService} currentlyPlayingService
     */
    constructor(private mediaTimestampService: MediaTimestampService,
                private store: Store<IAppStore>,
                public currentlyPlayingService: CurrentlyPlayingService)
    {
        this.init();
    }

    /**
     * Determines if a given segment is currently playing.
     * @param {IMediaSegment | IApronSegment} apronSegments
     * @returns {boolean}
     */

    public findCurrentlyPlayingSegment(apronSegments: IApronSegment[])
    {
        const result = apronSegments.find((apronSegment) =>
        {
            const playheadZulu = this.mediaTimestampService.playhead.currentTime.zuluMilliseconds;
            return playheadZulu < apronSegment.mediaSegment.times.zuluEndTime
                && playheadZulu >= apronSegment.mediaSegment.times.zuluStartTime;
        });
        return result || apronSegments[apronSegments.length - 1];
    }

    /**
     * Determines if user is playing audio at the live point.
     * @returns {boolean}
     */
    public isBehindLivePointObs(): Observable<boolean>
    {
        return this.mediaTimestampService.isPlayheadBehindLive$.pipe(share());
    }

    /**
     * Initialize this service so it starts listening for now playing store changes can create
     * the apron segments based on the the currently playing list of media segments.
     */
    public init(): Observable<any>
    {
        SegmentsService.logger.debug(`init()`);
        return this.createApronSegments();
    }

    /**
     * Create the apron segments and adds them to the store.
     *
     * The key here is listening to the list of raw segments from the API, the current playhead
     * time, and the media type streams and then updating the list of apron segments based on the latest data.
     */
    private createApronSegments(): Observable<any>
    {
        const self = this;
        const obs = observableCombineLatest([
                this.rawSegments$,
                this.mediaTimestampService.playheadTimestamp$,
                this.mediaType$,
                this.rawVideoMarkers$

            ]).pipe(
            filter(allObservablesHaveValues),
            map(createSegmentData.bind(this)));

        obs.subscribe(apronSegmentData => this.store.dispatch(new SetApronSegments(apronSegmentData as IApronSegmentStore)));
        return obs;

        function allObservablesHaveValues(result: any[]): boolean
        {
            return !!(result[0] && (!isNaN(result[1]) && (result[1] >= 0)) && result[2] && result[3]);
        }

        function createSegmentData(data: any[]): IApronSegmentStore
        {
            const mediaSegments: IMediaSegment[] = data[0] as IMediaSegment[];
            let videoMarkers: IMediaVideo[] = _.cloneDeep(data[3]) as IMediaVideo[];
            const mediaType: string = data[2] as string;
            const isLiveMediaType: boolean = MediaUtil.isLiveMediaType(mediaType);
            videoMarkers = videoMarkers.filter(vMarker=> vMarker.liveVideoStatus === VideoPlayerConstants.LIVE_VIDEO_ON);
            const apronSegments: IApronSegment[] = mediaSegments.reduce((apronSegments: IApronSegment[], mediaSegment: IMediaSegment) =>
            {
                const duration = mediaSegment.duration;
                const previousSegment = apronSegments[apronSegments.length - 1];
                const startTime = previousSegment
                    ? previousSegment.endTimeInSeconds
                    : 0;
                let videoStartTime:number = null;
                let matchedVideoMarker:any = null;

                if(isLiveMediaType)
                {
                    videoMarkers
                        .filter(vMarker =>
                        {
                            let mediaSegmentZuluEndTime = mediaSegment.times.zuluEndTime
                                ? mediaSegment.times.zuluEndTime : vMarker.times.zuluStartTime + 1;

                            let vMarkerZuluEndTime = vMarker.times.zuluEndTime
                                ? vMarker.times.zuluEndTime : mediaSegment.times.zuluStartTime + 1;

                            if ((vMarker.times.zuluStartTime >= mediaSegment.times.zuluStartTime &&
                                vMarker.times.zuluStartTime < mediaSegmentZuluEndTime)
                                || (vMarker.times.zuluStartTime < mediaSegment.times.zuluStartTime &&
                                    vMarkerZuluEndTime > mediaSegment.times.zuluStartTime))
                            {
                                return vMarker;
                            }
                        })
                        .some((videoMarker) =>
                        {
                            if (videoMarker.times.zuluStartTime < mediaSegment.times.zuluStartTime)
                            {
                                videoStartTime = mediaSegment.times.zuluStartTime;
                                matchedVideoMarker = videoMarker;
                                return true;
                            }
                            else if (videoMarker.times.zuluStartTime >= mediaSegment.times.zuluStartTime)
                            {
                                videoStartTime = videoMarker.times.zuluStartTime;
                                matchedVideoMarker = videoMarker;
                                return true;
                            }
                        });
                }

                apronSegments.push({
                    title: mediaSegment.title,
                    timeDisplay: this.getTimeDisplay(mediaSegment, startTime, isLiveMediaType),
                    startTimeInSeconds: startTime,
                    videoStartTime : videoStartTime,
                    videoMarker: matchedVideoMarker,
                    endTimeInSeconds: startTime + duration,
                    secondsFromBeginningOfEpisode: self.secondsFromBeginningOfEpisode(mediaSegment),
                    mediaSegment: mediaSegment
                });
                return apronSegments;
            }, []);

            return {
                // List of apron segments.
                segments: apronSegments,

                // Only show the list of segments more than 1 exist or if anyone of them has video start time.
                displaySegments: apronSegments.length > 1 || apronSegments.some(segment => !!segment.videoStartTime),

                // Only allow the expand and collapse of the UI list of segments if the min number to do so is met.
                canExpandSegmentsView: apronSegments.length > SegmentsService.MIN_SEGMENTS_TO_EXPAND,

                // Find the currently playing segment by looking at the current playhead value and the list of segments.
                currentlyPlayingSegment: self.findCurrentlyPlayingSegment(apronSegments)
            };
        }
    }

    /**
     * Creates a UI time tring for a given segment.
     * @param {IMediaSegment} segment
     * @param {number} startTime
     * @param {boolean} isLive
     * @returns {string}
     */
    public getTimeDisplay(segment: IMediaSegment, startTime: number, isLive: boolean): string
    {
        return isLive ?
            FormatSegmentTimestampService.formatStartTimeDisplay(segment.times.zuluStartTime / 1000) :
            FormatSegmentTimestampService.formatDurationDisplay(startTime);
    }

    /**
     * Returns a number that represents the number of seconds that a mediaSegment starts
     * after the start of an episode.
     * @param {IMediaSegment} mediaSegment
     * @returns {number}
     */
    public secondsFromBeginningOfEpisode(mediaSegment: IMediaSegment): number
    {
        const episodeStartZulu = this.currentlyPlayingService.getCurrentEpisodeZuluStartTime();
        if (episodeStartZulu === 0) return -1;
        const seconds =  msToSeconds(mediaSegment.times.zuluStartTime - episodeStartZulu);
        if (seconds < 0) return -1;
        return seconds;
    }
}
