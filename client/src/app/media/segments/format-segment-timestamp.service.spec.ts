import { FormatSegmentTimestampService } from "./format-segment-timestamp.service";

describe('FormatSegmentTimestampService', () =>
{
    describe('formatStartTimeDisplay()', () =>
    {
        it('returns a local time formatted as 10:10 PM for example', () =>
        {
            expect(FormatSegmentTimestampService.formatStartTimeDisplay(100000)).toMatch('^([0-1]?[0-9]|2[0-3]):[0-5][0-9] ?([AP][M])');
        });
    });

    describe('formatDurationDisplay()', () =>
    {
        describe('when seconds is less than an hour', () =>
        {
            it('returns returns a local time formatted as 59m for example', () =>
            {
                expect(FormatSegmentTimestampService.formatDurationDisplay(3000)).toEqual('50m');
            });
        });

        describe('when seconds is less than an hour and one minute', () =>
        {
            it('returns returns a local time formatted as 1h instead instead of 1h 0m', () =>
            {
                expect(FormatSegmentTimestampService.formatDurationDisplay(3650)).toEqual('1h 1m');
            });
        });

        describe('when seconds is greater than an hour and one minute', () =>
        {
            it('returns a local time formatted as 10:10 PM for example', () =>
            {
                expect(FormatSegmentTimestampService.formatDurationDisplay(3700)).toEqual('1h 2m');

            });
        });
    });
});
