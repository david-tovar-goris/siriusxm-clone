/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />

import { TestBed } from "@angular/core/testing";
import { SegmentsService } from "./segments.service";
import { Store, StoreModule } from "@ngrx/store";
import { nowPlayingReducer } from "../../common/reducer/now-playing.reducer";
import { Observable } from "rxjs";
import { INowPlayingActionPayload, SelectNowPlaying } from "../../common/action/now-playing.action";
import { CurrentlyPlayingService, MediaTimestampService } from "sxmServices";

describe('SegmentsService', function()
{
    beforeEach(function()
    {
        this.mediaTimestampService = {
            playheadTimestamp$: Observable.create((observer) => { observer.next(40); }),
            playhead: {
                currentTime: {
                    zuluMilliseconds: 13
                }
            }
        };
        this.currentlyPlayingService = {
            getCurrentEpisodeZuluStartTime: jasmine.createSpy('getCurrentEpisodeZuluStartTime').and.returnValue(0)
        };
        TestBed.configureTestingModule({
            imports: [
                StoreModule.forRoot({
                    nowPlayingStore: nowPlayingReducer
                })
            ],
            providers: [
                SegmentsService,
                { provide: MediaTimestampService, useValue: this.mediaTimestampService },
                { provide: CurrentlyPlayingService, useValue: this.currentlyPlayingService }
            ]
        });
        this.store = TestBed.get(Store);
        spyOn(this.store, 'dispatch').and.callThrough();
        this.segmentsService = TestBed.get(SegmentsService);
    });

    describe("findCurrentlyPlayingSegment()", function()
    {
        beforeEach(function()
        {
            this.apronSegments = [
                {
                    mediaSegment: {
                        times: {
                            zuluStartTime: 0,
                            zuluEndTime: 10
                        }
                    }
                },
                {
                    mediaSegment: {
                        times: {
                            zuluStartTime: 20,
                            zuluEndTime: 30
                        }
                    }
                }
            ];
        });

        it("finds correct segment", function()
        {
            let result = this.segmentsService.findCurrentlyPlayingSegment(this.apronSegments);
            expect(result).toEqual(this.apronSegments[1]);
        });
    });

    describe("init()", function()
    {
        describe("when mediaType is live, number of segments more than one, not enough to expand", function()
        {
            beforeEach(function()
            {
                let payload = {
                    mediaType: "live",
                    episode: {
                        segments: [
                            {
                                duration: 60,
                                endTimeInSeconds: 60,
                                title: 'A',
                                times: {
                                    zuluStartTime: 1525456840000
                                }
                            },
                            {
                                duration: 60,
                                endTimeInSeconds: 0,
                                title: 'B',
                                times: {
                                    zuluStartTime: 1525456920000
                                }
                            }
                        ]
                    }
                } as INowPlayingActionPayload;
                this.store.dispatch(new SelectNowPlaying(payload));
            });

            it('sets segment titles to correct values', function(done)
            {
                let obs = this.segmentsService.init();
                obs.subscribe((apronSegmentsData) =>
                {
                    expect(apronSegmentsData.segments[0].title).toEqual('A');
                    expect(apronSegmentsData.segments[1].title).toEqual('B');
                    done();
                });
            });

            it('sets segment start and end times to correct values', function(done)
            {
                let obs = this.segmentsService.init();
                obs.subscribe((apronSegmentsData) =>
                {
                    expect(apronSegmentsData.segments[0].startTimeInSeconds).toEqual(0);
                    expect(apronSegmentsData.segments[0].endTimeInSeconds).toEqual(60);

                    expect(apronSegmentsData.segments[1].startTimeInSeconds).toEqual(60);
                    expect(apronSegmentsData.segments[1].endTimeInSeconds).toEqual(120);
                    done();
                });
            });

            it('calls getTimeDisplay()', function(done)
            {
                spyOn(this.segmentsService, 'getTimeDisplay');
                let obs = this.segmentsService.init();
                obs.subscribe(() =>
                {
                    expect(this.segmentsService.getTimeDisplay.calls.count()).toBe(4);
                    done();
                });

            });

            it('sets displaySegments to false', function(done)
            {
               let obs = this.segmentsService.init();
               obs.subscribe((apronSegmentsData) =>
               {
                   expect(apronSegmentsData.displaySegments).toBe(true);
                   done();
               });
            });

            it('sets canExpandSegmentsView to false', function(done)
            {
                let obs = this.segmentsService.init();
                obs.subscribe((apronSegmentsData) =>
                {
                    expect(apronSegmentsData.canExpandSegmentsView).toBe(false);
                    done();
                });
            });

            it('sets canExpandSegmentsView to false', function(done)
            {
                let obs = this.segmentsService.init();
                obs.subscribe((apronSegmentsData) =>
                {
                    expect(apronSegmentsData.canExpandSegmentsView).toBe(false);
                    done();
                });
            });

            it('sets correct currentlyPlayingSegment', function(done)
            {
                let obs = this.segmentsService.init();
                obs.subscribe((apronSegmentsData) =>
                {
                    expect(apronSegmentsData.currentlyPlayingSegment).toEqual(apronSegmentsData.segments[1]);
                    done();
                });
            });
        });
    });
});
