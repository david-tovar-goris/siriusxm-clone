import { Injectable } from "@angular/core";
import * as moment from "moment";
const oneHour = 3600;
const oneMinute = 60;
const halfMinute = 30;

@Injectable()
export class FormatSegmentTimestampService
{
    public static formatStartTimeDisplay(seconds: number): string
    {
        const timeInSeconds = FormatSegmentTimestampService.roundToNearestMinute(seconds);
        return timeInSeconds.local().format("hh:mm a").replace(/^0+/, '').toUpperCase();
    }

    public static formatDurationDisplay(seconds: number): string
    {
        const duration = FormatSegmentTimestampService.roundToNearestMinute(seconds);
        let durationFormat = "";

        if (seconds < (oneHour-halfMinute))
        {
            durationFormat = "m[m]";
        }
        else if (seconds < (oneHour+halfMinute))
        {
            durationFormat = "h[h]";
        }
        else
        {
            durationFormat = "h[h] m[m]";
        }

        return duration.format(durationFormat);
    }

    private static roundToNearestMinute(seconds: number)
    {
        const duration = moment.duration(seconds, "seconds");

        if (seconds === 0)
        {
            return moment.utc(duration.asMilliseconds());
        }
        else
        {
            return moment.utc(duration.asMilliseconds())
                .add(halfMinute, "seconds")
                .startOf("minute");
        }
    }
}
