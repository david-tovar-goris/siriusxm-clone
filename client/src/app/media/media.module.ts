import { NgModule } from "@angular/core";
import { Logger } from "sxmServices";
import { MediaAssetService } from "./media-player/media-asset.service";
import { ProgressBarService } from "./progress-bar/progress-bar.service";
import { FormatSegmentTimestampService } from "./segments/format-segment-timestamp.service";
import { SegmentsService } from "./segments/segments.service";

@NgModule({
    providers: [
        ProgressBarService,
        MediaAssetService,
        SegmentsService,
        FormatSegmentTimestampService
    ]
})

export class MediaModule
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("MediaModule");

    /**
     * Eagerly instantiate the segments service.
     * @param {SegmentsService} segmentsService - Maps media segments from the now playing store to UI segments.
     */
    constructor(private segmentsService: SegmentsService) {}
}
