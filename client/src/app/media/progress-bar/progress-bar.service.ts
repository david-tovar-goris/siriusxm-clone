import {
    filter,
    map,
    distinctUntilChanged
} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable ,  BehaviorSubject, combineLatest } from "rxjs";
import { IAppStore } from "../../common/store/app.store";
import { SeekService, KeyboardEventKeyTypes, DmcaService, MediaUtil, MediaTimestampService } from "sxmServices";
import { NowPlayingStoreService } from "../../common/service/now-playing.store.service";
import {
    INowPlayingStore,
    selectNowPlayingState
} from "../../common/store/now-playing.store";

@Injectable()
export class ProgressBarService
{
    public isDragging: boolean = false;
    public nowPlayingStore: INowPlayingStore;

    /**
     * isDragging BehaviorSubject
     */
    public isBeingDragged$: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public isBeingDragged: Observable<boolean>;

    private nowPlayingStore$: Observable<INowPlayingStore> = this.store.select(selectNowPlayingState);

    private playheadPercentageSubject: BehaviorSubject<number>;
    public playheadPercentage$: Observable<number>;

    public liveTimePercentage$: Observable<number>;

    public progressBarStartX : number = 0;
    public progressBarWidth: number  = 0;

    constructor(private mediaTimestampService: MediaTimestampService,
                private seekService: SeekService,
                private nowPlayingStoreService: NowPlayingStoreService,
                private store: Store<IAppStore>,
                private dmcaService: DmcaService)
    {
        this.observeNowPlayingStore();
        this.isBeingDragged = this.isBeingDragged$;

        this.playheadPercentageSubject = new BehaviorSubject<number>(
            this.progressInPercentage(
                this.mediaTimestampService.playheadTimestamp
            )
        );
        this.playheadPercentage$ = this.playheadPercentageSubject;

        this.observePlayHeadTime();
        this.observeLiveTime();
    }

    /**
     * Get the progress bar width and startx values from the DOM element.  This needs to be done on the
     * initial creation of a component and whenever a resize event is received
     *
     * @param width
     * @param startX
     */
    public onResize(width, startX)
    {
        this.progressBarWidth = width;
        this.progressBarStartX =  startX;
    }

    /**
     * Seek the player when the progress bar is clicked.
     * This method is also triggered when the user stops dragging.
     */
    public onClickProgressBar(evt: any, width: number, startX: number): void
    {

        // exit early if the media is restricted
        // TODO: Jordan D. Nelson
        // This might be a bug, I did not write this logic.
        // restricted is not the same as not unrestricted because disallowed
        // is also in the set of possibilities.
        if (!this.dmcaService.isUnrestricted(this.nowPlayingStore)) { return; }

        // if you are dragging return early.
        // this method will get called again by onDragStop
        // when the event bubbles up to player controls level.
        if (this.isDragging) return;

        let maximumX = width;
        let liveX = Math.floor(this.mediaTimestampService.livePointTimestamp *
                               width / this.mediaTimestampService.durationTimestamp);

        // simulates offsetX, which we can't rely on because of stacked progress bar elements
        let mouseX = evt.pageX - startX;

        // guard against dragging the mouse outside of the bounds of the element
        mouseX = (mouseX < 0) ? 0 : mouseX;
        mouseX = (mouseX < maximumX) ? mouseX : maximumX;

        if (MediaUtil.isLiveMediaType(this.nowPlayingStore.mediaType) && mouseX >= liveX && !evt.doNotSkipForward)
        {
            return;
        }
        const seekTime: number = (mouseX / maximumX) * this.mediaTimestampService.getDurationTimestamp();
        this.seekService.seekThenPlay(seekTime, false);
    }

    public onDragStart(): void
    {
        this.isDragging = true;
        this.mediaTimestampService.isDraggingScrubBall = true;
        this.isBeingDragged$.next(true);
    }

    public onDragMove(evt: any): void
    {
        const width: number = this.progressBarWidth;
        const startX: number = this.progressBarStartX;

        // exit early if the mouse button isn't down
        if (!this.isDragging) { return; }
        const pageX = evt.pageX || evt.touches[0].pageX;

        this.isBeingDragged$.next(true);

        let maximumX = width;

        // simulates offsetX, which we can't rely on because of stacked progress bar elements
        let mouseX = pageX - startX;

        // guard against dragging the mouse outside of the bounds of the element
        mouseX = (mouseX < 0) ? 0 : mouseX;
        mouseX = (mouseX < maximumX) ? mouseX : maximumX;

        if (MediaUtil.isLiveMediaType(this.nowPlayingStore.mediaType))
        {
            let maximumXForLive = this.mediaTimestampService.livePointTimestamp *
                                             width / this.mediaTimestampService.durationTimestamp;

            mouseX = (mouseX < maximumXForLive) ? mouseX : maximumXForLive;

            if (mouseX >= maximumXForLive)
            {
                this.onDragStop({
                    pageX: startX + maximumXForLive,
                    doNotSkipForward: true
                });
            }
        }

        const seekTime: number = (mouseX / maximumX) * this.mediaTimestampService.getDurationTimestamp();
        this.mediaTimestampService.setPlayheadTimestamp(seekTime);

        this.nowPlayingStoreService.updateVideoMarker(seekTime);
    }

    public onDragStop(evt: any): void
    {
        const width: number = this.progressBarWidth;
        const startX: number = this.progressBarStartX;

        if (this.isDragging)
        {
            this.isDragging = false;
            this.mediaTimestampService.isDraggingScrubBall = false;
            this.isBeingDragged$.next(false);
            this.onClickProgressBar(evt,width,startX);
        }
    }

    /**
     * Handles keyboard event when progress bar is in focus.
     * @param event
     */
    public onKeyboardScrub(event: any)
    {
        const isArrowRight =
            (event.key === KeyboardEventKeyTypes.ARROW_RIGHT) ||
            (event.key === KeyboardEventKeyTypes.ARROW_RIGHT_IE); // skip forward 15 seconds
        const isArrowLeft =
            (event.key === KeyboardEventKeyTypes.ARROW_LEFT) ||
            (event.key === KeyboardEventKeyTypes.ARROW_LEFT_IE); // skip backward 15 seconds
        const isArrowUp =
                  (event.key === KeyboardEventKeyTypes.ARROW_UP) ||
                  (event.key === KeyboardEventKeyTypes.ARROW_UP_IE); // skip forward 15 seconds
        const isArrowDown =
                  (event.key === KeyboardEventKeyTypes.ARROW_DOWN) ||
                  (event.key === KeyboardEventKeyTypes.ARROW_DOWN_IE); // skip backward 15 seconds
        const SECONDS_TO_SCCRUB = 15;

        if (!isArrowLeft && !isArrowRight && !isArrowUp && !isArrowDown) return;

        // necessary to keep screen reader from blowing up
        event.preventDefault();

        let seconds: number = 0;

        if (isArrowRight || isArrowUp)
        {
            seconds = ((this.mediaTimestampService.playheadTimestamp + SECONDS_TO_SCCRUB) < this.mediaTimestampService.durationTimestamp)
                ? (this.mediaTimestampService.playheadTimestamp + SECONDS_TO_SCCRUB)
                : this.mediaTimestampService.durationTimestamp;
        }
        else if (isArrowLeft || isArrowDown)
        {
            seconds = (this.mediaTimestampService.playheadTimestamp > SECONDS_TO_SCCRUB)
                ? (this.mediaTimestampService.playheadTimestamp - SECONDS_TO_SCCRUB)
                : 0;
        }

        this.seekService.seekThenPlay(seconds, false);
    }

    /**
     * Returns a number (percentage) which is used to set the X position of elements on the progress bar.
     */
    public progressInPercentage(seconds: number)
    {
        const percentage: number = (seconds/this.mediaTimestampService.getDurationTimestamp())*100;

        // If any of our values take time to update, there's a chance we might send back a value
        // greater than the width of the progress bar.  This conditional prevents that.
        return (percentage < 100)
               ? percentage
               : 100;
    }

    private observeNowPlayingStore(): void
    {
        this.nowPlayingStore$
            .pipe(filter(nowPlayingStore => !!nowPlayingStore))
            .subscribe((nowPlayingStore: INowPlayingStore) =>
            {
                this.nowPlayingStore = nowPlayingStore;
            });
    }

    /**
     * Observe Media Time stamp and set it to Play head time
     */
    private observePlayHeadTime(): void
    {
        this.mediaTimestampService.playheadTimestamp$.subscribe(playheadTimestamp =>
        {
            this.playheadPercentageSubject.next(this.progressInPercentage(playheadTimestamp));
        });
    }

    /**
     * Observe Live Time Stamp and return in percentage
     */
    private observeLiveTime(): void
    {
        this.liveTimePercentage$ = this.mediaTimestampService.liveTime$.pipe(
            filter(val => !!val),
            map(liveTimestamp =>
            {
                return this.progressInPercentage(liveTimestamp);
            }),
            distinctUntilChanged());
    }
}
