import { ProgressBarService } from "./progress-bar.service";
import { MediaTimestampServiceMock } from "../../../../test/mocks/media-timestamp.service.mock";
import { NowPlayingStoreServiceMock } from "../../../../test/mocks/now-playing.store.service.mock";
import { SeekServiceMock } from "../../../../test/mocks/seek.service.mock";
import {
    KeyboardEventKeyTypes,
    DmcaService
} from "sxmServices";
import { BehaviorSubject } from 'rxjs';
import { IAppStore } from "../../common/store/app.store";
import { Store } from '@ngrx/store';

interface ThisContext{
    service: ProgressBarService;
    mediaTimestampService: any;
    nowPlayingStoreService: any;
    seekService: any;
    dmcaService: DmcaService;
    store: any;
    nowPlayingStore: BehaviorSubject<any>;
    setupModule: any;
}

describe('ProgressBarService', function()
{
    let event = {
            pageX: 0
        },
        keyboardEvent = {
            key: "",
            preventDefault: function()
            {}
        };

    const seconds = 10;

    beforeEach(function(this: ThisContext)
    {
        this.nowPlayingStore = new BehaviorSubject<any>(null);
        this.nowPlayingStoreService = new NowPlayingStoreServiceMock();
        this.mediaTimestampService = new MediaTimestampServiceMock();
        this.seekService = new SeekServiceMock();

        this.store = {
            select: () => this.nowPlayingStore
        } as any as Store<IAppStore>;

        this.dmcaService = {
            isUnrestricted: function(){return true;},
            isRestricted: jasmine.createSpy('isRestricted'),
            isDisallowed: jasmine.createSpy('isDisallowed'),
            skipInfoDictionary$: new BehaviorSubject<any>({}) as any
        } as any as DmcaService;

        this.service = new ProgressBarService(
            this.mediaTimestampService,
            this.seekService,
            this.nowPlayingStoreService,
            this.store,
            this.dmcaService
        );

        this.nowPlayingStore.next({
            mediaType: 'aod'
        });
    });

    describe('onKeyboardScrub()', function()
    {
        describe('when the content is unrestricted', function()
        {
            beforeEach(function(this: ThisContext)
            {
                spyOn(this.seekService, 'seekThenPlay');
            });

            it('seeks forward 15 seconds when right arrow key is clicked', function(this: ThisContext)
            {
                this.mediaTimestampService.playheadTimestamp = 10;
                this.mediaTimestampService.durationTimestamp = 50;
                keyboardEvent.key = KeyboardEventKeyTypes.ARROW_RIGHT;

                this.service.onKeyboardScrub(keyboardEvent);

                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(25, false);
            });

            it('seeks backward 15 seconds when left arrow key is clicked', function(this: ThisContext)
            {
                this.mediaTimestampService.playheadTimestamp = 25;
                this.mediaTimestampService.durationTimestamp = 50;
                keyboardEvent.key = KeyboardEventKeyTypes.ARROW_LEFT;

                this.service.onKeyboardScrub(keyboardEvent);

                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(10, false);
            });
        });
    });

    describe('onClickProgressBar()', function()
    {
        describe('when the content is unrestricted', function()
        {
            it('seeks to the time returned from the position of the scrubber', function(this: ThisContext)
            {
                spyOn(document, 'getElementById').and.returnValue({
                    offsetWidth: 10
                });
                spyOn(this.mediaTimestampService, 'getDurationTimestamp').and.returnValue(0);
                spyOn(this.seekService, 'seekThenPlay');
                this.mediaTimestampService.livePointTimestamp = 10;
                this.mediaTimestampService.durationTimestamp = 10;
                this.service.onClickProgressBar({pageX: 0, pageY: 0},10,0);
                expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(0, false);
            });
        });

        describe('when the content is Live ' +
            '&& the mouse position is > the Live point ' +
            '&& the progress ball should not skip forward', function()
        {
            it('returns early', function(this: ThisContext)
            {
                event.pageX = 11;

                this.nowPlayingStore.next({ mediaType: 'live' });
                spyOn(this.seekService, 'seek');
                this.mediaTimestampService.livePointTimestamp = 10;
                this.mediaTimestampService.durationTimestamp = 10;
                this.service.onClickProgressBar(event,10,0);
                expect(this.seekService.seek).toHaveBeenCalledTimes(0);
            });
        });
    });

    describe('onDragMove()', function()
    {
        describe('when isDragging is truthy', function()
        {
            describe('when content is Live', function()
            {
                it('when the position of the mouse when the event fired is > than the Live point', function(this: ThisContext)
                {
                    this.service.isDragging = true;
                        spyOn(document, 'getElementById').and.returnValue({
                            offsetWidth: 10
                        });
                        spyOn(this.service, 'onDragStop');
                        this.mediaTimestampService.durationTimestamp = 10;
                        event.pageX = 15;
                        this.mediaTimestampService.livePointTimestamp = 12;

                        this.service.onDragMove(event);
                        expect(this.nowPlayingStoreService.updateVideoMarker).toHaveBeenCalled();
                });

                it('dispatches onDragStop to prevent the user from scrubbing beyond Live', function(this: ThisContext)
                {
                    this.service.isDragging = true;
                    let spy = spyOn(this.service,"onClickProgressBar");

                    this.service.onDragStop(null);
                    expect(spy).toHaveBeenCalled();
                });

            });

            describe('when content is not Live', function()
            {
                it('calls the mediaTimestampService to set the playheadTimestamp', function(this: ThisContext)
                {
                    this.service.isDragging = true;
                    spyOn(document, 'getElementById').and.returnValue({
                        offsetWidth: 15
                    });

                    spyOn(this.service, 'onDragStop');
                    this.service.progressBarWidth = 10;
                    this.service.progressBarStartX = 0;
                    this.mediaTimestampService.durationTimestamp = 10;
                    event.pageX = 15;
                    this.mediaTimestampService.livePointTimestamp = 12;

                    this.service.onDragMove(event);
                    expect((this.mediaTimestampService.setPlayheadTimestamp as any).calls.argsFor(0)).toContain(10);
                    expect(this.nowPlayingStoreService.updateVideoMarker).toHaveBeenCalled();
                });
            });
        });
    });

    describe('onDragStop()', function()
    {
        describe('when isDragging is truthy', function()
        {
            beforeEach(function(this: ThisContext)
            {
                spyOn(this.service, 'onClickProgressBar');
                this.service.isDragging = true;
                this.service.onDragStop(event);
            });

            it('sets isDragging to false', function(this: ThisContext)
            {
                expect(this.service.isDragging).toEqual(false);
            });

            it('calls onClickProgressBar with the new position', function(this: ThisContext)
            {
                expect(this.service.onClickProgressBar).toHaveBeenCalledWith(event,0,0);
            });
        });
    });

    describe("OnObserver ", function()
    {

        it("PlayHeadTime", function(this: ThisContext)
        {
            let percentage = 0;
            this.mediaTimestampService.playheadTimestamp = 0;
            spyOn(this.mediaTimestampService, 'getDurationTimestamp').and.returnValue(200);
            this.service.playheadPercentage$.subscribe(res =>
            {
                percentage = res;

            });
            this.mediaTimestampService.playheadTimestamp$.next("150");
            expect(percentage).toBe(75);
        });

        it("LiveTime", function(this: ThisContext)
        {
            let percentage = 0;
            spyOn(this.mediaTimestampService, 'getDurationTimestamp').and.returnValue(200);
            this.service.liveTimePercentage$.subscribe( res =>
            {
                percentage = res;

            });
            this.mediaTimestampService.liveTime$.next("150");
            expect(percentage).toBe(75);
        });
    });
});
