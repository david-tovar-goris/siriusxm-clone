import { TestBed } from "@angular/core/testing";
import { MediaAssetService } from "./media-asset.service";
import { NowPlayingStoreService } from "../../common/service/now-playing.store.service";
import { NowPlayingStoreServiceMock } from "../../../../test/mocks/now-playing.store.service.mock";
import { MediaPlayerServiceMock } from "../../../../test/mocks/media-player.service.mock";
import { MediaPlayerService } from "sxmServices";

describe('MediaAssetService', () =>
{
    let service: MediaAssetService;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
          providers: [
              MediaAssetService,
              { provide: NowPlayingStoreService, useClass: NowPlayingStoreServiceMock },
              { provide: MediaPlayerService, useClass: MediaPlayerServiceMock }
          ]
        });
        service = TestBed.get(MediaAssetService);
    });

    describe('PRIVATE - getMediaAssetMetadata()', () =>
    {
        it('calls the mediaPlayer to retrieve the metadata', () =>
        {
            expect(service.mediaAssetMetadata).toEqual({
                mediaId: "",
                seriesName: "",
                episodeTitle: "",
                apronSegments: []
            });
        });
    });
});
