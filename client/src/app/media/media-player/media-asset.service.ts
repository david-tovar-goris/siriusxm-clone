import { Injectable } from "@angular/core";
import {
    ContentTypes,
    IMediaAssetMetadata,
    MediaPlayerService
} from "sxmServices";
import { NowPlayingStoreService } from "../../common/service/now-playing.store.service";
import { SubscriptionLike as ISubscription } from "rxjs";
import { INowPlayingStore } from "../../common/store";

@Injectable()

export class MediaAssetService
{
    public mediaAssetMetadata: IMediaAssetMetadata;
    private nowPlayingStoreData: INowPlayingStore;

    constructor(private mediaPlayerService: MediaPlayerService,
                private nowPlayingStore: NowPlayingStoreService)
    {
        this.observeNowPlayingStore();
    }

    private getMediaAssetMetadata(): IMediaAssetMetadata
    {
        return this.mediaAssetMetadata = this.mediaPlayerService.mediaPlayer.getMediaAssetMetadata();
    }

    private resetMediaAssetMetadata(): IMediaAssetMetadata
    {
        return this.mediaAssetMetadata = {
            mediaId: "",
            seriesName: "",
            episodeTitle: "",
            apronSegments: []
        };
    }

    private observeNowPlayingStore(): ISubscription
    {
        return this.nowPlayingStore.nowPlayingStore.subscribe((data) =>
        {
            const currentMediaType: string = this.nowPlayingStoreData && this.nowPlayingStoreData.mediaType || "";
            const isNewMediaType: boolean = currentMediaType !== data.mediaType;

            this.nowPlayingStoreData = data;

            this.getMediaAssetMetadata();

            if (isNewMediaType || (currentMediaType === ContentTypes.VOD && this.mediaPlayerService.isNewMedia(data)))
            {
                this.resetMediaAssetMetadata();
                this.getMediaAssetMetadata();
            }
        });
    }
}
