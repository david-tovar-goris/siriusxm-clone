import { combineLatest as observableCombineLatest, Observable, Subscription } from 'rxjs';
import { filter, share } from 'rxjs/operators';
import { Injectable }                                                    from "@angular/core";
import { Store }                                                         from "@ngrx/store";
import { ConfigService, Logger, MediaTimestampService, MediaUtil } from 'sxmServices';
import { CarouselStoreService }                                          from "../common/service/carousel.store.service";
import { IAppStore }                                                     from "../common/store";
import { getContentType }                                                from "../common/store/now-playing.store";
import * as _                                                            from "lodash";
import { NowPlayingStoreService }                         from "../common/service/now-playing.store.service";

/**
 * @MODULE:     client
 * @CREATED:    05/07/18
 * @COPYRIGHT:  2018 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *   AutoplayService gathers tiles from multiple carousel sources,
 *   orders them by business rules.  It then exposes a
 *   collection, "compositeTiles", to the AutoplayComponent.
 */

@Injectable()
export class AutoplayService
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("AutoplayService");

    /**
     * Observable string indicating the media type is AOD or VOD.
     */
    public contentType$: Observable<string> = this.store.select(getContentType);

    /**
     * Indicates if the currently playing media can go to live.
     */
    public canGotoLive$: Observable<boolean> = this.mediaTimestampService.isPlayheadBehindLive$.pipe(share());

    /**
     * Stores current upNextCarousels which will help to prevent multiple calls to api.
     */
    private upNextCarouselMediaGuidSet = {
        channelGuid: null,
        showGuid: null,
        cutGuid: null,
        episodeGuid: null
    };

    /**
     * Constructor
     * @param {CarouselStoreService} carouselStoreService
     * @param {Store<IAppStore>} store
     * @param {MediaTimestampService} mediaTimestampService
     * @param {NowPlayingStoreService} nowPlayingStoreService
     */
    constructor(private carouselStoreService: CarouselStoreService,
                private store: Store<IAppStore>,
                private mediaTimestampService: MediaTimestampService,
                private nowPlayingStoreService: NowPlayingStoreService,
                public configService: ConfigService)
    {
        this.getUpNextCarousels();
    }

    /**
     * Used to Kick off UpNext carousel when the video reaches end ( 5 sec left)
     * and also when we play new video.
     */
    getUpNextCarousels()
    {
        const upNextCarousels = observableCombineLatest(this.nowPlayingStoreService.nowPlayingStore,
                                                         this.mediaTimestampService.playheadTimestamp$,
                                                         this.contentType$);

        upNextCarousels.pipe(filter(([nowPlayingData, playheadTime, contentType]) =>
        {
            //In case of user tuning away from OD to live content- we should reset the upNextCarouselMediaGuidSet
            // as when user comes back to same OD content after live-> we know that previous upNextGuid is differnt than one we are tuning now
            if(!(MediaUtil.isOnDemandMediaType(contentType)) && this.upNextCarouselMediaGuidSet)
            {
                this.upNextCarouselMediaGuidSet = null;
            }
            return ( MediaUtil.isOnDemandMediaType(contentType)
                    && !!nowPlayingData.mediaId
                    && this.mediaTimestampService.durationTimestamp > 0);
        })).subscribe(([nowPlayingData, playheadTime, contentType]) =>
        {
            let timeLeft : number = Math.max(this.mediaTimestampService.durationTimestamp - playheadTime, 0);
            let mediaGuidSet      = {
                channelGuid: _.get(nowPlayingData, "channel.channelGuid", null),
                showGuid   : _.get(nowPlayingData, "show.assetGUID", null),
                cutGuid    : _.get(nowPlayingData, "cut.assetGUID", null),
                episodeGuid: _.get(nowPlayingData, "episode.assetGUID", null)
            };

            const timeBeforeEnd = this.configService.getAutoplayNextEpisodeDelay() * 2;

            //We are making the upNext call immediately we tune new OD and we display when the user is at the end of the media
            if (!!mediaGuidSet.episodeGuid &&
                (!this.upNextCarouselMediaGuidSet || this.upNextCarouselMediaGuidSet.episodeGuid !== mediaGuidSet.episodeGuid))
            {
                this.upNextCarouselMediaGuidSet = mediaGuidSet;
                this.carouselStoreService.selectNowPlayingUpNext(mediaGuidSet.channelGuid,
                                                                 mediaGuidSet.showGuid,
                                                                 mediaGuidSet.episodeGuid,
                                                                 mediaGuidSet.cutGuid,
                                                                 contentType);
            }

        });
    }
}
