import { Component, Input, ViewChild, ElementRef } from "@angular/core";

@Component({
    selector: "countdown",
    template: `
       <div class="countdown" fill="#fff">
           <div class="countdown__number" #countdown></div>
           <svg>
               <circle r="18" cx="20" cy="20"></circle>
           </svg>
       </div>
    `,
    styleUrls: [ `./countdown.component.scss` ]
})

export class CountdownComponent
{
    /**
     * The countdown timer length in seconds
     */
    @Input() countdownNumber: number = 10;

    @ViewChild('countdown') countdown: ElementRef;

    ngOnInit(): void
    {
        let countdown = this.countdownNumber;

        this.countdown.nativeElement.innerText = countdown;

        setInterval(() =>
        {
            countdown = countdown > 0 ? --countdown : countdown;

            this.countdown.nativeElement.innerText = countdown;
        }, 1000);
    }
}
