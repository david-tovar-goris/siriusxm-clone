import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { CarouselModule } from "../carousel/carousel.module";
import { ContentTilesModule } from "../common/component/tiles/content-tiles.module";
import { AutoplayService } from "./autoplay.service";
import { PlaybackCompleteService } from "./playback-complete.service";
import { TranslationModule } from "../translate/translation.module";
import { AutoplayComponent } from "./autoplay.component";
import { CountdownComponent } from "./countdown.component";
import { ContextMenuModule } from "../context-menu/context-menu.module";
import { SharedModule } from "app/common/shared.module";

@NgModule({
    imports: [
        CarouselModule,
        CommonModule,
        TranslationModule,
        ContentTilesModule,
        ContextMenuModule,
        SharedModule
    ],
    declarations: [
        AutoplayComponent,
        CountdownComponent
    ],
    exports: [
        CountdownComponent
    ],
    entryComponents:[
        AutoplayComponent
    ],
    providers: [
        AutoplayService,
        PlaybackCompleteService
    ]
})

export class AutoplayModule
{
}
