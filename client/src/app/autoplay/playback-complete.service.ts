import { combineLatest } from "rxjs";
import { filter, pairwise } from "rxjs/operators";
import {
    ComponentFactoryResolver,
    ViewContainerRef,
    Injectable,
    ComponentRef,
    ApplicationRef
}                                 from "@angular/core";
import {
    Router,
    NavigationEnd
}                                 from "@angular/router";
import { AutoplayComponent }      from "./autoplay.component";
import {
    AudioPlayerService,
    ContentTypes,
    ICarouselDataByType,
    VideoPlayerService,
    MediaPlayerConstants
}                                 from "sxmServices";
import * as screenfull            from 'screenfull';
import { SingletonVideoService }  from '../video/singleton-video/singleton-video.service';
import { NowPlayingStoreService } from "../common/service/now-playing.store.service";
import {CarouselStoreService}     from "../common/service/carousel.store.service";

@Injectable()
export class PlaybackCompleteService
{
    private autoplayCompleteComponentRef: ComponentRef<AutoplayComponent> = null;

    /**
     * Constructor
     * @param {ComponentFactoryResolver} componentFactoryResolver
     * @param {Router} router
     * @param {AudioPlayerService} audioPlayerService
     * @param {ApplicationRef} applicationRef
     * @param {SingletonVideoService} singletonVideoService
     * @param {VideoPlayerService} videoPlayerService
     */
    constructor(private componentFactoryResolver: ComponentFactoryResolver,
                private router: Router,
                private audioPlayerService: AudioPlayerService,
                private applicationRef: ApplicationRef,
                private singletonVideoService: SingletonVideoService,
                private videoPlayerService: VideoPlayerService,
                private nowPlayingStoreService: NowPlayingStoreService,
                private carouselStoreService: CarouselStoreService)
    {
        this.router.events.pipe(
            filter((event) => (event instanceof NavigationEnd)),
            pairwise()
        )
        .subscribe((events: [NavigationEnd, NavigationEnd]) =>
        {
            if(events[0].urlAfterRedirects.indexOf("now-playing") === -1)
            {
                this.close();
            }
        });

        // There's quite a bit of setup to get the video player up and running and it's all async processes,
        // so we'll use this stream to indicate when it's safe to interact with it.
        combineLatest(this.nowPlayingStoreService.contentType$,
            this.videoPlayerService.playbackReady$,
            this.videoPlayerService.playbackComplete$,
            this.audioPlayerService.playbackComplete$,
            this.carouselStoreService.nowPlayingUpNextCarousels)
        .pipe(filter(([contentType, playbackReady, videoPlaybackComplete, audioPlaybackComplete, upNextCarousel]) =>
        {
            const tilesAvailable = (!!upNextCarousel && upNextCarousel.zone[0]
                && !!upNextCarousel.zone[0].content[0].tiles
                && upNextCarousel.zone[0].content[0].tiles.length > 0);

            // There is no point in triggering the up next UI if there are no up next tiles available
            // to display or play
            if (!tilesAvailable)
            {
                return tilesAvailable;
            }

            if (contentType === ContentTypes.VOD)
            {
                return playbackReady && videoPlaybackComplete;
            }
            if (contentType === ContentTypes.AOD || contentType === ContentTypes.PODCAST)
            {
                return audioPlaybackComplete;
            }

            return false;
        }))
        .subscribe(([contentType, playbackReady, videoPlaybackComplete, audioPlaybackComplete, upNextCarousel]) =>
        {
            if (contentType === ContentTypes.VOD && screenfull.isFullscreen)
            {
                this.open(this.singletonVideoService.upNextViewContainerRef, upNextCarousel);
            }
            else
            {
                this.open(this.applicationRef.components[0].instance.viewContainerRef, upNextCarousel);
            }
        });

        /**
         *  To close upnext if play event triggered from outside Mini player controller
         *  eg: spacebar, Firefox-pic in pic.
         */
        combineLatest(this.videoPlayerService.playbackState,
                      this.audioPlayerService.playbackState)
            .pipe(filter( ([videoState, audioState]) =>
                    {
                        return (videoState === MediaPlayerConstants.PLAYING || audioState === MediaPlayerConstants.PLAYING)
                            && (this.autoplayCompleteComponentRef && !this.autoplayCompleteComponentRef.hostView.destroyed);
                    })
            )
            .subscribe(() =>
            {
                this.close();
            });
    }

    public open(viewContainerRef: ViewContainerRef, upNextCarousel: ICarouselDataByType): void
    {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(AutoplayComponent);
        this.autoplayCompleteComponentRef = viewContainerRef.createComponent(componentFactory);
        this.autoplayCompleteComponentRef.instance._ref = this.autoplayCompleteComponentRef;
        this.autoplayCompleteComponentRef.instance.upNextCarousel = upNextCarousel.zone[0].content[0].tiles;
    }

    public close(): void
    {
        if(this.autoplayCompleteComponentRef && !this.autoplayCompleteComponentRef.hostView.destroyed)
        {
            this.autoplayCompleteComponentRef.instance.close();
        }
    }
}
