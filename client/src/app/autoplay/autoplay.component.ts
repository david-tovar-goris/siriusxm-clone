import {
    Component,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy,
    NgZone
} from "@angular/core";
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { TranslateService } from "@ngx-translate/core";
import { NeriticLinkService } from "../neritic-links";
import { FocusUtil } from "../common/util/focus.util";
import { ITile, SettingsConstants, SettingsService, ConfigService, SxmAnalyticsService } from "sxmServices";
import * as $ from "jquery";
import "slick-carousel";
import {
    autoplayCarouselOptions,
    autoplayFullscreenCarouselOptions
} from "../carousel/content/slick-responsive-breakpoints";
import { CarouselService } from "../carousel/carousel.service";
import * as screenfull from 'screenfull';
import { ContextMenuService } from "../context-menu";
import { CMOption } from "../context-menu";
import { TileContextMenuOptionsService } from "app/context-menu/options/tile-context-menu-options.service";
import {
    AnalyticsCarouselNames,
    AnalyticsElementTypes,
    AnalyticsFindingMethods,
    AnalyticsInputTypes,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import { Router } from "@angular/router";
import { BrowserUtil } from "app/common/util/browser.util";
import { AutoUnSubscribe } from "app/common/decorator/auto-unsubscribe";

@Component({
    selector: "autoplay",
    templateUrl: "./autoplay.component.html",
    styleUrls: [ "./autoplay.component.scss" ],
    host: { '(window:keydown.escape)': 'close($event)' }
})

@AutoUnSubscribe()
export class AutoplayComponent implements AfterViewInit, OnDestroy, AfterViewChecked
{
    public _ref: any;

    public screenfull = screenfull;
    public isFullScreen: boolean = false;

    public countdownNumber: number = this.configService.getAutoplayNextEpisodeDelay();
    public isAutoplayEnabled: boolean = this.settingsService.isGlobalSettingOn(SettingsConstants.AUTO_PLAY_NEXT_EPISODE);

    /**
     * Tile that is played if Autoplay is enabled
     */
    public upNextTile: ITile;
    /**
     * Carousel of 4 Related Tiles
     */
    private _upNextCarousel: Array<ITile> = [];

    public set upNextCarousel(tiles : Array<ITile>)
    {
        this.upNextTile      = tiles[0]; //grab 1 for Up Next
        this._upNextCarousel = tiles.slice(1, 4); //only need 4 tiles

        if (this.upNextTile && this.isAutoplayEnabled)
        {
            this.autoplayUpNextEpisode();
        }
    }

    public get upNextCarousel() { return this._upNextCarousel; }

    $ = $;
    private carouselInitialized: boolean = false;
    private autoplayTimeout;
    private subscriptions: Subscription[] = [];

    /*Analytics Contants*/
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;
    public activeUrl: string = null;


    constructor(
        public tileContextMenuOptionsService: TileContextMenuOptionsService,
        public neriticLinkService: NeriticLinkService,
        public translate: TranslateService,
        public carouselService: CarouselService,
        public settingsService: SettingsService,
        private contextMenuService: ContextMenuService,
        public configService: ConfigService,
        public ngZone: NgZone,
        private analyticsService: SxmAnalyticsService,
        private router: Router)
    {
        this.activeUrl = this.router.url;
    }

    ngOnInit()
    {
        this.setAutoplayOpenAnaTags();
    }

    ngAfterViewInit(): void
    {
        FocusUtil.setupAccessibleDialog(document.getElementById('autoplay'), true);
        window.scrollTo(window.scrollX, window.scrollY - 1);
        window.scrollTo(window.scrollX, window.scrollY + 1);
        this.isFullScreen = screenfull.isFullscreen;
        screenfull.on("change", this.onFullScreenToggled);
        
        this.subscriptions.push(
            this.router.events.pipe(
                take(1)
            ).subscribe(() =>
            {
                this.close();
            })
        );
    }

    public onFullScreenToggled = () =>
    {
        // destroy and recreate upnext carousel with updated slickCarouselOptions when switching from fullScreen
        this.destroyCarousel();
        this.createCarousel();
        this.isFullScreen = screenfull.isFullscreen;
        setTimeout(() =>
        {
            //to remove unnecessary div appearing in carousel
            const extraDiv: HTMLElement = this._ref && this._ref.location && this._ref.location.nativeElement
                && this._ref.location.nativeElement.querySelector('div.slick-list.draggable.slick-slide.slick-current.slick-active');

            if(extraDiv)
            {
                extraDiv.parentNode.removeChild(extraDiv);
            }
        }, 100);
    }

    ngAfterViewChecked(): void
    {
        if (!this.carouselInitialized && this.upNextCarousel.length > 0)
        {
            this.createCarousel();
        }
    }

    ngOnDestroy(): void
    {
        if (this.carouselInitialized)
        {
            clearTimeout(this.autoplayTimeout);
            this.destroyCarousel();
            FocusUtil.closeFocusedDialog();
        }
        this.contextMenuService.closeContextMenu();
        screenfull.off("change", this.onFullScreenToggled);
    }

    /**
     * Used to create the carousel and assign to local carousel ref property
     */
    private createCarousel(): void
    {
        const slickCarouselOptions = screenfull.isFullscreen ? autoplayFullscreenCarouselOptions : autoplayCarouselOptions;
        this.carouselInitialized = true;

        const opts = Object.assign(slickCarouselOptions, {
            prevArrow: "#autoplay-carousel-arrow__prev",
            nextArrow: "#autoplay-carousel-arrow__next"
        });

        let element;
        this.ngZone.runOutsideAngular(() =>
        {
            element = (this.$("#autoplay-carousel") as any).slick(opts);
        });
        return element;
    }

    /**
     * destroy the carousel
     */
    private destroyCarousel()
    {
        if (this.carouselInitialized)
        {
            this.ngZone.runOutsideAngular(() =>
            {
                (this.$("#autoplay-carousel") as any).slick("unslick");
            });
            this.carouselInitialized = false;
        }
    }

    public close(event?): void
    {
        const escapeKeyEvent = event && event.keyCode === 27;
        //to fix IE issue - Upnext window closing while navigating back from full screen WEBEVEREST-3663
        if(BrowserUtil.isInternetExplorer() && this.isFullScreen && escapeKeyEvent)
        {
            return;
        }
        this.setAutoplayCloseAnaTags();
        this._ref.destroy();
    }

    public autoplayUpNextEpisode(): void
    {
        this.autoplayTimeout = setTimeout(() =>
        {
            this.neriticLinkService.takePrimaryTileAction( this.upNextTile );
            this.close();
        },this.countdownNumber * 1000 );
    }

    public getCMOptionsFunc(tile): () => CMOption[]
    {
        return function()
        {
            return this.tileContextMenuOptionsService.getCMOptions(tile);
        }.bind(this);
    }

    /* Analytics tags for autoplay modal open */
    private setAutoplayOpenAnaTags()
    {
        this.analyticsService.logAnalyticsTag({
            name: AnalyticsTagNameConstants.MD_ELEMENT,
            elementType: AnalyticsElementTypes.MODAL,
            userPath: AnalyticsUserPaths.MD_ELEMENT,
            action: AnalyticsTagActionConstants.OPEN,
            modal: AnalyticsTagNameConstants.MDL_UP_NEXT_MODAL_NAME,
            userPathScreenPosition: 2,
            screen: this.activeUrl,
            skipCurrentPosition: true,
            skipActionSource: true
        });
    }

    /* Analytics tags for autoplay modal close */
    private setAutoplayCloseAnaTags()
    {
        this.analyticsService.logAnalyticsTag({
            name: AnalyticsTagNameConstants.MD_ELEMENT,
            elementType: AnalyticsElementTypes.MODAL,
            userPath: AnalyticsUserPaths.MD_ELEMENT,
            action: AnalyticsTagActionConstants.CLOSE,
            modal: AnalyticsTagNameConstants.MDL_UP_NEXT_MODAL_NAME,
            userPathScreenPosition: 2,
            screen: this.activeUrl,
            skipCurrentPosition: true,
            skipActionSource: true
        });
    }
}
