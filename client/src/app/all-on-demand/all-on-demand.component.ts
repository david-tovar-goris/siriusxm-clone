import { Component } from "@angular/core";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { CarouselTypeConst, ICarouselData, ITile } from "sxmServices";
import { displayViewType } from "../channel-list/component/display.view.type";
import { appRouteConstants } from "../app.route.constants";
import { getSegmentCarousel } from "../../../../servicelib/src/carousel/carousel.selector.util";

/**
 * @MODULE:     client
 * @CREATED:    03/16/18
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *    AllOnDemandComponent used to load on All on demand episodes
 */

@Component({
    templateUrl: "./all-on-demand.component.html",
    styleUrls  : [ "./all-on-demand.component.scss" ]
})

export class AllOnDemandComponent
{
    /**
     * Holds the episode data
     */
    public episodes: ICarouselData = {
        tiles: []
    };

    /**
     * Indicated  the flag which have all data is needed
     * @type {boolean}
     */
    public dataLoading = false;

    /**
     * Constructor -
     * @param {CarouselStoreService} carouselStoreService
     */
    constructor(public carouselStoreService: CarouselStoreService)
    {
    }

    /**
     * ngOnInit
     */
    ngOnInit()
    {
        this.carouselStoreService.allOnDemandCarousels
            .subscribe((data) =>
            {
                if (!data || !data.zone || !data.zone.length || !data.zone[0].content)
                {
                    return;
                }
                this.episodes    = data && data.zone[0].content ? data.zone[0].content[ 0 ] : this.episodes;
                this.dataLoading = true;
            });
    }

    /**
     * Used to return the Episode type which supports show list item
     * @param {string} tileSubContentType
     * @returns {string}
     */
    getEpisodeType(tileSubContentType: string): string
    {
        return tileSubContentType === CarouselTypeConst.AOD_EPISODE_TILE ? appRouteConstants.ON_DEMAND.AUDIO : appRouteConstants.ON_DEMAND.VIDEO;
    }
}
