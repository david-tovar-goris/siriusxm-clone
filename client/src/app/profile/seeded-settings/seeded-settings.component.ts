import { map, filter } from 'rxjs/operators';
import { Component, NgZone, OnInit } from "@angular/core";
import {
    Logger,
    ITile,
    ICarouselDataByType,
    ICarouselData
} from "sxmServices";
import { TranslateService } from "@ngx-translate/core";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { SubscriptionLike as ISubscription ,  BehaviorSubject ,  Observable } from "rxjs";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { noResultsPagesConst } from "../../no-results/no-results.const";
import { SeededStationsClientService } from "../../common/service/seeded-stations/seeded-stations.client.service";
import { IModalData } from "../../common/service/modal/modal.interface";
import { ModalService } from "../../common/service/modal/modal.service";
import { Store } from "@ngrx/store";
import { IAppStore } from "../../common/store";
import { selectSeededStationsCarousel } from "../../common/store/carousel.store";
import * as _ from "lodash";
import {
    AnalyticsCarouselNames,
    AnalyticsElementTypes,
    AnalyticsFindingMethods,
    AnalyticsInputTypes,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "seeded-stations",
    templateUrl: "./seeded-settings.component.html",
    styleUrls: ["./seeded-settings.component.scss"]
})

@AutoUnSubscribe()
export class SeededSettingsComponent implements OnInit
{
    /**
     * Analytics constants
     */
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("SeededSettingsComponent");

    private static MAX_TILES_TO_RENDER_AT_ONCE = 10;
    private static MS_BETWEEN_RENDERS = 50;

    /**
     * Stores settings subscription to un subscribe when component is destroyed
     * @type {Subscription}
     * @memberof SeededSettingsComponent
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * noResultsPageName page/screen Name to render the NoResultsComponent
     */
    public noResultsPageName: string = noResultsPagesConst.MANAGE_SEEDED_STATIONS;

    /**
     * Backing store and getter for the tiles that will be passed to the seeded channel list items children for the view
     */
    private _tiles$ = new BehaviorSubject<ITile[]>([]);
    public get tiles$ (){ return this._tiles$ as Observable<ITile[]>; }

    /**
     * Backing store and getter for the page header.
     */
    public _pageHeader$ = new BehaviorSubject<string>(null);
    public get pageHeader$ () { return this._pageHeader$ as Observable<string>; }

    private renderMoreTilesTimer: any;

    /**
     * seeded stations.
     */
    public seededStations$: Observable<ICarouselDataByType> = this.store.select(selectSeededStationsCarousel);

    /**
     *
     * @param {CarouselStoreService} carouselStoreService
     * @param {TranslateService} translate
     */
    constructor (private carouselStoreService: CarouselStoreService,
                 private seededStationsClientService: SeededStationsClientService,
                 private modalService: ModalService,
                 public translate: TranslateService,
                 private ngzone: NgZone,
                 private store: Store<IAppStore>)
    {
        SeededSettingsComponent.logger.debug("Constructor");
    }

    /**
     * We want the initialization code here to run outside the view zonejs change detection.  The reason for this
     * is because we are going to handle change detection through the tiles$ and filterText$ observables, so we do
     * not need zonejs triggered Angular change detection uneccesarily as we get the all stations carousel pulled
     * from the servicelib.
     */
    ngOnInit ()
    { this.ngzone.runOutsideAngular(this.onInit.bind(this)); }

    /**
     * Function to be used to track tiles with the ngFor trackBy functionality.  Keeps angular from having to
     * completely rewrite the DOM when tiles are added or deleted or changed.
     * @param index of the tile (ignored here)
     * @param tile that we want to track
     */
    public trackBy (index: number, tile: ITile)
    { return tile.tileAssetInfo.channelGuid; }


    /**
     * Opens the Modal prompting the user to confirm removing all the seeded stations.
     */
    public openRemoveAllSeededStationsModal(): void
    {
        let modalText = this.translate.instant("profile.seededSettings.removeAllStationsModal");
        modalText.buttonOne.action = this.removeAllSeededStations.bind(this);
        this.openModal(modalText);
    }

    /**
     * Removes all seeded stations
     */
    private removeAllSeededStations()
    {
        this.seededStationsClientService.removeAllSeededStations(this._tiles$.getValue());
    }

    /**
     * Display modal for "Remove All"
     * @param {IModalData} modalData
     */
    private openModal(modalData: IModalData): void
    {
        this.modalService.addDynamicModal(modalData);
    }

    /**
     * Observe and debounce the seeded stations carousel observable, and then combine
     * them to feed data to the seeded channel list item child components to manage the change detection for those components
     */
    private onInit ()
    {
        const subscription =   this.seededStations$.pipe(
                                 filter(carousel => !!carousel),
                                 map(SeededSettingsComponent.getTilesCarousel),
                                 filter(carousel => !this.areTilesSame(carousel)))
                                 .subscribe((carousel: ICarouselData) => { this.setTiles(carousel); });

        this.subscriptions.push(subscription);
    }

    /**
     * checks if any we got any new tile (while comparing with rendered tiles) using the channelGuid property
     * while enforcing order as change
     *
     * @param {carousel} carousel data
     */
    private areTilesSame(carousel: ICarouselData): boolean
    {
        const newTiles : ITile[] = _.get(carousel,"tiles",  []);
        const tilesRendered: ITile[] = this._tiles$.getValue();

        return newTiles.length === tilesRendered.length && newTiles.every( (newTile, index) =>
        {
            return tilesRendered[index].tileAssetInfo.channelGuid === newTile.tileAssetInfo.channelGuid;
        });
    }


    /**
     * Trigger the tile$ observable with new data
     *
     * This function will break up the tile array into smaller groups and feed them to the template.  This, along
     * with the usage of the track by capability of Angular allows us to break up the UI rendering into smaller
     * pieces and avoid locking up the browser trying to render the *entire* UI all at once.
     *
     * @param {carousel} carousel data
     */
    private setTiles (carousel : ICarouselData)
    {
        const tiles : ITile[] = carousel && carousel.tiles ? carousel.tiles : [];
        const title: string = carousel && carousel.title && carousel.title.textValue ? carousel.title.textValue : "";

        this._tiles$.next([]);
        this._pageHeader$.next(tiles.length > 0 ? title : "");

        if (this.renderMoreTilesTimer)
        {
            clearTimeout(this.renderMoreTilesTimer);
        }

        renderMoreTiles.bind(this)(0);

        function renderMoreTiles (startIndex)
        {
            const newTiles = tiles.slice(startIndex,
                startIndex + SeededSettingsComponent.MAX_TILES_TO_RENDER_AT_ONCE);
            let tilesToRender: ITile[] = this._tiles$.getValue() || [];
            startIndex += newTiles.length;

            this._tiles$.next(tilesToRender.concat(newTiles));

            if (startIndex < tiles.length)
            {
                this.renderMoreTilesTimer = setTimeout(
                    () => { renderMoreTiles.bind(this)(startIndex); }, SeededSettingsComponent.MS_BETWEEN_RENDERS);
            }
        }
    }

    /**
     * Go to the all seesed stations carousel and get the carousel data.  This will be included all the seeded channel tiles
     * @param {ICarouselDataByType} carousel is the carousel object with the all channels carousel
     * @returns {ICarouselData} the carousel data for seeded stations
     */
    private static getTilesCarousel (carousel: ICarouselDataByType): ICarouselData
    {
        return _.get(carousel, "zone[0].content[0]", {} as ICarouselData);
    }
}
