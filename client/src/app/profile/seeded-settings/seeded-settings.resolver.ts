import { switchMap, filter, share, skipWhile, first } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import {
    ICarouselDataByType,
    InitializationService,
    InitializationStatusCodes
} from "sxmServices";
import { CarouselStoreService } from "../../common/service/carousel.store.service";

@Injectable()
export class SeededSettingsResolver implements Resolve<{}>
{
    constructor(private initService: InitializationService,
                private carouselStoreService: CarouselStoreService)
    {}

    resolve()
    {
        return this.initService.initState.pipe(
                   skipWhile((data) => data !== InitializationStatusCodes.RUNNING),
                   switchMap(() => resolveSeededSettings(this.carouselStoreService)),
                   first());
    }
}

function resolveSeededSettings(carouselStoreService:CarouselStoreService): Observable<ICarouselDataByType>
{
    return carouselStoreService.selectSeededStations().pipe(filter(data => !!data),share());
}
