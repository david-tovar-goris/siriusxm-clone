import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
    Logger,
    ISetting,
    ISettings,
    SettingsConstants,
    SettingsService
} from "sxmServices";
import { TranslateService } from "@ngx-translate/core";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { SubscriptionLike as ISubscription } from "rxjs";

@Component({
    selector: "messaging-settings",
    templateUrl: "./messaging-settings.component.html",
    styleUrls: [ "./messaging-settings.component.scss" ]
})

@AutoUnSubscribe()
export class MessagingSettingsComponent implements OnInit, OnDestroy
{
    /**
     * Boolean flag to check weather all settings are loaded from the Service
     */
    public isLoading: boolean = true;

    /**s
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("MessagingSettingsComponent");

    /**
     * globalSettings from settingsService.settings Observable.
     * And used to extract setting values from this list.
     */
    private globalSettings: Array<ISetting> = [];

    /**
     * deviceSettings from settingsService.settings Observable.
     * And used to extract setting values from this list.
     */
    private deviceSettings: Array<ISetting> = [];

    /**
     * Used to check whether offers messaging is On/Off
     */
    public offersMessaging: boolean = true;

    /**
     * Used to check whether content messaging is On/Off
     */
    public contentMessaging: boolean = true;

    /**
     * Used to check whether updates messaging is On/Off
     */
    public updatesMessaging: boolean = true;

    /**
     * Stores settings subscription to un subscribe when component is destroyed
     */
    private subscriptions: Array<ISubscription> = [];

    constructor(public translateService: TranslateService,
                private settingsService: SettingsService,
                private router: Router)
    {
        MessagingSettingsComponent.logger.debug("Show Reminder Component Constructor");
        this.loadMessagingSettings();
    }

    ngOnInit()
    {
        MessagingSettingsComponent.logger.debug("ngOnInit()");
    }

    ngOnDestroy()
    {
        MessagingSettingsComponent.logger.debug("ngOnDestroy()");
    }

    /**
     * Load reminder settings on creation of component by subscribing to ....
     */
    private loadMessagingSettings()
    {
        let settingsSubscription = this.settingsService.settings.subscribe((response: ISettings) =>
        {
            if (response)
            {
                this.deviceSettings = response.deviceSettings;
                this.globalSettings = response.globalSettings;
                this.offersMessaging = this.settingsService.isGeneralSettingOn(
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_OFFERS,
                    true
                );
                this.contentMessaging = this.settingsService.isGeneralSettingOn(
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_CONTENT,
                    true
                );
                this.updatesMessaging = this.settingsService.isGeneralSettingOn(
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_UPDATES,
                    true
                );
                this.isLoading = false;
            }
        });
        this.subscriptions.push(settingsSubscription);
    }

    /**
     * Used to update the settings.
     * Prepare payload with globalSettings and deviceSettings then trigger update service.
     */
    private updateSettings()
    {
        let settingsPayload: ISettings = {
            deviceSettings: this.deviceSettings,
            globalSettings: this.globalSettings
        };
        let updateSubscription = this.settingsService.updateSettings(settingsPayload).subscribe(response =>
        {
            if (response)
            {
                MessagingSettingsComponent.logger.debug("Successfully Updated");
            }
            else
            {
                MessagingSettingsComponent.logger.warn("Failed to update settings");
            }
        });

        this.subscriptions.push(updateSubscription);
    }

    /**
     * used to update Messaging Settings i.e. NOTIFICATION_SUBSCRIPTION_CONTENT |
     *                                        NOTIFICATION_SUBSCRIPTION_UPDATES |
     *                                        NOTIFICATION_SUBSCRIPTION_OFFERS
     */
    private updateMessaging(settingValue, settingType) : void
    {
        let setting: ISetting = this.globalSettings.find(setting => setting.name === settingType);
        let value: string = settingValue ? SettingsConstants.ON : SettingsConstants.OFF;

        if (setting)
        {
            setting.value = value;
        }
        else
        {
            setting = { name: settingType, value: value } as ISetting;
            this.globalSettings.push(setting);
        }
        this.updateSettings();
    }

    /**
     * used to update Offers Messaging
     */
    public updateOffersMessaging() : void
    {
        this.updateMessaging(!this.offersMessaging, SettingsConstants.NOTIFICATION_SUBSCRIPTION_OFFERS);
    }

    /**
     * used to update Content Messaging
     */
    public updateContentMessaging(): void
    {
        this.updateMessaging(!this.contentMessaging, SettingsConstants.NOTIFICATION_SUBSCRIPTION_CONTENT);
    }

    /**
     * used to update Updates Messaging
     */
    public updateUpdatesMessaging(): void
    {
        this.updateMessaging(!this.updatesMessaging, SettingsConstants.NOTIFICATION_SUBSCRIPTION_UPDATES);
    }

}
