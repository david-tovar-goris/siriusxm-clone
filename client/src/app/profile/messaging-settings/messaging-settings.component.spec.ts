import { MessagingSettingsComponent } from './messaging-settings.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, NO_ERRORS_SCHEMA } from "@angular/core";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../test/mocks";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { SettingsService } from "sxmServices";
import { SettingsServiceMock } from "../../../../test/mocks/settings.service.mock";
import { By } from "@angular/platform-browser";
import { of as observableOf } from "rxjs";

describe('MessagingSettingsComponent', () =>
{
    let component: MessagingSettingsComponent;
    let fixture: ComponentFixture<MessagingSettingsComponent>;
    let debugElement: DebugElement;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            declarations: [ MessagingSettingsComponent ],
            imports: [
                TranslateModule
            ],
            providers: [
                { provide: SettingsService, useValue: SettingsServiceMock.getSpy() },
                { provide: Router, useClass: RouterStub },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() }
            ],
            schemas: [ NO_ERRORS_SCHEMA ]
        }).compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(MessagingSettingsComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        fixture.detectChanges();
    });

    it('should create component', () =>
    {
        expect(component).toBeDefined();
    });

    describe('Messaging Settings component Execution', () =>
    {
        it('Subscription to SettingsService', () =>
        {
            SettingsServiceMock.getSpy().settings.subscribe(response =>
            {
                expect(response.globalSettings).toBeDefined();
                expect(response.deviceSettings).toBeDefined();
            });
        });

        it('verify 3 radio buttons and Update the Messaging settings', () =>
        {
            SettingsServiceMock.getSpy().updateSettings.and.returnValue(observableOf(true));
            let radioElements: DebugElement[] = debugElement.queryAll(By.css('.switch'));
            expect(radioElements.length).toBe(3);
            radioElements[1].triggerEventHandler('click', null);
            fixture.detectChanges();
            expect(SettingsServiceMock.getSpy().updateSettings).toHaveBeenCalled();
        });

        it("can update offers messaging", () =>
        {
            SettingsServiceMock.getSpy().updateSettings.and.returnValue(observableOf(true));
            component.updateOffersMessaging();
            expect(SettingsServiceMock.getSpy().updateSettings).toHaveBeenCalled();
        });

        it("can update content messaging", () =>
        {
            SettingsServiceMock.getSpy().updateSettings.and.returnValue(observableOf(true));
            component.updateContentMessaging();
            expect(SettingsServiceMock.getSpy().updateSettings).toHaveBeenCalled();
        });

        it("can update updates messaging", () =>
        {
            SettingsServiceMock.getSpy().updateSettings.and.returnValue(observableOf(true));
            component.updateUpdatesMessaging();
            expect(SettingsServiceMock.getSpy().updateSettings).toHaveBeenCalled();
        });

    });
});
