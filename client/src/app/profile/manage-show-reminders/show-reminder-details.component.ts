
import {
    forkJoin as observableForkJoin,
    of as observableOf,
    combineLatest as observableCombineLatest,
    Observable,
    SubscriptionLike as ISubscription
} from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CarouselStoreService } from "app/common/service/carousel.store.service";
import { ITile, ICarouselDataByType, AlertType } from "sxmServices";
import { AutoUnSubscribe } from "app/common/decorator";
import { NavigationService } from "app/common/service/navigation.service";
import { appRouteConstants } from "app/app.route.constants";
import { AlertClientService } from "../../common/service/alert/alert.client.service";

@Component({
    selector: "show-reminder-details",
    templateUrl: "./show-reminder-details.component.html",
    styleUrls: [ "./show-reminder-details.component.scss" ]
})
@AutoUnSubscribe()
export class ShowReminderDetailsComponent
{

    private tile: ITile = null;
    private subscriptions: Array<ISubscription> = [];
    public AlertType = AlertType;

    constructor(route: ActivatedRoute,
                private carouselStoreService: CarouselStoreService,
                private alertClientService: AlertClientService,
                private navigationService: NavigationService)
    {
        this.subscriptions.push(
            observableCombineLatest( route.params.pipe(map(({showGuid}) => showGuid)),
                       carouselStoreService.showReminderCarousels.pipe(filter( data => !!data)),
                       (showGuid: string, reminderCarousel: ICarouselDataByType): ITile =>
                    {
                        if (reminderCarousel.zone.length && reminderCarousel.zone[0].content.length > 0)
                        {
                            return reminderCarousel.zone[0].content[0].tiles.find( (tile: ITile) => tile.tileAssetInfo.showGuid === showGuid);
                        }
                    }).subscribe( tile => this.tile = tile)
        );
    }

    ngOnInit()
    {
        this.carouselStoreService.selectShowReminders();
    }

    public updateReminder(alertType: AlertType)
    {
        switch(alertType)
        {
            case AlertType.SHOW:
                this.addOrRemoveAlert(this.tile.reminders.showReminderSet, alertType);
                this.tile.reminders.showReminderSet = !this.tile.reminders.showReminderSet;
                break;
            case AlertType.LIVE_VIDEO_START:
                this.addOrRemoveAlert(this.tile.reminders.liveVideoReminderSet, alertType);
                this.tile.reminders.liveVideoReminderSet = !this.tile.reminders.liveVideoReminderSet;
                break;
        }
    }

    public removeAllRemindersForShow(): void
    {
        let waitForShow: Observable<any> = observableOf(true).pipe(take(1));
        let waitForliveVideo: Observable<any> = observableOf(true).pipe(take(1));

        if(this.tile.reminders.showReminderSet)
        {
            waitForShow = this.removeAlert(AlertType.SHOW).pipe(take(1));
        }
        if(this.tile.reminders.liveVideoReminderSet)
        {
            waitForliveVideo = this.removeAlert(AlertType.LIVE_VIDEO_START).pipe(take(1));
        }

        observableForkJoin(waitForShow, waitForliveVideo).subscribe(() =>
        {
            this.navigationService.go([appRouteConstants.MANAGE_SHOW_REMINDERS]);
        });
    }

    private addOrRemoveAlert(remove: boolean, alertType: AlertType)
    {
        remove ? this.removeAlert(alertType) : this.addAlert(alertType);
    }

    private addAlert(alertType: AlertType)
    {
        this.alertClientService.createAlert(this.tile.tileAssetInfo.channelId, this.tile.tileAssetInfo.showGuid, alertType);
    }
    private removeAlert(alertType: AlertType): any
    {
        return this.alertClientService.removeAlert(null, this.tile.tileAssetInfo.showGuid, alertType);
    }
}
