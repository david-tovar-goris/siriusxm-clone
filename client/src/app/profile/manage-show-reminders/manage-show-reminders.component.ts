import { filter } from 'rxjs/operators';
import { Component, OnInit } from "@angular/core";
import {
    Logger,
    ICarouselData
} from "sxmServices";
import { TranslateService } from "@ngx-translate/core";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { SubscriptionLike as ISubscription } from "rxjs";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { AlertClientService } from "../../common/service/alert/alert.client.service";
import { IModalData } from "../../common/service/modal/modal.interface";
import { ModalService } from "../../common/service/modal/modal.service";
import { noResultsPagesConst } from "../../no-results/no-results.const";
import * as _ from "lodash";

@Component({
    selector: "manage-show-reminders",
    templateUrl: "./manage-show-reminders.component.html",
    styleUrls: [ "./manage-show-reminders.component.scss" ]
})

@AutoUnSubscribe()
export class ManageShowRemindersComponent implements OnInit
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ManageShowRemindersComponent");

    showRemindersCarousel : ICarouselData = { tiles: [] };
    /**
     * Stores settings subscription to un subscribe when component is destroyed
     * @type {Subscription}
     * @memberof ManageShowRemindersComponent
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * noResultsPageName page/screen Name to render the NoResultsComponent
     */
    public noResultsPageName: string = noResultsPagesConst.MANAGE_SHOW_REMINDERS;

    /**
     *
     * @param {CarouselStoreService} carouselStoreService
     * @param {TranslateService} translate
     */
    constructor( private carouselStoreService: CarouselStoreService,
                 public translate: TranslateService,
                 private alertClientService: AlertClientService,
                 private modalService: ModalService)
    {
        this.subscriptions.push(this.carouselStoreService.showReminderCarousels.pipe(
        filter(carouselData => !!carouselData))
        .subscribe((carouselData) =>
        {
            this.showRemindersCarousel = _.get(carouselData, "zone[0].content[0]", { tiles: [] });
        }));

        ManageShowRemindersComponent.logger.debug("Manage Show Reminders Component Constructor");
    }

    ngOnInit()
    {
        this.carouselStoreService.selectShowReminders();
    }

    removeAllShowReminders(): void
    {
        this.alertClientService.removeAllAlerts();
    }

    /**
     * Opens the Modal prompting the user to confirm removing all the show reminders.
     */
    public openRemoveAllRemindersModal(): void
    {
        let modalText = this.translate.instant("manageShowReminders.removeAllRemindersModal");
        modalText.buttonOne.action = this.removeAllShowReminders.bind(this);
        this.openModal(modalText);
    }

    /**
     * Display modal for "Remove All"
     * @param {IModalData} modalData
     */
    private openModal(modalData: IModalData): void
    {
        this.modalService.addDynamicModal(modalData);
    }
}
