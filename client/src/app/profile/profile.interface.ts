import { StringSetting } from "sxmServices";

export interface ProfileConfig
{
    FAQURL: StringSetting;
    FeedbackUrl: StringSetting;
    CustomerAgreement: StringSetting;
    PrivacyPolicy: StringSetting;
    OAC: StringSetting;
}

export interface AudioQualityOptions
{
    label:string;
    value:string;
}
