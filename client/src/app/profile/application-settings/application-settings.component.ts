import { Component, Inject } from "@angular/core";
import { SubscriptionLike as ISubscription } from "rxjs";
import {
    ISetting,
    ISettings,
    Logger,
    SettingsConstants,
    SettingsService,
    IAppConfig
} from "sxmServices";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { streamingQualityOptions } from "../profile.const";
import { AudioQualityOptions } from "../profile.interface";
// import { ProfileAnalyticsService } from "../analytics/profile-analytics.service";
// import { applicationSettingsTags } from "../analytics/profile-tags";
import { APP_CONFIG } from "../../sxmservicelayer/sxm.service.layer.module";
import { TranslateService } from "@ngx-translate/core";
import { IModalData } from "../../common/service/modal/modal.interface";
import { ModalService } from "../../common/service/modal/modal.service";

@Component({
    selector: "application-settings",
    templateUrl: "./application-settings.component.html",
    styleUrls: [ "./application-settings.component.scss" ]
})

@AutoUnSubscribe()
export class ApplicationSettingsComponent
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ApplicationSettingsComponent");

    /**
     * streamingQualityOptions
     */
    public streamingQualityOptions: Array<AudioQualityOptions> = streamingQualityOptions;

    /**
     * Boolean flag to check weather all settings are loaded from the Service
     */
    public isLoading: boolean = true;

    /**
     * globalSettings from settingsService.settings Observable.
     * And used to extract setting values from this list.
     */
    private globalSettings: Array<ISetting> = [];

    /**
     * deviceSettings from settingsService.settings Observable.
     * And used to extract setting values from this list.
     */
    private deviceSettings: Array<ISetting> = [];

    /**
     * Used to store the Audio Quality
     */
    public audioQuality: string;

    /**
     * Used to check whether Mini Play is On/Off
     */
    public enableMiniPlay: boolean = false;

    /**
     * Used to check whether Tunestart is On/Off
     */
    public enableTuneStart: boolean = false;

    /**
     * Used to check whether Autoplay is On/Off; Default is ON.
     */
    public enableAutoplay: boolean = true;

    /**
     * Stores settings subscription to un subscribe when component is destroyed
     */
    private subscriptions: Array<ISubscription> = [];

    private maximumAudioQualityModal : IModalData = {
        ...this.translateService.instant("profile.maximumAudioQualityModalData")
    };

    /**
     * Flag to display TeamCityBuildNumber or Git Hash number
     */
    public displayTeamCityBuildNumber: boolean = true;

    constructor(@Inject(APP_CONFIG)
                public appConfig: IAppConfig,
                private translateService: TranslateService,
                private settingsService: SettingsService)
    {
        ApplicationSettingsComponent.logger.debug("Application Settings Component Constructor");
        this.loadApplicationSettings();
    }

    /**
     * Load application settings on creation of component by subscribing to settingsService
     */
    private loadApplicationSettings(): void
    {
        let settingsSubscription = this.settingsService.settings.subscribe((response: ISettings) =>
        {
            if (response)
            {
                this.deviceSettings = response.deviceSettings;
                this.globalSettings = response.globalSettings;

                const audioQuality = this.settingsService.getAudioQuality();
                const normalQuality = this.streamingQualityOptions
                                           .find((quality => quality.label === SettingsConstants.AUDIO_QUALITY_NORMAL));
                this.audioQuality = audioQuality ? audioQuality : normalQuality.value ;
                if(!audioQuality)
                {
                    this.updateQuality(normalQuality.value);
                }
                this.enableTuneStart = this.settingsService.isGlobalSettingOn(SettingsConstants.TUNE_START);
                this.enableMiniPlay = this.settingsService.isDeviceSettingOn(SettingsConstants.MINI_PLAY);
                this.enableAutoplay = this.settingsService.isGlobalSettingOn(SettingsConstants.AUTO_PLAY_NEXT_EPISODE);

                this.isLoading = false;
            }
        });
        this.subscriptions.push(settingsSubscription);
    }

    /**
     * Used to update the Audio Quality.
     * @param value: Type of audio quality Needs to be updated in the application settings
     */
    private updateQuality(value: string): void
    {
        this.deviceSettings.find(setting => setting.name == SettingsConstants.AUDIO_QUALITY).value = value;
        this.updateSettings();

    }

    /**
     * Used to update the Tune Start.
     */
    private updateTuneStart(): void
    {
        const tuneStart: ISetting = this.globalSettings.find(setting => setting.name === SettingsConstants.TUNE_START);

        if (tuneStart)
        {
            tuneStart.value = !this.enableTuneStart ? SettingsConstants.ON : SettingsConstants.OFF;
            this.updateSettings();
        }
    }

    /**
     * Used to update the Mini Play.
     */
    private updateMiniPlay(): void
    {
        let miniPlay : ISetting = this.deviceSettings.find(setting => setting.name === SettingsConstants.MINI_PLAY);
        const value: string = !this.enableMiniPlay ? SettingsConstants.ON : SettingsConstants.OFF;

        if (miniPlay)
        {
            miniPlay.value = value;
        }
        else
        {
            miniPlay = { name: SettingsConstants.MINI_PLAY, value: value } as ISetting;
            this.deviceSettings.push(miniPlay);
        }
        this.updateSettings();
    }

    /**
     * Used to update the settings.
     * Prepare payload with globalSettings and deviceSettings then trigger update service.
     */
    private updateSettings(): void
    {
        const settingsPayload: ISettings = {
            deviceSettings: this.deviceSettings,
            globalSettings: this.globalSettings
        };
        const updateSubscription = this.settingsService.updateSettings(settingsPayload).subscribe(response =>
        {
            response ?
                ApplicationSettingsComponent.logger.debug("Successfully Updated") :
                ApplicationSettingsComponent.logger.warn("Failed to update settings");
        });

        this.subscriptions.push(updateSubscription);
    }

    private updateAutoplay(): void
    {
        const autoPlay: ISetting = this.globalSettings.find(setting => setting.name === SettingsConstants.AUTO_PLAY_NEXT_EPISODE);

        if (autoPlay)
        {
            autoPlay.value = !this.enableAutoplay ? SettingsConstants.ON : SettingsConstants.OFF;
            this.updateSettings();
        }
    }

    /**
     * toggleBuildNumber method used to toggle between Git Hash number and TeamCity build number
     */
    private toggleBuildNumber(): void
    {
        ApplicationSettingsComponent.logger.debug(`toggleBuildNumber()`);
        this.displayTeamCityBuildNumber = !this.displayTeamCityBuildNumber;
    }
}
