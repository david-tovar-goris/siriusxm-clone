import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ApplicationSettingsComponent } from './application-settings.component';
import { DebugElement, NO_ERRORS_SCHEMA } from "@angular/core";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { SettingsService } from "sxmServices";
import { SettingsServiceMock } from "../../../../test/mocks/settings.service.mock";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { By } from "@angular/platform-browser";
import { of as observableOf } from "rxjs";

describe('ApplicationSettingsComponent', () =>
{
    let component: ApplicationSettingsComponent;
    let fixture: ComponentFixture<ApplicationSettingsComponent>;
    let debugElement: DebugElement;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                TranslateModule
            ],
            declarations: [ ApplicationSettingsComponent ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: SettingsService, useValue: SettingsServiceMock.getSpy() },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(ApplicationSettingsComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        fixture.detectChanges();
    });

    describe('Application Settings component Execution', () =>
    {
        it('Subscription to SettingsService', () =>
        {
            SettingsServiceMock.getSpy().settings.subscribe(response =>
            {
                expect(response.globalSettings).toBeDefined();
                expect(response.deviceSettings).toBeDefined();
            });

        });


        it('Update the Audio Quality', () =>
        {
            SettingsServiceMock.getSpy().updateSettings.and.returnValue(observableOf(true));
            component.isLoading = false;
            fixture.detectChanges();
            let radioElements: DebugElement[] = debugElement.queryAll(By.css('.radio-settings-list__item input[type=radio]'));
            expect(radioElements.length).toBe(3);
            radioElements[ 1 ].triggerEventHandler('change', null);
            fixture.detectChanges();
            expect(SettingsServiceMock.getSpy().updateSettings).toHaveBeenCalled();
        });

        it('Update the Tune start', () =>
        {
            SettingsServiceMock.getSpy().updateSettings.and.returnValue(observableOf(true));
            component.isLoading = false;
            fixture.detectChanges();
            let buttons: DebugElement[] = debugElement.queryAll(By.css('.switch'));
            expect(buttons.length).toBe(3);
            buttons[ 0 ].triggerEventHandler('click', null);
            expect(SettingsServiceMock.getSpy().updateSettings).toHaveBeenCalled();
        });

        it('Update the Mini Play', () =>
        {
            SettingsServiceMock.getSpy().updateSettings.and.returnValue(observableOf(true));
            component.isLoading = false;
            fixture.detectChanges();
            let buttons: DebugElement[] = debugElement.queryAll(By.css('.switch'));
            buttons[ 1 ].triggerEventHandler('click', null);
            fixture.detectChanges();
            expect(SettingsServiceMock.getSpy().updateSettings).toHaveBeenCalled();
        });

        it('Failed to Update settings', () =>
        {
            SettingsServiceMock.getSpy().updateSettings.and.returnValue(observableOf(true));
            component.isLoading = false;
            fixture.detectChanges();
            let buttons: DebugElement[] = debugElement.queryAll(By.css('.switch'));
            buttons[ 1 ].triggerEventHandler('click', null);
            fixture.detectChanges();
            expect(SettingsServiceMock.getSpy().updateSettings).toHaveBeenCalled();
        });

        it('Toggles build number with Git hash on click of build/git number', () =>
        {
            component.isLoading = false;
            console.log(component.displayTeamCityBuildNumber);
            fixture.detectChanges();
            let button = debugElement.query(By.css('.application-settings__build-number'));
            button.triggerEventHandler('click', null);
            fixture.detectChanges();
            expect(component.displayTeamCityBuildNumber).toBe(false);
        });
    });
});
