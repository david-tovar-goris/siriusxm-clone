import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfileCarouselComponent } from './profile-carousel.component';
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { CarouselService } from "sxmServices";
import { CarouselServiceMock } from "../../../../test/mocks/carousel.service.mock";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { CarouselStoreServiceMock } from "../../../../test/mocks/carousel.store.service.mock";

describe('ProfileCarouselComponent', () =>
{
    let component: ProfileCarouselComponent;
    let fixture: ComponentFixture<ProfileCarouselComponent>;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            declarations: [ ProfileCarouselComponent ],
            schemas: [ NO_ERRORS_SCHEMA ],
            imports: [
                TranslateModule
            ],
            providers: [
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: CarouselStoreService, useClass: CarouselStoreServiceMock },
                { provide: CarouselService, useValue: CarouselServiceMock.getSpy() }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(ProfileCarouselComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

    });

    it('should create', () =>
    {
        expect(component).toBeTruthy();
    });
});
