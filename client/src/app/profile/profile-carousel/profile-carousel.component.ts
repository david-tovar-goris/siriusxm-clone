import { Component, OnInit } from '@angular/core';
import { CarouselService, ICarouselData, Logger, ICarouselDataByType, CarouselTypeConst } from "sxmServices";
import { profileCarouselTypes } from "../profile.const";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { CarouselStoreService } from "../../common/service/carousel.store.service";

/**
 * @MODULE:     client
 * @CREATED:    08/02/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     ProfileCarouselComponent used to load Carousels on Profile screen
 */
@Component({
    selector: 'profile-carousel',
    templateUrl: './profile-carousel.component.html',
    styleUrls: [ './profile-carousel.component.scss' ]
})
@AutoUnSubscribe()
export class ProfileCarouselComponent
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ProfileCarouselComponent");

    /**
     * Stores subscription to un subscribe when component is destroyed
     * @type Subscription
     */
    private subscription: any;

    /**
     * used to store the carousels for type saveForLater.
     */
    private saveForLaterCarousels: ICarouselData;

    /**
     * used to store the carousels for type recentlyPlayed.
     */
    private recentlyPlayedCarousels: ICarouselData;

    /**
     * Boolean flag to check if API response is completed
     */
    public isLoading: boolean = true;

    /**
     * Constructor
     * @param {CarouselStoreService} carouselStoreService
     */
    constructor(private carouselStoreService: CarouselStoreService)
    {
        ProfileCarouselComponent.logger.debug("Profile-CarouselComponent Constructor");

        this.subscription = carouselStoreService.selectProfile()
                            .subscribe((carouselData: ICarouselDataByType) =>
                            {
                                if (!carouselData) { return; }

                                this.isLoading               = false;
                                carouselData.zone.forEach(zone=>
                                {
                                    this.saveForLaterCarousels   = zone.content.find(
                                        carousel => carousel.title.textValue === profileCarouselTypes.SAVE_FOR_LATER);
                                    this.recentlyPlayedCarousels = zone.content.find(
                                        carousel => carousel.title.textValue.toLowerCase() === profileCarouselTypes.RECENTLY_PLAYED);
                                });
                            });
    }
}
