import { Location } from '@angular/common';

import {
    ApplicationRef,
    Component, EventEmitter, HostListener,
    Inject, Input,
    OnDestroy,
    OnInit, Output,
    AfterViewInit,
    ViewChild,
    ElementRef,
    ViewContainerRef
} from "@angular/core";
import {
    IAppConfig,
    IComponentConfig,
    AuthenticationService,
    Logger,
    ConfigService,
    ISession,
    IAppByPassState,
    LogService,
    BypassMonitorService,
    KeyboardEventKeyTypes,
    StorageService
} from "sxmServices";
import { Subscription }                                         from "rxjs";
import { TranslateService }                                     from "@ngx-translate/core";
import { ProfileConfig }                                        from "./profile.interface";
import { profileConfigConst }                                   from "./profile.const";
import { AutoUnSubscribe }                                      from "../common/decorator/auto-unsubscribe";
import { APP_CONFIG }                                           from "../sxmservicelayer/sxm.service.layer.module";
import { ModalService }        from "../common/service/modal/modal.service";
import { IModalData }          from "app/common/service/modal/modal.interface";
import { LoginOverlayService } from "../open-access/popups/login-overlay/login-overlay.service";
import { appRouteConstants }   from "../app.route.constants";
import { languageConstants } from "../common/component/language-toggle/language.consts";
import { BrowserUtil } from "app/common/util/browser.util";

/**
 * @MODULE:     client
 * @CREATED:    08/02/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     ProfileComponent used to load profile page
 */

@Component({
    selector: "profile-dropdown",
    templateUrl: "./profile.component.html",
    styleUrls: [ "./profile.component.scss" ]

})

@AutoUnSubscribe([])
export class ProfileComponent implements OnInit, AfterViewInit
{
    public session: ISession;
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ProfileComponent");

    /**
     * current Language
     */
    private currentLang: string;

    /**
     * Check whether the data is ready or not
     */
    public isLoading: boolean = true;

    /**
     * Holds the Profile External URL's.
     */
    private profileConfigs: ProfileConfig = {} as any;

    /**
     * Holds the configuration all the components:FAQ|Feedback|Help_Support...
     */
    private componentConfig: any = null;

    /**
     * Stores subscriptions to un subscribe when component is destroyed
     * @type {Array<Subscription>}
     * @memberof ProfileComponent
     */
    private subscriptions: Array<any> = [];

    /**
     * Used to store Profile gup bypass enabled or not. If enabled then disables
     * Reminders, application settings and messaging
     * @type {boolean}
     */
    public gupByPass: boolean = false;

    /**
     * Holds the Do not Sell my info CCPA Url.
     */
    public dataPrivacyUrl: string;

    @Input() public displayProfileDropdown: boolean;
    @Output() public closeProfileDropdown: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() public focusProfileDropdown = new EventEmitter<boolean>();

    @HostListener('document:click', ['$event']) onClick(event)
    {

        if (!(event.target.id === "settings-dropdown-trigger" || event.target.className === "profile-dropdown__list__item"))
        {
            this.closeProfileDropdown.emit(false);
        }
    }

    /**
     * Keyboard event handler
     */
    @HostListener("document:keydown", [ "$event" ])
    onKeyPress(event: KeyboardEvent)
    {
        if (event.key === KeyboardEventKeyTypes.ESC || event.key === KeyboardEventKeyTypes.ESCAPE)
        {
            this.closeProfileDropdown.emit(false);
        }
    }

    @ViewChild('dropdownMenu') dropdownMenuElement: ElementRef;

    constructor(@Inject(APP_CONFIG) public appConfig: IAppConfig,
                private authenticationService: AuthenticationService,
                private configService: ConfigService,
                private translateService: TranslateService,
                private loginOverlayService: LoginOverlayService,
                private location: Location,
                private modalService: ModalService,
                private storageService: StorageService,
                public viewContainerRef: ViewContainerRef,
                private applicationRef: ApplicationRef,
                public bypassMonitorService: BypassMonitorService,
                private logService : LogService)
    {
        ProfileComponent.logger.debug("Profile Component Constructor");

        this.currentLang = this.translateService.currentLang;

        let languageSubscription: Subscription = this.translateService.onLangChange.subscribe(data =>
        {
            this.currentLang = data.lang;

            this.loadProfileConfigs();
        });

        this.subscriptions.push(this.authenticationService.userSession.subscribe((session) =>
        {
            this.session = session;
        }));

        this.subscriptions.push(this.bypassMonitorService.bypassErrorState.subscribe((state: IAppByPassState) =>
        {
            this.gupByPass = state.GUP_BYPASS2 || state.GUP_BYPASS;
        }));

        this.subscriptions.push(languageSubscription);
    }

    /**
     * onInit
     * Subscribe to configuration from configService.
     */
    ngOnInit(): void
    {
        let configSubscription = this.configService.configuration.subscribe(configuration =>
        {
            if (configuration)
            {
                this.componentConfig = ProfileComponent.normalizeConfigurationData(configuration.components);
                this.loadProfileConfigs();
            }
        });
        this.subscriptions.push(configSubscription);
    }

    public ngAfterViewInit(): void
    {
        const listItems = this.dropdownMenuElement.nativeElement.children ? this.dropdownMenuElement.nativeElement.children : [];
        if (listItems.length > 0)
        {
            listItems[0].addEventListener('keydown', closeMenuOnShiftTabKey.bind(this));
            listItems[listItems.length-1].addEventListener('keydown', closeMenuOnTabKey.bind(this));
        }

        function closeMenuOnShiftTabKey(event)
        {
            if (event.key === KeyboardEventKeyTypes.TAB && event.shiftKey)
            {
                this.closeProfileDropdown.emit(false);
                this.focusProfileDropdown.emit(true);
                event.preventDefault();
            }
        }

        function closeMenuOnTabKey(event)
        {
            if (event.key === KeyboardEventKeyTypes.TAB && !event.shiftKey)
            {
                this.closeProfileDropdown.emit(false);
                this.focusProfileDropdown.emit(true);
                event.preventDefault();
            }
        }
    }

    /**
     * TODO : Will need to move this normailization to config-delegate
     * Used to normalize all the properties into a Object
     * @param configurationData from the config service
     */
    private static normalizeConfigurationData(configurationData: Array<IComponentConfig>)
    {
        let config = {};
        for (let i = 0; i < configurationData.length; ++i)
        {
            config[ configurationData[ i ].name ] = configurationData[ i ];
        }
        return config;
    }

    /**
     * Used to load the profile configurations from componentConfig
     * @param {settings} Array
     */
    private loadProfileConfigs(): void
    {
        if (!this.componentConfig) return;

        let keys = Object.keys(profileConfigConst);
        const localeKey = this.currentLang === languageConstants.ENGLISH_CANADA ? languageConstants.ENGLISH : this.currentLang;

        keys.forEach(key =>
        {
            if( key !== 'b')
            {
                const settings = this.componentConfig[ key ].settings;
                let setting = settings.find(setting => setting.locale === localeKey);
                if(!setting && settings.length > 0)
                {
                    setting = settings[0];
                }
                if(setting)
                {
                    this.profileConfigs[ key ] = setting.urlSettings[ 0 ];
                }
            }
        });

        const onBoardingSettings = this.configService.getOnBoardingSettings(this.currentLang);
        this.dataPrivacyUrl        = onBoardingSettings.dataPrivacyUrl;
        this.isLoading = false;
    }

    /**
     * logout method used to logout the user using authentication service
     */
    logout(): void
    {
        ProfileComponent.logger.debug(`logout()`);

        this.authenticationService.logout()
            .subscribe(onLogout.bind(this),onLogout.bind(this));

        function onLogout()
        {
            this.storageService.clearAll();
            const routeConstant = this.appConfig.isFreeTierEnable ? appRouteConstants.FT_WELCOME
                                                                  : appRouteConstants.AUTH.LOGIN;
            this.location.go(`${routeConstant}`);
            BrowserUtil.reload();
        }
    }

    /**
     * openLoginOverlay method used open the overlay-login screen
     */
    openLoginOverlay()
    {
        this.loginOverlayService.open(this.applicationRef.components[0].instance.viewContainerRef);
    }

    displayLogoutModal()
    {
        let modalData: IModalData;

        this.translateService.get("profile.logoutModalData")
            .subscribe(translation => modalData = translation)
            .unsubscribe();

        modalData.buttonTwo.action = this.logout.bind(this);

        this.modalService.addDynamicModal(modalData);
    }

    sendLogs(obj,event)
    {
        const ctrlHeld = event.ctrlKey;

        this.logService.sendLogs(ctrlHeld);
    }
}
