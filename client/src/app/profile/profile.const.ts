import { SettingsConstants } from "sxmServices";
import {
    AudioQualityOptions,
    ProfileConfig
} from "./profile.interface";

/**
 * @MODULE:     service-lib
 * @CREATED:    07/31/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  profileConstants are constants are used for Configure External URL's.
 **/

export const profileConfigConst = {
    FAQURL: "FAQURL",
    FeedbackUrl: "FeedbackUrl",
    PrivacyPolicy: "PrivacyPolicy",
    CustomerAgreement: "CustomerAgreement",
    OAC: "OAC"
};

/**
 *  Data Model for the Audio streaming quality Options to iterate in the template .
 **/
export const streamingQualityOptions: Array<AudioQualityOptions> = [
    {
        label: SettingsConstants.AUDIO_QUALITY_NORMAL,
        value: SettingsConstants.AUDIO_QUALITY_NORMAL
    },
    {
        label: SettingsConstants.AUDIO_QUALITY_HIGH,
        value: SettingsConstants.AUDIO_QUALITY_HIGH
    },
    {
        label: SettingsConstants.AUDIO_QUALITY_MAXIMUM,
        value: SettingsConstants.AUDIO_QUALITY_MAXIMUM
    }
];

/**
 * screenName Used to request carousels based on page Name.
 **/
export const profileCarouselTypes = {
    SAVE_FOR_LATER: "save for later",
    RECENT_HISTORY: "Listening History",
    RECENTLY_PLAYED: "recently played"

};


/**
 * Used to show/hide open access overlay
 *
 */
export const overlayConsts = {
    DISPLAY_OA_OVERLAY: "displayOA",
    DISPLAY_CS_OVERLAY: "displayCS"
};

