import { TestBed } from '@angular/core/testing';
import { By } from "@angular/platform-browser";
import { ViewContainerRef, ApplicationRef } from '@angular/core';
import { ProfileComponent } from "./profile.component";
import { BypassMonitorService, ConfigService, IAppByPassState, LogService, StorageService } from "sxmServices";
import { ConfigServiceMock } from "../../../test/mocks/config.service.mock";
import { TranslateService } from "@ngx-translate/core";
import { of as observableOf } from "rxjs";
import { AuthenticationService } from "sxmServices";
import { AuthenticationServiceMock } from "../../../test/mocks/authentication.service.mock";
import { MockComponent } from "../../../test/mocks/component.mock";
import { TranslationModule } from "../translate/translation.module";
import { MockTranslateService } from "../../../test/mocks/translate.service";
import { APP_CONFIG, SXMServiceLayerModule } from "../sxmservicelayer/sxm.service.layer.module";
import { LoginOverlayService } from "app/open-access/popups/login-overlay/login-overlay.service";
import { Location } from "@angular/common";
import { ModalService } from "app/common/service/modal/modal.service";
import { instance, mock } from "ts-mockito";
import { BrowserUtil } from "app/common/util/browser.util";

describe('ProfileComponent', function()
{
    beforeEach(function()
    {
        this.viewContainerRef = instance(mock(ViewContainerRef));
        this.applicationRef   = {
            components: [ {
                instance: {
                    viewContainerRef: "viewContainerRef"
                }
            } ]
        };

        this.appConfig = {
            clientConfiguration:
                {
                    seededRadioSubscribed: false
                },
            deviceInfo:
                {
                    clientCapabilities: "defaultValue"
                }
        };

        this.authenticationService = AuthenticationServiceMock.getSpy();
        this.translateService      = MockTranslateService.getSpy();

        this.loginOverlayService = {
            open: jasmine.createSpy('open')
        };

        this.modalService = {
            addDynamicModal: jasmine.createSpy('addDynamicModal')
        };

        this.storageService = {
            clearAll: jasmine.createSpy('clearAll'),
            getItem: jasmine.createSpy('getItem').and.returnValue("storageServiceName")
        };

        this.bypassMonitorService = {
            bypassErrorState: observableOf({} as IAppByPassState)
        };

        this.logService = {
            sendLogs: jasmine.createSpy('sendLogs')
        };

        this.location = {
            go: jasmine.createSpy("go")
        };

        this.configService = new ConfigServiceMock();

        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SXMServiceLayerModule
            ],
            declarations: [
                ProfileComponent,
                MockComponent({ selector: 'a', inputs: [ 'routerLink' ] })
            ],
            providers: [
                ProfileComponent,
                { provide: TranslateService, useValue: this.translateService },
                { provide: AuthenticationService, useValue: this.authenticationService },
                { provide: ConfigService, useValue: this.configService },
                { provide: LoginOverlayService, useValue: this.loginOverlayService },
                { provide: Location, useValue: this.location },
                { provide: ModalService, useValue: this.modalService },
                { provide: StorageService, useValue: this.storageService },
                { provide: BypassMonitorService, useValue: this.bypassMonitorService },
                { provide: LogService, useValue: this.logService },
                { provide: ViewContainerRef, useValue: this.viewContainerRef },
                { provide: ApplicationRef, useValue: this.applicationRef },
                { provide: APP_CONFIG, useValue: this.appConfig }
            ]
        });

        this.fixture = TestBed.createComponent(ProfileComponent);
        this.component = this.fixture.componentInstance;
        this.component.isLoading = false;
        this.component.dropdownMenuElement = {nativeElement: {}};
        this.configService = TestBed.get(ConfigService);
        this.debugElement = this.fixture.debugElement;
        this.fixture.detectChanges();
    });

    describe('Profile component Execution', function ()
    {
        it('Subscription to ConfigService', function ()
        {
            this.component.ngOnInit();
            this.configService.configuration.subscribe(response =>
            {
                let components = response.components;
                expect(Array.isArray(components)).toBe(true);
                expect(components[ 0 ].settings).toBeDefined();
            });

        });

        it('Trigger logout modal on click of logout button', function ()
        {
            this.component.ngOnInit();
            this.fixture.detectChanges();
            let button = this.debugElement.query(By.css('.login button'));
            button.triggerEventHandler('click', null);
            expect(this.modalService.addDynamicModal).toHaveBeenCalled();
        });

        it('Trigger login modal on click of login button', function ()
        {
            this.component.session    = { ...this.component.session, activeOpenAccessSession: true };

            this.component.ngOnInit();
            this.fixture.detectChanges();
            let button = this.debugElement.query(By.css('.login button'));
            button.triggerEventHandler('click', null);
            expect(this.loginOverlayService.open).toHaveBeenCalled();
            expect(this.loginOverlayService.open).toHaveBeenCalledWith('viewContainerRef');
        });

        it('Logout the User:', function ()
        {
            spyOn(BrowserUtil, "reload");
            this.authenticationService.logout.and.returnValue(observableOf(true));
            this.component.ngOnInit();
            this.component.logout();
            expect(this.authenticationService.logout).toHaveBeenCalled();
            expect(this.storageService.clearAll).toHaveBeenCalled();

        });

        it('when sendLogs is called, trigger logService sendlogs', function()
        {
            this.component.sendLogs("obj", {});
            expect(this.logService.sendLogs).toHaveBeenCalled();
        });
    });
});
