import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { NavigationModule } from "../navigation/navigation.module";
import { ApplicationSettingsComponent, ProfileCarouselComponent, ReminderSettingsComponent } from "./index";
import { CarouselModule } from "../carousel/carousel.module";
import { TranslationModule } from "../translate/translation.module";
import { SharedModule } from "../common/shared.module";
import { RouterModule } from "@angular/router";
import { ProfileCarouselRouteGuard } from "./profile-carousel/profile-carousel.route-guard";
import { ManageShowRemindersComponent } from "./manage-show-reminders/manage-show-reminders.component";
import { ShowTileModule } from "../common/component/list-items/show/show-list-item.module";
import { NoResultsModule } from "../no-results/no-results.module";
import { FavoritesModule } from "../favorites/favorites.module";
import { MessagingSettingsComponent } from "./messaging-settings/messaging-settings.component";
import { ShowReminderDetailsComponent } from "./manage-show-reminders/show-reminder-details.component";
import { SeededSettingsComponent } from "./seeded-settings/seeded-settings.component";
import { SeededSettingsResolver } from "./seeded-settings/seeded-settings.resolver";
import { ChannelTileModule } from "../common/component/list-items/channel/channel-list-item.module";

@NgModule({
    imports     : [
        CommonModule,
        FormsModule,
        HttpModule,
        NavigationModule,
        CarouselModule,
        TranslationModule,
        SharedModule,
        RouterModule,
        ShowTileModule,
        NoResultsModule,
        FavoritesModule,
        ChannelTileModule
    ],
    declarations: [
        ApplicationSettingsComponent,
        ManageShowRemindersComponent,
        ShowReminderDetailsComponent,
        MessagingSettingsComponent,
        ReminderSettingsComponent,
        ProfileCarouselComponent,
        SeededSettingsComponent
    ],
    providers   : [
        SeededSettingsResolver,
        ProfileCarouselRouteGuard
    ]
})

export class ProfileModule
{
}
