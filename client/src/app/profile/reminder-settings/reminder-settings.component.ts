import { Component, OnDestroy, OnInit, Inject } from "@angular/core";
import {
    ISetting,
    ISettings,
    Logger,
    SettingsConstants,
    SettingsService,
    IAppConfig
} from "sxmServices";
import { appRouteConstants } from "../../app.route.constants";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { SubscriptionLike as ISubscription } from "rxjs";
import { NavigationService } from "../../common/service/navigation.service";
import { ContextualUtilities } from "../../../../../servicelib/src/contexual/contextual.utilities";
import { APP_CONFIG } from "app/sxmservicelayer/sxm.service.layer.module";

@Component({
    selector: "reminder-settings",
    templateUrl: "./reminder-settings.component.html",
    styleUrls: [ "./reminder-settings.component.scss" ]
})

@AutoUnSubscribe()
export class ReminderSettingsComponent implements OnInit, OnDestroy
{
    /**
     * Boolean flag to check weather all settings are loaded from the Service
     */
    public isLoading: boolean = true;

    /**s
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ReminderSettingsComponent");

    /**
     * globalSettings from settingsService.settings Observable.
     * And used to extract setting values from this list.
     */
    private globalSettings: Array<ISetting> = [];

    /**
     * deviceSettings from settingsService.settings Observable.
     * And used to extract setting values from this list.
     */
    private deviceSettings: Array<ISetting> = [];

    /**
     * Used to check whether Show Reminder is On/Off
     */
    public showReminders: boolean = true;

    /**
     * Used to check whether Suggested Show Reminder is On/Off
     */
    public isSuggestedShowRemindersOn: boolean = true;

    /**
     * Used to check whether Suggested Show Reminder is On/Off
     */
    public isSuggestedLiveVideoRemindersOn: boolean = true;

    //temp live video disable
    public isDevEndpoint: boolean = !ContextualUtilities.isProdServer(this.SERVICE_CONFIG.apiEndpoint);

    /**
     * Stores settings subscription to un subscribe when component is destroyed
     */
    private subscriptions: Array<ISubscription> = [];

    constructor(private navigationService: NavigationService,
                private settingsService: SettingsService,
                @Inject(APP_CONFIG) private SERVICE_CONFIG: IAppConfig)
    {
        ReminderSettingsComponent.logger.debug("Show Reminder Component Constructor");
        this.loadReminderSettings();
    }

    ngOnInit()
    {
        ReminderSettingsComponent.logger.debug("ngOnInit()");
    }

    ngOnDestroy()
    {
        ReminderSettingsComponent.logger.debug("ngOnDestroy()");
    }

    /**
     * Load reminder settings on creation of component by subscribing to ....
     */
    private loadReminderSettings()
    {
        let settingsSubscription = this.settingsService.settings.subscribe((response: ISettings) =>
        {
            if (response)
            {
                this.deviceSettings = response.deviceSettings;
                this.globalSettings = response.globalSettings;
                this.showReminders = this.settingsService.isGeneralSettingOn(SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS, true);
                this.isSuggestedShowRemindersOn = this.settingsService.isGeneralSettingOn(
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_SUGGESTED_SHOW,
                    true
                );
                this.isSuggestedLiveVideoRemindersOn = this.settingsService.isGeneralSettingOn(
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_SUGGESTED_LIVE_VIDEO,
                    true
                );

                this.isLoading = false;
            }
        });
        this.subscriptions.push(settingsSubscription);
    }

    /**
     * used to update Show reminders
     */
    public updateShowReminders(): void
    {
        this.settingsService.switchGlobalSettingOnOrOff(
            !this.showReminders,
            SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS);
    }

    /**
     * used to update Suggested show reminders
     */
    public updateSuggestedShowReminders(): void
    {
        this.settingsService.switchGlobalSettingOnOrOff(
            !this.isSuggestedShowRemindersOn,
            SettingsConstants.NOTIFICATION_SUBSCRIPTION_SUGGESTED_SHOW);
    }

    /**
     * used to update Suggested Live Video Reminders
     */
    public updateSuggestedLiveVideoReminders(): void
    {
        this.settingsService.switchGlobalSettingOnOrOff(
            !this.isSuggestedLiveVideoRemindersOn,
            SettingsConstants.NOTIFICATION_SUBSCRIPTION_SUGGESTED_LIVE_VIDEO);
    }

    /**
     * used to navigate to manage show reminders
     */
    public manageShowReminders(): void
    {
        this.navigationService.go([appRouteConstants.MANAGE_SHOW_REMINDERS]);
    }

}
