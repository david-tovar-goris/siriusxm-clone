import {
    TestBed
} from '@angular/core/testing';
import { ReminderSettingsComponent } from './reminder-settings.component';
import { TranslateModule } from "@ngx-translate/core";
import {
    SettingsConstants,
    SettingsService
} from "sxmServices";
import { NavigationService } from "../../common/service/navigation.service";
import { Subject ,  of as observableOf } from "rxjs";
import { appRouteConstants } from "../../app.route.constants";
import { SettingsServiceMock } from "../../../../test/mocks/settings.service.mock";


describe('ReminderSettingsComponent', function()
{
    beforeEach(function()
    {
        this.navigationService = {
            go: jasmine.createSpy('go')
        };

        this.settingsSubject = new Subject();

        TestBed.configureTestingModule({
            imports: [
                TranslateModule
            ],
            providers: [
                ReminderSettingsComponent,
                { provide: NavigationService, useValue: this.navigationService },
                { provide: SettingsService, useValue: SettingsServiceMock.getSpy() }
            ]
        });

        this.component = TestBed.get(ReminderSettingsComponent);
    });

    describe('Component when initialised', function()
    {
        it('loadReminderSettings()', function()
        {
            expect(this.component.showReminders).toEqual(true);
            expect(this.component.isSuggestedShowRemindersOn).toEqual(false);
            expect(this.component.isSuggestedLiveVideoRemindersOn).toEqual(true);
        });
    });

    describe('manageShowReminders()', function()
    {
        it('calls navigationService.go()', function()
        {
            this.component.manageShowReminders();
            expect(this.navigationService.go).toHaveBeenCalledWith([appRouteConstants.MANAGE_SHOW_REMINDERS]);
        });
    });

    describe('updateSuggestedLiveVideoReminders', function()
    {
        describe('when setting is on', function()
        {
            beforeEach(function()
            {
                this.component.isSuggestedLiveVideoRemindersOn = true;
            });

            it('calls settingsService switchGlobalSettingOnOrOff', function()
            {
                this.component.updateSuggestedLiveVideoReminders();
                expect(SettingsServiceMock.getSpy().switchGlobalSettingOnOrOff).toHaveBeenCalledWith(false,
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_SUGGESTED_LIVE_VIDEO);
            });
        });

        describe('when setting is off', function()
        {
            beforeEach(function()
            {
                this.component.isSuggestedLiveVideoRemindersOn = false;
            });

            it('calls settingsService switchGlobalSettingOnOrOff', function()
            {
                this.component.updateSuggestedLiveVideoReminders();
                expect(SettingsServiceMock.getSpy().switchGlobalSettingOnOrOff).toHaveBeenCalledWith(true,
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_SUGGESTED_LIVE_VIDEO);
            });
        });
    });

    describe('updateSuggestedShowReminders', function()
    {
        describe('when setting is on', function()
        {
            beforeEach(function()
            {
                this.component.isSuggestedShowRemindersOn = true;
            });

            it('calls settingsService switchGlobalSettingOnOrOff', function()
            {
                this.component.updateSuggestedShowReminders();
                expect(SettingsServiceMock.getSpy().switchGlobalSettingOnOrOff).toHaveBeenCalledWith(false,
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_SUGGESTED_SHOW);
            });
        });

        describe('when setting is off', function()
        {
            beforeEach(function()
            {
                this.component.isSuggestedShowRemindersOn = false;
            });

            it('calls settingsService switchGlobalSettingOnOrOff', function()
            {
                this.component.updateSuggestedShowReminders();
                expect(SettingsServiceMock.getSpy().switchGlobalSettingOnOrOff).toHaveBeenCalledWith(true,
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_SUGGESTED_SHOW);
            });
        });
    });

    describe('updateShowReminders', function()
    {
        describe('when setting is on', function()
        {
            beforeEach(function()
            {
                this.component.showReminders = true;
            });

            it('calls settingsService switchGlobalSettingOnOrOff', function()
            {
                this.component.updateShowReminders();
                expect(SettingsServiceMock.getSpy().switchGlobalSettingOnOrOff).toHaveBeenCalledWith(false,
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS);
            });
        });

        describe('when setting is off', function()
        {
            beforeEach(function()
            {
                this.component.showReminders = false;
            });

            it('calls settingsService switchGlobalSettingOnOrOff', function()
            {
                this.component.updateShowReminders();
                expect(SettingsServiceMock.getSpy().switchGlobalSettingOnOrOff).toHaveBeenCalledWith(true,
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS);
            });
        });
    });
});

