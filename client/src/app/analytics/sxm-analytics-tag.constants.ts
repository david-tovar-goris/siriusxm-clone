export class ArtistRadioAnalyticsTags
{
    public static NP_ARTS_RDIO_RTNG = "NpArtsRdioRtng";
    public static ART_RDIO_RMV = "ArtRdioRmv";
}

export class AnalyticsFindingMethods
{
    public static FAVORITES = "FAVORITES";
    public static RECENT = "RECENT";
    public static BROWSE = "BROWSE";
    public static ARTIST_RADIO = "ARTIST_RADIO";
    public static SEARCH = "SEARCH";
    public static CAROUSEL = "CAROUSEL";
    public static MODAL = "MODAL";
    public static TOAST = "TOAST";
}

export class AnalyticsTagNameConstants
{
    // public static DISCLAIMER

    public static MNP_EXPAND             = "MnpContExp";
    public static MNP_EXPAND_BUTTON_NAME = "Content Expand";

    public static MNP_ADD_REM_FAV             = "MnpAddRemFav";
    public static MNP_ADD_REM_FAV_BUTTON_NAME = "Mini Add/Remove Favorite";

    public static MNP_RESTART             = "MnpRestart";
    public static MNP_RESTART_BUTTON_NAME = "StartOver";

    public static MNP_GO_LIVE             = "MnpGoLive";
    public static MNP_GO_LIVE_BUTTON_NAME = "GoLive";

    public static MNP_BACK_15             = "MnpBack15";
    public static MNP_BACK_15_BUTTON_NAME = "Back15";

    public static MNP_FORWARD_15             = "MnpFwd15";
    public static MNP_FORWARD_15_BUTTON_NAME = "Forward15";

    public static MNP_BACK_SKIP             = "MnpBackSkip";
    public static MNP_BACK_SKIP_BUTTON_NAME = "SkipBack";

    public static MNP_FORWARD_SKIP             = "MnpFwdSkip";
    public static MNP_FORWARD_SKIP_BUTTON_NAME = "SkipForward";

    public static MNP_CAST             = "MnpCast";
    public static MNP_CAST_BUTTON_NAME = "Casting";

    public static MNP_PLAY_PAUSE             = "MnpPlayPause";
    public static MNP_PLAY_PAUSE_BUTTON_NAME = "Mini Play/Pause";

    public static MNP_PROGRESS_BAR             = "MnpProgressBar";
    public static MNP_PROGRESS_BAR_BUTTON_NAME = "MiniProgress";
    public static MDL_CLOSE                    = "MdlClose";
    public static MDL_CLOSE_BUTTON_NAME        = "Close";

    public static MDL_UP_NEXT_MODAL_NAME = "UpNext";

    public static MDL_CAROUSEL_NAV_BF             = "MdlCarouselNavBF";
    public static MDL_CAROUSEL_NAV_BF_BUTTON_NAME = "MdlCarouselNav";

    public static MDL_CAROUSEL_TILE             = "MdlCarouselTile";
    public static MDL_CAROUSEL_TILE_BUTTON_NAME = "MdlCarouselTile";

    public static RCNT_TILE             = "RcntTile";
    public static RCNT_TILE_BUTTON_NAME = "RecentTile";

    public static RCNT_CLEAR_ALL             = "RcntClearAll";
    public static RCNT_CLEAR_ALL_BUTTON_NAME = "Clear All";

    public static BUTTON_NAME          = "ButtonName";
    public static MV_CLOSE             = "MVClose";
    public static MV_CLOSE_BUTTON_NAME = "Mini Video Close";

    public static NP_SHOW_EDP             = "NpShwEdp";
    public static NP_SHOW_EDP_BUTTON_NAME = "ShowEdp";

    public static MV_EXPAND_NP             = "MVExpandNP";
    public static MV_EXPAND_NP_BUTTON_NAME = "Mini Video Expand NP";

    public static MV_EXPAND             = "MVExpand";
    public static MV_EXPAND_BUTTON_NAME = "Mini Video Expand";

    public static MV_PLAY_PAUSE             = "MVPlayPause";
    public static MV_PLAY_PAUSE_BUTTON_NAME = "Mini Video Play/Pause";

    public static NP_CNTN_RTN             = "NpCntnRtn";
    public static NP_CNTN_RTN_BUTTON_NAME = "Return";

    public static NP_AVAIL_SEG             = "NpAvailSeg";
    public static NP_AVAIL_SEG_BUTTON_NAME = "SegmentNavigationTile";

    public static NP_ADD_REM_REMIND             = "NpAddRemRemind";
    public static NP_ADD_REM_REMIND_BUTTON_NAME = "NowPlaying Add/Remove Reminder";

    public static NP_MIN             = "NpMin";
    public static NP_MIN_BUTTON_NAME = "Minimize";

    public static NP_PREV_CHAN             = "NpPrevChan";
    public static NP_PREV_CHAN_BUTTON_NAME = "PreviousChannelIndicator";

    public static NP_NXT_CHAN             = "NpNxtChan";
    public static NP_NXT_CHAN_BUTTON_NAME = "NextChannelIndicator";

    public static NP_CH_EDP             = "NpChEdp";
    public static NP_CH_EDP_BUTTON_NAME = "ChannelEdp";

    public static NP_ADD_REM_FAV             = "NpAddRemFav";
    public static NP_ADD_REM_FAV_BUTTON_NAME = "Add/Remove Favorite";

    public static NP_FULL_SCREEN_EXP_RTN             = "NpFullScreenExpRtn";
    public static NP_FULL_SCREEN_EXP_RTN_BUTTON_NAME = "FullScreen";

    public static FAV_CHANNELS             = 'FavChan';
    public static FAV_CHANNELS_BUTTON_NAME = 'Channels';

    public static FAV_SHOWS             = 'FavShow';
    public static FAV_SHOWS_BUTTON_NAME = 'Shows';

    public static FAV_EPISODES             = 'FavEpisodes';
    public static FAV_EPISODES_BUTTON_NAME = 'OnDemand';

    public static FAV_TILE             = 'FavTile';
    public static FAV_TILE_BUTTON_NAME = 'FavoritesTile';

    public static FAV_MOVE_TILE             = 'FavMoveTile';
    public static FAV_MOVE_TILE_BUTTON_NAME = 'MoveFavorite';

    public static MDL_BUTTON = "MdlButton";
    public static MD_ELEMENT = "MdElement";

    public static MDL_CONFIRM_DELETE_MODAL = "ConfirmDelete";

    public static VIEW_ALL             = "ViewAll";
    public static VIEW_ALL_BUTTON_NAME = "More/View All";

    public static ARTIST_RADIO_TILE             = "ArtRdioTile";
    public static ARTIST_RADIO_TILE_BUTTON_NAME = "ArtistRadioTile";

    public static NP_CREATE_ARTIST_RADIO                  = "NpCreateArtRadio";
    public static NP_CREATE_PLAY_ARTIST_RADIO_BUTTON_NAME = "CreatePlayArtistRadio";

    public static CAROUSEL_TILE             = "CarouselTile";
    public static CAROUSEL_TILE_BUTTON_NAME = "CarouselTile";

    public static SHOW_EPISODES_TILE= "ShowEpsTile";
    public static SHOW_EPISODES_TILE_BUTTON_NAME= "ShowEpisodesTile";

    public static ADDTNL_CONTENT_TILE= "AddtlContent";
    public static ADDTNL_CONTENT_TILE_BUTTON_NAME= "AdditionalContent";

    public static ART_RDIO_ADD_REM_FAV          = "ArtRdioAddRemFav";
    public static ART_RDIO_ADD_REM_FAV_BUTTON_NAME = "Add/Remove Favorite";

    public static NP_ARTS_RDIO_RTNG             = "NpArtsRdioRtng";
    public static NP_ARTS_RDIO_RTNG_BUTTON_NAME = "Thumbs Up/Down";

    public static BREADCRUMB_BACK = "BrdBck";

    public static CAROUSEL_NAV_BACK_FWD = "CarouselNavBF";
    public static CAROUSEL_NAV_BF_BUTTON_NAME = "CarouselNav";
    public static SEARCH_CLEAR_HISTORY = "SrchClearHist";
    public static SEARCH_CLEAR_HISTORY_BUTTON_NAME = "ClearHistory";

    public static SEARCH_INPUT = "SearchInput";
    public static SEARCH_INPUT_BUTTON_NAME = "SearchInput";

    public static SEARCH_RECENT = "SearchRecent";
    public static SEARCH_RECENT_BUTTON_NAME = "RecentSearch";

    public static UNKNOWN = "UNKNOWN";

    public static VIDEO_FULL_SCREEN_RTN = "VideoFullScreenRtn";
    public static VIDEO_FULL_SCREEN_RTN_BUTTON_NAME = "Return";

    public static EDP_LSTN_NW = "EdpLstnNw";
    public static EDP_LSTN_NW_BUTTON_NAME = "ListenNow";

    public static SRCH_LAUNCH = "SrchLaunch";
    public static SRCH_LAUNCH_BUTTON_NAME = "Launch";

    public static SRCH_RETURN_RESULTS = "SrchReturnResults";
    public static SRCH_RETURN_RESULTS_BUTTON_NAME = "ReturnResults";

    public static SUPER_CAT_MENU = "SuperCatMenu";
    public static SUPER_CAT_MENU_BUTTON_NAME= "SuperCategoryMenu";

    public static SUB_CATEGORY_LIST = "CategoryList";
    public static SUB_CATEGORY_LIST_BUTTON_NAME = "CategoryListRender";

    public static CATEGORIES_CHANNELS = "CatChan";
    public static CATEGORIES_CHANNELS_BUTTON_NAME = "Channels";

    public static CATEGORIES_ONDEMAND = "CatOnDemand";
    public static CATEGORIES_ONDEMAND_BUTTON_NAME = "OnDemand";

    public static SEGMENT_BTN_ALLCHANNELS = 'AllChannels';
    public static SEGMENT_BTN_ALLCHANNELS_BUTTON_NAME = 'AllChannels';

    public static SEGMENT_BTN_ONDEMAND = 'AllOnDemand';
    public static SEGMENT_BTN_ONDEMAND_BUTTON_NAME = 'AllOnDemand';

    public static SEGMENT_BTN_VIDEOS = 'AllVideos';
    public static SEGMENT_BTN_VIDEOS_BUTTON_NAME = 'AllVideos';

    public static CAT_FILTER_CLEAR = "CatFilterClear";
    public static CAT_FILTER_CLEAR_BUTTON_NAME = "FilterClear";

    public static CAT_FILTER_INPUT = "CatFilterInput";
    public static CAT_FILTER_INPUT_BUTTON_NAME = "FilterInput";

    public static ZONE_CAT_MENU = "ZoneCatMenu";
    public static ZONE_CAT_MENU_BUTTON_NAME = "ZoneCategoryMenu";

    public static MSG_TOAST = "MsgToast";
    public static MSG_TOAST_BUTTON_NAME = "Toast Message";

    public static MSG_TOAST_CLICK = "MsgToastClk";
    public static MSG_TOAST_CLICK_BUTTON_NAME = "Toast Message Click";

    public static  LOGIN_PLATFORM_DOWNLOAD = "LgnPlatformDwnld";
    public static  LOGIN_PLATFORM_DOWNLOAD_BUTTON_NAME = "Platform Downloads";

    public static  LOGIN_GET_STARTED = "LgnGetStarted";
    public static  LOGIN_GET_STARTED_BUTTON_NAME = "GetStarted";

    public static  LOGIN_SIGN_IN_OUT = "LgnSignInOut";
    public static  LOGIN_SIGN_IN_OUT_BUTTON_NAME = "Login Sign In/Out";

    public static  LOGIN_FORGOT = "LgnForgot";
    public static  LOGIN_FORGOT_LINK_NAME = "ForgotUsernamePassword";

    public static  LOGIN_DISCLAIMER = "LgnDisclaim";
    public static  LOGIN_DISCLAIMER_LINK_NAME = "LoginDisclaimers";

    public static OVERLAY_CLOSE = "OvlyClose";
    public static OVERLAY_CLOSE_BUTTON_NAME = "Close";
    public static ERROR_MESSAGE = "MsgError";

    public static EDP_ADD_RMV_FAV_CH = "EdpAddRmvFavCh";
    public static EDP_ADD_RMV_FAV_SHW = "EdpAddRmvFavShw";
    public static EDP_ADD_RMV_FAV_CH_BTN_NAME = "AddRemoveFavoritesCh";
    public static EDP_ADD_RMV_FAV_SHW_BTN_NAME = "AddRemoveFavoritesShw";

    public static EDP_BACK = "EdpBck";
    public static EDP_BACK_BUTTON_NAME = "CloseBack";

    public static EDP_ADD_RMV_SHW_REM = "EdpAddRmvShw";
    public static EDP_ADD_RMV_SHW_REM_BTN_NAME = "ShowReminder";

    public static EDP_READ_MORE_LESS = "EdpReadMreLss";
    public static EDP_READ_MORE_LESS_BTN_NAME = "MoreLess";

    public static SRCH_BROWSE_CAT = "SrchBrwsCat";
    public static SRCH_BROWSE_CAT_BTN_NAME = "SearchBrowseCategories";

    public static WEB_WELCOME_PAGE = "WebWlcmPage";
    public static WEB_WELCOME_DISCLAIM = "WebWlcmDisclaim";
    public static WEB_WELCOME_SUBSCRIBE = "WebWlcmSubscribe";
    public static WEB_WELCOME_SUBSCRIBE_BUTTON = "Subscribe";
    public static WEB_DEEPLINK_SUBSCRIBE = "WebDeepLinkSubscribe";
    public static WEB_DEEPLINK_EXPLORE = "WebDeepLinkExplore";
    public static WEB_WELCOME_EXPLORE = "WebWlcmExplore";
    public static WEB_WELCOME_EXPLORE_BUTTON = "Explore";
    public static WEB_WELCOME_ACCESSNOW = "WebWlcmAccessNow";
    public static WEB_WELCOME_ACCESSNOW_LINK = "AccessNow";
    public static WEB_SUBSCRIBE_CREATE_CREDENTIALS = "WebOvlyElement";
    public static WEB_OVERLAY_ELEMENT = "WebOvlyElement";
    public static WEB_OVERLAY_CLOSE = "WebOvlyClose";
    public static WEB_DEEP_LINK_PAGE = "WebDeepLinkPage";
    public static WEB_INVALID_LOGIN = "WebInvalidLogin";
    public static WEB_OVERLAY_BUTTON = "WebOvlyButton";
    public static WEB_EXPLORE_SHIM_SUBSCRIBE = "WebExploreShimSubscribe";

    public static WEB_FREE_ACCESS_LTUX = "WebFreeAccessLtux";
    public static WEB_FREE_ACCESS_LTUX_CTA = "WebFreeAccessLtuxCTA";
    public static WEB_FREE_ACCESS_LTUX_LOGIN = "WebFreeAccessLtuxLogin";
    public static EXPLORE_PLANS = "ExplorePlans";
    public static SIGN_IN = "SignIn";
}

export class AnalyticsTagShimConstants
{
    public static WEB_EXPLORE_SHIM = "Explore";
}

export class AnalyticsOverlayNameConstants
{
    public static WEB_EXPLORE_CREATE_CREDENTIALS = "Web_Explore_Create_Credentials";
    public static WEB_SUBSCRIBE_CREATE_CREDENTIALS = "Web_Subscribe_Create_Credentials";
    public static WEB_ACCESS_NOW = "Web_Access_Now";
}

export class AnalyticsMercuryEventConstants
{
    public static EVENT_SXM_EVEREST_TAG_WEB_EXPLORE_SHIM_SUBSCRIBE = "event_sxm_everest_tag_web_explore_shim_subscribe";
    public static EVENT_SXM_EVEREST_TAG_WEB_FREE_ACCESS_LTUX = "event_sxm_everest_tag_web_free_access_ltux";
    public static EVENT_SXM_EVEREST_TAG_WEB_FREE_ACCESS_LTUX_CTA = "event_sxm_everest_tag_web_free_access_ltux_cta";
    public static EVENT_SXM_EVEREST_TAG_WEB_FREE_ACCESS_LTUX_LOGIN = "event_sxm_everest_tag_web_free_accespublic static EVENT_SXM_EVEREST_TAG_WEB_WELCOME_PAGE = \"event_sxm_everest_tag_web_wlcm_page\";s_ltux_log_in";
    public static EVENT_SXM_EVEREST_TAG_WEB_WELCOME_PAGE = "event_sxm_everest_tag_web_wlcm_page";
    public static EVENT_SXM_EVEREST_TAG_WEB_WELCOME_DISCLAIM = "event_sxm_everest_tag_web_wlcm_disclaim";
    public static EVENT_SXM_EVEREST_TAG_WEB_WELCOME_SUBSCRIBE = "event_sxm_everest_tag_web_wlcm_subscribe";
    public static EVENT_SXM_EVEREST_TAG_WEB_WELCOME_EXPLORE = "event_sxm_everest_tag_web_wlcm_explore";
    public static EVENT_SXM_EVEREST_TAG_WEB_WELCOME_ACCESS_NOW = "event_sxm_everest_tag_web_wlcm_access_now";
    public static EVENT_SXM_EVEREST_TAG_WEB_OVERLAY_ELEMENT = "event_sxm_everest_tag_web_ovly_element";
    public static EVENT_SXM_EVEREST_TAG_WEB_INVALID_LOGIN = "event_sxm_everest_tag_web_invalid_login";
}

export class AnalyticsTagTextConstants
{
    public static MNP_CAST = "ChromeCast";

    public static NP_PLAY = "Play";
    public static NP_PAUSE = "Pause";

    public static NP_FULL_SCREEN_RTN = "Return";
    public static NP_FULL_SCREEN_EXP = "Expand";

    public static ADD_FAV_BUTTON_NAME = "AddFavorite";
    public static REM_FAV_BUTTON_NAME = "RemoveFavorite";

    public static MDL_CAROUSEL_NAV_BWD = "Back";
    public static MDL_CAROUSEL_NAV_FWD = "Fwd";

    public static NP_ADD_REMIND = "AddReminder";
    public static NP_REM_REMIND = "RemoveReminder";

    public static VIEW_ALL = "View All";

    public static NP_CREATE_ARTIST_RADIO = "CreateArtistRadio";
    public static NP_PLAY_ARTIST_RADIO = "PlayArtistRadio";

    public static NP_ARTS_RDIO_RTNG_UP = "ThumbsUp";
    public static NP_ARTS_RDIO_RTNG_DOWN = "ThumbsDown";

    public static CAROUSEL_NAV_FWD = "Fwd";
    public static CAROUSEL_NAV_BACK = "Back";
    public static FAV_EPISODES = "Episodes";

    public static SEGMENT_BTN_VIDEOS = "VIDEOS";

    public static GOOGLE_PLAY = "GooglePlay";
    public static APP_STORE = "AppleStore";
    public static WINDOWS_STORE = "Windows10";

    public static SIGN_IN = "SignIn";
    public static SIGN_OUT = "SignOut";

    public static ADD = "Add";
    public static REMOVE = "Remove";

    public static READ_MORE = "ReadMore";
    public static READ_LESS = "ReadLess";

    public static CHECK_AGREEMENT = "CheckAgreement";
    public static UNCHECK_AGREEMENT = "UncheckAgreement";
    public static SHOW_PASSWORD = "ShowPassword";
    public static HIDE_PASSWORD = "HidePassword";
    public static CONTINUE = "Continue";
    public static ACCESS_NOW = "AccessNow";

    public static DISCLAIMER = "Disclaimer";
}

export class AnalyticsTagActionConstants
{
    public static BUTTON_CLICK = "CLICK";
    public static DRAG = "DRAG";
    public static OPEN = "OPEN";
    public static CLOSE = "CLOSE";
    public static PAGE_LOAD = "PAGE_LOAD";
    public static LOAD = "LOAD";
}

export class AnalyticsTagActionSourceConstants
{
    public static MININP = "MININP";
    public static MAIN = "MAIN";
    public static NOW_PLAYING_MAIN = "NOW_PLAYING_MAIN";
    public static WEB_FLOW = "WEB_FLOW";
    public static WEB_WELCOME_LOAD = "WEB_WELCOME_LOAD";
}

export class AnalyticsCarouselNames
{
    public static HERO_CAROUSEL = "Hero";
    public static RECENT_CAROUSEL = "Recent";
    public static FAVORITES_CAROUSEL = "Favorites";
    public static AVAILABLE_SEGMENTS = "AvailableSegments";
    public static ARTIST_RADIO = "ArtistRadio";
    public static RECENT_SEARCHES = "Recent Searches";
    public static SUGGESTED_SEARCHES = "Suggested Searches";
    public static RELATED_EPISODE = "Related Episodes";
    public static CATEGORIES = "Categories";
    public static CHANNELS_LIST = "ChannelsList";
    public static EPISODES_LIST = "EpisodesList";
    public static ONDEMAND_LIST = "OnDemandList";
    public static CHANNELS = "Channels";
}

export class AnalyticsPageFrames
{
    public static MODAL = "MODAL";
    public static PAGE = "PAGE";
    public static NOW_PLAYING_MAIN = "NOW_PLAYING_MAIN";
    public static SUPER_CATEGORY_MENU = "SUPER_CATEGORY_MENU";
    public static SEARCH_HEADER = "SEARCH_HEADER";
    public static HEADER = "HEADER";
    public static EDP = "EDP";
    public static OVERLAY = "OVERLAY";
    public static SHIM = "SHIM";
}

export class AnalyticsElementTypes
{
    public static BUTTON = "BUTTON";
    public static SCREEN = "SCREEN";
    public static MODAL = "MODAL";
    public static TOAST = "TOAST";
    public static LINK = "LINK";
    public static OVERLAY = "OVERLAY";
}

export class AnalyticsInputTypes
{
    public static MOUSE = "MOUSE";
}

export class AnalyticsTileTypes
{
    public static CHANNEL = "channel";
    public static SHOW = "show";
    public static UNKNOWN = "UNKNOWN";
}

export class AnalyticsContentTypes
{
    public static NOW_PLAYING = "nowplaying";
    public static UNKNOWN = "UNKNOWN";
}

export class AnalyticsBannerInds
{
    public static UNAVAILABLE = "UNAVAILABLE";
}

export class AnalyticsScreens
{
    public static PROFILE_SETTINGS = "ProfileSettings";
    public static MANAGE_ARTIST_RADIO = "ManageArtistRadio";
    public static NOW_PLAYING = "NowPlaying";
    public static MODAL = "Modal";

    public static WEB_WELCOME_PAGE = "Web_Welcome_Page";
    public static WEB_DEEP_LINK_PAGE = "Web_Deep_Link_Page";

    public static WEB_EXPLORE_SHIM_SUBSCRIBE = "web_explore_shim_subscribe";
    public static WEB_FREE_ACCESS_LTUX = "Web_Free_Access_Ltux";
}

export class AnalyticsEDPNames
{
    public static CHANNEL = "ChannelEDP";
    public static SHOW = "ShowEDP";
}

export class AnalyticsZoneNames
{
    public static UNKNOWN = "Unknown";
    public static MUSIC = 'Music';
    public static TALKS = 'Talks';
    public static SPORTS = 'Sports';
    public static NEWS = 'News';
    public static HOWARD = 'Howard';
    public static DASHBOARD = 'Dashboard';
    public static ALL_CONTENT_BUTTONS = 'AllContentButtons';
}

export class AnalyticsModals
{
    public static MINI_NOW_PLAYING_BAR = "MINI_NOW_PLAYING_BAR";
    public static MINI_VIDEO = "MINI_VIDEO";
}

export class AnalyticsUserPaths
{
    public static AUTHENTICATION_CALL = "AuthenticationCall";
    public static SXM_LOGO = "SXMLogo_Screen";
    public static CNB_BROWSE = "CNB_Browse";
    public static CNB_FAVORITES = "CNB_Favorites";
    public static CNB_RECENTS = "CNB_Recents";
    public static CNB_SETTINGS = "CNB_Settings";
    public static CNB_SEARCH = "CNB_Search";
    public static CNB_SEARCH_CANCEL = "Search_Cancel";
    public static SUPER_CATEGORY_MENU = "SuperCategoryMenu";
    public static RCNT_TILE = "ContentTile";
    public static RCNT_CLEAR_ALL = "ClearAll";
    public static MODAL_BUTTON = "MdlButton_ModalName_Text>";
    public static MINI_CONTEXP = "Mini_ContExp";
    public static MNP_ADD_FAV = "Mini_AddFavorite";
    public static MNP_REM_FAV = "Mini_RemoveFavorite";

    public static ARTIST_RADIO_REM_FAV = "ArtistRadio_RemoveFav";
    public static ARTIST_RADIO_ADD_FAV = "ArtistRadio_AddFav";

    public static MNP_RESTART = "Mini_StartOver";
    public static MNP_GO_LIVE = "Mini_GoLive";
    public static MNP_BACK_15 = "Mini_Back15";
    public static MNP_FORWARD_15 = "Mini_Forward15";
    public static MNP_BACK_SKIP = "Mini_SkipBack";
    public static MNP_FORWARD_SKIP = "Mini_SkipForward";
    public static MNP_CAST = "Mini_Cast_MiniNowPlaying";
    public static MNP_PLAY = "Mini_Play";
    public static MNP_PAUSE = "Mini_Pause";
    public static MNP_PROGRESS_BAR = "Mini_ProgressBar_MiniNowPlaying";

    public static MDL_CLOSE = "MdlClose_UpNext";
    public static MDL_CAROUSEL_NAV_FWD = "MdlCarouselNav_UpNext_Related Episodes_Fwd";
    public static MDL_CAROUSEL_NAV_BWD = "MdlCarouselNav_UpNext_Related Episodes_Back";
    public static MDL_CAROUSEL_TILE = "MdlCarouselTile_UpNext_Related Episodes";
    public static MD_ELEMENT = "MdlElement_UpNext";

    public static MV_CLOSE = "MiniVideo_Close";
    public static MV_EXPAND_NP = "MiniVideo_ExpandNP";
    public static MV_EXPAND = "MiniVideo_Expand";
    public static MV_PLAY = "MiniVideo_Play";
    public static MV_PAUSE = "MiniVideo_Pause";

    public static NP_CNTN_RTN = "FullScreenContent_Return";
    public static NP_SHOW_EDP = "NowPlaying_ShowEdp";

    public static NP_AVAIL_SEG = "AvailableSegments";

    public static NP_ADD_REMIND = "AddReminder";
    public static NP_REM_REMIND = "RemoveReminder";

    public static NP_MIN = "Minimize";
    public static NP_PREV_CHAN = "PreviousChannel";
    public static NP_NXT_CHAN = "NextChannel";
    public static NP_CH_EDP = "ChannelEdp";
    public static NP_ADD_FAV = "AddFavorite";
    public static NP_REM_FAV = "RemoveFav";

    public static NP_FULL_SCREEN_RTN = "FullScreen_Return";
    public static NP_FULL_SCREEN_EXP = "FullScreen_Expand";

    public static FAV_CHANNELS = 'Favorites_Channels';
    public static FAV_SHOWS = 'Favorites_Shows';
    public static FAV_EPISODES = 'Favorites_Episodes';

    public static FAV_TILE = 'Favorites_Tile';
    public static FAV_MOVE_TILE = 'Favorites_MoveTile';

    public static VIDEO_FULL_SCREEN_RTN = "Mini_Return";

    public static VIEW_ALL = 'Carousel_ViewAll';

    public static ARTIST_RADIO_TILE = "ArtistRadio_ContentTile";
    public static ARTIST_RADIO = "ArtistRadio";

    public static NP_ARTS_RDIO_RTNG = "NpArtsRdioRtng";

    public static CAROUSEL_TILE = "CarouselTile";
    public static SHOE_EPISODES_TILE = "ShowEpisodesTile";
    public static ADDTNL_CONTENT_TILE = "AddtlContent";

    public static EDP_LSTN_NW = "EDPListenNow";

    public static BREADCRUMB_BACK = "Breadcrumb";

    public static CAROUSEL_NAV_BACK_FWD = "CarouselNav";
    public static SEARCH_CLEAR_HISTORY = "Search_ClearHistory";

    public static SEARCH_RECENT = "Search_Recent";

    public static SEARCH_INPUT = "Search_Input";

    public static SRCH_LAUNCH = "Search_Launch";
    public static SRCH_RETURN_RESULTS = "Search_ReturnResults";

    public static SUB_CATEGORY_LIST = "CategoryList";

    public static CATEGORIES_CHANNELS = "Categories_Channels";
    public static CATEGORIES_ONDEMAND = "Categories_OnDemand";

    public static SEGMENT_BTN_ALLCHANNELS = 'All Channels';
    public static SEGMENT_BTN_ONDEMAND = 'All OnDemand';
    public static SEGMENT_BTN_VIDEOS = 'All Videos';

    public static CAT_FILTER_CLEAR = "Categories_FilterClear";
    public static CAT_FILTER_INPUT = "Categories_FilterInput";

    public static ZONE_CAT_MENU = "ZoneCategoryMenu";

    public static MSG_TOAST = "Message_Toast";
    public static MSG_TOAST_CLICK = "Message_ToastClick";

    public static LOGIN_PLATFORM_DOWNLOAD = "LogIn_PlatformDownloads";
    public static LOGIN_GET_STARTED = "LogIn_OnBoarding_GetStarted";
    public static LOGIN_SIGN_IN_OUT = "LogIn";
    public static LOGIN_FORGOT = "LogIn_ForgotUsernamePwd";
    public static LOGIN_DISCLAIMER = "LogIn_Disclaimers";

    public static OVERLAY = "Overlay";
    public static ERROR_MESSAGE = "Message_Error";

    public static EDP_BACK = "EDPBack";

    public static EDP_CH_FAV = "EDPMenu_ChannelFavorites";
    public static EDP_SHW_FAV = "EDPMenu_ShowFavorites";
    public static EDP_SHW_REM = "EDPMenu_ShowReminder";

    public static EDP_MENU = "EDPMenu";

    public static SRCH_BROWSE_CAT = "Search_BrowseCategories";

    public static WEB_WELCOME_PAGE = "Web_Welcome_Page";
    public static WEB_SUBSCRIBE_CREATE_CREDENTIALS = "Web_Subscribe_Create_Credentials";
    public static WEB_EXPLORE_CREATE_CREDENTIALS = "Web_Explore_Create_Credentials";
    public static WEB_DEEP_LINK_CREATE_CREDENTIALS = "Web_Explore_Create_Credentials";
    public static WEB_ACCESS_NOW = "Web_Access_Now";
    public static WEB_OVERLAY_ELEMENT = "Web_Overlay_Element";
    public static WEB_OVERLAY = "Web_Overlay";
    public static WEBSHIM_EXPLORE_SHIM_SUBSCRIBE = "WebShim_Explore_Shim_Subscribe";
    public static WEB_FREE_ACCESS_LTUX_LOAD = "Web_free_access_ltux_Load";
}

export class AnalyticsCoreNavBarTags
{
    public static CNB_BROWSE_SHORTNAME = "CnbBrowse";
    public static CNB_BROWSE_BUTTON_NAME = "Browse";

    public static CNB_FAVORITES_SHORTNAME = "CnbFavorites";
    public static CNB_FAVORITES_BUTTON_NAME = "Favorites";

    public static CNB_RECENTS_SHORTNAME = "CnbRecent";
    public static CNB_RECENTS_BUTTON_NAME = "Recent";

    public static CNB_SETTINGS_SHORTNAME = "CnbSettings";
    public static CNB_SETTINGS_BUTTON_NAME = "Settings";

    public static CNB_SEARCH_SHORTNAME = "CnbSearch";
    public static CNB_SEARCH_BUTTON_NAME = "Search";

    public static CNB_SEARCH_CANCEL = "SrchCancel";
    public static CNB_SEARCH_CANCEL_BUTTON_NAME = "Cancel";
}

export class AnalyticsToastNameTags
{
    public static AUDIO_TOAST_NAME = "LiveAudio";
    public static VIDEO_TOAST_NAME = "LiveVideo";
}

export class AnalyticsErrorNumbers
{
    public static DEFAULT_ERROR_MSG_NUMBER = 411;
}

export class AnalyticsPageNames
{
    public static DEFAULT = "Default";
    public static COLD_START = "ColdStart";
}

export class AnalyticsDisclaimerLinkNames
{
    public static WELCOME_DISCLAIMERS = "WlcmDisclaimers";
    public static DISCLAIMER = "Disclaimer";
}
