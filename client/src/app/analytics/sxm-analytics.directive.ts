import { Directive, ElementRef, Input, HostListener } from "@angular/core";
import { IAppStore, INowPlayingStore } from "app/common/store";
import { Store } from "@ngrx/store";
import { selectNowPlayingState } from "app/common/store/now-playing.store";
import { AutoUnSubscribe } from "../common/decorator";
import { CarouselTypeConst, Logger, SxmAnalyticsService } from "sxmServices";
import { NavigationEnd, Router } from "@angular/router";
import { filter, pairwise, skip, take } from "rxjs/operators";
import { SubscriptionLike as ISubscription } from "rxjs/internal/types";
import { BrowserUtil } from "app/common/util/browser.util";
import { appRouteConstants } from "app/app.route.constants";
import {
    AnalyticsBannerInds,
    AnalyticsCarouselNames,
    AnalyticsCoreNavBarTags,
    AnalyticsElementTypes,
    AnalyticsFindingMethods,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsZoneNames,
    AnalyticsPageNames
} from "app/analytics/sxm-analytics-tag.constants";
import { selectSearchCarousel } from "app/common/store/carousel.store";

/**
 * @MODULE:     shared
 * @CREATED:    04/22/19
 * @COPYRIGHT:  2019 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     Analytics directive to capture UI events and pass the corresponding
 *     tags to the analytics service where the api call will be handled
 */
@Directive({
    selector: '[sxm-new-analytics]'
})
@AutoUnSubscribe()
export class SxmNewAnalyticsDirective
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("SxmNewAnalyticsDirective");

    @Input() tagName;
    @Input() tagType;
    @Input() tagText;
    @Input() carouselPosition;
    @Input() carouselName;
    @Input() pageFrame;
    @Input() elementType;
    @Input() tileType;
    @Input() screen;
    @Input() userPath;
    @Input() buttonName;
    @Input() linkName;
    @Input() tileAssetInfo;
    @Input() zoneOrder;
    @Input() zoneTitle;
    @Input() elementPosition;
    @Input() action;
    @Input() inputType;
    @Input() modal;
    @Input() actionSource;
    @Input() userPathScreenPosition;
    @Input() bannerInd;
    @Input() consumedPerc;
    @Input() skipConsumedPerc: boolean = false;
    @Input() tileContentType;
    @Input() tileContentSubType;
    @Input() findingMethod: string;
    @Input() newFavoriteIndex: number;
    @Input() skipActionSource: boolean = false;
    @Input() edp: string;
    @Input() skipScreenName: boolean = false;
    @Input() searchTerm: string = null;
    @Input() numSearchResults: number;
    @Input() skipNpData: boolean = false;
    @Input() isTile: boolean = false;
    @Input() skipCurrentPosition: boolean = false;
    @Input() messageType: string;
    @Input() toast: string;
    @Input() skipContentSource: boolean;
    @Input() skipOrientation: boolean;
    @Input() overlayName: string;
    @Input() pageName: string;
    @Input() shim: string;
    @Input() mercuryEvent: string;
    @Input() perfVal1: string;
    @Input() collectionGuid: string;

    public activeUrl: string = null;
    private subscriptions: Array<ISubscription> = [];

    constructor(element: ElementRef,
                private analyticsService: SxmAnalyticsService,
                private store: Store<IAppStore>,
                private router: Router)
    {
        this.store.select(selectNowPlayingState).subscribe((nowPlayingData : INowPlayingStore) =>
        {
            this.analyticsService.updateNowPlayingData(nowPlayingData);
        });
        this.setActiveScreenName();
    }

    ngOnInit()
    {
        if(this.tagName === AnalyticsTagNameConstants.MSG_TOAST)
        {
            this.doAnaCall();
            this.tagName = null;
        }
    }

    /**
        Handle the click event and not input field
     */
    @HostListener('click', ['$event']) onClick($event)
    {
        if($event && $event.target.tagName !== "INPUT")
        {
            this.handleEnterActions($event);
        }
    }

    /**
     Handle the input enter event
     */
    @HostListener('focus', ['$event']) onEnter($event)
    {
        // to handle event for Core Navbar Search button click only
        if ($event.target.tagName === "INPUT" && !$event.target.value && this.tagName === AnalyticsCoreNavBarTags.CNB_SEARCH_SHORTNAME)
        {
            const isChrome = BrowserUtil.isChrome();
            if(!isChrome || (isChrome && $event.sourceCapabilities))
            {
                this.handleEnterActions($event);
            }
        }
    }

    @HostListener('keyup.enter', ['$event']) onKeyEnter($event)
    {
        // to handle event for search input field on search triggered only
        if($event.target.tagName === "INPUT" && $event.target.value && this.tagName === AnalyticsTagNameConstants.SEARCH_INPUT)
        {
            this.handleEnterActions($event);
        }
    }

    @HostListener('input', ['$event']) onInput($event)
    {
        // to handle event for all channels filter input when each character entered/changed only
        if($event.target.tagName === "INPUT" && this.tagName === AnalyticsTagNameConstants.CAT_FILTER_INPUT)
        {
            this.searchTerm = $event.target.value;
            this.handleEnterActions($event);
        }
    }

    /**
     Handle the drag drop event
     */
    @HostListener('dragend', ['$event']) onDrop($event)
    {
            this.handleEnterActions($event);
    }

    private handleEnterActions(event)
    {
        /**
         * simple log to the console for debugging
         */
        SxmNewAnalyticsDirective.logger.debug(event);

        /**
         Handle Tile tags on different pages(Favorites, Recents, Search Results and other).
         */
        if(this.isTile)
        {
            this.setTileAnaProps(this.activeUrl);
        }

        if(this.tagName === AnalyticsTagNameConstants.VIEW_ALL)
        {
            this.zoneTitle = this.zoneTitle || this.getZoneNameAnaProp();
        }

        if(this.tagName === AnalyticsTagNameConstants.SEARCH_INPUT || this.tagName === AnalyticsTagNameConstants.SEARCH_RECENT)
        {
            this.store.select(selectSearchCarousel).pipe(
                skip(1),
                filter(carouselData => carouselData != null),
                take(1)
            ).subscribe(carouselData =>
            {
                let searchResultsCount = 0;
                carouselData.zone.forEach(zone =>
                {
                    zone.content.forEach(carousel =>
                    {
                        searchResultsCount += carousel.tiles.length;
                    });
                });
                this.numSearchResults = searchResultsCount;
                this.doAnaCall();
            });
            return;
        }

        this.doAnaCall();
    }

    private doAnaCall()
    {
        /**
         Pass the analytics tag to the analytics service.
         */
        if(this.tagName)
        {
            this.analyticsService.logAnalyticsTag({
                type: this.tagType,
                name: this.tagName,
                text: this.tagText,
                carouselPosition: this.carouselPosition,
                carouselName: this.carouselName,
                pageFrame: this.pageFrame,
                elementType: this.elementType,
                tileType: this.tileType,
                screen: this.screen ? this.screen : this.activeUrl,
                userPath: this.userPath,
                buttonName: this.buttonName,
                linkName: this.linkName,
                tileAssetInfo: this.tileAssetInfo,
                zoneTitle: this.zoneTitle,
                zoneOrder: this.zoneOrder,
                elementPosition: this.elementPosition,
                action: this.action,
                inputType: this.inputType,
                modal: this.modal,
                actionSource: this.actionSource,
                userPathScreenPosition: this.userPathScreenPosition,
                bannerInd: this.bannerInd,
                consumedPerc: this.consumedPerc,
                tileContentType: this.tileContentType,
                findingMethod: this.findingMethod,
                newFavoriteIndex: this.newFavoriteIndex,
                skipActionSource: this.skipActionSource,
                edp: this.edp,
                skipScreenName: this.skipScreenName,
                searchTerm: this.searchTerm,
                numSearchResults: this.numSearchResults,
                skipNpData: this.skipNpData,
                skipCurrentPosition: this.skipCurrentPosition,
                skipConsumedPerc: this.skipConsumedPerc,
                messageType: this.messageType,
                toast: this.toast,
                skipContentSource: this.skipContentSource,
                skipOrientation: this.skipOrientation,
                overlayName: this.overlayName,
                pageName: this.pageName,
                shim: this.shim,
                mercuryEvent: this.mercuryEvent,
                perfVal1: this.perfVal1,
                collectionGuid: this.collectionGuid
            });
        }
    }

    /**
    * method to set the current screen name (url).
    * */
    private setActiveScreenName()
    {
        this.activeUrl = this.router.url;
        this.subscriptions.push(
            this.router.events.pipe(
                filter(event => event instanceof NavigationEnd),
                take(1))
                .subscribe((event: NavigationEnd) =>
                {
                    this.activeUrl = event.url;
                }),
            this.router.events.pipe(
                filter(event => event instanceof NavigationEnd),
                pairwise())
                .subscribe((events: [ NavigationEnd, NavigationEnd ]) =>
                {
                    this.activeUrl = events[ 1 ].url;
                })
        );
    }

    /* Tile Ana Props */
    private setTileAnaProps(currentPage)
    {
        const isFavoritesPage = currentPage.includes(appRouteConstants.FAVORITES);
        const isRecentsPage = currentPage.includes(appRouteConstants.RECENTLY_PLAYED);
        const isSearchResultsPage = currentPage.includes(appRouteConstants.SEARCH);

        const isShowTile = this.tileType.toLowerCase() === CarouselTypeConst.SHOW_TILE;
        const isCategoryTile = this.tileContentType === CarouselTypeConst.CATEGORY_TILE;
        const isCollectionTile = this.tileContentType === CarouselTypeConst.COLLECTION;
        if (isSearchResultsPage)
        {
            const isShowListView = this.tileContentType === CarouselTypeConst.SHOW_TILE &&
                        this.tileContentSubType !== CarouselTypeConst.FUTURE_AUDIO_TYPE &&
                        this.tileContentSubType !== CarouselTypeConst.BUFFER_AUDIO_TYPE;

            if(isCategoryTile || isCollectionTile || isShowListView)
            {
                /* Search Results list view tiles */
                this.tagName = AnalyticsTagNameConstants.SRCH_RETURN_RESULTS;
                this.userPath = `${AnalyticsUserPaths.SRCH_RETURN_RESULTS}_${this.carouselName}`;
                this.buttonName = AnalyticsTagNameConstants.SRCH_RETURN_RESULTS_BUTTON_NAME;
                this.findingMethod = null;
            }
            else
            {
                /* Search Results direct tunable */
                this.tagName       = AnalyticsTagNameConstants.SRCH_LAUNCH;
                this.userPath      = `${AnalyticsUserPaths.SRCH_LAUNCH}_${this.carouselName}`;
                this.buttonName    = AnalyticsTagNameConstants.SRCH_LAUNCH_BUTTON_NAME;
                this.findingMethod = AnalyticsFindingMethods.SEARCH;
            }
            this.bannerInd = this.bannerInd || AnalyticsBannerInds.UNAVAILABLE;
            this.pageFrame = null;
            this.zoneTitle = null;
            this.zoneOrder = null;
            this.carouselPosition = null;
            this.consumedPerc = null;
            this.skipConsumedPerc = true;
        }
        else if(isFavoritesPage)
        {
            const isFavMoveTile = Number.isInteger(this.newFavoriteIndex);

            if(isFavMoveTile)
            {
                this.tagName    = AnalyticsTagNameConstants.FAV_MOVE_TILE;
                this.userPath   = AnalyticsUserPaths.FAV_MOVE_TILE;
                this.buttonName = AnalyticsTagNameConstants.FAV_MOVE_TILE_BUTTON_NAME;
                this.action     = AnalyticsTagActionConstants.DRAG;
            }
            else if(this.tileType.toLowerCase() === CarouselTypeConst.SHOW_TILE)
            {
                this.tagName    = AnalyticsTagNameConstants.SHOW_EPISODES_TILE;
                this.userPath   = AnalyticsUserPaths.SHOE_EPISODES_TILE;
                this.buttonName = AnalyticsTagNameConstants.SHOW_EPISODES_TILE_BUTTON_NAME;
                this.action     = AnalyticsTagActionConstants.BUTTON_CLICK;
                this.userPathScreenPosition = 2;
                this.tileContentType = this.tileContentSubType;
                this.zoneTitle = this.zoneTitle || this.getZoneNameAnaProp();
            }
            else
            {
                this.tagName    = AnalyticsTagNameConstants.FAV_TILE;
                this.userPath   = AnalyticsUserPaths.FAV_TILE;
                this.buttonName = AnalyticsTagNameConstants.FAV_TILE_BUTTON_NAME;
                this.action     = AnalyticsTagActionConstants.BUTTON_CLICK;
                this.findingMethod  = AnalyticsFindingMethods.FAVORITES;
            }
            this.carouselName   = AnalyticsCarouselNames.FAVORITES_CAROUSEL;
            this.pageFrame      = AnalyticsPageFrames.PAGE;
            this.elementType    = AnalyticsElementTypes.BUTTON;
            this.actionSource   = AnalyticsTagActionSourceConstants.MAIN;
            this.inputType      = AnalyticsInputTypes.MOUSE;
            this.skipConsumedPerc = true;
        }
        else if(isRecentsPage)
        {
            /* Recents Page Tiles */
            this.tagName = AnalyticsTagNameConstants.RCNT_TILE;
            this.userPath = AnalyticsUserPaths.RCNT_TILE;
            this.carouselName = AnalyticsCarouselNames.RECENT_CAROUSEL;
            this.buttonName = AnalyticsTagNameConstants.RCNT_TILE_BUTTON_NAME;
            this.inputType = AnalyticsInputTypes.MOUSE;
            this.elementType = AnalyticsElementTypes.BUTTON;
            this.action = AnalyticsTagActionConstants.BUTTON_CLICK;
            this.findingMethod = AnalyticsFindingMethods.RECENT;
            this.userPathScreenPosition = 1;
        }
        else if(isShowTile)
        {
            this.tagName    = AnalyticsTagNameConstants.SHOW_EPISODES_TILE;
            this.userPath   = AnalyticsUserPaths.SHOE_EPISODES_TILE;
            this.userPathScreenPosition = 2;
            this.buttonName = AnalyticsTagNameConstants.SHOW_EPISODES_TILE_BUTTON_NAME;
            this.elementType = AnalyticsElementTypes.BUTTON;
            this.inputType      = AnalyticsInputTypes.MOUSE;
            this.action     = AnalyticsTagActionConstants.BUTTON_CLICK;
            this.carouselPosition = null;
            this.tileContentType = this.tileContentSubType;
            this.zoneTitle = this.zoneTitle || this.getZoneNameAnaProp();
            this.pageFrame = AnalyticsPageFrames.PAGE;
            this.skipConsumedPerc = true;
            this.bannerInd = null;
        }
        else if(isCategoryTile || isCollectionTile)
        {
            this.tagName = AnalyticsTagNameConstants.ADDTNL_CONTENT_TILE;
            this.userPath = `${AnalyticsUserPaths.ADDTNL_CONTENT_TILE}_${this.carouselName}`;
            this.userPathScreenPosition = 2;
            this.pageFrame = AnalyticsPageFrames.PAGE;
            this.buttonName = AnalyticsTagNameConstants.ADDTNL_CONTENT_TILE_BUTTON_NAME;
            this.action = AnalyticsTagActionConstants.BUTTON_CLICK;
            this.elementType = AnalyticsElementTypes.BUTTON;
            this.inputType = AnalyticsInputTypes.MOUSE;
            this.skipActionSource = true;
            this.skipConsumedPerc = true;
            this.bannerInd = this.bannerInd || AnalyticsBannerInds.UNAVAILABLE;
            this.zoneTitle = this.zoneTitle || this.getZoneNameAnaProp();
            this.collectionGuid = this.collectionGuid || undefined;
        }
        else
        {
               /* All the tiles in the content carousel */
               this.tagName = AnalyticsTagNameConstants.CAROUSEL_TILE;
               this.userPath = `${AnalyticsUserPaths.CAROUSEL_TILE}_${this.carouselName}`;
               this.userPathScreenPosition = 2;
               this.pageFrame = AnalyticsPageFrames.PAGE;
               this.buttonName = AnalyticsTagNameConstants.CAROUSEL_TILE_BUTTON_NAME;
               this.action = AnalyticsTagActionConstants.BUTTON_CLICK;
               this.elementType = AnalyticsElementTypes.BUTTON;
               this.inputType = AnalyticsInputTypes.MOUSE;
               this.skipActionSource = true;
               this.findingMethod = AnalyticsFindingMethods.CAROUSEL;
               this.bannerInd = this.bannerInd || AnalyticsBannerInds.UNAVAILABLE;
               this.zoneTitle = this.zoneTitle || this.getZoneNameAnaProp();
        }

        // skip current position value for all the tile tags
        this.skipCurrentPosition = true;
    }

    getZoneNameAnaProp(): string
    {
        if(this.activeUrl.includes(appRouteConstants.HOME_FORYOU))
        {
            return AnalyticsZoneNames.DASHBOARD;
        }
        else if (this.activeUrl.includes(appRouteConstants.HOME_MUSIC) || this.activeUrl.includes(`${appRouteConstants.CATEGORY}/${appRouteConstants.MUSIC}`))
        {
            return AnalyticsZoneNames.MUSIC;
        }
        else if (this.activeUrl.includes('home/sports') || this.activeUrl.includes(`${appRouteConstants.CATEGORY}/${appRouteConstants.SPORTS}`))
        {
            return AnalyticsZoneNames.SPORTS;
        }
        else if (this.activeUrl.includes('home/news') || this.activeUrl.includes(`${appRouteConstants.CATEGORY}/${appRouteConstants.NEWS}`))
        {
            return AnalyticsZoneNames.NEWS;
        }
        else if (this.activeUrl.includes('home/entertainment') || this.activeUrl.includes(`${appRouteConstants.CATEGORY}/${appRouteConstants.ENTERTAINMENT}`))
        {
            return AnalyticsZoneNames.TALKS;
        }
        else if (this.activeUrl.includes(appRouteConstants.HOME_HOWARD))
        {
            return AnalyticsZoneNames.HOWARD;
        }
        return AnalyticsZoneNames.UNKNOWN;
    }
}
