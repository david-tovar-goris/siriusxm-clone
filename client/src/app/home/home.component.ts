import { of as observableOf, Observable, SubscriptionLike as ISubscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApplicationRef, Component, Inject, OnInit } from "@angular/core";
import { ChangeDetectionStrategy } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute, Params, NavigationExtras } from "@angular/router";
import {
    ICategory,
    ISuperCategory,
    Logger,
    ICarouselDataByType,
    ISettings,
    ISetting,
    IAppConfig,
    IBaseCategory,
    ConnectivityService,
    SettingsConstants,
    SettingsService,
    INeriticLinkData,
    neriticActionConstants,
    IZoneDataByType,
    AuthenticationService,
    relativeUrlToAbsoluteUrl,
    ConfigService,
    ConfigConstants,
    SxmAnalyticsService,
    EPageName
} from "sxmServices";
import { appRouteConstants } from "../app.route.constants";
import { AutoUnSubscribe } from "../common/decorator/auto-unsubscribe";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { ChannelListStoreService } from "../common/service/channel-list.store.service";
import { displayViewType } from "app/channel-list/component/display.view.type";
import { IChannelListStore } from "../common/store/channel-list.store";
import { NavigationService } from "../common/service/navigation.service";
import { ProgressCursorService } from "../common/service/progress-cursor/progress-cursor.service";
import { IModalData } from "../common/service/modal/modal.interface";
import { ModalService } from "../common/service/modal/modal.service";
import { IAppStore } from "../common/store/app.store";
import { Store } from "@ngrx/store";
import { TranslateService } from "@ngx-translate/core";
import { NeriticLinkService } from "../neritic-links";
import { APP_CONFIG } from "../sxmservicelayer/sxm.service.layer.module";
import { selectSuperCategoryCarousel } from "../common/store/carousel.store";
import { OpenAccessOverlayService } from "app/open-access/popups/open-access-overlay/open-access-overlay.service";
import { overlayConsts } from "app/profile/profile.const";
import { ColdStartOverlayService } from "app/open-access/popups/cold-start-overlay/cold-start-overlay.service";

import {
    AnalyticsCarouselNames,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsPageNames
} from "app/analytics/sxm-analytics-tag.constants";

/**
 * @MODULE:     client
 * @CREATED:    07/11/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     HomeComponent used to load home page
 */

@Component({
    templateUrl: "./home.component.html",
    styleUrls: [ "./home.component.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})

@AutoUnSubscribe()
export class HomeComponent implements OnInit
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsPageNames = AnalyticsPageNames;

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("HomeComponent");

    /**
     * Used to determine whether in cold start state or default. Used for PageName in analytics calls.
     */
    public coldStartDefinition: number;

    /**
     * Used to determine when to show a default home screen or curated "cold start" home screen. 5 is the threshold.
     */
    public userDaysListened: number;

    /**
     * Used as default selected super category from list and load categories based on this value.
     */
    public selectedSuperCategory: ISuperCategory = {} as ISuperCategory;

    /**
     * List of channels wrapped in observables from the NGRX channels store. The Observable
     * wrapping will need to be stripped for use in the UI via the async pipe; e.g., "channels | async".
     */
    public homeSuperCategoryAnalyticProps: {};

    public homeCategoryAnalyticProps: {};

    private subscriptions: Array<ISubscription> = [];

    public isHoward: boolean;
    public showFooter: boolean = SettingsConstants.SHOW_FOOTER;

    /**
     * Stores the active super category from the active route params observable
     */
    private activeSuperCategory: string;

    /**
     * Stores the active super category data, so it can be quickly accessed.
     */
    public currentSuperCategoryCarouselData: ICarouselDataByType = {} as ICarouselDataByType;

    /**
     * globalSettings from settingsService.settings Observable.
     * And used to extract setting values from this list.
     */
    private globalSettings: Array<ISetting> = [];

    /**
     * deviceSettings from settingsService.settings Observable.
     * And used to extract setting values from this list.
     */
    private deviceSettings: Array<ISetting> = [];

    /**
     * Used to check whether DisplayAICOverlay is On/Off
     */
    public isDisplayAICOverlayOn: boolean = true;

    /**
     * Used to check whether DisplayColdStartOverlay is On/Off
     */
    public isDisplayColdStartOverlayOn: boolean = true;

    /**
     * Used to check whether to display openAccessOverlay or not
     */
    public displayOpenAccessOverlay: any;

    /**
     * emits truthy when no connectivity toast should be shown.
     */
    public displayNoConnectivity$: Observable<boolean> = observableOf(false);

    /**
     * Observable for channelStore.
     */
    public channelListStore$: Observable<IChannelListStore> = this.store.select('channelStore');

    /**
     *  Observable for the supercategory carousels.
     */
    public superCategoryCarousels$: Observable<ICarouselDataByType> = this.store.select(selectSuperCategoryCarousel);

    public pageName: string;

    /**
     * Constructor
     * @param activeRoute used to inpect the routing information for the app
     * @param carouselStoreService used to observe changes to the active carousel
     * @param channelListStoreService is used to observe changes to the channel lineup
     * @param location is currently not used and shopuld be removed
     * @param navigationService is used to change routes
     * @param progressCursorService is used to update the progress cursor around carousel loading
     * @param connectivityService is used to determine when to show the loss of connectivity toast
     * @param settingsService is used to determine if we need to display the AIC intro overlay.
     * @param modalService is used to display the Ihe AIC intro overlay
     * @param store is used to get info out of the store.
     * @param translate is used in the template for i8n purposes.
     * @param appConfig is the injected application configuration object
     * @param {ConfigService} configService
     */
    constructor(private activeRoute: ActivatedRoute,
                public carouselStoreService: CarouselStoreService,
                private channelListStoreService: ChannelListStoreService,
                private location: Location,
                private navigationService: NavigationService,
                public progressCursorService: ProgressCursorService,
                private sxmAnalyticsService: SxmAnalyticsService,
                public connectivityService: ConnectivityService,
                private settingsService: SettingsService,
                private neriticLinkService: NeriticLinkService,
                private modalService: ModalService,
                private store: Store<IAppStore>,
                private translate: TranslateService,
                private authenticationService : AuthenticationService,
                @Inject(APP_CONFIG) public appConfig: IAppConfig,
                private openAccessOverlayService : OpenAccessOverlayService,
                private applicationRef: ApplicationRef,
                private configService: ConfigService,
                private coldStartOverlayService: ColdStartOverlayService)
    {
        this.channelListStore$ = this.store.select('channelStore');

        this.subscriptions.push(
            this.superCategoryCarousels$
                .subscribe((carouselData: ICarouselDataByType) =>
                {
                    this.currentSuperCategoryCarouselData = carouselData;
                    this.progressCursorService.stopSpinning();

                    this.coldStartDefinition = carouselData &&
                        carouselData.pageAssetInfo &&
                        carouselData.pageAssetInfo.coldStartDefinition ?
                        parseInt(carouselData.pageAssetInfo.coldStartDefinition) : 0;

                    this.userDaysListened = carouselData &&
                        carouselData.pageAssetInfo &&
                        carouselData.pageAssetInfo.userDaysListened ?
                        parseInt(carouselData.pageAssetInfo.userDaysListened) : 0;

                    this.sxmAnalyticsService.userDaysListened = this.userDaysListened;

                    this.pageName = this.userDaysListened > this.coldStartDefinition
                        ? EPageName.DEFAULT : EPageName.COLD_START;

                    this.loadColdStartOverlaySettings();

                    if((this.userDaysListened === this.coldStartDefinition
                            && this.coldStartDefinition === ConfigConstants.USER_DAYS_LISTENED_DEFAULT_THRESHOLD)
                            && this.isDisplayColdStartOverlayOn)
                    {
                        this.displayColdStartOverlay();
                    }
                })
        );

        this.loadAICOverlaySettings();

        this.displayNoConnectivity$ =
            this.connectivityService.isNoConnectionSubject.pipe(
                map(isNoConnection =>
                {
                    return isNoConnection
                        && this.carouselDataIsMissing(this.currentSuperCategoryCarouselData);
                })) as any as Observable<boolean>;

    }

    /**
     * ngOnInit - Initialize the component.
     */
    public ngOnInit(): void
    {
        HomeComponent.logger.debug("ngOnInit( Subscribing to Channels Store. )");

        this.activeRoute.params.
            subscribe((params: Params) =>
            {
                if(!params['superCat'])
                {
                    const debugMode = this.activeRoute.snapshot.queryParams['debug'] === 'true';
                    const debugParam = {
                        debug: true
                    };
                    const extras: NavigationExtras = {
                        queryParamsHandling: ""
                    };

                    if(debugMode)
                    {
                        extras['queryParams'] = debugParam;
                    }

                    this.navigationService.go([appRouteConstants.HOME,
                                               this.appConfig.defaultSuperCategory.key],
                                              extras);
                }
                else
                    {
                        this.activeSuperCategory = params['superCat'] ? params['superCat'] : this.appConfig.defaultSuperCategory.key;
                        this.isHoward = params['superCat'] === 'howard';
                    }
            });

        const channelStoreResult = (channelListStore: IChannelListStore) =>
        {
            this.selectedSuperCategory = channelListStore.selectedSuperCategory;
        };

        const channelStoreFault = (fault: any) =>
        {
            HomeComponent.logger.warn(`channelStoreFault( Channel lineup has ${fault} channels )`);
        };

        this.subscriptions.push(
            this.channelListStore$
                .subscribe(channelStoreResult, channelStoreFault),
            this.activeRoute.data
                .subscribe(({ categoryData }) =>
                {
                    this.channelListStoreService.selectSuperCategory(categoryData);

                   // Display AIC Overlay only when the user access music supercategory page
                    if( categoryData && categoryData.key === appRouteConstants.MUSIC)
                    {
                        this.displayAICOverlay();
                    }
                })
        );

         this.displayOpenAccessOverlay = window.sessionStorage.getItem(overlayConsts.DISPLAY_OA_OVERLAY)
                                         ? window.sessionStorage.getItem(overlayConsts.DISPLAY_OA_OVERLAY)
                                         : this.authenticationService.isOpenAccessEligible();

        // Show Open Access Overlay
        if ((this.displayOpenAccessOverlay === true || this.displayOpenAccessOverlay == "true")
                && !this.appConfig.isFreeTierEnable)
        {
            this.openAccessOverlayService.open(this.applicationRef.components[0].instance.viewContainerRef);
        }

    }

    private displayColdStartOverlay()
    {
        this.coldStartOverlayService.open(this.applicationRef.components[0].instance.viewContainerRef);
    }

    /**
     * Calls ChannelListStoreService to get the list of categories for the selected super category.
     * @param {ISuperCategory} superCategory is selected super category.
     */
    public selectSuperCategory(superCategory: ISuperCategory, $event: any)
    {
        HomeComponent.logger.debug(`selectSuperCategory( "${ superCategory.name }", ID = ${ superCategory.categoryGuid } )`);
        this.progressCursorService.startSpinning();

        this.navigationService.go([
            appRouteConstants.HOME,
            superCategory.key
        ], { queryParamsHandling: "merge" });
    }

    /**
     * Calls the channel List Service to get channels and route to the category component.
     * @param {ICategory} category is selected category.
     */
    public selectCategory(category: ICategory): void
    {
        HomeComponent.logger.debug(`selectCategory( "${ category.name }", ID = ${ category.categoryGuid } )`);

        const subCat = this.selectedSuperCategory.categoryList.find(c => c.key === category.key);

        this.progressCursorService.startSpinning();

        this.carouselStoreService.selectSubCategory(subCat);

        this.navigationService.go([
                appRouteConstants.CATEGORY,
                this.selectedSuperCategory.key,
                category.key,
                displayViewType.channels
            ], { queryParamsHandling: "merge" });
    }

    /**
     * Determine if a super category is the current, selected super category.
     * @param {ISuperCategory} superCategory
     * @returns {boolean}
     */
    public isSelectedSuperCategory(superCategory): boolean
    {
        return superCategory.key === this.activeSuperCategory;
    }

    /**
     * Returns the correct icon image for each category based on its name and width properties
     * @param {ICategory} category
     * @returns {string}
     */
    public getCategoryImage(category: ICategory): string
    {
        let image:any;

        if(category.imageList.some(image => image.name === "category icon web" && image.width === 80))
        {
            image = category.imageList.find(image => image.name === "category icon web" && image.width === 80);
        }
        else
        {
            image = category.imageList.find(image => image.name === "category icon" && image.width === 80);
        }
        const relativeUrl = this.configService.getRelativeUrlSettings();
        return (image && image.relativeUrl) ? relativeUrlToAbsoluteUrl(image.relativeUrl, relativeUrl) : "";
    }


    /**
     * returns boolean for whether or not carouselData is blankish.
     */
    public carouselDataIsMissing(carouselData: ICarouselDataByType): boolean
    {
        if (carouselData.error) return true;
        return !carouselData.zone
            || !carouselData.zone.length
            || !carouselData.zone[0].hero
            || !carouselData.zone[0].hero.length
            || !carouselData.zone[0].hero[0].guid
            || !carouselData.zone[0].content
            || !carouselData.zone[0].content.length;
    }

    public trackCategory(index, category: IBaseCategory)
    {
        return (category) ? category.categoryGuid : index;
    }

    /**
     * Load AIC Overlay settings on creation of component
     */
    private loadAICOverlaySettings()
    {
        const settingsSubscription = this.settingsService.settings.subscribe((response: ISettings) =>
        {
            if (response)
            {
                this.deviceSettings = response.deviceSettings;
                this.globalSettings = response.globalSettings;
                this.isDisplayAICOverlayOn = this.settingsService.isGeneralSettingOn(SettingsConstants.DISPLAY_AIC_OVERLAY, true);
            }
        });
        this.subscriptions.push(settingsSubscription);
    }

    /**
     * Load Cold Start Overlay settings on creation of component
     */
    private loadColdStartOverlaySettings()
    {
        const settingsSubscription = this.settingsService.settings.subscribe((response: ISettings) =>
        {
            if (response)
            {
                this.deviceSettings = response.deviceSettings;
                this.globalSettings = response.globalSettings;
                this.isDisplayColdStartOverlayOn = this.settingsService.isGeneralSettingOn(SettingsConstants.DISPLAY_COLD_START_OVERLAY, true);
            }
        });
        this.subscriptions.push(settingsSubscription);
    }

    /**
     * opens modal with AIC promo modal
     */
    private displayAICOverlay() : void
    {
        if (this.isDisplayAICOverlayOn && this.appConfig.deviceInfo.supportsAddlChannels)
        {
            const modalText = this.translate.instant("aic.aicOverlay");
            modalText.buttonOne.action = this.displayAllAICChannels.bind(this);
            modalText.buttonTwo.action = this.updateAICOverlaySetting.bind(this);

            let hasAIC = false;
            this.modalService.modalComponents.forEach( (component) =>
            {
                if(!!component.instance.modalData.altText.aicPromoImage)
                {
                    hasAIC = true;
                }
            });
            if(!hasAIC)
            {
                this.openModal(modalText);
            }
        }
    }

    private displayAllAICChannels(): void
    {
        // TODO CWC is there a better url to use for the ALL Xtra Channels collection?
        const neriticLinkData: INeriticLinkData = {
            actionType: "carousel?page_name=channels_all&function=onlyAdditionalChannels",
            contentType: neriticActionConstants.ALL_ADDITIONAL_CHANNELS,
            url: "page-name=channels_all&function=onlyAdditionalChannels"
        };
        this.neriticLinkService.navigateNeriticAction(neriticLinkData);
        this.updateAICOverlaySetting();
    }

    /**
     * Update the DisplayAICOverlay setting to on/off
     */
    private updateAICOverlaySetting(): void
    {
        this.settingsService.switchGlobalSettingOnOrOff(false, SettingsConstants.DISPLAY_AIC_OVERLAY);
    }

    /**
     * Update the DisplayColdStartOverlay setting to on/off
     */
    private updateColdStartOverlaySetting(): void
    {
        this.settingsService.switchGlobalSettingOnOrOff(false, SettingsConstants.DISPLAY_COLD_START_OVERLAY);
    }

    /**
     * Set up data elements for AIC promo modal
     * @param {IModalData} modalData
     */
    private openModal(modalData: IModalData): void
    {
        modalData.logo = {
            url :"../../../../assets/images/sxm-logo.png",
            altText: "aic.aicOverlay.altText.sxmLogo"
        };
        modalData.promoImage = {
            url :"../../../../assets/images/aic-promo.svg",
            altText: "aic.aicOverlay.altText.aicPromoImage"
        };
        this.modalService.addDynamicModal(modalData);
    }

    /**
     * will navigate to repective neritic link if it has any
     * @param {zone} modalData
     */
    public goToZoneLink(zone:IZoneDataByType)
    {
        if(zone.neriticLinkData)
        {
            this.neriticLinkService.navigateNeriticAction(zone.neriticLinkData);
        }
    }
}
