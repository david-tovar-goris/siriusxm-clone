import { of as observableOf } from "rxjs";

import { IAppConfig, InitializationService, ISuperCategory } from "sxmServices";

import { HomeResolver } from "./home-resolver";
import { ChannelListServiceMock } from "../../../test/mocks";
import { SplashScreenServiceMock } from "../../../test/mocks/splash-screen.service.mock";

describe("HomeResolver", () =>
{
    let homeResolver: HomeResolver,
        mockInitService: InitializationService = ({ initState: observableOf("running") }) as any,
        mockChannelService = ChannelListServiceMock.getSpy(),
        mockAppConfig = {
            defaultSuperCategory :
                {
                    key:"defaultKey"
                } as ISuperCategory
        } as IAppConfig;

    beforeEach(() =>
    {
        homeResolver = new HomeResolver(mockChannelService, mockInitService, SplashScreenServiceMock.getSpy(), mockAppConfig);
    });

    describe("resolve()", () =>
    {
        it("should return a supercategory when the app is initialized", () =>
        {
            const superCat = { key: "music" };
            let result;

            mockChannelService.channelStore = observableOf({ superCategories: [ superCat ] });

            homeResolver
                .resolve(({ params: { superCat: "music" } }) as any, null)
                .subscribe(val => result = val);

            expect(result).toEqual(superCat);
        });
    });
});
