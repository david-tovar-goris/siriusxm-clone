/// <reference path="../../../node_modules/@types/jasmine/index.d.ts" />

import * as _                      from "lodash";
import { NO_ERRORS_SCHEMA }        from "@angular/core";
import {
    async,
    ComponentFixture,
    inject,
    TestBed
}                                  from "@angular/core/testing";
import { Location }                from "@angular/common";
import { ActivatedRoute }          from "@angular/router";
import {
    ChannelListServiceMock,
    createCategory
}                                  from "../../../test/mocks/channel-list.service.mock";
import { NeriticLinkServiceMock }    from "../../../test/mocks/neritic-link.service.mock";
import { AuthenticationModule }    from "../auth/authentication.module";
import { ChannelListStoreService } from "../common/service/channel-list.store.service";
import { NeriticLinkService }      from "../neritic-links";
import { SXMServiceLayerModule }   from "../sxmservicelayer/sxm.service.layer.module";
import { TranslationModule }       from "../translate/translation.module";
import { HomeComponent }           from "./home.component";
import { SharedModule }            from "../common/shared.module";
import { OverlayService }          from "../common/service/overlay/overlay.service";
import { CarouselServiceMock }     from "../../../test/mocks/carousel.service.mock";
import { AuthenticationService, CarouselService, SettingsService, SxmAnalyticsService } from "sxmServices";
import { of as observableOf } from "rxjs";
import { appRouteConstants } from "app/app.route.constants";
import { displayViewType } from "app/channel-list/component/display.view.type";
import { MockComponent } from "../../../test/mocks/component.mock";
import { NavigationService } from "../common/service/navigation.service";
import { navigationServiceMock } from "../../../test/mocks/navigation.service.mock";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { ProgressCursorService } from "../common/service/progress-cursor/progress-cursor.service";
import { SettingsServiceMock } from "../../../test/mocks/settings.service.mock";
import { ModalService } from "../common/service/modal/modal.service";
import { ModalServiceMock } from "../../../test/mocks/modal.service.mock";
import { TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../test/mocks/translate.service";
import { channelsReducer } from "../common/reducer/channel-list.reducer";
import { Store, StoreModule } from '@ngrx/store';
import { carouselReducer } from "../common/reducer/carousel.reducer";
import { OpenAccessOverlayService } from "app/open-access/popups/open-access-overlay/open-access-overlay.service";
import { ApplicationRef } from "@angular/core";
import { ColdStartOverlayService } from "app/open-access/popups/cold-start-overlay/cold-start-overlay.service";

describe("HomeComponent", () =>
{
    let component: HomeComponent,
        location: Location,
        applicationRef,
        carouselStoreServiceMock,
        openAccessOverlayServiceMock,
        authenticationServiceMock,
        category = createCategory(),
        fixture: ComponentFixture<HomeComponent>,
        $event: any,
        mockActiveRoute = {
            params: observableOf({ superCat: 'howard' }),
            data: observableOf({ categoryData : category })
        },
        progressCursorService = {
            isSpinning: { subscribe: jasmine.createSpy('isSpinningSubscription') },
            startSpinning: jasmine.createSpy('startSpinning'),
            stopSpinning: jasmine.createSpy('stopSpinning')
        };

    beforeEach(async(() =>
    {
        // Create the mock channel lineup testSubject.
        carouselStoreServiceMock = {
            selectSubCategory: _.noop,
            selectSuperCategory : jasmine.createSpy("selectSuperCategory")
        };

        applicationRef = {
            components: [{ instance: { viewConainterRef: {} } }]
        };

        openAccessOverlayServiceMock = {
            open: jasmine.createSpy('open')
        };

        authenticationServiceMock = {
            userSession: observableOf({ authenticated: true }),
            isAuthenticated: jasmine.createSpy("isAuth").and.returnValue("isAuth"),
            logout: jasmine.createSpy('logout').and.returnValue(observableOf(true)),
            isOpenAccessEligible: jasmine.createSpy('isOpenAccessEligible').and.returnValue(observableOf(false))
        };

        let sxmAnalyticsServiceMock = {
            updateNowPlayingData: function() {},
            logAnalyticsTag: function() {},
            fireTerminalEvent: function() {}
        };

        let mockColdStartOverlayService = {};

        TestBed.configureTestingModule({
                imports: [
                    SXMServiceLayerModule,
                    AuthenticationModule,
                    TranslationModule,
                    SharedModule,
                    StoreModule.forRoot({
                        channelStore: channelsReducer,
                        carouselStore: carouselReducer
                    })
                ],
                declarations: [
                    HomeComponent,
                    MockComponent({ selector: "howard-show-listing"}),
                    MockComponent({ selector: "all-channels-btn"})
                ],
                schemas: [ NO_ERRORS_SCHEMA ],
                providers: [
                    { provide: ActivatedRoute, useValue: mockActiveRoute },
                    { provide: CarouselService, useValue: CarouselServiceMock.getSpy() },
                    { provide: CarouselStoreService, useValue : carouselStoreServiceMock },
                    { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                    { provide: Location, useValue: location },
                    { provide: NavigationService, useValue: navigationServiceMock },
                    { provide: NeriticLinkService, useValue: NeriticLinkServiceMock },
                    { provide: OverlayService, useClass: OverlayService },
                    { provide: ProgressCursorService, useValue: progressCursorService },
                    { provide: SettingsService, useValue: SettingsServiceMock.getSpy()},
                    { provide: ModalService, useValue: ModalServiceMock},
                    { provide: TranslateService, useValue: MockTranslateService.getSpy()},
                    { provide: AuthenticationService, useValue: authenticationServiceMock },
                    { provide: ApplicationRef, useValue: applicationRef },
                    { provide: OpenAccessOverlayService, useValue:openAccessOverlayServiceMock },
                    { provide: ColdStartOverlayService, useValue: mockColdStartOverlayService },
                    { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock }
                ]
            })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        this.store = TestBed.get(Store);
        $event = { target : { blur : () => {}}};
        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });


    describe("getCategoryImage()", () =>
    {
        it("should return the url with width 80 and name 'category icon on light'", () =>
        {
            const correctImg = { name: "category icon", width: 80, url: "correct/url", relativeUrl: "correct/url"},
                  wrongImg = { name: "background image", width: 350, url: "bad/url", relativeUrl: "bad/url"};

            category.imageList.push(correctImg);
            category.imageList.push(wrongImg);

            expect(component.getCategoryImage(category)).toEqual(correctImg.relativeUrl);
        });
    });

    describe("selectSuperCategory()", () =>
    {
       it("should navigate to home/:superCatKey so the component can set the selected super category in the store",
           inject([ NavigationService ], (navigationService: NavigationService) =>
       {
            const superCat = { key: "music" } as any;
            spyOn(navigationService, "go");

            component.selectSuperCategory(superCat, $event);

            expect(navigationService.go).toHaveBeenCalledWith([
                appRouteConstants.HOME,
                superCat.key
            ], { queryParamsHandling: "merge" });
       }));

       it("should call progressCursorService.startSpinning", () =>
       {
           const superCat = { key: "music" } as any;
           component.selectSuperCategory(superCat, $event);
           expect(progressCursorService.startSpinning).toHaveBeenCalled();
       });
    });

    describe("selectCategory()", () =>
    {
        it("should navigate to the category-listing/:subCategory/:superCategory to display the category component",
            inject([ NavigationService ], (navigationService: NavigationService) =>
        {
            const subCat = { key: "pop" } as any;
            spyOn(navigationService, "go");

            component.selectedSuperCategory = { key: "music", categoryList: [] } as any;
            component.selectCategory(subCat);

            expect(navigationService.go).toHaveBeenCalledWith([
                    appRouteConstants.CATEGORY,
                    component.selectedSuperCategory.key,
                    subCat.key,
                    displayViewType.channels
            ], { queryParamsHandling: "merge" });
        }));

        it("should call progressCursorService.startSpinning",
            inject([ NavigationService ], (navigationService: NavigationService) =>
            {
                const subCat = { key: "pop" } as any;
                spyOn(navigationService, "go");

                component.selectedSuperCategory = { key: "music", categoryList: [] } as any;
                component.selectCategory(subCat);

                expect(progressCursorService.startSpinning).toHaveBeenCalled();
            }));
    });

    describe("isSelectedSuperCategory()", () =>
    {
        describe("when the super category key matches the route", () =>
        {
            it("should return true", () =>
            {
                expect(component.isSelectedSuperCategory({key: 'howard'})).toEqual(true);
            });
        });

        describe("when the super category key does not match the route", () =>
        {
            it("should return false", () =>
            {
                expect(component.isSelectedSuperCategory({key: 'music'})).toEqual(false);
            });
        });
    });
});
