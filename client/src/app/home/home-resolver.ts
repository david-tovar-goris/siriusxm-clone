import { switchMap, map, skipWhile, first } from 'rxjs/operators';
import {
    Resolve,
    ActivatedRouteSnapshot,
    RouterStateSnapshot, ActivatedRoute
} from "@angular/router";

import { Inject, Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { ChannelListStoreService } from "../common/service/channel-list.store.service";

import {
    ISuperCategory,
    InitializationService,
    InitializationStatusCodes, IAppConfig
} from "sxmServices";
import { SplashScreenService } from "../common/service/splash-screen/splash-screen.service";
import { APP_CONFIG } from "../sxmservicelayer/sxm.service.layer.module";

@Injectable()
export class HomeResolver implements Resolve<ISuperCategory | {}>
{

   constructor(private channelListStoreSerivce: ChannelListStoreService,
               private initService: InitializationService,
               private splashScreenService: SplashScreenService,
               @Inject(APP_CONFIG) public appConfig: IAppConfig)
  {}

   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
   {
       return this.initService.initState.pipe(
                  skipWhile(initState =>
                  {
                      return initState !== InitializationStatusCodes.RUNNING ;
                  }), // Waits until the resume has completed
                  switchMap(() => resolveSuperCategory(this.channelListStoreSerivce,
                      route,
                      this.splashScreenService,
                      this.appConfig)),
                  first()); // Completes the observable and returns a value so Angular can resolve the route
   }
}

function resolveSuperCategory(cls: ChannelListStoreService,
                              route: ActivatedRouteSnapshot,
                              splashScreenService:SplashScreenService,
                              appConfig: IAppConfig): Observable<ISuperCategory>
{
    return cls.channelStore.pipe(
              skipWhile(store => store.superCategories.length === 0), // Waits for the supercategories to come back from ChannelLineup call
              map(({ superCategories }) =>
              {
                  splashScreenService.closeSplashScreen();
                  if(!route.params[ 'superCat' ])
                  {
                     return appConfig.defaultSuperCategory;
                  }
                  return superCategories.find(c => c.key === route.params[ 'superCat' ]);
              }));
}
