import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { CarouselModule } from "../carousel";
import { ChannelListModule } from "../channel-list/channel-list.module";
import { NavigationModule } from "../navigation/navigation.module";
import { TranslationModule } from "../translate/translation.module";
import { HomeComponent } from "./home.component";
import { SharedModule } from "../common/shared.module";
import { AllChannelsModule } from "../all-channels/all-channels.module";
import { ConnectivityModule } from "../connectivity/connectivity.module";
import { ContentTilesModule } from "../common/component/tiles/content-tiles.module";
import { IESupportBannerComponent } from "app/home/ie-support-banner.component";

@NgModule({
    imports: [
        CommonModule,
        ChannelListModule,
        NavigationModule,
        CarouselModule,
        TranslationModule,
        SharedModule,
        AllChannelsModule,
        ConnectivityModule,
        ContentTilesModule
    ],
    declarations: [
        HomeComponent,
        IESupportBannerComponent
    ],
    exports: [
        IESupportBannerComponent
    ],
    providers: []
})
export class HomeModule
{
}
