import { Component } from "@angular/core";
import { BrowserUtil } from "app/common/util/browser.util";

@Component({
    selector: 'ie-support-banner',
    template: `
        <div id="ie-message"
             class="ie-message stickier"
             sxm-sticky
             [stickyClass]="'sticky-bar'"
             [alwaysSticky]="true"
             *ngIf="showIEBanner">
            {{ 'landing.downloadEdge' | translate }}
            <a href="{{ 'landing.links.downloadEdge' | translate }}" target="_blank">
                {{ 'landing.msEdge' | translate }}
            </a>.
            <button class="close-btn" (click)="close()" alt="close button">
                <img src="../../../../assets/images/close-icon-white.svg"  alt="close icon"/>
            </button>
        </div>`,
    styleUrls: ["./ie-support-banner.component.scss"]
})

/**
 *  Component to show Internet Explorer users a message that IE will no longer be supported
 *  and provide a link to download the Edge browser.
 */
export class IESupportBannerComponent
{
    /**
     *  Flag to show the IE support message and download link for Edge
     */
    public showIEBanner: boolean;

    constructor()
    {
        this.showIEBanner = BrowserUtil.isInternetExplorer();
    }

    /**
     *  Remove the banner from the DOM and trigger scroll event to force DOM update
     */
    public close(): void
    {
        this.showIEBanner = false;

        // This forces IE to update the DOM after the element is removed.
        BrowserUtil.triggerVerticalScroll();
    }
}
