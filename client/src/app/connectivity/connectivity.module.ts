import { NoConnectivityComponent } from "./no-connectivity.component";
import { NgModule } from "@angular/core";
import { TranslationModule } from "../translate/translation.module";

@NgModule({
    imports: [
      TranslationModule
    ],
    exports: [
        NoConnectivityComponent
    ],
    declarations: [
        NoConnectivityComponent
    ]
})

export class ConnectivityModule { }
