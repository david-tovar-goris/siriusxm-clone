import { Component } from '@angular/core';

@Component({
    selector: 'no-connectivity',
    templateUrl: "./no-connectivity.component.html",
    styleUrls: ["./no-connectivity.component.scss"]
})

export class NoConnectivityComponent { }
