import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ContextMenuComponent } from "./context-menu.component";
import { ContextMenuService } from "./context-menu.service";
import { SharedModule } from "../common/shared.module";
import { TileContextMenuOptionsService } from "./options/tile-context-menu-options.service";
import { DoubleTapOpenContextMenu } from "./directives/double-tap-open-context-menu.directive";
import { TranslationModule } from "../translate/translation.module";

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        TranslationModule
    ],
    declarations: [
        ContextMenuComponent,
        DoubleTapOpenContextMenu
    ],
    exports: [
        ContextMenuComponent,
        DoubleTapOpenContextMenu
    ],
    providers: [
        ContextMenuService,
        TileContextMenuOptionsService
    ]
})
export class ContextMenuModule
{
}
