import { Injectable } from '@angular/core';
import { CMOption } from "./context-menu.interface";

@Injectable()
export class ContextMenuService
{

    /**
     * Flag for displaying or hiding context menu
     *
     * @type {boolean}
     * @memberof ContextMenuService
     */
    isContextMenuOpen: boolean;

    /**
     * Flag to disable the scroll event (Firefix 71 version caused to trigger scroll when not needed)
     */
    disableScrollEvent:boolean = false;

    /**
     * Provides contextMenuList with the text and corresponding action
     *
     * @type {*}
     * @memberof ContextMenuService
     */
    currentMenuOptions: CMOption[];

    /**
     * Used to calculate the placement and orientation of the contextMenuList component.
     * @type {Event}
     * @memberof ContextMenuService
     */
    event: Event;

    /**
     * Flag that can be set to true. Tells
     * context menu list to reopen itself after it has been
     * destroyed.
     */
    shouldReOpenOnDestroy: boolean = false;

    /**
     *
     * The options are used by the contextMenuList component to display text
     * and take an action when a list is clicked
     *
     * @param {string} [options]
     * @memberof ContextMenuService
     */
    public openContextMenu(options)
    {
        if(!options) return;
        this.currentMenuOptions = options;

        this.disableScrollEvent = true;

        if (this.isContextMenuOpen)
        {
            this.shouldReOpenOnDestroy = true;
            this.isContextMenuOpen = false;
        }
        else
        {
            this.isContextMenuOpen = true;
        }

        setTimeout(()=>
        {
            this.disableScrollEvent = false;
        },0);
    }

    /**
     * Closes the context menu
     *
     * @memberof ContextMenuService
     */
    public closeContextMenu()
    {
        if(!this.disableScrollEvent)
        {
            this.isContextMenuOpen = false;
        }
    }

    /**
     * Returns the context menu's open/close state
     *
     * @memberof ContextMenuService
     */
    public contextMenuIsOpen = (): boolean =>
    {
        return this.isContextMenuOpen;
    }

    /**
     *  Used to set the event and pass it to the contextMenuList component
     *
     * @param {any} event
     * @memberof ContextMenuService
     */
    public setEvent(event: Event)
    {
        this.event = event;
    }

    /**
     * @returns {Event}
     * @memberof ContextMenuService
     *
     * Returns event in order to calculate the where to place context menu
     */
    public getEvent(): Event
    {
        return this.event;
    }

    /**
     * Returns the options to the contextMenuList so it can display the correct text
     * and take appropriate actions when a list item is clicked
     * @returns {Array<CMOption>}
     * @memberof ContextMenuService
     */
    public getOptions(): CMOption[]
    {
        return this.currentMenuOptions;
    }
}
