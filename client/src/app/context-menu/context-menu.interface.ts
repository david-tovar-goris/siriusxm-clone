export interface CMOption
{
    ariaText: string;
    text: string;
    action: Function;
    tile?: any;
    gupByPass: boolean;
}
