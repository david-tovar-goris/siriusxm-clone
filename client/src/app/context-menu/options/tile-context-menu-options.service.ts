import { combineLatest as observableCombineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable, Inject } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { CMOption } from "../context-menu.interface";
import { NeriticLinkService } from "../../neritic-links";
import { neriticActionConstants } from "../../../../../servicelib/src/service/consts/neritic-action-const";
import {
    ITile,
    INeriticLinkData,
    IAppByPassState,
    CarouselTypeConst,
    BypassMonitorService,
    ConfigService,
    IAppConfig,
    ContextualUtilities,
    InitializationService,
    InitializationStatusCodes,
    FavoriteModel,
    AlertService,
    CarouselUtil
} from "sxmServices";
import { APP_CONFIG } from 'app/sxmservicelayer/sxm.service.layer.module';
import { CarouselService } from "../../carousel/carousel.service";
import { FavoriteListStoreService } from "../../common/service/favorite-list.store.service";
import { Location } from "@angular/common";
import { appRouteConstants } from "../../app.route.constants";

@Injectable()

export class TileContextMenuOptionsService
{
    public gupByPass: boolean = false;

    private isDevEndpoint : boolean      = !ContextualUtilities.isProdServer(this.SERVICE_CONFIG.apiEndpoint);
    private isLiveVideoEnabled : boolean = false;
    private REMOVE_TRANSLATION : string;
    private ADD_TRANSLATION : string;

    constructor(private neriticLinkService: NeriticLinkService,
                private bypassMonitorService: BypassMonitorService,
                @Inject(APP_CONFIG) private SERVICE_CONFIG: IAppConfig,
                private configService: ConfigService,
                private translate: TranslateService,
                private carouselService: CarouselService,
                private favoriteModel: FavoriteModel,
                private alertService: AlertService,
                private favoriteListStoreService: FavoriteListStoreService,
                initializationService : InitializationService,
                private location: Location)
    {
        this.REMOVE_TRANSLATION = this.translate.instant('contextMenu.remove');
        this.ADD_TRANSLATION = this.translate.instant('contextMenu.add');

        initializationService.initState.subscribe((state) =>
        {
            if (state !== InitializationStatusCodes.UNCONFIGURED
                && state !== InitializationStatusCodes.UNINITIALIZED)
            {
                // TODO : once live video is released into production we can get rid of the dev endpoint check
                this.isLiveVideoEnabled = this.isDevEndpoint && this.configService.liveVideoEnabled();
            }
        });

        this.bypassMonitorService.bypassErrorState.subscribe((state: IAppByPassState) =>
        {
            this.gupByPass = state.GUP_BYPASS2 || state.GUP_BYPASS;
        });
    }

    /**
     * Returns an observable that will fire whenever the tile data for favorites list has changed.
     *
     * @param tileData is the tileData that the Context Menu options are for
     * @returns observable that fires with the tile text, favorites list or alerts list changes
     */
    public observeCMOptions(tileData: ITile) : Observable<CMOption[]>
    {
        return observableCombineLatest(tileData.line1$,
                                        tileData.line2$,
                                        tileData.line3$,
                                        this.favoriteModel.favorites$,
                                        this.alertService.alerts).pipe(
                         map(() => this.getCMOptions(tileData)));
    }

    /**
     * Returns a list of context menu options for tile
     * @param {ITile} tileData
     * @returns {CMOption[]}
     */
    public getCMOptions(tileData: ITile): CMOption[]
    {
        if(!tileData) return;

        //TODO VK : Remove temporary work around when recents carousel return correct CMOptions
        if (this.isRecentsTile(tileData))
        {
            return [{
                ariaText: '',
                text : "Clear",
                action: () => this.neriticLinkService.removeRecentlyPlayedTile(tileData),
                gupByPass : this.gupByPass
            }];
        }

        if (this.isReminderTile(tileData))
        {
            let reminderContextMenu = [
                {
                    ariaText: '',
                    text : "contextMenu.setShowReminder.remove",
                    action: () =>
                    {
                        return this.neriticLinkService.removeShowReminder(null,
                            tileData.tileAssetInfo.showGuid,
                            +tileData.tileAssets.find(asset => asset.assetInfoKey === "alertType").assetInfoValue
                        );
                    },
                    gupByPass : this.gupByPass
                }
            ];

            //temp live video disable
            //REMOVE isDevEndpoint eventually
            if (this.isLiveVideoEnabled && tileData.tileAssetInfo.isLiveVideoEligible)
            {
                reminderContextMenu.unshift({
                    ariaText: '',
                    text : "contextMenu.showReminderDetails",
                    action: () => this.neriticLinkService.goToShowReminderDetails(tileData.tileAssetInfo.showGuid),
                    gupByPass : this.gupByPass
                });
            }

            return reminderContextMenu;
        }

        return tileData.neriticLinkData
            .filter((data) => data.functionalGroup !== 'primaryAction' )
            //If we have live video show everything
            //If we don't, only show it if it's not a reminder
            .filter((data) =>
                (this.isLiveVideoEnabled
                 || data.functionalGroup !== neriticActionConstants.FUNCTIONAL_GROUPS.SET_SHOW_LIVE_VIDEO_REMINDER)
            )
            .map((singleNeriticLinkData) =>
            {
              let gupByPassValue = false;
              if(singleNeriticLinkData.functionalGroup === neriticActionConstants.FUNCTIONAL_GROUPS.ADD_TO_FAVORITES ||
                  singleNeriticLinkData.functionalGroup === neriticActionConstants.FUNCTIONAL_GROUPS.ADD_SHOW_TO_FAVORITES ||
                  singleNeriticLinkData.functionalGroup === neriticActionConstants.FUNCTIONAL_GROUPS.SET_SHOW_REMINDER)
              {
                  gupByPassValue = this.gupByPass;
              }

              return {
                ariaText: this.getCMOptionAriaText(tileData, singleNeriticLinkData),
                text : this.getTranslationPath(tileData, singleNeriticLinkData),
                action: () => this.neriticLinkService.takeSecondaryTileAction(tileData, singleNeriticLinkData),
                gupByPass : gupByPassValue
              };
            });
    }

    /**
     * Returns a string path to a the translation for a given neritic action
     * e.g. "contextMenu.addToFavorites.add.show"
     * These strings can be run through the i18n translator to get the correct text
     * for a context menu option.
     * @param {ITile} tileData
     * @param {INeriticLinkData} neriticLinkData
     * @returns {CMOption[]}
     */
    public getTranslationPath(tileData: ITile, neriticLinkData: INeriticLinkData): string
    {
        const REMOVE_TRANSLATION_KEY = 'remove';
        const ADD_TRANSLATION_KEY = 'add';

        let translationPath = `contextMenu.${neriticLinkData.functionalGroup}`;
        let addOrRemove;

        switch(neriticLinkData.functionalGroup) {
            case neriticActionConstants.FUNCTIONAL_GROUPS.ADD_TO_FAVORITES:
                addOrRemove = tileData.isFavorite ? REMOVE_TRANSLATION_KEY : ADD_TRANSLATION_KEY;
                translationPath += `.${addOrRemove}.${neriticLinkData.contentType}`;
                break;

            case neriticActionConstants.FUNCTIONAL_GROUPS.ADD_SHOW_TO_FAVORITES:
                let favoriteLocal: boolean = tileData.isFavorite;

                if (CarouselUtil.isEpisodeTile(tileData))
                {
                    favoriteLocal = !!this.favoriteListStoreService.getFavorite(tileData.tileAssetInfo.showGuid);
                }

                addOrRemove = favoriteLocal ? REMOVE_TRANSLATION_KEY : ADD_TRANSLATION_KEY;
                translationPath += `.${addOrRemove}`;
                break;

            case neriticActionConstants.FUNCTIONAL_GROUPS.SET_SHOW_REMINDER:
                addOrRemove = tileData.reminders.showReminderSet ? REMOVE_TRANSLATION_KEY : ADD_TRANSLATION_KEY;
                translationPath += `.${addOrRemove}`;
                break;

            case neriticActionConstants.FUNCTIONAL_GROUPS.SET_SHOW_LIVE_VIDEO_REMINDER:
                addOrRemove = tileData.reminders.liveVideoReminderSet ? REMOVE_TRANSLATION_KEY : ADD_TRANSLATION_KEY;
                translationPath += `.${addOrRemove}`;
                break;

            case neriticActionConstants.FUNCTIONAL_GROUPS.REMOVE_SEEDED_STATION:
               translationPath =  `contextMenu.remove`;
               break;
        }

        return translationPath;
    }

    /**
     * Curates and Translates the Aria Text for the Context Menu Option
     * @param tileData
     * @param neriticLinkData
     */
    public getCMOptionAriaText(tileData: ITile, neriticLinkData: INeriticLinkData): string
    {
        const title         = this.carouselService.isChannelTile(tileData) && tileData.channelInfo
                              ? tileData.channelInfo.name : tileData.line2;
        let ariaText        = this.translate.instant(this.getTranslationPath(tileData, neriticLinkData)) || "";
        const ariaTextArray = ariaText.split(' ');

        /* Aria Label for Favorites */
        if (ariaTextArray[0] === this.REMOVE_TRANSLATION || ariaTextArray[0] === this.ADD_TRANSLATION)
        {
            ariaTextArray.splice(2, 0, title);
            ariaText = ariaTextArray.join(' ');
        }

        return ariaText;
    }

    /**
     * Check if this is a Recents tile
     * @param {ITile} tileData
     * @returns {boolean}
     */
    private isRecentsTile( tileData: ITile): boolean
    {
        const isRecentsPath =  this.location.path().indexOf(appRouteConstants.RECENTLY_PLAYED) > 0 ? true : false;

        return !!tileData.tileAssetInfo.recentPlayGuid && isRecentsPath;
    }

    /**
     * Check if this is Reminder tile
     * @param {ITile} tileData
     * @returns {boolean}
     */
    private isReminderTile( tileData: ITile): boolean
    {
        return !!tileData.tileAssetInfo.alertGuid;
    }
}
