import { TestBed }                                                                      from '@angular/core/testing';
import { NeriticLinkService }                                                           from '../../neritic-links';
import {
    ITile,
    BypassMonitorService,
    IAppByPassState,
    ConfigService,
    FavoriteModel,
    AlertService,
    InitializationStatusCodes,
    InitializationService
} from 'sxmServices';
import { NeriticLinkServiceMock }                                                       from '../../../../test/mocks/neritic-link.service.mock';
import { TileContextMenuOptionsService }                                                from './tile-context-menu-options.service';
import { TranslateService}                                                              from "@ngx-translate/core";
import { of as observableOf }                                                                   from "rxjs";
import { APP_CONFIG }                                                                   from 'app/sxmservicelayer/sxm.service.layer.module';
import { CarouselService }                                                              from "../../carousel/carousel.service";
import { CarouselServiceMock, FavoriteListStoreServiceMock } from "../../../../test/mocks";
import { MockTranslateService }                                                         from "../../../../test/mocks/translate.service";
import { FavoriteListStoreService } from "../../common/service/favorite-list.store.service";
import { Location } from "@angular/common";

describe('TileContextMenuOptionsService', () =>
{
    beforeEach(() =>
    {
        this.neriticLinkServiceMock = new NeriticLinkServiceMock();

        let bypassMonitorService = {
            bypassErrorState: observableOf({} as IAppByPassState)
        };
        let location = {
            path (includeHash?: boolean): string
            {
                return "https:local-dev.siriusxm.com/recently-played";
            }
        };

        TestBed.configureTestingModule({
            providers: [
                TileContextMenuOptionsService,
                { provide: NeriticLinkService, useValue: this.neriticLinkServiceMock },
                { provide: BypassMonitorService, useValue: bypassMonitorService },
                { provide: APP_CONFIG, useValue: { apiEndpoint: "whatever" }},
                { provide: CarouselService, useClass: CarouselServiceMock },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: ConfigService, useValue: { liveVideoEnabled: () => true} },
                { provide: FavoriteModel, useValue : {}},
                { provide: AlertService,useValue : {}},
                { provide: InitializationService, useValue : { initState : observableOf(InitializationStatusCodes.RUNNING)}},
                { provide: FavoriteListStoreService, useClass: FavoriteListStoreServiceMock },
                { provide: Location, useValue: location }
            ]
        });

        this.service = TestBed.get(TileContextMenuOptionsService);

        this.primaryNeriticLink = { functionalGroup: 'primaryAction'};
        this.neriticLink2 = { functionalGroup: 'mockFunctionalGroup2' };
        this.neriticLink3 = { functionalGroup: 'mockFunctionalGroup2' };
        this.tile = {
            neriticLinkData: [ this.primaryNeriticLink, this.neriticLink2, this.neriticLink3 ],
            tileAssetInfo: {  }
        } as ITile;
        this.mockRecentTile = {
            tileAssetInfo: { recentPlayGuid: 'ca1e84d7-55f2-4c44-a954-54e2a01a214f' }
        } as ITile;
    });

    describe('getCMOptions()', () =>
    {
        describe('when there is no tile content on the provided tile', () =>
        {
            it('returns early', () =>
            {
                expect(this.service.getCMOptions(null)).toEqual(undefined);
            });
        });

        describe('when the tile is a recents tile', () =>
        {
            it('returns an CMOptions', () =>
            {
                this.neriticLinkServiceMock.getTileContent.and.returnValue(true);
                let cmOptions = this.service.getCMOptions(this.mockRecentTile);
                cmOptions[0].action();
                expect(this.neriticLinkServiceMock.removeRecentlyPlayedTile).toHaveBeenCalledWith(this.mockRecentTile);
            });
        });

        describe('when there is tile content on the provided tile', () =>
        {
            beforeEach(() =>
            {
                this.neriticLinkServiceMock.getTileContent.and.returnValue(true);
                spyOn(this.service, 'getTranslationPath').and.returnValue('mockTranslationPath');
                this.cmOptions = this.service.getCMOptions(this.tile);
            });

            it('returns an array of populated CMOptions', () =>
            {
                expect(this.cmOptions[0]).toEqual(jasmine.objectContaining({ text: 'mockTranslationPath', action: jasmine.any(Function)}));
                expect(this.cmOptions[1]).toEqual(jasmine.objectContaining({ text: 'mockTranslationPath', action: jasmine.any(Function)}));
            });

            it('filters out the primary neritic link action', () =>
            {
                expect(this.cmOptions.length).toBe(2);
                expect(this.service.getTranslationPath).not.toHaveBeenCalledWith(this.tile, this.primaryNeriticLink);
            });

            it('returns CMOptions with actions that call neriticLinkService.takeSecondaryTileAction with the tileData and the neriticLink', () =>
            {
                this.cmOptions[0].action();
                expect(this.neriticLinkServiceMock.takeSecondaryTileAction).toHaveBeenCalledWith(this.tile, this.neriticLink2);
                this.cmOptions[1].action();
                expect(this.neriticLinkServiceMock.takeSecondaryTileAction).toHaveBeenCalledWith(this.tile, this.neriticLink3);
            });
        });
    });

    describe('getTranslationPath()', () =>
    {
        it('returns a translation path using the provided functionalGroup', () =>
        {
            let translationPath = this.service.getTranslationPath(this.tile, { functionalGroup: 'functionalGroupNumber1' });
            expect(translationPath).toBe('contextMenu.functionalGroupNumber1');
        });

        describe('when the functional group is addToFavorites', () =>
        {
            beforeEach(() =>
            {
                this.neriticLink2.functionalGroup = 'addToFavorites';
                this.neriticLink2.contentType = 'episode';
            });

            describe('when the channel/show/episode is already favorited', () =>
            {
                it('returns a translation path with remove', () =>
                {
                    this.tile.isFavorite = true;
                    expect(this.service.getTranslationPath(this.tile, this.neriticLink2)).toBe('contextMenu.addToFavorites.remove.episode');
                });
            });

            describe('when the channel/show/episode is not favorited', () =>
            {
                it('returns a translation path with add', () =>
                {
                    this.tile.isFavorite = false;
                    expect(this.service.getTranslationPath(this.tile, this.neriticLink2)).toBe('contextMenu.addToFavorites.add.episode');
                });
            });
        });

        describe('when the functional group is addShowToFavorites', () =>
        {
            beforeEach(() =>
            {
                this.tile.tileContentSubType = 'aod';
                this.neriticLink2.functionalGroup = 'addShowToFavorites';
            });

            describe('when the show for the provided episode is already favorited', () =>
            {
                it('returns a translation path with remove', () =>
                {
                    this.tile.isFavorite = true;
                    expect(this.service.getTranslationPath(this.tile, this.neriticLink2)).toBe('contextMenu.addShowToFavorites.remove');
                });
            });

            describe('when the show for the provided episode is not favorited', () =>
            {
                it('returns a translation path with add', () =>
                {
                    expect(this.service.getTranslationPath(this.tile, this.neriticLink2)).toBe('contextMenu.addShowToFavorites.add');
                });
            });
        });
    });
});
