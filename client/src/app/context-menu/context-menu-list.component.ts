import {
    Component,
    AfterViewInit,
    OnDestroy,
    ChangeDetectorRef,
    ViewChild
} from '@angular/core';
import { ContextMenuService, CMOption } from "./";
import { FocusUtil } from "../common/util/focus.util";

@Component({
    selector: 'context-menu-list',
    template: `<ul #contextMenuList
                   id="context-menu"
                   class="context-menu-list"
                   [ngStyle]="positionOfContextMenu">

                 <li class="context-menu-list__item"
                     *ngFor="let item of options; let i = index"
                     role="presentation">
                    <button class="context-menu-list__item__btn"
                            [ngClass]="{'cursor-off': item.gupByPass}"
                            [attr.aria-label]="item.ariaText"
                            data-focus-trap
                            tabindex="{{i + 1}}"
                            (click)="takeContextMenuAction(item)">
                        <span tabindex="-1">{{ item.text | translate }}</span>
                    </button>
                 </li>

               </ul>`,
    host: { '(document:click)': 'this.contextMenuService.closeContextMenu()',
            '(window:scroll)': 'this.contextMenuService.closeContextMenu()',
            '(window:dragover)': 'this.contextMenuService.closeContextMenu()',
            '(window:dragleave)': 'this.contextMenuService.closeContextMenu()',
            '(document:keydown)': 'escapeContextMenu($event)'
          },
    styleUrls: ['./context-menu-list.component.scss']
})

export class ContextMenuListComponent implements AfterViewInit, OnDestroy
{
    /**
     * Used to calculate the context menu's placement and orientation.
     *
     * @type {Event}
     * @memberof ContextMenuListComponent
     */
    event: any;

    /**
     * Provides context menu with text and action to take on click
     *
     * @type {Array<CMOption>}
     * @memberof ContextMenuListComponent
     */
    options: CMOption[] = [];

    positionOfContextMenu: any;

    @ViewChild('contextMenuList') contextMenuList;
    constructor(private contextMenuService: ContextMenuService,
                private changeDetectorRef: ChangeDetectorRef)
    {
        this.options = this.contextMenuService.getOptions();
    }

    ngAfterViewInit()
    {
        this.positionOfContextMenu = this.calculatePosition();
        FocusUtil.setupAccessibleDialog(this.contextMenuList.nativeElement, true);
        this.changeDetectorRef.detectChanges();
    }


    ngOnDestroy()
    {
        FocusUtil.closeFocusedDialog();
        if (this.contextMenuService.shouldReOpenOnDestroy)
        {
            this.contextMenuService.isContextMenuOpen = true;
            this.contextMenuService.shouldReOpenOnDestroy = false;
        }
    }

    /**
     * Calculates the context menu's X and Y position based on the screen size so
     * the menu will not appear off screen
     *
     * @returns {{ top: string, left: string, right(optional): string }}
     * @memberof ContextMenuListComponent
     */
    calculatePosition(): { top: string, left?: string, right?: string }
    {
        //miniPlayerHeight is added to prevent context menu/mini player overlap
        const miniPlayerHeight = 90;

        this.event = this.contextMenuService.getEvent();

        const eventData = {
            event: {
                x: this.getXCoord(),
                y: this.getYCoord()
            },
            page: {
                innerHeight: this.event.view.innerHeight,
                innerWidth: document.documentElement.clientWidth
            },
            contextMenuList: {
                height: this.options.length * 40,
                width: this.contextMenuList.nativeElement.getBoundingClientRect().width
            }
        };

        const exceedsPageHorizontally: boolean =
                  eventData.page.innerWidth <= (eventData.event.x + eventData.contextMenuList.width);
        const exceedsPageVertically: boolean =
         eventData.page.innerHeight <= (eventData.event.y + eventData.contextMenuList.height + miniPlayerHeight);
        const coordY = exceedsPageVertically ? eventData.event.y - eventData.contextMenuList.height: eventData.event.y;

        if (exceedsPageHorizontally)
        {
          return {
              top: coordY + 'px',
              right: eventData.page.innerWidth - eventData.event.x - 35 + 'px'
          };
        }
        else
        {
          return {
              top: coordY + 'px',
              left:  eventData.event.x + 'px'
          };
        }

    }

    public escapeContextMenu(event): void
    {
        if(event.keyCode === 27) this.contextMenuService.closeContextMenu();
    }

    /**
     * Returns the X coord to draw the context menu depending on whether it's a mouse click, touch, or keydown event
     * @returns {number}
     */
    private getXCoord(): number
    {
        let xCoord: number;

        if (this.event.clientX > 0) //Click Event
        {
            xCoord = this.event.clientX || this.event.touches[1].clientX;
        }
        else if (this.event.originalTarget) //Firefox key event
        {
            xCoord = this.event.originalTarget.getBoundingClientRect().left;
        }
        else if (this.event.srcElement) //IE key event
        {
            xCoord = this.event.srcElement.getBoundingClientRect().left;
        }

        return xCoord;
    }

    /**
     * Returns the Y coord to draw the context menu depending on whether it's a mouse click, touch, or keydown event
     * @returns {number}
     */
    private getYCoord(): number
    {
        let yCoord: number;

        if (this.event.clientY > 0) //Click event
        {
            yCoord = this.event.clientY || this.event.touches[1].clientY;
        }
        else if (this.event.originalTarget) //Firefox key event
        {
            yCoord = this.event.originalTarget.getBoundingClientRect().top;
        }
        else if (this.event.srcElement) //IE key event
        {
            yCoord = this.event.srcElement.getBoundingClientRect().top;
        }

        return yCoord;
    }

    public takeContextMenuAction(item)
    {
        item.gupByPass ? null : item.action();
        this.contextMenuService.closeContextMenu();
    }
}
