import {
    Component,
    Input,
    Output,
    OnInit,
    EventEmitter,
    ChangeDetectionStrategy
} from '@angular/core';
import { ContextMenuService, CMOption } from "./";
import { TranslateService } from "@ngx-translate/core";

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'context-menu',
    template: `<button (click)="openContextMenu($event)"
                       class="context-menu-btn"
                       *ngIf="this.options.length > 0"
                       [attr.aria-label]="ariaLabel">
                   <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" focusable="false" tabindex="-1">
                        <g class="context-menu--icon">
                            <circle cx="10" cy="2" r="2"/>
                            <circle cx="10" cy="10" r="2"/>
                            <circle cx="10" cy="18" r="2"/>
                        </g>
                   </svg>
                </button>`,
    styleUrls: [ "./context-menu.component.scss" ]
})

export class ContextMenuComponent implements OnInit
{
    public options: CMOption[];
    @Input() getOptionsFunc: () => CMOption[];
    @Input() ariaText?: string;
    public ariaLabel: string = '';
    public ariaLabelIntro: string = '';

    constructor(private contextMenuService: ContextMenuService,
                public translate: TranslateService)
    {
        this.ariaLabelIntro = this.translate.instant('contextMenu.contextMenuBtn');
    }

    ngOnInit()
    {
        this.options = this.getOptionsFunc();
        if (this.ariaText)
        {
            this.ariaLabel = this.ariaLabelIntro + ' for ' + this.ariaText;
        }
        else
        {
            this.ariaLabel = this.ariaLabelIntro;
        }
    }

    /**
     * Sets the event on the service to be passed to context menu list component.
     * Opens the context menu and passes options to be used for context menu list.
     *
     * @param {Event} event
     * @memberof ContextMenuComponent
     */
    public openContextMenu(event: Event): void
    {
        event.stopPropagation();
        this.contextMenuService.setEvent(event);

        this.options = this.getOptionsFunc();
        this.contextMenuService.openContextMenu(this.options);
    }
}
