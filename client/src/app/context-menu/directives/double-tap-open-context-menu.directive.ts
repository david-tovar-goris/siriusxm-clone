import { Directive, HostListener, Input } from "@angular/core";
import { ContextMenuService } from "..";

@Directive({
    selector: "[double-tap-context-menu]"
})
export class DoubleTapOpenContextMenu
{
    @Input() contextMenuOptions;

    constructor(private contextMenuService: ContextMenuService)
    {

    }

    @HostListener('touchstart', ['$event']) onTouch(event)
    {
        event.preventDefault();
        if (event.touches.length === 2)
        {
            this.contextMenuService.setEvent(event);
            this.contextMenuService.openContextMenu(this.contextMenuOptions);
        }
    }

    @HostListener('contextmenu', ['$event']) onContextMenu(event)
    {
        return event.touches && event.touches.length === 2;
    }
}
