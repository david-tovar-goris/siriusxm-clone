/**
 * Constants for context menu options
 */
export const contextMenuConsts = {
    TRANSLATION_PATHS: {
        CLEAR: "contextMenu.clear",
        UNKNOWN_TILE_DATA: "contextMenu.unknownTileData",
        ADD_SHOW_TO_FAVORITES: "contextMenu.addShowToFavorites.add",
        REMOVE_SHOW_FROM_FAVORITES: "contextMenu.addToFavorites.remove.show",
        ADD_CHANNEL_TO_FAVORITES: "contextMenu.addToFavorites.add.channel",
        REMOVE_CHANNEL_FROM_FAVORITES: "contextMenu.addToFavorites.remove.channel",
        ADD_EPISODE_TO_FAVORITES: "contextMenu.addToFavorites.add.episode",
        REMOVE_EPISODE_FROM_FAVORITES: "contextMenu.addToFavorites.remove.episode",
        ON_DEMAND_SHOWS: "contextMenu.onDemandShows",
        SET_SHOW_REMINDER: "contextMenu.setShowReminder.add",
        REMOVE_SHOW_REMINDER: "contextMenu.setShowReminder.remove",
        ON_DEMAND_EPISODES: "contextMenu.onDemandEpisodes",
        MORE_EPISODES: "contextMenu.moreEpisodes"
    }
};
