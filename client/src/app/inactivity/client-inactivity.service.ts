import {
    merge as observableMerge,
    fromEvent as observableFromEvent,
    Observable
} from 'rxjs';
import { filter } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { MediaPlayerService } from "sxmServices";
import { ModalService } from "../common/service/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";

import {
    InactivityService,
    AuthenticationService,
    IInactivityEvent
} from "sxmServices";

@Injectable()
export class ClientInactivityService
{
    public events: IInactivityEvent[] = [
        { target: document, type: "click" },
        { target: document, type: "keydown" },
        { target: document, type: "mousemove" }
    ];
    /**
     * Flag indicating if inactivity monitoring is paused
     */
    private isMonitoringPaused : boolean = false;


    public documentEventObservable: Observable<IInactivityEvent>;

    /**
     * Sets up the event listeners with the InactivityService.
     *
     * Subscribes to the userSession observable to start monitoring for inactivity when the user is authenticated
     * and stop monitoring when the user becomes unauthenticated.
     */
    constructor(private authenticationService: AuthenticationService,
                private inactivityService: InactivityService,
                private modalService: ModalService,
                private translate: TranslateService,
                private mediaPlayerService: MediaPlayerService)
    {
        // Observe authentication service and determine if monitoring should be conducted.
        this.observeAuthenticationService();

        // Start listening for user input if there is an inactivity interval set.
        this.createInactivityStream();
        this.documentEventObservable = this.createDocumentEventObservable(this.events);

        // Functions to execute after inactivity interval expires.
        this.inactivityService.addInactivityCallback(this.pausePlayback.bind(this));
    }

    /**
     * Pause media playback when inactivity timeout expires.
     */
    private pausePlayback()
    {
        // Pause playback when timer expires.
        if (this.mediaPlayerService.isPlaying())
        {
            this.mediaPlayerService.mediaPlayer.pause();
        }

        // Pause monitoring when timer expires.
        this.isMonitoringPaused = true;
    }

    /**
     * Create a single stream based on user inactivity within a given interval, and passes it the inactivity service.
     */
    private createInactivityStream()
    {
        // Create the stream to capture and monitor user interactions like: click, mousemove, keyup.
        this.createDocumentEventObservable(this.events).pipe(
            filter(() =>
            {
                return InactivityService.USER_INACTIVITY_INTERVAL > 0 && !this.isMonitoringPaused;
            }))
            .subscribe(() =>
            {
                this.inactivityService.throttledInactivityTimer();
            });
    }


    /**
     * Create an observable of user input by combining multiple user event streams into one.
     */
    public createDocumentEventObservable(events: IInactivityEvent[]): Observable<IInactivityEvent>
    {
        // Create array of observables from supplied user events.
        // The passed events must meet the IInactivityEvent interface.
        let observables = events.map(event =>
        {
            return observableFromEvent(event.target, event.type) as Observable<IInactivityEvent>;
        });
        // Merge all the event observables.
        return observableMerge(...observables);
    }


    private observeAuthenticationService(): void
    {
        this.authenticationService.userSession.subscribe(userSession =>
        {
            if (userSession.authenticated || userSession.activeOpenAccessSession)
            {
                this.isMonitoringPaused = false;
            }

            if (!userSession.authenticated)
            {
                this.isMonitoringPaused = true;
            }
        });
    }


    public continueListening()
    {
        // Resume playback when user closes modal.
        this.mediaPlayerService.mediaPlayer.resume();

        // Resume monitoring when user closes modal.
        this.isMonitoringPaused = false;
    }
}
