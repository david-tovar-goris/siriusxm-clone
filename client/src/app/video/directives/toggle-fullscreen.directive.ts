import { Directive, HostListener } from "@angular/core";
import { FullscreenService } from "../overlays/fullscreen/fullscreen.service";

@Directive({
    selector: "[toggleFullscreen]"
})

export class ToggleFullscreenDirective {
    constructor(private fullscreenService: FullscreenService)
    {
    }

    @HostListener("click") onClick()
    {
        this.fullscreenService.toggle();
    }
}
