import { Component, HostBinding } from "@angular/core";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "now-playing-video-overlay",
    template: `<button toggleFullscreen
                       sxm-new-analytics
                       [tagName]="AnalyticsTagNameConstants.NP_FULL_SCREEN_EXP_RTN"
                       [screen]="AnalyticsScreens.NOW_PLAYING"
                       [pageFrame]="AnalyticsPageFrames.NOW_PLAYING_MAIN"
                       [elementType]="AnalyticsElementTypes.BUTTON"
                       [buttonName]="AnalyticsTagNameConstants.NP_FULL_SCREEN_EXP_RTN_BUTTON_NAME"
                       [tagText]="AnalyticsTagTextConstants.NP_FULL_SCREEN_EXP"
                       [userPath]="AnalyticsUserPaths.NP_FULL_SCREEN_EXP"
                       [userPathScreenPosition]="1"
                       [elementPosition]="0"
                       [action]=AnalyticsTagActionConstants.BUTTON_CLICK
                       [inputType]=AnalyticsInputTypes.MOUSE
                       [title]="'playerControls.fullscreenVideo' | translate"
                       class="fullscreen-toggle"
                       (focus)="onButtonFocused()"
                       (blur)="onButtonBlurred()">
                   <img src="../../../../assets/images/fullscreen-on-white.svg"
                        role="presentation"
                        alt=""
                        tabindex="-1"/>
               </button>`,
    styleUrls: ["./now-playing-video-overlay.component.scss"]
})
export class NowPlayingVideoOverlayComponent {

    @HostBinding('class.focused-within') isFocusedWithin: boolean = false;

    /* Analytics Constants */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    constructor()
    {
    }

    onButtonFocused(): void
    {
        this.isFocusedWithin = true;
    }

    onButtonBlurred(): void
    {
        this.isFocusedWithin = false;
    }
}
