import { Component } from "@angular/core";
import { merge, timer as observableTimer, SubscriptionLike as ISubscription, fromEvent } from 'rxjs';
import { switchMap, map } from "rxjs/operators";
import { SingletonVideoService } from "../../singleton-video/singleton-video.service";
import { UpdateLastPlayhead } from "../../../common/action/now-playing.action";
import { MediaTimestampService } from "sxmServices";
import { TuneClientService } from "../../../common/service/tune/tune.client.service";
import { IAppStore } from "../../../common/store/app.store";
import { Store } from "@ngrx/store";
import { getIsLiveVideoAvailable } from "../../../common/store/now-playing.store";
import { AutoUnSubscribe } from "../../../common/decorator/auto-unsubscribe";
import { FocusUtil } from "../../../common/util/focus.util";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@AutoUnSubscribe()

@Component({
    selector: "fullscreen-video-overlay",
    templateUrl: "./fullscreen-video-overlay.component.html",
    styleUrls: ["./fullscreen-video-overlay.component.scss"]
})

export class FullScreenVideoOverlayComponent
{
    public isLiveVideoAvailable : boolean = false;

    focusin$ = fromEvent(document, 'focusin');
    mousemove$ = fromEvent(document, 'mousemove');

    animateOverlay$ = merge(this.focusin$, this.mousemove$)
        .pipe(
            switchMap(() => observableTimer(0, 3000)),
            map(val => !!val)
        );

    // Analytics Constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    constructor(public singletonVideoService: SingletonVideoService,
                private mediaTimestampService: MediaTimestampService,
                private tuneClientService: TuneClientService,
                private store: Store<IAppStore>)
    {
        this.store.select(getIsLiveVideoAvailable).subscribe(isLiveVideoAvailable => this.isLiveVideoAvailable = isLiveVideoAvailable);
    }

    ngAfterViewInit(): void
    {
        const overlayTextEl = document.getElementById('fullscreen-video-overlay');
        const playerControlsEl = document.getElementsByClassName('video-player-controls')[0];
        const focusEl = this.singletonVideoService.overlaySeriesName
            ? overlayTextEl
            : playerControlsEl;

        // if overlay text is not yet defined, apply focus to video player controls
        FocusUtil.setupAccessibleDialog(focusEl);
    }

    ngOnDestroy(): void
    {
        FocusUtil.closeFocusedDialog();
    }

    /**
     * Toggles between live audio and live video media playback.
     */
    public toggleLivePlayback(): void
    {
        this.store.dispatch(new UpdateLastPlayhead(this.mediaTimestampService.playheadTimestamp));
        this.tuneClientService.switchPlayback();
    }
}
