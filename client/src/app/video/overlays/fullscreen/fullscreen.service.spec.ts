import { FullscreenService } from "app/video/overlays/fullscreen/fullscreen.service";
import * as screenfull from "screenfull";
import {mock} from "ts-mockito";

describe("full screen service", () =>
{
    let fullscreenService;

    let screenfullMock = {
        exit: function() {},
        toggle: function() {},
        isFullscreen: true
    };

    // let screenfullMock = mock(screenfull);

    beforeEach(() =>
    {
        fullscreenService = new FullscreenService();
    });

    it("can instantiate the service", () =>
    {
        expect(fullscreenService).toBeDefined();
    });

    it("can toggle full screen", () =>
    {
        spyOn(screenfull, "toggle");
        fullscreenService.toggle();
        expect(screenfull.toggle).toHaveBeenCalled();
    });

    it("can not exit full screen if not in full screen mode", () =>
    {
        spyOn(screenfull, "exit");
        fullscreenService.exit();
        expect(screenfull.exit).not.toHaveBeenCalled();
    });
});
