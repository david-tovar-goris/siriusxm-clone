import {
    async,
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import { FullScreenVideoOverlayComponent } from "./fullscreen-video-overlay.component";
import { FullscreenService } from "app/video/overlays/fullscreen/fullscreen.service";
import {DebugElement, NO_ERRORS_SCHEMA} from "@angular/core";
import {SharedModule} from "app/common/shared.module";
import {TranslateModule} from "@ngx-translate/core";

import {SettingsService} from "app/common/service/settings/settings.service";
import {SettingsServiceMock} from "../../../../../test/mocks/settings.service.mock";
import {TuneClientService} from "app/common/service/tune/tune.client.service";

import {OverlayService} from "app/common/service/overlay/overlay.service";
import {OverlayServiceMock} from "../../../../../test/mocks/overlay.service.mock";
import {ChannelListStoreService} from "app/common/service/channel-list.store.service";
import {ChannelListServiceMock, RouterStub} from "../../../../../test";
import {
    AudioPlayerService, ConsumeService,
    CurrentlyPlayingService, DmcaService, LiveTimeService, MediaPlayerFactory, MediaPlayerService, MediaTimeLineService,
    MediaTimestampService,
    RefreshTracksService,
    SxmAnalyticsService,
    TuneService, VideoPlayerService
} from "sxmServices";
import {MockComponent, MockDirective} from "../../../../../test/mocks/component.mock";
import {SingletonVideoService} from "app/video/singleton-video/singleton-video.service";
import {MediaTimestampServiceMock} from "../../../../../test/mocks/media-timestamp.service.mock";
import {TuneServiceMock} from "../../../../../test/mocks/tune.service.mock";
import {CurrentlyPlayingServiceMock} from "../../../../../test/mocks/currently-playing.service.mock";
import {NowPlayingStoreService} from "app/common/service/now-playing.store.service";
import {NowPlayingStoreServiceMock} from "../../../../../test/mocks/now-playing.store.service.mock";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {MediaPlayerServiceMock} from "../../../../../test/mocks/media-player.service.mock";
import {NavigationService} from "app/common/service/navigation.service";
import {navigationServiceMock} from "../../../../../test/mocks/navigation.service.mock";
import {By} from "@angular/platform-browser";
import {SXMServiceLayerModule} from "app/sxmservicelayer/sxm.service.layer.module";
import {LiveTimeServiceMock} from "../../../../../test/mocks/live-time.service.mock";
import {ChromecastPlayerService} from "../../../../../../servicelib/src/mediaplayer/chromecastplayer";
import {MultiTrackAudioPlayerService} from "../../../../../../servicelib/src/mediaplayer/multitrackaudioplayer";
import {MediaPlayerFactoryMock} from "../../../../../test/mocks/media-player.factory.mock";
import {Store} from "@ngrx/store";
import {FocusUtil} from "app/common/util/focus.util";
import * as screenfull from "screenfull";
import {Router} from "@angular/router";

describe('FullScreenVideoOverlayComponent', () =>
{
    let component: FullScreenVideoOverlayComponent;
    let fixture: ComponentFixture<FullScreenVideoOverlayComponent>;
    let debugElement: DebugElement;

    let sxmAnalyticsServiceMock = {
        updateNowPlayingData: jasmine.createSpy('updateNowPlayingData'),
        logAnalyticsTag: jasmine.createSpy('logAnalyticsTag')
    };

    let singletonVideoServiceMock = {
        overlaySeriesName: true,
        selectShow: jasmine.createSpy('selectShow')
    };

    let refreshTracksService = {
        setTuneInProgress: () => false
    };

    let dmcaService: any = {
        isUnrestricted: jasmine.createSpy('isUnrestricted'),
        isRestricted: jasmine.createSpy('isRestricted'),
        isDisallowed: jasmine.createSpy('isDisallowed'),
        skipInfoDictionary$: new BehaviorSubject<any>({}) as any
    } as any as DmcaService;

    let storeMock = {
        select: () => new BehaviorSubject(null),
        dispatch: function() {}
    };

    let fullScreenServiceMock = {
        toggle: function()
        {
            screenfull.toggle(fixture.debugElement.query(By.css('.video-wrap')));
        }

    };

    let mockFocusUtil = {
        setupAccessibleDialog: function() {},
        closeFocusedDialog: function() {}
    };

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                SharedModule,
                TranslateModule.forRoot(),
                SXMServiceLayerModule
            ],
            declarations: [
                FullScreenVideoOverlayComponent,
                MockComponent({
                    selector: "video-controls",
                    inputs: ["playheadWidth", "isVideoFullScreen"]
                }),
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "userPath",
                        "pageFrame", "carouselName", "buttonName", "actionSource",
                        "elementPosition", "elementType", "action", "inputType","findingMethod",
                        "searchTerm", "tileContentType", "skipCurrentPosition"]
                })
            ],
            providers: [
                { provide: SettingsService, useValue: SettingsServiceMock.getSpy() },
                { provide: TuneClientService, useClass : TuneClientService },
                { provide: OverlayService, useValue: OverlayServiceMock.getSpy() },
                { provide: SingletonVideoService, useValue: singletonVideoServiceMock },
                { provide: MediaTimestampService, useClass: MediaTimestampServiceMock },
                { provide: TuneService, useValue: TuneServiceMock },
                { provide: RefreshTracksService, useValue : refreshTracksService },
                { provide: CurrentlyPlayingService, useClass: CurrentlyPlayingServiceMock },
                { provide: NowPlayingStoreService, useClass: NowPlayingStoreServiceMock },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: DmcaService, useValue: dmcaService },
                { provide: MediaPlayerService, useClass: MediaPlayerServiceMock },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: FullscreenService, useValue: fullScreenServiceMock },
                { provide: MediaTimeLineService, useValue: {} },
                { provide: LiveTimeService, useClass: LiveTimeServiceMock },
                { provide: ChromecastPlayerService, useValue: {} },
                { provide: AudioPlayerService, useValue: {} },
                { provide: MultiTrackAudioPlayerService, useValue: {} },
                { provide: VideoPlayerService, useValue: {} },
                { provide: MediaPlayerFactory, useValue: MediaPlayerFactoryMock },
                { provide: ConsumeService, useValue: {} },
                { provide: Store, useValue: storeMock },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: Router, useClass: RouterStub },
                { provide: FocusUtil, useValue: mockFocusUtil }

            ],
            schemas: [ NO_ERRORS_SCHEMA ]
        }).compileComponents();

        spyOn(mockFocusUtil,"setupAccessibleDialog");
        spyOn(mockFocusUtil,"closeFocusedDialog");
        fixture = TestBed.createComponent(FullScreenVideoOverlayComponent);
        component = fixture.componentInstance;
        component.isLiveVideoAvailable = true;
        spyOn(component, "ngAfterViewInit").and.callFake(() => {});
        debugElement = fixture.debugElement;
        fixture.detectChanges();
    });

    describe ("Overlay Actions", () =>
    {
       it("can select the show.", () =>
       {
           fixture.debugElement.query(By.css('.series-name'))
               .triggerEventHandler('click', {
                   target: {
                       tagName: "forTesting"
                   }
               });
           expect(singletonVideoServiceMock.selectShow).toHaveBeenCalled();
       });
    });

    describe("Full screen video overlay component analytics.", () =>
    {
        it("can log the select show button click event.", () =>
        {
            fixture.debugElement.query(By.css('.series-name'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "forTesting"
                    }
                });
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });

        // No analytics tags have been added to this button.
        xit("can log the toggle live playback button click event.", () =>
        {
            fixture.debugElement.query(By.css('.switch-audio-button'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "forTesting"
                    }
                });
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
