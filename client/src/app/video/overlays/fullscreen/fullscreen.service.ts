import { share } from 'rxjs/operators';
import * as screenfull from "screenfull";
import { Injectable } from "@angular/core";
import { BehaviorSubject ,  Observable } from "rxjs";
import { Logger } from "sxmServices";

/**
 * @DESCRIPTION:
 *
 *    FullscreenService wraps the screenfull npm library.
 *    https://www.npmjs.com/package/screenfull
 *
 *    We also expose an observable isFullscreen$ so that we can know if we're in fullscreen
 *    and react to changes.
 */

@Injectable()
export class FullscreenService
{
    private static logger: Logger = Logger.getLogger("FullscreenService");

    /**
     * Behavior subject that indicates if screenfull is in fullscreen.
     */
    private isFullScreenSubject: BehaviorSubject<boolean>;

    /**
     * Observable - we'll subscribe to know if screenfull is in fullscreen.
     */
    public isFullscreen$: Observable<boolean> = null;

    constructor(
    )
    {
        this.isFullScreenSubject = new BehaviorSubject(false);
        this.isFullscreen$ = this.isFullScreenSubject.pipe(share());

        if (screenfull.enabled)
        {
            // send an initial value to our subscribers
            this.isFullScreenSubject.next(screenfull.isFullscreen);

            // use screenful's built-in event listener to update our public observable
            screenfull.on("change", () =>
            {
                this.isFullScreenSubject.next(screenfull.isFullscreen);

                FullscreenService.logger.debug(`on change: isFullscreen = ${screenfull.isFullscreen}`);
            });

            screenfull.on("error", event =>
            {
                FullscreenService.logger.error("on error");
                FullscreenService.logger.error(JSON.stringify(event));
            });
        }
        else
        {
            FullscreenService.logger
                .warn("constructor() attempted to register observable but screenfull is not enabled");
        }
    }

    /**
     * Calls screenfull's "toggle"
     */
    public toggle(): void
    {
        if (!screenfull.enabled)
        {
            FullscreenService.logger.warn("toggle() attempted but screenfull is not enabled");

            return;
        }

        FullscreenService.logger.debug("toggle()");

        screenfull.toggle(document.getElementsByClassName("video-wrap")[0]);
    }

    /**
     * Calls screenfull's "exit"
     */
    public exit(): void
    {
        if (!screenfull.enabled || !screenfull.isFullscreen)
        {
            FullscreenService.logger.warn("exit() attempted but screenfull is not enabled");

            return;
        }

        FullscreenService.logger.debug("exit()");

        screenfull.exit();
    }
}
