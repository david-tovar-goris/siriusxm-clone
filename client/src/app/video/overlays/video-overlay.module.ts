import { NgModule } from "@angular/core";
import { FullScreenVideoOverlayComponent } from "./fullscreen/fullscreen-video-overlay.component";
import { MiniVideoOverlayComponent } from "./mini/mini-video-overlay.component";
import { NowPlayingVideoOverlayComponent } from "./now-playing/now-playing-video-overlay.component";
import { FreeTierVideoOverlayComponent } from "app/video/overlays/free-tier/free-tier-overlay.component";
import { CommonModule } from "@angular/common";
import { TranslationModule } from "../../translate/translation.module";
import { VideoControlsComponent } from "../video-controls/video-controls.component";
import { VideoPlayerModule } from "../video-player.module";
import { PlayerControlsModule } from "../../player-controls/player-controls.module";
import { FullscreenService } from "./fullscreen/fullscreen.service";
import { ToggleFullscreenDirective } from "../directives/toggle-fullscreen.directive";
import { SharedModule } from "app/common/shared.module";

@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        VideoPlayerModule,
        PlayerControlsModule,
        SharedModule
    ],
    declarations: [
        FullScreenVideoOverlayComponent,
        MiniVideoOverlayComponent,
        NowPlayingVideoOverlayComponent,
        FreeTierVideoOverlayComponent,
        VideoControlsComponent,
        ToggleFullscreenDirective
    ],
    providers: [
        FullscreenService
    ],
    exports: [
        FullScreenVideoOverlayComponent,
        MiniVideoOverlayComponent,
        NowPlayingVideoOverlayComponent,
        FreeTierVideoOverlayComponent
    ]
})

export class VideoOverlayModule
{
}
