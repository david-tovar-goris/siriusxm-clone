import { Component } from "@angular/core";
import { ExploreService } from "sxmServices";

@Component({
    selector: "free-tier-video-overlay",
    templateUrl: "./free-tier-overlay.component.html",
    styleUrls: ["./free-tier-overlay.component.scss"]
})

export class FreeTierVideoOverlayComponent {

    constructor(private exploreService: ExploreService){}

    public onClose(): void
    {
        this.exploreService.onTeaseReelClose();
    }
}
