import { Component, HostBinding } from "@angular/core";
import { appRouteConstants } from "../../../app.route.constants";
import { NavigationService } from "../../../common/service/navigation.service";
import { SingletonVideoService } from "../../singleton-video/singleton-video.service";
import { VideoUIModeService } from "../../video-ui-mode/video-ui-mode.service";
import { MediaPlayerService } from "sxmServices";
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes, AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import { EllipsisStringUtil } from "app/common/util/ellipsisString.util";

@Component({
    selector: "mini-video-overlay",
    templateUrl: "./mini-video-overlay.component.html",
    styleUrls: ["./mini-video-overlay.component.scss"]
})

export class MiniVideoOverlayComponent
{
    @HostBinding('class.focused-within') isFocusedWithin: boolean = false;

    /* Analytics constants */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsContentTypes = AnalyticsContentTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;

    constructor(private navigationService: NavigationService,
                public singletonVideoService: SingletonVideoService,
                public videoUIModeService: VideoUIModeService,
                private mediaPlayerService: MediaPlayerService)
    {
    }


    /**
     * Takes the user to the now playing view for video.
     */
    public onMiniPlayerClick(): void
    {
        this.navigationService.go([appRouteConstants.NOW_PLAYING]);
    }

    /**
     * Click handler for the close button.
     */
    public closeVideoPlayer(): void
    {
        this.mediaPlayerService.mediaPlayer.pause().subscribe();
        this.videoUIModeService.closePipPlayer();
    }


    public onChildFocused(): void
    {
        this.isFocusedWithin = true;
    }


    public onChildBlurred(): void
    {
        this.isFocusedWithin = false;
    }

    /*
     * Returns string with ellipsis if the seriesName exceeds 28 characters
     *  to avoid text from extending to next line
     */
    public getSeriesName(seriesName: string): string
    {
        return EllipsisStringUtil.ellipsisString(seriesName,28);
    }
}
