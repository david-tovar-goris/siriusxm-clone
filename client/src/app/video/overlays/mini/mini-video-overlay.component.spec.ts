import {
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import {MiniVideoOverlayComponent} from "app/video/overlays/mini/mini-video-overlay.component";
import {DebugElement} from "@angular/core";
import {SharedModule} from "app/common/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {RouterStub} from "../../../../../test";
import {MediaPlayerService, SxmAnalyticsService} from "sxmServices";
import {Store} from "@ngrx/store";
import {Router} from "@angular/router";
import {By} from "@angular/platform-browser";
import {SingletonVideoService} from "app/video/singleton-video/singleton-video.service";
import {NavigationService} from "app/common/service/navigation.service";
import {VideoUIModeService} from "app/video/video-ui-mode/video-ui-mode.service";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import { of as observableOf } from "rxjs";
import { EllipsisStringUtil } from "app/common/util/ellipsisString.util";

describe('MiniVideoOverlayComponent', () =>
{
    let component: MiniVideoOverlayComponent;
    let fixture: ComponentFixture<MiniVideoOverlayComponent>;
    let debugElement: DebugElement;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tagName) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    let singletonVideoServiceMock = {
        overlaySeriesName: true,
        togglePausePlay: jasmine.createSpy("togglePausePlay"),
        isPlaying: function() {}
    };

    let videoUIModeServiceMock: any = {
        closePipPlayer: jasmine.createSpy("closePipPlayer")
    } as any as VideoUIModeService;

    let mediaPlayerServiceMock: any = {
        mediaPlayer: {
            pause: jasmine.createSpy("pause").and.returnValue(observableOf(true))
        }
    } as any as MediaPlayerService;

    let mockNavigationService: any = {
        go: jasmine.createSpy("go")
    } as any as NavigationService;

    let storeMock = {
        select: () => new BehaviorSubject(null)
    };

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                SharedModule,
                TranslateModule.forRoot()
            ],
            declarations: [
                MiniVideoOverlayComponent
            ],
            providers: [
                { provide: SingletonVideoService, useValue: singletonVideoServiceMock },
                { provide: NavigationService, useValue: mockNavigationService },
                { provide: VideoUIModeService, useValue: videoUIModeServiceMock },
                { provide: MediaPlayerService, useValue: mediaPlayerServiceMock },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: Store, useValue: storeMock },
                { provide: Router, useClass: RouterStub }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(MiniVideoOverlayComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        fixture.detectChanges();
    });

    let fireButtonEvent = function(selector, event, tagName)
    {
        fixture.debugElement.query(By.css(selector))
            .triggerEventHandler('click', {
                target: {
                    tagName: tagName
                }
            });
    };

    describe('Mini overlay', () =>
    {
        it('Can be initialized', () =>
        {
           expect(component).toBeTruthy();
        });
    });

    describe('Mini overlay methods', () =>
    {
        it("can take the user to the now playing view for video.", () =>
        {
            fireButtonEvent('.expander' ,'click', 'MVExpandNP');
            expect(mockNavigationService.go).toHaveBeenCalledWith(["now-playing"]);
        });

        it("can close the mini overlay.", () =>
        {
            fireButtonEvent('.close' ,'click', 'MVClose');
            expect(mediaPlayerServiceMock.mediaPlayer.pause).toHaveBeenCalled();
            expect(videoUIModeServiceMock.closePipPlayer).toHaveBeenCalled();
        });

        it("can toggle play/pause.", () =>
        {
            fireButtonEvent('.play-pause-btn' ,'click', 'MVPlayPause');
            expect(singletonVideoServiceMock.togglePausePlay).toHaveBeenCalled();
        });

        it("can track focus.", () =>
        {
            fixture.debugElement.query(By.css('.play-pause-btn'))
                .triggerEventHandler('focus', {
                    target: {
                        tagName: "MVPlayPause"
                    }
                });
            expect(component.isFocusedWithin).toEqual(true);

            fixture.debugElement.query(By.css('.play-pause-btn'))
                .triggerEventHandler('blur', {
                    target: {
                        tagName: "MVPlayPause"
                    }
                });
            expect(component.isFocusedWithin).toEqual(false);
        });

        it("can get the series name.", () =>
        {
            spyOn(EllipsisStringUtil, "ellipsisString");
            component.getSeriesName("Series");
            expect(EllipsisStringUtil.ellipsisString).toHaveBeenCalledWith("Series", 28);
        });

    });

    describe("Mini overlay analytics", () =>
    {
        it("can log the expand event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fireButtonEvent('.expander' ,'click', 'MVExpandNP');
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalledWith(jasmine.objectContaining({
                name: "MVExpandNP"
            }));
        });

        it("can log the close event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fireButtonEvent('.close' ,'click', 'MVClose');
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalledWith(jasmine.objectContaining({
                name: "MVClose"
            }));
        });

        it("can log the play/pause event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fireButtonEvent('.play-pause-btn' ,'click', 'MVPlayPause');
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalledWith(jasmine.objectContaining({
                name: "MVPlayPause"
            }));
        });
    });

});
