import { VideoPlayerServiceMock } from "../../../../../servicelib/src/test/mocks/mediaplayer/video/video-player.service.mock";
import { RouterStub } from "../../../../test";
import { SingletonVideoService } from "app/video/singleton-video/singleton-video.service";
import * as _ from "lodash";
import { MediaPlayerServiceMock } from "../../../../test/mocks/media-player.service.mock";
import { BehaviorSubject } from "rxjs/index";
import { of as observableOf } from "rxjs";

describe("video ui mode service", () =>
{
    let router,
        store,
        fullscreenService,
        singletonVideoService,
        videoPlayerService,
        carouselStoreServiceMock,
        mockMediaPlayerService,
        showServiceMock,
        chormecastModel,
        mockNavigationService,
        mediaPlayerServiceMock;

    beforeEach(() =>
    {
        router = new RouterStub();

        store = {
            select: () => new BehaviorSubject(null)
        };

        fullscreenService = {
            exit: jasmine.createSpy("exit")
        };

        carouselStoreServiceMock = {
            selectSubCategory: _.noop,
            selectSuperCategory: jasmine.createSpy("selectSuperCategory")
        };

        mockMediaPlayerService = new MediaPlayerServiceMock();

        videoPlayerService = new VideoPlayerServiceMock();

        mediaPlayerServiceMock = {
          isPlaying: function() {},
          mediaPlayer: {
              togglePausePlay: jasmine.createSpy("togglePausePlay").and.returnValue(observableOf(true))
          }
        };

        mockNavigationService = {};

        showServiceMock = {
            selectShow: jasmine.createSpy("selectShow")
        };

        chormecastModel = {};

        singletonVideoService = new SingletonVideoService(
            carouselStoreServiceMock,
            mediaPlayerServiceMock,
            mockNavigationService,
            router,
            showServiceMock,
            store,
            videoPlayerService,
            chormecastModel
        );

        singletonVideoService.episode$ = new BehaviorSubject({});
    });

    beforeEach(() =>
    {
        spyOn(store, "select");
    });

    it("should be instantiated", () =>
    {
        expect(singletonVideoService).toBeDefined();
    });

    it("can initialize", () =>
    {
        spyOn(videoPlayerService, "init");
        singletonVideoService.init({});
        expect(videoPlayerService.init).toHaveBeenCalledWith({}, undefined);
    });

    it("can determine if playing", () =>
    {
       spyOn(mediaPlayerServiceMock, "isPlaying");
       singletonVideoService.isPlaying();
       expect(mediaPlayerServiceMock.isPlaying).toHaveBeenCalled();
    });

    it("can toggle pause/play", () =>
    {
        singletonVideoService.togglePausePlay();
        expect(mediaPlayerServiceMock.mediaPlayer.togglePausePlay).toHaveBeenCalled();
    });

    it("can select show", () =>
    {
        singletonVideoService.selectShow();
        expect(showServiceMock.selectShow).toHaveBeenCalled();
    });
});
