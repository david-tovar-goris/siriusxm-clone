import { of as observableOf,  Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Injectable, ViewContainerRef } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import {
    ChromecastModel,
    IChannel,
    IMediaEpisode,
    IMediaVideo,
    Logger,
    VideoPlayerConstants,
    VideoPlayerService,
    MediaPlayerService
} from 'sxmServices';
import { IAppStore } from "../../common/store/index";
import {
    getIsVod,
    getIsLiveVideo,
    getEpisode,
    getCurrentVideoMarker,
    getPosterImageUrl
} from "../../common/store/now-playing.store";
import { NavigationService } from "../../common/service/navigation.service";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { ShowService } from "../../common/service/series/show.service";
import { getSelectedChannel } from "../../common/store/channel-list.store";
import * as screenfull from 'screenfull';

@Injectable()
export class SingletonVideoService
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("SingletonVideoService");

    /**
     * The series name.
     */
    public overlaySeriesName: string = '';

    /**
     * The episode name.
     */
    public overlayEpisodeTitle: string = '';

    /**
     * Indicates if VOD is the current content type.
     */
    public isVod$: Observable<boolean> = this.store.select(getIsVod);

    /**
     * Indicates if is Live Video
     */
    public isLiveVideo$: Observable<boolean> = this.store.select(getIsLiveVideo);

    /**
     * The currently playing episode.
     */
    public episode$: Observable<IMediaEpisode> = this.store.select(getEpisode);

    /**
     * The currently playing episode.
     */
    public episode: IMediaEpisode = null;

    /**
     * The currently playing channel.
     */
    public channel: IChannel;

    /**
     * The currently playing channel.
     */
    public posterImageUrl$ = this.store.select(getPosterImageUrl);

    /**
     * Indicates if the video player is buffering.
     */
    public isBuffering$ = null;

    /**
     * Indicates if the video player is in Idle state.
     */
    public isIdle$: Observable<boolean> = null;

    /**
     * Indicates if the video player is Loaded fully.Keep the video UI state in loading until player goes to playing state
     * this was mainly used forsafari, for livestream it takes to livePoint before the video is full ready
     */
    public isLoading$: Observable<boolean> = observableOf(false) as any;

    /**
     * Indicates if the NowPlayingRoute Resolved or not.
     */
    public isNowPlayingRouteResolved: boolean = true;

    /**
     * currentVideoMarker playing when vod/live-videomarkers playing.
     */
    public currentVideoMarker$: Observable<IMediaVideo> = this.store.select(getCurrentVideoMarker);

    public upNextViewContainerRef: ViewContainerRef;

    /**
     * Constructor.
     * @param {Store<IAppStore>} store
     * @param {VideoPlayerService} videoPlayerService
     * @param {CarouselStoreService} carouselStoreService
     * @param {MediaPlayerService} mediaPlayerService
     * @param {Router} router
     */
    constructor(public carouselStoreService: CarouselStoreService,
                public mediaPlayerService: MediaPlayerService,
                private navigationService: NavigationService,
                private router: Router,
                private showService: ShowService,
                private store: Store<IAppStore>,
                private videoPlayerService: VideoPlayerService,
                public chormecastModel: ChromecastModel)
    {
        this.isBuffering$ = this.videoPlayerService.isBuffering$;
        this.isIdle$ = this.videoPlayerService.isIdle$;

        this.setOverlayDetails();


        // TODO: Mani: This is not a good way to set class member vars and we need to get away from subscriptions in the comps.
        this.store.select(getSelectedChannel).subscribe(channel => this.channel = channel);

        // Keep the video UI state in loading until player goes to playing state--> this logic was added mainly for
        // safari, for livestream it takes to livePoint before the video is full ready
        this.isLoading$ = this.videoPlayerService.playbackState.pipe(
            map(playbackState =>
            {
                return playbackState === VideoPlayerConstants.PRELOADING;
            })
        );
    }

    /**
     * Kicks off the video player creation ability to start playback.
     */
    public init(videoWrapperElement: any, flashId?: string): void
    {
        SingletonVideoService.logger.debug(`init()`);

        this.videoPlayerService.init(videoWrapperElement, flashId);
    }

    /**
     * Helps us know if current media is playing
     */
    public isPlaying(): boolean
    {
        return this.mediaPlayerService.isPlaying();
    }

    /**
     * Toggle pause/play
     */
    public togglePausePlay(): void
    {
        this.mediaPlayerService.mediaPlayer.togglePausePlay().subscribe();
    }


    /**
     * Sets the series name and episode title.
     * @returns {any}
     */
    private setOverlayDetails(): void
    {
        this.episode$.pipe(
            filter(episode => !!episode))
            .subscribe(episode =>
            {
                this.episode = episode;
                this.overlaySeriesName   = episode.show ? episode.show.longTitle : "";
                this.overlayEpisodeTitle = episode.longTitle ? episode.longTitle : "";
            });
    }

    /**
     * Sets the series name and episode title.
     * @returns {any}
     */
    public selectShow(): void
    {
        if (screenfull.isFullscreen) screenfull.exit();

        // TODO: Mani: This is not a good way to set class member vars and we need to get away from subscriptions in the comps.
        this.showService.selectShow(this.channel, this.episode ? this.episode.show : null);
    }
}
