import { combineLatest as observableCombineLatest, of as observableOf,  Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import {
    Component,
    OnInit,
    ElementRef,
    ViewChild,
    ViewContainerRef, AfterViewInit
} from "@angular/core";
import {TranslateService}                  from "@ngx-translate/core";
import { ContentTypes, IMediaVideo, Logger, PlayerTypes } from "sxmServices";
import { EVideoUIMode }                    from "../video-ui-mode/video-ui-mode.enum";
import { SingletonVideoService }           from "./singleton-video.service";
import { VideoUIModeService }              from "../video-ui-mode/video-ui-mode.service";
import { StickyService }                   from "../../common/service/sticky/sticky.service";
import { AutoplayService } from "../../autoplay/autoplay.service";

@Component({
    selector: "singleton-video-player",
    templateUrl: "./singleton-video.component.html",
    styleUrls: ["./singleton-video.component.scss"]
})

export class SingletonVideoComponent implements OnInit, AfterViewInit
{
    private static logger: Logger = Logger.getLogger("SingletonVideoComponent");

    /**
     * A reference that allows the video UI mode enum to be accessed in our template.
     */
    public eVideoUIMode = EVideoUIMode;

    public liveVideoLabel = '';

    public readonly adobeFlashId = 'adobe-flash-content';

    /**
     * Flag to display the Label on the Video player
     */
    public displayLiveVideoLabel$ : Observable<boolean> = observableOf(false);

    public remotePlayerType = PlayerTypes.REMOTE;

    @ViewChild('wrap') wrapElement: ElementRef;

    @ViewChild('upNext', { read: ViewContainerRef }) upNextViewContainerRef: ViewContainerRef;

    constructor(public singletonVideoService: SingletonVideoService,
                public videoUIModeService: VideoUIModeService,
                public translateService: TranslateService,
                public stickyService: StickyService,
                private autoplayService: AutoplayService)
    {

        const currentVideoMarker$: Observable<IMediaVideo> = this.singletonVideoService.currentVideoMarker$.pipe(
                                                             filter( (currentVideoMarker: IMediaVideo) => !!currentVideoMarker ));

        observableCombineLatest(currentVideoMarker$, this.autoplayService.canGotoLive$,
            (currentVideoMarker, canGotoLive) =>
            {
                this.liveVideoLabel = currentVideoMarker.airingType !== ContentTypes.LIVE_AUDIO
                    ? currentVideoMarker.airingType :
                    canGotoLive ? this.translateService.instant('nowPlaying.earlierToday') : ContentTypes.LIVE_AUDIO;
            }).subscribe();

        this.displayLiveVideoLabel$ = observableCombineLatest(
            this.singletonVideoService.isLoading$,
            this.singletonVideoService.isLiveVideo$, (isLoading, isLiveVideo) => !isLoading && isLiveVideo);
    }

    /**
     * Kicks off the video player.
     */
    public ngOnInit(): void
    {
        this.videoUIModeService.videoUIMode$.subscribe( (mode: EVideoUIMode) =>
        {
            if(mode === EVideoUIMode.NOW_PLAYING)
            {
                this.wrapElement.nativeElement.style.top =  (this.stickyService.getFullHeight() + 80) + 'px';
            }
            else
            {
                this.wrapElement.nativeElement.style.top = '';
            }
        });
    }

    ngAfterViewInit(): void
    {
        this.singletonVideoService.init(this.wrapElement.nativeElement, this.adobeFlashId);
        this.singletonVideoService.upNextViewContainerRef = this.upNextViewContainerRef;
    }
}
