import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { SingletonVideoComponent } from "./singleton-video.component";
import { SingletonVideoService } from "./singleton-video.service";
import { TranslateModule } from "@ngx-translate/core";
import { MediaModule } from "../../media/media.module";
import { VideoOverlayModule } from "../overlays/video-overlay.module";
import { VideoUIModeService } from "../video-ui-mode/video-ui-mode.service";
import { SharedModule } from "../../common/shared.module";

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        TranslateModule,
        MediaModule,
        VideoOverlayModule,
        SharedModule
    ],
    declarations: [
        SingletonVideoComponent
    ],
    exports: [
        SingletonVideoComponent
    ],
    providers: [
        SingletonVideoService,
        VideoUIModeService
    ]
})
export class SingletonVideoModule
{
}
