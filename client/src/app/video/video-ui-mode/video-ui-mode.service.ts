import { combineLatest, BehaviorSubject, Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Event, NavigationEnd, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import {
    Logger,
    VideoPlayerConstants,
    VideoPlayerService,
    TuneModel,
    TuneState
} from "sxmServices";
import { appRouteConstants } from "../../app.route.constants";
import { EVideoUIMode } from "./video-ui-mode.enum";
import { FullscreenService } from "../overlays/fullscreen/fullscreen.service";
import { getIsVideo } from "../../common/store/now-playing.store";
import { IAppStore } from "../../common/store/app.store";
import { SingletonVideoService } from "../singleton-video/singleton-video.service";

/**
 * @DESCRIPTION:
 *
 *    VideoUIModeService exposes an observable so that other parts of
 *    the app can know which of four modes our video player UI is in:
 *        dormant, fullscreen, mini-player, or now-playing
 *
 *    In this service, we listen to updates from the router, fullscreen service,
 *    and video player state to set the UI mode for the video player.
 */

@Injectable()
export class VideoUIModeService
{
    private static logger: Logger = Logger.getLogger("VideoUIModeService");

    /**
     * Observable stream indicating the media type.
     */
    private isVideo$: Observable<boolean> = this.store.select(getIsVideo);

    /**
     * Flag to check whether the PIP is disabled or not, this is enabled only when user closes the PIP view.
     * Initializing this as true avoids flashing the PIP player when a user clicks a VOD tile.
     */
    private hideMiniPlayer: boolean = true;

    /**
     * Behavior subject and observable that indicates which UI mode the video player is in.
     */
    private videoUIModeSubject: BehaviorSubject<EVideoUIMode>;
    public videoUIMode$: Observable<EVideoUIMode> = null;


    public singletonVideoClass$: Observable<string[]> = of(['', '']);

    /**
     * Behavior subject and observable that indicates if we're on the now playing screen.
     */
    private isOnNowPlayingScreenSubject: BehaviorSubject<boolean>;
    public isOnNowPlayingScreen$: Observable<boolean> = null;

    private get isOnNowPlayingScreen(): boolean
    {
        return this.router.url.includes(appRouteConstants.NOW_PLAYING);
    }

    private get isOnFreeTierPage(): boolean
    {
        return this.router.url.includes(appRouteConstants.FT_WELCOME);
    }

    constructor(private router: Router,
                private store: Store<IAppStore>,
                private fullscreenService: FullscreenService,
                private singletonVideoService: SingletonVideoService,
                private videoPlayerService: VideoPlayerService,
                private tuneModel: TuneModel)
    {
        this.videoUIModeSubject = new BehaviorSubject(EVideoUIMode.DORMANT);
        this.videoUIMode$ = this.videoUIModeSubject.asObservable();

        this.isOnNowPlayingScreenSubject = new BehaviorSubject(this.isOnNowPlayingScreen);
        this.isOnNowPlayingScreen$ = this.isOnNowPlayingScreenSubject;

        // Listen to subsequent router events to set isOnNowPlayingScreen$
        this.router.events.pipe(
            filter((event: Event) => (event instanceof NavigationEnd)))
            .subscribe((routerEvent) =>
            {
                this.isOnNowPlayingScreenSubject.next(this.isOnNowPlayingScreen);
            });

        // Listen to updates from the router, fullscreen service, and video player state
        // to set the UI mode for the video player.
        combineLatest(
            this.isVideo$, this.videoPlayerService.playbackState, this.isOnNowPlayingScreen$,
            this.tuneModel.tuneModel$, this.fullscreenService.isFullscreen$,
            (isVideo, videoPlaybackState, isOnNowPlayingScreen, tuneModel, isFullscreen) =>
            {
                //Hide all the video player for freetier page. Explore service will handle to display video.
                if(this.isOnFreeTierPage)
                {
                   return;
                }
                // Reset the hideMiniPlayer state to false, if we are not in PAUSE state
                if(this.hideMiniPlayer && videoPlaybackState !== VideoPlayerConstants.PAUSED
                    && videoPlaybackState !== VideoPlayerConstants.FINISHED)
                {
                    this.hideMiniPlayer = false;
                }
                if (!isVideo || !videoPlaybackState)
                {
                    this.setToDormant();
                }
                else if (isFullscreen)
                {
                    this.setToFullscreen();
                }
                else if (!this.singletonVideoService.isNowPlayingRouteResolved)
                {
                    this.setToDormant();
                }
                else if (this.isOnNowPlayingScreen || (videoPlaybackState === VideoPlayerConstants.IDLE && this.isOnNowPlayingScreen ))
                {
                    this.setToNowPlaying();
                }
                else if (videoPlaybackState === VideoPlayerConstants.FINISHED && tuneModel.tuneState === TuneState.TUNING)
                {
                    this.setToDormant();
                }
                else if(tuneModel.tuneState !== TuneState.TUNING)
                {
                    this.setToMiniPlayer();
                }
            }).subscribe();


        this.singletonVideoClass$ = combineLatest(
            this.videoUIMode$,
            this.singletonVideoService.isLoading$
        ).pipe(
            map(arr =>
            {
                let classStr0 = '';
                let classStr1 = '';

                if (arr[0] === EVideoUIMode.DORMANT)
                {
                    classStr0 = 'dormant';
                }
                else if (arr[0] === EVideoUIMode.FULLSCREEN)
                {
                    classStr0 = 'fullscreen';
                }
                else if (arr[0] === EVideoUIMode.MINI_PLAYER)
                {
                    classStr0 = 'mini-player';
                }
                else if (arr[0] === EVideoUIMode.NOW_PLAYING)
                {
                    classStr0 = 'now-playing';
                }
                else if (arr[0] === EVideoUIMode.FREE_TIER)
                {
                    classStr0 = 'free-tier';
                }

                if (arr[1])
                {
                    classStr1 = 'loading';
                }

                return [classStr0, classStr1];
            })
        );
    }

    /**
     * Sets the video UI mode to DORMANT
     */
    public setToDormant(): void
    {
        if (this.videoUIModeSubject.getValue() !== EVideoUIMode.DORMANT)
        {
            VideoUIModeService.logger.debug(`setToDormant()`);
            this.videoUIModeSubject.next(EVideoUIMode.DORMANT);
            this.fullscreenService.exit();
        }
    }

    /**
     * Sets the video UI mode to FULLSCREEN
     */
    public setToFullscreen(): void
    {
        // this guard improves performance by preventing unnecessary updates
        if (this.videoUIModeSubject.getValue() !== EVideoUIMode.FULLSCREEN)
        {
            // console.log(`setToFullscreen()`); // @ecarraway debugging
            VideoUIModeService.logger.debug(`setToFullscreen()`);
            this.videoUIModeSubject.next(EVideoUIMode.FULLSCREEN);
        }
    }

    /**
     * Sets the video UI mode to MINI_PLAYER
     */
    public setToMiniPlayer(): void
    {
        // this guard improves performance by preventing unnecessary updates
        if (this.videoUIModeSubject.getValue() !== EVideoUIMode.MINI_PLAYER && !this.hideMiniPlayer)
        {
            // console.log(`setToMiniPlayer()`); // @ecarraway debugging
            VideoUIModeService.logger.debug(`setToMiniPlayer()`);
            this.videoUIModeSubject.next(EVideoUIMode.MINI_PLAYER);
            this.fullscreenService.exit();
        }
        else if(this.hideMiniPlayer)
        {
            this.setToDormant();
        }
    }

    /**
     * Sets the video UI mode to DORMANT and enables the hidePIPView flag to true
     */
    public closePipPlayer(): void
    {
        this.hideMiniPlayer = true;
        this.setToDormant();
    }

    /**
     * Sets the video UI mode to NOW_PLAYING
     */
    public setToNowPlaying(): void
    {
        // this guard improves performance by preventing unnecessary updates
        if (this.videoUIModeSubject.getValue() !== EVideoUIMode.NOW_PLAYING)
        {
            // console.log(`setToNowPlaying()`); // @ecarraway debugging
            VideoUIModeService.logger.debug(`setToNowPlaying()`);
            this.videoUIModeSubject.next(EVideoUIMode.NOW_PLAYING);
            this.fullscreenService.exit();
        }
    }

    /**
     * Sets the video UI mode to FREE_TIER
     */
    public setToFreeTier(): void
    {
        // this guard improves performance by preventing unnecessary updates
        if (this.videoUIModeSubject.getValue() !== EVideoUIMode.FREE_TIER)
        {
            VideoUIModeService.logger.debug(`setToFreeTier()`);
            this.videoUIModeSubject.next(EVideoUIMode.FREE_TIER);
            this.fullscreenService.exit();
        }
    }
}
