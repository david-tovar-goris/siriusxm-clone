/**
 * The video player is a singleton that, once instantiated,
 * lives for the entire life of the app.
 *
 * The video player can be in one of four UI modes:
 *     dormant, fullscreen, mini-player, or now-playing
 */
export enum EVideoUIMode {
    DORMANT,
    FULLSCREEN,
    MINI_PLAYER,
    NOW_PLAYING,
    FREE_TIER
}
