import { VideoUIModeService } from "./video-ui-mode.service";
import { mock } from "ts-mockito";
import { Store } from "@ngrx/store";
import { VideoPlayerServiceMock } from "../../../../../servicelib/src/test/mocks/mediaplayer/video/video-player.service.mock";
import { TuneModel } from "../../../../../servicelib/src/tune/tune.model";
import { RouterStub } from "../../../../test";
import { EVideoUIMode } from "app/video/video-ui-mode/video-ui-mode.enum";

describe("video ui mode service", () =>
{
    let videoUIModeService,
        router,
        store,
        fullscreenService,
        singletonVideoService,
        videoPlayerService,
        tuneModel;

    beforeEach(() =>
    {
        router = new RouterStub();
        store = mock(Store);
        fullscreenService = {
            exit: jasmine.createSpy("exit")
        };
        singletonVideoService = {};
        videoPlayerService = new VideoPlayerServiceMock();
        tuneModel = new TuneModel();

        videoUIModeService = new VideoUIModeService(
            router,
            store,
            fullscreenService,
            singletonVideoService,
            videoPlayerService,
            tuneModel
        );
    });

    it("should be instantiated", () =>
    {
        expect(videoUIModeService).toBeDefined();
    });

    it("can set the video UI mode to dormant",() =>
    {
        spyOn(videoUIModeService.videoUIModeSubject, "getValue").and.returnValue(EVideoUIMode.NOW_PLAYING);
        spyOn(videoUIModeService.videoUIModeSubject, "next");

        videoUIModeService.setToDormant();

        expect(videoUIModeService.videoUIModeSubject.next).toHaveBeenCalledWith(EVideoUIMode.DORMANT);
        expect(fullscreenService.exit).toHaveBeenCalled();
    });

    it("can set the video UI mode to fullscreen",() =>
    {
        spyOn(videoUIModeService.videoUIModeSubject, "getValue").and.returnValue(EVideoUIMode.NOW_PLAYING);
        spyOn(videoUIModeService.videoUIModeSubject, "next");

        videoUIModeService.setToFullscreen();

        expect(videoUIModeService.videoUIModeSubject.next).toHaveBeenCalledWith(EVideoUIMode.FULLSCREEN);
    });

    it("can set the video UI mode to mini player",() =>
    {
        videoUIModeService.hideMiniPlayer = false;
        spyOn(videoUIModeService.videoUIModeSubject, "getValue").and.returnValue(EVideoUIMode.NOW_PLAYING);
        spyOn(videoUIModeService.videoUIModeSubject, "next");

        videoUIModeService.setToMiniPlayer();

        expect(videoUIModeService.videoUIModeSubject.next).toHaveBeenCalledWith(EVideoUIMode.MINI_PLAYER);
        expect(fullscreenService.exit).toHaveBeenCalled();
    });

    it("can close the pip player",() =>
    {
        spyOn(videoUIModeService, "setToDormant");

        videoUIModeService.closePipPlayer();

        expect(videoUIModeService.hideMiniPlayer).toBe(true);
        expect(videoUIModeService.setToDormant).toHaveBeenCalled();
    });

    it("can set the video UI mode to now playing",() =>
    {
        spyOn(videoUIModeService.videoUIModeSubject, "getValue").and.returnValue(EVideoUIMode.DORMANT);
        spyOn(videoUIModeService.videoUIModeSubject, "next");

        videoUIModeService.setToNowPlaying();

        expect(videoUIModeService.videoUIModeSubject.next).toHaveBeenCalledWith(EVideoUIMode.NOW_PLAYING);
        expect(fullscreenService.exit).toHaveBeenCalled();
    });

    it("can set the video UI mode to free tier",() =>
    {
        spyOn(videoUIModeService.videoUIModeSubject, "getValue").and.returnValue(EVideoUIMode.DORMANT);
        spyOn(videoUIModeService.videoUIModeSubject, "next");

        videoUIModeService.setToFreeTier();

        expect(videoUIModeService.videoUIModeSubject.next).toHaveBeenCalledWith(EVideoUIMode.FREE_TIER);
        expect(fullscreenService.exit).toHaveBeenCalled();
    });
});
