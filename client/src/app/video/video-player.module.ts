import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { MediaModule } from "../media/media.module";
import { SwitchPlaybackModule } from "../common/component/switch-playback-button/switch-playback-button.module";
import { TranslationModule } from "../translate/translation.module";
import { VideoApronComponent } from "./apron/video-apron.component";
import { OpenAccessModule } from "../open-access/open-access.module";

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        MediaModule,
        SwitchPlaybackModule,
        TranslationModule,
        OpenAccessModule
    ],
    declarations: [
        VideoApronComponent
    ],
    exports: [
        VideoApronComponent
    ]
})

export class VideoPlayerModule
{
}
