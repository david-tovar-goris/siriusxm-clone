import {
    IMediaAssetMetadata,
    IMediaEpisode
} from "sxmServices";

export interface IVideoPlayerData {
    mediaAssetMetadata: IMediaAssetMetadata;
    playheadTime: number;
    vodEpisode: IMediaEpisode;
}
