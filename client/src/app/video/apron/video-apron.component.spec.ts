import {
    async,
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import {
    AuthenticationService,
    IMediaSegment,
    VideoPlayerService,
    VideoPlayerUtil,
    VolumeService,
    MediaPlayerService,
    MediaTimestampService
} from "sxmServices";
import { MediaTimestampServiceMock } from "../../../../test/mocks/media-timestamp.service.mock";
import { PlayerControlsService } from "../../player-controls/player-controls.service";
import { TranslationModule } from "../../translate/translation.module";
import { VideoApronComponent } from "./video-apron.component";
import { MockComponent } from "../../../../test/mocks/component.mock";
import { AuthenticationServiceMock } from "../../../../test";
import { SingletonVideoService } from "app/video/singleton-video/singleton-video.service";

describe("VideoApronComponent", () =>
{
    const authenticationServiceMock = AuthenticationServiceMock.getSpy();

    const mockPlayerControlsService = {
        destroy: jasmine.createSpy("destroy")
    };

    const mockVideoPlayerService = {
        destroy: jasmine.createSpy("destroy"),
        restartMediaPlayer: jasmine.createSpy("restartMediaPlayer"),
        setCreatePlayerCallback: jasmine.createSpy("setCreatePlayerCallback")
    };

    const mockVideoPlayerUtil = {
        getVideoAsset: jasmine.createSpy("getVideoAsset"),
        getPlayerParams: jasmine.createSpy("getPlayerParams")
    };

    const volumeServiceMock =
        {
            getVolume: function ()
            {
                return 10;
            },
            getVolumeImageSrc: function ()
            {
                return "testUrl";
            }
        };

    const mockedMediaAssetMetadata = {
        mediaId: "",
        seriesName: "",
        episodeTitle: "",
        apronSegments: [
            {
                title: "",
                timeDisplay: "",
                startTimeInSeconds: 0,
                endTimeInSeconds: 0,
                secondsFromBeginningOfEpisode: 0,
                mediaSegment: {} as IMediaSegment
            }
        ]
    };

    const singletonVideoServiceMock = {};

    let component: VideoApronComponent;
    let element: HTMLElement;
    let fixture: ComponentFixture<VideoApronComponent>;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
                imports: [
                    TranslationModule
                ],
                declarations: [
                    VideoApronComponent,
                    MockComponent({ selector: "switch-playback-button" })
                ],
                providers: [
                    { provide: AuthenticationService, useValue: authenticationServiceMock },
                    { provide: MediaPlayerService, useValue: mockVideoPlayerService },
                    { provide: PlayerControlsService, useValue: mockPlayerControlsService },
                    { provide: VideoPlayerService, useValue: mockVideoPlayerService },
                    { provide: VideoPlayerUtil, useValue: mockVideoPlayerUtil },
                    { provide: VolumeService, useValue: volumeServiceMock },
                    { provide: MediaTimestampService, useClass: MediaTimestampServiceMock },
                    { provide: SingletonVideoService, useValue: singletonVideoServiceMock }
                ]
            })
            .compileComponents();

        fixture = TestBed.createComponent(VideoApronComponent);
        component = fixture.componentInstance;
        element = fixture.nativeElement;

        // set component @Input() values here
        component.isMini = true;
        component.mediaAssetMetadata = mockedMediaAssetMetadata;
        component.liveVideoPDT = { artistName: "", trackName: "" };

        fixture.detectChanges();
    }));

    it("should be instantiated", () =>
    {
        expect(component).toBeTruthy();
    });

    it("can handle selectShowName event.", () =>
    {
        spyOn(component.selectShowName, "emit");
        component.onSelectShowName();
        expect(component.selectShowName.emit).toHaveBeenCalled();
    });
});
