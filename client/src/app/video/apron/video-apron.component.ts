import { filter } from 'rxjs/operators';
import { BehaviorSubject ,  Observable } from "rxjs";

import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output
}                                 from "@angular/core";
import {
    IMediaAssetMetadata,
    Logger,
    ILiveVideoPDT
}                                 from "sxmServices";
import { SingletonVideoService }  from "../singleton-video/singleton-video.service";

@Component({
    selector: "sxm-video-apron",
    templateUrl: "./video-apron.component.html",
    styleUrls: [ "./video-apron.component.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class VideoApronComponent implements AfterViewInit
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("VideoApronComponent");

    private static LIVE_VIDEO_AVAILABLE_CLASS = "live-video-btn-container";
    private static LIVE_VIDEO_UNAVAILABLE_CLASS = "live-video-unavailable";

    /////////////////////////////////////////////////////////////////////////////////////
    // Inputs
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Flag indicating if there's live video available.
     */

    @Input() set isLiveVideoAvailable(flag : boolean)
    {
        this.liveVideoClassSubject
            .next((flag) ? VideoApronComponent.LIVE_VIDEO_AVAILABLE_CLASS
                         : VideoApronComponent.LIVE_VIDEO_UNAVAILABLE_CLASS);
    }

    /**
     * Contains details for the currently playing media asset.
     */
    private _mediaAssetMetaData : IMediaAssetMetadata;

    @Input() set mediaAssetMetadata(mediaAssetMetadata: IMediaAssetMetadata)
    {
        this._mediaAssetMetaData = mediaAssetMetadata;
        this.seriesName          = (mediaAssetMetadata) ? mediaAssetMetadata.seriesName : null;
        this.episodeTitle        = (mediaAssetMetadata) ? mediaAssetMetadata.episodeTitle : null;

        this.seriesTitleSubject.next(this.seriesName || this.showName);
        this.trackTitleSubject.next(this.episodeTitle || this.trackName);
    }
    get mediaAssetMetadata() : IMediaAssetMetadata { return this._mediaAssetMetaData; }

    /**
     * The program descriptive text for live video.
     */
    private _liveVideoPDT : ILiveVideoPDT;

    @Input() set liveVideoPDT(liveVideoPDT: ILiveVideoPDT)
    {
        this._liveVideoPDT = liveVideoPDT;
        this.showName      = (liveVideoPDT) ? liveVideoPDT.showName : null;
        this.trackName     = (liveVideoPDT) ? liveVideoPDT.trackName : null;

        this.seriesTitleSubject.next(this.seriesName || this.showName);
        this.trackTitleSubject.next(this.episodeTitle || this.trackName);
    }
    get liveVideoPDT() : ILiveVideoPDT { return this._liveVideoPDT; }

    /**
     * Flag for the view.  Conditionally applies CSS for mini-player mode.
     */
    @Input() isMini: boolean = false;

    /**
     * Flag for the view.  Determines if apron PDT text will be shown.
     */
    @Input() allowApron: boolean = true;

    /////////////////////////////////////////////////////////////////////////////////////
    // Outputs
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Broadcasts an event when the user clicks the video's show name.
     */
    @Output() selectShowName = new EventEmitter<any>();

    /**
     * Broadcast when the user toggles live playback media type.
     */
    @Output()
    public toggleLivePlayback = new EventEmitter<any>();

    private liveVideoClassSubject : BehaviorSubject<string>
                = new BehaviorSubject('VideoApronComponent.LIVE_VIDEO_UNAVAILABLE_CLASS');
    public liveVideoClass : Observable<string> = this.liveVideoClassSubject;

    /**
     * BehaviourSubject/Observable for triggering changes to the template for the series title
     */
    private seriesTitleSubject : BehaviorSubject<string> = new BehaviorSubject(null);
    public seriesTitle : Observable<string> = this.seriesTitleSubject.pipe(filter((text) => !!text));

    /**
     * BehaviourSubject/Observable for triggering changes to the template for the track title
     */
    private trackTitleSubject : BehaviorSubject<string> = new BehaviorSubject(null);
    public trackTitle : Observable<string> = this.trackTitleSubject.pipe(filter((text) => !!text));

    /*
     * Name of the show for the video apron
     */
    private showName : string = "";

    /**
     * Name of the series for the video apron
     */
    private seriesName : string = "";

    /**
     * Title of the on demand episode we are playing
     */
    private episodeTitle : string = "";

    /*
     * Name of the track we are currently playing
     */
    private trackName : string = "";


    /**
     * Constructor.
     * @param {SingletonVideoService} singletonVideoService
     */
    constructor(private singletonVideoService: SingletonVideoService) {}

    /**
     * We only want to show the video player once we're finally on the now playing route AND the view has initialized.
     *
     * NOTE: Tried moving this to the service `singletonVideoService` where the rest of the logic is for video display,
     * but the problem is the video is ready before the route and view are initialized and it looks bad. It's a better
     * UX to do so here wher we know exactly when the view is ready to rock.
     */
    public ngAfterViewInit(): void
    {
        VideoApronComponent.logger.debug("ngAfterViewInit()");
    }

    /**
     * Clicking on the video show name will take the user to the show's on-demand episode listing.
     */
    public onSelectShowName(): void
    {
        VideoApronComponent.logger.debug("onSelectShowName()");
        this.selectShowName.emit();
    }
}
