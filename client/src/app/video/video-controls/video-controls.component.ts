import {
    combineLatest as observableCombineLatest,
    SubscriptionLike as ISubscription,
    Observable
} from 'rxjs';
import {
    Component,
    HostListener,
    Input
} from "@angular/core";
import {
    getContentType,
    getShow,
    selectNowPlayingState
} from "../../common/store/now-playing.store";
import {
    ContentTypes,
    HtmlElementTypes,
    IApronSegment,
    IMediaShow,
    KeyboardEventKeyTypes,
    MediaUtil,
    DmcaService,
    MediaPlayerService,
    MediaTimestampService
} from "sxmServices";
import { selectSegments } from "../../common/store/apron-segment.store";
import { IAppStore, INowPlayingStore } from "../../common/store/index";
import { PlayerControlsService } from "../../player-controls/player-controls.service";
import { ProgressBarService } from "../../media/progress-bar/progress-bar.service";
import { FavoriteListStoreService } from "../../common/service/favorite-list.store.service";
import { Store } from "@ngrx/store";
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { Skip15SecondsService } from "../../player-controls/control-buttons/skip-15-seconds/skip-15-seconds.service";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes, AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import { VideoUIModeService } from "app/video/video-ui-mode/video-ui-mode.service";

@Component({
    selector: "video-controls",
    templateUrl: "./video-controls.component.html",
    styleUrls: ["./video-controls.component.scss"]
})

export class VideoControlsComponent
{
    @Input() playheadWidth: number;

    /**
     * The currently playing channel.
     */
    public nowPlayingData$: Observable<INowPlayingStore> = this.store.select(selectNowPlayingState);

    /**
     * The can goto live flag stream.
     */
    public canGotoLive$: Observable<boolean> = this.mediaTimestampService.isPlayheadBehindLive$;

    /**
     * The can show goto live flag stream.
     */
    public canShowGotoLive$: Observable<boolean> = this.store.select(selectNowPlayingState)
        .pipe(filter(nowPlayingStore => !!nowPlayingStore),
            map((nowPlayingStore: INowPlayingStore) =>
            {
                const mediaType = nowPlayingStore.mediaType;
                return MediaUtil.isLiveMediaType(mediaType)
                    && (this.dmcaService.isUnrestricted(nowPlayingStore) || this.dmcaService.isRestricted(nowPlayingStore));
            }));


    /**
     * The is disallowed flag stream.
     */
    public isDisallowed$: Observable<boolean> = this.store.select(selectNowPlayingState)
        .pipe(filter(nowPlayingStore => !!nowPlayingStore),
            map((nowPlayingStore: INowPlayingStore) =>
            {
                return this.dmcaService.isDisallowed(nowPlayingStore);
            }));


    /**
     * The current show stream.
     */
    public show$: Observable<IMediaShow> = this.store.select(getShow);

    /**
     * List of episode segments that are consumed and used by the UI.
     */
    public segments$: Observable<IApronSegment[]> = this.store.select(selectSegments);

    /**
     * The DMCA is unrestricted flag stream.
     */
    public isDmcaUnrestricted$: Observable<boolean> = this.store.select(selectNowPlayingState)
        .pipe(filter(nowPlayingStore => !!nowPlayingStore),
            map((nowPlayingStore: INowPlayingStore) =>
            {
                return this.dmcaService.isUnrestricted(nowPlayingStore);
            }));

    /**
     * Observable string indicating the media type.
     */
    public contentType$: Observable<string> = this.store.select(getContentType);

    /**
     * isVideoFullScreen guard to use player controls on full screen.
     */
    @Input() isVideoFullScreen: boolean = false;

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * This is set by our subscription to isOnDemand$
     * It is used by the favoriting icon.
     */
    public isOnDemand: boolean;

    /**
     * Flag indicating if currently playing content is media type live and is also dmca disallowed.
     */
    public isLiveAndDisallowed: boolean;

    /* Analytics Constants */
    public tagName: string = null;
    public pageFrame: string = null;
    public buttonName: string = null;
    public tagText: string = null;
    public userPath: string = null;
    public userPathScreenPosition: number = null;
    public modal: string = null;
    public elementPosition: number = null;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    constructor(public progressBarService: ProgressBarService,
                public mediaPlayerService: MediaPlayerService,
                public playerControlsService: PlayerControlsService,
                private favoriteListStoreService: FavoriteListStoreService,
                private store: Store<IAppStore>,
                public mediaTimestampService: MediaTimestampService,
                public skip15SecondsService: Skip15SecondsService,
                public dmcaService: DmcaService,
                private videoUIModeService: VideoUIModeService)
    {
        this.subscriptions.push(
            this.nowPlayingData$
                .pipe(filter(nowPlayingStore => !!nowPlayingStore))
                .subscribe((nowPlayingStore: INowPlayingStore) =>
                {
                    const mediaType = nowPlayingStore.mediaType;

                    const isLive = mediaType === ContentTypes.LIVE_AUDIO ||
                        mediaType === ContentTypes.LIVE_VIDEO;

                    this.isLiveAndDisallowed = this.dmcaService.isDisallowed(nowPlayingStore) && isLive;
                }),
            this.videoUIModeService.isOnNowPlayingScreen$.pipe(distinctUntilChanged()).subscribe((isOnNowPlayingScreen: boolean) =>
                {
                   this.setAnaTagsFullToggle(isOnNowPlayingScreen);
                })
            );
    }

    /**
     * Allows users to restart the currently playing episode.
     */
    public restart(): void
    {
        this.playerControlsService.restart();
    }

    /**
     * Allows users to goto the live point for the currently playing episode.
     */
    public goToLive(): void
    {
        this.playerControlsService.gotoLive();
    }

    /**
     * Keyboard event handler
     */
    @HostListener("document:keydown", [ "$event" ])
    onKeyPress(event: KeyboardEvent)
    {
        if (event.key === KeyboardEventKeyTypes.SPACEBAR)
        {
            // Cast this as an HTML element so that we can access the target's tag name.
            const target = <HTMLElement> event.target;

            // If target is an input (e.g. the search field) return true without pausing or playing.
            if (target.tagName === HtmlElementTypes.INPUT)
            {
                return true;
            }

            // Prevent scrolling.
            event.preventDefault();

            this.mediaPlayerService.mediaPlayer.togglePausePlay();
        }
    }

    /* set Ana tags for full screen toggle*/
    private setAnaTagsFullToggle(isNowPlayingPage)
    {
        if(isNowPlayingPage)
        {
            this.tagName = AnalyticsTagNameConstants.NP_FULL_SCREEN_EXP_RTN;
            this.pageFrame = AnalyticsPageFrames.NOW_PLAYING_MAIN;
            this.modal = null;
            this.buttonName = AnalyticsTagNameConstants.NP_FULL_SCREEN_EXP_RTN_BUTTON_NAME;
            this.tagText = AnalyticsTagTextConstants.NP_FULL_SCREEN_RTN;
            this.userPath = AnalyticsUserPaths.NP_FULL_SCREEN_RTN;
            this.userPathScreenPosition = 1;
            this.elementPosition = 7;
        }
        else
            {
                this.tagName = AnalyticsTagNameConstants.VIDEO_FULL_SCREEN_RTN;
                this.pageFrame = AnalyticsPageFrames.MODAL;
                this.modal = AnalyticsModals.MINI_NOW_PLAYING_BAR;
                this.buttonName = AnalyticsTagNameConstants.VIDEO_FULL_SCREEN_RTN_BUTTON_NAME;
                this.tagText = null;
                this.userPath = AnalyticsUserPaths.VIDEO_FULL_SCREEN_RTN;
                this.userPathScreenPosition = 3;
                this.elementPosition = 7;
            }
    }
}
