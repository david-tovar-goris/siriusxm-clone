/* Slick Carousel Options Interfaces */
export interface SlickSettingsOptions {
    slidesToScroll?: number;
    slidesToShow?: number;
    infinite: boolean;
}

export interface SlickCarouselOptions extends SlickSettingsOptions {
    responsive?: Array<SlickResponsiveOptions>;
}

export interface SlickResponsiveOptions {
    breakpoint: number;
    settings: SlickSettingsOptions;
}
