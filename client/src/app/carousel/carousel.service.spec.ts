import { TestBed } from "@angular/core/testing";
import { CarouselService } from "./carousel.service";
import { ITile } from "sxmServices";
import { NeriticLinkService } from "../neritic-links";
import { NeriticLinkServiceMock } from "../../../test/mocks/neritic-link.service.mock";

describe("CarouselService", () =>
{
    let service: CarouselService;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            providers: [
                CarouselService,
                { provide: NeriticLinkService, useClass: NeriticLinkServiceMock }
            ]
        });

        service = TestBed.get(CarouselService);
    });

    describe("isShowTile()", () =>
    {
        describe("when the tile type is a show", () =>
        {
            it("should return as true", () =>
            {
                const tile = {
                    tileContentType: 'show'
                } as ITile;

                expect(service.isShowTile(tile)).toEqual(true);
            });
        });

        describe("when the tile type is not a show", () =>
        {
            it("should return as false", () =>
            {
                const tile = {
                    tileContentType: 'channel'
                } as ITile;

                expect(service.isShowTile(tile)).toEqual(false);
            });
        });
    });

    describe("isEpisodeTile", () =>
    {
        describe("when the tile type is an episode", () =>
        {
            it("should return as true", () =>
            {
                const tile = {
                    tileContentType: 'episode'
                } as ITile;

                expect(service.isEpisodeTile(tile)).toEqual(true);
            });
        });

        describe("when the tile type is not a episode", () =>
        {
            it("should return as false", () =>
            {
                const tile = {
                    tileContentType: 'show'
                } as ITile;

                expect(service.isEpisodeTile(tile)).toEqual(false);
            });
        });
    });

    describe("isChannelTile", () =>
    {
        describe("when the tile type is a channel", () =>
        {
            it("should return as true", () =>
            {
                const tile = {
                    tileContentType: 'channel'
                } as ITile;

                expect(service.isChannelTile(tile)).toEqual(true);
            });
        });

        describe("when the tile type is not a channel", () =>
        {
            it("should return as false", () =>
            {
                const tile = {
                    tileContentType: 'show'
                } as ITile;

                expect(service.isChannelTile(tile)).toEqual(false);
            });
        });
    });

    describe("isViewAllTile", () =>
    {
        describe("when the tile type is a viewAll", () =>
        {
            it("should return as true", () =>
            {
                const tile = {
                    tileContentType: 'viewAll'
                } as ITile;

                expect(service.isViewAllTile(tile)).toEqual(true);
            });
        });

        describe("when the tile type is not a viewAll", () =>
        {
            it("should return as false", () =>
            {
                const tile = {
                    tileContentType: 'channel'
                } as ITile;

                expect(service.isViewAllTile(tile)).toEqual(false);
            });
        });
    });

    describe("isAodEpisode()", () =>
    {
        describe("when the content sub type for the episode is aod", () =>
        {
            it("should return true", () =>
            {
                const tile = {
                    tileContentType: 'episode',
                    tileContentSubType: 'aod'
                } as ITile;

                expect(service.isAodEpisode(tile)).toEqual(true);
            });
        });

        describe("when the content sub type for the episode is not aod", () =>
        {
            it("should return false", () =>
            {
                const tile = {
                    tileContentType: 'episode',
                    tileContentSubType : 'vod'
                } as ITile;

                expect(service.isAodEpisode(tile)).toEqual(false);
            });
        });
    });

    describe("isVodEpisode()", () =>
    {
        describe("when the content sub type for the episode is vod", () =>
        {
            it("should return true", () =>
            {
                const tile = {
                    tileContentType: 'episode',
                    tileContentSubType: 'vod'
                } as ITile;

                expect(service.isVodEpisode(tile)).toEqual(true);
            });
        });

        describe("when the content sub type for the episode is not vod", () =>
        {
            it("should return false", () =>
            {
                const tile = {
                    tileContentType: 'episode',
                    tileContentSubType : 'aod'
                } as ITile;

                expect(service.isVodEpisode(tile)).toEqual(false);
            });
        });
    });

    describe("getImageAltText()", () =>
    {
        describe("when the tile has imageAltText that exceeds 40 characters", () =>
        {
            it("should return imageAltText with ellipsis", () =>
            {
                const tile = {
                    imageAltText: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit."
                } as ITile;

                const ellipsisText = "Lorem ipsum dolor sit amet, consectet...";

                expect(service.getImageAltText(tile.imageAltText)).toEqual(ellipsisText);
            });
        });

        describe("when the tile has imageAltText that is below 40 characters", () =>
        {
            it("should return imageAltText", () =>
            {
                const tile = {
                    imageAltText: "Lorem ipsum dolor sit amet"
                } as ITile;

                expect(service.getImageAltText(tile.imageAltText)).toEqual(tile.imageAltText);
            });
        });
    });
});
