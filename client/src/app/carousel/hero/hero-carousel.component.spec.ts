import {Input, NO_ERRORS_SCHEMA} from "@angular/core";
import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import {ConfigServiceMock} from "../../../../test/mocks/config.service.mock";
import {
    ChannelListServiceMock,
    CarouselServiceMock,
    RouterStub
} from "../../../../test/mocks/index";
import { ChannelListStoreService } from "../../common/service/channel-list.store.service";
import { HeroCarouselComponent } from "./hero-carousel.component";
import {
    ChannelLineupService,
    ConfigService,
    ITileAssetInfo,
    SxmAnalyticsService,
    CurrentlyPlayingService,
    ITile
} from "sxmServices";
import { SharedModule } from "../../common/shared.module";
import { channelLineupServiceMock } from "../../../../test/index";
import { NeriticLinkService } from "../../neritic-links";
import { NeriticLinkServiceMock } from "../../../../test/mocks/neritic-link.service.mock";
import { TranslateService, TranslateModule } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { CarouselService } from "../carousel.service";
import { MockComponent, MockDirective } from "../../../../test/mocks/component.mock";
import { TileContextMenuOptionsService } from "../../context-menu/options/tile-context-menu-options.service";
import { TileContextMenuOptionsServiceMock } from "../../../../test/mocks/tile-context-menu-options.service.mock";
import { tileMock } from "../../../../test/mocks/data/tile.mock";
import { NowPlayingIndicatorService } from "../../common/service/now-playing-indicator/now-playing-indicator.service";
import { NowPlayingIndicatorServiceMock } from "../../../../test/mocks/now-playing-indicator.service.mock";
import { ContextMenuServiceMock } from "../../../../test/mocks/context-menu.service.mock";
import { ContextMenuService } from "../../context-menu";
import { DoubleTapOpenContextMenu } from "../../context-menu/directives/double-tap-open-context-menu.directive";
import { MediaPlayerService } from "sxmServices";
import { MediaPlayerServiceMock } from "../../../../test/mocks/media-player.service.mock";
import { tileAssetUtil } from '../../common/util/tileAsset.util';
import { IAppStore } from "../../common/store/app.store";
import { Store } from '@ngrx/store';
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Router } from "@angular/router";

describe("HeroCarouselComponent", () =>
{
    let component: HeroCarouselComponent = null,
        fixture: ComponentFixture<HeroCarouselComponent> = null;

    let sxmAnalyticsServiceMock = {
        updateNowPlayingData: function() {}
    };

    let nowPlayingStore = new BehaviorSubject<any>(null);

    let store = {
        select: () => nowPlayingStore
    } as any as Store<IAppStore>;

    let currentlyPlayingServiceMock = {
        isTileTuned: function(tile: ITile)
        {
            return true;
        }
    };

    beforeEach(() =>
    {
        TestBed.configureTestingModule({

            imports: [
                SharedModule,
                TranslateModule.forRoot()
            ],
            declarations: [
                HeroCarouselComponent,
                DoubleTapOpenContextMenu,
                MockComponent({ selector: "context-menu", inputs: ["options", "contextMenuOptions"] }),
                MockDirective({ selector: "[sxm-new-analytics]", inputs: ["tagName", "tagType", "tagText"]})
            ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: ChannelLineupService, useValue: channelLineupServiceMock },
                { provide: NeriticLinkService, useClass: NeriticLinkServiceMock },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: CarouselService, useValue: CarouselServiceMock.getSpy() },
                { provide: ConfigService, useValue: ConfigServiceMock },
                { provide: TileContextMenuOptionsService, useClass: TileContextMenuOptionsServiceMock },
                { provide: ContextMenuService, useClass: ContextMenuServiceMock },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: MediaPlayerService, useValue: MediaPlayerServiceMock },
                { provide: CurrentlyPlayingService, useValue: currentlyPlayingServiceMock },
                { provide: Store, useValue: store },
                { provide: Router, useClass: RouterStub }
            ]
        }).compileComponents();

        spyOn(HeroCarouselComponent.prototype, 'createCarousel').and.callFake(():any => {});
    });

    beforeEach(() =>
    {
        HeroCarouselComponent.prototype.categoryType = "superCategory";
        fixture = TestBed.createComponent(HeroCarouselComponent);
        component = fixture.componentInstance;
        component.carouselData = [ {
            tiles: [ tileMock ],
            carouselOrientation: "horizontal"
        }
        ];
        fixture.detectChanges();
    });

    describe("ngOnChanges()", () =>
    {
        it("calls destroyCarousel() to remove the Slick Carousel", () =>
        {
            spyOn(component, "destroyCarousel");
            component.ngOnChanges({});
            fixture.detectChanges();

            expect(component.destroyCarousel).toHaveBeenCalled();
        });
    });

    describe("isCarouselAvailable()", () =>
    {
        describe("when either Hero Carousel data is defined or Fallback Carousel data is defined", () =>
        {
            it("returns True to display the Hero Carousel", () =>
            {
                expect(component.isCarouselAvailable()).toEqual(true);
            });
        });

        describe("when both Hero Carousel data is undefined and Fallback Carousel data is undefined", () =>
        {
            it("returns False to hide the Hero Carousel", () =>
            {
                component.carouselData = null;
                expect(component.isCarouselAvailable()).toEqual(false);
            });
        });
    });
});

