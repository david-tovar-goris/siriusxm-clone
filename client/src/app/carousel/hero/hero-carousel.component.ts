import { debounceTime } from 'rxjs/operators';
import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnChanges,
    NgZone
} from "@angular/core";
import { CarouselComponent } from "../carousel.component";
import {
    ChannelLineupService,
    CarouselTypeConst,
    ConfigService,
    ICarouselData,
    MediaUtil,
    CarouselOrientationTypeConsts,
    ContentTypes,
    ITile,
    CurrentlyPlayingService
} from "sxmServices";
import { NeriticLinkService } from "../../neritic-links";
import { TranslateService } from "@ngx-translate/core";
import { CarouselService } from "../carousel.service";
import { TileContextMenuOptionsService } from "../../context-menu/options/tile-context-menu-options.service";
import { tileAssetUtil } from '../../common/util/tileAsset.util';
import { heroCarouselOptions } from "../content/slick-responsive-breakpoints";
import { Subject } from "rxjs";
import { CMOption } from "../../context-menu/context-menu.interface";
import { ColorUtil } from "../../common/util/color.util";
import { FocusUtil } from "../../common/util/focus.util";
import { TileUtil } from "../../common/util/tile.util";
import {
    AnalyticsTagActionConstants,
    AnalyticsCarouselNames,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsPageFrames,
    AnalyticsInputTypes,
    AnalyticsElementTypes,
    AnalyticsTagTextConstants,
    AnalyticsFindingMethods
} from "../../analytics/sxm-analytics-tag.constants";
import { BrowserUtil } from "../../common/util/browser.util";

@Component({
    selector: "sxm-hero-carousel",
    templateUrl: "./hero-carousel.component.html",
    host: { '(window:resize)': 'this.resizeEvent$.next(event)' },
    styleUrls: ["./hero-carousel.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class HeroCarouselComponent extends CarouselComponent implements OnChanges
{
    /**
     * Analytics Constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;

    @Input() categoryType: string;
    @Input() pageName: string;

    public liveBanner: string = CarouselTypeConst.ON_WATCH_LIVE_BANNER_CLASS;
    public vodContentType: string = ContentTypes.VOD;
    public promoImageType: string = CarouselTypeConst.PROMO_IMAGE_TYPE;

    private resizeEvent$: Subject<any> = new Subject<any>();

    public lazyLoadUrl: string = null;

    public isTileImageFailedList: boolean[] = [];

    constructor(public channelLineupService: ChannelLineupService,
                public neriticLinkService: NeriticLinkService,
                public translate: TranslateService,
                public carouselService: CarouselService,
                public configService : ConfigService,
                public tileContextMenuOptionsService: TileContextMenuOptionsService,
                public currentlyPlayingService: CurrentlyPlayingService,
                public ngZone: NgZone)
    {
        super(channelLineupService,
            neriticLinkService,
            translate,
            ngZone);
        this.zoneId = this.zoneId ? this.zoneId : '0';
        this.zoneOrder = this.zoneOrder ? this.zoneOrder : 0;
        this.zoneTitle = this.zoneTitle ? this.zoneTitle : "";

        this.onViewReady = () =>
        {
            this.carouselData.forEach((carousel, index) =>
            {
                if (carousel.carouselOrientation == CarouselOrientationTypeConsts.HORIZONTAL && carousel.tiles.length)
                {
                    const carouselSelector = `#hero-carousel-${index}-${this.zoneId}`;
                    const opts = Object.assign(heroCarouselOptions, {
                        prevArrow: `#hero-carousel-${index}-${this.zoneId}-prev`,
                        nextArrow: `#hero-carousel-${index}-${this.zoneId}-next`
                    });

                    this.carouselRefs.push(this.createCarousel(carouselSelector, opts));
                }
            });
            this.carouselInitialized = true;
        };

        this.resizeEvent$.pipe(debounceTime(500)).subscribe(() =>
        {
            this.carouselData.forEach((carousel, index) =>
            {
                if (carousel.carouselOrientation == CarouselOrientationTypeConsts.HORIZONTAL && carousel.tiles.length)
                {
                    const carouselSelector = `#hero-carousel-${index}-${this.zoneId}`;
                    this.isElementVisible(carouselSelector);
                }
            });
            BrowserUtil.triggerVerticalScroll();
        });
    }

    /**
     * returns the content carousels.
     * @returns {Array<ICarouselData>}
     */
    public getHeroCarousels(): Array<ICarouselData>
    {
        if (this.isCarouselAvailable() === false) return [];
        return this.carouselData;
    }

    /**
     * checks whether tile is currently playing or not.
     * @returns {boolean}
     */
    public isCurrentlyPlayingTile(tile): boolean
    {
        return this.currentlyPlayingService.isTileTuned(tile);
    }

    public onCarouselTileClick(tile)
    {
        if(!this.isSliding)
        {
            this.carouselService.onClickNeriticLinkAction(tile);
        }
    }


    public getCMOptionsFunc(tile): () => CMOption[]
    {
        return function()
        {
            return this.tileContextMenuOptionsService.getCMOptions(tile);
        }.bind(this);
    }


    public wrapperStyle(tile: ITile): object
    {
        const fallbackColor = '#637485';
        const backgroundColor = tile.backgroundColor || fallbackColor;

        if (this.carouselService.isSeededRadioTile(tile))
        {
            const rgba1 = ColorUtil.hexToRgba(backgroundColor, 0);
            const rgba2 = ColorUtil.hexToRgba(backgroundColor, 0.7);
            const rgba3 = ColorUtil.hexToRgba(backgroundColor, 1);

            const val = `linear-gradient(${rgba1} 50%, ${rgba2} 72%, ${rgba3} 100%)`;
            return  { 'background-image': val };
        }
        else
        {
            return {
                'background-color': backgroundColor,
                'background-image': 'linear-gradient(to bottom, rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0))'
            };
        }
    }


    public getLazyLoadUrl(tile: ITile): string
    {
        return TileUtil.normalizeImageUrl(tile.bgImageUrl, 'width=640&height=480&preserveAspect=true');
    }

    /**
     * set Default background if image fails to load
     * Handled only for Iris Podcast tile.
     * @param tile
     */
    public onFGImageError(tile: ITile, index: number)
    {
        if(tile.tileAssetInfo.isPandoraPodcast)
        {
            this.isTileImageFailedList[index] = true;
        }
    }

    public getFallbackStyle(tile, index)
    {
        if( tile.tileAssetInfo.isPandoraPodcast && (this.isTileImageFailedList[index] || !tile.fgImageUrl))
        {
            return TileUtil.getDefaultBackground(tile);
        }
        else
        {
            return this.wrapperStyle(tile);
        }
    }

    /**
     * Returns true if tile is pandora/iris podcast show tile
     */
    public isIrisShowTile(tile: ITile): boolean
    {
        return tile.tileAssetInfo.isPandoraPodcast && tile.tileContentType === ContentTypes.SHOW;
    }

    public isIrisEpisodeTile(tile: ITile): boolean
    {
        return tile.tileAssetInfo.isPandoraPodcast && tile.tileContentType === ContentTypes.EPISODE;
    }

}
