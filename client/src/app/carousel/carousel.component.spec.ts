import { TestBed } from "@angular/core/testing";
import { of as observableOf } from "rxjs";
import { CarouselComponent } from "./carousel.component";
import { FavoriteListStoreServiceMock } from "../../../test/mocks/favorite-list.store.service.mock";
import { mockChannelLineup } from "../../../../servicelib/src/test/mocks/channel.lineup";
import { NeriticLinkService } from "../neritic-links";
import { NeriticLinkServiceMock } from "../../../test/mocks/neritic-link.service.mock";
import { OnDemandListServiceMock } from "../../../test/mocks/on-demand-list.service.mock";
import { MockTranslateService } from "../../../test/mocks/translate.service";
import { TranslateService } from "@ngx-translate/core";
import { CarouselServiceMock } from "../../../test/mocks";
import { ChannelLineupServiceMock } from "../../../test/mocks/channel-lineup.service.mock";
import {
    ChannelLineupService,
    mockForYouData,
    ICarouselData
} from "sxmServices";

describe("CarouselComponent", () =>
{
    let fixture,
        component: CarouselComponent,
        favoriteListStoreService,
        neriticLinkService,
        onDemandListStoreService,
        channelLineupServiceMock,
        superCategoryForYouMock = mockForYouData,
        superCategoryListMock = [
        superCategoryForYouMock
    ];
    const fakeEl = { slick: jasmine.createSpy("slick"), on: jasmine.createSpy("on") };

    beforeEach(() =>
    {
        // Create the mock channel lineup testSubject.
        channelLineupServiceMock = {
            channelLineup: {
                superCategories: {
                    subscribe: (result: Function, fault: Function) => result(superCategoryListMock),
                    flatMap: (mapResult: Function) => mapResult(superCategoryListMock)
                },
                channels : {
                    subscribe: (result: Function, fault: Function) => result(mockChannelLineup),
                    flatMap: (mapResult: Function) => mapResult(mockChannelLineup)
                }

            },
            discoverAod: function ()
            {
                return observableOf([]);
            }
        };

        favoriteListStoreService = new FavoriteListStoreServiceMock();
        neriticLinkService =  new NeriticLinkServiceMock();
        onDemandListStoreService = new OnDemandListServiceMock();

        TestBed.configureTestingModule({
            declarations: [
                CarouselComponent
            ],
            providers: [
                { provide: NeriticLinkService, useValue: neriticLinkService },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: ChannelLineupService, useClass: ChannelLineupServiceMock }
            ]
        }).compileComponents();

        CarouselComponent.prototype.onCarouselDataChange = jasmine.createSpy("onCarouselDataChange");
        //CarouselComponent.prototype.carousels = observableOf(mockCarousel);
        // TODO will need to fix this up with a proper MOCK
        CarouselComponent.prototype.carouselData = CarouselServiceMock.contentCarouselMockData as any as ICarouselData[];
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(CarouselComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe("createCarousel", () =>
    {
        beforeEach(() =>
        {
            component.$ = jasmine.createSpy("$").and.returnValue(fakeEl) as any;
            component.createCarousel("selector", {});
        });

        it("uses the selector passed to create the carousel and sets the carousel as initialized", () =>
        {
            expect(fakeEl.slick).toHaveBeenCalled();
        });
    });

    describe("destroyCarousel()", () =>
    {
        beforeEach(() =>
        {
            component.destroyCarousel(fakeEl as any);
        });

        it("calls slick with 'unslick' on the passed carouselRef to destroy the given carousel", () =>
        {
            expect(fakeEl.slick).toHaveBeenCalledWith("unslick");
        });
    });

    describe("ngOnChanges()", () =>
    {
        it("calls destroyCarousel() to remove the Slick Carousel", () =>
        {
            spyOn(component, "destroyCarousel");
            component.ngOnChanges({});
            fixture.detectChanges();

            expect(component.carouselInitialized).toEqual(false);
        });
    });
});
