export const heroCarouselOptions = {
    variableWidth: true,
    infinite: false,
    removeOuterEdgeWhiteSpace: true,
    swipeToSlide: true
};

export const contentCarouselOptions = {
    infinite: false,
    variableWidth:true,
    removeOuterEdgeWhiteSpace: true,
    swipeToSlide: true
};

export const autoplayCarouselOptions = {
    slidesToScroll: 2,
    slidesToShow: 2,
    infinite: false,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToScroll: 1,
                slidesToShow: 1
            }
        }]
};

export const autoplayFullscreenCarouselOptions = {
    slidesToScroll: 3,
    slidesToShow: 3,
    infinite: false,
    responsive: [
        {
            breakpoint: 1440,
            settings: {
                slidesToScroll: 2,
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToScroll: 1,
                slidesToShow: 1
            }
        }]
};

export const freeTierCarouelOptions = {
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear'
};
