import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ViewAllComponent } from "./view-all.component";
import { VerticalListModule } from "../../../common/component/list-items/vertical-list/vertical-list.module";

@NgModule({
    imports: [
        CommonModule,
        VerticalListModule
    ],
    declarations: [
        ViewAllComponent
    ],
    exports: [
        ViewAllComponent
    ]
})

export class ViewAllModule
{
}

