import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {ICarouselData, ICarouselDataByType, Logger} from "sxmServices";
import {CarouselStoreService} from "../../../common/service/carousel.store.service";
import {CarouselService} from "../../carousel.service";
import {appRouteConstants} from "../../../app.route.constants";
import {NavigationService} from "../../../common/service/navigation.service";
import { SubscriptionLike as ISubscription, Observable } from "rxjs";
import { filter, map, mergeMap } from "rxjs/operators";

@Component({
    selector   : "view-all",
    templateUrl: "./view-all.component.html",
    styleUrls  : [ "./view-all.component.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ViewAllComponent implements OnInit
{
    public carousel: Observable<ICarouselData>;

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ViewAllComponent");

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * guid to find which carousel is being selected
     */
    private carouselGuid;
    /**
     * Constructor
     * @param {Router} router
     * @param {CarouselStoreService} carouselStoreService
     * @param {ActivatedRoute} activeRoute
     * @param {CarouselService} carouselService
     */
    constructor(private activeRoute: ActivatedRoute,
                public carouselService: CarouselService,
                private carouselStoreService: CarouselStoreService,
                private navigationService: NavigationService)
    {}

    /**
     * ngOnInit
     */
    ngOnInit(): void
    {
        ViewAllComponent.logger.debug("ngOnInit()");
        this.carouselGuid = this.activeRoute.snapshot.data.viewAllDetails.carouselGuid;
        this.carousel = this.carouselStoreService.selectViewAll(this.carouselGuid)
            .pipe(
                mergeMap( () => this.carouselStoreService.viewAllCarousels),
                filter(data => !!data && !!data.zone),
                map((data: ICarouselDataByType) =>
                {
                    if (data.error)
                    {
                        //Note: If in carousel guid is not valid and failed to get the data we navigate to Home route.
                        this.navigationService.go([ appRouteConstants.HOME ], { queryParamsHandling: "merge" });

                        return null;
                    }

                    let carouselData;

                    data.zone.forEach(zone=>
                    {
                        carouselData = zone.content.find( content => content.guid === this.carouselGuid)
                            || zone.hero.find( content => content.guid === this.carouselGuid);
                    });

                    return carouselData;
                })
            );
    }
}
