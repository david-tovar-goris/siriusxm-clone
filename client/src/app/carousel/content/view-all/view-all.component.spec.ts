import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { ActivatedRoute } from "@angular/router";
import { ViewAllComponent } from "./view-all.component";
import { MockComponent } from "../../../../../test/mocks/component.mock";
import { CarouselStoreService } from "../../../common/service/carousel.store.service";
import { CarouselStoreServiceMock } from "../../../../../test/mocks/carousel.store.service.mock";
import { CarouselService } from "../../carousel.service";
import { CarouselServiceMock } from "../../../../../test/mocks/carousel.service.mock";
import { NavigationService } from "../../../common/service/navigation.service";
import { navigationServiceMock } from "../../../../../test/mocks/navigation.service.mock";
import { SharedModule } from '../../../common/shared.module';

describe("ViewAllComponent()", () =>
{
    let component: ViewAllComponent,
    fixture: ComponentFixture<ViewAllComponent>,
    mockActivatedRoute;

    beforeEach(() =>
    {
        let data = { viewAllDetails: { carouselGuid: "0010" } };
        mockActivatedRoute = {
            snapshot: { data: data }
        };
        TestBed.configureTestingModule({
            imports: [
                SharedModule
            ],
            declarations: [
                ViewAllComponent,
                MockComponent({ selector: "vertical-list", inputs: [ "carouselData", "enableBackButton" ] })
            ],
            providers   : [
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: CarouselService, useClass: CarouselServiceMock },
                { provide: CarouselStoreService, useClass: CarouselStoreServiceMock },
                { provide: NavigationService, useValue: navigationServiceMock }
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(ViewAllComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe("setTileData()", () =>
    {
        describe("ngOnInit()", () =>
        {
            it("should pull carousels from the carousel store", () =>
            {
                component.carousel.subscribe(
                    data => expect(data).toEqual(CarouselStoreServiceMock.carouselMockData as any)
                );
            });
        });
    });
});
