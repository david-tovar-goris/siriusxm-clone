import { of as observableOf, Observable } from 'rxjs';
import { first, switchMap, skipWhile, combineLatest } from 'rxjs/operators';
import {
    Resolve,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from "@angular/router";

import { Injectable } from "@angular/core";
import { InitializationService, InitializationStatusCodes } from "sxmServices";
import { CarouselStoreService } from "app/common/service/carousel.store.service";

@Injectable()
export class ViewAllResolver implements Resolve<{}>
{
    constructor(private initService: InitializationService,
                private carouselStoreService: CarouselStoreService)
    {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    {
        return this.initService.initState.pipe(combineLatest(this.carouselStoreService.viewAllCarousels),
                   skipWhile((data) =>
                   {
                       return data[ 0 ] !== InitializationStatusCodes.RUNNING || data[ 1 ] === null;
                   }),
                   switchMap(() => resolveViewAllData(route)),
                   first());
    }
}

function resolveViewAllData(route: ActivatedRouteSnapshot): Observable<{}>
{
    return observableOf({
        carouselGuid: route.params[ 'carouselGuid' ]
    });
}
