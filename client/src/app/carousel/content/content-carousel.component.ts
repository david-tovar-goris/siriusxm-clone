import { debounceTime } from 'rxjs/operators';
import {
    Component,
    Input,
    ChangeDetectionStrategy,
    NgZone
} from "@angular/core";

import { CarouselComponent } from "../carousel.component";
import { ICarouselData, ChannelLineupService, ITile, CarouselOrientationTypeConsts } from 'sxmServices';
import { CMOption } from "../../context-menu/context-menu.interface";
import { appRouteConstants } from "../../app.route.constants";
import { NeriticLinkService } from "../../neritic-links";
import { carouselNameConstants } from "../carousel.const";
import { TranslateService } from "@ngx-translate/core";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { CarouselService } from "../carousel.service";
import { CapitalizeStringUtil } from "../../common/util/capitalizeString.util";
import { contentCarouselOptions } from "./slick-responsive-breakpoints";
import { NavigationService } from "../../common/service/navigation.service";
import { SlickCarouselOptions } from "../carousel.interface";
import { ProgressCursorService } from "../../common/service/progress-cursor/progress-cursor.service";
import { tileAssetUtil } from '../../common/util/tileAsset.util';
import { QueryParamsHandling } from "@angular/router/src/config";
import { Subject } from "rxjs";
import {
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsPageFrames,
    AnalyticsModals,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsContentTypes,
    AnalyticsTagTextConstants,
    AnalyticsZoneNames
} from "app/analytics/sxm-analytics-tag.constants";
import { BrowserUtil } from "../../common/util/browser.util";
import { TileContextMenuOptionsService } from "app/context-menu/options/tile-context-menu-options.service";


@Component({
    selector: "content-carousel",
    templateUrl: "./content-carousel.component.html",
    host: { '(window:resize)': 'this.resizeEvent$.next(event)' },
    styleUrls: ["./content-carousel.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class ContentCarouselComponent extends CarouselComponent
{
    @Input() cmOptions: CMOption[];
    @Input() searchText: string = null;
    @Input() pageName: string; // PageName tag for Analytics.

    private resizeEvent$: Subject<any> = new Subject<any>();

    // TODO: CWC we can make this more generic.  If we can create an array that will store all the different
    // content menus that are present for a given tile type, then we can set this up to be more data driven where
    // we setup the array in the constructor, and then take action based on what we find in the array in
    // getCMOptionsTile.  This would allow child classes to add or remove things from the context menus.
    //
    // However, doing that right now will require a significant change to this component.  Instead, we have this
    // protected property that allows us to control this one content menu item in an ad-hoc way.  If we find ourselves
    // doing a *lot* of this in the future, we should make a more general solution.  Also, a more general solution
    // may belong in CarouselComponent.
    protected moreEpisodesOnEpisodeTileContextMenus: boolean = true;

    /* Analytics Constants*/
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsContentTypes = AnalyticsContentTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsZoneNames = AnalyticsZoneNames;

    constructor(channelLineupService: ChannelLineupService,
                neriticLinkService: NeriticLinkService,
                translate: TranslateService,
                public tileContextMenuOptionsService: TileContextMenuOptionsService,
                private carouselStoreService: CarouselStoreService,
                public carouselService: CarouselService,
                private navigationService: NavigationService,
                public progressCursorService: ProgressCursorService,
                public ngZone: NgZone)
    {
        super(channelLineupService, neriticLinkService, translate, ngZone);
        this.zoneId = this.zoneId ? this.zoneId : '0';
        this.zoneOrder = this.zoneOrder ? this.zoneOrder : 0;
        this.zoneTitle = this.zoneTitle ? this.zoneTitle : "";

        /**
         * @see carousel.component.ts
         * onViewReady() is called AFTER view checked
         */
        this.onViewReady = () =>
        {
            this.carouselData.forEach((carousel, index) =>
            {
                if (carousel.carouselOrientation == CarouselOrientationTypeConsts.HORIZONTAL && carousel.tiles.length)
                {
                    const carouselSelector = `#content-carousel-${index}-${this.zoneId}`;
                    const opts = Object.assign(this.getCarouselOptions(carousel), {
                        prevArrow: `#content-carousel-${index}-${this.zoneId}-prev`,
                        nextArrow: `#content-carousel-${index}-${this.zoneId}-next`
                    });

                    this.carouselRefs.push(this.createCarousel(carouselSelector, opts));
                }
            });
            this.carouselInitialized = true;
        };

        this.resizeEvent$.pipe(debounceTime(500)).subscribe(() =>
        {
            this.carouselData.forEach((carousel, index) =>
            {
                if (carousel.carouselOrientation == CarouselOrientationTypeConsts.HORIZONTAL && carousel.tiles.length)
                {
                    const carouselSelector = `#content-carousel-${index}-${this.zoneId}`;
                    this.isElementVisible(carouselSelector);
                }
            });
            BrowserUtil.triggerVerticalScroll();
        });
    }

    /**
     * returns the content carousels.
     * @returns {Array<ICarouselData>}
     */
    public getContentCarousels(): Array<ICarouselData>
    {
        if (this.isCarouselAvailable() === false) return [];
        return this.carouselData;
    }

    public viewAllCarouselAction(carousel: ICarouselData): void
    {
        if (carousel.carouselViewAll.showViewAll && !!carousel.carouselViewAll.viewAllLink && carousel.carouselViewAll.viewAllLink.length > 0)
        {
            const neriticLink = this.neriticLinkService.getNeriticData(carousel.carouselViewAll.viewAllLink);
            this.neriticLinkService.takeAction(neriticLink);
        }
        else
        {
            this.contentCarouselAction(carousel);
        }
    }

    /**
     * perform different actions based on the type of content carousel
     * @param carousel
     */
    public contentCarouselAction(carousel: ICarouselData): void
    {
        let goto = [appRouteConstants.VIEW_ALL, carousel.guid];
        let extras = { queryParamsHandling : 'merge' as QueryParamsHandling };

        switch (carousel.title.textValue.toLowerCase())
        {
            case carouselNameConstants.RECENTLY_PLAYED:
                goto   = [ appRouteConstants.RECENTLY_PLAYED ];
                extras = undefined;
                break;
            case carouselNameConstants.RECENTLY_PANDORA_STATIONS:
                goto   = [ appRouteConstants.SEEDED_SETTINGS ];
                extras = undefined;
                break;
            default:
                this.progressCursorService.startSpinning();
                this.carouselStoreService.selectViewAll(carousel.guid);
                break;
        }

        this.navigationService.go(goto,extras);
    }

    public getCarouselOptions({ tiles = [] }: ICarouselData): SlickCarouselOptions
    {
        return contentCarouselOptions;
    }

    public isCarouselVertical(orientation: string) : boolean
    {
        return orientation === "vertical";
    }

    public trackCarousel(index : number, carousel : ICarouselData)
    {
        return (carousel) ? carousel.guid : index;
    }

    public trackTile(index : number, tile : ITile)
    {
        return (tile) ? tile.assetGuid : index;
    }

    public onCarouselTileClick(tile)
    {
        if(!this.isSliding)
        {
            this.carouselService.onClickNeriticLinkAction(tile);
        }
    }

    public getCMOptionsFunc(tile): () => CMOption[]
    {
        return function()
        {
            return this.tileContextMenuOptionsService.getCMOptions(tile);
        }.bind(this);
    }
}
