import { Component, NgZone } from "@angular/core";
import { ContentCarouselComponent } from "./content-carousel.component";
import { ChannelLineupService } from "sxmServices";
import { NeriticLinkService } from "../../neritic-links";
import { CMOption } from "../../context-menu/context-menu.interface";
import { TranslateService } from "@ngx-translate/core";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { CarouselService } from "../carousel.service";
import { NavigationService } from "../../common/service/navigation.service";
import { ProgressCursorService } from "../../common/service/progress-cursor/progress-cursor.service";
import { TileContextMenuOptionsService } from "app/context-menu/options/tile-context-menu-options.service";

@Component({
    selector: "sxm-howard-content-carousel",
    templateUrl: "./content-carousel.component.html",
    styleUrls: ["./content-carousel.component.scss"]
})
export class HowardContentCarouselComponent extends ContentCarouselComponent
{
    cmOptions: CMOption[];

    /**
     * Constructor
     * @param channelLineupService - channel line up
     * @param router - angular router, needed by the parent class
     * @param neriticLinkService - used to parse neritic links and determine what to do with them
     * @param translate - the angular translation service
     * @param contextMenuAnalyticsService - for reporting on content menu usage
     */
    constructor(channelLineupService: ChannelLineupService,
                neriticLinkService: NeriticLinkService,
                translate: TranslateService,
                tileContextMenuOptionsService: TileContextMenuOptionsService,
                carouselStoreService: CarouselStoreService,
                carouselService: CarouselService,
                navigationService: NavigationService,
                progressCursorService: ProgressCursorService,
                ngZone: NgZone)
    {
        super(channelLineupService,
              neriticLinkService,
              translate,
              tileContextMenuOptionsService,
              carouselStoreService,
              carouselService,
              navigationService,
              progressCursorService,
              ngZone);

        // The property below is used in the the parent class to determine if episode tile context menus should have
        // a "more episodes" menu item.  For the Howard Stern carousels we want to turn this off.
        this.moreEpisodesOnEpisodeTileContextMenus = false;
    }
}

