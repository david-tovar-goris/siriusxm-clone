import {
    SlickElement,
    ChannelLineupService,
    ITile,
    ICarouselData,
    PerformanceUtil
} from "sxmServices";
import {
    AfterViewChecked,
    Component,
    Input,
    OnChanges,
    NgZone
} from "@angular/core";
import * as $ from "jquery";
import "slick-carousel";
import { NeriticLinkService } from "../neritic-links";
import { TranslateService } from "@ngx-translate/core";
import { BrowserUtil } from "../common/util/browser.util";
import { Subject } from "rxjs";

@Component({
    template: ''
})
export class CarouselComponent implements OnChanges, AfterViewChecked
{
    carouselInitialized: boolean = false;

    /* while swiping tiles do not trigger tile click event.
     Always handle click on isSliding: false if tile has click event.*/
    isSliding:boolean = false;

    onCarouselDataChange: Function;
    onViewReady: Function;
    $ = $;

    @Input() carouselData: ICarouselData[];
    @Input() zoneId: string;
    @Input() zoneTitle: string;
    @Input() zoneOrder: number;

    carouselRefs: Array<SlickElement> = [];
    // initialize lazyload after slick init
    onSlickInit$ = new Subject() ;

    constructor(public channelLineupService: ChannelLineupService,
                public neriticLinkService: NeriticLinkService,
                public translate: TranslateService,
                public ngZone: NgZone)
    {}

    /**
     * creates the carousel.
     * @param cssSelector - css class name to be used for slick component
     * @param options - options to be used for slick component
     * @returns {SlickElement} - returns slick element
     */
    createCarousel(cssSelector: string, options: Object): SlickElement
    {
        this.$(cssSelector).on('init', (event, slick) =>
        {
            this.isElementVisible(cssSelector);
            this.onSlickInit$.next(true);
        });
        this.$(cssSelector).on('afterChange', (event, slick) =>
        {
            this.isSliding = false;
            BrowserUtil.triggerVerticalScroll();
            this.isElementVisible(cssSelector);
        });
        this.$(cssSelector).on('swipe', (event, slick) =>
        {
            this.isSliding = true;
        });

        let slickElement;

        this.ngZone.runOutsideAngular(() =>
        {
            slickElement = (this.$(cssSelector) as any).slick(options);
        });

        return slickElement;
    }

    isElementVisible(selector)
    {
        const holder = this.$(selector)[ 0 ];
        const elem   = this.$(selector).find('li').last()[ 0 ];
        if (holder && elem)
        {
            const elRect     = elem.getBoundingClientRect();
            const holderRect = holder.getBoundingClientRect();
            if (elRect.right*0.99 < holderRect.right)
            {
                this.hideRightNavigation(selector);
            }
            else
            {
                this.unhideRightNavigation(selector);
            }
        }
    }

    /**
    * hide right arrow for the carousel
    * @param selector - carousel id
    */
    hideRightNavigation(selector)
    {
        this.$(selector + '-next')
            .addClass('slick-disabled')
            .attr('aria-disabled', 'true');
    }

    /**
     * unhide right arrow for the carousel
     * @param selector - carousel id
     */
    unhideRightNavigation(selector)
    {
        this.$(selector + '-next')
            .removeClass('slick-disabled')
            .attr('aria-disabled', 'false');
    }

    /**
     * to track carousel by trackBy func - for performance , helps in adding or removing the carousel ref's
     * @param {index, ICarouselData}
     */
    public trackCarousel(index: number, carousel: ICarouselData): string | number
    {
        return (carousel) ? carousel.guid : index;
    }

    /**
     * to track the tile- for performance , helps in adding or removing the tiles
     * @param {index, ITile}
     */
    public trackTile(index: number, tile: ITile): string | number
    {
        return (tile) ? tile.assetGuid : index;
    }

    /**
     * destroy or removes the slick Carousel.
     * @param {SlickElement} carouselRef
     */
    destroyCarousel(carouselRef: SlickElement)
    {
        if(carouselRef)
        {
            // doing removeSlide causing the slick to break in search screen and doing unslick first and removeSlide
            // later does creating problems with smaller screen sizes
            // carouselRef.slick("removeSlide", null, null, true);
            this.ngZone.runOutsideAngular(() =>
            {
                carouselRef.slick("unslick");
            });
        }
    }

    /**
     * checks whether carousel data having tiles or not.
     * @returns {boolean}
     */
    public isCarouselAvailable(): boolean
    {
        return !!this.carouselData && this.carouselData.length > 0 && this.carouselData[0].tiles.length >0;
    }

    /**
     * ngAfterViewChecked
     */
    ngAfterViewChecked(): void
    {
        let thereIsNoCarousel = (!this.carouselData)
                                || (this.carouselData && !this.carouselData.length);

        if ((thereIsNoCarousel) || this.carouselInitialized) return;

        if (this.onViewReady) { this.onViewReady(); }

        PerformanceUtil.markMoment(PerformanceUtil.moments.FIRST_PAINT);
    }

    /**
     * ngOnChanges
     * unslick and empty the current carousel list if carouselInitialized already
     * exists and reset the carouselInitialized flag to false
     */
    ngOnChanges(change: any): void
    {
        if (this.carouselInitialized)
        {
            this.carouselRefs.forEach(carouselRef => this.destroyCarousel(carouselRef));
            this.carouselInitialized = false;
            this.carouselRefs        = [];
        }
    }
}
