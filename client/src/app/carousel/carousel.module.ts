import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HeroCarouselComponent } from "./hero/hero-carousel.component";
import { ContentCarouselComponent } from "./content/content-carousel.component";
import { HowardContentCarouselComponent } from "./content/howard-content-carousel-component";
import { SharedModule } from "../common/shared.module";
import { TranslationModule } from "../translate/translation.module";
import { NeriticLinkService } from "../neritic-links";
import { ViewAllModule } from "./content/view-all/view-all.module";
import { CarouselComponent } from "./carousel.component";
import { CarouselService } from "./carousel.service";
import { ContentTilesModule } from "../common/component/tiles/content-tiles.module";
import { ContextMenuModule } from "../context-menu/context-menu.module";
import { VerticalListModule } from "../common/component/list-items/vertical-list/vertical-list.module";

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        SharedModule,
        TranslationModule,
        ViewAllModule,
        ContentTilesModule,
        ContextMenuModule,
        VerticalListModule
    ],
    declarations: [
        CarouselComponent,
        HeroCarouselComponent,
        ContentCarouselComponent,
        HowardContentCarouselComponent
    ],
    exports: [
        HeroCarouselComponent,
        ContentCarouselComponent,
        HowardContentCarouselComponent
    ],
    providers: [
        NeriticLinkService,
        CarouselService
    ]
})

export class CarouselModule
{
}
