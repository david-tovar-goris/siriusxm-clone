import { skipWhile, takeWhile, filter, map, take, tap, delay, switchMap } from 'rxjs/operators';
import { Observable, combineLatest } from 'rxjs';
import * as screenfull from 'screenfull';

import {
    Component,
    Inject,
    HostListener,
    OnInit,
    OnDestroy,
    ViewContainerRef,
    ApplicationRef
} from "@angular/core";

import { Location, ViewportScroller } from "@angular/common";

import {
    NavigationEnd,
    Router,
    Scroll
} from "@angular/router";

import { TranslateService } from "@ngx-translate/core";

import {
    IAppConfig,
    InitializationService,
    Logger,
    InitializationStatusCodes,
    ResumeService,
    ApiCodes,
    AppMonitorService,
    AuthenticationService,
    ConfigService,
    IComponentConfig,
    ClientCodes,
    SxmAnalyticsService,
    EAction,
    TerminalAnalyticsTags,
    IExternalAsset,
    ApiLayerTypes,
    IInternalAsset
} from "sxmServices";
import { appRouteConstants } from "./app.route.constants";
import { LoadingScreenService } from "./common/service/loading-screen/loading-screen.service";
import { APP_CONFIG } from "./sxmservicelayer/sxm.service.layer.module";
import { SplashScreenService } from "app/common/service/splash-screen/splash-screen.service";
import { ModalService } from "./common/service/modal/modal.service";
import { ClientInactivityService } from "./inactivity/client-inactivity.service";
import { SingletonVideoService } from "./video/singleton-video/singleton-video.service";
import { TuneClientService } from "./common/service/tune/tune.client.service";
import { AlertClientService} from "./common/service/alert/alert.client.service";
import { PlaybackCompleteService } from "./autoplay/playback-complete.service";
import { FlepzScreenService } from "./common/service/flepz/flepz-screen.service";
import { NavigationService } from "./common/service/navigation.service";
import { DynamicAssetLoaderService } from "./common/service/dynamic-asset-loader.service";
import { ContextMenuService } from "./context-menu";
import { Title }     from '@angular/platform-browser';
import { StickyService } from "app/common/service/sticky/sticky.service";
import { OpenAccessOverlayService } from "app/open-access/popups/open-access-overlay/open-access-overlay.service";
import { WINDOW }  from "./common/service/window/window.service";
import { PWAUtil } from "./common/util/pwa.util";

declare global {
    interface Window
    {
        s: any;
        s_gi: Function;
        Visitor: any;
        ndsapi: any;
        kochavaAppId: any;
    }
}

@Component({
    moduleId: module.id,
    selector: "app-sxm-web-client",
    templateUrl: "./app.component.html",
    styleUrls: [ "./app.component.scss" ],
    providers: [],
    host: {
        '(window:message)': 'this.flepzScreenService.onMessageEvent($event)'
    }
})

export class AppComponent implements OnInit, OnDestroy
{
    @HostListener('window:unload', [ '$event' ]) unloadHandler() { this.ngOnDestroy(); }
    @HostListener('window:beforeunload', [ '$event' ]) beforeUnloadHandler() { this.ngOnDestroy(); }

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("AppComponent");

    public screenfull = screenfull;
    public defaultTitle: string = '';
    public titlePreamble: string = '';

    public paddingTopPx$: Observable<number> = combineLatest(
        this.router.events,
        this.stickyService.fullHeightPx$
    ).pipe(
        map(val =>
        {
            const px = val[1];
            if (!this.showNavControl)
            {
                return 0;
            }
            return px;
        })
    );

    /**
     * showNavControl
     * @type - used to show/hide nav control.
     */
    public get showNavControl(): boolean
    {
        return this.initializationService.state === InitializationStatusCodes.RUNNING
                && !this.location.path().includes(appRouteConstants.AUTH.LOGIN)
                && !this.location.path().includes(appRouteConstants.WELCOME)
                && !this.location.path().includes(appRouteConstants.FT_UPSELL)
                && !this.location.path().includes(appRouteConstants.FT_WELCOME)
                && !this.matchesOrigin(window.location.href, window.location.origin);
    }

    /*
        When the user interacts with carousel dots on the welcome page, the browser puts that in the history.
        The back button reverts the url to xxxxx.com/ with no route on there. We can't test the location.path()
        for the base url. This tests for exact match of window.location.href === window.location.origin.
     */
    private matchesOrigin(path, origin): boolean
    {
        let originMatch = path;
        if (path[path.length-1] === "/") originMatch = path.slice(0, -1);
        return originMatch === origin;
    }

    public get isFreeTier(): boolean
    {
        return this.config.isFreeTierEnable;
    }

    /**
     * subscriber used to subscribe router/navigation events.
     */
    private routerSubscriber: any;

    /**
     * used to show miniplayer based on tune service is successful or not
     */
    public showMiniPlaying: boolean = true;

    /**
     * @param config - injects the application config details.
     * @param initializationService - injects the initialization service.
     * @param translate - used to translate between languages.
     * @param router - used to switch between routes and gets the details of the route.
     * @param splashScreenService - used to display/banish the splash screen
     * @param containerRef - given to the splash screen service as the container for the splash screen
     * @param location - used to query the app route
     */
    constructor(@Inject(APP_CONFIG) private config: IAppConfig,
                @Inject(WINDOW) public window: Window,
                private initializationService: InitializationService,
                private authenticationService: AuthenticationService,
                private translate: TranslateService,
                private router: Router,
                private splashScreenService: SplashScreenService,
                private containerRef: ViewContainerRef,
                private location: Location,
                private sxmAnalyticsService: SxmAnalyticsService,
                public tuneClientService: TuneClientService,
                public appMonitorService: AppMonitorService,
                public modalService: ModalService,
                private navigationService: NavigationService,
                private resumeService: ResumeService,
                private clientInactivityService: ClientInactivityService,
                private titleService: Title,
                public viewContainerRef: ViewContainerRef,
                public singletonVideoService: SingletonVideoService,
                public loadingScreenService: LoadingScreenService,
                public configService: ConfigService,
                public alertClientService: AlertClientService,
                public flepzScreenService: FlepzScreenService,
                public playbackCompleteService: PlaybackCompleteService,
                public contextMenuService: ContextMenuService,
                public stickyService: StickyService,
                private viewportScroller: ViewportScroller,
                private openAccessOverlayService : OpenAccessOverlayService,
                private applicationRef: ApplicationRef,
                private dynamicAssetLoaderService: DynamicAssetLoaderService
            )
    {
        this.routerSubscriber = router.events
            .pipe(
                filter(event => event instanceof Scroll),
                delay(0)
            )
            .subscribe((event:Scroll) =>
            {
                if (event.position)
                {
                    viewportScroller.scrollToPosition(event.position);
                }
                else
                {
                    viewportScroller.scrollToPosition([0, 0]);
                }
            });

        const closeSplashSub = this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((event)  =>
            {
                if ((event as any).url.includes(appRouteConstants.AUTH.LOGIN))
                {
                    this.splashScreenService.closeSplashScreen();
                    closeSplashSub.unsubscribe();
                }
            });

        const paths = this.location.path().split("/");

        if (paths && paths[1] === appRouteConstants.HOME && paths.length > 2)
        {
           const urlParamStart = paths[2].indexOf("?");
           const ajsRouteStart = paths[2].indexOf("#");

           let superCategory = paths[2];

           if (urlParamStart >= 0 || ajsRouteStart >= 0)
           {
               const routeEnd = Math.min((urlParamStart >= 0) ? urlParamStart : superCategory.length,
                                         (ajsRouteStart >= 0) ? ajsRouteStart : superCategory.length);
               superCategory = paths[2].substring(0,routeEnd);
           }

           this.initializationService.superCategoryToPreCache = superCategory;
        }

        const sub = this.initializationService.initState.pipe(
                        takeWhile(() => (!this.location.path().includes(appRouteConstants.AUTH.LOGIN) &&
                            !this.location.path().includes(appRouteConstants.NOW_PLAYING))))
                        .subscribe(state =>
                        {
                            if (state === InitializationStatusCodes.RUNNING ||
                                state === InitializationStatusCodes.UNAUTHENTICATED ||
                                (state === InitializationStatusCodes.OPENACCESS && this.config.isFreeTierEnable))
                            {
                                sub.unsubscribe();
                                if(!this.config.isFreeTierEnable && this.config.contextualInfo.deepLink)
                                {
                                    // Show Open Access Overlay for deeplink
                                    if (this.authenticationService.isOpenAccessEligible())
                                    {
                                        this.openAccessOverlayService.open(this.applicationRef.components[0].instance.viewContainerRef);
                                    }
                                }
                                this.splashScreenService.closeSplashScreen();
                            }
                            else
                            {
                                this.splashScreenService.addDynamicSplashScreen(this.containerRef);
                            }
                        });

        this.initializeKochava();
        this.initializeNuDetect();
        PWAUtil.setManifestFile();

        let pageUrl = "";
        this.navigationService.routerEventUrl
            .pipe(
                filter(url => !!url),
                switchMap( url =>
                {
                    pageUrl = url;
                    return this.translate.get(['title.preamble','title.default']);
                }),
                map((title) =>
                {
                    this.titlePreamble = this.titlePreamble || title["title.preamble"];
                    this.defaultTitle = this.defaultTitle || title["title.default"];
                    return this.titlePreamble + this.urlToTitle(pageUrl);
                })
            )
            .subscribe(title =>
            {
                this.titleService.setTitle(title);
            });
    }

    /**
     * Maps different urls to page titles.
     */
    public urlToTitle(url: string): string
    {
        let title = this.defaultTitle;
        if (url.includes(appRouteConstants.HOME_FORYOU))
        {
            title = this.translate.instant('superCategory.foryou');
        }
        else if (url.includes(appRouteConstants.MUSIC))
        {
            title = this.translate.instant('superCategory.music');
        }
        else if (url.includes(appRouteConstants.ENTERTAINMENT))
        {
            title = this.translate.instant('superCategory.entertainment');
        }
        else if (url.includes(appRouteConstants.NEWS))
        {
            title = this.translate.instant('superCategory.news');
        }
        else if (url.includes(appRouteConstants.SPORTS))
        {
            title = this.translate.instant('superCategory.sports');
        }
        else if (url.includes(appRouteConstants.HOWARD_SUPERCATEGORY))
        {
            title = this.translate.instant('superCategory.howard');
        }
        else if (url.includes(appRouteConstants.SEARCH))
        {
            title = this.translate.instant('search.title');
        }
        else if (url.includes(appRouteConstants.FAVORITES))
        {
            title = this.translate.instant('favorites.favorites');
        }
        else if (url.includes(appRouteConstants.RECENTLY_PLAYED))
        {
            title = this.translate.instant('recentlyPlayed.recentlyPlayed');
        }
        else if (url.includes(appRouteConstants.ALL_CHANNELS))
        {
            title = this.translate.instant('allChannels.allChannels');
        }
        else if (url.includes(appRouteConstants.ALL_VIDEO))
        {
            title = this.translate.instant('allVideo.allVideo');
        }
        else if (url.includes(appRouteConstants.COLLECTION)
        && url.includes('channels_all')
        && url.includes('onlyAdditionalChannels'))
        {
            title = this.translate.instant('allXtraChannels.title');
        }
        else if (url.includes(appRouteConstants.REMINDER_SETTINGS))
        {
            title = this.translate.instant('profile.reminderSettings.reminderSettings');
        }
        else if (url.includes(appRouteConstants.MANAGE_SHOW_REMINDERS))
        {
            title = this.translate.instant('profile.reminderSettings.manageShowReminders');
        }
        else if (url.includes(appRouteConstants.MESSAGING_SETTINGS))
        {
            title = this.translate.instant('profile.settings.messaging');
        }
        else if (url.includes(appRouteConstants.APPLICATION_SETTINGS))
        {
            title = this.translate.instant('profile.applicationSettings.header');
        }
        else if (url.includes(appRouteConstants.SEEDED_SETTINGS))
        {
            title = this.translate.instant('profile.seededSettings.manageSeededStation');
        }
        else
        {
            title = this.defaultTitle;
        }
        return title;
    }


    /**
     * ngOnInit event triggered when AppComponent is initialized.
     */
    ngOnInit()
    {
        AppComponent.logger.debug(`ngOnInit()`);

        this.checkMiniPlayerIsEnabled();
    }


    /**
     * when ngOnDestroy called then subscriber is un subscribed.
     * #TODO: doesn't <app-sxm-web-client> only ever destroy on tab close?
     */
    ngOnDestroy()
    {
        /*
            Log terminal event (navigating away or closing tab/window) for analytics.
         */
        this.sxmAnalyticsService.fireTerminalEvent(
            TerminalAnalyticsTags.TERMINAL_EVENT_TYPE,
            TerminalAnalyticsTags.TERMINAL_EVENT_NAME,
            TerminalAnalyticsTags.TERMINAL_EVENT_DESC,
            TerminalAnalyticsTags.TERMINAL_EVENT_USERPATH,
            this.location.path(),
            EAction.CLOSE);

        this.routerSubscriber.unsubscribe();
        this.authenticationService.exit();
    }

    /**
     * TODO: Need to move this to player-component- once we resolve css padding when miniplayer is shown/hidden
     * Hide the Mini Now playing when we get unavailable content/channel.
     * and enable it back if we get success tune service
     */
    checkMiniPlayerIsEnabled()
    {
        this.tuneClientService.tuneResponseSubject.subscribe((response: number) =>
        {
            if (response === ClientCodes.SUCCESS) this.showMiniPlaying = true;
        });

        this.appMonitorService.faultStream.pipe(map(fault => fault.apiCode),filter(Boolean)).subscribe((apiCode: number) =>
        {
            switch (apiCode)
            {
                case ApiCodes.CHANNEL_NOT_AVAILABLE :
                case ApiCodes.AOD_EXPIRED_CONTENT :
                case ApiCodes.CONTENT_NOT_IN_SUBSCRIPTION_PACKAGE :
                case ApiCodes.CONTENT_NOT_IN_PACKAGE_BUSINESS :
                case ApiCodes.UNAVAILABLE_ONDEMAND_EPISODE:
                case ApiCodes.FLTT_STREAMING_GAME_BLACKOUT:
                case ApiCodes.FLTT_NO_STREAMING_ACCESS:
                {
                    this.showMiniPlaying = false;
                    break;
                }

                /**
                 * Minibar will not be visible if resume service does not have anything to resume to.
                 *
                 * Also, if a resume comes in later (after we have entered the running state) that does not have
                 * anything to play, We will not take away the minibar if we are currently playing something.
                 * This protects against errors in resume where we are just trying to refresh the user's session.
                 */
                case ApiCodes.UNAVAILABLE_RESUME_CONTENT:
                {
                    const appState = this.initializationService.state;

                    if (appState != InitializationStatusCodes.RUNNING)
                    {
                        this.initializationService
                                        .initState.pipe(
                                        skipWhile((state : string) : boolean => state !== InitializationStatusCodes.RUNNING),
                                        take(1))
                                        .subscribe((state : string) =>
                                        {
                                            /**
                                             * Note, if the mini player is disabled because we have no content from resume, we could
                                             * have a problem if the user was refreshing to the now playing page.  Rather than check
                                             * what page we are going to, just route the user to the for you page if we ever detect that
                                             * we have been resumed with no content to play.
                                             * */
                                            if(location.pathname.includes(appRouteConstants.NOW_PLAYING))
                                            {
                                                this.navigationService.go([ appRouteConstants.HOME ]);
                                            }
                                        });

                        this.showMiniPlaying = false;
                    }
                }
            }
        });
    }

    private initializeKochava()
    {
        let dirPath = "assets/kochava/kochava-web-analytics.min.js";

        this.window.kochavaAppId = this.config.deviceInfo.appRegion === ApiLayerTypes.REGION_CA?
                              "kosiriusxm-canada-web-za3aormn" : "kosiriusxm-web-gj9tt5usc";

        const kochavaAsset: IInternalAsset = {
            kind: "internal",
            name: "kochava",
            dirPath: dirPath
        } as IInternalAsset;

        this.dynamicAssetLoaderService.loadInternalScript(kochavaAsset);
    }

    private initializeNuDetect()
    {

        this.configService.nuDetect$().subscribe((nuDetectConfig: IComponentConfig) =>
        {
            let nuDetectEnabled;
            let ndClientId;
            let ndBaseUrl;

            /**
             * Iterate through all the nudetect settings and found out if nudetecty is enabled and what the nudetect
             * parameters are.
             */

            nuDetectConfig.settings.forEach((setting) =>
            {
                nuDetectEnabled = setting.enableNudetect === true ? true : nuDetectEnabled;

                if (!!setting.nuDetect)
                {
                    ndClientId = setting.nuDetect.websiteId;
                    ndBaseUrl = setting.nuDetect.url;
                }
            });

            /**
             * If nudetect is enabled and we have the parameters we need then we can initialize nudetect
             */
            if (!!nuDetectEnabled && !!ndBaseUrl && !!ndClientId)
            {
                const url = `${ndBaseUrl}/2.2/w/${ndClientId}/sync/js/`;
                const nds: any = {};
                this.config.nuDetect.api = this.window.ndsapi = nds;

                nds.config = {
                    q: [],
                    ready: function (cb)
                    {
                        this.q.push(cb);
                    }
                };

                const nuDetectAsset: IExternalAsset = {
                    kind: "external",
                    name: "nuDetect",
                    url: url,
                    onLoadCallback: () =>
                    {
                        nds.load(url);
                    }
                };

                this.dynamicAssetLoaderService.loadExternalScript(nuDetectAsset);

                nds.config.ready(() =>
                {
                    this.config.nuDetect.sessionId = this.config.uniqueSessionId;
                    nds.setSessionId(this.config.uniqueSessionId);
                    nds.setPlacement("Login");
                    nds.setPlacementPage("1");

                    if (typeof this.config.nuDetect.ndsReadyCallback === 'function')
                    {
                        this.config.nuDetect.ndsReadyCallback();
                    }

                    this.config.nuDetect.ndsReady = true;
                });

             }
        });
    }

    /**
     * If context menu is in full screen mode we have to put it as a
     * child of the singleton video player
     */
    private isCMInFullscreenMode(): boolean
    {
        return this.contextMenuService.contextMenuIsOpen() && this.screenfull.isFullscreen;
    }


    /**
     * If the context menu is not in full screen mode we put in the root of the app.
     */
    private isCMInDefaultMode(): boolean
    {
        return this.contextMenuService.contextMenuIsOpen() && !this.screenfull.isFullscreen;
    }
}
