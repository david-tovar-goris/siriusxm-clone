import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { NoResultsModule } from "../no-results/no-results.module";
import { FilterModule } from "../filter/filter.module";
import { ChannelTileModule } from "../common/component/list-items/channel/channel-list-item.module";
import { SharedModule } from "../common/shared.module";
import { TranslationModule } from "../translate/translation.module";
import { CategoryPageComponent } from "./category-page.component";
import { CarouselModule } from "../carousel/carousel.module";
import { EpisodeTileModule } from "../common/component/list-items/episode/episode-list-item.module";
import { CategoryPageResolver } from "app/category-page/category-page.resolver";
import { ContentTilesModule } from "app/common/component/tiles/content-tiles.module";

@NgModule({
    imports     : [
        CommonModule,
        TranslationModule,
        CarouselModule,
        NoResultsModule,
        FilterModule,
        EpisodeTileModule,
        ChannelTileModule,
        SharedModule,
        ContentTilesModule
    ],
    declarations: [
        CategoryPageComponent
    ],
    exports     : [
        CategoryPageComponent
    ],
    providers: [
        CategoryPageResolver
    ]
})
export class CategoryPageModule
{
}
