import {Component, OnInit}      from "@angular/core";
import {SubscriptionLike as ISubscription}          from "rxjs";
import {AutoUnSubscribe}        from "../common/decorator";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import {
    CarouselTypeConst,
    neriticActionConstants,
    ICarouselDataByType
} from "sxmServices";
import { appRouteConstants }    from "../app.route.constants";
import {NavigationService}      from "../common/service/navigation.service";
import { Observable } from "rxjs";
import { filter, map } from "rxjs/operators";
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import { displayViewType } from "app/channel-list/component/display.view.type";
import { CarouselService } from "app/carousel/carousel.service";

/**
 * @MODULE:     client
 * @CREATED:    03/16/18
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *    AllOnDemandComponent used to load on All on vedio episodes and shows
 */

@Component({
    templateUrl: "./category-page.component.html",
    styleUrls  : [ "./category-page.component.scss" ]
})

export class CategoryPageComponent implements OnInit
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    public carouselData$: Observable<ICarouselDataByType>;

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    public pageTitle:string = "";

    /**
     * Constructor -
     * @param {CarouselStoreService} carouselStoreService
     */
    constructor(public carouselStoreService: CarouselStoreService,
                private route: ActivatedRoute,
                private translateService: TranslateService,
                public navigationService: NavigationService,
                private carouselService: CarouselService)
    {
    }

    /**
     * ngOnInit
     */
    ngOnInit()
    {
        this.carouselData$ = this.route.data.pipe(filter(data => !!data && !!data.collectionDetails), map((data: any ) =>
        {
            if (data.collectionDetails.error)
            {
                //Note: If in carousel guid is not valid and failed to get the data we navigate to Home route.
                this.navigationService.go([ appRouteConstants.HOME ], { queryParamsHandling: "merge" });

                return null;
            }
            this.pageTitle = data && data.collectionDetails.pageTitle ? data.collectionDetails.pageTitle.textValue : "";
            if (!data.collectionDetails.zone || !data.collectionDetails.zone.length)
            {
                return null;
            }
            return data.collectionDetails;
        }));
        this.route.params.subscribe(params =>
        {
            if (this.pageTitle.length === 0 && params.categoryType)
            {
                if (params.categoryType === appRouteConstants.ALL_VIDEO)
                {
                    this.pageTitle = this.translateService.instant("allVideo.allVideo");
                }
                else if (params.categoryType === neriticActionConstants.MEGA_STARS)
                {
                    this.pageTitle = this.translateService.instant("common.megaStar");
                }
                else if (params.categoryType === appRouteConstants.ALL_PODCASTS)
                {
                    this.pageTitle = this.translateService.instant("common.allPodCasts");
                }
            }
        });
    }
}
