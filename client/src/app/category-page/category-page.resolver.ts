import { Injectable } from "@angular/core";
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot
} from "@angular/router";
import { Observable, of } from "rxjs";
import { skipWhile, switchMap, first, filter } from "rxjs/operators";
import {
    ICarouselDataByType,
    InitializationService,
    InitializationStatusCodes,
    neriticActionConstants
} from "sxmServices";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { SplashScreenService } from "../common/service/splash-screen/splash-screen.service";
import { appRouteConstants } from "app/app.route.constants";

@Injectable()
export class CategoryPageResolver implements Resolve<{}>
{
    constructor(private initService: InitializationService,
                private carouselStoreService: CarouselStoreService,
                private splashScreenService: SplashScreenService)
    {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    {
        return this.initService.initState.pipe(
           skipWhile((data) => data !== InitializationStatusCodes.RUNNING),
           switchMap(() => resolveCategoryPageData(route, this.splashScreenService, this.carouselStoreService)),
           first()
        );
    }
}

function resolveCategoryPageData(route: ActivatedRouteSnapshot,
                                   splashScreenService: SplashScreenService,
                                   carouselStoreService:CarouselStoreService): Observable<ICarouselDataByType>
{
    splashScreenService.closeSplashScreen();

    if (route.params.categoryType)
    {
        if (route.params.categoryType === appRouteConstants.ALL_VIDEO)
        {
            return carouselStoreService.selectAllVideoCarousels().pipe(filter(data => !!data));
        }
        else if (route.params.categoryType === neriticActionConstants.MEGA_STARS)
        {
            return carouselStoreService.selectViewAll(route.params.categoryType);
        }
        else if (route.params.categoryType === appRouteConstants.ALL_PODCASTS)
        {
            return carouselStoreService.selectAllPodCastsCarousels().pipe(filter(data => !!data));
        }
        else
        {
            return of(null);
        }
    }
}
