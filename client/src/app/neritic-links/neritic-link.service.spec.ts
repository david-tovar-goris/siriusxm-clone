import { inject, TestBed } from "@angular/core/testing";
import {
    ContentTypes,
    ChannelLineupService,
    ITile,
    mockNormalizedTile,
    INeriticLinkData,
    IAodEpisode,
    ConfigService,
    TunePayload,
    IChannel,
    CarouselConsts,
    CarouselTypeConst
} from "sxmServices";
import {
    ChannelLineupServiceMock,
    ChannelListServiceMock
} from "../../../test/mocks";
import { neriticLinkDataMock } from "../../../test/mocks/data/neritic-link-data.mock";
import { TuneServiceMock } from "../../../test/mocks/tune.service.mock";
import { appRouteConstants } from "../app.route.constants";
import { ChannelListStoreService } from "../common/service/channel-list.store.service";
import { TuneClientService } from "../common/service/tune/tune.client.service";
import { NeriticLinkService } from "./neritic-link.service";
import { displayViewType } from "../channel-list/component/display.view.type";
import { FavoriteListStoreService } from "../common/service/favorite-list.store.service";
import { FavoriteListStoreServiceMock } from "../../../test/mocks/favorite-list.store.service.mock";
import { AlertServiceMock } from "../../../test/mocks/alert.service.mock";
import { createChannel } from "../../../test/mocks";
import { RecentlyPlayedStoreServiceMock } from "../../../test/mocks/recently-played.store.service.mock";
import { RecentlyPlayedStoreService } from "../common/service/recently-played.store.service";
import { AlertClientService } from "../common/service/alert/alert.client.service";
import { ConfigServiceMock } from "../../../test/mocks/config.service.mock";
import { NavigationService } from "../common/service/navigation.service";
import { navigationServiceMock } from "../../../test/mocks/navigation.service.mock";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { ToastService } from "../common/service/toast/toast.service";
import { ToastServiceMock } from "../../../test/mocks/toast.service.mock";
import { AppMonitorService } from "sxmServices";
import { NowPlayingStoreService } from "../common/service/now-playing.store.service";
import { neriticActionConstants } from "../../../../servicelib/src/service/consts/neritic-action-const";
import { NowPlayingStoreServiceMock } from "../../../test/mocks/now-playing.store.service.mock";
import { of as observableOf } from "rxjs";
import { SeededStationsClientService } from "../common/service/seeded-stations/seeded-stations.client.service";
import { SeededStationsServiceMock } from "../../../test/mocks/seeded-stations.service.mock";
import { ModalService } from "../common/service/modal/modal.service";
import { ModalServiceMock } from "../../../test/mocks/modal.service.mock";
import { TranslateService }        from "@ngx-translate/core";
import { MockTranslateService }        from "../../../test/mocks/translate.service";

import * as _ from 'lodash';

describe("NeriticLinkService", () =>
{
    let service: NeriticLinkService,
        channelLineupService: ChannelLineupService,
        favoriteListStoreService: FavoriteListStoreService,
        recentlyPlayedStoreService: RecentlyPlayedStoreService,
        tuneClientService: TuneClientService,
        carouselStoreServiceMock,
        channelListStoreService: ChannelListStoreService;

    let netriticLinkMockClone;

    beforeEach(() =>
    {
        netriticLinkMockClone = _.cloneDeep(neriticLinkDataMock);

        netriticLinkMockClone.channelId = "testId";
        carouselStoreServiceMock = {
            selectShow : jasmine.createSpy("selectShow"),
            selectSubCategory : jasmine.createSpy("selectSubCategory")
        };

        this.appMonitorService = {

        };

        TestBed.configureTestingModule({
            providers: [
                NeriticLinkService,
                { provide: AlertClientService, useClass: AlertServiceMock },
                { provide: ChannelLineupService, useClass: ChannelLineupServiceMock },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: ConfigService, useClass: ConfigServiceMock },
                { provide: FavoriteListStoreService, useClass: FavoriteListStoreServiceMock },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: RecentlyPlayedStoreService, useClass: RecentlyPlayedStoreServiceMock},
                { provide: TuneClientService, useClass: TuneServiceMock },
                { provide: CarouselStoreService, useValue: carouselStoreServiceMock },
                { provide: ToastService, useValue: ToastServiceMock.getSpy() },
                { provide: AppMonitorService, useValue: this.appMonitorService },
                { provide: NowPlayingStoreService, useClass: NowPlayingStoreServiceMock },
                { provide: SeededStationsClientService, useClass: SeededStationsServiceMock },
                { provide: ModalService, useClass: ModalServiceMock },
                { provide: TranslateService, useClass: MockTranslateService }
            ]
        });
        service = TestBed.get(NeriticLinkService);

        channelLineupService = TestBed.get(ChannelLineupService);
        favoriteListStoreService = TestBed.get(FavoriteListStoreService);
        recentlyPlayedStoreService = TestBed.get(RecentlyPlayedStoreService);
        tuneClientService = TestBed.get(TuneClientService);
        channelListStoreService = TestBed.get(ChannelListStoreService);
    });

    describe("takePrimaryTileAction()", () =>
    {
        describe("when the linkType is API", () =>
        {
            it("should take the api action", () =>
            {
                spyOn(service, "takeApiAction");
                mockNormalizedTile.primaryNeriticLink.linkType = "Api";
                service.takePrimaryTileAction(mockNormalizedTile as ITile);
                expect(service.takeApiAction).toHaveBeenCalled();
            });
        });

        describe("when the linkType is APP", () =>
        {
            it("should take the app action", () =>
            {
                spyOn(service, "takeTileAppAction");
                mockNormalizedTile.primaryNeriticLink.linkType = "App";
                service.takePrimaryTileAction(mockNormalizedTile as ITile);
                expect(service.takeTileAppAction).toHaveBeenCalled();
            });
        });

    });

    describe("takeApiAction()", () =>
    {
        describe("when the actionType is Tune", () =>
        {
            it("should call to tune", () =>
            {
                spyOn(service, "tuneNeriticAction");
                netriticLinkMockClone.actionType = "tune";
                service.takeApiAction(netriticLinkMockClone);
                expect(service.tuneNeriticAction).toHaveBeenCalled();
            });

        });

    });

    describe("takeTileAppAction()", () =>
    {
        describe("when the actionType is ArchiveViewEpisodes", () =>
        {
            it("should call to tune", () =>
            {
                spyOn(service, "navigateTileNeriticAction");
                mockNormalizedTile.primaryNeriticLink.actionType = "ArchiveViewEpisodes";
                service.takeTileAppAction(mockNormalizedTile as ITile);
                expect(service.navigateTileNeriticAction).toHaveBeenCalled();
            });

        });
    });

    describe("tuneNeriticAction()", () =>
    {
        beforeEach(() =>
        {
            spyOn(tuneClientService, "tune").and.returnValue(observableOf(true));
        });

        describe("when the contentType is a Live Show", () =>
        {
            it("tunes to Now Playing", () =>
            {
                const payload: TunePayload = {
                    channelId: "testId",
                    showGuid: "",
                    cutAssetGuid: "",
                    startTime : netriticLinkMockClone.startTime,
                    contentType: ContentTypes.LIVE_AUDIO,
                    channel: { channelNumber: 100 } as IChannel,
                    isPandoraPodcast: false
                };

                netriticLinkMockClone.contentType = "show";
                netriticLinkMockClone.contentSubType = "live";
                service.tuneNeriticAction(netriticLinkMockClone, mockNormalizedTile as ITile);
                expect(tuneClientService.tune).toHaveBeenCalledWith(payload);
            });
        });

        describe("when contentType is liveAudio", () =>
        {
            it("tunes to Now Playing - Live Channel with Live Audio", () =>
            {
                const payload: TunePayload = {
                    channelId: "testId",
                    showGuid: "",
                    cutAssetGuid: "",
                    startTime : netriticLinkMockClone.startTime,
                    contentType: ContentTypes.LIVE_AUDIO,
                    channel: { channelNumber: 100 } as IChannel,
                    isPandoraPodcast: false
                };

                netriticLinkMockClone.contentType = neriticActionConstants.LIVE_AUDIO;
                service.tuneNeriticAction(netriticLinkMockClone , mockNormalizedTile as ITile);
                expect(tuneClientService.tune).toHaveBeenCalledWith(payload);
            });
        });

        describe("when the contentSubType is liveVideo", () =>
        {
            it("tunes to Now Playing - Live Video", () =>
            {

                const payload: TunePayload = {
                    channelId: "testId",
                    showGuid: "",
                    cutAssetGuid: "",
                    startTime : netriticLinkMockClone.startTime,
                    contentType: ContentTypes.LIVE_VIDEO,
                    channel: { channelNumber: 100 } as IChannel,
                    isPandoraPodcast: false
                };
                netriticLinkMockClone.contentType = neriticActionConstants.SHOW;
                netriticLinkMockClone.contentSubType = neriticActionConstants.LIVE_VIDEO;
                service.tuneNeriticAction(netriticLinkMockClone, mockNormalizedTile as ITile);
                expect(tuneClientService.tune).toHaveBeenCalledWith(payload);
            });
        });

        describe("when the contentType is liveVideo", () =>
        {
            it("tunes to Now Playing - Live Video", () =>
            {

                const payload: TunePayload = {
                    channelId: "testId",
                    showGuid: "",
                    cutAssetGuid: "",
                    startTime : netriticLinkMockClone.startTime,
                    contentType: ContentTypes.LIVE_VIDEO,
                    channel: { channelNumber: 100 } as IChannel,
                    isPandoraPodcast: false
                };
                netriticLinkMockClone.contentType = neriticActionConstants.LIVE_VIDEO;
                service.tuneNeriticAction(netriticLinkMockClone, mockNormalizedTile as ITile);
                expect(tuneClientService.tune).toHaveBeenCalledWith(payload);
            });
        });

        describe("when the contentType is an AOD Episode", () =>
        {
            beforeEach(() =>
            {
                netriticLinkMockClone.contentType = ContentTypes.AOD;
            });

            it("tunes to Now Playing if the asset is available", () =>
            {
                const payload: TunePayload = {
                    channelId: "testId",
                    showGuid: "",
                    cutAssetGuid: "",
                    contentType: ContentTypes.AOD,
                    channel: { channelNumber: 100 } as IChannel,
                    episodeIdentifier: '',
                    isPandoraPodcast: false
                };

                service.tuneNeriticAction(netriticLinkMockClone, mockNormalizedTile as ITile);
                expect(tuneClientService.tune).toHaveBeenCalledWith(payload);
            });
        });

        describe("when the contentType is a VOD", () =>
        {
            beforeEach(() =>
            {
                netriticLinkMockClone.contentType = ContentTypes.VOD;
            });

            it("tunes to Now Playing  if the asset is available", () =>
            {
                const payload: TunePayload = {
                    channelId: "testId",
                    showGuid: "",
                    cutAssetGuid: "",
                    contentType: ContentTypes.VOD,
                    channel: { channelNumber: 100 } as IChannel,
                    episodeIdentifier: '',
                    isPandoraPodcast: false
                };

                service.tuneNeriticAction(netriticLinkMockClone, mockNormalizedTile as ITile);
                expect(tuneClientService.tune).toHaveBeenCalledWith(payload);
            });
        });
    });

    describe("navigateTileNeriticAction()", () =>
    {
        describe("when contentType is show", () =>
        {
            beforeEach(() =>
            {
                mockNormalizedTile.primaryNeriticLink = {
                    analyticsTag: "",
                    functionalGroup: "primaryAction",
                    linkType: "Api",
                    actionType: "tune",
                    contentType: "show",
                    channelId: "9138",
                    assetGuid: "DSADS767687687DSADSA"
                };
            });

            describe("when the contentSubType is aod", () =>
            {
                it("should navigate to the audio on demand episode listing",
                    inject([ NavigationService ], (navigationService: NavigationService) =>
                {
                    spyOn(navigationService, "go");

                    const mockNeriticLinkData: INeriticLinkData = mockNormalizedTile.primaryNeriticLink;
                    mockNeriticLinkData.contentSubType = ContentTypes.AOD;
                    service.navigateTileNeriticAction(mockNormalizedTile as ITile);
                    const url =`${CarouselConsts.PAGE_NAME}=${CarouselTypeConst.EPISODE_EDP}&showGuid=${mockNeriticLinkData.assetGuid}`;
                    expect(navigationService.go).toHaveBeenCalledWith([
                        appRouteConstants.ON_DEMAND.EPISODES_LIST,
                        mockNeriticLinkData.channelId,
                        mockNeriticLinkData.assetGuid,
                        appRouteConstants.ON_DEMAND.AUDIO,
                        url
                    ]);
                }));
            });

            describe("when the contentSubType is not aod", () =>
            {
                it("should navigate to the video on demand episode listing",
                    inject([ NavigationService ], (navigationService: NavigationService) =>
                {
                    spyOn(navigationService, "go");

                    const mockNeriticLinkData: INeriticLinkData = mockNormalizedTile.primaryNeriticLink;
                    mockNeriticLinkData.contentSubType = ContentTypes.VOD;
                    service.navigateTileNeriticAction(mockNormalizedTile as ITile);
                    const url =`${CarouselConsts.PAGE_NAME}=${CarouselTypeConst.EPISODE_EDP}&showGuid=${mockNeriticLinkData.assetGuid}`;
                    expect(navigationService.go).toHaveBeenCalledWith([
                        appRouteConstants.ON_DEMAND.EPISODES_LIST,
                        mockNeriticLinkData.channelId,
                        mockNeriticLinkData.assetGuid,
                        appRouteConstants.ON_DEMAND.VIDEO,
                        url
                    ]);
                }));
            });
        });

        describe("when contentType is category", () =>
        {
            beforeEach(() =>
            {
                mockNormalizedTile.primaryNeriticLink.contentType = "category";
            });

            it("should navigate to the category page",
                inject([ NavigationService ], (navigationService: NavigationService) =>
            {
                spyOn(navigationService, "go");

                const mockNeriticLinkData: INeriticLinkData = mockNormalizedTile.primaryNeriticLink;
                service.navigateTileNeriticAction(mockNormalizedTile as ITile);

                expect(navigationService.go).toHaveBeenCalledWith([
                    appRouteConstants.CATEGORY,
                    mockNormalizedTile.superCategoryInfo.key,
                    mockNormalizedTile.subCategoryInfo.key,
                    displayViewType.channels
                ]);
            }));
        });
    });

    describe("takeSecondaryTileAction()", () =>
    {
        let tile;
        beforeEach(() =>
        {
          tile = {};
          spyOn(service, 'navigateToEpisodeListing');
          spyOn(service, 'navigateToEnhancedEdpPage');
          spyOn(service, 'navigateToShowsList');
          spyOn(service, 'updateShowReminder');
          spyOn(service, 'toggleFavorite');
        });

        describe("when the neriticLinkData.functionalGroup is onDemandEpisodes", () =>
        {
            it('calls navigateToEpisodeListing with the showInfo', () =>
            {
              tile.showInfo = {};
              tile.tileAssetInfo ={
                  channelId : "channelId",
                  showGuid : "showGuid"
              };

              service.takeSecondaryTileAction(tile, { functionalGroup: 'onDemandEpisodes'});
              expect(service.navigateToEpisodeListing).toHaveBeenCalledWith(tile.tileAssetInfo.channelId ,
                  tile.tileAssetInfo.showGuid, CarouselTypeConst.EPISODE_EDP,
                appRouteConstants.ON_DEMAND.VIDEO_AND_AUDIO);
            });
        });

        describe("when the neriticLinkData.functionalGroup is moreEpisodes", () =>
        {
            it('calls navigateToEpisodeListing with the aodEpisodeInfo.show', () =>
            {
                tile.showInfo = {};
                tile.tileAssetInfo ={
                    channelId : "channelId",
                    showGuid : "showGuid"
                };
                service.takeSecondaryTileAction(tile, { functionalGroup: 'moreEpisodes'});
              expect(service.navigateToEpisodeListing).toHaveBeenCalledWith(tile.tileAssetInfo.channelId,
                    tile.tileAssetInfo.showGuid, CarouselTypeConst.EPISODE_EDP,
                  appRouteConstants.ON_DEMAND.VIDEO_AND_AUDIO);
            });
        });

        describe("when the neriticLinkData.functionalGroup is onDemandShows", () =>
        {
            it('calls navigateToShowsList with the channelInfo', () =>
            {
              tile.channelInfo = {};
              service.takeSecondaryTileAction(tile, { functionalGroup: 'onDemandShows'});
              expect(service.navigateToShowsList).toHaveBeenCalledWith(tile.channelInfo);
            });
        });


        describe("when the neriticLinkData.functionalGroup is channelDetails", () =>
        {
            it('calls navigateToEnhancedEdpPage with the url', () =>
            {
                tile.neriticLinkData ={
                    actionType: ""
                };
                service.takeSecondaryTileAction(tile, { functionalGroup: 'channelDetails', actionType: ""});
                expect(service.navigateToEnhancedEdpPage).toHaveBeenCalledWith(tile.neriticLinkData.actionType);
            });
        });

        describe("when the neriticLinkData.functionalGroup is showDetails", () =>
        {
            it('calls navigateToEnhancedEdpPage with the url', () =>
            {
                tile.neriticLinkData ={
                    actionType: ""
                };
                service.takeSecondaryTileAction(tile, { functionalGroup: 'showDetails', actionType: ""});
                expect(service.navigateToEnhancedEdpPage).toHaveBeenCalledWith(tile.neriticLinkData.actionType);
            });
        });

        describe("when the neriticLinkData.functionalGroup is episodeDetails", () =>
        {
            it('calls navigateToEnhancedEdpPage with the url', () =>
            {
                tile.neriticLinkData ={
                    actionType: ""
                };
                service.takeSecondaryTileAction(tile, { functionalGroup: 'episodeDetails', actionType: ""});
                expect(service.navigateToEnhancedEdpPage).toHaveBeenCalledWith(tile.neriticLinkData.actionType);
            });
        });


        describe("when the neriticLinkData.functionalGroup is setShowReminder", () =>
        {
            it('calls setShowReminder', () =>
            {
              tile.reminders = { showReminderSet: false };
              service.takeSecondaryTileAction(tile, { functionalGroup: 'setShowReminder'});
              expect(service.updateShowReminder).toHaveBeenCalled();
            });
        });

        describe("when the neriticLinkData.functionalGroup is addToFavorites", () =>
        {
            it('calls toggleFavorite with the channelInfo/showInfo/episodeInfo', () =>
            {
                tile.tileAssetInfo ={
                    channelId : "channelId",
                    showGuid : "showGuid"
                };
              service.takeSecondaryTileAction(tile, { functionalGroup: 'addToFavorites'});
              expect(service.toggleFavorite).toHaveBeenCalled();
            });
        });

        describe("when the neriticLinkData.functionalGroup is addShowToFavorites", () =>
        {
            it('calls toggleFavorite with the aodEpisodeInfo.show', () =>
            {
                tile.tileAssetInfo ={
                    channelId : "channelId",
                    showGuid : "showGuid",
                    imageAltText: 'Howard Stern Interviews',
                    isPandoraPodcast: false
                };
              service.takeSecondaryTileAction(tile, { functionalGroup: 'addShowToFavorites'});
              expect(service.toggleFavorite).toHaveBeenCalledWith(undefined, "show", "show",
                    tile.tileAssetInfo.channelId, tile.tileAssetInfo.showGuid, tile.imageAltText, tile.tileAssetInfo.isPandoraPodcast);
            });
        });
    });

    describe("navigateToEpisodeListing()", () =>
    {
        it("should route to the on demand episode list page",
            inject([ NavigationService ], (navigationService: NavigationService) =>
        {
            spyOn(navigationService, "go");

            const showGuid = "e44a5023-9ffa-4b28-b64b4-0671cf3c6655";
            const episode = {
                show: {
                    isFavorite: true,
                    relatedChannelIds: ["howard100"],
                    guid:showGuid
                }
            } as IAodEpisode;
            service.navigateToEpisodeListing(episode.show.relatedChannelIds[0],
                episode.show.guid,
                CarouselTypeConst.EPISODE_EDP,
                appRouteConstants.ON_DEMAND.VIDEO_AND_AUDIO);
            const url =`${CarouselConsts.PAGE_NAME}=${CarouselTypeConst.EPISODE_EDP}&showGuid=${episode.show.guid}`;

            expect(navigationService.go).toHaveBeenCalledWith([
                appRouteConstants.ON_DEMAND.EPISODES_LIST,
                episode.show.relatedChannelIds[0],
                episode.show.guid,
                appRouteConstants.ON_DEMAND.VIDEO_AND_AUDIO,
                url
            ]);
            expect(channelLineupService.findChannelById).toHaveBeenCalledWith(episode.show.relatedChannelIds[0]);
            expect(channelListStoreService.selectChannel).toHaveBeenCalled();
        }));
    });

    describe("navigateToShowsList()", () =>
    {
        it("should navigate to the on demand show listing",
            inject([ NavigationService ], (navigationService: NavigationService) =>
        {
            spyOn(navigationService, "go");

            const channel = createChannel();
            service.navigateToShowsList(channel);

            expect(navigationService.go).toHaveBeenCalledWith([
                appRouteConstants.CATEGORY,
                "Entertainment",
                "howard",
                displayViewType.shows,
                channel.channelId
            ]);
        }));
    });

    describe("toggleFavorite()", () =>
    {
        describe("when the channel is not favorited", () =>
        {
            it("should add the channel to favorites", () =>
            {
                const channel = { isFavorite: false, channelId: "howard100", type: ContentTypes.LIVE_AUDIO,
                   subType: ContentTypes.CHANNEL, channelGuid: "channelGuid", name: 'Howard 100', isIrisPodcast: false};
                service.toggleFavorite(channel.isFavorite, channel.type, channel.subType,
                                channel.channelId, channel.channelGuid, channel.name, channel.isIrisPodcast);

                expect(favoriteListStoreService.toggleFavorite).toHaveBeenCalledWith(channel.isFavorite, channel.type,
                    channel.subType, channel.channelId, channel.channelGuid, channel.isIrisPodcast);
            });
        });

        describe("when the show is not favorited", () =>
        {
            it("should add the show to favorites", () =>
            {
                 const show = { isFavorite: false, channelId: "howard100", type: ContentTypes.SHOW,
                    subType: ContentTypes.SHOW, assetGuid: "showGuid", imageAltText: 'Howard Stern Interviews', isIrisPodcast: false};

                service.toggleFavorite(show.isFavorite, show.type, show.subType,
                                show.channelId, show.assetGuid, show.imageAltText, show.isIrisPodcast);

                expect(favoriteListStoreService.toggleFavorite).toHaveBeenCalledWith(show.isFavorite, show.type,
                    show.subType, show.channelId, show.assetGuid, show.isIrisPodcast);
            });
        });

        describe("when the channel is favorited", () =>
        {
            it("should remove the channel from favorites", () =>
            {
                const channel = { isFavorite: true, channelId: "howard100", type: ContentTypes.LIVE_AUDIO,
                    subType: ContentTypes.CHANNEL, channelGuid: "channelGuid", name: 'Howard 100', isIrisPodcast: false};
                service.toggleFavorite(channel.isFavorite, channel.type, channel.subType, channel.channelId,
                            channel.channelGuid, channel.name, channel.isIrisPodcast);


                expect(favoriteListStoreService.toggleFavorite).toHaveBeenCalledWith(channel.isFavorite, channel.type,
                    channel.subType, channel.channelId, channel.channelGuid, channel.isIrisPodcast);
            });
        });
    });

    describe("removeRecentlyPlayedTile()", () =>
    {
        it("should remove tile from recents", () =>
        {
            const tile = {
                tileAssetInfo: {
                    channelId: "channelId1",
                    recentPlayGuid: "recentlyPlayedGuid"
                }
            } as ITile;

            service.removeRecentlyPlayedTile(tile);
            expect(recentlyPlayedStoreService.removeRecentlyPlayedItem).toHaveBeenCalledWith(tile);
        });
    });
});
