
export interface ICallbacks
{
    onSuccess();
    onError();
}


export interface IModalOptions
{
    description: string;
}

export type NeriticLinkOptions = ICallbacks | IModalOptions;
