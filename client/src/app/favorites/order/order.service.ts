import { filter } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { FavoriteChangeTypes, IInitialTile, ITile } from 'sxmServices';
import { FavoriteListStoreService } from '../../common/service/favorite-list.store.service';

@Injectable()
export class OrderService
{
    private favoritesByAssetTypes;
    private favoritesOrders: { channels: number[], shows: number[], episodes: number[]  } = { channels: [], episodes: [], shows: [] };

    constructor(private favoritesListStoreService: FavoriteListStoreService)
    {
        this.favoritesListStoreService.favorites.pipe(
            filter(({favorites}) =>
            {
                const tiles = favorites.channels;
                const tile = tiles[0] as IInitialTile;
                return tile && tile.defaultFav && tile.defaultFav !== true;
            }))
            .subscribe(({favorites}) =>
        {
            this.favoritesByAssetTypes = favorites;

            this.favoritesOrders = {
                channels: this.favoritesByAssetTypes["channels"].map(fav => fav.tabSortOrder),
                episodes: this.favoritesByAssetTypes["episodes"].map(fav => fav.tabSortOrder),
                shows: this.favoritesByAssetTypes["shows"].map(fav => fav.tabSortOrder)
            };
        });
    }

    public updateFavorites(favorites, activeTab): void
    {
       this.favoritesListStoreService.updateFavorites(favorites.map((favorite: any, index) =>
        {
            (favorite as any).tabSortOrder = index + 1;
            let assetGUID = FavoriteListStoreService.getAssetGuid(favorite.tileContentType,
                favorite.tileContentSubType, favorite.tileAssetInfo),
                channelId = favorite.tileAssetInfo.channelId;

            return this.favoritesListStoreService.createFavoriteUpdateItem(
                FavoriteChangeTypes.UPDATE,
                favorite.tileContentType,
                favorite.tileContentSubType,
                channelId,
                assetGUID,
                favorite.tileAssetInfo.isPandoraPodcast,
                favorite.tabSortOrder
            );
        }));
    }
}
