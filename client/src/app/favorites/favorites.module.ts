import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FavoritesComponent } from "./index";
import { FavoriteListStoreService } from "../common/service/favorite-list.store.service";
import { OrderService } from './order/order.service';
import { FavoriteButtonComponent } from "./button/favorite-button.component";
import { TranslationModule } from "../translate/translation.module";
import { ContentTilesModule } from "../common/component/tiles/content-tiles.module";
import { SharedModule } from "../common/shared.module";
import { ContextMenuModule } from "app/context-menu/context-menu.module";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        TranslationModule,
        ContentTilesModule,
        ContextMenuModule
    ],
    exports: [
        FavoriteButtonComponent
    ],
    declarations: [
        FavoritesComponent,
        FavoriteButtonComponent
    ],
    providers: [
        FavoriteListStoreService,
        OrderService
    ]
})
export class FavoritesModule
{
}
