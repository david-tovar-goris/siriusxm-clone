import {
    ComponentFixture,
    TestBed,
    inject
} from "@angular/core/testing";
import { ActivatedRoute } from "@angular/router";
import { FavoritesComponent } from "../favorites/favorites.component";
import { of as observableOf } from "rxjs";
import { FavoriteListStoreServiceMock } from "../../../test/mocks/favorite-list.store.service.mock";
import { FavoriteListStoreService } from "../common/service/favorite-list.store.service";
import { appRouteConstants } from "../app.route.constants";
import { MockComponent, MockDirective } from "../../../test/mocks/component.mock";
import { TranslationModule } from "../translate/translation.module";
import { CarouselService } from "../carousel/carousel.service";
import { NavigationService } from "../common/service/navigation.service";
import { navigationServiceMock } from "../../../test/mocks/navigation.service.mock";
import { NeriticLinkServiceMock } from "../../../test/mocks/neritic-link.service.mock";
import { NeriticLinkService } from "../neritic-links";
import { OrderService } from "./order/order.service";
import { OrderServiceMock } from "../../../test/mocks/order.service.mock";
import { CarouselServiceMock } from "../../../test/mocks";
import { ContextMenuModule } from "../context-menu/context-menu.module";
import { TileContextMenuOptionsService } from "app/context-menu/options/tile-context-menu-options.service";
import { TileContextMenuOptionsServiceMock } from "../../../test/mocks/tile-context-menu-options.service.mock";
import { LoginOverlayService } from "app/open-access/popups/login-overlay/login-overlay.service";
import { AuthenticationService } from "sxmServices";

describe("FavoritesComponent", () =>
{
    let component: FavoritesComponent,
        fixture: ComponentFixture<FavoritesComponent>,
        mockActivatedRoute = {
            params: observableOf(
                {
                    tab: "channels"
                }
            )
        };

    beforeEach(() =>
    {
        let   loginOverlayServiceMock, authenticationService;

        loginOverlayServiceMock = {
            open: jasmine.createSpy('open')
        };

        authenticationService = {
            userSession : observableOf({}),
            isActiveOpenAccessSession: jasmine.createSpy('isActiveOpenAccessSession').and.returnValue(false)
        };

        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [
                FavoritesComponent,
                MockComponent({
                        selector: "channel-content-tile",
                        inputs: ["tileData", "contextMenuOpenAnalytics", "tileOrientation","initiatelazyLoad"]
                }),
                MockComponent({
                    selector: "episode-content-tile",
                    inputs: ["tileData", "contextMenuOpenAnalytics", "tileOrientation","initiatelazyLoad"]
                }),
                MockComponent({
                    selector: "show-content-tile",
                    inputs: ["tileData", "contextMenuOpenAnalytics", "tileOrientation","initiatelazyLoad"]
                }),
                MockComponent({
                    selector: "context-menu",
                    inputs: [ "getOptionsFunc", "analytics", "ariaText" ]
                }),
                MockDirective({
                    selector: "[sxm-new-analytics]",
                    inputs: ["tagName", "userPath",  "carouselName", "pageFrame", "elementType", "buttonName",
                        "elementPosition", "tagText", "action", "actionSource","inputType", "tileType", "tileContentType",
                        "findingMethod", "newFavoriteIndex", "skipActionSource", "isTile", "skipCurrentPosition", "tileContentSubType"]})
            ],
            providers: [
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: CarouselService, useClass: CarouselServiceMock },
                { provide: TileContextMenuOptionsService, useClass: TileContextMenuOptionsServiceMock },
                { provide: FavoriteListStoreService, useClass: FavoriteListStoreServiceMock },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: OrderService, useClass: OrderServiceMock },
                { provide: NeriticLinkService, useClass: NeriticLinkServiceMock },
                { provide: LoginOverlayService, useValue: loginOverlayServiceMock },
                { provide: AuthenticationService, useValue:authenticationService }
             ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(FavoritesComponent);
        component = fixture.componentInstance;
        spyOn(document, 'getElementById').and.returnValue(document.createElement('ul'));
        fixture.detectChanges();
    });

    describe("tabAction()", () =>
    {
        it("should navigate to the favorites content type", inject([ NavigationService ], (navigationService: NavigationService) =>
        {
            spyOn(navigationService, "go");
            const tab = 'channels';

            component.tabAction(tab);
            expect(navigationService.go).toHaveBeenCalledWith([appRouteConstants.FAVORITES, tab]);
        }));
    });
});
