import {
    ApplicationRef,
    Component,
    OnInit
} from "@angular/core";
import { SubscriptionLike as ISubscription, Subject } from "rxjs";
import { AuthenticationService, IGroupedFavorites, ISession, UserPathPositions } from "sxmServices";
import { FavoriteListStoreService } from "../common/service/favorite-list.store.service";
import { AutoUnSubscribe } from "../common/decorator/auto-unsubscribe";
import { appRouteConstants } from "../app.route.constants";
import {
    ActivatedRoute,
    Params
} from "@angular/router";
import { debounceTime, filter } from 'rxjs/operators';
import { CarouselService } from "../carousel/carousel.service";
import { NavigationService } from "../common/service/navigation.service";
import { OrderService } from "./order/order.service";
import { BrowserUtil } from "../common/util/browser.util";
import { CMOption } from "app/context-menu";
import { TileContextMenuOptionsService } from "app/context-menu/options/tile-context-menu-options.service";
import { LoginOverlayService } from "app/open-access/popups/login-overlay/login-overlay.service";
import {
    AnalyticsCarouselNames,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
const sortablejs = require("sortablejs");

/**
 * @MODULE:     client
 * @CREATED:    08/02/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     FavoritesComponent used to load favorites page
 */

export const favoriteTypes = {
    CHANNELS: 'channels',
    SHOWS: 'shows',
    EPISODES: 'episodes'
};

@Component({
    templateUrl: "./favorites.component.html",
    styleUrls: ["./favorites.component.scss"],
    host: { '(window:resize)': 'this.resizeEvent$.next(event)' }
})

@AutoUnSubscribe()
export class FavoritesComponent implements OnInit
{
    /**
     * Analytics constants
     */
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public anaFavoriteTypesConstants = favoriteTypes;

    /**
     * Analytics properties
     */
    public newFavIndex: number;

    /**
     * List of favorites from the favorites service.
     */
    favorites: IGroupedFavorites = null;

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    public favoriteTabBtns = [
        { name: favoriteTypes.CHANNELS },
        { name: favoriteTypes.SHOWS },
        { name: favoriteTypes.EPISODES }
    ];

    public activeTab: string = favoriteTypes.CHANNELS;

    private resizeEvent$: Subject<any> = new Subject<any>();

    public isOpenAccessLink: boolean = false;


    constructor(private favoritesListStoreService: FavoriteListStoreService,
                private route: ActivatedRoute,
                public carouselService: CarouselService,
                public tileContextMenuOptionsService: TileContextMenuOptionsService,
                private navigationService: NavigationService,
                private orderService: OrderService,
                private loginOverlayService: LoginOverlayService,
                private applicationRef: ApplicationRef,
                private authenticationService: AuthenticationService)
    {
        this.resizeEvent$.pipe(debounceTime(500)).subscribe(() =>
        {
            // create browser vertical scroll for lazyload tile background Images
            BrowserUtil.triggerVerticalScroll();
        });

        this.subscriptions.push(
            this.favoritesListStoreService.favorites.pipe(
                filter(({favorites}) => favorites.channels !== null))
                .subscribe(({ favorites }) =>
                {
                    this.favorites = favorites;
                })
        );

        this.route.params.subscribe(
            (params: Params) =>
            {
                this.activeTab = params['tab'];
                this.isOpenAccessLink =
                    this.authenticationService.isActiveOpenAccessSession() && this.activeTab === favoriteTypes.CHANNELS;
            }
        );
        this.favoritesListStoreService.getFavoriteCarousels();
    }

    ngOnInit()
    {
        sortablejs.create(document.getElementById("sortable-favorites-list"),
            {
                sort: true,
                animation: 150,
                forceFallback: false,
                onEnd: (event) =>
                {
                    //setting newFavIndex for analytics purpose
                    this.newFavIndex = event.newIndex;
                    const favorite = this.favorites[this.activeTab][event.oldIndex];
                    this.favorites[this.activeTab].splice(event.oldIndex, 1);
                    this.favorites[this.activeTab].splice(event.newIndex, 0, favorite);
                    this.orderService.updateFavorites(this.favorites[this.activeTab], this.activeTab);
                }
            });

        //for analytics
        this.resetNewFavIndex();
    }

    public tabAction(tab: string): void
    {
        this.navigationService.go([
            appRouteConstants.FAVORITES,
            tab
        ]);
    }

    public getTiles()
    {
        const tiles = this.favorites[this.activeTab];
        if(tiles.length > 0 && tiles[0].defaultFav && tiles[0].defaultFav === true) { return []; }
        return this.favorites[this.activeTab];
    }

    public getCMOptionsFunc(tile): () => CMOption[]
    {
        return function()
        {
            return this.tileContextMenuOptionsService.getCMOptions(tile);
        }.bind(this);
    }

    /*
    * Opens the login overlay
    * */
    public openLoginOverlay()
    {
        this.loginOverlayService.open(this.applicationRef.components[0].instance.viewContainerRef);
    }

    /**
     * For Analytics sets params for click event
     */
    resetNewFavIndex()
    {
        this.newFavIndex = null;
    }

    /**
     * For Analytics
     */
    getAnaTagsFromFavType(type: string): {
        tagName: string,
        userPath: string,
        buttonName: string
    }
    {
        switch(type){
            case favoriteTypes.CHANNELS:
                return {
                    tagName: AnalyticsTagNameConstants.FAV_CHANNELS,
                    userPath: AnalyticsUserPaths.FAV_CHANNELS,
                    buttonName: AnalyticsTagNameConstants.FAV_CHANNELS_BUTTON_NAME
                };
            case favoriteTypes.EPISODES:
                return {
                    tagName: AnalyticsTagNameConstants.FAV_EPISODES,
                    userPath: AnalyticsUserPaths.FAV_EPISODES,
                    buttonName: AnalyticsTagNameConstants.FAV_EPISODES_BUTTON_NAME
                };
            case favoriteTypes.SHOWS:
                return {
                    tagName: AnalyticsTagNameConstants.FAV_SHOWS,
                    userPath: AnalyticsUserPaths.FAV_SHOWS,
                    buttonName: AnalyticsTagNameConstants.FAV_SHOWS_BUTTON_NAME
                };
            default:
                return {
                    tagName: '',
                    userPath: '',
                    buttonName: ''
                };
        }

    }
}
