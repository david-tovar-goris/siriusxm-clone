import { filter } from 'rxjs/operators';
import {
    ChangeDetectorRef,
    Component,
    Input,
    OnInit,
    AfterViewChecked, ViewChild, ElementRef
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { FavoriteListStoreService } from "../../common/service/favorite-list.store.service";
import {
    IAppByPassState,
    IFavoriteAsset,
    BypassMonitorService,
    CarouselTypeConst, MediaUtil
} from "sxmServices";
import { SubscriptionLike as ISubscription } from "rxjs";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { FavoriteButtonConstant } from "./favorite-button.constant";
import { ToastService } from "../../common/service/toast/toast.service";
import {
    AnalyticsTagActionConstants,
    AnalyticsPageFrames,
    AnalyticsElementTypes,
    AnalyticsScreens,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsModals,
    AnalyticsTagActionSourceConstants,
    AnalyticsInputTypes,
    AnalyticsContentTypes,
    AnalyticsTagTextConstants,
    AnalyticsCarouselNames,
    AnalyticsEDPNames
} from "../../analytics/sxm-analytics-tag.constants";
import { Router } from "@angular/router";

@Component({
    selector: 'favorite-btn',
    template: `
        <button sxm-new-analytics
                [tagName]="tagName"
                [userPath]="userPath"
                [carouselName]="carouselName"
                [tagType]="AnalyticsTagActionConstants.BUTTON_CLICK"
                [modal]="modal"
                [tagText]="tagText"
                [pageFrame]="pageFrame"
                [elementType]="AnalyticsElementTypes.BUTTON"
                [buttonName]="buttonName"
                [userPathScreenPosition]="userPathScreenPosition"
                [action]="AnalyticsTagActionConstants.BUTTON_CLICK"
                [actionSource]="actionSource"
                [inputType]=AnalyticsInputTypes.MOUSE
                [elementPosition]="elementPosition"
                [skipScreenName]="skipScreenName"
                [tileType]="assetTileContentType"
                [tileContentType]="assetTileContentSubType"
                [attr.aria-label]="favBtnAriaLabel"
                [ngClass]="{'white': color === 'white', 'blue': color !== 'white',
                'is-favorite': asset && asset.isFavorite , 'not-favorite': asset && !asset.isFavorite}"
                [disabled]="gupByPass"
                (click)="toggleFavorite($event)"
                (mouseover)="setFavoriteIcon(true)"
                (mouseleave)="setFavoriteIcon(false)"
                #favButton>
            <span class="fav-accessibility-text"
                  attr.aria-live="assertive"
                  [attr.aria-label]="getFavAccessibilityAlertText() | translate">
                 {{ getFavAccessibilityAlertText() | translate }}
            </span>
        </button>
    `,
    styleUrls: [ "./favorite-button.component.scss" ]
})

@AutoUnSubscribe()
export class FavoriteButtonComponent implements OnInit, AfterViewChecked
{
    private _favAsset: IFavoriteAsset;

    /* flags for list, nowplaying and miniNowPlaying view*/
    public isListView: boolean =false;
    public isNowPlayingView: boolean =false;
    public isEdpView: boolean = false;
    private _isMiniNPView: boolean =false;
    public tagName: string = null;
    public userPath: string = null;
    public screen: string = null;
    public carouselName: string = null;
    public pageFrame: string = null;
    public modal: string = null;
    public buttonName: string = null;
    public actionSource: string = null;
    public tagText: string = null;
    public userPathScreenPosition: number = 0;
    public elementPosition: number = null;
    public skipScreenName:boolean = false;
    public edp: string = null;
    public assetTileContentType:string = null;
    public assetTileContentSubType:string = null;

    @ViewChild('favButton') favButton: ElementRef;
    /**
     * type of fav button - channel or show or episode
     */
    @Input() set listAsset (asset: IFavoriteAsset)
    {
        this._favAsset = asset;
        this._favAsset.isFavorite = !!this.favoritesListStoreService.getFavorite(asset.assetGuid);
        this.setFavoriteIcon();
        this.setAnalyticsConstants();
    }

    get asset(): IFavoriteAsset
    {
        return this._favAsset;
    }

    /**
     * View type is list or now playing
     */
    @Input() set viewType(viewType: 'listView' | 'nowPlaying' | 'edpView')
    {
        if (viewType === FavoriteButtonConstant.NOW_PLAYING_VIEW)
        {
            this.isNowPlayingView = true;
            this.favoritesListStoreService.determineFavoriteAsset('nowPlaying')
                .subscribe(favAsset =>
                {
                    this._favAsset = favAsset;
                    this._favAsset.isFavorite = !!this.favoritesListStoreService.getFavorite(favAsset.assetGuid);
                    this.setFavoriteIcon();
                    this.changeDetectorRef.markForCheck();
                    this.setAnalyticsConstants();
                });
        }
        else if(viewType === FavoriteButtonConstant.EDP_VIEW)
        {
            this.isEdpView = true;
        }
        else if(viewType === FavoriteButtonConstant.LIST_VIEW)
        {
           this.isListView = true;
        }
    }

    @Input() set isMiniNPView(isMiniNPView: boolean)
    {
        this._isMiniNPView = isMiniNPView;
        this.setAnalyticsConstants();
    }

    get isMiniNPView(): boolean
    {
        return this._isMiniNPView;
    }

    /**
     * type of color used to display fav button.
     * @type {string}
     */
    @Input() color: ("white" | "blue") = "blue";

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * Holds the gup by pass flag.
     * @type {boolean}
     */
    public gupByPass: boolean = false;

    public favBtnAriaLabel = "";

    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    constructor(private favoritesListStoreService: FavoriteListStoreService,
                private changeDetectorRef: ChangeDetectorRef,
                private bypassMonitorService: BypassMonitorService,
                private toastService: ToastService,
                private translate: TranslateService,
                private router: Router)
    {
        this.subscriptions.push(
            this.bypassMonitorService.bypassErrorState.subscribe((state: IAppByPassState) =>
            {
                this.gupByPass = state.GUP_BYPASS2 || state.GUP_BYPASS;
            }));
    }

    ngOnInit(): void
    {
        let favoritesSubscription = this.favoritesListStoreService.favorites.pipe(
                                        filter(() => !!this.asset))
                                        .subscribe(favList =>
                                        {
                                            this._favAsset.isFavorite =
                                                !!this.favoritesListStoreService.getFavorite(this._favAsset.assetGuid);
                                            this.setAnalyticsConstants();
                                            this.changeDetectorRef.detectChanges();
                                            this.setFavoriteIcon();
                                        });

        this.subscriptions.push(favoritesSubscription);
    }

    ngAfterViewChecked(): void
    {
        this.favBtnAriaLabel = this.getAriaLabel();
        this.changeDetectorRef.detectChanges();
    }

    /**
     * Update the favorite based on assetType and changeType
     * @param event
     */
    toggleFavorite(event): void
    {
        event.stopPropagation();
        // Perform an optimistic update to the boolean `isFavorite` flag. This will allow the UI
        // to immediately reflect the current state of the favorite icon since the API can be really
        // slow. When the API does come back it'll also update this flag so even if we're wrong (b/c
        // the API fails and/or we can't update the favorite flag for this asset, it'll get set correctly
        // from the outside at a higher-level and this will get overwritten to the correct value.
        const isFav           = this.asset.isFavorite;
        this.displayToastMessage();
        this._favAsset.isFavorite = !this.asset.isFavorite;
        this.setFavoriteIcon();
        this.favBtnAriaLabel = this.getAriaLabel();

        this.favoritesListStoreService.toggleFavorite(isFav,
            this.asset.contentType,
            this.asset.subContentType,
            this.asset.channelId,
            this.asset.assetGuid,
            this.asset.isIrisPodcast);
    }

    // not applying this at CSS to support in IE & Edge
    public setFavoriteIcon(favoriteHovered = false)
    {
        if(this.color === 'white')
        {
            this.favButton.nativeElement.style.background =
                this._favAsset && this._favAsset.isFavorite?
                 "url('../../../assets/images/favorite-on-white.svg') no-repeat" :
                (favoriteHovered ? "url('../../../assets/images/favorite-hover-white.svg') no-repeat" :
                 "url('../../../assets/images/favorite-off-white.svg') no-repeat");
        }
        else
        {
            this.favButton.nativeElement.style.background =
                this._favAsset && this._favAsset.isFavorite?
                 "url('../../../assets/images/favorite-on.svg') no-repeat" :
                (favoriteHovered ? "url('../../../assets/images/favorite-hover-gray.svg') no-repeat" :
            "url('../../../assets/images/favorite-off.svg') no-repeat") ;
        }
        this.favButton.nativeElement.style.backgroundPosition = "center center";

    }

    private displayToastMessage(): void
    {
        const contentFavoriteState = this.asset.isFavorite ? 'unFavorite' : 'Favorite';
        const tileContentType = this.asset.contentType === CarouselTypeConst.LIVE_SHOW_TILE ? CarouselTypeConst.SHOW_TILE : this.asset.contentType;
        const toastTitle: string =  MediaUtil.isOnDemandMediaType(this.asset.subContentType) ? `"${this.asset.title}"` : this.asset.title;

        this.toastService.open({
            customMessage: toastTitle,
            messagePath: `nowPlaying.toastFavorites.${tileContentType}${contentFavoriteState}`,
            isAltColor: true,
            hasCloseButton: true,
            closeToastTime: 10000
        });
    }

    getAriaLabel(): string
    {
        if (!this.asset) return "";

        const isFavoriteTextTranslation = this.translate.instant(this.asset.isFavorite ? 'favoriteButton.remove' : 'favoriteButton.add');
        const fromFavTextTranslation = this.translate.instant(this.asset.isFavorite ? 'favoriteButton.fromFav' : 'favoriteButton.toFav');

        return `${isFavoriteTextTranslation} ${this.asset.title} ${fromFavTextTranslation}`;
    }

    /**
     * Gets the fav accessibility alert text
     * @returns {string}
     */
    getFavAccessibilityAlertText(): string
    {
        if (!this.asset) return "";

        return this.asset.isFavorite ? 'favoriteButton.removedFav' : 'favoriteButton.addedFav';
    }

    // Analytics tags for phase2
    setAnalyticsConstants(): void
    {
        if (!this.asset) return;
        if (this.isListView)
        {
            if (this.asset.subContentType === CarouselTypeConst.SEEDED_RADIO)
            {
                // Analytics tags for Manage Pandora stations in settings ArtRdioAddRemFav:297
                this.tagName   = AnalyticsTagNameConstants.ART_RDIO_ADD_REM_FAV;
                this.userPath = this.asset.isFavorite ? AnalyticsUserPaths.ARTIST_RADIO_REM_FAV : AnalyticsUserPaths.ARTIST_RADIO_ADD_FAV;
                this.screen    = AnalyticsScreens.MANAGE_ARTIST_RADIO;
                this.buttonName   = AnalyticsTagNameConstants.ART_RDIO_ADD_REM_FAV_BUTTON_NAME;
                this.pageFrame = null;
                this.elementPosition = 0;
                this.carouselName = AnalyticsCarouselNames.ARTIST_RADIO;
                this.tagText = this.asset.isFavorite ?
                               AnalyticsTagTextConstants.REM_FAV_BUTTON_NAME : AnalyticsTagTextConstants.ADD_FAV_BUTTON_NAME;
            }
        }
        else if (this.isNowPlayingView && this.isMiniNPView)
        {
            // Analytics tags for Mini Now Playing view MnpAddRemFav:194
            this.tagName      = AnalyticsTagNameConstants.MNP_ADD_REM_FAV;
            this.userPath     = this.asset.isFavorite ? AnalyticsUserPaths.MNP_REM_FAV : AnalyticsUserPaths.MNP_ADD_FAV;
            this.pageFrame    = AnalyticsPageFrames.MODAL;
            this.modal        = AnalyticsModals.MINI_NOW_PLAYING_BAR;
            this.actionSource = AnalyticsTagActionSourceConstants.MININP;
            this.buttonName   = AnalyticsTagNameConstants.MNP_ADD_REM_FAV_BUTTON_NAME;
            this.elementPosition = 0;
            this.userPathScreenPosition = 3;
            this.carouselName = null;
            this.tagText = this.asset.isFavorite ?
                           AnalyticsTagTextConstants.REM_FAV_BUTTON_NAME : AnalyticsTagTextConstants.ADD_FAV_BUTTON_NAME;
        }
        else if(this.isNowPlayingView)
        {
            // Analytics tags for Now Playing view NpAddRemFav:17
            this.tagName  = AnalyticsTagNameConstants.NP_ADD_REM_FAV;
            this.userPath = this.asset.isFavorite ? AnalyticsUserPaths.NP_REM_FAV : AnalyticsUserPaths.NP_ADD_FAV;
            this.userPathScreenPosition = 1;
            this.pageFrame    = AnalyticsPageFrames.NOW_PLAYING_MAIN;
            this.buttonName   = AnalyticsTagNameConstants.NP_ADD_REM_FAV_BUTTON_NAME;
            this.elementPosition = 1;
            this.carouselName = null;
            this.tagText = this.asset.isFavorite ?
                           AnalyticsTagTextConstants.REM_FAV_BUTTON_NAME : AnalyticsTagTextConstants.ADD_FAV_BUTTON_NAME;
        }
        else if(this.isEdpView)
        {
            // Analytics tags for EdpAddRmvFav: 156, 158
            if(this.asset.contentType === CarouselTypeConst.CHANNEL_TILE)
            {
                this.tagName = AnalyticsTagNameConstants.EDP_ADD_RMV_FAV_CH;
                this.userPath = AnalyticsUserPaths.EDP_CH_FAV + '_' + (this.asset.isFavorite ? AnalyticsTagTextConstants.REMOVE
                                                                                             : AnalyticsTagTextConstants.ADD);
                this.pageFrame = AnalyticsPageFrames.EDP;
                this.skipScreenName = true;
                this.edp = AnalyticsEDPNames.CHANNEL;
                this.buttonName = AnalyticsTagNameConstants.EDP_ADD_RMV_FAV_CH_BTN_NAME;
                this.tagText = this.asset.isFavorite ? AnalyticsTagTextConstants.REMOVE : AnalyticsTagTextConstants.ADD;
                this.elementPosition = 1;
                this.assetTileContentType = this.asset.contentType;
                this.assetTileContentSubType = this.asset.subContentType;
            }
            else if(this.asset.contentType == CarouselTypeConst.SHOW_TILE)
            {
                this.tagName = AnalyticsTagNameConstants.EDP_ADD_RMV_FAV_SHW;
                this.userPath = AnalyticsUserPaths.EDP_SHW_FAV + '_' + (this.asset.isFavorite ? AnalyticsTagTextConstants.REMOVE
                                                                                              : AnalyticsTagTextConstants.ADD);
                this.pageFrame = AnalyticsPageFrames.EDP;
                this.skipScreenName = true;
                this.edp = AnalyticsEDPNames.SHOW;
                this.buttonName = AnalyticsTagNameConstants.EDP_ADD_RMV_FAV_SHW_BTN_NAME;
                this.tagText = this.asset.isFavorite ? AnalyticsTagTextConstants.REMOVE : AnalyticsTagTextConstants.ADD;
                this.elementPosition = 0;
                this.assetTileContentType = this.asset.contentType;
                this.assetTileContentSubType = this.asset.subContentType;
            }
        }
    }
}
