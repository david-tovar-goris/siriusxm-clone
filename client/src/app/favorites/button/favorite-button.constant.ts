export class FavoriteButtonConstant
{
    public static NOW_PLAYING_VIEW: string = "nowPlaying";
    public static NOW_PLAYING_SHOW_AND_EPISODE_VIEW: string = "showAndEpisode";
    public static LIST_VIEW: string = "listView";
    public static EDP_VIEW: string = "edpView";
}
