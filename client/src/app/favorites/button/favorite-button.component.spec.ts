import { ComponentFixture, TestBed } from "@angular/core/testing";
import {MediaPlayerService, SxmAnalyticsService, BypassMonitorService, IAppByPassState} from "sxmServices";
import { MockDirective } from "../../../../test/mocks/component.mock";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../test/mocks/router.mock";
import { SharedModule } from "app/common/shared.module";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { By } from "@angular/platform-browser";
import { FavoriteButtonComponent } from "app/favorites/button/favorite-button.component";
import { FavoriteListStoreService } from "app/common/service/favorite-list.store.service";
import { FavoriteListStoreServiceMock } from "../../../../test/mocks/favorite-list.store.service.mock";
import { TranslationModule } from "app/translate/translation.module";
import { ToastService } from "app/common/service/toast/toast.service";
import { of as observableOf } from "rxjs";
import { ToastServiceMock } from "../../../../test/mocks/toast.service.mock";

describe('Favorite button component', () =>
{
    let mediaPlayerService,
        favoriteListStoreService,
        fixture: ComponentFixture<FavoriteButtonComponent>,
        component: FavoriteButtonComponent;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(() =>
    {
        mediaPlayerService = {
            mediaPlayer: {
                togglePausePlay: function()
                {
                }
            },
            isPlaying: function()
            {
                return true;
            }
        };

        this.store = {
            select: () => new BehaviorSubject(null)
        };

        favoriteListStoreService = new FavoriteListStoreServiceMock();

        let bypassMonitorServiceMock = {
            bypassErrorState: observableOf({} as IAppByPassState)
        };

        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SharedModule
            ],
            declarations: [
                FavoriteButtonComponent,
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType",
                        "userPathScreenPosition", "modal", "tagText", "actionSource"]})
            ],
            providers: [
                { provide: MediaPlayerService, useValue: mediaPlayerService },
                { provide: Store, useValue: this.store},
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: FavoriteListStoreService, useValue: favoriteListStoreService },
                { provide: BypassMonitorService, useValue: bypassMonitorServiceMock },
                { provide: ToastService, useValue: ToastServiceMock.getSpy() },
                { provide: Router, useClass: RouterStub}
            ]
        });
        mediaPlayerService = TestBed.get(MediaPlayerService);
        fixture = TestBed.createComponent(FavoriteButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('favorite button analytics', () =>
    {
        it("logs the favorite button event", () =>
        {
            component.isNowPlayingView = true;
            component.isMiniNPView = true;

            component.listAsset = {
                assetGuid: "",
                channelId: "",
                contentType: "",
                subContentType: "",
                title: "",
                isFavorite: true
            };

            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag").and.callThrough();
            fixture.detectChanges();
            fixture.debugElement.query(By.css('button'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "FavButton"
                    }
                });
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
