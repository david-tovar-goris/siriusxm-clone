import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { ChannelListServiceMock } from "../../../../test/mocks/channel-list.service.mock";
import { ChannelListStoreService } from "../../common/service/channel-list.store.service";
import { OnDemandListComponent } from "../component/on-demand-list.component";
import { ChannelListContainer } from "./channel-list.container";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { MockComponent } from "../../../../test/mocks/component.mock";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { CarouselStoreServiceMock } from "../../../../test/mocks/carousel.store.service.mock";

// TODO: BMR: Need to figure out how to mock the store data.
describe("ChannelListContainer", () =>
{
    let component: ChannelListContainer;
    let fixture: ComponentFixture<ChannelListContainer>;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [
                ChannelListContainer,
                OnDemandListComponent,
                MockComponent({ selector: "sxm-no-results", inputs: [ "results", "filterText", "pageName" ] })
            ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                // TODO: Combine ChannelListServiceMock with MockChannelListService in category.component.spec
                { provide: CarouselStoreService, useClass: CarouselStoreServiceMock },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() }
            ]
            }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(ChannelListContainer);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () =>
    {
        expect(component).toBeTruthy();
    });
});
