import {
    Component,
    Input
} from "@angular/core";
import { Observable } from "rxjs";
import { Logger } from "sxmServices";
import { ChannelListStoreService } from "../../common/service/channel-list.store.service";
import { IChannelListStore } from "../../common/store/index";
import { displayViewType } from "../component/display.view.type";
import { CarouselStoreService } from "../../common/service/carousel.store.service";

@Component({
    selector: "channel-list-container",
    templateUrl: "./channel-list.container.html"
})

export class ChannelListContainer
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ChannelListContainer");

    /**
     * List of channels wrapped in observables from the NGRX channels store. The Observable
     * wrapping will need to be stripped for use in the UI via the async pipe; e.g., "channels | async".
     */
    public channelStore: Observable<IChannelListStore>;

    /**
     * Enum indicating if this is for live channels or On-Demand.
     */
    @Input() displayView: displayViewType;

    /**
     * Injects AJS services and binds the UI view model to service data that contains slices of the NGRX store model.
     * @param {ChannelListStoreService} channelListService provides the channel store for the channel lineup
     * @param {CarouselStoreService} carouselStoreService provides the carousel store for the carousels data
     */
    constructor(private channelListStoreService: ChannelListStoreService,
                private carouselStoreService: CarouselStoreService)
    {
        this.channelStore = this.channelListStoreService.channelStore;
    }

    /**
     * Determines if the current view is for channels.
     * @returns {boolean}
     */
    isChannelView(): boolean
    {
        return this.displayView === displayViewType.channels;
    }

    /**
     * Determines if the current view is for On Demand or podcast
     * @returns {boolean}
     */
    isOnDemandView(): boolean
    {
        return this.displayView === displayViewType.podcasts || this.displayView === displayViewType.ondemand
                        || this.displayView === displayViewType.shows;
    }
}
