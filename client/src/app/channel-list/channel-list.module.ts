import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ChannelListStoreService } from "../common/service/channel-list.store.service";
import { ChannelListComponent } from "./component/channel-list.component";
import { OnDemandListComponent } from "./component/on-demand-list.component";
import { ChannelListContainer } from "./container/channel-list.container";
import { CarouselModule } from "../carousel/carousel.module";
import { NoResultsModule } from "../no-results/no-results.module";
import { FilterModule } from "../filter/filter.module";
import { ChannelTileModule } from "../common/component/list-items/channel/channel-list-item.module";
import { ShowTileModule } from "../common/component/list-items/show/show-list-item.module";
import { SharedModule } from "app/common/shared.module";

@NgModule({
    imports: [
        CommonModule,
        CarouselModule,
        NoResultsModule,
        FilterModule,
        ChannelTileModule,
        ShowTileModule,
        SharedModule
    ],
    declarations: [
        ChannelListContainer,
        ChannelListComponent,
        OnDemandListComponent
    ],
    exports: [
        ChannelListContainer,
        ChannelListComponent,
        OnDemandListComponent
    ],
    providers: [
        ChannelListStoreService
    ]
})
export class ChannelListModule
{
}
