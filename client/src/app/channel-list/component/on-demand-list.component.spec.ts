import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { OnDemandListComponent } from "./on-demand-list.component";
import { MockComponent, MockDirective } from "../../../../test/mocks/component.mock";

describe("OnDemandListComponent", () =>
{
    let component: OnDemandListComponent;
    let fixture: ComponentFixture<OnDemandListComponent>;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            declarations: [
                OnDemandListComponent,
                MockComponent({ selector: "show-list-item", inputs: ["tileData","index", "filterText"] }),
                MockComponent({ selector: "sxm-no-results", inputs: [ "results", "pageName" ] }),
                MockDirective({ selector: "[sxm-new-analytics]", inputs: ["isTile", "tagName", "userPath", "bannerInd", "tileType", "consumedPerc",
                        "userPathScreenPosition", "pageFrame", "findingMethod", "carouselName", "carouselPosition", "buttonName", "tagText",
                        "elementPosition", "skipActionSource", "elementType", "action", "inputType", "zoneOrder", "tileContentType", "tileContentSubType"]})
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(OnDemandListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () =>
    {
        expect(component instanceof OnDemandListComponent).toBe(true);
    });
});

