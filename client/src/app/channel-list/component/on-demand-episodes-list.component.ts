import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    OnInit,
    Output,
    ViewChild
} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {
    AuthenticationService,
    ChannelLineupService,
    ICarouselData,
    ICarouselDataByType,
    IChannel,
    NAME_BACKGROUND,
    PLATFORM_WEBEVEREST,
    POSTER_TILE_IMAGE_HEIGHT,
    POSTER_TILE_IMAGE_WIDTH
} from "sxmServices";
import { appRouteConstants } from "../../app.route.constants";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { NavigationService } from "../../common/service/navigation.service";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { SubscriptionLike as ISubscription, Observable } from "rxjs";
import { filter, debounceTime } from 'rxjs/operators';
import { ChannelListStoreService } from "app/common/service/channel-list.store.service";
import { getSelectedChannel } from "app/common/store/channel-list.store";
import { TileUtil } from "app/common/util/tile.util";
import { Store } from "@ngrx/store";
import { IAppStore } from "app/common/store";
import * as _ from "lodash";
import { BehaviorSubject, Subject } from "rxjs/index";
import { selectOnDemandEpisodeCarousel } from "app/common/store/carousel.store";

/**
 * @MODULE:     client
 * @CREATED:    08/29/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *    OnDemandEpisodesListComponent used to load on demand episodes list
 */

@Component({
    selector   : "sxm-on-demand-episode-list",
    templateUrl: "./on-demand-episodes-list.component.html",
    styleUrls  : [ "./on-demand-episodes-list.component.scss" ],
    host: { '(window:resize)': 'this.resizeEvent$.next(event)', '(window:scroll)': 'animateHeader()' },
    changeDetection: ChangeDetectionStrategy.OnPush
})

@AutoUnSubscribe()
export class OnDemandEpisodesListComponent implements OnInit, AfterViewInit
{
    @Output() displayView: string = "episodeView";

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * Backed up the show id from router object
     */
    private _showId: string;

    /**
     * Backed up the channel id from router
     */
    private _channelId: string;

    /**
     * Holds the show logo url
     */
    public showLogoUrl: string;

    /**
     * Holds the show logo alt text
     */
    public showLogoAltText: string;

    /**
     * Holds the channel background image urk
     */
    public channelBackgroundImageUrl: string;

    /**
     * Holds the episode carousel data
     */
    public carousel$: Observable<ICarouselData> = this.store.select(selectOnDemandEpisodeCarousel);
    /**
     * Holds the line 1 text
     */
    public line1: string;

    /**
     * Holds the line 2 text
     */
    public pageTextLine2: string;

    /**
     * Indicates is image failed to load or not.
     */
    public isImageLoaded = true;

    /**
     * Element reference to the player controls container
     */
    @ViewChild('onDemandDescContainer') onDemandDescContainer: ElementRef;
    public readLess: boolean;
    public toggleTextBtn: boolean;
    private resizeEvent$: Subject<any> = new Subject<any>();

    constructor(private channelLineupService: ChannelLineupService,
                private carouselStoreService: CarouselStoreService,
                private activeRoute: ActivatedRoute,
                public navigationService: NavigationService,
                public translate: TranslateService,
                public authenticationService: AuthenticationService,
                public channelListStoreService: ChannelListStoreService,
                private store: Store<IAppStore>,
                private changeDetectorRef: ChangeDetectorRef)
    {
        this.toggleTextBtn = false;
        this.readLess = false;
    }

    private _line2$ = new BehaviorSubject<string>(null);

    public isActiveOpenAccessSession: boolean;

    private channel$: Observable<IChannel> = this.store.select(getSelectedChannel);

    public get line2$ (){ return this._line2$ as Observable<string>; }

    /**
     * Subscribe to On Demand/Channel Store
     */
    public ngOnInit()
    {
        this.subscriptions.push(
            this.activeRoute.data.subscribe(({ onDemandDetails }) =>
            {
                this._channelId = onDemandDetails.channelId;
                this._showId    = onDemandDetails.showId;
                if (this._channelId)
                {
                    const channel = this.channelLineupService.findChannelById(this._channelId);
                    this.channelListStoreService.selectChannel(channel);
                }
            }),
            this.channel$.subscribe((channel: IChannel) =>
            {
                this.channelBackgroundImageUrl = ChannelLineupService.getChannelImage(
                    channel,
                    NAME_BACKGROUND,
                    PLATFORM_WEBEVEREST,
                    POSTER_TILE_IMAGE_WIDTH,
                    POSTER_TILE_IMAGE_HEIGHT
                );
            })
        );

        this.subscriptions.push(
            this.carouselStoreService.episodesCarousels.pipe(
                filter(data => !!data))
                .subscribe(
                    (carouselData: ICarouselDataByType) =>
                    {
                        if (carouselData.error)
                        {
                            //Note: If in carousel guid is not valid and failed to get the data we navigate to Home route.
                            this.navigationService.go([ appRouteConstants.HOME ], { queryParamsHandling: "merge" });
                            return;
                        }

                        this.showLogoUrl     = carouselData.pageLogo && carouselData.pageLogo.imageLink
                                               ? TileUtil.normalizeImageUrl(carouselData.pageLogo.imageLink, 'width=250&height=200&preserveAspect=true')
                                               : "";
                        this.showLogoAltText = carouselData.pageLogo ? carouselData.pageLogo.imageAltText : "";
                        this.line1           = carouselData.pageTextLine1 ? carouselData.pageTextLine1 : "";
                        this.pageTextLine2 = carouselData.pageTextLine2 ? carouselData.pageTextLine2 : "";
                        this._line2$.next(this.pageTextLine2);
                    }
                )
        );

        //on window resize set/reset readLess/readMore button
        this.resizeEvent$.pipe(debounceTime(200)).subscribe(() =>
        {
            this.onResize();
        });
        this.resizeEvent$.next(null);

        this._line2$.pipe(filter(data=> !!data),debounceTime(0)).subscribe((text)=>
        {
            this.toggleTextBtn = false;
            this.readLess = false;
            this.onResize();
        });

        this.subscriptions.push(this.authenticationService.userSession.subscribe(data =>
        {
            this.isActiveOpenAccessSession = data.activeOpenAccessSession;
        }));
    }

    ngAfterViewInit(): void
    {
        this.onResize();
    }

    /**
     * animates the header on scroll
     */
    public animateHeader(): void
    {
        const distanceY       = window.pageYOffset || document.documentElement.scrollTop,
              shrinkOn        = 40,
              className       = "shrink",
              header          = document.querySelector(".show-info-container"),
              headerContainer = document.querySelector('.show-header'),
              content         = document.querySelector('.sxm-on-demand-episode-list-container');

        if (distanceY > shrinkOn)
        {
            header.classList.add(className);
            headerContainer.classList.add("show-header-fixed");
            content.classList.add("show-header-fixed");
        }
        else if (header && header.classList && header.classList.contains(className))
        {
            header.classList.remove(className);
            headerContainer.classList.remove("show-header-fixed");
            content.classList.remove("show-header-fixed");
        }
    }

    /**
     *  initial setup of readLess/readMore button
     */
    private onResize()
    {
        if (!this.onDemandDescContainer) return;

        let lineHeight: any = window.getComputedStyle(this.onDemandDescContainer.nativeElement, null)
                                    .getPropertyValue("line-height");
        let lhIndex = lineHeight.indexOf('px');

        if(lhIndex != -1)
        {
            lineHeight = Number(lineHeight.substr(0, lhIndex));
        }

        let textLength = this.pageTextLine2 && this.pageTextLine2.length;
        const divHeight = this.onDemandDescContainer.nativeElement.clientHeight;


        if ( divHeight !== 0 && textLength !== 0 )
        {
            let textDivRatio = divHeight / lineHeight;

            //if the text is more than 5 lines
            if ( textDivRatio > 5 )
            {
                this.onDemandDescContainer.nativeElement.classList.add("less");
                this.readLess = true;
                this.toggleTextBtn = true;
            }

            // if text is less than or equal to 5 lines
            else if (textDivRatio < 6 && textDivRatio > 5)
            {
                this.onDemandDescContainer.nativeElement.classList.remove("less");
                this.readLess = false;
            }

            else if (textDivRatio <= 5)
            {
                this.toggleTextBtn = false;
            }
        }
        this.changeDetectorRef.detectChanges();
    }

    public onImageLoadError(err: any)
    {
        this.isImageLoaded = false;
    }
}


