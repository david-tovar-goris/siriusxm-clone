import { first, switchMap, skipWhile, filter, map } from 'rxjs/operators';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import {
    InitializationService,
    InitializationStatusCodes
} from "sxmServices";
import { CarouselStoreService } from "../../common/service/carousel.store.service";

interface IOnDemandDetails
{
    channelId: string;
    showId: string;
}

@Injectable()
export class OnDemandDetailsResolver implements Resolve <IOnDemandDetails | {}>
{
    constructor(private initService: InitializationService,
                private carouselStoreService: CarouselStoreService)
    {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    {
        return this.initService.initState.pipe(
                   skipWhile(initState => initState !== InitializationStatusCodes.RUNNING),
                   switchMap(() => resolveEpisodeData(route, this.carouselStoreService)),
                   first());
    }
}

function resolveEpisodeData(route: ActivatedRouteSnapshot, carouselStoreService): Observable<IOnDemandDetails>
{
    return carouselStoreService.selectEpisode(route.params.url)
                               .pipe(filter(data => !!data), map((data) =>
                               {
                                   return {
                                       channelId: route.params.channelId,
                                       showId   : route.params.showId
                                   };
                               }));
}
