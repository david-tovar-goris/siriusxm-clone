import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { NoResultsModule } from "../../no-results/no-results.module";
import { OnDemandEpisodesListComponent } from "./on-demand-episodes-list.component";
import { FilterModule } from "../../filter/filter.module";
import { SharedModule } from "../../common/shared.module";
import { VerticalListModule } from "../../common/component/list-items/vertical-list/vertical-list.module";

@NgModule({
    imports     : [
        CommonModule,
        SharedModule,
        TranslateModule,
        NoResultsModule,
        VerticalListModule,
        FilterModule
    ],
    declarations: [
        OnDemandEpisodesListComponent
    ],
    exports     : [
        OnDemandEpisodesListComponent
    ]
})
export class OnDemandEpisodeListModule
{
}
