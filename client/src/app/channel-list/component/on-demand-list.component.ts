import {
    Component,
    Input,
    OnInit
} from "@angular/core";
import { IGroupedCarousel } from "sxmServices";
import { noResultsPagesConst } from "../../no-results/no-results.const";
import {
    AnalyticsTagActionConstants,
    AnalyticsCarouselNames,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsPageFrames,
    AnalyticsInputTypes,
    AnalyticsElementTypes,
    AnalyticsTagTextConstants,
    AnalyticsFindingMethods
} from "../../analytics/sxm-analytics-tag.constants";

/**
 * @MODULE:     client
 * @CREATED:    08/29/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *    OnDemandListComponent used to load on demand shows
 */

@Component({
    selector   : "sxm-on-demand-list",
    templateUrl: "./on-demand-list.component.html",
    styleUrls  : [ "./on-demand-list.component.scss" ]
})

export class OnDemandListComponent implements OnInit
{
    /**
     * Analytics Constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;

    /**
     * Holds the backup the carousel data
     */
    private _groupedCarousels: Array<IGroupedCarousel>;

    /**
     * Component input of grouped carousel list from it's parent component. Acts as the view model to
     * which our view binds to.
     */
    @Input()
    set groupedCarouselData(groupedCarousels: Array<IGroupedCarousel>)
    {
        this._groupedCarousels = groupedCarousels;

        this._groupedCarousels.forEach((groupedCarousel: IGroupedCarousel) =>
        {
            const length = groupedCarousel.carousel && groupedCarousel.carousel.tiles ? groupedCarousel.carousel.tiles.length : 0;
            this.totalShowCount.length += length;
        });
    }

    /**
     * Returns the grouped carousels
     * @returns {Array<IGroupedCarousel>}
     */
    get groupedCarouselData(): Array<IGroupedCarousel>{ return this._groupedCarousels; }

    /**
     * The text used to filter the list on demand list.
     */
    @Input() filterText: string;

    /**
     * noResultsPageName page/screen Name to render the NoResultsComponent
     */
    public noResultsPageName: string = noResultsPagesConst.ONDEMAND;

    /**
     * Holds the value for number of onDemand shows - used to display or hide no results message.
     */
    public totalShowCount: Array<number> = [];

    /**
     * Check if there are on demand shows for the selected subcategory.
     */
    public ngOnInit()
    {

    }
}


