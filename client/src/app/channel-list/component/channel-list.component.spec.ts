import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { ChannelListComponent } from "./channel-list.component";
import { MockComponent, MockDirective } from "../../../../test/mocks/component.mock";
import { CarouselServiceMock } from '../../../../test/mocks';
import { CarouselService } from "../../carousel/carousel.service";

describe("ChannelListComponent", () =>
{
    let component: ChannelListComponent;
    let fixture: ComponentFixture<ChannelListComponent>;

    beforeEach(() =>
    {

        TestBed.configureTestingModule({
            declarations: [
                ChannelListComponent,
                MockComponent({ selector: "channel-list-item", inputs: [ "tileData", "index", "filterText" ] }),
                MockComponent({ selector: "additional-channel-list-item", inputs: [ "tile", "index", "filterText" ] }),
                MockComponent({ selector: "sxm-no-results", inputs: [ "results", "pageName" ] }),
                MockDirective({ selector: "[sxm-new-analytics]", inputs: ["isTile", "tagName", "userPath", "bannerInd", "tileType", "consumedPerc",
                        "userPathScreenPosition", "pageFrame", "findingMethod", "carouselName", "carouselPosition", "buttonName", "tagText",
                        "elementPosition", "skipActionSource", "elementType", "action", "inputType", "zoneOrder", "tileContentType"]})
            ],
            providers: [
               { provide: CarouselService, useValue: CarouselServiceMock }
            ]
            }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(ChannelListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () =>
    {
        expect(component instanceof ChannelListComponent).toBe(true);
    });
});
