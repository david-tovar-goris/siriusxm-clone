import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { ActivatedRoute } from "@angular/router";
import { of as observableOf } from "rxjs";
import { MockComponent } from "../../../../test/mocks/component.mock";
import { TuneServiceMock } from "../../../../test/mocks/tune.service.mock";
import { TuneClientService } from "../../common/service/tune/tune.client.service";
import { FilterPipe } from "../../filter/filter.pipe";
import { OnDemandEpisodesListComponent } from "./on-demand-episodes-list.component";
import { OrderByPipe } from "../../common/pipe/order-by.pipe";
import { TimeFormatPipe } from "../../common/pipe/time-format.pipe";
import { ChannelListStoreService } from "../../common/service/channel-list.store.service";
import { ChannelListServiceMock } from "../../../../test/mocks/channel-list.service.mock";
import { TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { mockLiveChannelListing } from "../../../../../servicelib/src/test/mocks/channel.lineup";
import { NavigationService } from "../../common/service/navigation.service";
import { navigationServiceMock } from "../../../../test/mocks/navigation.service.mock";
import { AuthenticationService } from "sxmServices";
import { AuthenticationServiceMock } from "../../../../test/mocks/authentication.service.mock";

describe("OnDemandEpisodesListComponent", () =>
{
    let component: OnDemandEpisodesListComponent,
        fixture: ComponentFixture<OnDemandEpisodesListComponent>,
        tuneService: TuneClientService;

    beforeEach(() =>
    {
        const mockActivatedRoute = {
            params: observableOf(
                {
                    type : "video-and-audio"
                }
            ),
            data: observableOf(
                {
                    onDemandDetails: {
                        channel: mockLiveChannelListing[0],
                        show: null
                    }
                }
            )
        };

        TestBed.configureTestingModule({
            imports: [],
            declarations: [
                OnDemandEpisodesListComponent,
                FilterPipe,
                OrderByPipe,
                TimeFormatPipe,
                MockComponent({ selector: "episode-list-item", inputs: ["filterText", "tileData"]}),
                MockComponent({ selector: "sxm-no-results", inputs: [ "filterText", "results", "pageName" ] }),
                MockComponent({ selector: "sxm-filter", inputs: [ "filterType" ] })
            ],
            providers: [
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: TuneClientService, useClass: TuneServiceMock },
                { provide: AuthenticationService, useClass: AuthenticationServiceMock }
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(OnDemandEpisodesListComponent);
        tuneService = TestBed.get(TuneClientService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
});
