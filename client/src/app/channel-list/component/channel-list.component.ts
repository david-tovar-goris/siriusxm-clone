import {
    Component,
    Input
} from "@angular/core";
import {
    CarouselTypeConst,
    ICarouselData,
    ICarouselDataByType
} from "sxmServices";
import { noResultsPagesConst } from "../../no-results/no-results.const";
import { getSegmentCarousel } from "../../../../../servicelib/src/carousel/carousel.selector.util";
import {
    AnalyticsTagActionConstants,
    AnalyticsCarouselNames,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsPageFrames,
    AnalyticsInputTypes,
    AnalyticsElementTypes,
    AnalyticsTagTextConstants,
    AnalyticsFindingMethods
} from "../../analytics/sxm-analytics-tag.constants";

@Component({
    selector: "sxm-channel-list",
    templateUrl: "./channel-list.component.html",
    styleUrls: [ "./channel-list.component.scss" ]
})
export class ChannelListComponent
{
    /**
     * Analytics Constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;

    /**
     * Holds the carousel data
     */
    private _carouselData: ICarouselDataByType;

    /**
     * Holds the carousels
     */
    private _carousels: ICarouselData = { tiles: [] }; // initialize with empty tiles so template always has tile array available
    private additionalChannels: string = CarouselTypeConst.ADDITIONAL_CHANNEL;

    /**
     * Component input of carouselData from it's parent component. Acts as the view model to
     * which our view binds to.
     */
    @Input()
    set carouselData(carouselData: ICarouselDataByType)
    {
        if (!carouselData) { return; }

        const carousels: ICarouselData = getSegmentCarousel(carouselData.selectors,
                                                            CarouselTypeConst.CONTENT_TOGGLE_SELECTOR,
                                                            CarouselTypeConst.SEGMENT_CHANNELS,
                                                            0);

        this._carousels = carousels ? carousels : { tiles: [] };
    }

    get carouselData() : ICarouselDataByType { return this._carouselData; }

    /**
     * filter text
     */
    @Input() filterText: string;

    /**
     * noResultsPageName page/screen Name to render the NoResultsComponent
     */
    public noResultsPageName: string = noResultsPagesConst.CHANNELS;

    /**
     * Returns the carousels
     * @returns {any}
     */
    public get carousels()
    {
        return this._carousels ? this._carousels : null;
    }
}
