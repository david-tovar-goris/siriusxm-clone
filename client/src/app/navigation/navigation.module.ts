import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { TranslationModule } from "../translate/translation.module";
import { NavigationComponent } from "./index";
import { SharedModule } from "../common/shared.module";
import { SearchDropdownComponent } from "../search/search-dropdown/search-dropdown.component";
import { ProfileComponent } from "../profile";
import { RouterModule } from "@angular/router";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        TranslationModule,
        SharedModule,
        RouterModule
    ],
    declarations: [
        NavigationComponent,
        SearchDropdownComponent,
        ProfileComponent
    ],
    providers: [],
    exports: [
        NavigationComponent
    ]
})
export class NavigationModule
{
}
