import { distinctUntilChanged, debounceTime, filter, take, pairwise } from 'rxjs/operators';
import { Observable, BehaviorSubject, Subject } from "rxjs";
import {
    AfterViewInit,
    Component,
    OnInit,
    OnDestroy,
    ViewContainerRef,
    Renderer2,
    ViewChild,
    ChangeDetectorRef,
    ElementRef,
    Inject
} from "@angular/core";
import {
    NavigationEnd,
    Router
}                                  from "@angular/router";
import {
    IAppByPassState,
    Logger,
    SearchService,
    BypassMonitorService,
    IChannel,
    IAutoCompleteResult,
    IAppConfig
} from "sxmServices";
import { appRouteConstants }       from "../app.route.constants";
import { ChannelListStoreService } from "../common/service/channel-list.store.service";
import { mockForYouData }          from "sxmServices";
import { FormControl }             from "@angular/forms";
import { favoriteTypes }           from "../favorites";
import { NavigationService }       from "../common/service/navigation.service";
import { NeriticLinkService }      from "../neritic-links/index";
import { AutoUnSubscribe }         from '../common/decorator';
import { TranslateService }        from "@ngx-translate/core";
import { Store }                   from "@ngrx/store";
import { getBackgroundColor }      from "../common/store/now-playing.store";
import { IAppStore }               from "../common/store/index";
import { CarouselStoreService } from "app/common/service/carousel.store.service";
import {
    AnalyticsCoreNavBarTags,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import { APP_CONFIG } from "app/sxmservicelayer/sxm.service.layer.module";

/**
 * @MODULE:     client
 * @CREATED:    08/01/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     NavigationComponent used to load Navigation page
 */
@AutoUnSubscribe()
@Component({
    selector: "navbar-control",
    templateUrl: "./navigation.component.html",
    styleUrls: [ "./navigation.component.scss" ],
    host: { '(window:resize)': 'this.resizeEvent$.next()' }
})

export class NavigationComponent implements OnInit, AfterViewInit, OnDestroy
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("CategoryComponent");

    /**
     * @type - Boolean value to swap Selected/De selected Home icon.
     */
    public isHomeView: boolean = false;

    public displayProfileDropdown: BehaviorSubject<boolean> = new BehaviorSubject(false);

    /**
     * @type - Boolean value to swap Selected/De selected Favorite icon.
     */
    public isFavoriteView: boolean = false;

    /**
     * @type - Boolean value to swap Selected/De selected Recents icon.
     */
    public isRecentsView: boolean = false;

    /**
     * @type - Boolean value to swap Selected/De selected Recents icon.
     */
    public isSearchView: boolean = false;

    /**
     * Indicates if we are on the Now Playing view.
     */
    public isNowPlayingView: boolean = false;

    /**
     * Indicates if we are on the enhanced edp view.
     */
    public isEnhancedEdpView: boolean = false;

    /**
     * Analytics information
     */
    public navigationAnalytics: {};

    /**
     * @type - search icon url. this value will update based on actions.
     */
    public searchIconUrl = "../../assets/images/srch-off-nav.svg";

    /**
     * @type - show or hide navigation list item . this value will be updated based on actions.
     */
    public isSearchFocusedSmallScreen: boolean = false;

    /**
     * {searchKey} holds the search input value .
     * @type - FormControl.
     */
    public searchKey: FormControl = new FormControl();

    /**
     * {isEnterKeyPressed} holds the boolean value.
     * @type - boolean.
     */
    public isEnterKeyPressed: boolean = false;

    public gupByPass: boolean = false;

    /**
     * A property that should be true if EITHER the search input is focused
     * or if the settings dropdown is focused.
     */
    public isSearchFocused: boolean = false;

    /**
     * Observable string indicating the background color for the NP UI.  This is used in the component to set the
     * background color property below
     */
    private backgroundColor$: Observable<string> = this.store.select(getBackgroundColor);

    /**
     * Property for the template to use to determine background color.  This will be changed when the backgroundColor$
     * observable fires.
     */
    public backgroundColor : string;

    public boundListener: EventListener = null;



    /* Analytics constants */
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsCoreNavBarTags = AnalyticsCoreNavBarTags;
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;

    /**
     * Analytics properties
     */
    public tagName: string;
    public userPath: string;
    public buttonName: string;
    public skipNpData: boolean;
    public userPathScreenPos: number;
    public directTuneSearchResultsCount: number;
    public autoCompleteSearchResultsCount:number;

    @ViewChild('searchForm') searchForm;
    @ViewChild('inputElementRef') inputElementRef : ElementRef;
    @ViewChild('profileBtn') profileBtnElementRef : ElementRef;

    public get navStyle() : any
    {
        if(this.isNowPlayingView)
        {
           return {
                'background-color': this.backgroundColor
            };
        }
    }

    /**
     * {searchResultCount} holds a total count of search results
     * @type - number.
     */
    public searchResultCount: number;

    private resizeEvent$: Subject<any> = new Subject<any>();

    constructor(private router: Router,
                private channelListService: ChannelListStoreService,
                private searchService: SearchService,
                public viewContainerRef: ViewContainerRef,
                private navigationService: NavigationService,
                public neriticLinkService: NeriticLinkService,
                public renderer: Renderer2,
                public bypassMonitorService: BypassMonitorService,
                private translate: TranslateService,
                private store: Store<IAppStore>,
                private changeDetectorRef: ChangeDetectorRef,
                private carouselStoreService: CarouselStoreService,
                @Inject(APP_CONFIG) public appConfig: IAppConfig)
    {
        NavigationComponent.logger.debug("Navigation Component Constructor");

        this.setValues(this.router.url);
        this.bypassMonitorService.bypassErrorState.subscribe((state: IAppByPassState) =>
        {
            this.gupByPass = state.GUP_BYPASS2 || state.GUP_BYPASS;
        });

        this.carouselStoreService.searchCarousels
            .pipe(filter(data => !!data))
            .subscribe(() =>
            {
                this.isSearchFocused = false;
            });
    }

    ngOnInit(): void
    {
        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd),
            pairwise()
        )
        .subscribe((events: [NavigationEnd, NavigationEnd]) =>
        {
            this.setValues(events[1].url);

            if(!events[0].url.includes("/query") &&
                !events[1].url.includes(`/${this.searchKey.value}`))
            {
                this.searchKey.setValue('');
            }

        });

        this.router.events.pipe(
        filter(event => event instanceof NavigationEnd),
        take(1))
        .subscribe((event: NavigationEnd) =>
        {
            this.setValues(event.url);
            this.checkBackground(); // observe background color and make changes to component property for rendering
        });

        this.searchService.searchKeyword.subscribe(keyword =>
        {
           this.searchKey.setValue(keyword);
        });

        this.searchKey.valueChanges.pipe(
            debounceTime(400),
            distinctUntilChanged())
            .subscribe(keyword =>
            {
                this.searchService.updateSearchKeyword(keyword);
            });

        this.neriticLinkService.searchFocus.subscribe((focusSearch: boolean): void =>
        {
            if (focusSearch === true)
            {
                this.setSearchIconUrl(true);
                const selector: string = ".search-input";
                const element = this.renderer.selectRootElement(selector);
                setTimeout(() => element.focus(), 250);
            }

        }, (fault: any): void =>
        {
            NavigationComponent.logger.warn(`searchFocus fault ${JSON.stringify(fault)}`);
        });

        // on window resize reset the super category names view on small and large screens
        this.resizeEvent$.pipe(debounceTime(100)).subscribe(() =>
        {
            if(window.innerWidth > 768 && this.searchKey.value)
            {
                this.setSearchFocusSmallScreen(false);
            }
            else if(this.searchKey.value)
            {
                this.setSearchFocusSmallScreen(true);
            }
        });

        this.searchService.directTuneSearchResults.subscribe((data: Array<IChannel>) =>
        {
            this.directTuneSearchResultsCount = data.length;
        });
        this.searchService.autoCompleteSearchResults.subscribe((data: Array<IAutoCompleteResult>) =>
        {
            this.autoCompleteSearchResultsCount = data.length;
        });
        this.setAnalyticsProperties();
    }

    ngAfterViewInit()
    {
        // insert text so screen readers will announce alert
        document.getElementById('loadedText').innerHTML = this.translate.instant('common.loaded');

        // set focus on logo button
        document.getElementById('logoBtn').focus();
    }


    ngOnDestroy(): void
    {
        this.tearDownListeners();
    }


    /**
     * Tears down a focus listener that listens for any focus events on document.
     */
    public tearDownListeners()
    {
        window.document.removeEventListener('click', this.boundListener, true);
        window.document.removeEventListener('focus', this.boundListener, true);
        this.boundListener = null;
    }


    /**
     * Sets up a focus listener and click listener
     * that listens for any events on the document.
     * Uses capture because focus events do not bubble.
     */
    public setUpListeners()
    {
        if (this.boundListener)
        {
            this.tearDownListeners();
        }
        this.boundListener = this.onDocumentElsewhere.bind(this);
        window.document.addEventListener('focus', this.boundListener, true);
        window.document.addEventListener('click', this.boundListener, true);
    }

    /**
     * @param url - route url
     */
    private setValues(url: string): void
    {
        this.isHomeView = false;
        this.isFavoriteView = false;
        this.isRecentsView = false;
        this.isNowPlayingView = false;
        this.isEnhancedEdpView = false;
        this.isSearchView = false;

        if (url.includes(appRouteConstants.HOME) || url.includes(appRouteConstants.CATEGORY))
        {
            this.isHomeView = true;
        }
        else if (url.includes(appRouteConstants.FAVORITES))
        {
            this.isFavoriteView = true;
        }
        else if (url.includes(appRouteConstants.RECENTLY_PLAYED))
        {
            this.isRecentsView = true;
        }
        else if (url.includes(appRouteConstants.SEARCH))
        {
            this.isSearchView = true;
        }
        else if (url.includes(appRouteConstants.NOW_PLAYING))
        {
            this.isNowPlayingView = true;
        }
        else if (url.includes(appRouteConstants.ENHANCED_EDP))
        {
            this.isEnhancedEdpView = true;
        }

        this.searchIconUrl = this.isNowPlayingView ? "../../assets/images/srch-off-nav-white.svg" : "../../assets/images/srch-off-nav.svg";
    }

    /**
     * home method - Redirects to home route.
     */
    home(): void
    {
        NavigationComponent.logger.debug(`Home()`);

        this.navigationService.go([ appRouteConstants.HOME,
            this.appConfig.defaultSuperCategory.key]);

        this.channelListService.selectSuperCategory(mockForYouData);
    }

    /**
     * favorites method - Redirects to favorites route.
     */
    favorites(): void
    {
        NavigationComponent.logger.debug(`favorites()`);

        this.navigationService.go([ appRouteConstants.FAVORITES, favoriteTypes.CHANNELS ]);
    }

    /**
     * recents method - Redirects to recents route.
     */
    recents(): void
    {
        NavigationComponent.logger.debug(`recents()`);

        this.navigationService.go([ appRouteConstants.RECENTLY_PLAYED ]);
    }

    /**
     * setSearchIconUrl method - sets the search icon url based on focus on the input element.
     * @param isFocus - Html sends value based on action. value determines is element on focus or not.
     */
    setSearchIconUrl(isFocus: boolean): void
    {
        if (!this.router.isActive('/query',false) && isFocus)
        {
            this.navigationService.go([appRouteConstants.SEARCH]);
        }
        this.searchIconUrl = isFocus ? "../../assets/images/srch-on-nav.svg" : "../../assets/images/srch-off-nav.svg";

    }

    /**
     * setSearchFocusSmallScreen method - set the form style and set the value to add / remove focus on search input.
     * @param reset - Html sends value based on action. value determines to reset styles or not.
     */
    setSearchFocusSmallScreen(reset: boolean)
    {
        this.isSearchFocusedSmallScreen = reset;
    }

    /**
     * focusSearch method - set focus onto the search input when clicked on smaller screen sizes (window width<768)
     */
    focusSearch()
    {
        this.changeDetectorRef.detectChanges();
        this.inputElementRef.nativeElement.focus();
    }

    /**
     * submitSearch method
     * TODO: this functionality is not done yet.
     */
    submitSearch(searchString: string, event: any)
    {
        const enterKeyValue = 13;
        const enterKeyPressed = event.keyCode ? event.keyCode : event.which;
        this.isEnterKeyPressed = enterKeyPressed === enterKeyValue;

        if (!this.searchKey.value) return; // do not search empty string

        this.navigationService.go([`${ appRouteConstants.SEARCH }/${ this.searchKey.value }`]);
    }

    /**
     * clearSearch used to clear the search Key value
     * and also Keeps the focus on input element on clear search
     */
    clearSearch(inputElementRef: HTMLInputElement)
    {
        inputElementRef.focus();
        this.searchKey.setValue('');
    }

    onDropdownTriggerClick()
    {
        this.displayProfileDropdown.getValue() ? this.displayProfileDropdown.next(false) : this.displayProfileDropdown.next(true);
    }

    public closeProfileDropdown(event)
    {
        this.displayProfileDropdown.next(event);
    }

    public focusProfileDropdown(event)
    {
        if (event)
        {
            this.profileBtnElementRef.nativeElement.focus();
        }
    }

    /**
     * when dropdown is focused
     */
    public onDropdownFocus(): void
    {
        this.isSearchFocused = true;
    }

    /**
     * When input is focused we have some other code to run
     * input focused needs to set the opacity to 1.
     */
    public onInputFocus(): void
    {
        this.setUpListeners();
        this.setSearchIconUrl(true);
        this.isSearchFocused = true;
        this.setAnalyticsSearchInputProperties();
    }

    public onInputBlur($event): void
    {
        this.setSearchIconUrl(false);

        //on smaller screens show icons when input has no value.
        if(!this.searchKey.value.length && $event.relatedTarget === null)
        {
            this.setSearchFocusSmallScreen(false);
        }
        this.setAnalyticsProperties();
    }

    public setAnalyticsProperties()
    {
        this.tagName = AnalyticsCoreNavBarTags.CNB_SEARCH_SHORTNAME;
        this.userPath = AnalyticsUserPaths.CNB_SEARCH;
        this.buttonName = AnalyticsCoreNavBarTags.CNB_SEARCH_BUTTON_NAME;
        this.skipNpData = true;
        this.userPathScreenPos = 3;
    }

    public setAnalyticsSearchInputProperties()
    {
        this.tagName = AnalyticsTagNameConstants.SEARCH_INPUT;
        this.userPath = AnalyticsUserPaths.SEARCH_INPUT;
        this.buttonName = AnalyticsTagNameConstants.SEARCH_INPUT_BUTTON_NAME;
        this.skipNpData = false;
        this.userPathScreenPos = null;
    }

    public onDocumentElsewhere(event)
    {
        if (!this.searchForm.nativeElement.contains(
            event.target
        ))
        {
            this.isSearchFocused = false;
            this.tearDownListeners();
        }
    }

    /**
     * Uses the backgroundColor$ observable to update the backgroundColor property when the background color changes
     */
    private checkBackground()
    {
        this.backgroundColor$
            .subscribe((backgroundColor) =>
        {
            backgroundColor = backgroundColor || "#243a5c"; // use default if we have no background color

            this.backgroundColor =  backgroundColor;
        });
    }

    public setSearchValue(searchValue)
    {
        this.searchKey.setValue(searchValue);
    }
}
