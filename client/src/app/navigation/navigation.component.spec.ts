import {
    async,
    ComponentFixture,
    fakeAsync,
    inject,
    TestBed
} from "@angular/core/testing";
import { Router } from "@angular/router";
import { RouterStub } from "../../../test/";
import { appRouteConstants } from "../app.route.constants";
import { NavigationComponent } from "../navigation/";
import { TranslationModule } from "../translate/translation.module";
import { ChannelListStoreService } from "../common/service/channel-list.store.service";
import { ChannelListServiceMock } from "../../../test/mocks/channel-list.service.mock";
import { BypassMonitorService, IAppByPassState, mockForYouData, SearchService, CarouselService, SxmAnalyticsService }
    from "sxmServices";
import { SharedModule } from "../common/shared.module";
import { MockComponent, MockDirective } from "../../../test/mocks/component.mock";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SearchServiceMock } from "../../../test/mocks/search/search.service.mock";
import { By } from "@angular/platform-browser";
import { favoriteTypes } from "../favorites";
import { of as observableOf,  BehaviorSubject } from "rxjs";
import { Renderer2 } from "@angular/core";
import { NavigationService } from "../common/service/navigation.service";
import { navigationServiceMock } from "../../../test/mocks/navigation.service.mock";
import { NeriticLinkService } from "../neritic-links";
import { NeriticLinkServiceMock } from "../../../test/mocks/neritic-link.service.mock";
import { CarouselServiceMock } from "../../../test/mocks/carousel.service.mock";
import { TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../test/mocks/translate.service";
import { Store } from "@ngrx/store";
import { APP_CONFIG } from 'app/sxmservicelayer/sxm.service.layer.module';

describe("NavigationComponent", () =>
{
    let component: NavigationComponent = null;
    let fixture: ComponentFixture<NavigationComponent> = null;
    let neriticLinkServiceMock = new NeriticLinkServiceMock();

    let bypassMonitorService = {
        bypassErrorState: observableOf({} as IAppByPassState)
    };

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    let mockAppConfig = {
        defaultSuperCategory :
            {
                key:"foryou"
            },
        deviceInfo :
            {
                appRegion: "defaultValue"
            }
    };

    beforeEach(async(() =>
    {
        this.carouselStoreSubject = new BehaviorSubject(null);

        this.store = {
            select: () => this.carouselStoreSubject
        };

        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SharedModule,
                FormsModule,
                ReactiveFormsModule
            ],
            declarations: [
                NavigationComponent,
                MockComponent({ selector: "profile-dropdown", inputs: ['closeProfileDropdown', 'displayProfileDropdown'] }),
                MockComponent({ selector: "sxm-search-dropdown" }),
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType"]})
            ],
            providers: [
                { provide: BypassMonitorService, useValue: bypassMonitorService },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: NeriticLinkService, useValue: neriticLinkServiceMock },
                { provide: Renderer2, useClass: Renderer2 },
                { provide: Router, useClass: RouterStub },
                { provide: SearchService, useValue: SearchServiceMock.getSpy() },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: Store, useValue: this.store},
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: CarouselService, useValue: CarouselServiceMock },
                { provide: APP_CONFIG, useValue: mockAppConfig }
            ]
        }).compileComponents();

        const mockLoadedAlert = document.createElement("span");
        mockLoadedAlert.setAttribute("id", "loadedText");

        const mockLogoButton = document.createElement("button");
        mockLogoButton.setAttribute("id", "logoBtn");

        document.body.appendChild(mockLoadedAlert);
        document.body.appendChild(mockLogoButton);
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(NavigationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe("Infrastructure >> ", () =>
    {
        it("Should create the component.", () =>
        {
            expect(component instanceof NavigationComponent).toBe(true);
        });
    });


    describe("home() Test Suite >>", () =>
    {
        it("Should be publicly accessible.", () =>
        {
            expect(component.home).toBeDefined();
        });

        it("Should navigate to the home page", inject([ NavigationService ], (navigationService: NavigationService) =>
        {
            const superCat = { key: "foryou"};
            // Spy on our custom navigate method.
            const spy = spyOn(navigationService, "go");

            component.home();
            expect(ChannelListServiceMock.getSpy().selectSuperCategory).toHaveBeenCalledWith(mockForYouData);

            // Run the tests...
            expect(navigationService.go).toHaveBeenCalledWith([ appRouteConstants.HOME, superCat.key ]);
        }));
    });

    describe("favorites() Test Suite >>", () =>
    {
        it("Should be publicly accessible.", () =>
        {
            expect(component.favorites).toBeDefined();
        });

        it("Should navigate to the favorites page", inject([ NavigationService ], (navigationService: NavigationService) =>
        {
            // Spy on our custom navigate method.
            const spy = spyOn(navigationService, "go");

            component.favorites();

            // Run the tests...
            expect(navigationService.go).toHaveBeenCalledWith([ appRouteConstants.FAVORITES, favoriteTypes.CHANNELS ]);
        }));
    });

    describe("recents() Test Suite >>", () =>
    {
        it("Should be publicly accessible.", () =>
        {
            expect(component.recents).toBeDefined();
        });

        it("Should navigate to the recently played page", inject([ NavigationService ], (navigationService: NavigationService) =>
        {
            // Spy on our custom navigate method.
            const spy = spyOn(navigationService, "go");

            component.recents();

            // Run the tests...
            expect(navigationService.go).toHaveBeenCalledWith([ appRouteConstants.RECENTLY_PLAYED ]);
        }));
    });

    describe("Form Search Input >>", () =>
    {
        it("setSearchIconUrl - Should enable search focus icon", fakeAsync(() =>
        {
            let debugElement = fixture.debugElement;
            let searchIconImg = debugElement.query(By.css('.search-item__icon img'));
            expect(searchIconImg.nativeElement.src.indexOf('srch-off-nav.svg') > 0).toEqual(true);

            component.setSearchIconUrl(true);
            fixture.detectChanges();

            searchIconImg = debugElement.query(By.css('.search-item__icon img'));
            expect(searchIconImg.nativeElement.src.indexOf('srch-on-nav.svg') > 0).toEqual(true);
        }));
    });

    describe("Navigation component analytics", () =>
    {
        let event = {
            target: {
                tagName: 'forTest'
            }
        };

        it("should log the discover click event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag").and.callThrough();
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.home-item button'))
                .triggerEventHandler('click', event);
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });

        it("should log the favorites navigation item click event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag").and.callThrough();
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.favorite-item button'))
                .triggerEventHandler('click', event);
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });

        it("should log the recents navigation item click event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag").and.callThrough();
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.recents-item button'))
                .triggerEventHandler('click', event);
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });

        it("should log the search navigation item click event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag").and.callThrough();
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.search-item button'))
                .triggerEventHandler('click', event);
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });

        it("should log the settings navigation item click event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag").and.callThrough();
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.settings-item button'))
                .triggerEventHandler('click', event);
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });

    });
});
