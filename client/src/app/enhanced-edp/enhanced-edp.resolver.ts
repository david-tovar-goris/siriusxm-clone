import { skipWhile, first, switchMap, filter } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot
} from "@angular/router";
import { Observable } from "rxjs";
import {
    CarouselConsts,
    CarouselTypeConst,
    ICarouselDataByType,
    InitializationService,
    InitializationStatusCodes
} from "sxmServices";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { SplashScreenService } from "app/common/service/splash-screen/splash-screen.service";

@Injectable()
export class EnhancedEdpResolver implements Resolve<{}>
{
    constructor(private initService: InitializationService,
                private carouselStoreService: CarouselStoreService,
                private splashScreenService: SplashScreenService)
    {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    {
        return this.initService.initState.pipe(
                          skipWhile((data) => data !== InitializationStatusCodes.RUNNING),
                   switchMap(() => resolveEnhancedEdpData(route, this.carouselStoreService, this.splashScreenService)),
        first());
    }
}

function resolveEnhancedEdpData (route: ActivatedRouteSnapshot,
                                 carouselStoreService: CarouselStoreService,
                                 splashScreenService: SplashScreenService): Observable<ICarouselDataByType>
{
    return carouselStoreService.selectEnhancedEdpCarousel(route.params['url']).pipe(
        filter(data =>
        {
            if(data) { splashScreenService.closeSplashScreen(); }
            return !!data;
        }));
}
