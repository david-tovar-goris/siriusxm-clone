import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";

import { MockTranslateService} from "../../../test/mocks/translate.service";
import { TranslateService } from "@ngx-translate/core";
import { EnhancedEdpComponent } from "./enhanced-edp.component";
import { NavigationService} from "../common/service/navigation.service";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { NeriticLinkServiceMock} from "../../../test/mocks/neritic-link.service.mock";
import { NeriticLinkService } from "../neritic-links/neritic-link.service";
import { CarouselStoreServiceMock } from "../../../test/mocks/carousel.store.service.mock";
import { TranslationModule } from "../translate/translation.module";
import { navigationServiceMock } from "../../../test/mocks/navigation.service.mock";
import { MockComponent } from "../../../test/mocks/component.mock";

describe("EnhancedEdpCompnent()", () =>
{
    let component: EnhancedEdpComponent,
        fixture: ComponentFixture<EnhancedEdpComponent>;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports : [TranslationModule],
            declarations: [
                EnhancedEdpComponent,
                MockComponent({ selector: "vertical-list", inputs: [ "carouselData", "showHeader" ] })
            ],
            providers: [
                { provide: CarouselStoreService, useValue: CarouselStoreServiceMock },
                { provide: NeriticLinkService, useClass: NeriticLinkServiceMock },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: NavigationService, useValue: navigationServiceMock }
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(EnhancedEdpComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
});
