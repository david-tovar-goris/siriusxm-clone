import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TranslationModule } from "../translate/translation.module";
import { EnhancedEdpComponent } from "./enhanced-edp.component";
import { NoResultsModule } from "../no-results/no-results.module";
import { ContextMenuModule } from "../context-menu/context-menu.module";
import { SharedModule } from "../common/shared.module";
import { FavoritesModule } from "../favorites/favorites.module";
import { EnhancedEdpResolver } from "./enhanced-edp.resolver";
import { CarouselModule } from "../carousel";
import { VerticalListModule } from "../common/component/list-items/vertical-list/vertical-list.module";
import { VerticalListEdpScheduleModule } from "../common/component/list-items/vertical-list-edp-schedule/vertical-list-edp-schedule.module";
import { ReminderButtonModule } from "../reminder/reminder-button.module";
import {LineClampComponent} from "app/common/component/line-clamp/line-clamp.component";

@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        NoResultsModule,
        ContextMenuModule,
        FavoritesModule,
        SharedModule,
        CarouselModule,
        VerticalListModule,
        ReminderButtonModule,
        VerticalListEdpScheduleModule
    ],
    declarations: [
        EnhancedEdpComponent,
        LineClampComponent
    ],
    exports: [
        EnhancedEdpComponent
    ],
    providers: [
        EnhancedEdpResolver
    ]
})

export class EnhancedEdpModule
{
}
