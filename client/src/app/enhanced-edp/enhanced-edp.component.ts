import { filter, debounceTime } from 'rxjs/operators';
import {
    Component,
    OnInit,
    ElementRef,
    ViewChild,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    AfterViewInit
} from "@angular/core";
import { AutoUnSubscribe } from "../common/decorator/auto-unsubscribe";
import { NavigationService } from "../common/service/navigation.service";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import {
    ContentTypes,
    getSegmentCarousel,
    ICarouselDataByType,
    ICarouselSelector,
    ICarouselSelectorSegment,
    IPageAssetInfo,
    IFavoriteAsset,
    Logger,
    ChannelLineupService,
    NAME_BACKGROUND,
    POSTER_TILE_IMAGE_WIDTH,
    PLATFORM_WEBEVEREST,
    POSTER_TILE_IMAGE_HEIGHT,
    MediaUtil
} from "sxmServices";
import { SubscriptionLike as ISubscription ,  Subject, BehaviorSubject, Observable } from "rxjs";
import { NeriticLinkService } from "../neritic-links/neritic-link.service";
import {
    CarouselTypeConst
} from "sxmServices";
import * as _ from "lodash";
import { TranslateService } from "@ngx-translate/core";
import { IReminderAssets } from "../reminder/reminder.interface";
import { appRouteConstants } from "app/app.route.constants";
import {
    AnalyticsContentTypes,
    AnalyticsEDPNames,
    AnalyticsElementTypes,
    AnalyticsFindingMethods,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsTileTypes,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import { FavoriteButtonConstant } from "app/favorites/button/favorite-button.constant";
import { LineClampComponent } from "app/common/component/line-clamp/line-clamp.component";

@Component({
    selector: "enhanced-edp-page",
    templateUrl: "./enhanced-edp.component.html",
    styleUrls: [ "./enhanced-edp.component.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})

@AutoUnSubscribe()
export class EnhancedEdpComponent implements OnInit
{
    public carousel: ICarouselDataByType;
    public connectAssetName: string;
    private selector: Array<ICarouselSelector>;
    public segments: Array <ICarouselSelectorSegment>;
    public edpSegmentListCarousel:ICarouselDataByType;
    public edpSegmentScheduleCarousel: ICarouselDataByType;
    public tabTitle: string;
    public connectInfo: boolean;
    public primaryButtonText: string;
    public readLess: boolean;
    public toggleTextBtn: boolean;
    public showTypes:Array<string> = [
        CarouselTypeConst.SEGMENT_ENHANCED_ON_DEMAND_CLASS,
        CarouselTypeConst.SEGMENT_ON_DEMAND_SHOWS_CLASS
    ];
    public scheduleTypes:Array<string> = [
        CarouselTypeConst.SEGMENT_CHANNEL_SCHEDULE_CLASS,
        CarouselTypeConst.SEGMENT_SHOW_SCHEDULE_CLASS
    ];
    public reminderAssets : IReminderAssets = {assetKey:"", assetGuid:"", channelId: "", contentType: "", subContentType: ""};
    public pageBackgroundImageUrl: string;

    public favoriteAsset: IFavoriteAsset;

    public edpView: string = FavoriteButtonConstant.EDP_VIEW;

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("EnhancedPageComponent");

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * Indicates status of pageLogo/showLogo
     */
    public isPageLogoLoaded: boolean = true;

    /**
     * Element reference to the edp page description container and the parent container used to hide the overflow
     * for the show more/show less functionality
     */
    @ViewChild('edpPageDescContainer') edpPageDescContainer: ElementRef;
    @ViewChild('edpPageDescriptionParent') edpPageDescriptionParent: ElementRef;

    private _pageTextLine2$ = new BehaviorSubject<string>(null);
    private _pageTextLine3$ = new BehaviorSubject<string>(null);
    public get pageTextLine2$ (){ return this._pageTextLine2$ as Observable<string>; }
    public get pageTextLine3$ (){ return this._pageTextLine3$ as Observable<string>; }
    public isIrisPodcast: boolean = false;

    // Analytics constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsEDPNames = AnalyticsEDPNames;
    public carouselTypeAnaConst = CarouselTypeConst;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsTileTypes = AnalyticsTileTypes;
    public AnalyticsContentTypes = AnalyticsContentTypes;
    public anaTileAsset = { contentType: "", subContentType: "" };

    /**
     * Constructor
     * @param {NavigationService} navigationService
     * @param {CarouselStoreService} carouselStoreService
     * @param {NeriticLinkService} neriticLinkService
     */
    constructor(public navigationService: NavigationService,
                public carouselStoreService: CarouselStoreService,
                public neriticLinkService: NeriticLinkService,
                private translate : TranslateService,
                private channelLineupService: ChannelLineupService,
                private changeDetectorRef: ChangeDetectorRef)
    {
        this.carousel = {};
    }

    /**
     * ngOnInit
     */
    ngOnInit(): void
    {
        EnhancedEdpComponent.logger.debug("ngOnInit()");

        this.subscriptions.push(
            this.carouselStoreService.enhancedEdpCarousels.pipe(
                filter(data => !!data))
                .subscribe(
                    (carouselData: ICarouselDataByType) =>
                    {
                        if(carouselData.error)
                        {
                            // Note: If in carousel call failed to get the data we navigate to Home route.
                            this.navigationService.go([ appRouteConstants.HOME ], { queryParamsHandling: "merge" });

                            return null;
                        }
                        this.carousel = carouselData;

                        this.setAnalyticsAssetType(this.carousel);

                        this.selector = _.get(this.carousel, "selectors", []);

                        this.connectInfo = false;

                        //pageAssetInfo
                        if (this.carousel.pageAssetInfo)
                        {
                            this.setupConnectInfoData(this.carousel.pageAssetInfo, this.carousel.screen);
                            this.setReminderAssets(this.carousel.pageAssetInfo);
                            this.isIrisPodcast = this.carousel.pageAssetInfo.isIrisPodcast;
                        }

                        //segmentCarousels
                        if (this.selector.length > 0)
                        {
                            this.segments = _.get(this.carousel, "selectors[0].segments", []).filter(segment =>
                                segment.carousels.length > 0);
                            this.tabTitle = this.segments.length > 0? this.segments[0].title: "";
                            this.setupEdpPageInfo(this.carousel);
                        }

                        this._pageTextLine2$.next(this.carousel.pageTextLine2);
                        this._pageTextLine3$.next(this.carousel.pageTextLine3);

                        this.setupPageBackground(this.carousel.pageAssetInfo.channelId);
                    }
                )
        );
    }

    /**
     * Sets reminder assets as needed
     * @param pageAssetInfo
     */
    private setReminderAssets(pageAssetInfo: IPageAssetInfo) : void
    {
        this.reminderAssets.assetKey = "showGUID";
        this.reminderAssets.assetGuid = pageAssetInfo.showGUID;
        this.reminderAssets.channelId = pageAssetInfo.channelId;
        this.reminderAssets.contentType = CarouselTypeConst.SHOW_TILE;
        this.reminderAssets.subContentType = pageAssetInfo.showType === ContentTypes.VOD ?
                        CarouselTypeConst.VOD_SHOW_TILE : CarouselTypeConst.AOD_SHOW_TILE;
    }

    /**
     * Set up connectInfo links and connectAssetName
     * @param pageAssetInfo
     * @param screen
     */
    private setupConnectInfoData(pageAssetInfo, screen)
    {
        if (pageAssetInfo.connectEmail.length > 0 || pageAssetInfo.connectTwitter.length > 0
            || pageAssetInfo.connectFacebook.length > 0 || pageAssetInfo.connectPhone.length > 0)
        {
            this.connectInfo = true;
        }

        switch (screen)
        {
            case CarouselTypeConst.ENHANCED_EPISODE_EDP:
            case CarouselTypeConst.ENHANCED_SHOW_EDP:
                this.connectAssetName = this.carousel.pageAssetInfo.showName;
                break;

            case CarouselTypeConst.ENHANCED_CHANNEL_EDP:
                this.connectAssetName = this.carousel.pageAssetInfo.channelName;
                break;
        }
    }

    /**
     * set up carousels, favorite asset, connectAssetName and primaryButtonText for each screen (CHANNEL/SHOW/EPISODE)
     * @param carouselData
     */
    private setupEdpPageInfo(carouselData)
    {
        switch (carouselData.screen)
        {
            case CarouselTypeConst.ENHANCED_EPISODE_EDP:
                this.connectAssetName = carouselData.pageAssetInfo.episodeName;
                this.edpSegmentListCarousel = getSegmentCarousel(carouselData.selectors,
                    CarouselTypeConst.CONTENT_TOGGLE_SELECTOR,
                    CarouselTypeConst.SEGMENT_ADDITIONAL_EPISODES,
                    0);

                this.primaryButtonText = MediaUtil.isAudioMediaType(carouselData.pageAssetInfo.episodeType)
                    ? this.translate.instant("enhancedEdp.listenNow")
                    : this.translate.instant("enhancedEdp.watchNow");

                let subContentType = CarouselTypeConst.VOD_EPISODE_TILE;
                let episodeGuid = carouselData.pageAssetInfo.vodEpisodeGUID;

                if(carouselData.pageAssetInfo.isIrisPodcast)
                {
                    subContentType = CarouselTypeConst.PODCAST_EPISODE_TILE;
                    episodeGuid = carouselData.pageAssetInfo.episodeGUID;
                }
                else if(carouselData.pageAssetInfo.episodeType === ContentTypes.AOD)
                {
                    subContentType = CarouselTypeConst.AOD_EPISODE_TILE;
                    episodeGuid = carouselData.pageAssetInfo.aodEpisodeCaId;
                }

                this.favoriteAsset =
                {
                    title         : carouselData.pageAssetInfo.episodeName,
                    channelId     : carouselData.pageAssetInfo.channelId,
                    assetGuid     : episodeGuid,
                    contentType   : CarouselTypeConst.EPISODE_TILE,
                    subContentType: subContentType,
                    isIrisPodcast : carouselData.pageAssetInfo.isIrisPodcast
                };
                break;

            case CarouselTypeConst.ENHANCED_SHOW_EDP:
                this.connectAssetName = this.carousel.pageAssetInfo.showName;
                this.edpSegmentListCarousel = getSegmentCarousel(carouselData.selectors,
                    CarouselTypeConst.CONTENT_TOGGLE_SELECTOR,
                    '',
                    0,
                    CarouselTypeConst.SEGMENT_ENHANCED_ON_DEMAND_CLASS);
                this.edpSegmentScheduleCarousel = getSegmentCarousel(carouselData.selectors,
                    CarouselTypeConst.CONTENT_TOGGLE_SELECTOR,
                    '',
                    0,
                    CarouselTypeConst.SEGMENT_SHOW_SCHEDULE_CLASS);
                this.favoriteAsset =
                {
                    title         : carouselData.pageAssetInfo.showName,
                    channelId     : carouselData.pageAssetInfo.channelId,
                    assetGuid     : carouselData.pageAssetInfo.showGUID,
                    contentType   : CarouselTypeConst.SHOW_TILE,
                    subContentType: carouselData.pageAssetInfo.showType === ContentTypes.AOD ?
                                    CarouselTypeConst.AOD_SHOW_TILE : CarouselTypeConst.VOD_SHOW_TILE,
                    isIrisPodcast : carouselData.pageAssetInfo.isIrisPodcast
                };
                break;

            case CarouselTypeConst.ENHANCED_CHANNEL_EDP:
                this.connectAssetName = this.carousel.pageAssetInfo.channelName;
                this.edpSegmentListCarousel = getSegmentCarousel(carouselData.selectors,
                    CarouselTypeConst.CONTENT_TOGGLE_SELECTOR,
                    '',
                    0,
                    CarouselTypeConst.SEGMENT_ON_DEMAND_SHOWS_CLASS);
                this.edpSegmentScheduleCarousel = getSegmentCarousel(carouselData.selectors,
                    CarouselTypeConst.CONTENT_TOGGLE_SELECTOR,
                    '',
                    0,
                    CarouselTypeConst.SEGMENT_CHANNEL_SCHEDULE_CLASS);
                this.primaryButtonText = this.translate.instant("enhancedEdp.listenNow");
                this.favoriteAsset =
                {
                        title         : carouselData.pageAssetInfo.channelName,
                        channelId     : carouselData.pageAssetInfo.channelId,
                        assetGuid     : carouselData.pageAssetInfo.channelGuid,
                        contentType   : CarouselTypeConst.CHANNEL_TILE,
                        subContentType: CarouselTypeConst.LIVE_AUDIO,
                        isIrisPodcast : carouselData.pageAssetInfo.isIrisPodcast

                };
                break;
        }
    }

    /**
     * set up page background from ChannelLineupService
     * @param {string} channelId
     */
    private setupPageBackground( channelId: string) : void
    {
        const channel               = this.channelLineupService.findChannelById(channelId);
        this.pageBackgroundImageUrl = ChannelLineupService.getChannelImage(channel,
        NAME_BACKGROUND,
        PLATFORM_WEBEVEREST,
        POSTER_TILE_IMAGE_WIDTH,
        POSTER_TILE_IMAGE_HEIGHT);
    }

    /**
     * Take appropriate action when an edpPageAction button is clicked
     * @param {string} action
     */
    public takeButtonAction(action: string)
    {
        const buttonAction = this.neriticLinkService.getNeriticData(action);
        this.neriticLinkService.takeAction(buttonAction);
    }

    /**
     * Checks if onDemandEpisode/additionalEpisodes/onDemandShow list carousel is available
     * @param segment
     * @returns {boolean}
     */
    public isListAvailable(segment) : boolean
    {
        return this.showTypes.indexOf(segment.class) >= 0
            && segment.carousels.length > 0
            && segment.carousels[0].tiles.length > 0;
    }

    /**
     * Checks if channelSchedule/showSchedule carousel is available
     * @param segment
     * @returns {boolean}
     */
    public isScheduleAvailable(segment) : boolean
    {
        return this.scheduleTypes.indexOf(segment.class) >= 0
            && segment.carousels.length > 0
            && segment.carousels[0].tiles.length > 0;
    }

    /**
     * animates the header on scroll
     */
    public animateHeader(): void
    {
        const distanceY       = window.pageYOffset || document.documentElement.scrollTop,
              className       = "shrink",
              header          = document.querySelector(".tab-container"),
              headerContainer = document.querySelector('.enhanced-edp-page-header'),
              content         = document.querySelector('.edp-segment-list-container'),
              shrinkOn        = 340;

        if (distanceY > shrinkOn)
        {
            header.classList.add(className);
            headerContainer.classList.add("header-fixed");
            content.classList.add("header-fixed");
        }
        else if (header && header.classList && header.classList.contains(className))
        {
            header.classList.remove(className);
            headerContainer.classList.remove("header-fixed");
            content.classList.remove("header-fixed");
        }
    }

    /**
     * sets analytics asset type
     */
    public setAnalyticsAssetType(carouselData)
    {
        switch (carouselData.screen)
        {
            case CarouselTypeConst.ENHANCED_EPISODE_EDP:
                this.anaTileAsset.contentType = CarouselTypeConst.EPISODE_TILE;
                this.anaTileAsset.subContentType = carouselData.pageAssetInfo.episodeType === ContentTypes.AOD ?
                                                   CarouselTypeConst.AOD_EPISODE_TILE : CarouselTypeConst.VOD_EPISODE_TILE;
                break;

            case CarouselTypeConst.ENHANCED_SHOW_EDP:

                this.anaTileAsset.contentType = CarouselTypeConst.SHOW_TILE;
                this.anaTileAsset.subContentType = carouselData.pageAssetInfo.showType === ContentTypes.AOD ?
                                        CarouselTypeConst.AOD_SHOW_TILE : CarouselTypeConst.VOD_SHOW_TILE;
                break;

            case CarouselTypeConst.ENHANCED_CHANNEL_EDP:
                this.anaTileAsset.contentType = CarouselTypeConst.CHANNEL_TILE;
                this.anaTileAsset.subContentType = CarouselTypeConst.LIVE_AUDIO;
                break;
        }
    }

    /**
     * set the status of PageLogo/ShowLogo
     */
    public onPageLogoError()
    {
        this.isPageLogoLoaded = false;
    }
}
