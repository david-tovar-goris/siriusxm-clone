import { merge, Observable } from 'rxjs';
import { take, filter, map } from 'rxjs/operators';
import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import {
    INeriticLinkData,
    InitializationService,
    InitializationStatusCodes,
    ShowReminder,
    TunePayload,
    LiveTimeService,
    ContentTypes,
    ChannelLineupService,
    MessagingService,
    LiveVideoReminderService,
    MessageTypes,
    OptionalNeriticAction
} from "sxmServices";
import { NeriticLinkService } from "../neritic-links";
import { TuneClientService } from "../common/service/tune/tune.client.service";
import { StickyService } from "app/common/service/sticky/sticky.service";
import {
    AnalyticsCarouselNames,
    AnalyticsElementTypes,
    AnalyticsFindingMethods,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants, AnalyticsToastNameTags,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

/*
 * @MODULE:     client
 * @CREATED:    04/20/18
 * @COPYRIGHT:  2018 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *   ShowRemindersComponent used to display show reminder notifications.
 */

@Component({
    selector: "show-reminders",
    templateUrl: "./show-reminders.component.html",
    styleUrls: ["./show-reminders.component.scss"]
})

export class ShowRemindersComponent implements OnInit
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;
    public AnalyticsToastNameTags = AnalyticsToastNameTags;

    public displayReminders: boolean = false;
    public showReminders: ShowReminder[] = [];
    public liveVideoMessageType = MessageTypes.LIVE_VIDEO_REMINDER;
    public timer: any;

    public paddingTopPx$: Observable<number> = this.stickyService.fullHeightPx$.pipe(
      map((height: number) =>
      {
          if (!height) return null;
          return height + 10;
      })
    );

    /**
     *  Constructor.
     * @param {TuneClientService} tuneClientService
     * @param {ChannelLineupService} channelLineUpService
     * @param {LiveTimeService} liveTimeService
     * @param {MessagingService} messagingService
     * @param {NeriticLinkService} neriticLinkService
     * @param {TranslateService} translate
     * @param {InitializationService} initializationService
     * @param {LiveVideoReminderService} liveVideoReminderService
     */
    constructor(private tuneClientService: TuneClientService,
                private channelLineUpService: ChannelLineupService,
                private liveTimeService: LiveTimeService,
                private messagingService: MessagingService,
                public neriticLinkService: NeriticLinkService,
                public translate: TranslateService,
                public initializationService: InitializationService,
                private liveVideoReminderService: LiveVideoReminderService,
                public stickyService: StickyService)
    {
        merge(
            this.messagingService.showRemindersSubject,
            this.liveVideoReminderService.showReminderSubject
        ).pipe(
            filter(showReminder => !!showReminder)
        ).subscribe((showReminder: ShowReminder) =>
        {
            let howardSternTitle = this.translate.instant("showReminders.howardStern");

            //TODO- Vijaya - Video Reminder Title needs to be dynamic. Change this hardcoded title when the UX is finalized.
            showReminder.titleText = showReminder.messageType === this.liveVideoMessageType ? howardSternTitle : showReminder.titleText;
            this.showReminders.push(showReminder);
            this.messagingService.sendConfirm(showReminder).subscribe();
            this.showNotifications();
        });
    }

    ngOnInit(): void
    {
        this.initializationService.initState.pipe(
            filter(initState => initState === InitializationStatusCodes.RUNNING))
            .subscribe(() =>
            {
                this.showNotifications();
            });
    }

    /**
     * This is to hide the show reminders when user clicks on "dismiss" button
     */
    dismiss(): void
    {
        this.displayReminders = false;
        this.showReminders = [];
    }

    /**
     * This is to display the show reminders
     */
    showNotifications() : void
    {
        this.displayReminders = true;

        // hide the show reminders after 10 seconds
        clearTimeout(this.timer);
        this.timer = setTimeout(() =>
        {
            this.displayReminders = false;
        }, 10000);
    }

    /**
     * Tune to channel that is playing the selected show (live)
     * @param {ShowReminder} showReminder
     */
    public reminderAction(showReminder: ShowReminder): void
    {
        // Manual Live Video Reminders
        if (!showReminder.outOfAppText)
        {
            const payload: TunePayload = {
                channel: this.channelLineUpService.findChannelById(showReminder.channelId),
                channelId : showReminder.channelId,
                contentType: ContentTypes.LIVE_VIDEO
            };
            if (payload.channel)
            {
                this.liveTimeService.liveTime$.pipe(take(1)).subscribe(liveTime =>
                {
                    payload.startTime = liveTime.zuluMilliseconds;
                    this.tuneClientService.tune(payload);
                });
            }
        }
        // API Reminders
        else
        {
            const neriticLink = showReminder.buttons.primaryButton.neriticLink;
            const neriticLinkData: INeriticLinkData = this.neriticLinkService.getNeriticData(neriticLink);
            if (showReminder.messageType === MessageTypes.SUGGESTED_LIVE_VIDEO_REMINDER_HOWARD_STERN)
            {
                neriticLinkData.optionalAction = OptionalNeriticAction.CHECK_SHOW_FOR_VIDEO_REMINDER;
            }
            this.neriticLinkService.takeAction(neriticLinkData);
            this.messagingService.sendFeedback(
                showReminder,
                showReminder.buttons.primaryButton
            ).subscribe();
        }
    }

    /**
     * For Analytics
     */
    public getAnaUserPathScreenPosn(messageType): number
    {
        if(messageType)
        {
            return messageType.split('_').length + 3;
        }
        return 0;
    }
}
