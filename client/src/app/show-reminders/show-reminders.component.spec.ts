import {
    TestBed, tick, fakeAsync
} from "@angular/core/testing";
import { By } from '@angular/platform-browser';
import { Subject, of, BehaviorSubject } from 'rxjs';
import { ShowRemindersComponent } from "./show-reminders.component";
import { TranslationModule } from "../translate/translation.module";
import { TuneClientService } from "../common/service/tune/tune.client.service";
import { TuneServiceMock } from "../../../test/mocks/tune.service.mock";
import {
    ChannelLineupService,
    InitializationService,
    LiveVideoReminderService,
    LiveTimeService,
    INotification,
    MessagingService
} from "sxmServices";
import { channelLineupServiceMock } from "../../../../servicelib/src/channellineup/channel.lineup.service.mock";
import { LiveTimeServiceMock } from "../../../test/mocks/live-time.service.mock";
import { NeriticLinkService } from "../neritic-links";
import { NeriticLinkServiceMock } from "../../../test/mocks/neritic-link.service.mock";
import { StickyService } from "app/common/service/sticky/sticky.service";
import { MockDirective } from "../../../test/mocks/component.mock";

describe('ShowRemindersComponent', function()
{
    beforeEach(function()
    {
        this.messagingService = {
            showRemindersSubject: new Subject(),
            sendConfirm: () => of(true),
            sendFeedback: () => of(true)
        };

        this.liveVideoReminderService = {
            showReminderSubject: new BehaviorSubject(null)
        };

        this.initializationService = { initState: of("running") };

        this.stickyService = {
            fullHeightPx$: new BehaviorSubject(null)
        };

        this.neriticLinkServiceMock = new NeriticLinkServiceMock();

        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [
                ShowRemindersComponent,
                MockDirective({ selector: "[sxm-new-analytics]", inputs: ["tagName", "userPath", "userPathScreenPosition",
                    "elementPosition", "buttonName", "elementType", "action", "inputType", "tagText", "findingMethod", "messageType",
                        "toast", "skipCurrentPosition", "skipContentSource", "skipOrientation"
                    ]})
            ],
            providers: [
                { provide: TuneClientService, useClass: TuneServiceMock },
                { provide: ChannelLineupService, useValue: channelLineupServiceMock },
                { provide: LiveTimeService, useClass: LiveTimeServiceMock },
                { provide: MessagingService, useValue: this.messagingService },
                { provide: NeriticLinkService, useValue: this.neriticLinkServiceMock },
                { provide: LiveVideoReminderService, useValue: this.liveVideoReminderService },
                { provide: InitializationService, useValue: this.initializationService },
                { provide: StickyService, useValue: this.stickyService }
            ]
        });

        this.fixture = TestBed.createComponent(ShowRemindersComponent);
        this.component = this.fixture.componentInstance;
        this.debugElement = this.fixture.debugElement;
    });

    it("the component exists", function()
    {
        expect(this.component).toBeDefined();
    });


    it('displays one show reminder if one emitted', function()
    {
        const showReminder: INotification = {
            buttons: {
                "primaryButton": {
                    "label": "Listen Now"
                }
            },
            expirationDate: "2020-10-17T04:02:40Z",
            isMarketingMessage: false,
            titleText: 'Title1'
        };

        this.messagingService.showRemindersSubject.next(showReminder);

        this.fixture.detectChanges();

        const liDebugElements = this.debugElement.queryAll(By.css('.show-reminder-list__item'));
        expect(liDebugElements.length).toBe(1);
    });


    it('displays one unexpired show reminder message', function()
    {
        const showReminder: INotification = {
            buttons: {
                "primaryButton": {
                    "label": "Listen Now"
                }
            },
            expirationDate: "2020-10-17T04:02:40Z",
            isMarketingMessage: false,
            titleText: 'Title1',
            outOfAppText: 'OutOfAppText1'
        };

        this.messagingService.showRemindersSubject.next(showReminder);

        this.fixture.detectChanges();

        const liDebugElements = this.debugElement.queryAll(By.css('.show-reminder-list__item'));
        const pDebugElement = liDebugElements[0].query(By.css('p.message'));
        expect(pDebugElement.nativeElement.textContent).toEqual('OutOfAppText1');
    });


    it('displays two unexpired show reminders in a list', function()
    {
        const showReminder1: INotification = {
            buttons: {
                "primaryButton": {
                    "label": "Listen Now"
                }
            },
            expirationDate: "2020-10-17T04:02:40Z",
            isMarketingMessage: false,
            titleText: 'Title1',
            outOfAppText: 'OutOfAppText1'
        };

        const showReminder2: INotification = {
            buttons: {
                "primaryButton": {
                    "label": "Listen Now"
                }
            },
            expirationDate: "2020-10-17T04:02:40Z",
            isMarketingMessage: false,
            titleText: 'Title2',
            outOfAppText: 'OutOfAppText2'
        };

        this.messagingService.showRemindersSubject.next(showReminder1);
        this.messagingService.showRemindersSubject.next(showReminder2);

        this.fixture.detectChanges();

        const liDebugElements = this.debugElement.queryAll(By.css('.show-reminder-list__item'));

        expect(liDebugElements.length).toBe(2);
    });


    it('displays two unexpired show reminders with text in a list', function()
    {
        const showReminder1: INotification = {
            buttons: {
                "primaryButton": {
                    "label": "Listen Now"
                }
            },
            expirationDate: "2020-10-17T04:02:40Z",
            isMarketingMessage: false,
            titleText: 'Title1',
            outOfAppText: 'OutOfAppText1'
        };

        const showReminder2: INotification = {
            buttons: {
                "primaryButton": {
                    "label": "Listen Now"
                }
            },
            expirationDate: "2020-10-17T04:02:40Z",
            isMarketingMessage: false,
            titleText: 'Title2',
            outOfAppText: 'OutOfAppText2'
        };

        this.messagingService.showRemindersSubject.next(showReminder1);
        this.messagingService.showRemindersSubject.next(showReminder2);

        this.fixture.detectChanges();

        const liDebugElements = this.debugElement.queryAll(By.css('.show-reminder-list__item'));
        const pDebugElement = liDebugElements[1].query(By.css('p.message'));
        expect(pDebugElement.nativeElement.textContent).toEqual('OutOfAppText2');
    });

    it("can get analytics user path screen position", function()
    {
        let split = this.component.getAnaUserPathScreenPosn("a_b_c_d_e_f_g");
        expect(split).toBe(10);
    });

    it("can hide the show reminders when user clicks on 'dismiss' button", function()
    {
        this.component.displayReminders = true;
        this.component.showReminders = [1, 2, 3];

        this.component.dismiss();

        expect(this.component.displayReminders).toEqual(false);
        expect(this.component.showReminders).toEqual([]);
    });

    it("can display the show reminders",fakeAsync(function()
    {
        this.component.displayReminders = false;
        this.component.showNotifications();
        expect(this.component.displayReminders).toEqual(true);
        tick(11000);
        expect(this.component.displayReminders).toEqual(false);
    }));

    it("can tune to channel that is playing the selected show (live)", function()
    {
        const showReminder = {
            messageType: "",
            outOfAppText: "",
            buttons: {},
            isMarketingMessage: false,
            channelId: "ABCD"
        };

        this.component.reminderAction(showReminder);

        expect(channelLineupServiceMock.findChannelById).toHaveBeenCalledWith("ABCD");
    });

    it("can tune to channel that is playing the selected show (API)", function()
    {
        const showReminder = {
            messageType: "",
            outOfAppText: "YES",
            buttons: {
                primaryButton: {
                    neriticLink: "dummyNeriticLink"
                }
            },
            isMarketingMessage: false,
            channelId: "ABCD"
        };

        this.component.reminderAction(showReminder);

        expect(this.neriticLinkServiceMock.getNeriticData).toHaveBeenCalledWith("dummyNeriticLink");
        expect(this.neriticLinkServiceMock.takeAction).toHaveBeenCalled();
    });
});
