import { combineLatest as observableCombineLatest, Observable, SubscriptionLike as ISubscription } from 'rxjs';
import { share, filter, map } from 'rxjs/operators';
import {
    Component,
    HostListener,
    Input,
    OnDestroy,
    AfterViewInit,
    OnInit,
    ApplicationRef,
    ViewContainerRef,
    ElementRef,
    ViewChild
} from "@angular/core";
import { Store } from "@ngrx/store";
import {
    HtmlElementTypes,
    IApronSegment,
    IChannel,
    IMediaShow,
    KeyboardEventKeyTypes,
    Logger,
    ContentTypes,
    IMediaEpisode,
    SettingsService,
    SettingsConstants,
    BypassMonitorService,
    IAppByPassState,
    DmcaService,
    MediaPlayerService
} from "sxmServices";
import { ChromecastService } from "sxmServices";
import { AutoUnSubscribe } from "../common/decorator";
import {
    IAppStore,
    INowPlayingStore
} from "../common/store";
import { selectSegments } from "../common/store/apron-segment.store";
import {
    getAlbumName,
    getAlbumImage,
    getArtistName,
    getChannelLogoUrl,
    getContentType,
    getMediaType,
    getShow,
    getShowMiniPlayer,
    getTrackName,
    selectNowPlayingState,
    getChannel,
    getEpisode,
    getIsOnDemand,
    getIsLive,
    getIsSeededRadio
} from "../common/store/now-playing.store";
import { ProgressBarService }         from "../media/progress-bar/progress-bar.service";
import { PlayerControlsService }      from "../player-controls/player-controls.service";
import { FavoriteButtonConstant } from "../favorites/button/favorite-button.constant";
import { ChromecastConst } from '../../../../servicelib/src/chromecast/chromecast.const';
import { CastingOverlayService } from "../player-controls/popups/chromecast-overlay/chromecast-overlay.service";
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from "../common/service/toast/toast.service";
import { AffinityConstants } from "../../../../servicelib/src/affinity";
import { Skip15SecondsService } from "./control-buttons/skip-15-seconds/skip-15-seconds.service";
import {
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsScreens,
    AnalyticsPageFrames,
    AnalyticsModals,
    AnalyticsElementTypes,
    AnalyticsTagTextConstants,
    AnalyticsInputTypes,
    AnalyticsTagActionSourceConstants,
    AnalyticsContentTypes
} from "../analytics/sxm-analytics-tag.constants";
import { MediaUtil, MediaTimestampService } from "sxmServices";
import { languageConstants } from "app/common/component/language-toggle/language.consts";
import { BrowserUtil } from "app/common/util/browser.util";

@Component({
    selector: "player-controls",
    templateUrl: "./player-controls.component.html",
    styleUrls: [ "./player-controls.component.scss" ]
})

@AutoUnSubscribe()
export class PlayerControlsComponent implements OnInit, OnDestroy, AfterViewInit
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("PlayerControlsComponent");


    /////////////////////////////////////////////////////////////////////////////////////
    // Streams
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * The currently playing channel.
     */
    public nowPlayingData$: Observable<INowPlayingStore> = this.store.select(selectNowPlayingState);

    /**
     * The can goto live flag stream.
     */
    public canGotoLive$: Observable<boolean> = this.mediaTimestampService.isPlayheadBehindLive$.pipe(share());

    /**
     * The can show goto live flag stream.
     */
    public canShowGotoLive$: Observable<boolean> = this.store.select(selectNowPlayingState)
        .pipe(filter(nowPlayingStore => !!nowPlayingStore),
            map((nowPlayingStore: INowPlayingStore) =>
            {
                const mediaType = nowPlayingStore.mediaType;
                return MediaUtil.isLiveMediaType(mediaType)
                    && (this.dmcaService.isUnrestricted(nowPlayingStore) || this.dmcaService.isRestricted(nowPlayingStore));
            }));

    /**
     * determines if the show can restart.
     */
    public canRestart$: Observable<boolean> = this.store.select(selectNowPlayingState)
        .pipe(filter(nowPlayingStore => !!nowPlayingStore),
            map((nowPlayingStore: INowPlayingStore) =>
            {
                const mediaType = nowPlayingStore.mediaType;
                const isDisallowed: boolean = this.dmcaService.isDisallowed(nowPlayingStore);
                const isOnDemand: boolean = mediaType === ContentTypes.AOD
                    || mediaType === ContentTypes.VOD || mediaType === ContentTypes.PODCAST;
                const isSeededRadio: boolean = mediaType === ContentTypes.SEEDED_RADIO;
                const isAIC: boolean = mediaType === ContentTypes.ADDITIONAL_CHANNELS
                    || mediaType === ContentTypes.AIC;
                const isLive: boolean = mediaType === ContentTypes.LIVE_AUDIO;

                if (isDisallowed === true || isSeededRadio === true || isAIC === true)
                {
                    return false;
                }

                return isLive === true || isOnDemand === true;
            }));

    /**
     * The current show stream.
     */
    public show$: Observable<IMediaShow> = this.store.select(getShow);

    /**
     * The currently playing channel.
     */
    public channel$: Observable<IChannel> = this.store.select(getChannel);

    /**
     * The current channel's logo URL stream.
     */
    public channelLogoUrl$: Observable<string> = this.store.select(getChannelLogoUrl);

    /**
     * The currently playing AOD episode.
     */
    public episode$: Observable<IMediaEpisode> = this.store.select(getEpisode);

    /**
     * List of episode segments that are consumed and used by the UI.
     */
    public segments$: Observable<IApronSegment[]> = this.store.select(selectSegments);

    /**
     * The DMCA is unrestricted flag stream.
     */
    public isDmcaUnrestricted$: Observable<boolean> = this.store.select(selectNowPlayingState)
        .pipe(filter(nowPlayingStore => !!nowPlayingStore),
            map((nowPlayingStore: INowPlayingStore) =>
            {
                return this.dmcaService.isUnrestricted(nowPlayingStore);
            }));

    /**
     * Observable flag indicating if the mini video player should show.
     */
    public showMiniPlayer$: Observable<boolean> = this.store.select(getShowMiniPlayer);

    /**
     * Observable string indicating the media type.
     */
    public mediaType$: Observable<string> = this.store.select(getMediaType);

    /**
     * Observable string indicating the media type.
     */
    public contentType$: Observable<string> = this.store.select(getContentType);

    /**
     * Observable string indicating the media is on demand or not.
     */
    public isOnDemand$: Observable<boolean> = this.store.select(getIsOnDemand);

    /**
     * The album name for the currently playing media.
     */
    public albumName$: Observable<string> = this.store.select(getAlbumName);

    /**
     * The album image URL for the currently playing media.
     */
    public albumImageUrl$: Observable<string> = this.store.select(getAlbumImage);

    /**
     * The artist name for the currently playing media.
     */
    public artistName$: Observable<string> = this.store.select(getArtistName);

    /**
     * The track name for the currently playing media.
     */
    public trackName$: Observable<string> = this.store.select(getTrackName);

    /**
     * Indicates if Live is the current content type.
     */
    public isLive$: Observable<boolean> = this.store.select(getIsLive);

    /**
     * isVideoFullScreen guard to use player controls on full screen.
     */
    @Input() isVideoFullScreen: boolean = false;

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * Tells us if the currently playing audio is artist radio.
     */
    public isSeededRadio$: Observable<boolean> = this.store.select(getIsSeededRadio);

    /**
     * Copy for the coach mark for thumbs up & thumbs down.
     */
    private coachMarkInstructionCopy: any = this.translator.instant("pandoraX.coachMarkInstructionCopy");

    /**
     * This activates the component with the right vars.
     */
    private displayToastMessage(message: string)
    {
        this.toastService.open({
            messagePath: message,
            closeToastTime: 10000,
            hasCloseButton: true,
            screen: "",
            errorType: "",
            isAltColor: true
        });
    }

    /**
     * shows the thumbs down coach mark.
     */
    public showDislikeCoachMark(message: string) : void
    {
        let dislikeCoachmarkSeen = this.settingsService.isCoachmarkFlagOn('DISPLAY_DISLIKE_COACHMARKS');

        if (dislikeCoachmarkSeen === false)
        {
            this.displayToastMessage(message);
            this.settingsService.switchGlobalSettingOnOrOff(true, SettingsConstants.DISPLAY_DISLIKE_COACHMARKS);
        }
    }

    /**
     * shows the thumbs up coach mark.
     */
    public showLikeCoachMark(message: string) : void
    {
        const likeCoachmarkSeen = this.settingsService.isCoachmarkFlagOn('DISPLAY_LIKE_COACHMARKS');

        if (likeCoachmarkSeen === false)
        {
            this.displayToastMessage(message);
            this.settingsService.switchGlobalSettingOnOrOff(true, SettingsConstants.DISPLAY_LIKE_COACHMARKS);
        }
    }

    /**
     * shows the initial instruction coach mark.
     */
    public showCoachInstructions(message: string) : void
    {
        let instructionSeen = this.settingsService.isCoachmarkFlagOn('DISPLAY_LIKE_INSTRUCTIONS');

        if (instructionSeen === false)
        {
            this.displayToastMessage(message);
            this.settingsService.switchGlobalSettingOnOrOff(true, SettingsConstants.DISPLAY_LIKE_INSTRUCTIONS);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Public Variables
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * This is set by our subscription to isOnDemand$
     * It is used by the favoriting icon.
     */
    public isOnDemand: boolean;

    /**
     * Flag indicating if currently playing content is media type live and is also dmca disallowed.
     */
    public isLiveAndDisallowed: boolean;

    /**
     * Now Playing constant
     * @type {string}
     */
    public nowPlaying = FavoriteButtonConstant.NOW_PLAYING_VIEW;

    /**
     * Flag indicating if currently playing content is media type live and is also dmca disallowed.
     */
    public isPlaceholderShow: boolean;

    /**
     * Flag indicating if casting welcome screen is shown.
     */
    public isCastingWelcomeScreen: boolean = false;

    /**
     * Used to store Profile gup bypass enabled or not. If enabled then disables
     * Reminders, application settings and messaging
     * @type {boolean}
     */
    public gupByPass: boolean = false;

    /**
     * Indicates the media is aic or not.
     * @type {boolean}
     */
    public isAICChannel: boolean = false;

    public castingData: any = {
        deviceName : "",
        castStatusMessage: ChromecastConst.CASTING_NONE
    };

    public affinityConstants = AffinityConstants;

    public languageConstants = languageConstants;
    private isChromeBrowser = BrowserUtil.isChrome();

    @ViewChild('googleCastLauncher') googleCastLauncherId: ElementRef;

    // Analytics Constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsContentTypes = AnalyticsContentTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;

    constructor(public progressBarService: ProgressBarService,
                public mediaPlayerService: MediaPlayerService,
                public playerControlsService: PlayerControlsService,
                private store: Store<IAppStore>,
                public mediaTimestampService: MediaTimestampService,
                private chromecastService: ChromecastService,
                private castingOverlayService: CastingOverlayService,
                private applicationRef: ApplicationRef,
                private viewContainerRef: ViewContainerRef,
                private translator: TranslateService,
                private settingsService: SettingsService,
                private bypassMonitorService: BypassMonitorService,
                private toastService: ToastService,
                private skip15SecondsService: Skip15SecondsService,
                private dmcaService: DmcaService)
    {
        this.subscriptions.push(
            this.nowPlayingData$
                .pipe(filter(nowPlayingStore => !!nowPlayingStore))
                .subscribe((nowPlayingStore: INowPlayingStore) =>
                {
                    const mediaType = nowPlayingStore.mediaType;

                    const isLive = mediaType === ContentTypes.LIVE_AUDIO ||
                        mediaType === ContentTypes.LIVE_VIDEO;

                    this.isLiveAndDisallowed = this.dmcaService.isDisallowed(nowPlayingStore) && isLive;
                    this.isAICChannel        = mediaType === ContentTypes.ADDITIONAL_CHANNELS;
                })
        );

        let showSubscription = this.show$.subscribe( show =>
        {
            this.isPlaceholderShow = show.isPlaceholderShow;
        });

        this.subscriptions.push(showSubscription);
        this.subscriptions.push(this.bypassMonitorService.bypassErrorState.subscribe((state: IAppByPassState) =>
        {
            this.gupByPass = state.GUP_BYPASS2 || state.GUP_BYPASS;
        }));

        this.observeChromecastData();
    }

    /**
     * Subscribes to Redux store data changes to populate the UI.
     */
    ngOnInit(): void
    {
        PlayerControlsComponent.logger.debug(`ngOnInit()`);

        this.isSeededRadio$.pipe(filter(response => !!response)).subscribe(response =>
        {
            this.showCoachInstructions(this.coachMarkInstructionCopy);
        });
    }

    /**
     * Used ngAfterViewInit to call resize/get progressbar width only after the Element has been rendered.
     */
    ngAfterViewInit(): void
    {
        PlayerControlsComponent.logger.debug(`ngAfterViewInit()`);

        if(this.gupByPass) { return; }

        const chromeCastElement = this.googleCastLauncherId.nativeElement;
        const isDisplayChromecastOverlayOn = this.settingsService.isGlobalSettingOn(SettingsConstants.DISPLAY_CHROMECAST_OVERLAY);

        // Change the casting button display to none as it is empty string on all non chrome browsers
        if(!this.isChromeBrowser)
        {
            chromeCastElement.style.display = 'none';
        }
        else if (chromeCastElement.style.display === 'block'
            && !isDisplayChromecastOverlayOn)
        {
            this.openCastingOverlay();
            this.settingsService.switchGlobalSettingOnOrOff(true, SettingsConstants.DISPLAY_CHROMECAST_OVERLAY);
        }
    }

    /**
     * shows/hides the casting toast
     * @returns {boolean}
     */
    public get showCastingToast() : boolean
    {
        return this.castingData && this.castingData.castStatusMessage !== ChromecastConst.CASTING_NONE;
    }

    /**
     * Allows users to restart the currently playing episode.
     */
    public restart(): void
    {
        this.playerControlsService.restart();
    }

    /**
     * Allows users to goto the live point for the currently playing episode.
     */
    public goToLive(): void
    {
        this.playerControlsService.gotoLive();
    }

    /**
     * On Mouse Drag over the Player Container
     *
     * @param evt
     */
    public onDragMouse(evt: any): void
    {
        this.progressBarService.onDragMove(evt);
    }

    /**
     * On Mouse Drag Stop over the Player Container
     *
     * @param evt
     */
    public onDragStop(evt: any): void
    {
        this.progressBarService.onDragStop(evt);
    }

    /**
     * Get the chromecast device name from the service to the player component.
     */
    public observeChromecastData(): void
    {
        this.subscriptions.push(this.chromecastService.chromecastInfo.pipe(
            filter(data => !!data))
            .subscribe((data =>
            {
                this.castingData       = {
                    deviceName        : data.deviceName,
                    castStatusMessage: data.castStatusMessage
                };
            })));
    }

    /**
     * Get the chromecast casting welcome screen from the service to the player component.
     */
    openCastingOverlay()
    {
        this.castingOverlayService.open(this.applicationRef.components[0].instance.viewContainerRef, this.viewContainerRef);
    }

    /**
     * Destroy the subscriptions via @AutoUnSubscribe()
     */
    ngOnDestroy(): void
    {
        PlayerControlsComponent.logger.debug(`ngOnDestroy()`);
    }

    /**
     * Keyboard event handler
     */
    @HostListener("document:keydown", [ "$event" ])
    onKeyPress(event: KeyboardEvent)
    {
        if (event.key === KeyboardEventKeyTypes.SPACEBAR)
        {
            // Cast this as an HTML element so that we can access the target's tag name.
            const target = <HTMLElement> event.target;

            // If target is an input (e.g. the search field) return true without pausing or playing.
            if (target.tagName === HtmlElementTypes.INPUT || target.tagName === HtmlElementTypes.BUTTON)
            {
                return true;
            }

            // Prevent scrolling.
            event.preventDefault();

            this.mediaPlayerService.mediaPlayer.togglePausePlay().subscribe();
        }
    }
}
