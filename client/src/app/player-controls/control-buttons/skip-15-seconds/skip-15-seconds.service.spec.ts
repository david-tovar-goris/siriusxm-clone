import { TestBed } from "@angular/core/testing";
import { Skip15SecondsService } from "./skip-15-seconds.service";
import { MediaTimestampServiceMock } from "../../../../../test/mocks/media-timestamp.service.mock";
import { NowPlayingStoreService } from "../../../common/service/now-playing.store.service";
import { NowPlayingStoreServiceMock } from "../../../../../test/mocks/now-playing.store.service.mock";
import { nowPlayingStoreMock } from "../../../../../test/mocks/data/now-playing-store.mock";
import { LiveTimeService, MediaTimestampService } from "sxmServices";
import { LiveTimeServiceMock } from "../../../../../test/mocks/live-time.service.mock";
import { liveTimeMock } from "../../../../../test/mocks/data/live-time.mock";
import { DmcaService } from "sxmServices";

describe('Skip15SecondsService', () =>
{
    let service: Skip15SecondsService,
        mediaTimestampService: MediaTimestampService;

    let dmcaService = {
        isUnrestricted: jasmine.createSpy('isUnrestricted'),
        isRestricted: jasmine.createSpy('isRestricted'),
        isDisallowed: jasmine.createSpy('isDisallowed')
    };

    let setupTestBed = () =>
    {
        TestBed.configureTestingModule({
            providers: [
                { provide: DmcaService, useValue: dmcaService },
                { provide: MediaTimestampService, useClass: MediaTimestampServiceMock },
                { provide: NowPlayingStoreService, useClass: NowPlayingStoreServiceMock },
                { provide: LiveTimeService, useClass: LiveTimeServiceMock },
                Skip15SecondsService
            ]
        });
        service = TestBed.get(Skip15SecondsService);
        mediaTimestampService = TestBed.get(MediaTimestampService);
    };

    describe('displaySkip15BackBtn()', () =>
    {
        describe('when content is disallowed', () =>
        {
            it('returns false to not display the Skip Back 15 seconds button', () =>
            {
                dmcaService.isDisallowed.and.returnValue(true);
                dmcaService.isRestricted.and.returnValue(false);
                setupTestBed();
                expect(service.displaySkip15BackBtn()).toEqual(false);
            });
        });

        describe('when content is restricted', () =>
        {
            it('returns false', () =>
            {
                dmcaService.isDisallowed.and.returnValue(false);
                dmcaService.isRestricted.and.returnValue(true);
                setupTestBed();
                expect(service.displaySkip15BackBtn()).toEqual(false);
            });
        });
    });

    describe('isSkipFwd15VisibilityHidden()', function()
    {
        describe('when within window, close to live point', function()
        {
            it('returns true', function()
            {
                setupTestBed();
                mediaTimestampService.livePointTimestamp = 20;
                mediaTimestampService.playheadTimestamp = 10;
                expect(service.isSkipFwd15VisibilityHidden()).toBe(true);
            });
        });

        describe('when not within window, not close to live point', function()
        {
            it('returns false', function()
            {
                setupTestBed();
                mediaTimestampService.livePointTimestamp = 20;
                mediaTimestampService.playheadTimestamp = 5;
                expect(service.isSkipFwd15VisibilityHidden()).toBe(false);
            });
        });
    });

    describe('displaySkip15FwdBtn()', () =>
    {
        describe('when content is disallowed', () =>
        {
            it('returns false to not display the Skip Back 15 seconds button', () =>
            {
                dmcaService.isDisallowed.and.returnValue(true);
                dmcaService.isRestricted.and.returnValue(false);
                setupTestBed();
                expect(service.displaySkip15FwdBtn()).toEqual(false);
            });
        });

        describe('when content is restricted', () =>
        {
            it('returns false', () =>
            {
                dmcaService.isDisallowed.and.returnValue(false);
                dmcaService.isRestricted.and.returnValue(true);
                setupTestBed();
                expect(service.displaySkip15FwdBtn()).toEqual(false);
            });
        });
    });

    describe('disableBack1hr()', () =>
    {
        describe('when currentPlayHeadTimeStamp is within 1hr of bufferTime', () =>
        {
            it('returns true to not display the Back 1 Hour button', () =>
            {
                liveTimeMock.zuluMilliseconds = 1529613652547; //Thu Jun 21 2018 15:40:52
                nowPlayingStoreMock.playhead.currentTime.zuluMilliseconds = 1529596147847; //Thu Jun 21 2018 10:49:07
                setupTestBed();
                expect(service.disableBack1Hour()).toEqual(true);
            });
        });

        describe('when currentPlayHeadTimeStamp is not within 1hr of bufferTime', () =>
        {
            it('returns false to display the Back 1 Hour button', () =>
            {
                liveTimeMock.zuluMilliseconds = 1529613642547; //Thu Jun 21 2018 15:40:42
                nowPlayingStoreMock.playhead.currentTime.zuluMilliseconds = 1529610059515; //Thu Jun 21 2018 14:40:59
                setupTestBed();
                expect(service.disableBack1Hour()).toEqual(false);
            });
        });
    });
});
