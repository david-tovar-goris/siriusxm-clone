import { Component, Input } from "@angular/core";
import { MediaTimestampService, SeekService } from "sxmServices";
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
@Component({
    selector: "skip-back-15-seconds",
    template: `<li class="center-buttons-list__item">
                    <button class="skip-back-15-btn"
                            sxm-new-analytics
                            [tagName]="AnalyticsTagNameConstants.MNP_BACK_15"
                            [userPath]="AnalyticsUserPaths.MNP_BACK_15"
                            [userPathScreenPosition]="3"
                            [pageFrame]="AnalyticsPageFrames.MODAL"
                            [modal]="AnalyticsModals.MINI_NOW_PLAYING_BAR"
                            [elementType]="AnalyticsElementTypes.BUTTON"
                            [buttonName]="AnalyticsTagNameConstants.MNP_BACK_15_BUTTON_NAME"
                            [elementPosition]="3"
                            [action]=AnalyticsTagActionConstants.BUTTON_CLICK
                            [inputType]=AnalyticsInputTypes.MOUSE
                            [actionSource]="AnalyticsTagActionSourceConstants.MININP"
                            (click)="onClickBack15()"
                            [title]="'playerControls.accessibility.back15' | translate">
                        <div class="btn-focus-container"
                             tabindex="-1">
                            <img class="skip-back-15-btn__img"
                                 *ngIf="!isVideo; else video"
                                 src="../../../../assets/images/transport-controls/back-15.svg"
                                 [alt]="'playerControls.back15' | translate"/>

                            <ng-template #video>
                                <img class="skip-back-15-btn__img"
                                     src="../../../../assets/images/transport-controls/back-15-white.png"
                                     srcset="../../../../assets/images/transport-controls/back-15-white@2x.png 2x,
                                         ../../../../assets/images/transport-controls/back-15-white@3x.png 3x"
                                     [alt]="'playerControls.back15' | translate"/>
                            </ng-template>
                        </div>
                    </button>
                </li>`,
    styleUrls: ["../control-buttons.scss"]
})
export class SkipBack15SecondsComponent
{
    @Input() content: {};
    @Input() isVideo: boolean = false;

    //Analytics constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    constructor(private mediaTimestampService: MediaTimestampService,
                private seekService: SeekService){}

    public onClickBack15(): void
    {
        const seconds = (this.mediaTimestampService.playheadTimestamp > 15) ? (this.mediaTimestampService.playheadTimestamp - 15) : 0;
        this.seekService.seekThenPlay(seconds, false);
    }
}
