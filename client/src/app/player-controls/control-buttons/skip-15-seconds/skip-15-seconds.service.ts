import { Injectable } from "@angular/core";
import { NowPlayingStoreService } from "../../../common/service/now-playing.store.service";
import { SubscriptionLike as ISubscription } from "rxjs";
import { INowPlayingStore } from "../../../common/store";
import {
    ILiveTime,
    LiveTimeService,
    DmcaService,
    MediaTimestampService
} from "sxmServices";
import { Skip15ServiceConsts } from "./skip-15-service.consts";

@Injectable()
export class Skip15SecondsService
{
    private isRestricted;
    private isDisallowed;
    private nowPlayingStoreData: INowPlayingStore;
    private liveTimeZulu;

    constructor(private dmcaService: DmcaService,
                private mediaTimestampService: MediaTimestampService,
                private nowPlayingStoreService: NowPlayingStoreService,
                private liveTimeService: LiveTimeService)
    {
        this.observeNowPlayingStore();

        this.liveTimeService.liveTime$.subscribe((liveTime: ILiveTime) =>
        {
            this.liveTimeZulu = liveTime.zuluMilliseconds;
        });
    }

    /**
     * Determines whether the skip 15 back button should be displayed or not.
     */
    public displaySkip15BackBtn(): boolean
    {
        return !this.isDisallowed && !this.isRestricted;
    }

    /**
     * Determines whether the skip 15 fwd button should be displayed or not.
     */
    public displaySkip15FwdBtn(): boolean
    {
        return !this.isDisallowed && !this.isRestricted;
    }


    public isSkipFwd15VisibilityHidden(): boolean
    {
        return this.withinLiveLimit();
    }

    /**
     * Determines if the "Back 1 Hour Button" should be disabled
     * This button should be disabled if
     *
     * We are on the now playing view where the live channel is playing a placeholder show     AND
     * the current playheadTimestamp is within 1 hr from the fiveHourBufferTime (liveTime - 5hours)
     * @returns {boolean}
     */
    public disableBack1Hour(): boolean
    {
        const fiveHourBufferZulu = this.liveTimeZulu - Skip15ServiceConsts.FIVE_HOUR_BUFFER_MS;
        return this.nowPlayingStoreData.playhead.currentTime.zuluMilliseconds - fiveHourBufferZulu <= Skip15ServiceConsts.ONE_HOUR_MS;
    }

    private withinLiveLimit(): boolean
    {
        return this.mediaTimestampService.livePointTimestamp - this.mediaTimestampService.playheadTimestamp
            <= Skip15ServiceConsts.SECONDS_BEHIND_LIVEPOINT_CONSIDERED_LIVE;
    }

    private observeNowPlayingStore(): ISubscription
    {
        return this.nowPlayingStoreService.nowPlayingStore.subscribe((nowPlayingStore: INowPlayingStore) =>
        {
            this.nowPlayingStoreData = nowPlayingStore;
            this.isRestricted = this.dmcaService.isRestricted(nowPlayingStore);
            this.isDisallowed = this.dmcaService.isDisallowed(nowPlayingStore);
        });
    }
}
