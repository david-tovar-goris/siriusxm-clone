export class Skip15ServiceConsts
{
    public static FIVE_HOUR_BUFFER_MS: number = 18000000;
    public static ONE_HOUR_MS: number = 3600000;
    public static ONE_HOUR_S: number = 3600;
    public static SECONDS_BEHIND_LIVEPOINT_CONSIDERED_LIVE = 14;
}
