import { SkipForward15SecondsComponent } from "./skip-forward-15-seconds.component";
import { TestBed } from "@angular/core/testing";
import { SeekService, MediaTimestampService, SxmAnalyticsService } from "sxmServices";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../../test/mocks/router.mock";
import { MockComponent, MockDirective } from "../../../../../test/mocks/component.mock";
import { By } from "@angular/platform-browser";
import { TranslationModule } from "app/translate/translation.module";
import { SharedModule } from "app/common/shared.module";
import {Store} from "@ngrx/store";
import {IAppStore} from "app/common/store";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import { Skip15SecondsService } from "./skip-15-seconds.service";
import { Skip15SecondsServiceMock } from "../../../../../test/mocks/skip-15-seconds.service.mock";

describe('SkipForward15SecondsComponent', function()
{

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(function()
    {
        this.seekService = {
            seekThenPlay: jasmine.createSpy('seekThenPlay')
        };

        this.mediaTimestampService = {

        };

        this.nowPlayingAnalyticsService = {

        };

        let nowPlayingStore = new BehaviorSubject<any>(null);

        let store = {
            select: () => nowPlayingStore
        } as any as Store<IAppStore>;

        TestBed.configureTestingModule({
            declarations: [
                SkipForward15SecondsComponent,
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType",
                        "userPathScreenPosition", "modal", "tagText", "actionSource"]})
            ],
            providers: [
                SkipForward15SecondsComponent,
                { provide: SeekService, useValue: this.seekService },
                { provide: MediaTimestampService, useValue: this.mediaTimestampService },
                { provide: Router, useClass: RouterStub },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: Store, useValue: store },
                { provide: Skip15SecondsService, useClass: Skip15SecondsServiceMock }
            ],
            imports: [
                TranslationModule,
                SharedModule
            ]
        });

        this.component = TestBed.get(SkipForward15SecondsComponent);
    });

    describe('Click forward 15 seconds', function()
    {
        it('seeks to playhead + 15 when playhead + 15 seconds is less than duration timestamp', function()
        {

            this.component.mediaTimestampService.playheadTimestamp = 0;
            this.component.mediaTimestampService.durationTimestamp = 100;

            this.component.onClickForward15();

            expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(15, false);
        });

        it('seeks to duration timestamp when playhead + 15 is greater than duration timestamp', function()
        {
            this.component.mediaTimestampService.playheadTimestamp = 100;
            this.component.mediaTimestampService.durationTimestamp = 90;

            this.component.onClickForward15();

            expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(this.component.mediaTimestampService.durationTimestamp, false);
        });
    });

    describe('skip forward 15 seconds analytics', function()
    {

        it('logs the skip forward button click event', function()
        {
            let fixture = TestBed.createComponent(SkipForward15SecondsComponent);
            let component = fixture.componentInstance;

            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.skip-forward-15-btn'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "MnpFwd15"
                    }
                });
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();

        });
    });
});
