import { SkipBack15SecondsComponent } from "./skip-back-15-seconds.component";
import { TestBed } from "@angular/core/testing";
import { SeekService, MediaTimestampService, SxmAnalyticsService } from "sxmServices";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../../test/mocks/router.mock";
import { MockComponent, MockDirective } from "../../../../../test/mocks/component.mock";
import { By } from "@angular/platform-browser";
import { TranslationModule } from "app/translate/translation.module";
import { SharedModule } from "app/common/shared.module";
import {Store} from "@ngrx/store";
import {IAppStore} from "app/common/store";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";

describe('SkipBack15SecondsComponent', function()
{

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(function()
    {
        this.seekService = {
            seekThenPlay: jasmine.createSpy('seekThenPlay')
        };

        this.mediaTimestampService = {

        };

        this.nowPlayingAnalyticsService = {

        };

        let nowPlayingStore = new BehaviorSubject<any>(null);

        let store = {
            select: () => nowPlayingStore
        } as any as Store<IAppStore>;

        TestBed.configureTestingModule({
            declarations: [
                SkipBack15SecondsComponent,
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType",
                        "userPathScreenPosition", "modal", "tagText", "actionSource"]})
            ],
            providers: [
                SkipBack15SecondsComponent,
                { provide: SeekService, useValue: this.seekService },
                { provide: MediaTimestampService, useValue: this.mediaTimestampService },
                { provide: Router, useClass: RouterStub },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: Store, useValue: store }
            ],
            imports: [
                TranslationModule,
                SharedModule
            ]
        });

        this.component = TestBed.get(SkipBack15SecondsComponent);
    });

    describe('onClickBack15()', function()
    {
        it('seeks to the playheadTimestamp - 15 seconds when playhead greater than 15', function()
        {
            this.component.mediaTimestampService.playheadTimestamp = 50;
            this.component.onClickBack15();
            expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(35, false);
        });

        it('calls seek with zero when playhead less than 15', function()
        {
            this.component.mediaTimestampService.playheadTimestamp = 14;
            this.component.onClickBack15();
            expect(this.seekService.seekThenPlay).toHaveBeenCalledWith(0, false);
        });
    });

    describe('skip back 15 seconds analytics', function()
    {

        it('logs the skip back button click event', function()
        {
            let fixture = TestBed.createComponent(SkipBack15SecondsComponent);
            let component = fixture.componentInstance;

            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.skip-back-15-btn'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "MnpBack15"
                    }
                });
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();

        });
    });
});
