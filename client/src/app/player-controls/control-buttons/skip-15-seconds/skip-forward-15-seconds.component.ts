import { Component, Input } from "@angular/core";
import { Skip15SecondsService } from "./skip-15-seconds.service";
import { MediaTimestampService, SeekService } from "sxmServices";
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
@Component({
    selector: "skip-forward-15-seconds",
    template: `<li class="center-buttons-list__item"
                   [ngClass]="{ 'visibility-hidden': skip15Service.isSkipFwd15VisibilityHidden() }">
                    <button class="skip-forward-15-btn"
                            sxm-new-analytics
                            [tagName]="AnalyticsTagNameConstants.MNP_FORWARD_15"
                            [userPath]="AnalyticsUserPaths.MNP_FORWARD_15"
                            [userPathScreenPosition]="3"
                            [pageFrame]="AnalyticsPageFrames.MODAL"
                            [modal]="AnalyticsModals.MINI_NOW_PLAYING_BAR"
                            [elementType]="AnalyticsElementTypes.BUTTON"
                            [buttonName]="AnalyticsTagNameConstants.MNP_FORWARD_15_BUTTON_NAME"
                            [elementPosition]="7"
                            [action]=AnalyticsTagActionConstants.BUTTON_CLICK
                            [inputType]=AnalyticsInputTypes.MOUSE
                            [actionSource]="AnalyticsTagActionSourceConstants.MININP"
                            (click)="onClickForward15()"
                            title="{{ 'playerControls.accessibility.forward15' | translate}}">
                        <div class="btn-focus-container"
                             tabindex="-1">
                            <img class="skip-forward-15-btn__img"
                                 *ngIf="!isVideo; else video"
                                 src="../../../../assets/images/transport-controls/forward-15.svg"
                                 alt="{{ 'playerControls.forward15' | translate }}"/>

                            <ng-template #video>
                                <img class="skip-forward-15-btn__img"
                                     src="../../../../assets/images/transport-controls/forward-15-white.png"
                                     srcset="../../../../assets/images/transport-controls/forward-15-white@2x.png 2x,
                                         ../../../../assets/images/transport-controls/forward-15-white@3x.png 3x"
                                     alt="{{ 'playerControls.forward15' | translate }}"/>
                            </ng-template>
                        </div>
                    </button>
                </li>`,
    styleUrls: ["../control-buttons.scss"]
})
export class SkipForward15SecondsComponent
{
    @Input() content: {};
    @Input() isVideo: boolean = false;

    //Analytics constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    constructor(public skip15Service: Skip15SecondsService,
                private mediaTimestampService: MediaTimestampService,
                private seekService: SeekService){}

    public onClickForward15(): void
    {
        const seconds = ((this.mediaTimestampService.playheadTimestamp + 15) < this.mediaTimestampService.durationTimestamp)
            ? (this.mediaTimestampService.playheadTimestamp + 15)
            : this.mediaTimestampService.durationTimestamp;

        this.seekService.seekThenPlay(seconds, false);
    }
}
