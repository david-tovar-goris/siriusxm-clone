import {
    SkipButtonService
} from "./skip-button.service";
import { TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { nowPlayingReducer } from "app/common/reducer/now-playing.reducer";
import { DmcaService, MediaTimeLineService } from "sxmServices";
import { LiveTimeService } from "sxmServices";
import { BehaviorSubject, Subject } from "rxjs";
import { INowPlayingActionPayload, SelectNowPlaying } from "app/common/action/now-playing.action";
import { SKIP_TYPE } from "app/player-controls/control-buttons/skip-button/skip.directions";

describe('SkipButtonService', function()
{
    beforeEach(function()
    {
        jasmine.clock().uninstall();
        jasmine.clock().install();

        this.dmcaService = {
            isUnrestricted: jasmine.createSpy('isUnrestricted'),
            isRestricted: jasmine.createSpy('isRestricted'),
            isDisallowed: jasmine.createSpy('isDisallowed'),
            getSkipInfo: jasmine.createSpy('getSkipInfo'),
            getMaxTotalSkips: jasmine.createSpy('getMaxTotalSkips'),
            getMaxBackSkips: jasmine.createSpy('getMaxBackSkips'),
            skipFwdWasSuccessful: jasmine.createSpy('skipFwdWasSuccessful'),
            skipBackWasSuccessful: jasmine.createSpy('skipBackWasSuccessful'),
            skipHappenedSkipInfo$: new Subject()

        };

        this.liveTimeService = {
            liveTime$: new BehaviorSubject({
                zuluMilliseconds: 0,
                zuluSeconds: 0
            })
        };

        this.mediaTimeLineService = {
            mediaTimeLine : new BehaviorSubject({
                clips: {
                    getCurrentTrack: jasmine.createSpy('getCurrentTrack').and.returnValue({
                        nextTrack: true
                    })
                }
            })
        };

        this.setup = () =>
        {

        };

        TestBed.configureTestingModule({
            imports: [
                StoreModule.forRoot({
                    nowPlayingStore: nowPlayingReducer
                })
            ],
            providers: [
                SkipButtonService,
                { provide: DmcaService, useValue: this.dmcaService },
                { provide: LiveTimeService, useValue: this.liveTimeService },
                { provide: MediaTimeLineService, useValue: this.mediaTimeLineService }
            ]
        });

        this.service = TestBed.get(SkipButtonService);
        this.store = TestBed.get(Store);
    });

    afterEach(function()
    {
        jasmine.clock().uninstall();
    });

    describe('constructor()', function()
    {
        describe('when disallowed', function()
        {
            beforeEach(function ()
            {
                this.dmcaService.isDisallowed.and.returnValue(true);
                let payload = {
                    mediaId: 'gwe3',
                    mediaType: 'live',
                    dmcaInfo: {
                        irNavClass: "DISALLOWED_0",
                        maxBackSkips: 1,
                        maxFwdSkips: 6,
                        maxTotalSkips: 6
                    }
                } as INowPlayingActionPayload;
                this.store.dispatch(new SelectNowPlaying(payload));
            });

            it('sets skip buttons state to disallowed', function()
            {
                expect(this.service.skipButtonsState).toEqual({
                    visible: false,
                    forward: false,
                    back: false
                });
            });
        });


        describe('not disallowed and not exhausted skips and skip occurs', function()
        {
            beforeEach(function ()
            {
                const skipInfo = {
                    mediaId: 'gwe3',
                    mediaType: 'live',
                    numSkipsBack: 0,
                    numSkipsForward: 5,
                    maxTotalSkips: 6
                };
                this.dmcaService.isDisallowed.and.returnValue(false);
                this.dmcaService.getMaxBackSkips.and.returnValue(1);
                this.dmcaService.getMaxTotalSkips.and.returnValue(6);
                this.dmcaService.getSkipInfo.and.returnValue(skipInfo);

                let payload = {
                    mediaId: 'gwe3',
                    mediaType: 'live',
                    dmcaInfo: {
                        irNavClass: "DISALLOWED_0",
                        maxBackSkips: 1,
                        maxFwdSkips: 6,
                        maxTotalSkips: 6
                    }
                } as INowPlayingActionPayload;
                this.store.dispatch(new SelectNowPlaying(payload));

                this.dmcaService.skipHappenedSkipInfo$.next(
                    skipInfo
                );
            });

            it('sets button state to disabled then enabled', function()
            {
                expect(this.service.skipButtonsState).toEqual(
                    {
                        visible: true,
                        forward: false,
                        back   : false
                    }
                );

                jasmine.clock().tick(2500);

                expect(this.service.skipButtonsState).toEqual(
                    {
                        visible: true,
                        forward: true,
                        back   : true
                    }
                );

            });
        });

        describe('not disallowed and exhausted skips and skip occurs', function()
        {
            beforeEach(function ()
            {
                const skipInfo = {
                    mediaId: 'gwe3',
                    mediaType: 'live',
                    numSkipsBack: 1,
                    numSkipsForward: 5,
                    maxTotalSkips: 6
                };
                this.dmcaService.isDisallowed.and.returnValue(false);
                this.dmcaService.getMaxBackSkips.and.returnValue(1);
                this.dmcaService.getMaxTotalSkips.and.returnValue(6);
                this.dmcaService.getSkipInfo.and.returnValue(skipInfo);

                let payload = {
                    mediaId: 'gwe3',
                    mediaType: 'live',
                    dmcaInfo: {
                        irNavClass: "RESTRICTED_0",
                        maxBackSkips: 1,
                        maxFwdSkips: 6,
                        maxTotalSkips: 6
                    }
                } as INowPlayingActionPayload;
                this.store.dispatch(new SelectNowPlaying(payload));

                this.dmcaService.skipHappenedSkipInfo$.next(
                    skipInfo
                );
            });

            it('sets button state to disabled then disabled', function()
            {
                expect(this.service.skipButtonsState).toEqual(
                    {
                        visible: true,
                        forward: false,
                        back: false
                    }
                );

                jasmine.clock().tick(2500);

                expect(this.service.skipButtonsState).toEqual(
                    {
                        visible: true,
                        forward: false,
                        back: false
                    }
                );
            });
        });

        describe('not disallowed and not exhausted skips and exhausted back skip and skip occurs', function()
        {
            beforeEach(function ()
            {
                const skipInfo = {
                    mediaId: 'gwe3',
                    mediaType: 'live',
                    numSkipsBack: 1,
                    numSkipsForward: 4,
                    maxTotalSkips: 6
                };
                this.dmcaService.isDisallowed.and.returnValue(false);
                this.dmcaService.getMaxBackSkips.and.returnValue(1);
                this.dmcaService.getMaxTotalSkips.and.returnValue(6);
                this.dmcaService.getSkipInfo.and.returnValue(skipInfo);

                let payload = {
                    mediaId: 'gwe3',
                    mediaType: 'live',
                    dmcaInfo: {
                        irNavClass: "RESTRICTED_0",
                        maxBackSkips: 1,
                        maxFwdSkips: 6,
                        maxTotalSkips: 6
                    }
                } as INowPlayingActionPayload;
                this.store.dispatch(new SelectNowPlaying(payload));

                this.dmcaService.skipHappenedSkipInfo$.next(
                    skipInfo
                );
            });

            it('sets button state to disabled then disabled', function()
            {
                expect(this.service.skipButtonsState).toEqual(
                    {
                        visible: true,
                        forward: false,
                        back: false
                    }
                );

                jasmine.clock().tick(2500);

                expect(this.service.skipButtonsState).toEqual(
                    {
                        visible: true,
                        forward: true,
                        back   : false
                    }
                );
            });
        });

        describe('onSkip()', function()
        {
            beforeEach(function ()
            {
                let payload = {
                    mediaId: 'gwe3',
                    mediaType: 'live',
                    dmcaInfo: {
                        irNavClass: "RESTRICTED_0",
                        maxBackSkips: 1,
                        maxFwdSkips: 6,
                        maxTotalSkips: 6
                    }
                } as INowPlayingActionPayload;

                this.store.dispatch(new SelectNowPlaying(payload));
            });

            it('when skip direction is forward', function()
            {
                this.service.onSkip(SKIP_TYPE.FORWARD);
                expect(this.dmcaService.skipFwdWasSuccessful).toHaveBeenCalled();
            });

            it('when skip direction is backword', function()
            {
                this.service.onSkip(SKIP_TYPE.BACKWARD);
                expect(this.dmcaService.skipBackWasSuccessful).toHaveBeenCalled();
            });
        });
    });
});
