import {
    TestBed
} from "@angular/core/testing";
import { Subject } from 'rxjs';
import { SkipMessageComponent } from "./skip-message.component";
import { SkipButtonService } from "./skip-button.service";
import { ChromecastPlayerService } from "../../../../../../servicelib/src/mediaplayer/chromecastplayer";
import { TranslateService } from '@ngx-translate/core';

describe("SkipMessageComponent", function()
{
    beforeEach(function()
    {
        this.skipButtonService = {
            displaySkipMessage$: new Subject()
        };

        this.chromecastPlayerService = {

        };

        this.translate = {
            instant: jasmine.createSpy('instant').and.returnValue('skips left')
        };

        TestBed.configureTestingModule({
            declarations: [
                SkipMessageComponent
            ],
            providers: [
                { provide: SkipButtonService, useValue: this.skipButtonService },
                { provide: ChromecastPlayerService, useValue: this.chromecastPlayerService },
                { provide: TranslateService, useValue: this.translate }
            ]
        }).compileComponents();

        const fixture = TestBed.createComponent(SkipMessageComponent);
        this.component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('constructor()', function()
    {
        describe('when skips left is greater than skip threshold and isVisible true', function()
        {
            beforeEach(function()
            {
                this.skipButtonService.displaySkipMessage$.next({
                    isVisible: true,
                    skipInfo: {
                        numSkipsForward: 2,
                        numSkipsBack: 0,
                        maxTotalSkips: 5
                    }
                });
            });

            it('displayMessage remains false', function()
            {
                expect(this.component.displayMessage).toBe(false);
            });
        });

        describe('when skips left is equal to skip threshold and isVisible true', function()
        {
            beforeEach(function()
            {
                this.skipButtonService.displaySkipMessage$.next({
                    isVisible: true,
                    skipInfo: {
                        numSkipsForward: 3,
                        numSkipsBack: 0,
                        maxTotalSkips: 5
                    }
                });
            });

            it('displayMessage remains false', function()
            {
                expect(this.component.displayMessage).toBe(true);
            });
        });


        describe('when skips left is equal to skip threshold and isVisible false', function()
        {
            beforeEach(function()
            {
                this.skipButtonService.displaySkipMessage$.next({
                    isVisible: false,
                    skipInfo: {
                        numSkipsForward: 3,
                        numSkipsBack: 0,
                        maxTotalSkips: 5
                    }
                });
            });

            it('displayMessage remains false', function()
            {
                expect(this.component.displayMessage).toBe(false);
            });
        });
    });

    describe('getSkipMessageData()', function()
    {
        beforeEach(function()
        {
            this.skipButtonService.displaySkipMessage$.next({
                isVisible: true,
                skipInfo: {
                    numSkipsForward: 3,
                    numSkipsBack: 0,
                    maxTotalSkips: 5,
                    skipTimestamp: 'Mon, 05 Aug 2019 10:17:17 GMT'
                }
            });
        });

        it('calculates skips left', function()
        {
            expect(this.component.skipData.skipsLeft).toBe(2);
        });

        it('sets time stamp to be one hour later', function()
        {
            expect(this.component.skipData.skipTimestamp.toDate().toUTCString())
                .toEqual('Mon, 05 Aug 2019 11:17:17 GMT');
        });
    });


    describe('getSkipMessageText()', function()
    {
        beforeEach(function()
        {
            this.skipButtonService.displaySkipMessage$.next({
                isVisible: true,
                skipInfo: {
                    numSkipsForward: 3,
                    numSkipsBack: 0,
                    maxTotalSkips: 5,
                    skipTimestamp: 'Mon, 05 Aug 2019 10:17:17 GMT'
                }
            });
        });

        it('has correct skipmessage', function()
        {
            expect(this.component.getSkipMessageText())
                .toEqual('2 skips left');
        });
    });
});
