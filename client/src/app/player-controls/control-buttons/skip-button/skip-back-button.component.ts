import { Component, Input } from "@angular/core";
import { IDmcaInfoItem, SkipService } from "sxmServices";
import { SkipButtonService } from "./skip-button.service";
import { INowPlayingStore } from "../../../common/store";
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes, AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens, AnalyticsTagActionConstants, AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
@Component({
    selector: "skip-back-button",
    template: `
        <li class="center-buttons-list__item">
            <button class="skip-back-btn"
                    sxm-new-analytics
                    [tagName]="AnalyticsTagNameConstants.MNP_BACK_SKIP"
                    [userPath]="AnalyticsUserPaths.MNP_BACK_SKIP"
                    [userPathScreenPosition]="3"
                    [pageFrame]="AnalyticsPageFrames.MODAL"
                    [modal]="AnalyticsModals.MINI_NOW_PLAYING_BAR"
                    [elementType]="AnalyticsElementTypes.BUTTON"
                    [buttonName]="AnalyticsTagNameConstants.MNP_BACK_SKIP_BUTTON_NAME"
                    [elementPosition]="4"
                    [action]=AnalyticsTagActionConstants.BUTTON_CLICK
                    [inputType]=AnalyticsInputTypes.MOUSE
                    [actionSource]="AnalyticsTagActionSourceConstants.MININP"
                    (click)="onSkipBack();
                             skipButtonService.onSkip(skipButtonService.skipType.BACKWARD);"
                    [ngClass]="{ 'disabled' : isLiveAndDisallowed }"
                    [disabled]="!skipButtonService.skipButtonsState.back || isLiveAndDisallowed"
                    [title]="'playerControls.accessibility.skipBack' | translate">
                <div class="btn-focus-container"
                     tabindex="-1">
                    <img class="skip-back-btn__img"
                         *ngIf="!isVideo; else video"
                         src="../../../../assets/images/transport-controls/skip-back.svg"
                         [alt]="'playerControls.skipBack' | translate"/>

                    <ng-template #video>
                        <img class="skip-back-btn__img"
                             src="../../../../assets/images/transport-controls/skip-back-white.svg"
                             [alt]="'playerControls.skipBack' | translate"/>
                    </ng-template>
                </div>
            </button>
        </li>`,
    styleUrls: ["../control-buttons.scss"]
})
export class SkipBackButtonComponent
{
    @Input() content: INowPlayingStore;

    /**
     * Indicates if the currently playing content is media type live and is also dmca disallowed.
     */
    @Input()
    public isLiveAndDisallowed: boolean;

    @Input() isVideo: boolean = false;

    //Analytics constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    constructor(public skipService: SkipService,
                public skipButtonService: SkipButtonService){}

    public onSkipBack(): void
    {
        let dcmaInfo:IDmcaInfoItem = {
            mediaType: this.content.mediaType,
            dmcaInfo: this.content.dmcaInfo,
            mediaId: this.content.mediaId
        };
        this.skipService.skipBack(dcmaInfo);
    }
}
