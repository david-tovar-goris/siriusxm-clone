import { ComponentFixture, TestBed } from "@angular/core/testing";
import { SxmAnalyticsService, SkipService, MediaTimestampService } from "sxmServices";
import { By } from "@angular/platform-browser";
import { SkipForwardButtonComponent } from "app/player-controls/control-buttons/skip-button/skip-forward-button.component";
import { TranslationModule } from "../../../translate/translation.module";
import { TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../../test/mocks/translate.service";
import { MockDirective } from "../../../../../test/mocks/component.mock";
import { SkipButtonService } from "./skip-button.service";
import { SkipButtonServiceMock } from "../../../../../test/mocks/skip-button.service.mock";
import { SkipServiceMock } from "../../../../../test/mocks/skip.service.mock";
import { MediaTimestampServiceMock } from "../../../../../test/mocks/media-timestamp.service.mock";

describe('Skip forward button component', () =>
{
    let fixture: ComponentFixture<SkipForwardButtonComponent>,
        component: SkipForwardButtonComponent;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function() {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            declarations: [
                SkipForwardButtonComponent,
                MockDirective({
                    selector: "sxm-new-analytics",
                    inputs: ["tagName", "screen", "userPath", "pageFrame", "elementType", "buttonName",
                        "elementPosition", "action", "inputType", "userPathScreenPosition", "modal",
                        "tagText", "actionSource"]
                })
            ],
            imports: [ TranslationModule ],
            providers: [
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: SkipButtonService, useValue: SkipButtonServiceMock },
                { provide: SkipService, useValue: SkipServiceMock },
                { provide: MediaTimestampService, useClass: MediaTimestampServiceMock }
            ]
        });
        fixture = TestBed.createComponent(SkipForwardButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    xdescribe('skip forward button component analytics', () =>
    {
        it("logs the skip forward event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.skip-forward-btn'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "MnpFwdSkip"
                    }
                });
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
