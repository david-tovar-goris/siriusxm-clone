import { ComponentFixture, TestBed } from "@angular/core/testing";
import { SxmAnalyticsService } from "sxmServices";
import { By } from "@angular/platform-browser";
import { TranslationModule } from "../../../translate/translation.module";
import { TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../../test/mocks/translate.service";
import { SkipBackButtonComponent } from "app/player-controls/control-buttons/skip-button/skip-back-button.component";
import { MockDirective } from "../../../../../test/mocks/component.mock";
import { SkipButtonService } from "./skip-button.service";
import { Subject } from "rxjs";

describe('Skip back button component', () =>
{
    let fixture: ComponentFixture<SkipBackButtonComponent>,
        component: SkipBackButtonComponent;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function() {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    let skipButtonService = {
        displaySkipMessage$: new Subject()
    };

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            declarations: [
                SkipBackButtonComponent,
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType",
                        "userPathScreenPosition", "modal", "tagText", "actionSource"]})
            ],
            imports:[ TranslationModule ],
            providers: [
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: SkipButtonService, useValue: skipButtonService }
            ]
        });
        fixture = TestBed.createComponent(SkipBackButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    /*
        Fix this
     */
    xdescribe('skip back button component analytics', () =>
    {
        it("logs the skip back event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.skip-back-btn'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "MnpBackSkip"
                    }
                });
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
