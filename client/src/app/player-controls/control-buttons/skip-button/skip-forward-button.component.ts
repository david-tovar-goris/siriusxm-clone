import { Component, Input } from "@angular/core";
import { SkipButtonService } from "./skip-button.service";
import { INowPlayingStore } from "../../../common/store";
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import { SkipService, MediaUtil, MediaTimestampService, IDmcaInfoItem } from "sxmServices";

/**
 * Constant used to disable forward skip for what's considered "live" so a user can't spam forward skip
 * @type {number}
 */
const LIVE_POINT_FACTOR: number = 5;
@Component({
    selector: "skip-forward-button",
    template: `
        <li class="center-buttons-list__item">
            <button class="skip-forward-btn"
                    sxm-new-analytics
                    [tagName]="AnalyticsTagNameConstants.MNP_FORWARD_SKIP"
                    [userPath]="AnalyticsUserPaths.MNP_FORWARD_SKIP"
                    [userPathScreenPosition]="3"
                    [pageFrame]="AnalyticsPageFrames.MODAL"
                    [modal]="AnalyticsModals.MINI_NOW_PLAYING_BAR"
                    [elementType]="AnalyticsElementTypes.BUTTON"
                    [buttonName]="AnalyticsTagNameConstants.MNP_FORWARD_SKIP_BUTTON_NAME"
                    [elementPosition]="6"
                    [action]=AnalyticsTagActionConstants.BUTTON_CLICK
                    [inputType]=AnalyticsInputTypes.MOUSE
                    [actionSource]="AnalyticsTagActionSourceConstants.MININP"
                    (click)="onSkipForward();
                             skipButtonService.onSkip(skipButtonService.skipType.FORWARD);"
                    [ngClass]="{ 'disabled' : isLiveAndDisallowed }"
                    [disabled]="!skipButtonService.skipButtonsState.forward || isAtLivePoint() || isLiveAndDisallowed"
                    [title]="'playerControls.accessibility.skipForward' | translate">
                <div class="btn-focus-container"
                     tabindex="-1">
                    <img class="skip-forward-btn__img"
                         *ngIf="!isVideo; else video"
                         src="../../../../assets/images/transport-controls/skip-forward.svg"
                         [alt]="'playerControls.skipForward' | translate"/>

                    <ng-template #video>
                        <img class="skip-forward-btn__img"
                             src="../../../../assets/images/transport-controls/skip-forward-white.svg"
                             [alt]="'playerControls.skipForward' | translate"/>
                    </ng-template>
                </div>
            </button>
        </li>`,
    styleUrls: ['../control-buttons.scss']
})

export class SkipForwardButtonComponent
{
    @Input() content: INowPlayingStore;
    @Input() isVideo: boolean = false;

    /**
     * Indicates if the currently playing content is media type live and is also dmca disallowed.
     */
    @Input()
    public isLiveAndDisallowed: boolean;

    //Analytics constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    constructor(public skipService: SkipService,
                public skipButtonService: SkipButtonService,
                private mediaTimestampService: MediaTimestampService){}

    public isAtLivePoint(): boolean
    {
        if(!MediaUtil.isAudioMediaTypeLive(this.content.mediaType)) { return false; }
        return this.mediaTimestampService.livePointTimestamp - this.mediaTimestampService.playheadTimestamp <= LIVE_POINT_FACTOR;
    }

    public onSkipForward(): void
    {
        let dcmaInfo:IDmcaInfoItem = {
            mediaType: this.content.mediaType,
            dmcaInfo: this.content.dmcaInfo,
            mediaId: this.content.mediaId
        };
        this.skipService.skipForward(dcmaInfo);
    }
}
