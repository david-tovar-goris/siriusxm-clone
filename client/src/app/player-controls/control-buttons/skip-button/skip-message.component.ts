import {
    Component
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import * as moment from "moment";

import { SkipButtonService } from "./skip-button.service";

import { Logger } from "sxmServices";
import { ChromecastConst } from '../../../../../../servicelib/src/chromecast/chromecast.const';
import { ChromecastPlayerService } from "../../../../../../servicelib/src/mediaplayer/chromecastplayer/chromecast-player.service";
import { SkipInfo } from "sxmServices";
import { SubscriptionLike as ISubscription } from "rxjs";
import { filter } from "rxjs/operators";

interface MessageData {
    skipsLeft: number;
    skipTimestamp: any;
}

@Component({
    selector: "skip-message",
    template: `<div [ngClass]="isChromeCasting() ? 'casting-skip-message casting-skip-animation' : this.skipData.skipsLeft <= 0
                    ? 'skip-message skip-limit-reached skip-animation' : 'skip-message skip-animation'"
                    *ngIf="displayMessage"
                    aria-live="assertive">
                    <p class="skip-message__text">{{ getSkipMessageText() }}</p>
                </div>`,
    styleUrls: [ "./skip-message.component.scss" ]
})
export class SkipMessageComponent
{

    private static SKIP_MESSAGE_THRESHOLD = 2;

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("SkipMessageComponent");

    private skipData: MessageData = { skipsLeft: 99, skipTimestamp: '' };
    private displayMessage: boolean = false;

    private skipLimitText = this.translate.instant('playerControls.skipLimit');
    private skipsLeftSingularText = this.translate.instant('playerControls.skipsLeftSingular');
    private skipsLeftPluralText = this.translate.instant('playerControls.skipsLeftPlural');

    constructor(public skipButtonService: SkipButtonService,
                private translate: TranslateService,
                private chromecastPlayerService: ChromecastPlayerService)
    {
        this.skipButtonService.displaySkipMessage$
            .subscribe((data: any) =>
        {
            this.skipData = this.getMessageData(data.skipInfo);

            if (this.skipData.skipsLeft <= SkipMessageComponent.SKIP_MESSAGE_THRESHOLD)
            {
                this.displayMessage = data.isVisible;
            }
        });
    }


    public getSkipMessageText(): string
    {
        SkipMessageComponent.logger.debug(`getSkipMessageText()`);

        const { skipsLeft, skipTimestamp } = this.skipData,
              formattedTime = skipTimestamp.format('LT');

        let messageText: string = "";

        if (skipsLeft > 1)
        {
            // 2+ skips left
            messageText = `${skipsLeft} ${this.skipsLeftPluralText}`;
        }
        else if (skipsLeft === 1)
        {
            // 1 skip left
            messageText = `${skipsLeft} ${this.skipsLeftSingularText}`;
        }
        else
        {
            // Skip limit reached until...
            messageText = `${this.skipLimitText} ${formattedTime}`;
        }

        return messageText;
    }


    private getMessageData(skipInfo: SkipInfo): MessageData
    {
        const { numSkipsForward, numSkipsBack, maxTotalSkips, skipTimestamp } = skipInfo;

        return {
            skipTimestamp: moment(skipTimestamp).add(1, 'h'),
            skipsLeft: maxTotalSkips - (numSkipsForward + numSkipsBack)
        };
    }

    private isChromeCasting(): boolean
    {
        return this.chromecastPlayerService.switchPlayerType$.getValue() === ChromecastConst.CHROMECAST;
    }
}
