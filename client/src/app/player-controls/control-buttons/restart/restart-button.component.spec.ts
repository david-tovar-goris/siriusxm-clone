import { ComponentFixture, TestBed } from "@angular/core/testing";
import { MediaPlayerService, SxmAnalyticsService } from "sxmServices";
import { TranslationModule } from "../../../translate/translation.module";
import { MockDirective } from "../../../../../test/mocks/component.mock";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../../test/mocks/router.mock";
import { SharedModule } from "../../../common/shared.module";
import { ChannelListStoreService } from "app/common/service/channel-list.store.service";
import { ChannelListServiceMock } from "../../../../../test/mocks/channel-list.service.mock";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { By } from "@angular/platform-browser";
import { RestartButtonComponent } from "app/player-controls/control-buttons/restart/restart-button.component";

describe('Restart show', () =>
{
    let mediaPlayerService,
        fixture: ComponentFixture<RestartButtonComponent>,
        component: RestartButtonComponent;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(() =>
    {
        mediaPlayerService = {
            mediaPlayer: {
                togglePausePlay: function()
                {
                }
            },
            isPlaying: function()
            {
                return true;
            }
        };

        this.store = {
            select: () => new BehaviorSubject(null)
        };

        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SharedModule
            ],
            declarations: [
                RestartButtonComponent,
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType",
                        "userPathScreenPosition", "modal", "tagText", "actionSource"]})
            ],
            providers: [
                { provide: MediaPlayerService, useValue: mediaPlayerService },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: Store, useValue: this.store},
                { provide: Router, useClass: RouterStub},
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock }
            ]
        });
        mediaPlayerService = TestBed.get(MediaPlayerService);
        fixture = TestBed.createComponent(RestartButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('restart show button analytics', () =>
    {
        it("logs the restart show event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.restart-btn'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "MnpRestart"
                    }
                });
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
