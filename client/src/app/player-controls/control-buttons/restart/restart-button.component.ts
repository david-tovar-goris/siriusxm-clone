import { INowPlayingStore } from '../../../common/store';

import {
    Component,
    EventEmitter,
    Input,
    Output
} from "@angular/core";
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes, AnalyticsInputTypes,
    AnalyticsModals, AnalyticsPageFrames,
    AnalyticsScreens, AnalyticsTagActionConstants, AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
@Component({
    selector: "restart-btn",
    template: `<button class="restart-btn"
                       sxm-new-analytics
                       [tagName]="AnalyticsTagNameConstants.MNP_RESTART"
                       [userPath]="AnalyticsUserPaths.MNP_RESTART"
                       [userPathScreenPosition]="3"
                       [modal]="AnalyticsModals.MINI_NOW_PLAYING_BAR"
                       [pageFrame]="AnalyticsPageFrames.MODAL"
                       [elementType]="AnalyticsElementTypes.BUTTON"
                       [buttonName]="AnalyticsTagNameConstants.MNP_RESTART_BUTTON_NAME"
                       [elementPosition]="2"
                       [action]=AnalyticsTagActionConstants.BUTTON_CLICK
                       [actionSource]="AnalyticsTagActionSourceConstants.MININP"
                       [inputType]=AnalyticsInputTypes.MOUSE
                       (click)="onRestartClick()"
                       [ngClass]="{ 'visibility-hidden': isVisibilityHidden, 'french-btn': isFrench }"
                       [attr.aria-label]="'nowPlaying.restart.ariaLabel' | translate">
                   <span class="btn-focus-container" tabindex="-1">
                       {{ 'nowPlaying.restart.line1' | translate }} {{ 'nowPlaying.restart.line2' | translate }}
                   </span>
               </button>`,
    styleUrls: [ "../control-buttons.scss" ]
})
export class RestartButtonComponent
{
    /**
     * Broadcast when the user clicks the restart button.
     */
    @Output()
    public restart = new EventEmitter<any>();

    /**
     * Indicates if the currently playing content is media type live and is also dmca disallowed.
     */
    @Input()
    public isVisibilityHidden: boolean;

    /**
     * Indicates if the current language is French
     */
    @Input()
    public isFrench: boolean;

    /**
     * The current Now Playing Store Data.
     */
    @Input()
    public content: INowPlayingStore;

    /**
     * Indicates if the currently playing media can go to live.
     */
    @Input()
    public canGoToLive: boolean;

    //Anaalytics Constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    /**
     * Constructor.
     */
    constructor(){}

    /**
     * Broadcasts the event to restart the current media playback.
     */
    public onRestartClick()
    {
        this.restart.emit();
    }
}


