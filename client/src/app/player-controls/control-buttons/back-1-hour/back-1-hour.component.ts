import {
    Component,
    Input
} from "@angular/core";

import { MediaTimestampService, SeekService } from "sxmServices";
import { Skip15SecondsService } from "../skip-15-seconds/skip-15-seconds.service";
import { INowPlayingStore } from "../../../common/store";
import { Skip15ServiceConsts } from "../skip-15-seconds/skip-15-service.consts";

@Component({
    selector: "back-1-hour",
    template: `<button class="back-1-hr-btn"
                       (click)="onClickBack1Hour()"
                       [disabled]="disableGoBack1hr()"
                       [ngClass]="{ 'french-btn': isFrench }"
                       [attr.aria-label]="'nowPlaying.back1hour.ariaLabel' | translate">
                   <span class="btn-focus-container" tabindex="-1">
                       {{ 'nowPlaying.back1hour.line1' | translate }}
                   </span>
               </button>`,
    styleUrls: [ "../control-buttons.scss" ]
})
export class Back1HourComponent
{
    /**
     * The current Now Playing Store Data.
     */
    @Input()
    public content: INowPlayingStore;
    /**
     * Indicates if the currently playing content is media type live and is also dmca disallowed.
     */
    @Input()
    public isLiveAndDisallowed: boolean;

    /**
     * Indicates if the current language is French
     */
    @Input()
    public isFrench: boolean;

    /**
     * Constructor.
     */
    constructor(public skip15Service: Skip15SecondsService,
                private mediaTimestampService: MediaTimestampService,
                private seekService: SeekService) {}

    /**
     * Seeks 1 hour back from the current playheadTimeStamp
     */
    public onClickBack1Hour(): void
    {
        const seconds = this.mediaTimestampService.playheadTimestamp - Skip15ServiceConsts.ONE_HOUR_S;
        this.seekService.seekThenPlay(seconds);
        this.disableGoBack1hr();
    }

    /**
     * returns a flag to hide/show back1hr button
     * @returns {boolean}
     */
    public disableGoBack1hr(): boolean
    {
        return this.skip15Service.disableBack1Hour();
    }
}
