import { ComponentFixture, TestBed } from "@angular/core/testing";
import { MediaTimestampServiceMock } from "../../../../../test/mocks/media-timestamp.service.mock";
import { SeekServiceMock } from "../../../../../test/mocks/seek.service.mock";
import {MediaTimestampService, SxmAnalyticsService} from "sxmServices";
import { SeekService } from "sxmServices";
import { Skip15SecondsServiceMock } from "../../../../../test/mocks/skip-15-seconds.service.mock";
import { Skip15SecondsService } from "../skip-15-seconds/skip-15-seconds.service";
import { Back1HourComponent } from "./back-1-hour.component";
import { TranslateModule, TranslateService} from "@ngx-translate/core";
import { MockTranslateService } from "../../../../../test/mocks/translate.service";
import {By} from "@angular/platform-browser";

describe('Back1HourComponent', () =>
{
    let fixture: ComponentFixture<Back1HourComponent>,
        component: Back1HourComponent,
        mediaTimestampService: MediaTimestampService,
        skip15SecondsService : Skip15SecondsService,
        seekService: SeekService;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            declarations: [
                Back1HourComponent
            ],
            imports:[
                TranslateModule
            ],
            providers: [
                { provide: SeekService, useClass: SeekServiceMock },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: MediaTimestampService, useClass: MediaTimestampServiceMock },
                { provide: Skip15SecondsService, useClass: Skip15SecondsServiceMock },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock }
            ]
        });
        seekService = TestBed.get(SeekService);
        mediaTimestampService = TestBed.get(MediaTimestampService);
        skip15SecondsService = TestBed.get(Skip15SecondsService);
        fixture = TestBed.createComponent(Back1HourComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('onClickBack1Hour()', () =>
    {
        describe('when playheadTimestamp is greater than 1 hour (3600 seconds)', () =>
        {
            it('seeks to the playheadTimestamp - 3600 seconds', () =>
            {
                spyOn(seekService, 'seekThenPlay');
                component.onClickBack1Hour();
                let seekTime = mediaTimestampService.playheadTimestamp - 3600;
                expect(seekService.seekThenPlay).toHaveBeenCalledWith(seekTime);
            });
        });
    });

    describe('disableGoBack1hr()', () =>
    {
        it('returns a boolean flag', () =>
        {
            component.disableGoBack1hr();
            expect(skip15SecondsService.disableBack1Hour).toHaveBeenCalled();
        });
    });

    /*
        This test is disabled because analytics not yet implemented on back one hour button.
        This can be turned on when analytics are implemented.
     */
    xdescribe('back one hour analytics', () =>
    {
        it("logs the go to back one hour event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.back-1-hr-btn'))
                .triggerEventHandler('click', event);
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
