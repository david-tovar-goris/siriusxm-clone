import { of as observableOf,  Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter
} from "@angular/core";
import { getContentType } from "../../../common/store/now-playing.store";
import { INowPlayingStore } from "../../../common/store";
import { IAppStore } from "../../../common/store";
import { Store } from "@ngrx/store";
import {
    ContentTypes,
    AffinityConstants,
    AffinityService,
    AffinityType,
    CurrentlyPlayingService,
    ICurrentlyPlayingMedia,
    DmcaService,
    MediaTimestampService,
    SkipService
} from "sxmServices";
import * as _ from "lodash";
import { TranslateService } from '@ngx-translate/core';
import { SkipButtonService } from "../skip-button/skip-button.service";
import { IDmcaInfoItem } from "sxmServices";
import {
    ArtistRadioAnalyticsTags,
    AnalyticsTagActionConstants,
    AnalyticsPageFrames,
    AnalyticsElementTypes,
    AnalyticsScreens
} from "../../../analytics/sxm-analytics-tag.constants";

@Component({
    selector: "affinity-button",
    changeDetection : ChangeDetectionStrategy.OnPush,
    templateUrl : "./affinity-button.component.html",
    styleUrls: ["../control-buttons.scss"]
})
export class AffinityButtonComponent
{
    @Output()
    affinityClick: EventEmitter<String> = new EventEmitter<String>();

    /**
     * Fires thumbs up / thumbs down event up to the player-controls.component
     * @param message: This is the message to pass to the toast.service.
     */
    private broadcastAffinityEvent(message: string): void
    {
        this.affinityClick.emit(message);
    }

    /**
     * Observable string indicating the media type.
     */
    public contentType$: Observable<string> = this.store.select(getContentType);

    /**
     * holds appropriate url for the image shown as thumbs up/down
     */
    public imageUrl$: Observable<string>;

    /**
     * controls whether thumbs up/down button is high lighted
     */
    public isHighlighted:boolean;

    /**
     * button class name thumbs-up-btn/thumbs-down-btn
     */
    public buttonClass$: Observable<string>;

    /**
     * determine if thumbs up/down button is shown or hidden
     */
    public showButton$: Observable<boolean> = observableOf(false);

    /**
     * current affinity value [LIKE | DISLIKE | NEUTRAL]
     */
    private affinityValue : AffinityType;

    public affinityButtonTitle: string;


    private currentlyPlayingMedia: ICurrentlyPlayingMedia;

    /**
     * affinity type for the button [LIKE | DISLIKE]
     */
    private _affinityType: string;
    @Input() set affinityType (buttonType: string)
    {
        this._affinityType = buttonType;
        this.buttonClass$ = buttonType === AffinityConstants.LIKE ? observableOf("thumbs-up-btn") : observableOf("thumbs-down-btn");

        this.affinityButtonTitle = buttonType === AffinityConstants.LIKE
            ? this.translator.instant("pandoraX.coachMarkLikeToolTip")
            : this.translator.instant("pandoraX.coachMarkDislikeToolTip");

        this.updateImageUrl();
    }

    get affinityType(): string
    {
        return this._affinityType;
    }

    @Input() content: INowPlayingStore;

    constructor(public affinityService: AffinityService,
                private currentlyPlayingService: CurrentlyPlayingService,
                private skipService: SkipService,
                private skipButtonService: SkipButtonService,
                private dmcaService: DmcaService,
                private store: Store<IAppStore>,
                private translator: TranslateService,
                private mediaTimestampService: MediaTimestampService)
    {
        this.contentType$.subscribe(contentType =>
        {
            this.showButton$ = observableOf(contentType === ContentTypes.SEEDED_RADIO);
        });

        this.currentlyPlayingService.currentlyPlayingData.pipe(
            filter(data => !!data))
            .subscribe((currentlyPlayingMedia: ICurrentlyPlayingMedia) =>
        {
            this.affinityValue  = _.get(currentlyPlayingMedia, "cut.affinity", AffinityConstants.NEUTRAL) as AffinityType;
            this.updateImageUrl();
            this.currentlyPlayingMedia = currentlyPlayingMedia;
        });
    }

    public ArtistRadioAnalyticsTags = ArtistRadioAnalyticsTags;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsScreens = AnalyticsScreens;

    /**
     * update button image based on current affinity (post update, if any)
     *
     */
    public updateImageUrl() : void
    {
        let theImageUrl = "";
        this.isHighlighted = this.affinityValue === this.affinityType;

        if (this.affinityType === AffinityConstants.LIKE)
        {
            theImageUrl = this.isHighlighted ?
                "../../../../assets/images/thumbs_up_highlighted.svg" : "../../../../assets/images/thumbs_up.svg";
        }
        else if (this.affinityType === AffinityConstants.DISLIKE)
        {
            theImageUrl= this.isHighlighted ?
                "../../../../assets/images/thumbs_down_highlighted.svg" : "../../../../assets/images/thumbs_down.svg";
        }
        this.imageUrl$ = observableOf(theImageUrl);
    }

    /**
     * toggles affinity of the current playing track
     *
     */
    public toggleAffinity(): void
    {
        const newAffinity: string = this.isHighlighted ? AffinityConstants.NEUTRAL : this.affinityType;
        const elapsedTime : number = this.mediaTimestampService.playheadTimestamp;

        let message: any;

        message = newAffinity === AffinityConstants.LIKE
            ? this.translator.instant("pandoraX.coachMarkLikedItCopy")
            : this.translator.instant("pandoraX.coachMarkDisikedItCopy");

        this.broadcastAffinityEvent(message);
        this.updateAffinity(newAffinity as AffinityType, elapsedTime);
    }

    /**
     * makes call to affinity service to update the thumbs up/down feedback
     *
     * @returns boolean
     */
    public updateAffinity(newAffinity: AffinityType, elapsedTime? : number): void
    {
        this.affinityService.updateAffinity(newAffinity, elapsedTime).subscribe(resp =>
        {
            this.affinityValue = newAffinity;
            this.updateImageUrl();
            if (newAffinity === AffinityConstants.DISLIKE)
            {
                let stationFactory = this.currentlyPlayingMedia.channel
                && this.currentlyPlayingMedia.channel.stationFactory;

                stationFactory = stationFactory || this.currentlyPlayingMedia.mediaId;

                const item: IDmcaInfoItem = { mediaId: stationFactory } as IDmcaInfoItem;
                if (!this.dmcaService.hasSkipsRemaining(item))
                {
                    return;
                }

                setTimeout(() =>
                {
                    this.skipService.skipForward({
                        mediaType: this.content.mediaType,
                        dmcaInfo: this.content.dmcaInfo,
                        mediaId: this.content.mediaId
                    });
                    this.skipButtonService.onSkip(this.skipButtonService.skipType.FORWARD);

                }, 1250);  // this delay is hardcoded
                           // to allow the affinity call to return
                           // and highlight the button before skipping.
                           // demoed this to Slava and he wants it for now.
            }
        });
    }
}
