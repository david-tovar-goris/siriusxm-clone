import { ComponentFixture, TestBed } from "@angular/core/testing";
import { PlayButtonComponent } from "./play-button.component";
import { MediaPlayerService, SxmAnalyticsService } from "sxmServices";
import { TranslationModule } from "../../../translate/translation.module";
import { MockDirective } from "../../../../../test/mocks/component.mock";
import { of } from "rxjs";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../../test/mocks/router.mock";
import { SharedModule } from "../../../common/shared.module";
import { ChannelListStoreService } from "app/common/service/channel-list.store.service";
import { ChannelListServiceMock } from "../../../../../test/mocks/channel-list.service.mock";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import {By} from "@angular/platform-browser";

describe('PlayButtonComponent', () =>
{
    let mediaPlayerService,
        fixture: ComponentFixture<PlayButtonComponent>,
        component: PlayButtonComponent;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(() =>
    {
        mediaPlayerService = {
            mediaPlayer: {
                togglePausePlay: function()
                {

                }
            },
            isPlaying: function()
            {
                return true;
            }
        };

        this.store = {
            select: () => new BehaviorSubject(null)
        };

        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SharedModule
            ],
            declarations: [
                PlayButtonComponent,
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType",
                        "userPathScreenPosition", "modal", "tagText", "actionSource"]})
            ],
            providers: [
                { provide: MediaPlayerService, useValue: mediaPlayerService },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: Store, useValue: this.store},
                { provide: Router, useClass: RouterStub},
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock }
            ]
        });

        mediaPlayerService = TestBed.get(MediaPlayerService);
        fixture = TestBed.createComponent(PlayButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('onClickPausePlay()', () =>
    {
        it('toggles the mediaPlayer to either play or pause', () =>
        {
            spyOn(mediaPlayerService.mediaPlayer, 'togglePausePlay')
                .and.returnValue(of('playing'));
            component.onClickPausePlay();
            expect(mediaPlayerService.mediaPlayer.togglePausePlay).toHaveBeenCalled();
        });
    });

    describe('play button analytics', () =>
    {
        it("logs the play event", () =>
        {
            spyOn(mediaPlayerService.mediaPlayer, 'togglePausePlay').and.returnValue(of('playing'));
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.play-pause-btn'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "MnpPlayPause"
                    }
                });
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
