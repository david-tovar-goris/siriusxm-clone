import { Component, Input } from "@angular/core";
import { MediaPlayerService } from "sxmServices";
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes, AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens, AnalyticsTagActionConstants, AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants, AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "play-button",
    template: `
        <li class="center-buttons-list__item">
            <button class="play-pause-btn"
                    [title]="((mediaPlayerService.isContentPlaying | async)
                                ? 'playerControls.accessibility.pause'
                                : 'playerControls.accessibility.play') | translate"
                    sxm-new-analytics
                    [tagName]="AnalyticsTagNameConstants.MNP_PLAY_PAUSE"
                    [userPath]="(mediaPlayerService.isContentPlaying | async) ?
                                    AnalyticsUserPaths.MNP_PAUSE : AnalyticsUserPaths.MNP_PLAY"
                    [userPathScreenPosition]="3"
                    [pageFrame]="AnalyticsPageFrames.MODAL"
                    [modal]="AnalyticsModals.MINI_NOW_PLAYING_BAR"
                    [elementType]="AnalyticsElementTypes.BUTTON"
                    [buttonName]="AnalyticsTagNameConstants.MNP_PLAY_PAUSE_BUTTON_NAME"
                    [tagText]="(mediaPlayerService.isContentPlaying | async) ?
                       AnalyticsTagTextConstants.NP_PAUSE : AnalyticsTagTextConstants.NP_PLAY"
                    [elementPosition]="5"
                    [action]=AnalyticsTagActionConstants.BUTTON_CLICK
                    [inputType]=AnalyticsInputTypes.MOUSE
                    [actionSource]="AnalyticsTagActionSourceConstants.MININP"
                    (click)="onClickPausePlay()">
                <div class="btn-focus-container"
                     tabindex="-1">
                    <img class="play-pause-btn__img"
                         *ngIf="!isVideo; else video"
                         [src]="(mediaPlayerService.isContentPlaying | async)
                                    ? '../../../../assets/images/transport-controls/pause.svg'
                                    : '../../../../assets/images/transport-controls/play.svg'"
                         [alt]="((mediaPlayerService.isContentPlaying | async)
                                    ? 'playerControls.pause'
                                    : 'playerControls.play') | translate"/>

                    <ng-template #video>
                        <img class="play-pause-btn__img"
                             [src]="(mediaPlayerService.isContentPlaying | async)
                                    ? '../../../../assets/images/transport-controls/pause-white.svg'
                                    : '../../../../assets/images/transport-controls/play-white.svg'"
                             [alt]="((mediaPlayerService.isContentPlaying | async)
                                    ? 'playerControls.pause'
                                    : 'playerControls.play') | translate"/>
                    </ng-template>
                </div>
            </button>
        </li>`,
    styleUrls: ["../control-buttons.scss"]
})
export class PlayButtonComponent
{
    @Input() content: {};
    @Input() isVideo: boolean = false;

    //Analytics constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    constructor(public mediaPlayerService: MediaPlayerService)
    {
    }

    public onClickPausePlay(): void
    {
        this.mediaPlayerService.mediaPlayer.togglePausePlay().subscribe();
    }
}
