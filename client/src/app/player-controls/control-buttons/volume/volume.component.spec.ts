import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolumeComponent } from './volume.component';
import { TranslationModule } from "../../../translate/translation.module";
import {SxmAnalyticsService, VolumeService} from "sxmServices";
import { MockComponent } from "../../../../../test/mocks/component.mock";
import {By} from "@angular/platform-browser";

describe('VolumeComponent', () =>
{
    let component: VolumeComponent;
    let fixture: ComponentFixture<VolumeComponent>;
    const volumeServiceMock =
        {
            getVolume        : function ()
            {
                return 10;
            },
            initializeVolume : function () {},
            getVolumeImageSrc: function ()
            {
                return "testUrl";
            }
        };

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function() {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule(
            {
                imports     : [ TranslationModule ],
                declarations: [ VolumeComponent ],
                providers   : [
                    { provide: VolumeService, useValue: volumeServiceMock },
                    { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock }
                ]
            })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture   = TestBed.createComponent(VolumeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () =>
    {
        expect(component).toBeTruthy();
    });

    /*
        These tests are disabled because analytics has not yet been implemented on the volume component.  They can
        be enabled once tagging work has been done for volume component.
     */
    xdescribe('volume button analytics', () =>
    {
        let event = {
            target: {
                tagName: "test"
            }
        };

        it("logs the volume button event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.volume-button'))
                .triggerEventHandler('click', event);
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });

        it("logs the volume bar event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.volume-bar'))
                .triggerEventHandler('change', event);
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });

    });
});
