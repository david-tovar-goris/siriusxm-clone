import {
    Logger,
    MediaPlayerConstants,
    VolumeService
} from "sxmServices";
import {
    Component,
    ElementRef,
    HostListener,
    Input,
    OnInit
} from "@angular/core";
import { BrowserUtil } from "../../../common/util/browser.util";
import * as screenfull from 'screenfull';
import { INowPlayingStore } from '../../../common/store';

@Component({
    selector   : 'sxm-volume',
    templateUrl: './volume.component.html',
    styleUrls  : [ './volume.component.scss' ]
})
export class VolumeComponent implements OnInit
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("VolumeComponent");

    /**
     * Constant for controlling the duration of the mute/unmute animation in MS
     * @type {number}
     */
    private static MUTE_DURATION_MS = 150;

    /**
     * Constant for the number of animation steps for mute/unmute
     * @type {number}
     */
    private static MUTE_STEPS = 12.5;

    /**
     * Constant for the number of ms between mute animation steps
     * @type {number}
     */
    private static MUTE_INTERVAL_TIME_MS = VolumeComponent.MUTE_DURATION_MS / VolumeComponent.MUTE_STEPS;

    /**
     * Array for precalculating the exponential portion of the easing equation for the mute on/off animation
     * @type {any[]}
     */
    private static EASING : Array<number> = [];

    /**
     * current volume of the player
     * @type {number}
     */
    public volume: number = 0;

    /**
     * current volume of the player
     * kept around for screen reader announcing purposes.
     * @type {number}
     */
    public statusVolume: number = 0;

    /**
     * Flag indicates if the volume control bar is shown or hidden.
     * @type {boolean}
     */
    public volumeControlIsShown: boolean = false;

    public screenfull = screenfull;
    public isFullscreen: boolean = false;

    /**
     * Image URL is used to load volume icon
     * Provide a default that can be overridden
     * @type {string}
     */
    public volumeImageUrl: string = "../../assets/images/volume/volume-4-blue";


    /**
     * holds the volume bar timer . can be used to clear and restart timer.
     */
    private closeVolumeBarTimer: any;

    /**
     * The current show.
     */
    @Input()
    public content: INowPlayingStore;

    /**
     * Constructor
     * @param {VolumeService} volumeService
     * @param {ElementRef} eRef
     */
    constructor(private volumeService: VolumeService,
                private eRef: ElementRef)
    {
        this.volumeService.initializeVolume(this);

        this.isFullscreen = (this.screenfull.isFullscreen && this.screenfull.isFullscreen === true) ? true : false;
        this.volumeImageUrl = this.volumeService.getVolumeImageSrc(this.volume, this.isFullscreen);

        /**
         * http://www.gizma.com/easing/
         *
         * The exponential easing function (accelerating from zero velocity) we use is as follows.
         *
         * t = time in milliseconds
         * b = start value
         * c = change in value (average)
         * d = duration in milliseconds
         *
         * Math.easeInExpo = function (t, b, c, d) {
         *    return c * Math.pow( 2, 10 * (t/d - 1) ) + b;
         * };
         *
         * The Math.pow portion of this calculation in fixed (not dependent on b or c).  So we can precompute this
         * into an array and then use a "step" value to index into the array.  This means that the computation during
         * the animation is less CPU intensive.
         */

        if (VolumeComponent.EASING.length === 0)
        {
            for (let i = 0; i < VolumeComponent.MUTE_STEPS; i++)
            {
                const exponent = 10 * (i * VolumeComponent.MUTE_INTERVAL_TIME_MS) / VolumeComponent.MUTE_DURATION_MS - 1;
                VolumeComponent.EASING.push(Math.pow(2, exponent));
            }
        }

        this.observeVolumeChangedFromRemotePlayer();
    }

    /**
     * This below method is more applicable to Chromecast . When R changes volume then we update on Sender.
     */
    public observeVolumeChangedFromRemotePlayer(): void
    {
        if(!this.volumeService || !this.volumeService.volumeChanged$)
        {
            return;
        }

        this.volumeService.volumeChanged$
            .subscribe((data) =>
            {
                // Make sure we have proper volume before the volume Data
                if (data && typeof data.volume === "number")
                {
                    let volumeLevel = data.volume;
                    let isMuted     = data.muted;
                    if (isMuted)
                    {
                        this.setVolumeData(0, true, false);
                    }
                    else
                    {
                        if (volumeLevel !== this.volumeService.pullNonZeroVolumeFromStorage())
                        {
                            this.setVolumeData(volumeLevel * MediaPlayerConstants.MAX_VOLUME_VALUE, true, false);
                        }
                        else
                        {
                            this.setVolumeData(this.volumeService.pullNonZeroVolumeFromStorage(), true, false);
                        }
                    }
                }
            });
    }

    /**
     * Used this method to handle the click outside of volume bar. If the click happens out side of volume bar then
     * volume bar should be hidden.
     * @param event
     */
    @HostListener('document:click', [ '$event' ])
    clickOutside(event)
    {
        if (!this.eRef.nativeElement.contains(event.target))
        {
            this.hideVolumeBar();
        }
    }

    /**
     * ngOnInit()
     */
    public ngOnInit()
    {
        // TODO: Needs to check why ngOnInit is not triggered on fullscreen toggle.
    }

    /**
     * Handles button click for "Volume"
     */
    public onClickVolumeButton(): void
    {
        this.showVolumeBar();
    }

    /**
     * Handles Mute button click
     */
    public onClickMuteButton(): void
    {
        const toMute          = this.volume !== 0;
        const startVolume     = this.volume;
        const endVolume       = (toMute) ? 0 : this.volumeService.pullNonZeroVolumeFromStorage();
        const volumeIncrement = (endVolume - startVolume) / VolumeComponent.MUTE_STEPS;
        let step              = 0;
        // Set an interval timer to handle the mute on/off animation.  Once we hit the target volume the timer
        // will be cleared
        const interval = setInterval(() =>
        {
            let nextVolume      = easeIn(step++, startVolume, volumeIncrement);
            const animationOver = ((nextVolume <= endVolume) && toMute) || ((nextVolume >= endVolume) && !toMute);

            if (animationOver)
            {
                clearInterval(interval);
                // Any change in app's volume/mute/unmute should be in sync with casting device
                VolumeComponent.logger.debug(`onClickMuteButton ` + endVolume);

                return this.setVolumeData(endVolume, true);
            }

            this.setVolumeData(nextVolume, false);
        }, VolumeComponent.MUTE_INTERVAL_TIME_MS);

        /**
         * Compute the next volume based on the exponential ease in
         *
         * @param t is the step of the animation
         * @param b is the value we started at
         * @param c os the average change in value per step of the animation
         * @returns {any}
         */
        function easeIn(t,b,c)
        {
            return c * VolumeComponent.EASING[t] + b;
        }
    }

    /**
     * Handles input volume slider changes
     * @param event
     */
    public volumeHandler(event: any)
    {
        this.clearVolumeBarTimer();
        const volume: number = parseInt(event.target.value);
        this.setVolumeData(volume, true);
    }

    /**
     * Used set the volume bar for progress color.
     * @returns {any}
     */
    public applyVolumeStyles(): any
    {

        if (BrowserUtil.isEdge() || BrowserUtil.isInternetExplorer())
        {
            //ie and edge require a height that can contain the largest part of the range
            //and don't need the gradient
            //needs to be so large to account for ball + shadow and ball +shadow with hover scale
            return { "height": "25px" };
        }
        else
        {
            return {
                "background-image": `-webkit-gradient(linear, left top, right top,
                  color-stop(${this.volume / MediaPlayerConstants.MAX_VOLUME_VALUE}, #006ed7),
                  color-stop(${this.volume / MediaPlayerConstants.MAX_VOLUME_VALUE}, #202d3f))`
            };
        }
    }

    /**
     *  Makes this component a VolumeSetable. See VolumeService#initialize
     */
    public setVolume(volume: number)
    {
        this.volume = volume;
    }


    /**
     *  Method for setting the static volume
     */
    public setStatusVolume(volume: number)
    {
        this.statusVolume = volume;
    }


    /**
     * Clears the volume bar timer. It allows to reset timer later.
     */
    private clearVolumeBarTimer()
    {
        clearTimeout(this.closeVolumeBarTimer);
    }

    /**
     * sets the timer as 3 sec to close the volume bar control.
     */
    private setVolumeBarTimer()
    {
        this.clearVolumeBarTimer(); // clear out any pre-existing timers
        this.closeVolumeBarTimer = setTimeout(() =>
        {
            this.hideVolumeBar();
        }, 3000);
    }

    /**
     * Shows the volume control
     */
    private showVolumeBar(): void
    {
        this.volumeControlIsShown = true;
    }

    /**
     * Hides the volume control
     */
    private hideVolumeBar(): void
    {
        this.volumeControlIsShown = false;
    }

    /**
     * Sets the local volume properties
     * @param {number} volume - volume to be updated
     * @param {boolean} persist - flag whether to put the volume in local storage.
     * @param {boolean} notifyMediaPlayer - flag whether to update volume in media player.
     */
    private setVolumeData(volume: number, persist: boolean, notifyMediaPlayer: boolean = true)
    {
        if (volume === this.volume)
        {
            persist = false;
            notifyMediaPlayer = false;
        }

        this.setVolume(volume);

        if (persist)
        {
            this.setStatusVolume(volume);
        }

        this.volumeService.adjustVolume(volume, persist, notifyMediaPlayer);

        this.volumeImageUrl = this.volumeService.getVolumeImageSrc(volume, this.isFullscreen);

        if (!this.volumeImageUrl)
        {
            VolumeComponent.logger.error(`setVolumeData( this.volume = ${ volume } , falling back )`);
            this.volumeImageUrl = !this.screenfull.isFullscreen ? "../../assets/images/volume/volume-3-blue" :
                                  "../../assets/images/volume/volume-3-white";
        }

        this.setVolumeBarTimer();
    }
}
