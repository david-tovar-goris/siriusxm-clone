// NOTE: ELC 2018-03-19 per WEBEVEREST-1252 the Chromecast button has been removed until it is implemented

import { Component, Input } from "@angular/core";

@Component({
    selector: 'chromecast-button',
    template: `<li class="chromecast-control-item">
                    <button (click)="onConnectChromecast()">
                        <img src="../../../../assets/images/chromecast/chromecast-available.svg"
                             alt="Cast"/>
                    </button>
                </li>`
})
export class ChromecastComponent
{
    @Input() content;

    constructor(){}
    public onConnectChromecast(): void
    {
        console.log('Pending Implementation');
    }
}
