import {
    Component,
    EventEmitter,
    Input,
    Output
} from "@angular/core";

import { INowPlayingStore } from '../../../common/store';
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes, AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "go-to-live-btn",
    template: `<button class="go-live-btn"
                       [ngClass]="{ 'visibility-hidden': !canShowGoToLive && !isLiveAndDisallowed, 'french-btn': isFrench }"
                       sxm-new-analytics
                       [tagName]="AnalyticsTagNameConstants.MNP_GO_LIVE"
                       [userPath]="AnalyticsUserPaths.MNP_GO_LIVE"
                       [userPathScreenPosition]="3"
                       [pageFrame]="AnalyticsPageFrames.MODAL"
                       [modal]="AnalyticsModals.MINI_NOW_PLAYING_BAR"
                       [elementType]="AnalyticsElementTypes.BUTTON"
                       [buttonName]="AnalyticsTagNameConstants.MNP_GO_LIVE_BUTTON_NAME"
                       [elementPosition]="8"
                       [action]=AnalyticsTagActionConstants.BUTTON_CLICK
                       [actionSource]="AnalyticsTagActionSourceConstants.MININP"
                       [inputType]=AnalyticsInputTypes.MOUSE
                       (click)="onGoToLiveClick()"
                       [disabled]="!canGoToLive"
                       [attr.aria-label]="(canGoToLive ? 'nowPlaying.goLive' : 'nowPlaying.live') | translate">
                   <span class="btn-focus-container" tabindex="-1">
                        {{ (canGoToLive ? 'nowPlaying.goLive' : 'nowPlaying.live') | translate }}
                   </span>
               </button>`,
    styleUrls: [ "../control-buttons.scss" ]
})

export class GoToLiveButtonComponent
{
    /**
     * Broadcast when the user clicks the go to live button.
     */
    @Output()
    public goToLive = new EventEmitter<any>();

    /**
     * Indicates if the currently playing media can go to live.
     */
    @Input()
    public canGoToLive: boolean;

    /**
     * Indicates if the currently playing content is media type live and is also dmca disallowed.
     */
    @Input()
    public isLiveAndDisallowed: boolean;

    /**
     * Indicates if the currently playing media can show go to live button.
     */
    @Input()
    public canShowGoToLive: boolean;

    /**
     * The current show.
     */
    @Input()
    public content: INowPlayingStore;

    /**
     * Indicates if the current language is French
     */
    @Input()
    public isFrench: boolean;

    //Analytics constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    /**
     * Constructor.
     */
    constructor(){}

   /**
     * Broadcasts the event to goto live for the current media playback.
     */
    public onGoToLiveClick()
    {
        this.goToLive.emit();
    }
}

