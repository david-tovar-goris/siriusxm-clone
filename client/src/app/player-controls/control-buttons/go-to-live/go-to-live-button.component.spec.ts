import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {ConfigService, MediaPlayerService, SxmAnalyticsService} from "sxmServices";
import { TranslationModule } from "../../../translate/translation.module";
import { MockDirective } from "../../../../../test/mocks/component.mock";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../../test/mocks/router.mock";
import { SharedModule } from "../../../common/shared.module";
import { ChannelListStoreService } from "app/common/service/channel-list.store.service";
import { ChannelListServiceMock } from "../../../../../test/mocks/channel-list.service.mock";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { By } from "@angular/platform-browser";
import { GoToLiveButtonComponent } from "app/player-controls/control-buttons/go-to-live/go-to-live-button.component";

describe('Go to live button component', () =>
{
    let mediaPlayerService,
        fixture: ComponentFixture<GoToLiveButtonComponent>,
        component: GoToLiveButtonComponent,
        debugElement;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function() {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(() =>
    {
        mediaPlayerService = {
            mediaPlayer: {
                togglePausePlay: function() {}
            },
            isPlaying: function()
            {
                return true;
            }
        };

        this.store = {
            select: () => new BehaviorSubject(null)
        };

        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SharedModule
            ],
            declarations: [
                GoToLiveButtonComponent,
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType",
                        "userPathScreenPosition", "modal", "tagText", "actionSource"]})
            ],
            providers: [
                { provide: MediaPlayerService, useValue: mediaPlayerService },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: Store, useValue: this.store},
                { provide: Router, useClass: RouterStub},
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock }
            ]
        });
        mediaPlayerService = TestBed.get(MediaPlayerService);
        fixture = TestBed.createComponent(GoToLiveButtonComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        fixture.detectChanges();
    });

    describe('go to live button analytics', () =>
    {
        it('should create the component', () =>
        {
            expect(component).toBeTruthy();
        });

        it("logs the go to live event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            spyOn(component, "onGoToLiveClick").and.callThrough();
            spyOn(component.goToLive, "emit");
            let goLiveButton = debugElement.query(By.css('.go-live-btn'));
                goLiveButton.triggerEventHandler('click', {
                    target: {
                        tagName: "MnpGoLive"
                    }
                });

            fixture.detectChanges();
            expect(component.onGoToLiveClick).toHaveBeenCalled();
            expect(component.goToLive.emit).toHaveBeenCalled();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
