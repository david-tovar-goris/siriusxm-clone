import { filter } from 'rxjs/operators';
import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {
    Router,
    Event,
    NavigationEnd
} from "@angular/router";
import {
    Logger
} from "sxmServices";
import { AutoUnSubscribe } from "../../../common/decorator";
import { appRouteConstants } from "../../../app.route.constants";
import { NavigationService } from "../../../common/service/navigation.service";
import { INowPlayingStore } from '../../../common/store';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { TranslateService } from '@ngx-translate/core';
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens, AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";


@Component({
    selector: "expand-button",
    templateUrl: "./expand-button.component.html",
    styleUrls: [ "./expand-button.component.scss" ]
})

@AutoUnSubscribe()
export class ExpandButtonComponent implements OnInit, OnDestroy
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ExpandButtonComponent");

    /**
     * Flag indicating if the "Expand" button should display.
     * We will hide it on the Now Playing page.
     */
    public shouldDisplay: boolean = true;

    /**
     * String that is announced by aria-live region
     * when expand button is clicked.
     */
    public expandAnnouncement: string = '';

    /**
     * AutoUnSubscribe will use this to unsubscribe from any observable subscriptions.
     */
    private subscriptions = [];

    /**
     * The current show.
     */
    @Input()
    public content: INowPlayingStore;

    /* Analytics Constants*/
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    /////////////////////////////////////////////////////////////////////////////////////
    // Public Variables
    /////////////////////////////////////////////////////////////////////////////////////

    constructor(private navigationService: NavigationService,
                private router: Router,
                private liveAnnouncer: LiveAnnouncer,
                private translate: TranslateService)
    {
        const routerSubscription = router.events.pipe(
            filter((event: Event) => (event instanceof NavigationEnd)))
            .subscribe((event: Event) =>
            {
                // do not display the "Expand" button if we are on the Now Playing page
                this.shouldDisplay = !(event as any).url.includes(appRouteConstants.NOW_PLAYING);
            });

        this.subscriptions.push(routerSubscription);

        this.expandAnnouncement = this.translate.instant('navigation.accessibility.expand');
    }

    ngOnInit(): void
    {
        // do not display the "Expand" button if we are on the Now Playing page
        this.shouldDisplay = !(window.location.href.includes(appRouteConstants.NOW_PLAYING));

        ExpandButtonComponent.logger.debug(`ngOnInit( shouldDisplay = ${this.shouldDisplay} )`);
    }

    /**
     * When this component is destroyed, remove the subscription
     */
    ngOnDestroy(): void
    {
        ExpandButtonComponent.logger.debug("ngOnDestroy()");
    }

    public onExpandButtonClick(): void
    {
        ExpandButtonComponent.logger.debug("onExpandButtonClick()");
        this.navigationService.go([ appRouteConstants.NOW_PLAYING ]);
        this.liveAnnouncer.announce(this.expandAnnouncement, 'polite');
    }
}
