import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ExpandButtonComponent } from "./expand-button.component";
import { SxmAnalyticsService } from "sxmServices";
import { TranslationModule } from "../../../translate/translation.module";
import { MockDirective } from "../../../../../test/mocks/component.mock";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../../test/mocks/router.mock";
import { SharedModule } from "../../../common/shared.module";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { By } from "@angular/platform-browser";
import { NavigationService } from "../../../common/service/navigation.service";
import { navigationServiceMock } from "../../../../../test/mocks/navigation.service.mock";

describe('ExpandButtonComponent', () =>
{
    let fixture: ComponentFixture<ExpandButtonComponent>,
        component: ExpandButtonComponent;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(() =>
    {

        this.store = {
            select: () => new BehaviorSubject(null)
        };

        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SharedModule
            ],
            declarations: [
                ExpandButtonComponent,
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType",
                        "userPathScreenPosition", "modal", "tagText", "actionSource"]})
            ],
            providers: [
                { provide: Store, useValue: this.store},
                { provide: Router, useClass: RouterStub},
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: NavigationService, useValue: navigationServiceMock }
            ]
        });
        fixture = TestBed.createComponent(ExpandButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('expand button analytics', () =>
    {
        it("logs the expand event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.expand-btn'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "MnpContExp"
                    }
                });
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
