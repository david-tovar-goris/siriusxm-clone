import { MediaUtil } from "sxmServices";
import { PlayerControlsService } from "./player-controls.service";
import { MediaTimestampServiceMock } from "../../../test/mocks/media-timestamp.service.mock";
import { LiveTimeServiceMock } from "../../../test/mocks/live-time.service.mock";
import { SeekServiceMock } from "../../../test/mocks/seek.service.mock";
import { Store } from '@ngrx/store';
import { IAppStore } from "app/common/store";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";

describe("Player Controls Service", () =>
{
    let nowPlayingStore = new BehaviorSubject<any>(null);
    let liveTimeService: any = new LiveTimeServiceMock();
    let seekService: any = new SeekServiceMock();
    let mediaTimestampService: any = new MediaTimestampServiceMock();
    let store = {
        select: () => nowPlayingStore
    } as any as Store<IAppStore>;
    let service = new PlayerControlsService(mediaTimestampService, seekService, liveTimeService, store);

    it("Should have a test subject.", () =>
    {
        expect(service).toBeDefined();
    });

    it("can determine if the current media type is live content", () =>
    {
        spyOn(MediaUtil, "isLiveMediaType");
        service.isLive();
        expect(MediaUtil.isLiveMediaType).toHaveBeenCalled();
    });

    it("can restart the currently playing episode.", () =>
    {
        spyOn(seekService, "seekThenPlay");
        service.isRestartButtonClicked$.subscribe((value) =>
        {
            console.log("isRestartButtonClicked$");
            expect(value).toBe(true);
            expect(seekService.seekThenPlay).toHaveBeenCalledWith(0, true);
        });
        service.restart();
    });

    it("can go to the live point of the currently playing episode.", () =>
    {
        spyOn(seekService, "seekThenPlay");
        mediaTimestampService.isPlayheadBehindLive = true;
        service.gotoLive();
        liveTimeService.liveTime$.subscribe(() =>
        {
            console.log("liveTime$");
            expect(seekService.seekThenPlay).toHaveBeenCalled();
        });
    });

});
