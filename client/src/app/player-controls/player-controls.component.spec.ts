import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { PlayerControlsComponent } from "app/player-controls/player-controls.component";
import { CommonModule } from "@angular/common";
import { NowPlayingStoreService } from "app/common/service/now-playing.store.service";
import { NowPlayingStoreServiceMock } from "../../../test/mocks/now-playing.store.service.mock";
import { nowPlayingStoreMock } from "../../../test/mocks/data/now-playing-store.mock";
import { ProgressBarService } from "app/media/progress-bar/progress-bar.service";
import { ProgressBarServiceMock } from "../../../test/mocks/progress-bar.service.mock";
import { MediaPlayerServiceMock } from "../../../test/mocks/media-player.service.mock";
import { MockComponent } from "../../../test/mocks/component.mock";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../test/mocks/translate.service";
import { PlayerControlsService } from "../player-controls/player-controls.service";
import { PlayerControlsServiceMock } from "../../../test/mocks/player-controls.service.mock";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { IAppStore } from "app/common/store";
import { MediaTimestampServiceMock } from "../../../test/mocks/media-timestamp.service.mock";
import { mock } from "ts-mockito";
import { CastingOverlayService } from "../player-controls/popups/chromecast-overlay/chromecast-overlay.service";
import { SettingsServiceMock } from "../../../test/mocks/settings.service.mock";
import { Observable, of, of as observableOf } from "rxjs";
import { ToastService } from "../common/service/toast/toast.service";
import { ToastServiceMock } from "../../../test/mocks/toast.service.mock";
import { Skip15SecondsService } from "../player-controls/control-buttons/skip-15-seconds/skip-15-seconds.service";
import { Skip15SecondsServiceMock } from "../../../test/mocks/skip-15-seconds.service.mock";
import {
    IAppByPassState,
    MediaPlayerService,
    MediaTimestampService,
    ChromecastService,
    SettingsService,
    BypassMonitorService,
    DmcaService,
    IMediaShow, KeyboardEventKeyTypes, HtmlElementTypes
} from "sxmServices";

import { DebugElement, NO_ERRORS_SCHEMA, Pipe, PipeTransform, ApplicationRef } from '@angular/core';
import { IChromecastData } from "../../../../servicelib/src/chromecast/chromecast.interface";

describe("PlayerControlsComponent", () =>
{

    let component: PlayerControlsComponent;
    let element: HTMLElement;
    let fixture: ComponentFixture<PlayerControlsComponent>;
    let debugElement: DebugElement;

    let nowPlayingStore;
    let store;

    let pcService;
    let castingOverlayServiceMock = mock(CastingOverlayService);
    let applicationRef;
    let settingsService;

    beforeEach(() =>
    {
        nowPlayingStore = new BehaviorSubject<any>(nowPlayingStoreMock);
        store = {
            select: () => nowPlayingStore
        } as any as Store<IAppStore>;

        let chromecastServiceMock = mock(ChromecastService);
        chromecastServiceMock.chromecastInfo = new BehaviorSubject<any>(null);
        castingOverlayServiceMock = mock(CastingOverlayService);
        let bypassMonitorService = {
            bypassErrorState: observableOf({} as IAppByPassState)
        };

        let dmcaService: any = {
            isUnrestricted: jasmine.createSpy('isUnrestricted'),
            isRestricted: jasmine.createSpy('isRestricted'),
            isDisallowed: jasmine.createSpy('isDisallowed'),
            skipInfoDictionary$: new BehaviorSubject<any>({}) as any
        } as any as DmcaService;

        pcService = new PlayerControlsServiceMock();

        applicationRef = {
            components: [{ instance: { viewConainterRef: {} } }]
        };

        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                TranslateModule.forRoot()
            ],
            providers: [
                { provide: NowPlayingStoreService, useClass: NowPlayingStoreServiceMock },
                { provide: ProgressBarService, useClass: ProgressBarServiceMock },
                { provide: MediaPlayerService, useClass: MediaPlayerServiceMock },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: PlayerControlsService, useValue: PlayerControlsServiceMock },
                { provide: Store, useValue: store },
                { provide: MediaTimestampService, useClass: MediaTimestampServiceMock },
                { provide: ChromecastService, useFactory: () => { return chromecastServiceMock; } },
                { provide: CastingOverlayService, useFactory: () => { return castingOverlayServiceMock; } },
                { provide: SettingsService, useValue: SettingsServiceMock.getSpy() },
                { provide: BypassMonitorService, useValue: bypassMonitorService },
                { provide: ToastService, useValue: ToastServiceMock.getSpy() },
                { provide: Skip15SecondsService, useValue: Skip15SecondsServiceMock },
                { provide: DmcaService, useValue: dmcaService },
                { provide: ApplicationRef, useValue: applicationRef }
            ],
            declarations: [
                PlayerControlsComponent,
                MockComponent({ selector: "favorite-btn", inputs: ["viewType", "isMiniNPView" ] }),
                MockComponent({ selector: "program-descriptive-text",
                    inputs: [
                        "channel",
                        "channelLogoUrl",
                        "albumName",
                        "albumImageUrl",
                        "artistName",
                        "trackName",
                        "episode",
                        "mediaType",
                        "isOnDemand",
                        "isSeededRadio",
                        "isLive"
                    ]}),
                MockComponent({ selector: "restart-btn",
                    inputs: [
                        "isVisibilityHidden",
                        "isFrench",
                        "canGoToLive",
                        "content"
                    ]}),
                MockComponent({ selector: "back-1-hour",
                    inputs: [
                        "isLiveAndDisallowed",
                        "isFrench",
                        "content"
                    ]}),
                MockComponent({ selector: "skip-back-15-seconds", inputs: ["content"] }),
                MockComponent({ selector: "affinity-button", inputs: ["affinityType", "content"] }),
                MockComponent({ selector: "skip-back-button", inputs: ["isLiveAndDisallowed", "content"] }),
                MockComponent({ selector: "play-button", inputs: ["content"] }),
                MockComponent({ selector: "skip-forward-button", inputs: ["isLiveAndDisallowed", "content"] }),
                MockComponent({ selector: "skip-forward-15-seconds", inputs: ["content"] }),
                MockComponent({ selector: "go-to-live-btn",
                    inputs: [
                        "isLiveAndDisallowed",
                        "isFrench",
                        "canShowGoToLive",
                        "canGoToLive",
                        "content"
                    ]}),
                MockComponent({ selector: "progress-bar",
                    inputs: [
                        "isDmcaUnrestricted",
                        "segments",
                        "content"
                    ]}),
                MockComponent({ selector: "time-display", inputs: []}),
                MockComponent({ selector: "expand-button", inputs: ["content"] }),
                MockComponent({ selector: "sxm-volume", inputs: ["content"] }),
                MockComponent({ selector: "google-cast-launcher",
                    inputs: [
                        "tagName",
                        "userPath",
                        "pageFrame",
                        "modal",
                        "elementType",
                        "buttonName",
                        "tagText",
                        "elementPosition",
                        "action",
                        "inputType",
                        "actionSource"
                    ] }),
                MockComponent({ selector: "skip-message", inputs: []})
            ],
            schemas: [
                NO_ERRORS_SCHEMA
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(PlayerControlsComponent);
        component = fixture.componentInstance;
        element = fixture.nativeElement;
        debugElement = fixture.debugElement;
    });

    it("should create the component instance", () =>
    {
        expect(component).toBeTruthy();
    });

    it("can observe the currently playing channel", () =>
    {
        component.nowPlayingData$.subscribe((val) =>
        {
            expect(val).toEqual(nowPlayingStore.value);
        });
    });

    it("can allow users to restart the currently playing episode", () =>
    {
        spyOn(PlayerControlsServiceMock, "restart");
        component.restart();
        expect(PlayerControlsServiceMock.restart).toHaveBeenCalled();
    });

    it("can allow users to goto the live point for the currently playing episode", () =>
    {
        spyOn(PlayerControlsServiceMock, "gotoLive");
        component.goToLive();
        expect(PlayerControlsServiceMock.gotoLive).toHaveBeenCalled();
    });

    describe("mouse event handlers", () =>
    {

        let progressBarService;

        beforeEach(() =>
        {
            progressBarService = TestBed.get(ProgressBarService);
        });

        it("can handle the onDragMove event", () =>
        {
            const event = { type: "dragmove" };
            spyOn(progressBarService, "onDragMove");
            component.onDragMouse(event);
            expect(progressBarService.onDragMove).toHaveBeenCalledWith(event);
        });

        it("can handle the onDragStop event", () =>
        {
            let event = { type: "dragstop" };
            spyOn(progressBarService, "onDragStop");
            component.onDragStop(event);
            expect(progressBarService.onDragStop).toHaveBeenCalledWith(event);
        });
    });

    it("can get the chromecast device name from the service", async(() =>
    {
        const chromecastService = TestBed.get(ChromecastService);

        let mockData: IChromecastData = {
            deviceName: "some device",
            castStatusMessage: "some cast status"
        };

        component.observeChromecastData();

        chromecastService.chromecastInfo.next(mockData);

        expect(component.castingData).toEqual(mockData);

        chromecastService.chromecastInfo.unsubscribe();

    }));

    describe("keyboard event handlers", () =>
    {

        let mediaPlayerService;

        beforeEach(() =>
        {
            mediaPlayerService = new MediaPlayerServiceMock();
        });

        it("can ignore the keyboard play/pause event if the target is an input", () =>
        {
            const inputElement = document.createElement('input');
            const spacebarEvent: any = {
                key: KeyboardEventKeyTypes.SPACEBAR,
                target: inputElement,
                preventDefault: () =>
                {
                    return;
                }
            };

            spyOn(mediaPlayerService.mediaPlayer, "togglePausePlay");
            component.onKeyPress(spacebarEvent);
            expect(mediaPlayerService.mediaPlayer.togglePausePlay).not.toHaveBeenCalled();
        });

        it("can handle keyboard play/pause event", () =>
        {
            const spacebarEvent: any = {
                key: KeyboardEventKeyTypes.SPACEBAR,
                target: element,
                preventDefault: () =>
                {
                    return;
                }
            };

            spyOn(mediaPlayerService.mediaPlayer, "togglePausePlay");
            component.mediaPlayerService.mediaPlayer.togglePausePlay = jasmine.createSpy('togglePausePlay')
                                                                            .and.returnValue(observableOf(true));
            component.onKeyPress(spacebarEvent);
            expect(mediaPlayerService.mediaPlayer.togglePausePlay).toHaveBeenCalled();
        });
    });

    it("can show the chromecast casting welcome screen", () =>
    {
        spyOn(castingOverlayServiceMock, "open");
        component.openCastingOverlay();
        expect(castingOverlayServiceMock.open).toHaveBeenCalled();
    });

});
