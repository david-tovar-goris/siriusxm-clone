import { IApronSegment } from "sxmServices";
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'segmentsStartInEpisodePipe' })
export class SegmentsStartInEpisodePipe implements PipeTransform
{
    transform(segments: IApronSegment[])
    {
        return segments.filter(segment => segment.secondsFromBeginningOfEpisode >= 0);
    }
}
