import * as _ from "lodash";
import { TimeDisplayComponent } from "../time-display/time-display.component";
import { TimeDisplayPipe } from './time-display.pipe';

describe("TimeDisplayPipe Test Suite >>", () =>
{
    let timeDisplayPipe: TimeDisplayPipe = null;

    beforeEach(() =>
    {
        timeDisplayPipe = new TimeDisplayPipe;
    });

    describe("Infrastructure >> ", () =>
    {
        it("should exist", () =>
        {
            expect(timeDisplayPipe.transform).toBeDefined();
            expect(_.isFunction(timeDisplayPipe.transform)).toEqual(true);
        });
    });

    describe("Execution >> ", () =>
    {
        let inputSeconds: number;
        let expectedString: String;

        it("formats `undefined` input as ''", () =>
        {
            inputSeconds = undefined;
            expectedString = TimeDisplayComponent.ZERO_TIME;

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats `NaN` input as ''", () =>
        {
            inputSeconds = NaN;
            expectedString = TimeDisplayComponent.ZERO_TIME;

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats zero input as 0:00", () =>
        {
            inputSeconds = 0;
            expectedString = "0:00";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats negative input as 0:00", () =>
        {
            inputSeconds = -1;
            expectedString = "0:00";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats 0:59", () =>
        {
            inputSeconds = 59;
            expectedString = "0:59";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats 1:00", () =>
        {
            inputSeconds = 60;
            expectedString = "1:00";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats 1:01", () =>
        {
            inputSeconds = 61;
            expectedString = "1:01";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats 9:59", () =>
        {
            inputSeconds = (60 * 10) - 1;
            expectedString = "9:59";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats 10:00", () =>
        {
            inputSeconds = 60 * 10;
            expectedString = "10:00";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats 10:01", () =>
        {
            inputSeconds = (60 * 10) + 1;
            expectedString = "10:01";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats 59:59", () =>
        {
            inputSeconds = (60 * 60) - 1;
            expectedString = "59:59";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats 1:00:00", () =>
        {
            inputSeconds = 60 * 60;
            expectedString = "1:00:00";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });

        it("formats 1:00:01", () =>
        {
            inputSeconds = (60 * 60) + 1;
            expectedString = "1:00:01";

            expect(timeDisplayPipe.transform(inputSeconds, [''])).toEqual(expectedString);
        });
    });
});
