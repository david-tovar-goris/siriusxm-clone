/**
 * @description Custom Time Format Pipe
 *              Used in the player controls to format elapsed / remaining time.
 *              Convert a time in seconds to human-readable display.
 *              Trim leading zeroes and display two digit numbers when appropriate.
 *
 * @example
 * Displays "0:59"
 *          <span>{{ 59 | timeDisplayPipe }}</span>
 * Displays "1:00"
 *          <span>{{ 60 | timeDisplayPipe }}</span>
 * Displays "1:01"
 *          <span>{{ 61 | timeDisplayPipe }}</span>
 * Displays "9:59"
 *          <span>{{ 599 | timeDisplayPipe }}</span>
 * Displays "59:59"
 *          <span>{{ 3599 | timeDisplayPipe }}</span>
 * Displays "1:00:00"
 *          <span>{{ 3600 | timeDisplayPipe }}</span>
 * Displays "1:00:01"
 *          <span>{{ 3601 | timeDisplayPipe }}</span>
 */

import {
    Pipe,
    PipeTransform
} from "@angular/core";
import { TimeDisplayComponent } from "../time-display/time-display.component";

@Pipe({ name: "timeDisplayPipe" })
export class TimeDisplayPipe implements PipeTransform
{
    transform(value: number, args: string[]): string
    {
        const isNumber: boolean = (typeof value === "number") && !isNaN(value);
        const isValidValue: boolean = value >= 1;

        // Never show the user NaN.
        // Never allow values less than the minimum threshold to be considered a valid value.
        // For example, we might get passed -1 as audio/video is loaded/unloaded format and
        // exit early since we should never display a negative number.
        if (!isNumber || !isValidValue)
        {
            return TimeDisplayComponent.ZERO_TIME;
        }

        const hours: string = String(Math.floor(value / 3600));
        let minutes: string = String(Math.floor(value / 60) % 60);
        let seconds: string = String(Math.floor(value) % 60);

        // seconds should always be two digits
        if (Number(seconds) < 10)
        {
            seconds = "0" + seconds;
        }

        if (Number(hours) === 0)
        {
            return `${minutes}:${seconds}`;
        }

        // for lengths above an hour, minutes should always be two digits
        if (Number(minutes) < 10)
        {
            minutes = "0" + minutes;
        }

        return `${hours}:${minutes}:${seconds}`;
    }
}
