import {
    Component,
    ElementRef,
    HostListener,
    Input,
    OnInit,
    ViewChild
} from "@angular/core";
import { IVideoPlayerData } from "app/video/video-player-data.interface";
import { SubscriptionLike as ISubscription } from "rxjs";
import {
    IChannel,
    IMediaAssetMetadata,
    IMediaEpisode,
    ConfigService,
    Logger
} from "sxmServices";
import { appRouteConstants } from "../../app.route.constants";
import { AutoUnSubscribe } from "../../common/decorator";
import { NavigationService } from "../../common/service/navigation.service";
import { NowPlayingStoreService } from "../../common/service/now-playing.store.service";
import { TranslateService } from "@ngx-translate/core";
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import {EllipsisStringUtil} from "app/common/util/ellipsisString.util";
import {TileUtil} from "app/common/util/tile.util";

@AutoUnSubscribe()
@Component({
    selector: "program-descriptive-text",
    templateUrl: "./program-descriptive-text.component.html",
    styleUrls: [ "./program-descriptive-text.component.scss" ]
})

export class ProgramDescriptiveTextComponent implements OnInit
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ProgramDescriptiveTextComponent");

    private static TEXT_SCROLL_LENGTH_LIMIT = 19;

    /////////////////////////////////////////////////////////////////////////////////////
    // Inputs
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * The currently playing channel.
     * mini player text on hover should be scrollable for seeded radio if length exceeds limit
     */
    private _channel : IChannel;

    @Input() set channel(channel : IChannel)
    {
        this._channel = channel;
        this.seededChannelNameScrollable(channel, this.isSeededRadio);
        this.getButtonLabel();
    }

    get channel()
    {
        return this._channel;
    }

    /**
     * The current channel logo's URL.
     */
    @Input()
    public channelLogoUrl: string;

    /**
     * The album name for the currently playing media.
     */
    @Input()
    public albumName: string;

    /**
     * The album image URL for the currently playing media.
     */
    @Input()
    public albumImageUrl: string;

    /**
     * Indicates if currently playing media is type seeded radio.
     */
    private _isSeededRadio : boolean;
    @Input() set isSeededRadio(isSeeded: boolean)
    {
        this._isSeededRadio = isSeeded;
        this.seededChannelNameScrollable(this.channel, isSeeded);
        this.getButtonLabel();
    }
    get isSeededRadio() { return this._isSeededRadio; }

    /**
     * Indicates if the user is listening to live channel or not.
     */
    @Input()
    public isLive: boolean;

    /**
     * The artist for the currently playing media.
     */
    private _artistName : string;

    @Input() set artistName(artistName : string)
    {
        this._artistName = artistName;

        this.artistNameScrollable = ProgramDescriptiveTextComponent.checkIfTextShouldBeScrolled(artistName);

        this.getButtonLabel();
    }

    get artistName() { return this._artistName; }

    /**
     * The track name for the currently playing media.
     */
    private _trackName : string;

    @Input() set trackName(trackName : string)
    {
        this._trackName = trackName;

        this.trackNameScrollable = ProgramDescriptiveTextComponent.checkIfTextShouldBeScrolled(trackName);
    }

    get trackName() { return this._trackName; }

    /**
     * The currently playing AOD episode.
     */
    @Input() set episode(episode : IMediaEpisode)
    {
        if (episode && episode.show)
        {
            this.showTitle = episode.show.mediumTitle;
            this.showTitleScrollable = ProgramDescriptiveTextComponent.checkIfTextShouldBeScrolled(this.showTitle);
        }
    }

    /**
     * The current media type for the media player.
     */
    @Input()
    public mediaType: string;

    private _isOnDemand : boolean;
    @Input() set isOnDemand(val : boolean)
    {
        this._isOnDemand = val;
        this.getButtonLabel();
    }

    get isOnDemand() { return this._isOnDemand; }

    private subscriptions: Array<ISubscription> = [];

    public   mediaAssetMetadata: IMediaAssetMetadata;
    public   playheadTime: number;
    public   showTitle : string;
    private  showTitleScrollable = false;
    private  artistNameScrollable = false;
    private  trackNameScrollable = false;
    public   ariaLabel: string;
    public   defaultAlbumImgClass: string = "album-image";
    public   fallbackImageUrl: string = "../../../assets/images/no-album-music-note.png";

    /**
     * public getter for pdtTextScrollable and the private backing store that will be recalculated when a resize event
     * is encountered
     */
    private  _pdtTextScrollable;
    public get pdtTextScrollable()
    {
        if (this._pdtTextScrollable === undefined) { this.onResize(); }
        return this._pdtTextScrollable;
    }

    /**
     * Window resize host listener will recalculate the private backing store for pdtTextScrollable when the UI is
     * resized
     */
    @HostListener('window:resize')
    onResize()
    {
        this._pdtTextScrollable = ((this.pdtTextContainer
                                    && this.pdtTextContainer.nativeElement.clientWidth <= "100")
                                   || (this.showTitleScrollable
                                   || this.artistNameScrollable
                                   || this.trackNameScrollable));
    }

    /**
     * Element reference to the player controls container
     */
    @ViewChild('pdtTextContainer') pdtTextContainer: ElementRef;

    /* Analytics Constants*/
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    /**
     * Indicates image loaded or not.
     */
    public isImageLoaded: boolean = true;

    constructor(private navigationService: NavigationService,
                private nowPlayingStoreService: NowPlayingStoreService,
                private configService: ConfigService,
                private translateService: TranslateService) {}

    ngOnInit()
    {
        ProgramDescriptiveTextComponent.logger.debug("ngOnInit()");
        this.subscribeToVideoPlayerData();
    }

    public onPdtTextClick(): void
    {
        ProgramDescriptiveTextComponent.logger.debug("onPdtTextClick()");
        this.navigationService.go([ appRouteConstants.NOW_PLAYING ]);
    }

    /**
     * Observe video player data for recreating mini video player
     */
    private subscribeToVideoPlayerData(): void
    {
        ProgramDescriptiveTextComponent.logger.debug(`subscribeToVideoPlayerData()`);

        const videoDataUpdateSuccess = (data: IVideoPlayerData): void =>
        {
            ProgramDescriptiveTextComponent.logger.warn(`videoDataUpdateSuccess()`);

            this.mediaAssetMetadata = data.mediaAssetMetadata || {} as IMediaAssetMetadata;
            this.playheadTime = data.playheadTime || 0;
        };

        const videoDataUpdateFault = (fault: any): void =>
        {
            ProgramDescriptiveTextComponent.logger.warn(`videoDataUpdateFault() ${JSON.stringify(fault)}`);
        };

        this.subscriptions.push(
            this.nowPlayingStoreService.videoPlayerDataSlice.subscribe(
                videoDataUpdateSuccess.bind(this),
                videoDataUpdateFault
            )
        );
    }

    /**
     * Check if given text is long enough that is should be scrolled
     *
     * @param {string} text to check for length
     * @returns {boolean} true if text is long enough for scrolling, false otherwise
     */
    private static checkIfTextShouldBeScrolled(text : string) : boolean
    {
        return (text && text.length >= ProgramDescriptiveTextComponent.TEXT_SCROLL_LENGTH_LIMIT);
    }

    /**
     * Creates label for button.
     * @returns {string} made up of pdt data and navigation message
     */
    public getButtonLabel(): void
    {
        const artistStationName = this.channel && this.isSeededRadio ? this.channel.name : "";
        const showTitle = this.isOnDemand ? this.showTitle : "";
        const artistName = this.artistName ? this.artistName : "";
        const trackName = this.trackName ? this.trackName : "";
        const channelNumber = (this.channel && this.channel.channelNumber) && (!this.isOnDemand)
            ? this.channel.channelNumber
            : "";

        this.ariaLabel = `${artistStationName} ${showTitle} ${channelNumber} ${artistName} ${trackName}`;
    }

    /**
     * check if channel name has to be scrolled for seeded radio.
     * @param {channel} currently playing channel.
     * @param {isSeeded} is artist radio.
     */
    private seededChannelNameScrollable(channel: IChannel, isSeeded: boolean)
    {
        this.trackNameScrollable =  isSeeded &&
            ProgramDescriptiveTextComponent.checkIfTextShouldBeScrolled(channel.name);
    }

    /**
     * Clips the alt text... for fallback state.
     * @param imgAltText
     */
    public clipTextWithEllipsis(imgAltText:string): string
    {
        return EllipsisStringUtil.ellipsisString(imgAltText, 42);
    }

    /**
     * Set show alt text when image errors
     * @param error
     */
    public onImageError(error: any)
    {
        this.isImageLoaded = false;
    }
}
