import { ComponentFixture, TestBed } from "@angular/core/testing";
import {MediaPlayerService,
    SxmAnalyticsService,
    BypassMonitorService,
    IAppByPassState,
    ConfigService} from "sxmServices";
import { MockDirective } from "../../../../test/mocks/component.mock";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../test/mocks/router.mock";
import { SharedModule } from "app/common/shared.module";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { By } from "@angular/platform-browser";
import { FavoriteListStoreService } from "app/common/service/favorite-list.store.service";
import { FavoriteListStoreServiceMock } from "../../../../test/mocks/favorite-list.store.service.mock";
import { TranslationModule } from "app/translate/translation.module";
import { ToastService } from "app/common/service/toast/toast.service";
import { of as observableOf } from "rxjs";
import { ToastServiceMock } from "../../../../test/mocks/toast.service.mock";

import { ProgramDescriptiveTextComponent } from "app/player-controls/program-descriptive-text/program-descriptive-text.component";
import { NavigationService } from "app/common/service/navigation.service";
import { navigationServiceMock } from "../../../../test/mocks/navigation.service.mock";
import { NowPlayingStoreService } from "app/common/service/now-playing.store.service";
import { NowPlayingStoreServiceMock } from "../../../../test/mocks/now-playing.store.service.mock";
import { ConfigServiceMock }     from "../../../../test/mocks/config.service.mock";

describe('Program descriptive text component', () =>
{
    let mediaPlayerService,
        favoriteListStoreService,
        fixture: ComponentFixture<ProgramDescriptiveTextComponent>,
        component: ProgramDescriptiveTextComponent;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(() =>
    {
        mediaPlayerService = {
            mediaPlayer: {
                togglePausePlay: function()
                {
                }
            },
            isPlaying: function()
            {
                return true;
            }
        };

        this.store = {
            select: () => new BehaviorSubject(null)
        };

        favoriteListStoreService = new FavoriteListStoreServiceMock();

        let bypassMonitorServiceMock = {
            bypassErrorState: observableOf({} as IAppByPassState)
        };

        let configServiceMock = new ConfigServiceMock();

        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SharedModule
            ],
            declarations: [
                ProgramDescriptiveTextComponent,
                MockDirective({ selector: "sxm-new-analytics", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType",
                        "userPathScreenPosition", "modal", "tagText", "actionSource"]})
            ],
            providers: [
                { provide: MediaPlayerService, useValue: mediaPlayerService },
                { provide: Store, useValue: this.store},
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: FavoriteListStoreService, useValue: favoriteListStoreService },
                { provide: BypassMonitorService, useValue: bypassMonitorServiceMock },
                { provide: ToastService, useValue: ToastServiceMock.getSpy() },
                { provide: Router, useClass: RouterStub},

                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: NowPlayingStoreService, useClass: NowPlayingStoreServiceMock },
                { provide: ConfigService, useValue: configServiceMock }
            ]
        });
        mediaPlayerService = TestBed.get(MediaPlayerService);
        fixture = TestBed.createComponent(ProgramDescriptiveTextComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('PDT analytics', () =>
    {
        it("logs the PDT button event", () =>
        {
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.pdt-columns-container'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "MnpContExp"
                    }
                });
            fixture.detectChanges();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
