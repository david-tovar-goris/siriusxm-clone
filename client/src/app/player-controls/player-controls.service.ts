import { combineLatest as observableCombineLatest, Observable, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import {
    LiveTimeService,
    Logger,
    MediaUtil,
    MediaTimestampService,
    LiveTime,
    SeekService
} from "sxmServices";
import { getCurrentEpisodeZuluStartTime, getMediaType} from "../common/store/now-playing.store";
import { Store } from "@ngrx/store";
import { IAppStore } from "../common/store/app.store";

@Injectable()
export class PlayerControlsService
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("PlayerControlsService");

    /**
     * The media type stream.
     */
    public mediaType$: Observable<string> = this.store.select(getMediaType);

    /**
     * The current playing episode zulu start time
     */
    public currentEpisodeZuluStartTime$: Observable<number> = this.store.select(getCurrentEpisodeZuluStartTime);

    /**
     * An observable that can be used to determine if restart button was clicked.
     */
    public isRestartButtonClicked$: Subject<boolean>;

    /**
     * Holds media type
     */
    private mediaType: string;

    /**
     * Holds the current episode zulu start time
     */
    private currentEpisodeZuluStartTime: number;

    constructor(private mediaTimestampService: MediaTimestampService,
                private seekService: SeekService,
                private liveTimeService: LiveTimeService,
                private store: Store<IAppStore>)
    {
        this.observeNowPlayingStore();
        this.isRestartButtonClicked$ = new Subject();
    }

    /**
     * Determines if the current media type is live content.
     * @returns {boolean}
     */
    public isLive(): boolean
    {
        return MediaUtil.isLiveMediaType(this.mediaType);
    }

    /**
     * Allows users to restart the currently playing episode.
     */
    public restart(): void
    {
        PlayerControlsService.logger.debug(`restart()`);

        const time: number = this.isLive() ? this.currentEpisodeZuluStartTime : 0;
        this.seekService.seekThenPlay(time, true);
        this.isRestartButtonClicked$.next(true);
    }

    /**
     * Allows users to goto the live point of the currently playing episode if they currently
     * are playing live and behind the live point in time.
     */
    public gotoLive(): void
    {
        if (this.mediaTimestampService.isPlayheadBehindLive)
        {
            this.liveTimeService.liveTime$.pipe(take(1)).subscribe((liveTime: LiveTime) =>
            {
                PlayerControlsService.logger.debug(`gotoLive( ${ liveTime.zuluMilliseconds } )`);
                this.seekService.seekThenPlay(liveTime.zuluMilliseconds);
            });
        }
    }

    /**
     * Used to observe the now playing store data.
     */
    private observeNowPlayingStore()
    {
        observableCombineLatest(this.mediaType$, this.currentEpisodeZuluStartTime$,
            (mediaType: string, currentEpisodeStartZuluTime: number) =>
            {
                this.mediaType = mediaType;
                this.currentEpisodeZuluStartTime = currentEpisodeStartZuluTime;
            }).subscribe();
    }
}
