import {
    Component,
    EventEmitter,
    Input,
    Output
} from "@angular/core";
import { SubscriptionLike as ISubscription } from "rxjs";

import { AutoUnSubscribe } from "../../../common/decorator/auto-unsubscribe";
import { IApronSegment, MediaTimestampService }         from "sxmServices";
import { ProgressBarService }    from "../../../media/progress-bar/progress-bar.service";
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "scrub-ball",
    templateUrl: "./scrub-ball.component.html",
    styleUrls: [ "./scrub-ball.component.scss" ]
})

@AutoUnSubscribe()
export class ScrubBallComponent
{
    /**
     * The list of segments for the current episode.
     */
    @Input()
    public segments: IApronSegment[];

    public SCRUB_BALL_RADIUS = 0.6578;

    /**
     * Flag indicating if the current playing content is DMCA unrestricted.
     */
    @Input()
    public isDmcaUnrestricted: boolean;

    /* Analytics constants */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsContentTypes = AnalyticsContentTypes;

    @Input() segmentInfo: {
        segmentTitle: string;
        segmentTime: string;
    } = {
        segmentTitle: "",
        segmentTime: ""
    };
    @Output() segmentTitleChange = new EventEmitter<{
        segmentTitle: string;
        segmentTime: string;
    }>();

    /**
     * Stores subscription to ProgressBarService.isBeingDragged so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    constructor(public progressBarService: ProgressBarService,
                public mediaTimestampService: MediaTimestampService)
    {
        this.subscriptions.push(
            progressBarService.isBeingDragged.subscribe((isBeingDragged: boolean) =>
            {
                if(isBeingDragged)
                {
                    this.retrieveSegmentTitle();
                }
            })
        );
    }

    public retrieveSegmentTitle(): void
    {
        this.segments.forEach((segment: IApronSegment) =>
        {
            if (this.mediaTimestampService.playheadTimestamp < segment.endTimeInSeconds
                && this.mediaTimestampService.playheadTimestamp >= segment.startTimeInSeconds)
            {
                Object.assign(this.segmentInfo, {
                    segmentTitle: segment.title,
                    segmentTime: segment.timeDisplay
                });
                this.segmentTitleChange.emit(this.segmentInfo);
            }
        });
    }

    /**
     * On Mouse Drag Stop over the Player Container
     *
     * @param event
     */
    public onDragStart(evt: any): void
    {
        this.progressBarService.onDragStart();

        // To Prevent selection cursor while dragging scrub ball for Safari.
        evt.preventDefault();
    }
}
