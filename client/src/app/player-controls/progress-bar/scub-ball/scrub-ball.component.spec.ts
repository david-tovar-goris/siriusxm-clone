import { ComponentFixture, TestBed} from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { DebugElement } from "@angular/core";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs";
import { ScrubBallComponent } from "./scrub-ball.component";
import { ProgressBarService } from "../../../media/progress-bar/progress-bar.service";
import { ProgressBarServiceMock } from "../../../../../test/mocks/progress-bar.service.mock";
import { IApronSegment,
    MediaTimestampService,
    SxmAnalyticsService
} from "sxmServices";
import { MediaTimestampServiceMock } from "../../../../../test/mocks/media-timestamp.service.mock";
import { SegmentsService } from "../../../media/segments/segments.service";
import { SegmentsServiceMock } from "../../../../../test/mocks/segments.service.mock";
import { SxmNewAnalyticsDirective } from "../../../analytics/sxm-analytics.directive";

interface ThisContext
{
    fixture: ComponentFixture<ScrubBallComponent>;
    component: ScrubBallComponent;
    segmentsService: SegmentsService;
    mediaTimestampService: MediaTimestampService;
    debugElement: DebugElement;
}

describe('ScrubBallComponent', () =>
{

    const sxmAnalyticsServiceMock = {
        updateNowPlayingData: function() {},
        logAnalyticsTag: function() {}
    };

    const storeMock = {
        select: () => new BehaviorSubject(null)
    };

    const segment = {
        title: 'Cool Beans',
        endTimeInSeconds: 100,
        startTimeInSeconds: 98,
        timeDisplay: '6h 2m'
    };

    const segments: IApronSegment[] = [
        segment
    ] as IApronSegment[];

    beforeEach(function(this: ThisContext)
    {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule
            ],
            declarations: [
                SxmNewAnalyticsDirective,
                ScrubBallComponent
            ],
            providers: [
                { provide: Store, useValue: storeMock},
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock},
                { provide: ProgressBarService, useClass: ProgressBarServiceMock },
                { provide: SegmentsService, useClass: SegmentsServiceMock },
                { provide: MediaTimestampService, useClass: MediaTimestampServiceMock }
            ]
        }).compileComponents();
        this.mediaTimestampService = TestBed.get(MediaTimestampService);
        this.segmentsService = TestBed.get(SegmentsService);
        this.fixture = TestBed.createComponent(ScrubBallComponent);
        this.component = this.fixture.componentInstance;
        this.component.segments = segments;
        this.component.isDmcaUnrestricted = true;
        this.debugElement = this.fixture.debugElement;
        this.fixture.detectChanges();
    });

    describe('retrieveSegmentTitle()', function(this: ThisContext)
    {
        beforeEach(function(this: ThisContext)
        {
            spyOn(this.component.segmentTitleChange, 'emit').and.returnValue(segment);
            this.mediaTimestampService.playheadTimestamp = 99;
            this.component.retrieveSegmentTitle();
        });

        it('returns the segment title and duration that matches the user\'s current scrubball position'
            , function(this: ThisContext)
        {
            expect(this.component.segmentInfo).toEqual({
                segmentTitle: 'Cool Beans',
                segmentTime: '6h 2m'
            });
        });

        it('emits an event to notify the parent component of changes', function(this: ThisContext)
        {
            expect(this.component.segmentTitleChange.emit).toHaveBeenCalledWith({
                segmentTitle: 'Cool Beans',
                segmentTime: '6h 2m'
            });
        });

        it("onDragStart by clicking scrubball", function(this: ThisContext)
        {
            let spy = spyOn(this.component, 'onDragStart');
            const scrubball = this.debugElement.nativeElement.querySelector("#progress-indicator-ball");
            scrubball.addEventListener("click",this.component.onDragStart);
            scrubball.click();
            expect(spy).toHaveBeenCalled();
        });
    });
});
