import {
        AfterViewInit,
        ChangeDetectionStrategy,
        Component,
        ElementRef,
        Input,
        ViewChild,
        ChangeDetectorRef
} from "@angular/core";
import { IApronSegment, MediaTimestampService } from "sxmServices";
import { Store } from "@ngrx/store";
import { IAppStore } from "../../common/store/app.store";
import { ProgressBarService } from "../../media/progress-bar/progress-bar.service";
import { combineLatest } from "rxjs";

@Component({
    selector: "progress-bar",
    templateUrl: "./progress-bar.component.html",
    styleUrls: [ "./progress-bar.component.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ProgressBarComponent implements AfterViewInit
{
    /**
     * The list of segments for the current episode.
     */
    @Input()
    public segments: IApronSegment[];

    /**
     * Flag indicating if the current playing content is DMCA unrestricted.
     */
    @Input()
    public isDmcaUnrestricted: boolean;
    @Input() content: {};
    @Input() isVideo: boolean = false;

    @ViewChild("progressBarBackground") progressBarElement: ElementRef;

    public segmentInfo: {
        segmentTitle: string;
        segmentTime: string;
    } = {
        segmentTime: "",
        segmentTitle: ""
    };

    constructor(public progressBarService: ProgressBarService,
                public mediaTimestampService: MediaTimestampService,
                public store: Store<IAppStore>,
                private changeDetector: ChangeDetectorRef)
    {
        this.changeDetector.detach();
    }

    /**
     * Used ngAfterViewInit to call resize/get prograssbar width only after the Element has been rendered.
     */
    ngAfterViewInit(): void
    {
        this.onResize();

        combineLatest(
            this.progressBarService.playheadPercentage$,
            this.mediaTimestampService.liveTime$
        ).subscribe(() =>
        {
            this.changeDetector.detectChanges();
        });
    }

    /**
     * Get the progress bar width and startx values from the DOM element.  This needs to be done on the
     * initial creation of a component and whenever a resize event is received
     *
     */
    public onResize(): void
    {
        if(this.progressBarElement)
        {
            let progressWidth = this.progressBarElement.nativeElement.offsetWidth;
            let progressStartX = this.progressBarElement.nativeElement.getBoundingClientRect().left;
            this.progressBarService.onResize(progressWidth, progressStartX);
        }
    }

    trackByFn(index, segment)
    {
        return index;
    }

    /**
     * Use the progress bar service to handle click events and pass along the current width and startx of the progress
     * bar
     * @param event for the click
     */
    public onClick(event: any)
    {
        this.progressBarService.onClickProgressBar
            (event, this.progressBarService.progressBarWidth, this.progressBarService.progressBarStartX);
    }

}


