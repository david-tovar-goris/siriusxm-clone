import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DebugElement,
         ElementRef,
         NO_ERRORS_SCHEMA
} from "@angular/core";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs";
import { ProgressBarComponent } from "app/player-controls/progress-bar/progress-bar.component";
import { ProgressBarService } from "../../media/progress-bar/progress-bar.service";
import { ProgressBarServiceMock } from "../../../../test/mocks/progress-bar.service.mock";
import { MediaTimestampService } from "sxmServices";
import { MediaTimestampServiceMock } from "../../../../test/mocks/media-timestamp.service.mock";
import { TranslationModule } from "../../translate/translation.module";
import { TimeDisplayPipe } from "../pipes/time-display.pipe";
import { SegmentsStartInEpisodePipe } from "../pipes/segments-start-in-episode.pipe";

interface ThisContext
{
    fixture: ComponentFixture<ProgressBarComponent>;
    component: ProgressBarComponent;
    debugElement: DebugElement;
}

describe('ProgressBarComponent Test Suite: ', function()
{
    const storeMock = {
        select: () => new BehaviorSubject(null)
    };

    const progressElement = {
        nativeElement: {
            offsetWidth: 200,
            getBoundingClientRect: function()
            {
                return {left:125};
            }
        }
    } as ElementRef;

    beforeEach(function(this: ThisContext)
    {
        TestBed.configureTestingModule({
            declarations: [
                ProgressBarComponent,
                TimeDisplayPipe,
                SegmentsStartInEpisodePipe
            ],
            imports: [
                TranslationModule
            ],
            providers: [
                { provide: Store, useValue: storeMock},
                { provide: ProgressBarService, useClass: ProgressBarServiceMock },
                { provide: MediaTimestampService, useClass: MediaTimestampServiceMock }
            ],
            schemas: [ NO_ERRORS_SCHEMA ]
        }).compileComponents();

        this.fixture = TestBed.createComponent(ProgressBarComponent);
        this.component = this.fixture.componentInstance;
        this.debugElement = this.fixture.debugElement;
        this.fixture.detectChanges();
    });

    it("Component to be generate", function(this: ThisContext)
    {
        expect(this.component).toBeTruthy();
    });

    it('should trigger onResize method when window is resized', function(this: ThisContext)
    {
        this.component.progressBarElement = progressElement;
        const spyOnResize = spyOn(this.component, 'onResize');
        window.dispatchEvent(new Event('resize'));
        expect(spyOnResize).toHaveBeenCalled();
    });

    it("OnClick of Progress Container", function(this: ThisContext)
    {
        let progressContainer = this.debugElement.nativeElement.querySelector("#progress-bar-container");
        let spy = spyOn(this.component, "onClick");

        progressContainer.addEventListener("click",this.component.onClick);
        progressContainer.click();

        expect(spy).toHaveBeenCalled();
    });
});
