import { NowPlayingStoreService } from "app/common/service/now-playing.store.service";
import { By } from '@angular/platform-browser';
import { TestBed } from "@angular/core/testing";
import { MediaTimestampService } from "sxmServices";
import { TimeDisplayComponent } from "./time-display.component";
import { BehaviorSubject } from 'rxjs';
import { INowPlayingStore } from "app/common/store";
import { ContentTypes } from "sxmServices";
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';


describe('TimeDisplayComponent', function()
{
    beforeEach(function()
    {
        this.nowPlayingStoreService = {
            nowPlayingStore: new BehaviorSubject(
                {
                    type: ContentTypes.LIVE_AUDIO,
                    episode: {
                        times: {
                            isoStartTime: "2020-01-29T16:00:00.000+0000",
                            isoEndTime: "2020-01-29T17:00:00.000+0000"
                        }
                    }
                } as INowPlayingStore
            )
        };

        this.translate = {
            instant: function(key)
            {
                if (key === 'timeDisplay.accessibility.showStart')
                {
                    return 'Show began at';
                }
                else if (key === 'timeDisplay.accessibility.showEnd')
                {
                    return 'and ends at';
                }
                else if (key === 'timeDisplay.accessibility.currentlyPlaying')
                {
                    return "Currently playing";
                }
                else if (key === 'timeDisplay.accessibility.of')
                {
                    return "of";
                }
            }
        };

        this.startMoment = moment(
            this.nowPlayingStoreService.nowPlayingStore
            .getValue().episode.times.isoStartTime
        );

        this.endMoment = moment(
            this.nowPlayingStoreService.nowPlayingStore
            .getValue().episode.times.isoEndTime
        );


        this.mediaTimestampService = {
            playheadTimestamp: 0,
            durationTimestamp: 3600
        };

        this.setup = function()
        {
            TestBed.configureTestingModule({
                declarations: [
                    TimeDisplayComponent
                ],
                providers: [
                    { provide: NowPlayingStoreService, useValue: this.nowPlayingStoreService },
                    { provide: MediaTimestampService, useValue: this.mediaTimestampService },
                    { provide: TranslateService, useValue: this.translate }
                ]
            })
            .compileComponents();

            this.fixture = TestBed.createComponent(TimeDisplayComponent);
            this.component = this.fixture.componentInstance;
            this.fixture.detectChanges();
        };
    });


    describe('when not on demand content', function()
    {
        describe('when valid data in now playing store', function()
        {
            beforeEach(function()
            {
                this.setup();
            });

            it('sets elapsed time span to show start time in local time LT', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.elapsed-time')).nativeElement;
                const str = " " + this.startMoment.format("LT") + " ";
                expect(span.textContent).toEqual(str);
            });

            it('sets remaining time span to show end time in local time LT', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.remaining-time')).nativeElement;
                const str = " " + this.endMoment.format("LT") + " ";
                expect(span.textContent).toEqual(str);
            });

            it('sets aria label to correct value', function()
            {
                const debugElement = this.fixture.debugElement;
                const container: HTMLElement = debugElement.query(By.css('.time-container')).nativeElement;

                const expected = `${this.translate.instant('timeDisplay.accessibility.showStart')}`
                + ` ${this.startMoment.format("LT")}`
                + ` ${this.translate.instant('timeDisplay.accessibility.showEnd')}`
                + ` ${this.endMoment.format("LT")}`;

                expect(container.getAttribute('aria-label')).toEqual(expected);
            });
        });

        describe('when invalid data in now playing store', function()
        {
            beforeEach(function()
            {
                this.nowPlayingStoreService = {
                    nowPlayingStore: new BehaviorSubject(
                        {
                            type: ContentTypes.LIVE_AUDIO,
                            episode: {
                                times: {
                                    isoStartTime: "aas;dkasgd",
                                    isoEndTime: "asdlgads"
                                }
                            }
                        } as INowPlayingStore
                    )
                };

                this.setup();
            });

            it('sets elapsed time span to 0:00', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.elapsed-time')).nativeElement;
                expect(span.textContent).toEqual(' 0:00 ');
            });

            it('sets remaining time span to 0:00', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.remaining-time')).nativeElement;
                expect(span.textContent).toEqual(' 0:00 ');
            });
        });

        describe('when playhead advances', function()
        {
            beforeEach(function()
            {
                this.mediaTimestampService.playheadTimestamp = 30;
                this.setup();
            });

            it('sets elapsed time span to show start time in local time LT', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.elapsed-time')).nativeElement;
                const str = " " + this.startMoment.format("LT") + " ";
                expect(span.textContent).toEqual(str);
            });

            it('sets remaining time span to show end time in local time LT', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.remaining-time')).nativeElement;
                const str = " " + this.endMoment.format("LT") + " ";
                expect(span.textContent).toEqual(str);
            });
        });
    });

    describe('when on demand content', function()
    {
        describe('when valid data in now playing store', function()
        {
            beforeEach(function()
            {
                this.nowPlayingStoreService = {
                    nowPlayingStore: new BehaviorSubject(
                        {
                            type: ContentTypes.AOD,
                            episode: {
                                times: {
                                    isoStartTime: "2020-01-29T16:00:00.000+0000",
                                    isoEndTime: "2020-01-29T17:00:00.000+0000"
                                }
                            }
                        } as INowPlayingStore
                    )
                };
                this.setup();
            });

            it('sets elapsed time span to show start time in local time LT', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.elapsed-time')).nativeElement;
                expect(span.textContent).toEqual(' 0:00 ');
            });

            it('sets remaining time span to show end time in local time LT', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.remaining-time')).nativeElement;
                expect(span.textContent).toEqual(' - 1:00:00 ');
            });

            it('sets aria label to correct value', function()
            {
                const debugElement = this.fixture.debugElement;
                const container: HTMLElement = debugElement.query(By.css('.time-container')).nativeElement;
                const result = `${this.translate.instant('timeDisplay.accessibility.currentlyPlaying')}`
                    + ` 0:00`
                    + ` ${this.translate.instant('timeDisplay.accessibility.of')}`
                    + ` 1:00:00`;

                expect(container.getAttribute('aria-label')).toEqual(result);
            });
        });

        describe('when invalid data in now playing store', function()
        {
            beforeEach(function()
            {
                this.nowPlayingStoreService = {
                    nowPlayingStore: new BehaviorSubject(
                        {
                            type: ContentTypes.AOD,
                            episode: {
                                times: {
                                    isoStartTime: "aas;dkasgd",
                                    isoEndTime: "asdlgads"
                                }
                            }
                        } as INowPlayingStore
                    )
                };

                this.setup();
            });

            it('sets elapsed time span to 0:00', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.elapsed-time')).nativeElement;
                expect(span.textContent).toEqual(' 0:00 ');
            });

            it('sets remaining time span to 0:00', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.remaining-time')).nativeElement;
                expect(span.textContent).toEqual(' - 1:00:00 ');
            });
        });

        describe('when playhead advances past duration', function()
        {
            beforeEach(function()
            {
                this.mediaTimestampService.playheadTimestamp = 30;

                this.nowPlayingStoreService = {
                    nowPlayingStore: new BehaviorSubject(
                        {
                            type: ContentTypes.AOD,
                            episode: {
                                times: {
                                    isoStartTime: "2020-01-29T16:00:00.000+0000",
                                    isoEndTime: "2020-01-29T17:00:00.000+0000"
                                }
                            }
                        } as INowPlayingStore
                    )
                };

                this.setup();
            });

            it('sets elapsed time span to show start time in local time LT', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.elapsed-time')).nativeElement;
                expect(span.textContent).toEqual(' 0:30 ');
            });

            it('sets remaining time span to show end time in local time LT', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.remaining-time')).nativeElement;
                expect(span.textContent).toEqual(' - 59:30 ');
            });
        });

        describe('when playhead advances', function()
        {
            beforeEach(function()
            {
                this.mediaTimestampService.playheadTimestamp = 3660;

                this.nowPlayingStoreService = {
                    nowPlayingStore: new BehaviorSubject(
                        {
                            type: ContentTypes.AOD,
                            episode: {
                                times: {
                                    isoStartTime: "2020-01-29T16:00:00.000+0000",
                                    isoEndTime: "2020-01-29T17:00:00.000+0000"
                                }
                            }
                        } as INowPlayingStore
                    )
                };

                this.setup();
            });

            it('caps elapsed time', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.elapsed-time')).nativeElement;
                expect(span.textContent).toEqual(' 1:00:00 ');
            });

            it('sets remaining time to zero', function()
            {
                const debugElement = this.fixture.debugElement;
                const span: HTMLElement = debugElement.query(By.css('.remaining-time')).nativeElement;
                expect(span.textContent).toEqual(' - 0:00 ');
            });
        });
    });
});
