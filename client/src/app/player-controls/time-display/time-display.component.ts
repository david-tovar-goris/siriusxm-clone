import { Component } from "@angular/core";
import * as moment from "moment";
import * as _ from "lodash";
import { SubscriptionLike as ISubscription } from "rxjs";
import { MediaUtil, MediaTimestampService } from "sxmServices";
import { NowPlayingStoreService } from "app/common/service/now-playing.store.service";
import { INowPlayingStore } from "app/common/store";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: "time-display",
    template: `
        <div class="time-container"
            *ngIf="displayTimeRemaining"
            attr.aria-label="{{ getLabelForOD() }}">
            <span class="elapsed-time"
                  aria-hidden="true">
                {{ getElapsedTime() }}
            </span>
            <span class="remaining-time"
                  aria-hidden="true">
                - {{ getTimeRemaining() }}
            </span>
        </div>

        <div class="time-container"
            *ngIf="!displayTimeRemaining"
            attr.aria-label="{{ getLabelForLive() }}">
            <span class="elapsed-time"
                  aria-hidden="true">
                {{ showAirTime }}
            </span>
            <span class="remaining-time"
                  aria-hidden="true">
                {{ showEndTime }}
            </span>
        </div>
    `,
    styleUrls: ["time-display.component.scss"]
})
export class TimeDisplayComponent
{
    /**
     * The zero based time to show "0:00".
     */
    public static ZERO_TIME: string = "0:00";

    /**
     * Flag indicating if the live time or time remaining for On Demand content should display.
     */
    public displayTimeRemaining: boolean = true;

    /**
     * The show's start time.
     */
    public showAirTime: string = TimeDisplayComponent.ZERO_TIME;

    /**
     * The show's end time.
     */
    public showEndTime: string = TimeDisplayComponent.ZERO_TIME;


    public liveBegText: string = '';

    public liveMidText: string = '';

    public onDemandBegText: string = '';

    public onDemandMidText: string = '';

    /**
     * Constructor.
     * @param {MediaTimestampService} mediaTimestampService
     * @param {NowPlayingStoreService} nowPlayingStoreService
     * @param {TranslateService} translate
     */
    constructor(public mediaTimestampService: MediaTimestampService,
                private nowPlayingStoreService: NowPlayingStoreService,
                private translate: TranslateService)
    {
        this.observeNowPlayingStore();

        this.liveBegText = this.translate.instant('timeDisplay.accessibility.showStart');
        this.liveMidText = this.translate.instant('timeDisplay.accessibility.showEnd');

        this.onDemandBegText = this.translate.instant('timeDisplay.accessibility.currentlyPlaying');
        this.onDemandMidText = this.translate.instant('timeDisplay.accessibility.of');
    }

    /**
     * Ensures that the playhead value is always less then the duration.
     *
     * @returns {number}
     */
    private fixPlayhead(): number
    {
        if(this.mediaTimestampService.playheadTimestamp > this.mediaTimestampService.durationTimestamp)
        {
            // if playhead is greater the duration--> return the duration value
            return this.mediaTimestampService.durationTimestamp;
        }
        return Math.floor(this.mediaTimestampService.playheadTimestamp);
    }

    /**
     * Gets the time elapsed string to display.
     *
     * @returns {string}
     */
    private getElapsedTime(): string
    {
        const playhead = this.fixPlayhead();
        return this.getTime(playhead);
    }

    /**
     * Gets the time remaining string to display.

     * @returns {string}
     */
    private getTimeRemaining(): string
    {
        const playhead = this.fixPlayhead();
        return this.getTime(this.mediaTimestampService.durationTimestamp - playhead);
    }

    /**
     * Gets the time string to display.
     *
     * @param {number} value
     * @returns {string}
     */
    private getTime(value: number): string
    {
        const isNumber: boolean = !isNaN(value);
        const isValidValue: boolean = value >= 1;

        // Never show the user NaN.
        // Never allow values less than the minimum threshold to be considered a valid value.
        // For example, we might get passed -1 as audio/video is loaded/unloaded format and
        // exit early since we should never display a negative number.
        if (!isNumber || !isValidValue)
        {
            return TimeDisplayComponent.ZERO_TIME;
        }

        const hours: string = String(Math.floor(value / 3600));
        let minutes: string = String(Math.floor(value / 60) % 60);
        let seconds: string = String(Math.floor(value) % 60);

        // seconds should always be two digits
        if (Number(seconds) < 10)
        {
            seconds = "0" + seconds;
        }

        if (Number(hours) === 0)
        {
            return `${minutes}:${seconds}`;
        }

        // for lengths above an hour, minutes should always be two digits
        if (Number(minutes) < 10)
        {
            minutes = "0" + minutes;
        }

        return `${hours}:${minutes}:${seconds}`;
    }

    /**
     * The string start time for a show in the format of "00:00:00".
     * @returns {string}
     */
    private getShowAirTime(nowPlayingStoreData: INowPlayingStore): string
    {
        let isoStartTime = _.get(nowPlayingStoreData, "episode.times.isoStartTime", TimeDisplayComponent.ZERO_TIME);
        return this.getShowTime(isoStartTime);
    }

    /**
     * The string end time for a show in the format of "00:00:00".
     * @returns {string}
     */
    private getShowEndTime(nowPlayingStoreData: INowPlayingStore): string
    {
        let isoEndTime = _.get(nowPlayingStoreData, "episode.times.isoEndTime", TimeDisplayComponent.ZERO_TIME);
        return this.getShowTime(isoEndTime);
    }

    /**
     * The string time for a show in the format of "00:00:00".
     * @returns {string}
     */
    private getShowTime(isoTime: string): string
    {
        // NOTE: BMR: 06/26/2018: Cannot use the following commented out line as it will RTE in Sarafi. Keeping here for reference.
        // const time: string = moment(isoTime).isValid()
        const time: string = (moment(isoTime, moment.ISO_8601).isValid())
            ? moment(isoTime).format("LT")
            : TimeDisplayComponent.ZERO_TIME;
        return time.trim() || TimeDisplayComponent.ZERO_TIME;
    }

    /**
     * Watch the now playing store for data changes so we can determine which time string to show.
     * @returns {ISubscription}
     */
    private observeNowPlayingStore(): void
    {
        this.nowPlayingStoreService.nowPlayingStore.subscribe((data: INowPlayingStore) =>
        {
            // Set a flag to display the show air time for on demand content or the live time .
            this.displayTimeRemaining = MediaUtil.isOnDemandMediaType(data.type) || MediaUtil.isMultiTrackAudioMediaType(data.type);

            if(this.displayTimeRemaining) { return; }

            // Set the show start and end times.
            this.showAirTime = this.getShowAirTime(data);
            this.showEndTime = this.getShowEndTime(data);
        });
    }

    /**
     * Creates dynamic label for live content.
     */
    public getLabelForLive(): string
    {
        return `${this.liveBegText} ${this.showAirTime} ${this.liveMidText} ${this.showEndTime}`;
    }

    /**
     * Creates dynamic label for on demand content.
     */
    public getLabelForOD(): string
    {
        return `${this.onDemandBegText} ${this.getElapsedTime()} ${this.onDemandMidText} ${this.getTimeRemaining()}`;
    }
}
