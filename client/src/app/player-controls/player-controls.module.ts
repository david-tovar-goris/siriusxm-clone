import { CommonModule } from "@angular/common";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FavoritesModule } from "../favorites/favorites.module";
import { TranslationModule } from "../translate/translation.module";
import { VideoPlayerModule } from "../video/video-player.module";
import { PlayerControlsComponent } from "./player-controls.component";
import { PlayerControlsService } from "./player-controls.service";
import { Skip15SecondsService } from "./control-buttons/skip-15-seconds/skip-15-seconds.service";
import { SkipButtonService } from "./control-buttons/skip-button/skip-button.service";
import { TimeDisplayPipe } from "./pipes/time-display.pipe";
import { SegmentsStartInEpisodePipe } from "./pipes/segments-start-in-episode.pipe";
import { VolumeComponent } from "./control-buttons/volume/volume.component";
import { ScrubBallComponent } from "./progress-bar/scub-ball/scrub-ball.component";
import { SkipMessageComponent } from "./control-buttons/skip-button/skip-message.component";
import { MediaModule } from "../media/media.module";
import { ProgressBarComponent } from "./progress-bar/progress-bar.component";
import { SkipBack15SecondsComponent } from "./control-buttons/skip-15-seconds/skip-back-15-seconds.component";
import { SkipForward15SecondsComponent } from "./control-buttons/skip-15-seconds/skip-forward-15-seconds.component";
import { PlayButtonComponent } from "./control-buttons/play/play-button.component";
import { SkipForwardButtonComponent } from "./control-buttons/skip-button/skip-forward-button.component";
import { SkipBackButtonComponent } from "./control-buttons/skip-button/skip-back-button.component";
import { ChromecastComponent } from "./control-buttons/chromecast/chromecast.component";
import { TimeDisplayComponent } from "./time-display/time-display.component";
import { ProgramDescriptiveTextComponent } from "./program-descriptive-text/program-descriptive-text.component";
import { SharedModule } from "../common/shared.module";
import { ExpandButtonComponent } from "./control-buttons/expand/expand-button.component";
import { RestartButtonComponent } from "./control-buttons/restart/restart-button.component";
import { GoToLiveButtonComponent } from "./control-buttons/go-to-live/go-to-live-button.component";
import { Back1HourComponent } from "./control-buttons/back-1-hour/back-1-hour.component";
import { CastingPopupsModule } from './popups/chromecast-overlay/chromecast-popups.module';
import { AffinityButtonComponent } from "./control-buttons/affinity-button/affinity-button.component";

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        FavoritesModule,
        TranslationModule,
        VideoPlayerModule,
        MediaModule,
        SharedModule,
        CastingPopupsModule
    ],
    declarations: [
        ChromecastComponent,
        ExpandButtonComponent,
        GoToLiveButtonComponent,
        PlayButtonComponent,
        PlayerControlsComponent,
        ProgramDescriptiveTextComponent,
        ProgressBarComponent,
        RestartButtonComponent,
        Back1HourComponent,
        ScrubBallComponent,
        SkipBack15SecondsComponent,
        SkipBackButtonComponent,
        SkipForward15SecondsComponent,
        SkipForwardButtonComponent,
        SkipMessageComponent,
        TimeDisplayComponent,
        TimeDisplayPipe,
        SegmentsStartInEpisodePipe,
        AffinityButtonComponent,
        VolumeComponent
    ],
    exports: [
        PlayerControlsComponent,
        SkipMessageComponent,
        ProgressBarComponent,
        TimeDisplayComponent,
        RestartButtonComponent,
        SkipBack15SecondsComponent,
        SkipBackButtonComponent,
        SkipForward15SecondsComponent,
        SkipForwardButtonComponent,
        SkipMessageComponent,
        PlayButtonComponent,
        GoToLiveButtonComponent,
        VolumeComponent
    ],
    providers: [
        PlayerControlsService,
        Skip15SecondsService,
        SkipButtonService
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class PlayerControlsModule
{
}
