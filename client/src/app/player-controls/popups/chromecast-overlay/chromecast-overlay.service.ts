import { ComponentFactoryResolver, Injectable, ViewContainerRef } from "@angular/core";
import { CastingOverlayComponent } from "./chromecast-overlay.component";

@Injectable()
export class CastingOverlayService
{
    constructor(private factoryResolver: ComponentFactoryResolver){}

    public open(viewContainerRef: ViewContainerRef, viewContainerSplashRef?: ViewContainerRef): void
    {
        const component = this.factoryResolver.resolveComponentFactory(CastingOverlayComponent),
            castingOverlayComponent = viewContainerRef.createComponent(component);
        castingOverlayComponent.instance._ref = castingOverlayComponent;
        castingOverlayComponent.instance.containerSplashRef = viewContainerSplashRef ? viewContainerSplashRef : null;
    }
}
