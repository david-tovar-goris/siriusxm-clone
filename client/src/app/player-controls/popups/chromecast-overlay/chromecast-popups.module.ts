import { CastingOverlayComponent } from "./chromecast-overlay.component";
import { CastingOverlayService } from "./chromecast-overlay.service";
import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { SettingsService } from "../../../common/service/settings/settings.service";

@NgModule({
    imports: [
        TranslateModule,
        CommonModule,
        FormsModule
    ],
    providers: [
        CastingOverlayService,
        SettingsService
    ],
    declarations: [CastingOverlayComponent],
    entryComponents: [CastingOverlayComponent]
})

export class CastingPopupsModule
{
}
