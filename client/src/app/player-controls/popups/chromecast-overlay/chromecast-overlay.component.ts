import {Component, OnDestroy, OnInit, ViewContainerRef, HostListener, ElementRef} from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { FocusUtil } from "../../../common/util/focus.util";
import {
    Logger
} from "sxmServices";

@Component({
    templateUrl: "./chromecast-overlay.component.html",
    styleUrls: ["./chromecast-overlay.component.scss"]
})

export class CastingOverlayComponent implements OnInit, OnDestroy
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ChromecastConst CastingOverlayComponent");

    public _ref: any;
    public viewContainerSplashRef: any;

    public set containerSplashRef(value: any)
    {
        this.viewContainerSplashRef = value ? value : this.viewContainerRef;
    }
    /**
     * @param {ElementRef} eRef
     */
    constructor(public translate: TranslateService,
                public viewContainerRef: ViewContainerRef,
                private eRef: ElementRef)
    {
    }

    ngOnInit()
    {
        CastingOverlayComponent.logger.debug(`ngOnInit()`);
        FocusUtil.setupAccessibleDialog(document.getElementById('chromecast-overlay'), true);
    }

    /**
     * Used this method to handle the click outside of volume bar. If the click happens out side of volume bar then
     * volume bar should be hidden.
     * @param event
     */
    @HostListener('document:click', [ '$event' ])
    clickOutside(event)
    {
        CastingOverlayComponent.logger.debug(`clickOutside()` + this.eRef.nativeElement.contains(event.target));
        if (this.eRef.nativeElement.contains(event.target))
        {
            CastingOverlayComponent.logger.debug(`clickOutside()`);
            this.close();
        }
    }

    ngOnDestroy()
    {
        FocusUtil.closeFocusedDialog();
    }

    public close(): void
    {
        CastingOverlayComponent.logger.debug(`close()`);
        this._ref.destroy();
    }

    /**
     * Used to open an overlay with text and buttons
     */
    public openOverlay(): void
    {
        CastingOverlayComponent.logger.debug(`ChromecastConst openOverlay()`);
        this.close();
    }
}
