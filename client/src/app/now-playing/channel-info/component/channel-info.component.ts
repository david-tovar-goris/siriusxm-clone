import {
    Component,
    EventEmitter,
    Inject,
    Input,
    Output
} from "@angular/core";
import {
    CarouselConsts,
    CarouselTypeConst,
    ContentTypes,
    IAppConfig,
    IChannel,
    IMediaShow,
    neriticActionConstants
} from "sxmServices";
import { FavoriteButtonConstant } from "../../../favorites/button/favorite-button.constant";
import { appRouteConstants } from "../../../app.route.constants";
import { NavigationService } from "../../../common/service/navigation.service";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import { APP_CONFIG } from "app/sxmservicelayer/sxm.service.layer.module";

@Component({
    selector: "sxm-channel-info",
    templateUrl: "./channel-info.component.html",
    styleUrls: [ "./channel-info.component.scss" ]
})
export class ChannelInfoComponent
{
    /////////////////////////////////////////////////////////////////////////////////////
    // Inputs
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Indicates if the user is listening to either AOD or VOD or PODCAST.
     */
    @Input()
    public isOnDemand: boolean;

    /**
     * Indicastes if the user is listening to Vod
     */
    @Input()
    public isVod: boolean;

    /**
     * Indicates if the user is listening to Additional internet channel or not.
     */
    @Input()
    public isAic: boolean;

    /**
     * Indicates if the user is listening to Seeded Radio or not.
     */
    @Input()
    public isSeededRadio: boolean;

    /**
     * Indicates if the user is listening to isLive or not.
     */
    @Input()
    public isLive: boolean;

    /**
     * The current channel.
     */
    @Input()
    public channel: IChannel;

    /**
     * The current show.
     */
    @Input()
    public show: IMediaShow;

    /**
     * The current channel logo's URL.
     */
    @Input()
    public channelLogoUrl: string;

    /**
     * The current channel logo's alt text.
     */
    @Input()
    public channelLogoAltText: string;

    /**
     * Broadcast when the user clicks the favorite button.
     */
    @Output()
    public toggleFavorite = new EventEmitter<any>();

    /////////////////////////////////////////////////////////////////////////////////////
    // Public Variables
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Now Playing constant
     * @type {string}
     */
    public nowPlaying = FavoriteButtonConstant.NOW_PLAYING_VIEW;

    /**
     * Analytics Constants
     * */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    constructor(@Inject(APP_CONFIG) public appConfig: IAppConfig,
                private  navigationService: NavigationService){}

    /**
     * When user clicks on channel logo - redirect to enhanced channel EDP page.
     * @param channel
     */
    public onChannelLogoClick(channel)
    {
        if(this.show.isIrisPodcast || this.show.isPodcast)
        {
            const paramChannelGuid =  this.show.isPodcast ? `&channelGuid=${channel.channelGuid}` : "";
            const url =
                      `${CarouselConsts.PAGE_NAME}=${CarouselTypeConst.ENHANCED_SHOW_EDP}&showGuid=${this.show.assetGUID}${paramChannelGuid}`;
            this.navigationService.go([appRouteConstants.ENHANCED_EDP, url, { queryParamsHandling: "merge" }]);
        }
        // only live audio channels have an channel edp page
        else if (channel.type === ContentTypes.LIVE_AUDIO)
        {
            const url =
                      `${CarouselConsts.PAGE_NAME}=${CarouselTypeConst.ENHANCED_CHANNEL_EDP}&channelGuid=${channel.channelGuid}`;
            this.navigationService.go([appRouteConstants.ENHANCED_EDP, url, { queryParamsHandling: "merge" }]);
        }
    }
}
