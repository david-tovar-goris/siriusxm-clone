import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ChannelListStoreService } from "../../common/service/channel-list.store.service";
import { FavoritesModule } from "../../favorites/favorites.module";
import { TranslationModule } from "../../translate/translation.module";
import { ChannelInfoComponent } from "./component/channel-info.component";
import { ChannelInfoContainer } from "./container/channel-info.container";
import { SharedModule } from "../../common/shared.module";

export const COMPONENTS = [
    ChannelInfoContainer,
    ChannelInfoComponent
];

export const SERVICES = [
    ChannelListStoreService
];

@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        FavoritesModule,
        SharedModule
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
    providers: SERVICES
})

export class ChannelInfoModule
{
}
