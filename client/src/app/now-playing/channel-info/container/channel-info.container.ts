import {
    ChangeDetectionStrategy,
    Component
} from "@angular/core";
import { Observable } from "rxjs";
import { Store } from "@ngrx/store";
import {
    IChannel, IMediaShow
} from "sxmServices";
import { AutoUnSubscribe } from "../../../common/decorator";
import { IAppStore } from "../../../common/store/app.store";
import {
    getChannel,
    getChannelLogoAltText,
    getChannelLogoUrl,
    getIsAIC, getIsLive,
    getIsOnDemand,
    getIsSeededRadio,
    getIsVod,
    getShow
} from "../../../common/store/now-playing.store";

@Component({
    selector: "sxm-channel-info-container",
    templateUrl: "./channel-info.container.html",
    styleUrls: [ "./channel-info.container.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChannelInfoContainer
{
    /**
     * Indicates if AOD or VOD or PODCAST is the current content type.
     */
    public isOnDemand$: Observable<boolean> = this.store.select(getIsOnDemand);

    /**
     * Indicates if Vod is the current content type.
     */
    public isVod$: Observable<boolean> = this.store.select(getIsVod);

    /**
     * Indicates if live is the current content type.
     */
    public isLive$: Observable<boolean> = this.store.select(getIsLive);

    /**
     * Indicates if  Additional internet is the current content type.
     */
    public isAic$: Observable<boolean> = this.store.select(getIsAIC);

    /**
     * Indicates if  the current content type is seeded-Radio
     */
    public isSeededRadio$: Observable<boolean> = this.store.select(getIsSeededRadio);

    /**
     * The current channel stream.
     */
    public channel$: Observable<IChannel> = this.store.select(getChannel);

    /**
     * The current show stream.
     */
    public show$: Observable<IMediaShow> = this.store.select(getShow);

    /**
     * The current channel's logo URL stream.
     */
    public channelLogoUrl$: Observable<string> = this.store.select(getChannelLogoUrl);

    /**
     * The current channel's logo alt text stream.
     */
    public channelLogoAltText$: Observable<string> = this.store.select(getChannelLogoAltText);

    /**
     * Constructor.
     */
    constructor(private store: Store<IAppStore>)
    {}
}
