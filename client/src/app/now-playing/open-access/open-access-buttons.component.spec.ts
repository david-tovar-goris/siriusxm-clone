import { TestBed } from "@angular/core/testing";
import { BehaviorSubject, of as observableOf } from "rxjs";

import { TranslationModule } from "../../translate/translation.module";
import { OpenAccessButtonsComponent } from "./open-access-buttons.component";
import { AuthenticationService,
    ConfigService,
    KochavaAnalyticsService
} from "sxmServices";
import { LoginOverlayService } from "../../open-access/popups/login-overlay/login-overlay.service";
import { SettingsService } from "../../common/service/settings/settings.service";
import { FlepzScreenService } from "../../common/service/flepz/flepz-screen.service";

import { ApplicationRef } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { SharedModule } from "app/common/shared.module";
import { LoginServiceMock } from "../../../../test/login.service.mock";
import { LoginService } from "app/auth/login/services/login.service";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../test/mocks";
import { Store } from "@ngrx/store";

describe('OpenAccessButtonsComponent', () =>
{
    let fixture, component, authenticationService, loginOverlayServiceMock, settingsService
        , configService, flepzScreenServiceMock, applicationRef, kochavaService;

    beforeEach(() =>
    {
        authenticationService = {
            userSession : observableOf({})
        };

        kochavaService = {};

        loginOverlayServiceMock = {
            open: jasmine.createSpy('open')
        };

        settingsService = {
            isSettingOn: jasmine.createSpy('isSettingOn')
        };

        configService = {
            configuration : observableOf({}),
            getOnBoardingSettings:jasmine.createSpy('getOnBoardingSettings')
        };

        flepzScreenServiceMock = {
            enableIframe: jasmine.createSpy('enableIframe')
        };

        applicationRef = {
            components: [{ instance: { viewConainterRef: {} } }]
        };

        this.getBackgroundColor = new BehaviorSubject(null);

        this.store = {
            select: () => this.getBackgroundColor
        };

        TestBed.configureTestingModule({
            imports : [TranslationModule, SharedModule],
            declarations: [ OpenAccessButtonsComponent ],
            providers: [
                { provide: AuthenticationService, useValue:authenticationService },
                { provide: LoginOverlayService, useValue: loginOverlayServiceMock },
                { provide: SettingsService, useValue: settingsService },
                { provide: ConfigService, useValue: configService },
                { provide: FlepzScreenService, useValue: flepzScreenServiceMock },
                { provide: LoginService, useClass: LoginServiceMock },
                { provide: ApplicationRef, useValue: applicationRef },
                { provide : TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: Store, useValue: this.store},
                { provide: Router, useClass: RouterStub },
                { provide: KochavaAnalyticsService, useValue: kochavaService }
            ]
        }).compileComponents();
        fixture   = TestBed.createComponent(OpenAccessButtonsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('openLoginOverlay', () =>
    {
        it('should call open on the login overlay', () =>
        {
            component.openLoginOverlay();
            expect(loginOverlayServiceMock.open).toHaveBeenCalled();
        });
    });
});
