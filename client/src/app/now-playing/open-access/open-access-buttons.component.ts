import { filter } from 'rxjs/operators';
import {
    KochavaAnalyticsService,
    AuthenticationService,
    ISession,
    ConfigService,
    IChannel,
    ChannelLineupService,
    NAME_BACKGROUND,
    PLATFORM_WEBEVEREST,
    POSTER_TILE_IMAGE_WIDTH,
    POSTER_TILE_IMAGE_HEIGHT
} from "sxmServices";
import { ApplicationRef, Component, OnInit } from "@angular/core";
import { AutoUnSubscribe } from "../../common/decorator";
import { Observable, SubscriptionLike as ISubscription } from "rxjs";
import { LoginOverlayService } from "../../open-access/popups/login-overlay/login-overlay.service";
import { SettingsService } from "../../common/service/settings/settings.service";
import { FlepzScreenService } from "../../common/service/flepz/flepz-screen.service";
import { TranslateService } from "@ngx-translate/core";
import { LoginService } from "../../auth/login/services/login.service";
import { OnBoardingSettings } from "../../../../../servicelib/src/config/interfaces/onboarding-configuration";
import { NavigationEnd, Router } from "@angular/router";
import { appRouteConstants } from "app/app.route.constants";
import { getBackgroundColor } from "app/common/store/now-playing.store";
import { Store } from "@ngrx/store";
import { IAppStore } from "app/common/store";
import { getSelectedChannel } from "app/common/store/channel-list.store";
import {languageConstants} from "app/common/component/language-toggle/language.consts";

@Component({
    selector: 'open-access-buttons',
    template: `
        <div class="open-access-banner"
             *ngIf="session.activeOpenAccessSession"
             sxm-sticky
             [alwaysSticky]="true"
             [stickyClass]="'sticky-bar'"
             [ngStyle]="{ 'background': backgroundColor }">
            <div class="open-access-banner-container">
                <div class="text">
                    <span><span class="free-preview">{{ 'openAccess.bannerCopyFreePreview' | translate }}</span>{{ 'openAccess.bannerCopy' | translate }}</span>
                </div>
                <div class="buttons">
                    <a class="setup-streaming-button"
                       [href]="loginService.getStartedUrl"
                       [attr.aria-label]="'login.accessibility.onBoarding' | translate"
                       [title]="getSignUpCopy()"
                       *ngIf="onBoardingSettings && onBoardingSettings.onboardingEnabled"
                       [target]="getTarget(loginService.getStartedUrl, 'iframe_getStarted')"
                       (click)="enableIframe('getStarted', loginService.getStartedUrl, 'iframe_getStarted')">
                        <span class="setup-streaming-button__text">{{ getSignUpCopy() }}</span>
                    </a>

                    <button class="sign-in-button"
                            (click)="openLoginOverlay()"
                            [title]="getSignInCopy()">
                        <span class="sign-in-button__text">{{ getSignInCopy() }}</span>
                    </button>
                </div>
            </div>
        </div>`,
    styleUrls: ['./open-access-buttons.component.scss']
})
@AutoUnSubscribe([])

export class OpenAccessButtonsComponent implements OnInit
{
    private subscriptions: Array<ISubscription> = [];
    public session: ISession;
    public onBoardingSettings: OnBoardingSettings;

    /**
     * Observable string indicating the background color for the NP UI.  This is used in the component to set the
     * background color property below
     */
    private backgroundColor$: Observable<string> = this.store.select(getBackgroundColor);
    private channel$: Observable<IChannel> = this.store.select(getSelectedChannel);

    /**
     * Property for the template to use to determine background color.  This will be changed when the backgroundColor$
     * observable fires.
     */
    public backgroundColor : string = null;
    private storeBackgroundColor: string = null;
    private defaultBgColor : string = '#243a5c';

    private activeURL : string = '';

    /**
     * Holds the channel background image url for episode listing page
     */
    public channelBackgroundImageUrl: string;

    constructor(authenticationService: AuthenticationService,
                private applicationRef: ApplicationRef,
                private loginOverlayService: LoginOverlayService,
                public settingsService: SettingsService,
                private configService: ConfigService,
                public flepzScreenService: FlepzScreenService,
                private loginService : LoginService,
                private router: Router,
                private store: Store<IAppStore>,
                private kochavaService : KochavaAnalyticsService,
                private translateService : TranslateService
    )
    {

        this.subscriptions.push(authenticationService.userSession.subscribe(data =>
        {
            this.session = data;
        }));

        this.subscriptions.push(this.backgroundColor$.subscribe((color: string) =>
        {
            this.storeBackgroundColor = color;
            this.setValues();
        }));
    }

    ngOnInit(): void
    {
        this.subscriptions.push(
            this.router.events.pipe(
                filter(event => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) =>
            {
                    this.activeURL = event.url;
                    this.setValues();
            }),
            this.channel$.subscribe((channel : IChannel) =>
            {
                this.channelBackgroundImageUrl = ChannelLineupService.getChannelImage(
                    channel,
                    NAME_BACKGROUND,
                    PLATFORM_WEBEVEREST,
                    POSTER_TILE_IMAGE_WIDTH,
                    POSTER_TILE_IMAGE_HEIGHT
                );
                this.setValues();
            })
        );

        this.configService.configuration.subscribe(configuration =>
        {
            if(configuration)
            {
                const currentLang = this.translateService.currentLang;
                const localeKey = currentLang === languageConstants.ENGLISH_CANADA ? languageConstants.ENGLISH : currentLang;
                this.onBoardingSettings = this.configService.getOnBoardingSettings(localeKey);
            }
        });
    }

    /**
     * set background color
     */
    private setValues(): void
    {
        if (this.activeURL.includes(appRouteConstants.ENHANCED_EDP))
        {
            this.backgroundColor = '#000';
        }
        else if (this.activeURL.includes(appRouteConstants.NOW_PLAYING))
        {
            this.backgroundColor = this.storeBackgroundColor || this.defaultBgColor;
        }
        else if (this.activeURL.includes(appRouteConstants.ON_DEMAND.EPISODES_LIST))
        {
            this.backgroundColor = `#000 url(${this.channelBackgroundImageUrl})`;
        }
        else
        {
            this.backgroundColor = null;
        }
    }

    public openLoginOverlay()
    {
       this.loginOverlayService.open(this.applicationRef.components[0].instance.viewContainerRef);
    }

    public getSignUpCopy()
    {
        return this.configService.getOpenAccessCopy("npOpenAccessSignUpButtonCopy");
    }

    public getSignInCopy()
    {
        return this.configService.getOpenAccessCopy("npOpenAccessSignInButtonCopy");
    }

    /**
     * determines if the given URL can be opened in iFrame if it is https based else new tab
     */
    public getTarget(url, httpsTarget) : string
    {
        return (url && url.length > 0 && url.match(/https:/)) ? httpsTarget:"_blank";
    }

    /**
     * enables iFrame only if url is https
     */
    public enableIframe(iFrame, url, httpsTarget) : void
    {
        if (this.getTarget(url, httpsTarget) === httpsTarget)
        {
            this.flepzScreenService.enableIframe(iFrame);
        }
    }
}
