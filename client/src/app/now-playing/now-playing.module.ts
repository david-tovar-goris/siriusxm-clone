import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { ApronSegmentsModule } from "../apron-segments/apron-segments.module";
import { CarouselModule } from "../carousel/carousel.module";
import { ChannelListModule } from "../channel-list/channel-list.module";
import { NowPlayingStoreService } from "../common/service/now-playing.store.service";
import { ShowService } from "../common/service/series/show.service";
import { SharedModule } from "../common/shared.module";
import { NavigationModule } from "../navigation/navigation.module";
import { PlayerControlsModule } from "../player-controls/player-controls.module";
import { TranslationModule } from "../translate/translation.module";
import { VideoPlayerModule } from "../video/video-player.module";
import { ChannelInfoModule } from "./channel-info/channel-info.module";
import { NowPlayingComponent } from "./now-playing.component";
import { NowPlayingContainer } from "./now-playing.container";
import { NowPlayingRouteGaurd } from "./now-playing.route-gaurd";
import { ProgramDataTextModule } from "./program-data-text/program-data-text.module";
import { YouJustHeardComponent } from "./you-just-heard/you-just-heard.component";
import { PreviousChannelArrowComponent } from "./navigation-arrows/previous-channel-arrow.component";
import { NextChannelArrowComponent } from "./navigation-arrows/next-channel-arrow.component";
import { ToastModule } from "../common/component/toast/toast.module";
import { AlbumImageComponent } from "./album-image/album-image.component";
@NgModule({
    imports: [
        CommonModule,
        CarouselModule,
        FormsModule,
        HttpModule,
        NavigationModule,
        ChannelListModule,
        ApronSegmentsModule,
        VideoPlayerModule,
        TranslationModule,
        ProgramDataTextModule,
        ChannelInfoModule,
        PlayerControlsModule,
        SharedModule,
        ToastModule
    ],
    declarations: [
        NowPlayingContainer,
        NowPlayingComponent,
        YouJustHeardComponent,
        PreviousChannelArrowComponent,
        NextChannelArrowComponent,
        AlbumImageComponent
    ],
    exports: [
        NowPlayingContainer,
        NowPlayingComponent
    ],
    providers: [
        NowPlayingStoreService,
        ShowService,
        NowPlayingRouteGaurd
    ]
})

export class NowPlayingModule
{
}
