import {
    Component,
    Input,
    OnChanges
} from "@angular/core";
import { AutoUnSubscribe } from "../../common/decorator";
import {
    AlertType,
    ConfigService,
    IAlert,
    IMediaShow
} from "sxmServices";
import { AlertClientService } from "../../common/service/alert/alert.client.service";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";

@Component({
    selector: 'set-live-video-reminder-button',
    template: `<button class="set-live-video-reminder-btn"
                       (click)="toggleButton()"
                       *ngIf="displayLiveVideoReminderBtn()"
                       [disabled]="!buttonEnabled"
                       [ngClass]="{ 'reminder-is-set': isReminderSet && buttonEnabled }">
                   <img class="set-live-video-reminder-btn__img"
                        [src]="imageSource"
                        alt=""/>
                   <span class="set-live-video-reminder-btn__text">{{ text }}</span>
               </button>`,
    styleUrls: ["./set-live-video-reminder-button.component.scss"]
})
@AutoUnSubscribe([])
export class SetLiveVideoReminderButtonComponent implements OnChanges
{
    @Input()
    public show: IMediaShow;

    @Input()
    public isOnDemand: boolean;

    public text: string;

    public imageSource: string;

    public isReminderSet: boolean;

    public buttonEnabled: boolean;

    public subscriptions: Subscription[];

    private onText: string;

    private offText: string;

    private onImageSource: string;

    private offImageSource: string;

    constructor(public alertClientService: AlertClientService,
                public translateService: TranslateService,
                public  configService : ConfigService)
    {
        this.isReminderSet = false;
        this.buttonEnabled = false;
        this.offText = this.translateService.instant('nowPlaying.shows.liveVideoReminderNotSet');
        this.onText = this.translateService.instant('nowPlaying.shows.liveVideoReminderSet');
        this.onImageSource = "../../../assets/images/reminder-on-white.svg";
        this.offImageSource = "../../../assets/images/reminder-off-white.svg";
        this.refreshImageSource();
        this.refreshText();
        this.subscriptions = [];
        this.subscriptions.push(
            this.alertClientService.alerts.subscribe(() =>
            {
                this.ngOnChanges();
            })
        );
    }

    ngOnChanges()
    {
        if (!this.isOnDemand && this.show && !this.show.isPlaceholderShow)
        {
            Object.keys(this.show).length > 0 ? this.buttonEnabled = true : this.buttonEnabled = false;
            this.isReminderSet = !!this.currentAlert();
            this.refreshImageSource();
            this.refreshText();
        }
    }

    public refreshText()
    {
        this.text = this.isReminderSet && this.buttonEnabled ? this.onText : this.offText;
    }

    public refreshImageSource()
    {
        this.imageSource = this.isReminderSet && this.buttonEnabled ? this.onImageSource : this.offImageSource;
    }

    public currentAlert(): IAlert
    {
        return this.alertClientService.getAlert(this.show.assetGUID, AlertType.LIVE_VIDEO_START);
    }

    /**
     * Toggles the live video reminder button for the show
     */
    public toggleButton()
    {
        const alert = this.currentAlert();
        if (alert)
        {
            this.alertClientService.removeAlert(
                alert.alertId,
                alert.assetGuid,
                AlertType.LIVE_VIDEO_START
            )
            .subscribe(response =>
            {
                if (response)
                {
                    this.isReminderSet = !!this.currentAlert();
                    this.refreshImageSource();
                    this.refreshText();
                }
            });
        }
        else
        {
            this.alertClientService.createAlert(
                this.show.channelId,
                this.show.assetGUID,
                AlertType.LIVE_VIDEO_START
            )
            .subscribe(response =>
            {
                if (response)
                {
                    this.isReminderSet = !!this.currentAlert();
                    this.refreshText();
                    this.refreshImageSource();
                }
            });
        }
    }

    /**
     * Show or hide "Set Live Video Reminder" button
     * @returns {boolean}
     */
    public displayLiveVideoReminderBtn(): boolean
    {
        return (!this.isOnDemand && !this.show.isPlaceholderShow
                && this.show.isLiveVideoEligible && this.configService.liveVideoEnabled());
    }
}
