import {
    Component, Input, OnChanges
} from "@angular/core";

import { TranslateService } from "@ngx-translate/core";
import { AlertClientService } from "../../common/service/alert/alert.client.service";
import { IAlert, IMediaShow, AlertType } from "sxmServices";
import { Store } from "@ngrx/store";
import { IAppStore } from "../../common/store/app.store";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { BehaviorSubject } from 'rxjs';
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "set-reminder-button",
    template: `
        <button class="set-reminder-btn"
                sxm-new-analytics
                [tagName]="AnalyticsTagNameConstants.NP_ADD_REM_REMIND"
                [userPath]="anaIsShowReminderSet ? AnalyticsUserPaths.NP_REM_REMIND : AnalyticsUserPaths.NP_ADD_REMIND"
                [userPathScreenPosition]="1"
                [buttonName]="AnalyticsTagNameConstants.NP_ADD_REM_REMIND_BUTTON_NAME"
                [pageFrame]="AnalyticsPageFrames.NOW_PLAYING_MAIN"
                [tagText]="anaIsShowReminderSet ? AnalyticsTagTextConstants.NP_REM_REMIND : AnalyticsTagTextConstants.NP_ADD_REMIND"
                [elementType]="AnalyticsElementTypes.BUTTON"
                [elementPosition]="6"
                [action]=AnalyticsTagActionConstants.BUTTON_CLICK
                [inputType]=AnalyticsInputTypes.MOUSE
                (click)="toggleShowReminder()"
                *ngIf="buttonEnabled && !isOnDemand && !show.isPlaceholderShow && !show.hideFromChannelList"
                [disabled]="!buttonEnabled"
                [ngClass]="{'reminder-is-set': alert && buttonEnabled}">
            <img class="set-reminder-btn__img"
                 [src]="setReminderImageSrc"
                 alt=""/>
            <span class="set-reminder-btn__text">{{ reminderEmitter | async  }}</span>
        </button>`,
    styleUrls: ["./set-reminder-button.component.scss"]
})

@AutoUnSubscribe([])
export class SetReminderButtonComponent implements OnChanges
{
    /**
     * Image will probably need to be toggled with reminder-on@3x.png
     */
    public setReminderImageSrc: string;

    /**
     * Reminder button text need to be toggled
     */
    public setReminderButtonText: string;

    public buttonEnabled: boolean = false;

    public reminderEmitter = new BehaviorSubject<string>(this.setReminderButtonText);

    @Input()
    public show: IMediaShow;

    /**
     * Indicates if AOD or VOD is the current content type.
     * Previous/Next Channel navigation arrows should not render for AOD/VOD
     */
    @Input()
    public isOnDemand: boolean;

    /**
     * used to store alert for the current running show
     */
    private alert: IAlert;

    // Analytics Constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsContentTypes = AnalyticsContentTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;

    public anaIsShowReminderSet: boolean = false;

    /**
     * Constructor
     * @param {TranslateService} translate
     * @param {Store<IAppStore>} store
     * @param {AlertClientService} alertClientService
     */
    constructor(private translate: TranslateService,
                private store: Store<IAppStore>,
                private alertClientService: AlertClientService)
    {
    }

    ngOnChanges()
    {
        Object.keys(this.show).length > 0 ?  this.buttonEnabled = true : this.buttonEnabled = false;
        if (!this.isOnDemand && !this.show.isPlaceholderShow)
        {
            this.setSetReminderButtonText();
            this.setReminderImageStatus();
        }
    }


    /**
     * Sets reminder Button Text
     */
    public setSetReminderButtonText()
    {
        const alert = this.alertClientService.getAlert(this.show.assetGUID, AlertType.SHOW);

        this.setReminderButtonText = alert && this.buttonEnabled ? this.translate.instant('nowPlaying.shows.showReminderSet')
            : this.translate.instant('nowPlaying.shows.showReminderNotSet');
        this.anaIsShowReminderSet = alert && this.buttonEnabled ? true : false;

        this.reminderEmitter.next(this.setReminderButtonText);

        this.alert = alert;
    }

    /**
     * Sets the reminder image status
     */
    public setReminderImageStatus()
    {
        this.setReminderImageSrc = this.alert && this.buttonEnabled ? "../../../assets/images/reminder-on-white.svg"
            : "../../../assets/images/reminder-off-white.svg";
    }

    /**
     * Toggles the show reminder button
     */
    public toggleShowReminder()
    {
        if (this.alert)
        {
            this.alertClientService.removeAlert(this.alert.alertId, this.alert.assetGuid, AlertType.SHOW)
                .subscribe((response) =>
                {
                    if (response)
                    {
                        this.setSetReminderButtonText();
                        this.setReminderImageStatus();
                    }
                });
        }
        else
        {
            this.alertClientService.createAlert(this.show.channelId, this.show.assetGUID, AlertType.SHOW)
                .subscribe((response) =>
                {
                    if (response)
                    {
                        this.setSetReminderButtonText();
                        this.setReminderImageStatus();
                    }
                });
        }
    }
}
