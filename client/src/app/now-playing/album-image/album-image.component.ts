import {
    Component,
    Input
} from "@angular/core";

import {
    ContentTypes,
    Logger
} from "sxmServices";

@Component({
    selector: "album-image",
    templateUrl: "./album-image.component.html",
    styleUrls: [ "./album-image.component.scss" ]
})
export class AlbumImageComponent
{

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("AlbumImageComponent");
    public defaultAlbumImgClass: string = "album-image";
    public defaultChannelImgClass: string = "channel-logo-image";
    public fallbackImageUrl: string = "../../../assets/images/no-album-music-note.png";
    public contentType = ContentTypes.LIVE_AUDIO;
    /**
     * The URL to the currently playing album art.
     */
    @Input()
    public albumImageUrl: string;
    /**
     * The URL to the currently playing channel logo.
     */
    @Input()
    public channelLogoUrl: string;
    /**
     * The fallback text for currently playing channel logo.
     */
    @Input()
    public channelLogoAltText: string;

    constructor()
    {
    }

}
