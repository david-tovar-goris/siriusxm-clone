import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { YouJustHeardComponent } from './you-just-heard.component';
import { TranslationModule } from "../../translate/translation.module";
import { TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import {
    IMediaCut,
    ConfigService
} from "sxmServices";
import { MockComponent, MockDirective } from "../../../../test/mocks/component.mock";
import { ConfigServiceMock } from "../../../../test/mocks/config.service.mock";

describe('YouJustHeardComponent', () =>
{
    let component: YouJustHeardComponent;
    let fixture: ComponentFixture<YouJustHeardComponent>;
    const fakeEl = { slick: jasmine.createSpy("slick"),  on: jasmine.createSpy("on") };

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule(
            {
                declarations: [
                    YouJustHeardComponent,
                    MockDirective({selector: '[download-failed]', inputs: ['download-failed']}),
                    MockDirective({ selector: "[sxm-new-analytics]", inputs: ["tagName", "userPath",
                            "userPathScreenPosition", "pageFrame", "carouselName", "buttonName", "tagText",
                            "elementPosition", "skipActionSource", "elementType", "action", "inputType", "skipCurrentPosition"]})
                ],
                imports     : [
                    TranslationModule
                ],
                providers   : [
                    { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                    { provide: ConfigService, useClass: ConfigServiceMock }
                ]
            })
               .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture                = TestBed.createComponent(YouJustHeardComponent);
        component              = fixture.componentInstance;
        component.youJustHeard = [];
        fixture.detectChanges();
    });

    it('should create', () =>
    {
        expect(component).toBeTruthy();
    });

    describe("ngAfterViewChecked", () =>
    {
        beforeEach(() =>
        {
            component.youJustHeard = [
                {
                    albumImageUrl: "imageUrl",
                    title        : "title",
                    artistName   : "artistName",
                    cut          : {} as IMediaCut
                },
                {
                    albumImageUrl: "imageUrl",
                    title        : "title",
                    artistName   : "artistName",
                    cut          : {} as IMediaCut
                }
            ];
            component.$            = jasmine.createSpy("$").and.returnValue(fakeEl) as any;
            component.ngAfterViewChecked();
        });

        it("uses the selector passed to create the carousel and sets the carousel as initialized", () =>
        {
            expect(fakeEl.slick).toHaveBeenCalled();
        });

        it("sets carouselInitialized to true so we know the carousel has been initialized", () =>
        {
            expect(component.carouselInitialized).toEqual(true);
        });
    });

    describe("ngDoCheck", () =>
    {
        beforeEach(() =>
        {
            component.$            = jasmine.createSpy("$").and.returnValue(fakeEl) as any;
            component.youJustHeard =
                [ {
                    albumImageUrl: "imageUrl",
                    title        : "title",
                    artistName   : "artistName",
                    cut          : {} as IMediaCut
                }
                ];

            component.carouselInitialized = true;
            component.ngDoCheck();
        });

        it("calls slick with 'unslick' on the passed carouselRef to destroy the given carousel", () =>
        {
            expect(fakeEl.slick).toHaveBeenCalledWith("unslick");
        });

        it("calls slick with 'unslick' on the passed carouselRef to destroy the given carousel", () =>
        {
            expect(component.carouselInitialized).toEqual(false);
        });
    });
});
