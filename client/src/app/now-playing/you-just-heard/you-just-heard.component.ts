import { debounceTime } from 'rxjs/operators';
import {
    AfterViewChecked,
    Component,
    DoCheck,
    Input,
    NgZone
} from '@angular/core';
import {
    IYouJustHeard
} from "sxmServices";
import * as $ from "jquery";
import { contentCarouselOptions } from "../../carousel/content/slick-responsive-breakpoints";
import { Subject } from "rxjs";
import {
    AnalyticsContentTypes,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: 'sxm-you-just-heard',
    templateUrl: './you-just-heard.component.html',
    styleUrls: ['./you-just-heard.component.scss'],
    host: { '(window:resize)': 'this.resizeEvent$.next(event)' }
})
export class YouJustHeardComponent implements AfterViewChecked, DoCheck
{
    /**
     * Analytics Constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsContentTypes = AnalyticsContentTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;

    /**
     * Jquery $ value
     */
    $ = $;

    /**
     * you just heard list feeding as input to this component
     */
    @Input() youJustHeard: Array<IYouJustHeard>;

    /**
     * The content type of currently playing media.
     */
    @Input() mediaType: string;

    /**
     * The fallback image url used in case of image download failure cases
     */
    public fallbackImageUrl: string = "../../../assets/images/no-album-music-note.png";

    /**
     * Value indicates the carousel is initiated or not
     * @type {boolean} - default value as false
     */
    carouselInitialized: boolean = false;

    /**
     * Holds the previous you just heard list.
     * @type {Array<IYouJustHeard>}
     */
    private previousYouJustHeardList = this.youJustHeard;

    private resizeEvent$: Subject<any> = new Subject<any>();

    public carouselSelector: string = '.you-just-heard-list';

    /**
     * Constructor
     */
    constructor(public ngZone: NgZone)
    {
        this.resizeEvent$.pipe(debounceTime(500)).subscribe(() =>
        {
            this.isElementVisible(this.carouselSelector);
        });
    }

    /**
     * Used to create the carousel and assign to local carosel ref property
     */
    private createCarousel(): void
    {
        this.carouselInitialized = true;
        const opts = Object.assign(contentCarouselOptions, {
            prevArrow: "#yjh-carousel-arrow__prev",
            nextArrow: "#yjh-carousel-arrow__next"
        });
        this.youJustHeard.forEach(tile =>
        {
            tile.albumImageUrl = this.applyResizingParams(tile.albumImageUrl,320,240);
        });
        this.$(this.carouselSelector).on('init', (event, slick) =>
        {
            this.isElementVisible(this.carouselSelector);

        });
        this.$(this.carouselSelector).on('afterChange', (event, slick) =>
        {
            this.isElementVisible(this.carouselSelector);
        });

        let element;
        this.ngZone.runOutsideAngular(() =>
        {
            element = (this.$(this.carouselSelector) as any).slick(opts);
        });
        return element;
    }

    isElementVisible (selector)
    {
        const holder = this.$(selector)[0];
        const elem = this.$(selector).find('li').last()[0];

        if (holder && elem)
        {
            const elRect = elem.getBoundingClientRect();
            const holderRect = holder.getBoundingClientRect();
            if(elRect.right < holderRect.right)
            {
                this.hideRightNavigation();
            }
            else
            {
                this.unhideRightNavigation();
            }
        }
    }

    /**
     * hide right arrow for the carousel
     */
    hideRightNavigation()
    {
        this.$('#yjh-carousel-arrow__next').css('display', 'none');
    }

    /**
     * unhide right arrow for the carousel
     */
    unhideRightNavigation()
    {
        this.$('#yjh-carousel-arrow__next').css('display', 'block');
    }

    public applyResizingParams(albumImageUrl: string, width: number, height: number): string
    {
        let url = albumImageUrl || "";
        if (url.length > 0)
        {
            if (url.indexOf("width=") === -1)
            {
                url += url.match(/\?/) ? "&width=" + width : "?width=" + width;
            }
            if (url.indexOf("height=") === -1)
            {
                url += "&height=" + height + "&preserveAspect=true";
            }
        }
        return url;
    }

    /**
     * destroy the carousel
     */
    destroyCarousel()
    {
        if (this.carouselInitialized)
        {
            this.ngZone.runOutsideAngular(() =>
            {
                (this.$(this.carouselSelector) as any).slick("unslick");
            });
            this.carouselInitialized = false;
        }
    }

    /**
     * used to detect changes on input you just heard . if any changes then destroy the carousel.
     * NOTE: Entered into one issue that OnChanges not firing all the time. Parent changes is not notified to child
     * component. This life cycle hook take cares of destroying carousel.
     *
     */
    ngDoCheck()
    {
        if (this.youJustHeard !== this.previousYouJustHeardList)
        {
            this.previousYouJustHeardList = this.youJustHeard;
            this.destroyCarousel();
        }
    }

    /**
     * Used to create carousel
     */
    ngAfterViewChecked(): void
    {
        if (!this.carouselInitialized && this.youJustHeard.length > 0)
        {
            this.createCarousel();
        }
    }
}
