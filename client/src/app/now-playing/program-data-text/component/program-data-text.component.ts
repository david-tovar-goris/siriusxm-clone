import {
    Component,
    EventEmitter,
    Input,
    Output
} from "@angular/core";
import {
    ContentTypes,
    ConfigService,
    IChannel,
    IMediaShow,
    ActionTypes,
    neriticActionConstants,
    INeriticLinkData,
    SettingsConstants,
    SettingsService
} from "sxmServices";
import { TranslateService } from "@ngx-translate/core";
import { ICarouselSelectorSegment } from "sxmServices";
import { NeriticLinkService } from "../../../neritic-links/neritic-link.service";
import { Observable } from "rxjs";
import {
    AnalyticsTagActionConstants,
    AnalyticsPageFrames,
    AnalyticsElementTypes,
    AnalyticsScreens,
    AnalyticsTagActionSourceConstants,
    AnalyticsInputTypes,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsTagTextConstants,
    AnalyticsFindingMethods
} from "../../../analytics/sxm-analytics-tag.constants";
import { IModalData } from "../../../common/service/modal/modal.interface";
import { EErrorTemplate } from "../../../common/service/error/error-template.enum";
import { ModalService } from "../../../common/service/modal/modal.service";

export type ISelectShow = {
    channel: IChannel,
    show: IMediaShow
};

@Component({
    selector: "sxm-program-data-text",
    templateUrl: "./program-data-text.component.html",
    styleUrls: [ "./program-data-text.component.scss" ]
})
export class ProgramDataTextComponent
{
    /**
     * Analytics constants
     */
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;

    public defaultAlbumImgClass: string = "album-image";
    public defaultChannelImgClass: string = "channel-logo-image";
    public fallbackImageUrl: string = "../../../../assets/images/no-album-music-note.png";

    private isCreateSeededRadio: boolean = false;

    /////////////////////////////////////////////////////////////////////////////////////
    // Inputs
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * The URL to the currently playing album art.
     */
    @Input()
    public albumImageUrl: string;

    /**
     * The URL to the currently playing channel logo.
     */
    @Input()
    public channelLogoUrl: string;

    /**
     * The fallback text for currently playing channel logo.
     */
    @Input()
    public channelLogoAltText: string;

    /**
     * The current channel.
     */
    @Input()
    public channel: IChannel;

    /**
     * The current show.
     */
    @Input()
    public show: IMediaShow;

    /**
     * The current episode name.
     */
    @Input()
    public episodeName: string;

    /**
     * The current show name.
     */
    @Input()
    public showName: string;

    /**
     * The current track name.
     */
    private _trackName: string;
    @Input() set trackName (trackName: string)
    {
        this._trackName = trackName;
        this._npArtistSelectorSegment = null;
    }
    get trackName() : string { return this._trackName; }

    /**
     * The current artist name.
     */
    @Input()
    public artistName: string;

    /**
     * Boolean flag for on demand content
     */
    @Input()
    public isOnDemand: boolean;

    @Input()
    public isInterstitial$: Observable<boolean>;

    private _contentType: string;
    @Input() set contentType(contentType: string)
    {
        this._contentType = contentType;
        this.isSeededRadio = contentType === ContentTypes.SEEDED_RADIO;
        this.label = this.nowPlayingLabel(contentType);
    }
    get contentType() : string { return this._contentType; }

    private _npArtistSelectorSegment: ICarouselSelectorSegment;

    @Input()
    public set npArtistSelectorSegment(segment: ICarouselSelectorSegment)
    {
        this._npArtistSelectorSegment = segment;

        if (segment && !this.isOnDemand)
        {
            this.isCreateSeededRadio = segment.action.actionType.indexOf('create') > -1;

            this.showPandoraXModal();
        }
    }

    public get npArtistSelectorSegment(): ICarouselSelectorSegment { return this._npArtistSelectorSegment; }

    /////////////////////////////////////////////////////////////////////////////////////
    // Outputs
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Broadcast when the user clicks the select show button.
     */
    @Output()
    public selectShow = new EventEmitter<ISelectShow>();

    /////////////////////////////////////////////////////////////////////////////////////
    // Public Variables
    /////////////////////////////////////////////////////////////////////////////////////

    public isLiveVideoPlaying: boolean = true;

    public clickShowLabel: string;

    public isSeededRadio: boolean;

    public label : string;

    private extraChannelLabel : string;

    private pandoraStationLabel : string;

    private onDemandLabel : string;

    private onDemandEpisodeLabel : string;

    private onAirShowLabel : string;

    /**
     * Constructor.
     */
    constructor(public neriticLinkService: NeriticLinkService,
                private configService: ConfigService,
                private translateService: TranslateService,
                private modalService: ModalService,
                private settingsService: SettingsService)
    {
        this.extraChannelLabel = this.translateService.instant('nowPlaying.XtraChannel');
        this.pandoraStationLabel = this.translateService.instant('artistRadio.pandoraStation');
        this.onDemandLabel = this.translateService.instant('nowPlaying.onDemand');
        this.onDemandEpisodeLabel = this.translateService.instant('nowPlaying.onDemandEpisode');

        this.onAirShowLabel = this.translateService.instant('nowPlaying.onAirShow');
    }

    public onSelectShowClick()
    {
        const event: ISelectShow = {
            channel: this.channel,
            show: this.show
        };
        this.selectShow.emit(event);
    }

    public takeArtistRadioButtonAction(segment: ICarouselSelectorSegment)
    {
        const neriticLinkData: INeriticLinkData = this.neriticLinkService.getNeriticData(segment.action.actionNeriticLink);
        neriticLinkData.analyticsTag = segment.action.actionAnalyticsTag;
        const data = {
            description: this.artistName
        };
        if (segment.action.actionType === ActionTypes.CREATE_SEEDED)
        {
            neriticLinkData.contentType = neriticActionConstants.CREATE_SEEDED_RADIO;
        }
        this.neriticLinkService.takeAction(neriticLinkData, data);
    }

    private nowPlayingLabel(contentType) : string
    {
        switch (contentType)
        {
            case ContentTypes.ADDITIONAL_CHANNEL:
            case ContentTypes.ADDITIONAL_CHANNELS:
                return this.extraChannelLabel;

            case ContentTypes.SEEDED_RADIO:
                return this.pandoraStationLabel;

            case ContentTypes.AOD:
            case ContentTypes.VOD:
            case ContentTypes.PODCAST:
                return this.onDemandEpisodeLabel;

            default:
                return this.onAirShowLabel;
        }
    }

    /**
     * PanodraX modal should show once when we get more selector
     */
    private showPandoraXModal(): void
    {
        const isPandoraXModalFlagOn = this.settingsService.isPandoraXModalFlagOn();

        if (!isPandoraXModalFlagOn)
        {
            const modalData: IModalData = {
                header: this.translateService.instant("pandoraX.newFeatureOverlayHeader"),
                description: this.translateService.instant("pandoraX.newFeatureOverlayCopy"),
                logo: {
                    url: "./assets/images/pandora_logo_intro_modal.png",
                    altText: "PandoraX"
                },
                customClass: "new-feature-modal",
                modalType: EErrorTemplate.MODAL,
                buttonOne: {
                    text: this.translateService.instant("pandoraX.newFeatureOverlayButtonLabel")
                }
            };
            this.modalService.addDynamicModal(modalData);
            this.settingsService.switchGlobalSettingOnOrOff(true, SettingsConstants.DISPLAY_PANDORAX_FEATURE_MODAL);
        }
    }

    //Show Switch Playback only for Live Video
    public showSwitchPlaybackBtn(): boolean
    {
        return this._contentType === ContentTypes.LIVE_VIDEO;
    }
}
