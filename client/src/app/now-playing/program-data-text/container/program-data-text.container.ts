import {
    ChangeDetectionStrategy,
    Component,
    Input
} from "@angular/core";
import { Store } from "@ngrx/store";
import {
    IChannel,
    IMediaShow,
    ICarouselSelectorSegment, ContentTypes, IMediaEpisode
} from "sxmServices";
import { ChannelListStoreService } from "../../../common/service/channel-list.store.service";
import { ShowService } from "../../../common/service/series/show.service";
import { IAppStore } from "../../../common/store/app.store";
import { getSelectedChannel } from "../../../common/store/channel-list.store";
import {
    getAlbumImage,
    getArtistName,
    getChannelLogoAltText,
    getChannelLogoUrl,
    getShow,
    getShowName,
    getStartTime,
    getTrackName,
    getContentType,
    getIsInterstitial,
    getIsOnDemand,
    getEpisode,
    getEpisodeName
} from "../../../common/store/now-playing.store";
import { MediaTimestampService } from "sxmServices";
import { PlayerControlsService } from "../../../player-controls/player-controls.service";
import { EllipsisStringUtil } from "../../../common/util/ellipsisString.util";
import { selectNPArtistSelectorSegment } from "../../../common/store/carousel.store";
import { Observable } from "rxjs";

@Component({
    selector: "sxm-program-data-text-container",
    templateUrl: "./program-data-text.container.html",
    styleUrls: [ "./program-data-text.container.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgramDataTextContainer
{
    /**
     * TODO: BMR: 01/02/2017: Come back to this. Would be nice to get this from a store. Comes from now playing comp right now.
     */
    @Input()
    public isVideo: boolean;

    /**
     * The current channel's logo URL stream.
     */
    public channelLogoUrl$: Observable<string> = this.store.select(getChannelLogoUrl);

    /**
     * The current channel's logo URL stream.
     */
    public channelLogoAltText$: Observable<string> = this.store.select(getChannelLogoAltText);

    /**
     * The start time for the current media player.
     */
    public startTime$: Observable<number> = this.store.select(getStartTime);

    /**
     * The current album art's logo URL stream.
     */
    public albumImageUrl$: Observable<string> = this.store.select(getAlbumImage);

    /**
     * The current channel stream.
     */
    public channel$: Observable<IChannel> = this.store.select(getSelectedChannel);

    /**
     * The current episode name.
     */
    public episodeName$: Observable<string> = this.store.select(getEpisodeName);

    /**
     * The current show stream.
     */
    public show$: Observable<IMediaShow> = this.store.select(getShow);

    /**
     * The current show name stream.
     */
    public showName$: Observable<string> = this.store.select(getShowName);

    /**
     * The current track name stream.
     */
    public trackName$: Observable<string> = this.store.select(getTrackName);

    /**
     * The current artist name stream.
     */
    public artistName$: Observable<string> = this.store.select(getArtistName);

    /**
     * The current content type stream.
     */
    public contentType$: Observable<string> = this.store.select(getContentType);

    /**
     * The current cut playing is interstitial or not.
     */
    public isInterstitial$: Observable<boolean> = this.store.select(getIsInterstitial);

    /**
     * The segment containing the neritic link if there is one for the now playing artist.
     */
    public npArtistSelectorSegment$: Observable<ICarouselSelectorSegment> = this.store.select(selectNPArtistSelectorSegment);

    /**
     * Boolean set to true if the current content type is AOD or VOD
     */
    public isOnDemand$: Observable<boolean> = this.store.select(getIsOnDemand);

    /**
     * Constructor.
     */
    constructor(private channelListStoreService: ChannelListStoreService,
                private mediaTimestampService: MediaTimestampService,
                private playerControlsService: PlayerControlsService,
                private showService: ShowService,
                private store: Store<IAppStore>)
    {
    }

    /**
     * Allows users to restart the currently playing episode.
     */
    public selectShow(channel: IChannel, show: IMediaShow): void
    {
        this.showService.selectShow(channel, show);
    }

    /**
     * Adds an ellipsis to track names that are exactly 35 characters long that don't end in ")".
     * Or, returns a two line text with ellipsis for a track name exceeding 50 characters.
     * @param {string} trackName
     * @returns {string}
     */
    public truncateTrackName(trackName: string): string
    {
        // The API seems to have a track title limit of 35 characters.  If the track title
        // is exactly 35 characters, add an ellipsis.  However, a 35-character track title
        // ending in ")" is likely exactly 35 characters long.  In this case, don't add an
        // ellipsis.
        if ((trackName.length === 35) && (trackName[34] !== ")"))
        {
            return `${trackName}...`;
        }
        else
        {
            return EllipsisStringUtil.ellipsisString(trackName, 47);
        }
    }

    /**
     * Returns a two line text with ellipsis for an artist name name exceeding 34 characters.
     * Because of the font size, 34 is the optimal number here.
     * @param {string} artistName
     * @returns {string}
     */
    public truncateArtistName(artistName: string): string
    {
        return EllipsisStringUtil.ellipsisString(artistName, 31);
    }
}
