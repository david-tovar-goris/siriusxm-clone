import * as _ from "lodash";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { of as observableOf } from "rxjs";
import { Store } from "@ngrx/store";
import { TestBed } from "@angular/core/testing";

// providers
import { ChannelListStoreService } from "../../../common/service/channel-list.store.service";
import { IAppStore } from "../../../common/store/app.store";
import { MediaTimestampService } from "sxmServices";
import { PlayerControlsService } from "../../../player-controls/player-controls.service";
import { ShowService } from "../../../common/service/series/show.service";
import { TuneClientService } from "../../../common/service/tune/tune.client.service";

// mocks
import { ChannelListServiceMock } from "../../../../../test/mocks/channel-list.service.mock";
import { MediaTimestampServiceMock } from "../../../../../test/mocks/media-timestamp.service.mock";
import { MockStore } from "../../../../../test/mocks/redux.store.mock";
import { PlayerControlsServiceMock } from "../../../../../test/mocks/player-controls.service.mock";
import { TuneServiceMock } from "../../../../../test/mocks/tune.service.mock";

// test subject
import { ProgramDataTextContainer } from "./program-data-text.container";

describe("ProgramDataTextContainer", () =>
{
    let appStoreMock;
    let component;
    let fixture;
    let showServiceMock;

    beforeEach(() =>
    {
        appStoreMock = new MockStore<IAppStore>();
        spyOn(appStoreMock, "select").and.returnValue(observableOf({}));

        showServiceMock = {
            selectShow: _.noop
        };

        TestBed.configureTestingModule({
            declarations: [ ProgramDataTextContainer ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: MediaTimestampService, useValue: MediaTimestampServiceMock },
                { provide: PlayerControlsService, useValue: PlayerControlsServiceMock },
                { provide: ShowService, useValue: showServiceMock },
                { provide: Store, useValue: appStoreMock },
                { provide: TuneClientService, useValue: TuneServiceMock }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(ProgramDataTextContainer);
        component = fixture.componentInstance;

        fixture.detectChanges();
    });

    it("should be created", () =>
    {
        expect(component).toBeTruthy();
    });

    describe("selectShow", () =>
    {
        it("should be a publicly exposed method", () =>
        {
            expect(component.selectShow).toBeTruthy();
            expect(typeof component.selectShow).toEqual("function");
        });
    });

    describe("truncateTrackName", () =>
    {
        it("should be a publicly exposed method", () =>
        {
            expect(component.truncateTrackName).toBeTruthy();
            expect(typeof component.truncateTrackName).toEqual("function");
        });

        it("should not add an ellipses if the track name is shorter than 35 characters", () =>
        {
            const shortTrackName = "Song With a Short Title";
            const expected = "Song With a Short Title";
            const actual = component.truncateTrackName(shortTrackName);

            expect(actual).toEqual(expected);
        });


        it("should add an ellipses if the track name is exactly 35 characters and does not end in ')'", () =>
        {
            const thirty5CharTrackName = "What the World Needs Now Is Love (6";
            const expected = "What the World Needs Now Is Love (6...";
            const actual = component.truncateTrackName(thirty5CharTrackName);

            expect(actual).toEqual(expected);
        });

        // A 35-character track name ending in ")" is likely exactly 35 characters long.
        // In this case we shouldn't add an ellipses.
        it("should not add an ellipses if the track name is 35 characters and ends in ')'", () =>
        {
            const thirty5CharTrackName = "I Got a Bad Case Of Lovin' You (79)";
            const expected = "I Got a Bad Case Of Lovin' You (79)";
            const actual = component.truncateTrackName(thirty5CharTrackName);

            expect(actual).toEqual(expected);
        });

        it("should truncate and an ellipses if the track name exceeds 50 characters", () =>
        {
            const reallyLongTrackName = "This is a ridiculously long track name that exceeds 50 characters";
            const expected = "This is a ridiculously long track name that ...";
            const actual = component.truncateTrackName(reallyLongTrackName);

            expect(actual).toEqual(expected);
            expect(actual.length).toEqual(47);
        });
    });

    describe("truncateArtistName", () =>
    {
        it("should be a publicly exposed method", () =>
        {
            expect(component.truncateArtistName).toBeTruthy();
            expect(typeof component.truncateArtistName).toEqual("function");
        });

        it("should not add an ellipses if the artist name is shorter than 34 characters", () =>
        {
            const shortArtistName = "Artist With a Short Name";
            const expected = "Artist With a Short Name";
            const actual = component.truncateArtistName(shortArtistName);

            expect(actual).toEqual(expected);
        });

        it("should truncate and an ellipses if the artist name exceeds 34 characters", () =>
        {
            const reallyLongArtistName = "This is a ridiculously long artist name that exceeds 34 characters";
            const expected = "This is a ridiculously long ...";
            const actual = component.truncateArtistName(reallyLongArtistName);

            expect(actual).toEqual(expected);
            expect(actual.length).toEqual(31);
        });
    });
});
