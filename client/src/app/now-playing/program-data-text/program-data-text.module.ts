import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ChannelListStoreService } from "../../common/service/channel-list.store.service";
import { TranslationModule } from "../../translate/translation.module";
import { ProgramDataTextComponent } from "./component/program-data-text.component";
import { ProgramDataTextContainer } from "./container/program-data-text.container";
import { SwitchPlaybackModule } from "../../common/component/switch-playback-button/switch-playback-button.module";
import { ShowService } from "../../common/service/series/show.service";
import { SharedModule } from "../../common/shared.module";
import { OpenAccessModule } from "../../open-access/open-access.module";
import { SecondaryFavoriteButtonComponent } from "../favorites/secondary-favorite-button.component";
import { SetReminderButtonComponent } from "../set-reminder-buttons/set-reminder-button.component";
import { SetLiveVideoReminderButtonComponent } from "../set-reminder-buttons/set-live-video-reminder-button.component";

export const COMPONENTS = [
    ProgramDataTextContainer,
    ProgramDataTextComponent,
    SecondaryFavoriteButtonComponent,
    SetReminderButtonComponent,
    SetLiveVideoReminderButtonComponent
];

export const SERVICES = [
    ChannelListStoreService,
    ShowService
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        SwitchPlaybackModule,
        TranslationModule,
        OpenAccessModule
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
    providers: SERVICES
})

export class ProgramDataTextModule
{
}
