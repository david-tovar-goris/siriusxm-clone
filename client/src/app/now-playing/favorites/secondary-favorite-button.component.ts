import { take, filter } from 'rxjs/operators';
import { Component, Input, OnChanges } from "@angular/core";
import { FavoriteListStoreService } from "../../common/service/favorite-list.store.service";
import { TranslateService } from "@ngx-translate/core";
import { FavoriteAssetTypes, IFavoriteAsset, IMediaShow, MediaUtil } from "sxmServices";
import { FavoriteButtonConstant } from "../../favorites/button/favorite-button.constant";
import { ToastService } from "../../common/service/toast/toast.service";

@Component({
    selector: 'secondary-favorite-button',
    template: `
        <button class="favorite-btn"
                *ngIf="buttonEnabled && asset && !asset.isPlaceholderShow"
                [ngClass]="{'favorited': asset.isFavorite && buttonEnabled}"
                [disabled]="!buttonEnabled"
                (click)="toggleFavorite()"
                [attr.aria-label]="favBtnAriaLabel">
            <img class="favorite-btn__img"
                 [src]="favoriteStarImageSrc"
                 alt=""/>
            <span class="favorite-btn__text">{{ favoriteButtonText }}</span>
        </button>`,
    styleUrls: ['./secondary-favorite-button.component.scss']
})

export class SecondaryFavoriteButtonComponent implements OnChanges
{
    @Input() public show: IMediaShow;

    public asset: IFavoriteAsset;
    public favoriteButtonText: string;
    public favoriteStarImageSrc: string;
    public buttonEnabled: boolean = false;
    public favBtnAriaLabel = "";

    constructor(private favoriteListStoreService: FavoriteListStoreService,
                private translate: TranslateService,
                private toastService: ToastService)
    {
    }

    ngOnChanges()
    {
        Object.keys(this.show).length > 0 ? this.buttonEnabled = true : this.buttonEnabled = false;
        this.setDefaultButton(this.asset);
        this.favoriteListStoreService.determineFavoriteAsset(FavoriteButtonConstant.NOW_PLAYING_SHOW_AND_EPISODE_VIEW).pipe(
            filter(favAsset => favAsset.contentType !== FavoriteAssetTypes.CHANNEL),
            take(1))
            .subscribe(favAsset =>
            {
                this.asset = favAsset;
                this.favBtnAriaLabel = this.getAriaLabel();
                this.setFavoriteButtonText();
                this.setFavoriteStarStatus();
            });
    }

    public setDefaultButton(asset)
    {
        if (!asset) return;
        this.setFavoriteButtonText(true);
        this.setFavoriteStarStatus(true);
    }

    public setFavoriteButtonText(useDefaultText: boolean = false)
    {
        if (MediaUtil.isOnDemandMediaType(this.asset.subContentType))
        {
            this.asset.isFavorite && !useDefaultText ?
                this.favoriteButtonText = this.translate.instant('nowPlaying.episodes.removeEpisodeFromFavorites')
                : this.favoriteButtonText = this.translate.instant('nowPlaying.episodes.addEpisodeToFavorites');
        }
        else
        {
            this.asset.isFavorite && !useDefaultText ?
                this.favoriteButtonText = this.translate.instant('nowPlaying.shows.removeShowFromFavorites')
                : this.favoriteButtonText = this.translate.instant('nowPlaying.shows.addShowToFavorites');

        }
    }

    public setFavoriteStarStatus(useDefaultText: boolean = false)
    {
        this.favoriteStarImageSrc = this.asset.isFavorite && !useDefaultText ? "../../../assets/images/favorite-on-white.svg"
            : "../../../assets/images/favorite-off-white.svg";
    }

    public toggleFavorite()
    {
        this.displayToastMessage();
        this.favoriteListStoreService.toggleFavorite(this.asset.isFavorite, this.asset.contentType,
            this.asset.subContentType, this.asset.channelId, this.asset.assetGuid, this.asset.isIrisPodcast);
        this.asset.isFavorite = !this.asset.isFavorite;
        this.favBtnAriaLabel = this.getAriaLabel();
        this.setFavoriteButtonText();
        this.setFavoriteStarStatus();
    }

    private displayToastMessage()
    {
        const contentFavoriteState = this.asset.isFavorite ? 'unFavorite' : 'Favorite';
        const toastTitle: string = MediaUtil.isOnDemandMediaType(this.asset.subContentType) ? `"${this.asset.title}"` : this.asset.title;

        this.toastService.open({
            customMessage: toastTitle,
            messagePath: `nowPlaying.toastFavorites.${this.asset.contentType}${contentFavoriteState}`,
            isAltColor: true,
            hasCloseButton: true,
            closeToastTime: 10000
        });
    }

    getAriaLabel(): string
    {
        if (!this.asset) return "";

        const isFavoriteTextTranslation = this.translate.instant(this.asset.isFavorite ? 'favoriteButton.remove' : 'favoriteButton.add');
        const fromFavTextTranslation = this.translate.instant(this.asset.isFavorite ? 'favoriteButton.fromFav' : 'favoriteButton.toFav');

        return `${isFavoriteTextTranslation} ${this.asset.title} ${fromFavTextTranslation}`;
    }
}
