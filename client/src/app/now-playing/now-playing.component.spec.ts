import {
    async,
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import {
    ActivatedRoute,
    Router
} from "@angular/router";
import { of as observableOf } from "rxjs";
import {
    AudioPlayerService,
    ChannelLineupService,
    CarouselService,
    RefreshTracksService,
    CurrentlyPlayingService,
    SettingsService,
    MediaTimeLineService,
    ClientFault,
    AppMonitorService,
    ChromecastModel,
    TuneService,
    DmcaService,
    MediaPlayerService
} from "sxmServices";
import {
    CarouselServiceMock,
    MockStore
} from "../../../test/mocks/index";
import { ChannelListServiceMock } from "../../../test/mocks/channel-list.service.mock";
import { ChannelListStoreService } from "../common/service/channel-list.store.service";
import { FavoriteListStoreService } from "../common/service/favorite-list.store.service";
import { MockComponent, MockDirective } from "../../../test/mocks/component.mock";
import { NowPlayingComponent } from "./now-playing.component";
import { NowPlayingStoreService } from "../common/service/now-playing.store.service";
import { PlayerControlsService } from "../player-controls/player-controls.service";
import { TranslationModule } from "../translate/translation.module";
import { NowPlayingStoreServiceMock } from "../../../test/mocks/now-playing.store.service.mock";
import { VideoPlayerService } from "../../../../servicelib/src/mediaplayer/videoplayer/video-player.service";
import { MediaAssetService } from "../media/media-player/media-asset.service";
import { MediaAssetServiceMock } from "../../../test/mocks/media-asset.service.mock";
import { PlayerControlsServiceMock } from "../../../test/mocks/player-controls.service.mock";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { TuneClientService } from "../common/service/tune/tune.client.service";
import { MediaPlayerServiceMock } from "../../../test/mocks/media-player.service.mock";
import { NavigationService } from "../common/service/navigation.service";
import { navigationServiceMock } from "../../../test/mocks/navigation.service.mock";
import { SettingsServiceMock } from "../../../test/mocks/settings.service.mock";
import * as _ from "lodash";
import { SeekService } from "sxmServices";
import { SeekServiceMock } from "../../../test/mocks/seek.service.mock";
import { ModalService } from "../common/service/modal/modal.service";
import { ModalServiceMock } from "../../../test/mocks/modal.service.mock";
import { IAppStore } from "../common/store";
import { Store } from "@ngrx/store";
import { TuneServiceMock } from "../../../test/mocks/tune.service.mock";

describe("NowPlayingComponent", () =>
{
    let component: NowPlayingComponent;
    let fixture: ComponentFixture<NowPlayingComponent>;
    let testTrackName = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium";


    let dmcaService = {

    };

    const mockActivatedRoute = {
        params: observableOf(
            {
                channelName: "AOD"
            }
        )
    };

    const videoPlayerServiceMock = {
        restartMediaPlayer: () => {}
    };

    const showServiceMock = {
        selectShow: jasmine.createSpy("selectShow")
    };

    let carouselStoreServiceMock = {
            selectSubCategory: _.noop,
            selectSuperCategory   : jasmine.createSpy("selectSuperCategory"),
            superCategoryCarousels: { subscribe: jasmine.createSpy('superCatSubscription') }
        },
        currentlyPlayingService  = {
            getCurrentEpisodeZuluStartTime: jasmine.createSpy('getCurrentEpisodeZuluStartTime').and.returnValue(0)
        },
        mockMediaPlayerService   = new MediaPlayerServiceMock(),
        appMonitorService = {
            faultStream: observableOf({} as ClientFault)
        },
        chromecastModel = {
            isDeviceVideoSupported: false,
            state$: {
                getValue: function() {}
            }
        } as ChromecastModel,
        appStoreMock = new MockStore<IAppStore>();

    beforeEach(async(() =>
    {
        spyOn(ChannelLineupService, "getShowOrEpisodeImage").and.returnValue("testimageurl.jpg");
        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [
                NowPlayingComponent,
                MockComponent({
                    selector: "sxm-video-player",
                    inputs: [
                        "allowApron",
                        "allowOverlay",
                        "isMini",
                        "mediaAssetMetadata",
                        "onClickVideoShowName",
                        "startTime",
                        "vodEpisode"
                    ]
                }),
                MockComponent({ selector: "sxm-channel-info-container" , inputs: [ "isOnDemand" ]}),
                MockComponent({ selector: "sxm-program-data-text-container" , inputs: [ "isVideo" ]}),
                MockComponent({ selector: "apron-segments" , inputs: [ "mediaAssetMetadata", "isVideo"]}),
                MockComponent({ selector: "sxm-you-just-heard" , inputs: [ "youJustHeard", "youJustHeard", "mediaType" ]}),
                MockComponent({ selector: "previous-channel-arrow", inputs: [ "isOnDemand", "prevChannel" ] }),
                MockComponent({ selector: "next-channel-arrow", inputs: [ "isOnDemand", "nextChannel" ] }),
                MockComponent({ selector: "secondary-favorite-button", inputs: [ "show" ] }),
                MockComponent({ selector: "set-reminder-button", inputs: [ "show", "isOnDemand" ] }),
                MockComponent({ selector: "set-live-video-reminder-button", inputs: [ "show", "isOnDemand" ] }),
                MockComponent({ selector: "sxm-apron-segments-container", inputs: [ "content" ] }),
                MockComponent({
                    selector: "sxm-video-apron",
                    inputs  : [ "isLiveVideoAvailable", "isLiveVideoAvailable", "liveVideoPDT", "allowApron", "mediaAssetMetadata" ]
                }),
                MockComponent({ selector: "content-carousel", inputs: [ "carouselData", "contentTileAnalyticTag", "analyticsInfo",
                        "zoneOrder", "zoneTitle" ] }),
                MockComponent({ selector: "album-image", inputs: [ "albumImageUrl", "channelLogoUrl", "channelLogoAltText" ] }),
                MockDirective({
                    selector: "[sxm-new-analytics]",
                    inputs: ["tagName", "screen", "userPath", "pageFrame", "elementType", "buttonName", "elementPosition",
                        "action", "inputType", "userPathScreenPosition", "modal", "tagText", "actionSource", "bannerInd",
                        "consumedPerc"
                    ]
                })
            ],
            providers: [
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: CarouselService, useValue: CarouselServiceMock.getSpy() },
                { provide: AudioPlayerService, useValue: null },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: FavoriteListStoreService, useValue: null },
                { provide: TuneService, useValue: TuneServiceMock },
                { provide: NowPlayingStoreService, useClass: NowPlayingStoreServiceMock },
                { provide: PlayerControlsService, useValue: PlayerControlsServiceMock },
                { provide: Router, useValue: null },
                { provide: CarouselStoreService, useValue : carouselStoreServiceMock },
                { provide: TuneClientService, useClass : TuneClientService },
                { provide: CurrentlyPlayingService, useValue : currentlyPlayingService },
                { provide: MediaPlayerService, useValue : mockMediaPlayerService },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: SettingsService, useValue: SettingsServiceMock.getSpy() },
                { provide: SeekService, useClass: SeekServiceMock },
                { provide: MediaTimeLineService, useValue: {} },
                { provide: AppMonitorService, useValue: appMonitorService },
                { provide: ChromecastModel, useValue: chromecastModel },
                { provide: ModalService, useValue: ModalServiceMock},
                { provide: Store, useValue: appStoreMock},
                { provide: RefreshTracksService, useValue : {} },
                { provide: VideoPlayerService, useValue: videoPlayerServiceMock },
                { provide: MediaAssetService, useClass: MediaAssetServiceMock },
                { provide: DmcaService, useValue: dmcaService}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(NowPlayingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () =>
    {
        expect(component).toBeTruthy();
    });
});
