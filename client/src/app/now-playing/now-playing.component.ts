import {
    Component,
    EventEmitter,
    Input,
    Output,
    ViewChild
} from "@angular/core";
import {
    ContentTypes,
    IChannel,
    IMediaAssetMetadata,
    IYouJustHeard,
    Logger,
    ILiveVideoPDT,
    IMediaShow,
    IMediaEpisode
} from "sxmServices";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { IAppStore, INowPlayingStore } from "../common/store";
import { TuneClientService } from "../common/service/tune/tune.client.service";

import { ModalService } from "../common/service/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { Store } from "@ngrx/store";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

export interface NowPlayingSelectedShow
{
    channel: IChannel;
    show: IMediaShow;
}

@Component({
    selector: "sxm-now-playing",
    templateUrl: "./now-playing.component.html",
    styleUrls: ["./now-playing.component.scss"]
})

export class NowPlayingComponent
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("NowPlayingComponent");

    /////////////////////////////////////////////////////////////////////////////////////
    // Inputs
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * The currently playing data.
     */
    @Input()
    public content: INowPlayingStore;

    /**
     * The currently playing channel.
     */
    @Input()
    public channel: IChannel;

    @Input()
    public show: IMediaShow;

    /**
     * The previous channel before the current channel.
     */
    @Input()
    public prevChannel: IChannel;

    /**
     * The next channel after the current channel.
     */
    @Input()
    public nextChannel: IChannel;

    /**
     * The DMCA unrestricted flag.
     *
     * Flag indicating if the playhead controls for scrubbing, click seeking, and progress indicators should be shown
     * in the UI. There are many live, music channels that don't allow the user to seek by clicking the scrub bar
     * or by dragging the scrubber -- this will drive that.
     */
    @Input()
    public isDmcaUnrestricted: boolean;

    /**
     * The DMCA restricted flag.
     */
    @Input()
    public isDmcaRestricted: boolean;

    /**
     * The start time for the current media player. Currently this is only used for the video player
     * when switching to/from the now playing view to the mini player view on the home page.
     */
    @Input()
    public startTime: number;

    /**
     * The list of tracks the user just listened to.
     */
    @Input()
    public youJustHeard: IYouJustHeard[];

    /**
     * The currently playing Episode.
     */
    @Input()
    public episode: IMediaEpisode;

    /**
     * Indicates if AOD is the current content type.
     */
    @Input()
    public isAod: boolean;

    /**
     * Indicates if AOD is the current content type.
     */
    @Input()
    public isVod: boolean;

    /**
     * Indicates if video (either live video or on demand video) is the current content type.
     */
    @Input()
    public isVideo: boolean;

    /**
     * Indicates if live video is available.
     */
    @Input()
    public isLiveVideoAvailable: boolean;

    /**
     * Indicates if AOD or VOD is the current content type.
     * Previous/Next Channel navigation arrows should not render for AOD/VOD
     */
    @Input()
    public isOnDemand: boolean;

    /**
     * Indicates if Podcast is the current content type.
     */
    @Input()
    public isIrisPodcast: boolean;

    /**
     * The background image that matches the currently playing media.
     */
    @Input()
    public backgroundImage: string;

    /**
     * The album image that matches the currently playing media.
     */
    @Input()
    public albumImage: string;


    /**
     * The album image that matches the currently playing media.
     */
    @Input()
    public channelLogoUrl: string;

    /**
     * The album image that matches the currently playing media.
     */
    @Input()
    public channelLogoAltText: string;


    /**
     * The metadata for the currently playing media.
     */
    @Input()
    public mediaAssetMetadata: IMediaAssetMetadata;

    /**
     * The program descriptive text for live video.
     */
    @Input()
    public liveVideoPDT: ILiveVideoPDT;

    /**
     * The current show name.
     */
    @Input()
    public showName;

    /**
     * The current show.
     */
    @Input()
    public showInfo;

    @Input()
    public isAIC: boolean;

    @Input()
    public isSeededRadio: boolean;

    @Input()
    public mediaType: boolean;


    /**
     * The background color that matches the currently playing media.
     */
    @Input()
    public backgroundColor: string;

    @ViewChild('backBtn') backBtn;


    /////////////////////////////////////////////////////////////////////////////////////
    // Outputs
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Broadcast when the user clicks the show button.
     */
    @Output()
    public selectShow = new EventEmitter<NowPlayingSelectedShow>();

    /**
     * Emit when user want to Go back to previous screen
     */
    @Output()
    public goBack = new EventEmitter();

    /**
     * Broadcast when the video player broadcasts its timestamp.
     */
    @Output()
    public videoPlayerTimestamp = new EventEmitter<number>();

    /**
    * Analytics Constants
    * */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    /**
     * Constructor
     * @param {CarouselStoreService} carouselStoreService
     * @param {TuneClientService} tuneClientService
     * @param {ModalService} modalService
     * @param {TranslateService} translateService
     * @param {SettingsService} settingsService
     * @param store
     */
    constructor(public carouselStoreService: CarouselStoreService,
                private tuneClientService: TuneClientService,
                private modalService: ModalService,
                private translateService: TranslateService,
                private store: Store<IAppStore>) {}

    ngOnInit()
    {
        this.backBtn.nativeElement.focus();
    }

    /**
     * Clicking on the video show name will take the user to the show's on-demand episode listing
     */
    public onSelectShow(): void
    {
        NowPlayingComponent.logger.debug(`onSelectShow()`);

        const selectedShow: NowPlayingSelectedShow = {
            channel: this.channel,
            show: this.episode.show
        };
        this.selectShow.emit(selectedShow);
    }

    /**
     * Tunes to a specific live audio channel.
     */
    public playChannel(channel: IChannel)
    {
        NowPlayingComponent.logger.debug(`playChannel( channelNumber = ${channel.channelNumber} )`);

        this.tuneClientService.tune({ channel: channel, channelId: channel.channelId, contentType: ContentTypes.LIVE_AUDIO });
    }


    /**
     * goBack to previous screen
     */
    public goToPreviousScreen()
    {
        NowPlayingComponent.logger.debug(`NowPlayingComponent - goToPreviousScreen`);

        this.goBack.emit();
    }
}
