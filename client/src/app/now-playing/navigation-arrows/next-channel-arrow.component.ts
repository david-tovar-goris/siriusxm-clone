import {
    Component,
    EventEmitter,
    Input,
    Output
} from "@angular/core";

import {
    IChannel,
    Logger
} from "sxmServices";

import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "next-channel-arrow",
    templateUrl: "./next-channel-arrow.component.html",
    styleUrls: [ "./navigation-arrows.component.scss" ]
})
export class NextChannelArrowComponent
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("NextChannelArrowComponent");

    /**
     * Indicates if the user is listening to either AOD or VOD in which case the next and previous
     * buttons should not show.
     */
    @Input()
    public isOnDemand: boolean;

    /**
     * The next channel after the current channel.
     */
    @Input()
    public nextChannel: IChannel;

    /**
     * Broadcast when the user clicks the "Next" button.
     */
    @Output()
    public playNextChannel = new EventEmitter<IChannel>();

    /**
     * Analytics Constants
     * */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    constructor()
    {
    }

    /**
     * Broadcasts the event to play the next channel.
     */
    public onNextChannelClick()
    {
        NextChannelArrowComponent.logger
            .debug(`onNextChannelClick( channelNumber = ${this.nextChannel.channelNumber} )`);

        this.playNextChannel.emit(this.nextChannel);
    }

}
