import {
    Component,
    EventEmitter,
    Input,
    Output
} from "@angular/core";

import {
    IChannel,
    Logger
} from "sxmServices";

import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "previous-channel-arrow",
    templateUrl: "./previous-channel-arrow.component.html",
    styleUrls: [ "./navigation-arrows.component.scss" ]
})
export class PreviousChannelArrowComponent
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("PreviousChannelArrowComponent");

    /**
     * Indicates if the user is listening to either AOD or VOD in which case the next and previous
     * buttons should not show.
     */
    @Input()
    public isOnDemand: boolean;

    /**
     * The previous channel before the current channel.
     */
    @Input()
    public prevChannel: IChannel;

    /**
     * Broadcast when the user clicks the "Previous" button.
     */
    @Output()
    public playPreviousChannel = new EventEmitter<IChannel>();

    /**
     * Analytics Constants
     * */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    constructor()
    {
    }

    /**
     * Broadcasts the event to play the previous channel.
     */
    public onPreviousChannelClick()
    {
        PreviousChannelArrowComponent.logger
            .debug(`onPreviousChannelClick( channelNumber = ${this.prevChannel.channelNumber} )`);

        this.playPreviousChannel.emit(this.prevChannel);
    }

}
