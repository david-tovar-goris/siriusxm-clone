import {
    combineLatest as observableCombineLatest,
    of as observableOf,
    Observable,
    SubscriptionLike as ISubscription
} from 'rxjs';
import { mergeMap, filter, map } from 'rxjs/operators';
import {
    Component,
    OnDestroy,
    OnInit
} from "@angular/core";
import { Store } from "@ngrx/store";
import {
    ChannelLineupService,
    IChannel,
    IMediaAssetMetadata,
    IVodEpisode,
    IYouJustHeard,
    Logger,
    NAME_BACKGROUND,
    ILiveVideoPDT,
    IMediaShow,
    IMediaEpisode,
    PLATFORM_WEBEVEREST
} from "sxmServices";
import { ISelectVideoPlayerDataActionPayload } from "../common/action/now-playing.action";
import { AutoUnSubscribe } from "../common/decorator";
import { NowPlayingStoreService } from "../common/service/now-playing.store.service";
import { ShowService } from "../common/service/series/show.service";
import {
    IAppStore,
    INowPlayingStore
} from "../common/store";
import {
    getNextChannel,
    getPreviousChannel
}  from "../common/store/channel-list.store";
import {
    getAlbumImage,
    getArtistName,
    getChannel,
    getIsAod,
    getIsLiveVideo,
    getIsLiveVideoAvailable,
    getIsOnDemand,
    getIsIrisPodcast,
    getIsVideo,
    getIsVod,
    getShowName,
    getTrackName,
    getStartTime,
    getYouJustHeard,
    getMediaType,
    selectNowPlayingState,
    getShow,
    getEpisode,
    getIsAIC,
    getIsSeededRadio,
    getChannelLogoAltText,
    getChannelLogoUrl,
    getBackgroundColor
} from "../common/store/now-playing.store";
import { NowPlayingSelectedShow } from "./now-playing.component";
import { EllipsisStringUtil } from "../common/util/ellipsisString.util";
import { NavigationService } from "../common/service/navigation.service";
import { ChannelListStoreService } from "../common/service/channel-list.store.service";
import { DmcaService } from "sxmServices";

@Component({
    selector: "sxm-now-playing-container",
    templateUrl: "./now-playing.container.html"
})

@AutoUnSubscribe()
export class NowPlayingContainer implements OnInit, OnDestroy
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("NowPlayingContainer");

    /**
     * The currently playing data.
     */
    public nowPlayingData$: Observable<INowPlayingStore> = this.store.select(selectNowPlayingState);

    /**
     * The currently playing channel.
     */
    public channel$: Observable<IChannel> = this.store.select(getChannel);

    /**
     * The current show.
     */
    public show$: Observable<IMediaShow> = this.store.select(getShow);

    /**
     * The previous channel stream.
     */
    public prevChannel$: Observable<IChannel> = this.store.select(getPreviousChannel);

    /**
     * The next channel stream.
     */
    public nextChannel$: Observable<IChannel> = this.store.select(getNextChannel);

    /**
     * The list of tracks the user just listened to.
     */
    public youJustHeard$: Observable<IYouJustHeard[]> = this.store.select(getYouJustHeard);

    /**
     * The currently playing episode which can be AOD/VOD.
     */
    public episode$: Observable<IMediaEpisode> = this.store.select(getEpisode);

    /**
     * The DMCA is unrestricted flag stream.
     *
     * Flag indicating if the playhead controls for scrubbing, click seeking, and progress indicators should be shown
     * in the UI. There are many live, music channels that don't allow the user to seek by clicking the scrub bar
     * or by dragging the scrubber -- this will drive that.
     */
    public isDmcaUnrestricted$: Observable<boolean> = this.store.select(selectNowPlayingState)
            .pipe(filter(nowPlayingStore => !!nowPlayingStore),
                map((nowPlayingStore: INowPlayingStore) =>
                {
                    return this.dmcaService.isUnrestricted(nowPlayingStore);
                }));

    /**
     * The DMCA is restricted flag stream.
     */
    public isDmcaRestricted$: Observable<boolean> = this.store.select(selectNowPlayingState)
        .pipe(filter(nowPlayingStore => !!nowPlayingStore),
            map((nowPlayingStore: INowPlayingStore) =>
            {
                return this.dmcaService.isRestricted(nowPlayingStore);
            }));

    /**
     * The start time for the current media player. Currently this is only used for the video player
     * when switching to/from the now playing view to the mini player view on the home page.
     */
    public startTime$: Observable<number> = this.store.select(getStartTime);

    /**
     * Indicates if AOD is the current content type.
     */
    public isAod$: Observable<boolean> = this.store.select(getIsAod);

    /**
     * Indicates if VOD is the current content type.
     */
    public isVod$: Observable<boolean> = this.store.select(getIsVod);

    /**
     * Indicates if video (live or on demand) is the current content type.
     */
    public isVideo$: Observable<boolean> = this.store.select(getIsVideo);

    /**
     * Indicates if video (live or on demand) is the current content type.
     */
    public isLiveVideo$: Observable<boolean> = this.store.select(getIsLiveVideo);

    /**
     * Indicates if live video is available.
     */
    public isLiveVideoAvailable$: Observable<boolean> = this.store.select(getIsLiveVideoAvailable);

    /**
     * Indicates if AOD or VOD is the current content type.
     */
    public isOnDemand$: Observable<boolean> = this.store.select(getIsOnDemand);

    /**
     * Indicates if Podcast is current content type.
     */
    public isIrisPodcast$: Observable<boolean> = this.store.select(getIsIrisPodcast);

    /**
     * The album images for the currently playing media.
     */
    public albumImage$: Observable<string> = this.store.select(getAlbumImage);

    /**
     * The current channel's logo URL stream.
     */
    public channelLogoUrl$: Observable<string> = this.store.select(getChannelLogoUrl);

    /**
     * The current channel's logo URL stream.
     */
    public channelLogoAltText$: Observable<string> = this.store.select(getChannelLogoAltText);


    /**
     * The indicates if the currently playing media is additional internet channel (AIC).
     */
    public isAIC$: Observable<boolean> = this.store.select(getIsAIC);

    /**
     * The indicates if the currently playing media is artist radio (AR or SR).
     */
    public isSeededRadio$: Observable<boolean> = this.store.select(getIsSeededRadio);

    /**
     * The content type of currently playing media.
     */
    public mediaType$: Observable<string> = this.store.select(getMediaType);

    /**
     * The background image that matches the currently playing media.
     */
    public backgroundImage$: Observable<any> = null;

    /**
     * Metadata for the media asset.
     */
    public mediaAssetMetadata$: Observable<IMediaAssetMetadata> = null;

    /**
     * PDT for live video.
     */
    public liveVideoPDT$: Observable<ILiveVideoPDT> = null;

    /**
     * The current timestamp for the video player.
     */
    private videoPlayerTimestamp: number = 0;

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * The current show name stream.
     */
    public showName$: Observable<string> = this.store.select(getShowName);

    /**
     * The current track name stream.
     */
    public trackName$: Observable<string> = this.store.select(getTrackName);

    /**
     * The current artist name stream.
     */
    public artistName$: Observable<string> = this.store.select(getArtistName);

    /**
     * The background color for the currently playing media.
     */
    public backgroundColor$: Observable<string> = this.store.select(getBackgroundColor);

     /**
     * Constructor.
     */
    constructor(private store: Store<IAppStore>,
                private showService: ShowService,
                private nowPlayingStoreService: NowPlayingStoreService,
                private channelListStoreService: ChannelListStoreService,
                public navigationService: NavigationService,
                public dmcaService: DmcaService) {}

    /**
     * Handles component initialization.
     */
    public ngOnInit(): void
    {
        NowPlayingContainer.logger.debug(`ngOnInit()`);

        // Create the background image stream using the current channel stream.
        this.backgroundImage$ = this.channel$.pipe(
            mergeMap((channel: IChannel) =>
            {
                this.channelListStoreService.selectSuperCategoryByChannelId(channel.channelId);
                this.channelListStoreService.selectCategoryByChannelId(channel.channelId);
                return observableOf(this.getBackgroundImage(channel));
            }));

        // Create the media metadata stream using the VOD data.
        this.subscriptions.push(
            observableCombineLatest(this.isVideo$, this.episode$,
                (isVideo, episode) =>
                {
                    this.createMediaAssetMetadata(isVideo, episode);
                })
                .subscribe()
        );

        // Create the program descriptive text for live video.
        this.subscriptions.push(
            observableCombineLatest(this.showName$, this.trackName$, this.artistName$, this.isLiveVideo$,
                (showName, trackName, artistName, isLiveVideo) =>
                {
                    if (!isLiveVideo) return;

                    const liveVideoPDT: ILiveVideoPDT = {
                        showName: showName,
                        trackName: EllipsisStringUtil.ellipsisString(trackName, 47),
                        artistName: artistName
                    };

                    this.liveVideoPDT$ = observableOf(liveVideoPDT);
                })
                .subscribe()
        );
    }

    /**
     * Handles component destruction.
     */
    public ngOnDestroy(): void
    {
        NowPlayingContainer.logger.debug(`ngOnDestroy()`);

        // TODO: BMR: 02/12/2018: Are these 2 lines doing the same thing?
        this.nowPlayingStoreService.leaveNowPlaying(this.videoPlayerTimestamp);

        // TODO: BMR: 02/12/2018: Are these 2 lines doing the same thing?
        this.subscriptions.push(
            observableCombineLatest(this.isVideo$, this.episode$, this.mediaAssetMetadata$,
                (isVideo, episode, mediaAssetMetadata) =>
                {
                    this.selectVideoPlayerData(isVideo, episode, this.videoPlayerTimestamp, mediaAssetMetadata);
                })
                .subscribe()
        );
    }

    /**
     * Saves the current video player data on the now playing store. This allows the mini-player to pick up where
     * the now playing component's video stops.
     * @param {boolean} isVideo will be true if the content we are playing is video (live or vod)
     * @param {IVodEpisode} vodEpisode
     * @param {number} timestamp
     * @param {IMediaAssetMetadata} mediaAssetMetadata
     */
    private selectVideoPlayerData(isVideo: boolean, episode: IMediaEpisode, timestamp: number, mediaAssetMetadata: IMediaAssetMetadata): void
    {
        if (isVideo)
        {
            let videoPlayerData: ISelectVideoPlayerDataActionPayload = {
                videoPlayerData: {
                    mediaAssetMetadata: mediaAssetMetadata,
                    playheadTime: timestamp,
                    vodEpisode: episode
                }
            };
            this.nowPlayingStoreService.selectVideoPlayerData(videoPlayerData);
        }
    }

    /**
     * it returns the background Image. If Image not returned by API then returns default style.
     * @returns {any}
     */
    private createMediaAssetMetadata(isVideo: boolean, episode: IMediaEpisode)
    {
        const mediaAssetMetadata: IMediaAssetMetadata = {
            mediaId: "",
            seriesName: "",
            episodeTitle: "",
            apronSegments: []
        };

        if (isVideo && episode)
        {
            mediaAssetMetadata.seriesName = episode.show ? episode.show.longTitle : "";
            mediaAssetMetadata.episodeTitle = episode.longTitle ? episode.longTitle : "";
        }

        this.mediaAssetMetadata$ = observableOf(mediaAssetMetadata);
    }

    /**
     * it returns the background Image. If Image not returned by API then returns default style.
     * @returns {any}
     */
    private getBackgroundImage(channel: IChannel)
    {
        const imageUrl = ChannelLineupService.getChannelImage(channel,
                                                              NAME_BACKGROUND,
                                                              PLATFORM_WEBEVEREST);

        if (!imageUrl)
        {
            return { "background-image": "radial-gradient(circle at 50% 43%, rgba(6, 64, 100, 0.42), #051923)" };
        }
        return {
            "background-image": `url(${imageUrl}?width=1024&height=768&preserveAspect=true)`,
            "background-size": "cover",
            "background-repeat": "no-repeat"
        };
    }

    /**
     * Handles the video player timestamp updates.
     * @param {number} timestamp
     */
    public onVideoPlayerTimestamp(timestamp: number): void
    {
        this.videoPlayerTimestamp = timestamp;
    }

    /**
     * Selects the show clicked in the component.
     * @param {NowPlayingSelectedShow} selectedShow
     */
    public selectShow(selectedShow: NowPlayingSelectedShow): void
    {
        this.showService.selectShow(selectedShow.channel, selectedShow.show);
    }
}
