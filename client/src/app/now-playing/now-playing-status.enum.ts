export enum ENowPlayingStatus {
    NONE,
    ENTERING,
    ENTERING_FIRST_TIME,
    ENTERING_FROM_MINI_PLAYER,
    LEAVING
}
