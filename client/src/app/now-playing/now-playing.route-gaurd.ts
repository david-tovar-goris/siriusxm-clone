import { timer as observableTimer, combineLatest as observableCombineLatest, Observable } from 'rxjs';
import { switchMap, take, map, tap, skipWhile, first } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import {
    ActivatedRouteSnapshot,
    CanActivate,
    Router,
    RouterStateSnapshot
} from "@angular/router";
import { Store } from "@ngrx/store";
import * as _ from "lodash";
import { InitializationService, InitializationStatusCodes, Logger } from "sxmServices";
import { HideMiniPlayerAndPdt } from "../common/action/now-playing.action";
import { IAppStore } from "../common/store";
import { getChannel, getMediaId } from "../common/store/now-playing.store";
import { SingletonVideoService } from "../video/singleton-video/singleton-video.service";
import { SplashScreenService } from "../common/service/splash-screen/splash-screen.service";
import { appRouteConstants } from "../app.route.constants";

@Injectable()
export class NowPlayingRouteGaurd implements CanActivate
{
    /**
     * Internal logger.
     */
    public static logger: Logger = Logger.getLogger("NowPlayingRouteGaurd");

    /**
     * Constructor.
     */
    constructor(private store: Store<IAppStore>,
                private initializationService: InitializationService,
                private singletonVideoService: SingletonVideoService,
                private splashScreenService: SplashScreenService,
                private router: Router) {}

    /**
     * Determines if the user can navigate to the requested route if they are authenticated.
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {boolean}
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>
    {
        this.singletonVideoService.isNowPlayingRouteResolved = false;

        return (this.initializationService.initState.pipe(
                    skipWhile(initState => initState !== InitializationStatusCodes.RUNNING),
                    tap(() =>{this.store.dispatch(new HideMiniPlayerAndPdt(true));}),
                    switchMap(() => channelLineUpAndNowPlayingDataExists(
                        this.store, this.splashScreenService, this.router, this.singletonVideoService)
                    ),
                    first())) as any as Observable<boolean>;
    }
}

/**
 * Determines if the channel line up data and now playing data available or not.
 * @param {Store<IAppStore>} store
 * @returns {Observable<boolean>}
 */
function channelLineUpAndNowPlayingDataExists(store: Store<IAppStore>,
                                              splashScreenService: SplashScreenService,
                                              router: Router,
                                              singletonVideoService: SingletonVideoService): Observable<boolean>
{
    const obs = observableCombineLatest(store.select(getChannel),store.select(getMediaId),observableTimer(1000,1000));

    return obs.pipe(skipWhile((data: Array<any>) =>
                         {
                             const channel = data[ 0 ];
                             const mediaId = data[ 1 ];
                             const timeout = data[ 2 ];

                             const shouldNotSkip = (timeout > 5) ||  (!!channel || (!!mediaId && mediaId !== ""));
                             return !shouldNotSkip;
                         }),
              take(1),
                  map((data) =>
                   {
                       const channel = data[ 0 ];
                       const mediaId = data[ 1 ];
                       const dataExists = (!!channel || (!!mediaId && mediaId !== ""));

                       singletonVideoService.isNowPlayingRouteResolved = true;

                       if(!dataExists)
                       {
                           NowPlayingRouteGaurd.logger.warn("Data still not existed re routing to Home route  " + dataExists);
                           router.navigate([ appRouteConstants.HOME ], { preserveQueryParams: true });
                           return ;
                       }

                       splashScreenService.closeSplashScreen();
                       return dataExists;
                   }));
}
