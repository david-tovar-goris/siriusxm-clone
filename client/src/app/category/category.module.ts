import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { ChannelListModule } from "../channel-list/channel-list.module";
import { NavigationModule } from "../navigation/navigation.module";
import { TranslationModule } from "../translate/translation.module";
import { CategoryComponent } from "./index";
import { CarouselModule } from "../carousel/carousel.module";
import { SharedModule } from "../common/shared.module";
import { ContextMenuModule } from "../context-menu/context-menu.module";
import { FavoritesModule } from "../favorites/favorites.module";
import { HowardModule } from "./howard/howard.module";
import { AllChannelsModule } from "../all-channels/all-channels.module";
import {ContentTilesModule}  from "../common/component/tiles/content-tiles.module";
import { RouterModule } from "@angular/router";
import { CategoryResolver } from "./category-resolver";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        NavigationModule,
        ChannelListModule,
        TranslationModule,
        CarouselModule,
        ContextMenuModule,
        FavoritesModule,
        SharedModule,
        HowardModule,
        AllChannelsModule,
        ContentTilesModule,
        RouterModule
    ],
    declarations: [
        CategoryComponent
    ],
    exports: [
        CategoryComponent
    ],
    providers: [
        CategoryResolver
    ]
})

export class CategoryModule
{
}
