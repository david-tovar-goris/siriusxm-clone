import { of as observableOf } from "rxjs";

import { InitializationService } from "sxmServices";

import { CategoryResolver } from "./category-resolver";
import { ChannelListServiceMock } from "../../../test/mocks";
import { displayViewType } from "app/channel-list/component/display.view.type";

describe("CategoryResolver", () =>
{
    let categoryResolver: CategoryResolver,
        mockInitService: InitializationService = ({ initState: observableOf("running") }) as any,
        mockChannelService = ChannelListServiceMock.getSpy(),
        carouselStoreServiceMock;

    beforeEach(() =>
    {
        // Create the mock channel lineup testSubject.
        carouselStoreServiceMock = {
            selectSuperCategory : jasmine.createSpy("selectSuperCategory"),
            superCategoryCarousels: { subscribe: jasmine.createSpy('superCatSubscription')},
            subCategoryCarousels: observableOf({})
        };
        categoryResolver = new CategoryResolver(mockInitService, mockChannelService, carouselStoreServiceMock);
    });

    describe("resolve()", () =>
    {
        it("should return the supercategory and subcategory specified in params when the app is initialized", () =>
        {
            const superCat = { key: "music", categoryList: [ { key: "pop" }] },
                  liveChannel = [ { channelId: '1'} ];
            let result;

            mockChannelService.channelStore = observableOf({ superCategories: [ superCat ], liveChannels: [ { channelId: '1'} ] });

            categoryResolver
                .resolve(
                    ({ params: {
                            superCategory: "music",
                            subCategory: "pop",
                            listView: displayViewType.channels,
                            channelId: '1',
                            superCatKey: 'music'  } }) as any, null
                )
                .subscribe(val => result = val);

            expect(result).toEqual({
                superCat,
                subCat: superCat.categoryList[0],
                listView: displayViewType.channels,
                channel: liveChannel[0],
                superCatKey: 'music' });
        });
    });
});
