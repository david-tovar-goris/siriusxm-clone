/**
 * Constants for Category components
 */
export const categoryConsts = {
    HOWARD_STERN: "Howard Stern",
    ON_DEMAND: "ondemand",
    SHOWS:  "shows"
};
