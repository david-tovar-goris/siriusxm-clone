import {NO_ERRORS_SCHEMA}          from "@angular/core";
import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { ActivatedRoute, Router } from "@angular/router";
import { of as observableOf } from "rxjs";
import { CategoryComponent } from "../category/category.component";
import { ChannelListStoreService } from "../common/service/channel-list.store.service";
import { TranslationModule } from "../translate/translation.module";
import { CarouselModule } from "../carousel/carousel.module";
import { ChannelLineupService, SxmAnalyticsService } from "sxmServices";
import { CarouselComponent } from "../carousel/carousel.component";
import {
    FavoriteListStoreServiceMock,
    channelLineupServiceMock, RouterStub
} from "../../../test/index";
import { FavoriteListStoreService } from "../common/service/favorite-list.store.service";
import { ChannelListServiceMock } from "../../../test/mocks/channel-list.service.mock";
import { MockComponent } from "../../../test/mocks/component.mock";
import { NavigationService } from "../common/service/navigation.service";
import { navigationServiceMock } from "../../../test/mocks/navigation.service.mock";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { SharedModule } from "../common/shared.module";
import { ProgressCursorService } from "../common/service/progress-cursor/progress-cursor.service";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Store } from "@ngrx/store";

describe("CategoryComponent", () =>
{
    let component: CategoryComponent,
        carouselStoreServiceMock,
        fixture: ComponentFixture<CategoryComponent>,
        mockActivatedRoute = {
            data: observableOf({ categoryData: { subCat: { name: "Howard Stern", key: "howard"}, superCat: { key: "howard"} } })
        },
        progressCursorService = {
            isSpinning: { subscribe: jasmine.createSpy('isSpinningSubscription') },
            stopSpinning: jasmine.createSpy("stopSpinning"),
            startSpinning: jasmine.createSpy('startSpinning')
        };

    beforeEach(() =>
    {
        // Create the mock channel lineup testSubject.
        carouselStoreServiceMock = {
            selectSuperCategory : jasmine.createSpy("selectSuperCategory"),
            subCategoryCarousels : observableOf({ content: [] }),
            selectSubCategory : jasmine.createSpy("selectSubCategory")
        };

        let sxmAnalyticsServiceMock = {
            updateNowPlayingData: function() {},
            logAnalyticsTag: function() {}
        };

        this.store = {
            select: () => new BehaviorSubject(null)
        };

        TestBed.configureTestingModule({
            imports: [
                CarouselModule,
                TranslationModule,
                SharedModule
            ],
            declarations: [
                CategoryComponent,
                MockComponent({ selector: "channel-list-container", inputs: ["displayView"]}),
                MockComponent({ selector: "howard"}),
                MockComponent({ selector: "[sxm-sticky]", inputs: ["stickyClass"]}),
                MockComponent({ selector: "all-channels-btn"})
            ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: CarouselStoreService, useValue: carouselStoreServiceMock },
                { provide: ChannelLineupService, useValue: channelLineupServiceMock },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: FavoriteListStoreService, useClass: FavoriteListStoreServiceMock },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: ProgressCursorService, useValue: progressCursorService },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: Store, useValue: this.store},
                { provide: Router, useClass: RouterStub}
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        spyOn(CarouselComponent.prototype, 'createCarousel').and.callFake(():any => {});
        fixture = TestBed.createComponent(CategoryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe("ngOnInit()", () =>
    {
        describe("isHoward property", () =>
        {
            describe("when the sub-category is Howard Stern", () =>
            {
                it("should be set to true", () =>
                {
                    expect(component.isHoward).toEqual(true);
                });
            });

        });
    });
});
