import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { ActivatedRoute } from "@angular/router";
import { of as observableOf } from "rxjs";
import { HowardComponent } from "./howard.component";
import { ChannelListStoreService } from "../../common/service/channel-list.store.service";
import { ChannelListServiceMock } from "../../../../test/mocks/channel-list.service.mock";
import { MockComponent } from "../../../../test/mocks/component.mock";
import { TranslationModule } from "../../translate/translation.module";
import { CarouselService } from "sxmServices";
import { CarouselServiceMock } from "../../../../test/index";
import { NavigationService } from "../../common/service/navigation.service";
import { navigationServiceMock } from "../../../../test/mocks/navigation.service.mock";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { ProgressCursorService } from "../../common/service/progress-cursor/progress-cursor.service";

describe("HowardComponent", () =>
{
    let component: HowardComponent,
        carouselStoreServiceMock,
        fixture: ComponentFixture<HowardComponent>,
        mockActivatedRoute = {
            data: observableOf({ categoryData: { subCat: { name: "Howard Stern", key: "howard" }, superCat: { name: "Talk" } } })
        },
        progressCursorService = {
            isSpinning: { subscribe: jasmine.createSpy('isSpinningSubscription') },
            stopSpinning: jasmine.createSpy("stopSpinning"),
            startSpinning: jasmine.createSpy('startSpinning')
        };

    beforeEach(() =>
    {
        // Create the mock channel lineup testSubject.
        carouselStoreServiceMock = {
            selectSuperCategory: jasmine.createSpy("selectSuperCategory"),
            subCategoryCarousels : observableOf({ content: [] }),
            selectSubCategory: jasmine.createSpy("selectSubCategory")
        };

        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [
                HowardComponent,
                MockComponent({ selector: "sxm-howard-content-carousel", inputs: ["carouselData"]}),
                MockComponent({ selector: "sxm-hero-carousel", inputs: ["carouselData"]}),
                MockComponent({ selector: "howard-show-listing"})
            ],
            providers: [
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: CarouselService, useValue: CarouselServiceMock.getSpy() },
                { provide: CarouselStoreService, useValue: carouselStoreServiceMock },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: ProgressCursorService, useValue: progressCursorService }
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(HowardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () =>
    {
        expect(component instanceof HowardComponent).toBe(true);
    });
});
