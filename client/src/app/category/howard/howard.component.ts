import { take } from 'rxjs/operators';
import {
    Component
} from "@angular/core";

import { IChannel, ISubCategory, ISuperCategory } from "sxmServices";
import { IChannelListStore } from "../../common/store/channel-list.store";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { CategoryComponent } from "../category.component";

/**
 * @MODULE:     client
 * @CREATED:    10/23/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     HowardComponent used to load the Enhanced Category Howard Stern page which is unique to other categories
 */

@Component({
    selector: "howard",
    template: `<div *ngIf="carouselStoreService.subCategoryCarousels | async">
                    <div *ngFor="let zone of (carouselStoreService.subCategoryCarousels | async).zone">
                        <sxm-hero-carousel
                            [carouselData]="zone.hero"></sxm-hero-carousel>

                        <sxm-howard-content-carousel class="sxm-border-top"
                                                     [carouselData]="zone.content">
                        </sxm-howard-content-carousel>
                    </div>

                </div>
    `,
    styles: [`
        .sxm-border-top {
            border-top: 1px solid $pale-grey !important;
            padding-top: 17px;
        }
    `]
})

@AutoUnSubscribe()
export class HowardComponent extends CategoryComponent
{
    /**
     * Make sure that the store is setup to use "Howard" as super category (Enhanced Category)
     * @param {ISubCategory} subCat is the subcategory that will be selected in the store
     * @param {ISuperCategory} superCat is the supercategory that will be selected in the store, in this case - "Howard Stern"
     * @param {string} listView is title of view that we will be displaying
     * @param {IChannel} channel is the channel that will be selected in the store
     * @returns {ISuperCategory} the super category -- "Howard Stern" will be returned.
     *
     */
   protected setupStore (subCat: ISubCategory, superCat: ISuperCategory, listView: string, channel: IChannel) : ISuperCategory
   {
       const isHoward = (superCat.name.toLowerCase().indexOf("howard") < 0) && (subCat.name.toLowerCase().indexOf("howard") >= 0);

       this.channelStore.pipe(take(1)).subscribe(((store : IChannelListStore)=>
       {
           const howardSuperCat = store.superCategories.find( supercategory => (supercategory.name.toLowerCase().indexOf("howard") >= 0));

           superCat = (isHoward && howardSuperCat) ? howardSuperCat : superCat;

           return super.setupStore(subCat, superCat, listView, channel,'');
       }));

       return superCat;
   }
}
