import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TranslationModule } from "../../translate/translation.module";
import { CarouselModule } from "../../carousel/carousel.module";
import { SharedModule } from "../../common/shared.module";
import { HowardComponent } from "./howard.component";
import { ContextMenuModule } from "../../context-menu/context-menu.module";
import { FavoritesModule } from "../../favorites/favorites.module";

@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        CarouselModule,
        ContextMenuModule,
        FavoritesModule,
        SharedModule
    ],
    declarations: [
        HowardComponent
    ],
    exports: [
        HowardComponent
    ]
})

export class HowardModule
{
}
