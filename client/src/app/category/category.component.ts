import {
    Component,
    OnInit
} from "@angular/core";

import { ActivatedRoute, NavigationEnd } from "@angular/router";
import { Observable, SubscriptionLike as ISubscription } from "rxjs";
import { filter } from "rxjs/operators";
import {
    ICategory,
    Logger,
    SettingsConstants,
    ISuperCategory,
    ISubCategory,
    IChannel,
    ICarouselSelector,
    neriticActionConstants,
    ICarouselDataByType
} from "sxmServices";
import { displayViewType } from "../channel-list/component/display.view.type";
import { ChannelListStoreService } from "../common/service/channel-list.store.service";
import { IChannelListStore } from "../common/store/channel-list.store";
import { AutoUnSubscribe } from "app/common/decorator";
import { appRouteConstants } from "app/app.route.constants";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { categoryConsts } from "./category.const";
import { NavigationService } from "../common/service/navigation.service";
import { ProgressCursorService } from "../common/service/progress-cursor/progress-cursor.service";
import {
    AnalyticsCarouselNames,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

/**
 * @MODULE:     client
 * @CREATED:    08/01/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     CategoryComponent used to load selectCategory page
 */

@Component({
    templateUrl: "./category.component.html",
    styleUrls: [ "./category.component.scss" ]
})

@AutoUnSubscribe()
export class CategoryComponent implements OnInit
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("CategoryComponent");

    /**
     * Category name
     * @type {string}
     */
    public categoryName: string = "";

    /**
     * The view to be displayed on category page.
     */
    public displayView: displayViewType;

    /**
     * TODO: should remove once Podcast Phase2 live on prod.
     */
    public demandView: string;

    /**
     * The currently selected category.
     */
    public selectedCategory: ICategory;

    /**
     * Currently selected super category
     */
    private selectedSuperCategory: ISuperCategory;

    /**
     * List of channels wrapped in observables from the NGRX channels store. The Observable
     * wrapping will need to be stripped for use in the UI via the async pipe; e.g., "channels | async".
     */
    protected channelStore: Observable<IChannelListStore>;

    /**
     * Holds all the subscriptions and AutoUnSubscribe will disposes when component destroys
     * @type {Array}
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * Holds the flag to detect howard or not
     */
    public isHoward: boolean;

    /**
     * Flag indicates to show footer or not
     * @type {boolean}
     */
    public showFooter: boolean = false;

    public appRouteConstants = appRouteConstants;

    /**
     * Holds the count of number of indexes that should be skipped while navigating back as channels/on-demand tab change is counted
     * as route change
     * @type {number}
     */
    public skipPageCount: number = 1;

    /**
     * Keep the current page url (page-name) so we can switch the listView tabs (channels, shows, etc.)
     * using the navigation service.
     * @type string
     */
    public pageUrl: string;

    /**
     * Indicates no. of selector segments
     */
    public segmentCount: number = 0;

    /**
     * Constructor.
     * @param {ChannelListStoreService} channelListStoreService - Contains a reference to the channel store.
     */
    constructor(private channelListStoreService: ChannelListStoreService,
                public  carouselStoreService: CarouselStoreService,
                private activeRoute: ActivatedRoute,
                private navigationService: NavigationService,
                private progressCursorService: ProgressCursorService)
    {
        this.channelStore = this.channelListStoreService.channelStore;
    }

    /**
     * Subscribes to data changes on the channel store and updates the selected category properties accordingly.
     */
    ngOnInit(): void
    {
        CategoryComponent.logger.debug("ngOnInit()");

        this.subscriptions.push(
            this.activeRoute.data
                .subscribe(({ categoryData: { subCat, superCat, listView, channel, superCatKey , pageUrl} }) =>
                {
                    this.displayView = listView;
                    this.demandView = this.displayView === categoryConsts.SHOWS ? categoryConsts.ON_DEMAND : listView;

                    if (pageUrl)
                    {
                        this.pageUrl = pageUrl;
                        this.carouselStoreService.selectSubCategoryByUrl(pageUrl);
                        this.isHoward = false;
                        return;
                    }
                    this.selectedCategory = subCat;
                    this.selectedSuperCategory = this.setupStore( subCat, superCat, listView, channel, superCatKey);

                    this.categoryName = subCat.name;
                    this.isHoward = this.categoryName === categoryConsts.HOWARD_STERN && listView !== categoryConsts.ON_DEMAND;
                    this.showFooter = SettingsConstants.SHOW_FOOTER && !this.isHoward;
                }),
            this.carouselStoreService.subCategoryCarousels
                .pipe(
                    filter(carousel => !!carousel)
                )
                .subscribe((carousel: ICarouselDataByType) =>
                {
                    this.categoryName = carousel.pageTitle ? carousel.pageTitle.textValue : "";
                    this.progressCursorService.stopSpinning();
                    this.segmentCount = carousel.selectors && carousel.selectors.length > 0
                                        && carousel.selectors[0].segments ? carousel.selectors[0].segments.length : 0;
                    if(this.segmentCount === 1)
                    {
                        const title =  carousel.selectors[0].segments[0].title.toLowerCase() as displayViewType;
                        this.displayView =  title ? title : this.displayView;
                    }
                })
        );
    }

    /**
     * Make sure the store is setup properly for the Category UI
     * @param {ISubCategory} subCat is the subcategory that will be selected in the store
     * @param {ISuperCategory} superCat is the supercategory that will be selected in the store
     * @param {string} listView is title of view that we will be displaying
     * @param {IChannel} channel is the channel that will be selected in the store
     * @returns {ISuperCategory} the super category that is actually selected will be returned.
     */
    protected setupStore (subCat: ISubCategory, superCat: ISuperCategory, listView: string, channel: IChannel, superCatKey: string) : ISuperCategory
    {
        if(!superCat)
        {
            this.carouselStoreService.selectSubCategory(subCat);
            return { key: superCatKey } as ISuperCategory;
        }

        const categoriesAreDifferentOrUndefined = (!this.selectedCategory || !this.selectedSuperCategory)
            || (this.selectedCategory.key !== subCat.key && this.selectedSuperCategory.key !== superCat.key);

        if (categoriesAreDifferentOrUndefined || !channel)
        {
            // Must select superCats first. This avoids RTE in channelListStoreReducer from store.categories being undefined
            this.channelListStoreService.selectSuperCategory(superCat);
            this.channelListStoreService.selectCategory(subCat);
        }

        if (listView === appRouteConstants.CATEGORY_ROUTE_PARAMS.LIST_VIEW.ON_DEMAND && channel)
        {
            this.carouselStoreService.selectSubCategory(subCat, channel.channelId);
        }

        return superCat;
    }

    /**
     * blurFocusedState method this will remove the focused state
     //but the tab index will be preserved
     */
    public blurFocusedState($event)
    {
        $event.target.blur();
    }

    // switch between view tabs
    public switchTabs(cat, view)
    {

        let commands = [cat];

        if (this.pageUrl) commands.push(this.pageUrl);
        if (this.selectedSuperCategory) commands.push(this.selectedSuperCategory.key);
        if (this.selectedCategory) commands.push(this.selectedCategory.key);
        if (view) commands.push(view.toLowerCase());

        if (view !== this.displayView)
        {
            this.skipPageCount ++;
            this.navigationService.go(commands);
        }
    }

    /**
     * For Analytics
     */
    public getCategoryAnaTags(type): {
        tagName: string,
        userPath: string,
        buttonName: string
    }
    {
        switch(type)
        {
            case 'CHANNELS':
                return {
                    tagName: AnalyticsTagNameConstants.CATEGORIES_CHANNELS,
                    userPath: AnalyticsUserPaths.CATEGORIES_CHANNELS,
                    buttonName: AnalyticsTagNameConstants.CATEGORIES_CHANNELS_BUTTON_NAME
                };
            case 'ONDEMAND':
                return {
                    tagName: AnalyticsTagNameConstants.CATEGORIES_ONDEMAND,
                    userPath: AnalyticsUserPaths.CATEGORIES_ONDEMAND,
                    buttonName: AnalyticsTagNameConstants.CATEGORIES_ONDEMAND_BUTTON_NAME
                };
            default:
                return {
                    tagName: '',
                    userPath: '',
                    buttonName: ''
                };
         }
    }
}
