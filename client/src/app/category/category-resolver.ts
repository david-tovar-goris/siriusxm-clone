import { combineLatest, map, first, switchMap, skipWhile } from 'rxjs/operators';
import {
    Resolve,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from "@angular/router";

import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { InitializationService, InitializationStatusCodes, neriticActionConstants } from "sxmServices";
import { ChannelListStoreService } from "app/common/service/channel-list.store.service";
import { CarouselStoreService } from "../common/service/carousel.store.service";

interface ICategoryListingData
{
    superCat?;
    subCat?;
    pageUrl?;
}

@Injectable()
export class CategoryResolver implements Resolve<ICategoryListingData | {}>
{
    constructor(private initService: InitializationService,
                private channelListStoreService: ChannelListStoreService,
                private carouselStoreService: CarouselStoreService)
    {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    {
        return this.initService.initState.pipe(combineLatest(this.carouselStoreService.subCategoryCarousels),
            skipWhile((data) =>
            {
                return data[ 0 ] !== InitializationStatusCodes.RUNNING || data[ 1 ] === null;
            }),
            switchMap(() => resolveCategoryData(this.channelListStoreService, route)),
            first());
    }
}

function resolveCategoryData(cls: ChannelListStoreService, route: ActivatedRouteSnapshot): Observable<ICategoryListingData>
{
    return cls.channelStore.pipe(
                skipWhile(store => store.superCategories.length === 0),
                map(({ superCategories, liveChannels }) =>
                {
                    const url = route.params[ 'url' ];
                    if(url)
                    {
                        const split = url.split(":");
                        return {
                            listView: route.params[ 'listView' ],
                            pageUrl: url
                        };
                    }
                    const superCat = superCategories.find(c => c.key === route.params[ 'superCategory' ]),
                          subCat   = superCat ? superCat.categoryList.find(c => c.key === route.params[ 'subCategory' ]) : { key: route.params[ 'subCategory' ] },
                          channel  = liveChannels.find(c => c.channelId === route.params[ 'channelId' ]);

                    return {
                        superCat,
                        subCat,
                        listView: route.params[ 'listView' ],
                        channel: channel,
                        superCatKey: route.params[ 'superCategory' ]
                    };
                }));
}
