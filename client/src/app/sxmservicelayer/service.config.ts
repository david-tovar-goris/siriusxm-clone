import { BrowserUtil } from "app/common/util/browser.util";
import {
    DeviceInfo,
    IAppConfig,
    IContextualInfo,
    DeepLinkTypes,
    MediaPlayerConstants,
    StorageKeyConstant,
    ApiLayerTypes
} from "sxmServices";
import { UUIDUtil } from "app/common/util/uuid.util";
import { TranslateConstants } from "app/translate/translate.constants";

// NOTE: The following 3 properties come from the build. See `client/webpack.common.js` for details.
declare const sxmAppVersion : string;
declare const sxmGitHashNumber : string;
declare const sxmTeamCityBuildNumber : string;

export function getServiceConfig() : IAppConfig
{
    const serviceConfig : IAppConfig = {
        apiEndpoint           : BrowserUtil.getApiEndpoint(),
        domainName            : BrowserUtil.getDomainName(),
        allProfilesData       : undefined, // gets filled in from http service
        appId                 : "EverestWebClient",
        resultTemplate        : "web",
        adsWizzSupported      : true,
        clientConfiguration   : undefined, // gets filled in from http service
        inPrivateBrowsingMode : null, // gets filled in later depending on type of config
        restart               : BrowserUtil.reload,
        deviceInfo            : getDeviceInfo(),
        contextualInfo        : getContextualInfo(),
        defaultSuperCategory    : null, // gets filled in when channel line up returns data.
        loginRequired : false,
        uniqueSessionId : UUIDUtil.generate(),
        isFreeTierEnable: BrowserUtil.getAppRegion() !== ApiLayerTypes.REGION_CA,  // Updating status again in config service.
        initialPathname : BrowserUtil.getBrowserPathname()
    };

    return serviceConfig;
}

/**
 * Return an object that contains all the device specific information needed within the app
 *
 * @returns {DeviceInfo} object that has device information that is either needed or set by the SiriusXM Everest API
 **/
function getDeviceInfo() : DeviceInfo
{
    // The full app version that is required by the API, especially for BI consumption reporting, is both the
    // app version number and the team city build number concatenated together for a "." separator.
    const appVersionAndBuildNumber : string = `${sxmAppVersion}.${sxmTeamCityBuildNumber}`;
    const appRegion:string = BrowserUtil.getAppRegion();

    const urlParam           = location.pathname.split("/")[1];
    const langPref           = TranslateConstants.LANGUAGELIST.indexOf(urlParam) !== -1 ? urlParam : null;

    //TODO : Cannot use the StorageService here as it has to be injected and cannot be used
    if(langPref && appRegion === 'CA' )
    {
        localStorage.setItem(StorageKeyConstant.LANGUAGE, langPref);
    }

    const language = localStorage.getItem(StorageKeyConstant.LANGUAGE) ? localStorage.getItem(StorageKeyConstant.LANGUAGE) : 'en';

    const apiEndPoint = BrowserUtil.getApiEndpoint();

    const clientCapabilities = [
        "enhancedEDP",
        "seededRadio",
        "tabSortOrder",
        "zones",
        "cpColorBackground",
        "additionalVideo",
        "podcast",
        "irisPodcast"
    ];

    return {
        appRegion              : appRegion,
        language               : language,
        browser                : BrowserUtil.getBrowserName(),
        browserVersion         : BrowserUtil.getBrowserVersion(),
        clientCapabilities     : clientCapabilities,
        clientDeviceId         : null, // gets filled in by the Everest API
        clientDeviceType       : "web",
        deviceModel            : "EverestWebClient",
        osVersion              : BrowserUtil.getOSName(),
        platform               : "Web",
        player                 : getPlayerType(),
        sxmAppVersion          : appVersionAndBuildNumber,
        sxmGitHashNumber       : sxmGitHashNumber.trim(),
        sxmTeamCityBuildNumber : sxmTeamCityBuildNumber,
        isChromeBrowser        : BrowserUtil.isChrome(),
        isMobile               : BrowserUtil.isMobile(),
        isNative               : BrowserUtil.isNative(),
        supportsAddlChannels   : true,
        supportsVideoSdkAnalytics : true
    };
}

/**
 * @returns {string} the playback type, which is either flash (IE11) or html5 (everything else)
 */
function getPlayerType(): string
{
    return MediaPlayerConstants.HTML5_PLAYER;
}

/**
 * Returns information about the execution context that is needed by the app and the sxm service layer code
 *
 * @returns {IContextualInfo} execution context information for the currently running instance of the app
 */
function getContextualInfo() : IContextualInfo
{
    const type = BrowserUtil.getQueryParameterValue( "type" );
    const id   = BrowserUtil.getQueryParameterValue( "id" );

    return {
        userAgent   : navigator.userAgent || navigator.vendor,
        queryString : window.location.search.substring( 1 ),
        host        : window.location.host,
        hostName    : window.location.hostname,
        protocol    : "https:",
        type        : type,
        id          : id.replace(/%20/g, " "),
        deepLink    : (type && id) || (type === DeepLinkTypes.VIDEOS || type === DeepLinkTypes.PODCASTS) ? true : false
    };
}
