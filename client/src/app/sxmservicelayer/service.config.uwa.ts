import { IAppConfig, inDev, inQA, inBeta, inCompanyBeta, ApiLayerTypes } from "sxmServices";
import { BrowserUtil }         from "../common/util/browser.util";
import { PrivateBrowsingUtil } from "../common/util/privateBrowsing.util";
import { UUIDUtil } from "app/common/util/uuid.util";

declare const sxmAppVersion : string;
declare const sxmGitHashNumber : string;
declare const sxmTeamCityBuildNumber : string;

export function getUWAServiceConfig()
{
    const apiEndPoint = BrowserUtil.getApiEndpoint();
    const clientCapabilities = ["enhancedEDP", "seededRadio", "tabSortOrder", "zones", "cpColorBackground", "additionalVideo", "podcast", "irisPodcast"];
    const uwaServiceConfig : IAppConfig = {
        apiEndpoint           : BrowserUtil.getApiEndpoint(),
        domainName            : BrowserUtil.getDomainName(),
        allProfilesData       : undefined, // gets filled in from http service
        appId                 : "EverestUWAClient",
        clientConfiguration   : undefined, // gets filled in from http service
        resultTemplate        : "windows",
        adsWizzSupported      : true,
        inPrivateBrowsingMode : PrivateBrowsingUtil.checkPrivateBrowsingMode( window ) as any,
        restart               : BrowserUtil.reload,
        deviceInfo            : {
            appRegion              : "US",
            language               : "en",
            browser                : "Chrome",
            browserVersion         : "54",
            clientCapabilities     : clientCapabilities,
            clientDeviceId         : null,
            clientDeviceType       : "web",
            deviceModel            : "EverestWebClient",
            osVersion              : "Windows",
            platform               : "Web",
            player                 : "html5",
            sxmAppVersion          : sxmAppVersion,
            sxmGitHashNumber       : sxmGitHashNumber,
            sxmTeamCityBuildNumber : sxmTeamCityBuildNumber,
            isChromeBrowser        : BrowserUtil.isChrome(),
            isMobile: BrowserUtil.isMobile(),
            isNative: BrowserUtil.isNative(),
            supportsAddlChannels   : true,
            supportsVideoSdkAnalytics : true
        },
        contextualInfo        : {
            userAgent   : navigator.userAgent || navigator.vendor,
            queryString : window.location.search.substring( 1 ),
            host        : window.location.host,
            hostName    : window.location.hostname,
            protocol    : window.location.protocol
        },
        defaultSuperCategory : null,
        loginRequired : false,
        uniqueSessionId : UUIDUtil.generate(),
        isFreeTierEnable: BrowserUtil.getAppRegion() !== ApiLayerTypes.REGION_CA,
        initialPathname: BrowserUtil.getBrowserPathname()
    };

    return uwaServiceConfig;
}

