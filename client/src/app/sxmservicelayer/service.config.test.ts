import { of as observableOf } from "rxjs";
import { IAppConfig }       from "sxmServices";
import { getServiceConfig } from "app/sxmservicelayer/service.config";

export function getTestServiceConfig() : IAppConfig
{
    const testServiceConfig = getServiceConfig();

    testServiceConfig.inPrivateBrowsingMode = observableOf(false) as any;

    return testServiceConfig;
}
