import {
    InjectionToken,
    NgModule
} from "@angular/core";

import {
    IAppConfig,
    injectAppConfig,
    injectAudioPlayer,
    injectVideoPlayer, Logger,
    providers
} from "sxmServices";

import { getUWAServiceConfig } from "./service.config.uwa";
import { getWebServiceConfig } from "./service.config.web";

import * as AudioPlayer from "sxm-audio-player";
import WebVideoPlayer  from "sxm-video-player";

declare const Windows : any;

export const APP_CONFIG   = new InjectionToken<IAppConfig>( "app.config" );
export const AUDIO_PLAYER = new InjectionToken<IAppConfig>( "app.audioplayer" );
export const VIDEO_PLAYER = new InjectionToken<IAppConfig>( "app.videoplayer" );
let isWindows           = false;

const videoPlayer = new WebVideoPlayer();

try { if (Windows) { isWindows = true; } }
catch (e) {}

let serviceConfigProvider;

if (isWindows === true) { serviceConfigProvider = { provide : APP_CONFIG, useValue : getUWAServiceConfig() }; }
else { serviceConfigProvider = { provide : APP_CONFIG, useValue : getWebServiceConfig() }; }

const audioPlayer = new AudioPlayer({
    logger: Logger.getLogger('sxm-audio-player')
});
const audioPlayerProvider = { provide: AUDIO_PLAYER, useValue: audioPlayer };
const videoPlayerProvider = { provide: VIDEO_PLAYER, useValue: videoPlayer };

injectAppConfig( serviceConfigProvider );
injectAudioPlayer( audioPlayerProvider );
injectVideoPlayer( videoPlayerProvider );

@NgModule( { providers : providers } )
export class SXMServiceLayerModule {}

