import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { MockComponent, MockDirective } from "../../../test/mocks/component.mock";
import { SegmentsServiceMock } from "../../../test/mocks/segments.service.mock";
import { SegmentsService } from "../media/segments/segments.service";

import { TranslationModule } from "../translate/translation.module";
import { ApronSegmentsComponent } from "./apron-segments.component";
import { EventEmitter,
         ElementRef
} from "@angular/core";
import { ContentTypes, CurrentlyPlayingService, IApronSegment, MediaPlayerService } from "sxmServices";
import { apronSegmentStoreMock } from "../../../test/mocks/data/apron-segment-store.mock";
import { MediaPlayerServiceMock } from "../../../test/mocks/media-player.service.mock";
import { NowPlayingIndicatorService } from "../common/service/now-playing-indicator/now-playing-indicator.service";
import { NowPlayingIndicatorServiceMock } from "../../../test/mocks/now-playing-indicator.service.mock";
import { OrderByPipe } from "../common/pipe/order-by.pipe";
import { CurrentlyPlayingServiceMock } from "../../../test/mocks/currently-playing.service.mock";


describe("ApronSegmentsComponent", function()
{
    let component: ApronSegmentsComponent,
        fixture: ComponentFixture<ApronSegmentsComponent>,
        segmentsService: SegmentsService;

    beforeEach(function()
    {
        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [
                ApronSegmentsComponent,
                OrderByPipe,
                MockComponent({
                    selector: "apron-segments"
                }),
                MockComponent({ selector: "[sxm-analytics]", inputs: ["analyticProps"]}),
                MockDirective({ selector: "[sxm-new-analytics]", inputs: ["tagName", "screen", "userPath",
                        "pageFrame", "elementType", "buttonName", "elementPosition", "action", "inputType",
                        "userPathScreenPosition", "modal", "tagText", "actionSource", "carouselName", "findingMethod",
                        "tileType", "tileContentType"]})
            ],
            providers: [
                { provide: SegmentsService, useClass: SegmentsServiceMock },
                { provide: MediaPlayerService, useClass: MediaPlayerServiceMock },
                { provide: CurrentlyPlayingService, useClass: CurrentlyPlayingServiceMock },
                { provide: NowPlayingIndicatorService, useClass: NowPlayingIndicatorServiceMock }
            ]
        }).compileComponents();

        segmentsService = TestBed.get(SegmentsService);

    });

    beforeEach(function()
    {
        fixture = TestBed.createComponent(ApronSegmentsComponent);
        component = fixture.componentInstance;

        // add component inputs / members here if necessary
        component.playSegment = new EventEmitter<IApronSegment>();

        fixture.detectChanges();
    });

    afterEach(function()
    {
        component = null;
        fixture.destroy();
    });

    it("Should exist", function()
    {
        expect(component).toBeTruthy();
    });

    describe("onClickApronRow when contentType live",  function()
    {
        it("Should emit an ApronSegment", function()
        {
            const callee = apronSegmentStoreMock.segments[1];
            spyOn(component.playSegment, "emit");

            component.onClickApronRow(callee);
            expect(component.playSegment.emit).toHaveBeenCalled();
            expect(component.playSegment.emit).toHaveBeenCalledWith(callee);
        });
    });

    describe("onClickApronRow when contentType vlive", () =>
    {
        it("Should emit an switchPlayback event", () =>
        {
            const callee = apronSegmentStoreMock.segments[1];
            component.mediaType=ContentTypes.LIVE_VIDEO;
            spyOn(component.switchPlayback, "emit");

            component.onClickApronRow(callee);
            expect(component.switchPlayback.emit).toHaveBeenCalled();
            expect(component.switchPlayback.emit).toHaveBeenCalledWith(callee);
        });
    });

    describe("playVideo when contentType live", () =>
    {
        it("Should emit an playVideoMarker event", () =>
        {
            const callee = apronSegmentStoreMock.segments[1];
            component.mediaType=ContentTypes.LIVE_AUDIO;
            spyOn(component.playVideoMarker, "emit");

            component.onClickApronRowVideoIcon(callee);
            expect(component.playVideoMarker.emit).toHaveBeenCalled();
            expect(component.playVideoMarker.emit).toHaveBeenCalledWith(callee);
        });
    });

    it("should return indicator image", function()
    {
         expect(component.getIndicatorImage()).toBe("../../assets/images/currently-playing.gif");
    });

    it("should get Segment title", function()
    {
        let spy = spyOn(component, "getSegmentTitle").and.callThrough();
        let segment = {
            timeDisplay: "time"
        } as IApronSegment;
        component.getSegmentTitle(segment);
        expect(spy).toHaveBeenCalled();

    });

    it("should get status of video marker played", function()
    {
        let segment = {
            timeDisplay: "time"
        } as IApronSegment;

        expect(component.isSegmentVideoMarkerBeingPlayed(segment)).toBeFalsy();
    });

    it("should called Order Segment", function()
    {
        let spy = spyOn(component, "orderedSegments").and.callThrough();
        component.orderedSegments();
        expect(spy).toHaveBeenCalled();
    });

    it("should set segment list wrapper", function()
    {
        component.segmentsListWrapper = {
            nativeElement: {
                querySelector: function()
                {
                    return {
                        offsetTop: 200,
                        clientHeight: 200,
                        scrollTop: 200
                    };
                }
            },
            offsetTop: 200,
            clientHeight: 200,
            scrollTop: 200
        } as ElementRef;
        let spy = spyOn(component, "setSegmentListWrapperScrollPosition").and.callThrough();
        component.setSegmentListWrapperScrollPosition();
        expect(spy).toHaveBeenCalled();
    });

    it("should Focus apron row", function()
    {
        let spy = spyOn(component, "onFocusApronRow").and.callThrough();
        component.onFocusApronRow(2);
        expect(spy).toHaveBeenCalledWith(2);
    });
});
