import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MediaModule } from "../media/media.module";
import { TranslationModule } from "../translate/translation.module";
import { ApronSegmentsComponent } from "./apron-segments.component";
import { ApronSegmentsContainer } from "./container/apron-segments.container";
import { SharedModule } from "../common/shared.module";

export const COMPONENTS = [
    ApronSegmentsComponent,
    ApronSegmentsContainer
];

@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        MediaModule,
        SharedModule
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
    providers: []
})

export class ApronSegmentsModule
{
}
