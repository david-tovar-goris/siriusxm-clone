import {
    Component,
    EventEmitter,
    Input,
    Output,
    AfterViewInit,
    ViewChild,
    ElementRef,
    OnDestroy
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { IApronSegment, ContentTypes, MediaPlayerService, CurrentlyPlayingService } from "sxmServices";
import { AutoUnSubscribe } from "../common/decorator";
import { SegmentsService } from "../media/segments/segments.service";
import { NowPlayingIndicatorService } from "../common/service/now-playing-indicator/now-playing-indicator.service";
import * as _ from "lodash";
import { FocusUtil } from "../common/util/focus.util";
import { SubscriptionLike as ISubscription } from "rxjs/internal/types";
import {
    AnalyticsCarouselNames,
    AnalyticsContentTypes,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsModals,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@AutoUnSubscribe()
@Component({
    selector: "sxm-apron-segments",
    templateUrl: "./apron-segments.component.html",
    styleUrls: [ "./apron-segments.component.scss" ]
})
export class ApronSegmentsComponent implements AfterViewInit, OnDestroy
{
    public rowsAreHidden: boolean = false;

    /**
     * Holds all the subscriptions and AutoUnSubscribe will disposes when component destroys
     * @type {Array}
     */
    private subscriptions: Array<ISubscription> = [];

    /////////////////////////////////////////////////////////////////////////////////////
    // Inputs
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Indicates if segments should show in the UI.
     */
    @Input()
    public displaySegments: boolean;

    /**
     * Flag indicating if the list of segments can expand and collapse.
     */
    @Input()
    public canExpandSegmentsView: boolean;

    /**
     * The list of segments for the current episode.
     */
    @Input()
    public segments: IApronSegment[];

    /**
     * The currently playing segments in the episode.
     */
    @Input()
    public currentlyPlayingSegment: IApronSegment;

    /**
     * A boolean that is updated to whether or not user is listening behind the live point.
     */
    @Input()
    public isBehindLivePoint: boolean = true;

     /**
     * Indicates if the user is playing video in which case the video icon will show.
     */
    @Input()
    public isVideo: boolean;

    /**
     * Indicates which media user is playing
     */
    @Input()
    public mediaType: string;

    /**
     * Indicates if the user is playing live channel or on demand.
     */
    @Input()
    public isOnDemandMediaType: boolean;

    @Input() content: {};

    /////////////////////////////////////////////////////////////////////////////////////
    // Outputs
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Broadcast when the user clicks the previous button.
     */
    @Output()
    public playSegment = new EventEmitter<IApronSegment>();

    /**
     * Broadcasts when the user clicks video icon
     */
    @Output()
    public playVideoMarker = new EventEmitter<IApronSegment>();

    /**
     * Broadcasts when the user clicks any where on apron row other than video icon
     */
    @Output()
    public switchPlayback = new EventEmitter<IApronSegment>();

    /////////////////////////////////////////////////////////////////////////////////////
    // Public Variables
    /////////////////////////////////////////////////////////////////////////////////////


    public currentlyFocusedIndex: number = FocusUtil.DEFAULT_FOCUSED_INDEX;

    private mutationObserver: MutationObserver;

    // Analytics Constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsModals = AnalyticsModals;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsContentTypes = AnalyticsContentTypes;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;

    /**
     * Element reference to the segments list Parent Element
     */
    @ViewChild('segmentsListWrapper') segmentsListWrapper: ElementRef;

    /**
     * Constructor.
     * @param {SegmentsService} segmentsService
     * @param {NowPlayingAnalyticsService} nowPlayingAnalyticsService
     */
    constructor(public segmentsService: SegmentsService,
                public mediaPlayerService: MediaPlayerService,
                private currentlyPlayingService: CurrentlyPlayingService,
                private translate: TranslateService,
                public nowPlayingIndicatorService: NowPlayingIndicatorService){}

    ngAfterViewInit(): void
    {
        const config = { attributes: true, childList: true, subtree: true, attributeOldValue: true, attributeFilter: ['class']};
        this.mutationObserver = new MutationObserver((mutationsList) =>
        {
            if(mutationsList.filter((mutationRecord) => mutationRecord.oldValue === 'segment-container current-segment-row').length)
            {
                this.setSegmentListWrapperScrollPosition();
            }
        });
        this.segmentsListWrapper && this.mutationObserver.observe(this.segmentsListWrapper.nativeElement, config);
        this.segmentsListWrapper && this.setSegmentListWrapperScrollPosition();
    }

    /**
     * Sets Scroll position of segments-list-wrapper so that currently playing segment is visible on screen
     * Should be called after a delay as the dom should be updated before setting the scroll position
     */
    setSegmentListWrapperScrollPosition()
    {
        const segmentsListWrapper = this.segmentsListWrapper.nativeElement;
        const segmentToBeVisible = segmentsListWrapper.querySelector(".current-segment-row");
        if(segmentToBeVisible)
        {
            const overTop = (segmentToBeVisible.offsetTop - segmentsListWrapper.offsetTop) < segmentsListWrapper.scrollTop;
            const overBottom = (segmentToBeVisible.offsetTop - segmentsListWrapper.offsetTop + segmentToBeVisible.clientHeight) >
                (segmentsListWrapper.scrollTop + segmentsListWrapper.clientHeight);
            if (overTop)
            {
                segmentsListWrapper.scrollTop = segmentToBeVisible.offsetTop - segmentsListWrapper.offsetTop - 2;
            }
            if (overBottom)
            {
                segmentsListWrapper.scrollTop = segmentToBeVisible.offsetTop - segmentsListWrapper.offsetTop -
                    segmentsListWrapper.clientHeight + segmentToBeVisible.clientHeight + 2;
            }
        }
    }

    /**
     * Broadcasts the event to play the segment clicked.
     */
    public onClickApronRow(segment: IApronSegment): void
    {
        this.isBehindLivePoint = true;

        (this.mediaType === ContentTypes.LIVE_VIDEO) ? this.switchPlayback.emit(segment) :this.playSegment.emit(segment);
    }

    /**
     * On focus event, set the currently focused index.
     */
    public onFocusApronRow(index: number)
    {
        this.currentlyFocusedIndex = index;
    }

    /**
     * On blur event, reset the currently focused index.
     * According to the W3C spec, the blur event is guaranteed to fire
     * before the focus event on an adjacent element.
     */
    public onBlurApronRow()
    {
        this.currentlyFocusedIndex = FocusUtil.DEFAULT_FOCUSED_INDEX;
    }


    /**
     * Switch to video if we are not already
     */
    public onClickApronRowVideoIcon(segment)
    {
        if (this.mediaType === ContentTypes.LIVE_VIDEO && this.isSegmentVideoMarkerBeingPlayed(segment)) return;

        this.playVideoMarker.emit(segment);
    }

    /**
     * Returns now playing indicator string based on whether the media player is paused or not.
     * @returns string
     */
    public getIndicatorImage(): string
    {
        return this.mediaPlayerService.isPlaying() ? "../../assets/images/currently-playing.gif" : "../../assets/images/paused.png";
    }

    public getSegmentTitle(segment: IApronSegment): string
    {
        const titleHeader = this.translate.instant('apronSegments.tune'),
              titleTime = this.currentlyPlayingSegment === segment && !this.isBehindLivePoint ?
                            this.translate.instant('apronSegments.live') :
                            segment.timeDisplay;

        return `${titleHeader} ${segment.title} ${titleTime}`;
    }

    /**
     * Returns whether video marker for the given segment is being played
     * @param segment
     * @returns boolean
     */
    public isSegmentVideoMarkerBeingPlayed(segment) : boolean
    {
        return this.isVideo && this.currentlyPlayingSegment === segment;
    }

    trackByFn(index, segment)
    {
        return index;
    }

    /**
     * Set segments Order based on whether the channel is live or not.
     * NOTE: This Ordering needed for LIVE channels because LIVE text is updating on HTML. Due to this not able
     * to do ordering in service.
     * TODO - VPaindla - The LIVE Text logic need to move to service.
     */
    public orderedSegments()
    {
        return _.orderBy(this.segments, this.isOnDemandMediaType ? "" : "mediaSegment.times.zuluStartTime", ['desc']);
    }

    public ngOnDestroy()
    {
        this.mutationObserver.disconnect();
    }
}
