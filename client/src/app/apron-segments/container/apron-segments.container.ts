import { first, merge } from 'rxjs/operators';
import { Component, Input } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable }                            from "rxjs";
import {
    MediaUtil,
    CurrentlyPlayingService,
    IApronSegment,
    IMediaSegment,
    MediaTimestampService,
    SeekService
}                                                from "sxmServices";
import { SelectCurrentlyPlayingSegment }         from "../../common/action/apron-segment.action";
import { IAppStore }                             from "../../common/store/app.store";
import {
    selectCanExpandSegmentsView,
    selectCurrentlyPlayingSegment,
    selectSegments,
    selectDisplaySegments
}                                                from "../../common/store/apron-segment.store";
import {getIsVideo, getIsOnDemand, getMediaType} from "../../common/store/now-playing.store";
import { SegmentsService }                       from "../../media/segments/segments.service";
import { TuneClientService }                     from "../../common/service/tune/tune.client.service";
import { UpdateLastPlayhead }                    from "../../common/action/now-playing.action";
import { PlayerControlsService }                 from "../../player-controls/player-controls.service";

@Component({
    selector: "sxm-apron-segments-container",
    templateUrl: "./apron-segments.container.html"
})
export class ApronSegmentsContainer
{
    @Input() content: {};
    /**
     * List of episode segments that are consumed and used by the UI.
     */
    public segments$: Observable<IApronSegment[]> = this.store.select(selectSegments);

    /**
     * Flag indicating if the list of segments should show in the UI stream.
     */
    public displaySegments$: Observable<boolean> = this.store.select(selectDisplaySegments);

    /**
     * Flag indicating if the list of segments can expand and collapse.
     */
    public canExpandSegmentsView$: Observable<boolean> = this.store.select(selectCanExpandSegmentsView);

    /**
     * The currently playing segment stream.
     */
    public currentlyPlayingSegment$: Observable<IApronSegment> = this.store.select(selectCurrentlyPlayingSegment);

    /**
     * The boolean stream of whether or not audio is being listened to at the live point.
     */
    public isBehindLivePoint$: Observable<boolean>;

    /**
     * Observable string indicating the media type is video or not.
     */
    public isVideo$: Observable<boolean> = this.store.select(getIsVideo);

    /**
     * Observable string indicating the media type.
     */
    public mediaType: Observable<string> = this.store.select(getMediaType);

    /**
     * Observable string indicating the media type is live or on demand.
     */
    public isOnDemandMediaType$: Observable<boolean> = this.store.select(getIsOnDemand);

    /**
     * Constructor.
     */
    constructor(private store: Store<IAppStore>,
                private segmentsService: SegmentsService,
                private currentlyPlayingService: CurrentlyPlayingService,
                private tuneClientService: TuneClientService,
                private mediaTimestampService: MediaTimestampService,
                private seekService: SeekService,
                private playerControlsService : PlayerControlsService)
    {
        this.isBehindLivePoint$ = this.segmentsService.isBehindLivePointObs().pipe(merge(this.playerControlsService.isRestartButtonClicked$));
    }

    /**
     * Plays the requested segment.
     * @param {IMediaSegment} segment
     */
    public playSegment(segment: IApronSegment): void
    {
        // Apply eager selection of the currently playing segment...if it's not right itll
        // get fixed very quickly.
        this.store.dispatch(new SelectCurrentlyPlayingSegment(segment));

        // Start playback at the start of the requested segment.
        this.seekService.seekThenPlay(segment.mediaSegment.times.zuluStartTime);
    }

    /**
     * Plays the requested video marker for the segment if playing audio else seek to marker start if already playing video.
     * @param {IMediaSegment} segment
     */
    public playVideoMarker(segment: IApronSegment): void
    {
        this.store.dispatch(new SelectCurrentlyPlayingSegment(segment));

        this.currentlyPlayingService.currentlyPlayingData.pipe(
            first())
            .subscribe(currentMedia =>
            {
                if (MediaUtil.isAudioMediaType(currentMedia.mediaType))
                {
                    this.tuneClientService.switchPlayback(segment.videoStartTime);
                }
                else
                {
                    this.seekService.seek(segment.videoStartTime);
                }
            });

    }

    /**
     * toggles playback between audio & video
     * @param {IMediaSegment} segment
     */
    public switchPlayback(segment: IApronSegment): void
    {
        this.store.dispatch(new UpdateLastPlayhead(this.mediaTimestampService.playheadTimestamp));
        this.tuneClientService.switchPlayback(segment.mediaSegment.times.zuluStartTime);
    }
}
