import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { SearchDropdownComponent } from './search-dropdown.component';
import { DebugElement, NO_ERRORS_SCHEMA } from "@angular/core";
import { of as observableOf } from "rxjs";
import {
    AppMonitorService,
    ChromecastModel,
    ClientFault,
    CurrentlyPlayingService,
    MediaTimeLineService,
    RefreshTracksService,
    SearchService,
    SettingsService,
    TuneService,
    MediaPlayerService
} from "sxmServices";
import { TranslationModule } from "../../translate/translation.module";
import { SearchServiceMock } from "../../../../test/mocks/search/search.service.mock";
import { appRouteConstants } from "../../app.route.constants";
import { ChannelListStoreService } from "../../common/service/channel-list.store.service";
import { ChannelListServiceMock } from "../../../../test/mocks/channel-list.service.mock";
import { NavigationService } from '../../common/service/navigation.service';
import { navigationServiceMock } from "../../../../test/mocks/navigation.service.mock";
import { TuneClientService } from "../../common/service/tune/tune.client.service";
import { NowPlayingStoreService } from "../../common/service/now-playing.store.service";
import { NowPlayingStoreServiceMock } from "../../../../test/mocks/now-playing.store.service.mock";
import { MediaPlayerServiceMock } from "../../../../test/mocks/media-player.service.mock";
import { SettingsServiceMock } from "../../../../test/mocks/settings.service.mock";
import { SeekServiceMock } from "../../../../test/mocks/seek.service.mock";
import { ModalService } from "../../common/service/modal/modal.service";
import { ModalServiceMock } from "../../../../test/mocks/modal.service.mock";
import { TuneServiceMock } from "../../../../test/mocks/tune.service.mock";
import { SeekService, DmcaService } from "sxmServices";
import { MockDirective } from "../../../../test/mocks/component.mock";

// TODO 2018/05/11 - These test are not running
describe('SearchDropdownComponent', () =>
{
    let component: SearchDropdownComponent;
    let fixture: ComponentFixture<SearchDropdownComponent>;
    let debugElement: DebugElement,
        tuneClientService,
        channel = SearchServiceMock.directTuneSearchResults[ 0 ],
        currentlyPlayingService  = {
        getCurrentEpisodeZuluStartTime: jasmine.createSpy('getCurrentEpisodeZuluStartTime').and.returnValue(0),
            triggerCurrentlyPlayingData: () => {},
            isTunedTo: () => false
        },
        refreshTracksService = {
            setTuneInProgress: () => false
        },
        mockMediaPlayerService   = new MediaPlayerServiceMock(),
        appMonitorService = {
            faultStream: observableOf({} as ClientFault)
        },
        dmcaService = {
            isUnrestricted: jasmine.createSpy('isUnrestricted'),
            isRestricted: jasmine.createSpy('isRestricted'),
            isDisallowed: jasmine.createSpy('isDisallowed')
        },
        chromecastModel = {
            isDeviceVideoSupported: false,
            state$: {
                getValue: function() {}
            }
        } as ChromecastModel;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [ SearchDropdownComponent,
                MockDirective({ selector: "[sxm-new-analytics]", inputs: ["tagName", "userPath",
                        "pageFrame", "carouselName", "buttonName", "actionSource",
                        "elementPosition", "elementType", "action", "inputType","findingMethod",
                        "searchTerm", "tileContentType", "skipCurrentPosition"]})],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: TuneClientService, useClass : TuneClientService },
                { provide: TuneService, useValue: TuneServiceMock },
                { provide: RefreshTracksService, useValue : refreshTracksService },
                { provide: CurrentlyPlayingService, useValue : currentlyPlayingService },
                { provide: NowPlayingStoreService, useClass: NowPlayingStoreServiceMock },
                { provide: MediaPlayerService, useValue : mockMediaPlayerService },
                { provide: SettingsService, useValue: SettingsServiceMock.getSpy() },
                { provide: SeekService, useClass: SeekServiceMock },
                { provide: MediaTimeLineService, useValue: {} },
                { provide: AppMonitorService, useValue: appMonitorService },
                { provide: ChromecastModel, useValue: chromecastModel },
                { provide: ModalService, useValue: ModalServiceMock},
                { provide: SearchService, useValue: SearchServiceMock.getSpy() },
                { provide: DmcaService, useValue: dmcaService }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(SearchDropdownComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        fixture.detectChanges();
        tuneClientService = TestBed.get(TuneClientService);
    });

    describe('Infrastructure', () =>
    {
        it('should create', () =>
        {
            expect(component).toBeTruthy();
        });

    });

    describe('SearchDropdown component Execution', () =>
    {

        it('Clear the Recent History success', () =>
        {
            SearchServiceMock.getSpy().clearSearchResults.and.returnValue(observableOf(true));
            component.clearSearchHistory();
            expect(SearchServiceMock.getSpy().clearSearchResults).toHaveBeenCalled();
        });

        it('Log Warning on Failure to clear History', () =>
        {
            SearchServiceMock.getSpy().clearSearchResults.and.returnValue(observableOf(false));
            component.clearSearchHistory();
            expect(SearchServiceMock.getSpy().clearSearchResults).toHaveBeenCalled();
        });

        it("should navigate to now playing page on click of tune results", () =>
        {
            spyOn(tuneClientService, "tune");
            component.goToNowPlayingPage(channel);
            expect(tuneClientService.tune).toHaveBeenCalledWith({ channel: channel, channelId: channel.channelId, contentType: channel.type });

        });

        it("Should navigate to Search results screen",
            inject([ NavigationService ], (navigationService: NavigationService) =>
        {
            spyOn(navigationService, "go");
            component.getSearchResults('pop');
            expect(navigationService.go).toHaveBeenCalledWith([ `${appRouteConstants.SEARCH}/pop` ]);
        }));

        it("Should navigate to Search results screen on click of View all results",
            inject([ NavigationService ], (navigationService: NavigationService) =>
        {
            spyOn(navigationService, "go");
            component.viewAllResults();
            expect(navigationService.go).toHaveBeenCalledWith([ `${appRouteConstants.SEARCH}/pop` ]);
        }));
    });
});
