import { Component } from '@angular/core';
import { Subscription } from "rxjs";
import {
    IAutoCompleteResult,
    IChannel,
    IRecentSearchResult,
    Logger
} from "sxmServices";
import { appRouteConstants } from "../../app.route.constants";
import { AutoUnSubscribe } from "../../common/decorator/auto-unsubscribe";
import { ChannelListStoreService } from "../../common/service/channel-list.store.service";
import { NavigationService } from '../../common/service/navigation.service';
import { SearchService } from "../../../../../servicelib/src/search/search.service";
import { TuneClientService } from "../../common/service/tune/tune.client.service";
import {
    EventEmitter,
    Output
 } from "@angular/core";
import {
    AnalyticsBannerInds,
    AnalyticsCarouselNames,
    AnalyticsContentTypes,
    AnalyticsElementTypes,
    AnalyticsFindingMethods,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsTileTypes,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

/**
 * @MODULE:     client
 * @CREATED:    08/02/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     SearchDropdownComponent used to load search dropdown
 */

@Component({
    selector: 'sxm-search-dropdown',
    templateUrl: './search-dropdown.component.html',
    styleUrls: [ './search-dropdown.component.scss' ]
})


@AutoUnSubscribe([])
export class SearchDropdownComponent
{
    /**
     * Analytics Constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;
    public AnalyticsTileTypes = AnalyticsTileTypes;
    public AnalyticsContentTypes = AnalyticsContentTypes;
    public AnalyticsBannerInds = AnalyticsBannerInds;
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("SearchDropdownComponent");

    /**
     * Holds the recent search results.
     * @type {Array<IRecentSearchResult>}
     */
    public recentSearchResults: Array<IRecentSearchResult> = [];

    /**
     * Holds the recent search results.
     * @type {Array<IRecentSearchResult>}
     */
    public suggestSearchResults: Array<IAutoCompleteResult> = [];

    /**
     * Holds the direct Tune search results.
     * @type {Array<IChannel>}
     */
    public directTuneSearchResults: Array<IChannel> = [];

    /**
     * Holds the search Keyword.
     * @type {string}
     */
    public searchKeyword: string = '';

    /**
     * Flag to show/hide recent search results
     */
    public displayRecentSearchResults: boolean = false;

    /**
     * Flag to show/hide SuggestedSearch search results
     */
    public displaySuggestedSearchResults: boolean = false;

    /**
     * Flag to check SuggestedSearchResults are empty
     */
    public displayEmptySuggestedSearchResults: boolean = false;

    /**
     * Flag to show/hide loader when fetching suggested search results
     */
    public isLoading: boolean = false;

    /**
     * Stores subscriptions to un subscribe when component is destroyed
     * @type {Array<Subscription>}
     * @memberof SearchDropdownComponent
     */
    private subscriptions: Array<any> = [];

    /**
     * Stores subscription to un-subscribe previous results when multiple searches are done.
     * @type {Subscription}
     * @memberof SearchDropdownComponent
     */
    private suggestedSearchSubscription: any;
    private prevSearchKey: String;

    @Output() focus: EventEmitter<FocusEvent> = new EventEmitter<FocusEvent>();
    @Output() sameSearch = new EventEmitter<String>();

    /**
     * @param SearchService - used to fetch data from API
     */
    constructor(private channelListStoreService: ChannelListStoreService,
                private navigationService: NavigationService,
                private tuneClientService: TuneClientService,
                private searchService: SearchService)
    {
        SearchDropdownComponent.logger.debug("SearchDropdown Component Constructor");

        this.subscriptions.push(
            this.searchService.recentSearchResults.subscribe(data =>
            {
                this.recentSearchResults = data || [];
                this.displayRecentSearchResults = (this.recentSearchResults.length > 0 && this.searchKeyword.length === 0);
            })
        );

        this.initializeSuggestedSearch();
    }

    initializeSuggestedSearch()
    {
        this.subscriptions.push(
            this.searchService.autoCompleteSearchResults.subscribe((data: Array<IAutoCompleteResult>) =>
            {
                this.suggestSearchResults = data || [];

                this.displayEmptySuggestedSearchResults = this.suggestSearchResults.length === 0;
            })
        );

        this.subscriptions.push(
            this.searchService.directTuneSearchResults.subscribe((data: Array<IChannel>) =>
            {
                this.directTuneSearchResults = data || [];
            })
        );

        this.subscriptions.push(
            this.searchService.searchKeyword.subscribe((keyword: string) =>
            {
                this.searchKeyword = keyword;

                this.displaySuggestedSearchResults = this.enableSuggestedSearchResults(keyword);

                this.displayRecentSearchResults = (this.recentSearchResults.length > 0 && this.searchKeyword.length === 0);

                if (this.displaySuggestedSearchResults)
                {
                    this.isLoading = true;

                    if (this.suggestedSearchSubscription)
                    {
                        this.suggestedSearchSubscription.unsubscribe();
                    }

                    this.suggestedSearchSubscription = this.searchService.getAutoCompleteSearchResults(keyword).subscribe(response =>
                    {
                        this.isLoading = false;

                    });
                }
            })
        );
    }

    /**
     * enableSuggestedSearchResults method used check whether to enable the suggested search
     * at least 2 numerical character or 3 key char to should be typed to fetch auto suggest results
     */
    enableSuggestedSearchResults(keyword)
    {
        return (keyword && keyword.length === 2 && !isNaN(keyword)) || keyword.length > 2;
    }

    /**
     * clearSearchHistory method used to clear the previous search results
     */
    clearSearchHistory(): void
    {
        SearchDropdownComponent.logger.debug(`getRecentSearchList()`);

        this.subscriptions.push(
            this.searchService.clearSearchResults().subscribe(response =>
            {
                if (response)
                {
                    SearchDropdownComponent.logger.debug("clearSearchHistory( Successfully Cleared History.)");
                }
                else
                {
                    SearchDropdownComponent.logger.warn("clearSearchHistory( Failed to Clear Recent History.)");
                }
            }));
    }

    /**
     * Navigate to the now playing page and play the selected channel
     * when the user clicks on direct tune results
     * @param {IChannel} channel
     */
    goToNowPlayingPage(channel: IChannel)
    {
        this.tuneClientService.tune({ channel: channel, channelId: channel.channelId, contentType: channel.type });
    }

    /**
     * getSearchResults method used to redirect the page to search results screen
     */
    getSearchResults(keyword: string): void
    {
        SearchDropdownComponent.logger.debug(`getSearchResults()`);
        if(keyword === this.prevSearchKey)
        {
            this.sameSearch.emit(keyword);
        }
        else
        {
            this.prevSearchKey = keyword;
            this.navigationService.go([ `${appRouteConstants.SEARCH}/${keyword}` ]);
        }
    }

    /**
     * viewAllResults method used to redirect the page to search results screen
     */
    viewAllResults(): void
    {
        SearchDropdownComponent.logger.debug(`viewAllResults()`);
        this.navigationService.go([ `${appRouteConstants.SEARCH}/${this.searchKeyword}` ]);
    }
}

