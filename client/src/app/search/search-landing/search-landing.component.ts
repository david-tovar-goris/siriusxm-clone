import { Component }            from '@angular/core';
import {
    SearchService
}                               from "sxmServices";
import { ActivatedRoute }       from "@angular/router";
import { Logger }               from "sxmServices";
import {CarouselService}        from "../../carousel/carousel.service";
import { noResultsPagesConst }  from "../../no-results/no-results.const";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { TranslateService }     from "@ngx-translate/core";
import { AutoUnSubscribe }      from "../../common/decorator";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/internal/operators";
import { BrowserUtil } from "app/common/util/browser.util";
import {
    AnalyticsCarouselNames,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

/**
 * @MODULE:     client
 * @CREATED:    08/02/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  SearchLandingComponent used to load default categories and landing screen for search
 */

@Component({
    selector: 'app-search-landing',
    templateUrl: './search-landing.component.html',
    styleUrls: [ "./search-landing.component.scss" ],
    host: { '(window:resize)': 'this.resizeEvent$.next(event)' }
})
@AutoUnSubscribe()
export class SearchLandingComponent
{

    //Analytics constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("SearchLandingComponent");


    /**
     * noResultsPageName page/screen Name to render the NoResultsComponent
     */
    public noResultsPageName: string = noResultsPagesConst.SEARCH;

    private resizeEvent$: Subject<any> = new Subject<any>();

    /**
     * @param searchService - used to fetch getSearchResults from API
     * @param route - Used to get the route params
     * @param carouselService - Used to get search carousels
     */
    constructor(private searchService: SearchService,
                private route: ActivatedRoute,
                private carouselStoreService: CarouselStoreService,
                private carouselService: CarouselService)
    {
        this.carouselStoreService.selectSearchLanding().subscribe();

        this.resizeEvent$.pipe(debounceTime(500)).subscribe(() =>
        {
            //trigger vertical scroll for lazy loading background images.
            BrowserUtil.triggerVerticalScroll();
        });
    }
}
