import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SearchResultsComponent } from './search-results.component';
import { DebugElement, NO_ERRORS_SCHEMA } from "@angular/core";
import { SearchService } from "sxmServices";
import { TranslationModule } from "../../translate/translation.module";
import { SearchServiceMock } from "../../../../test/mocks/search/search.service.mock";
import { ActivatedRoute } from "@angular/router";
import { CarouselService } from "sxmServices";
import { CarouselServiceMock } from "../../../../test/mocks/carousel.service.mock";
import { of as observableOf } from "rxjs";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { TranslateService } from "@ngx-translate/core";
import * as _ from "lodash";
import { CarouselStoreService } from "../../common/service/carousel.store.service";

describe('SearchResultsComponent', () =>
{
    let component: SearchResultsComponent;
    let fixture: ComponentFixture<SearchResultsComponent>;
    let debugElement: DebugElement;

    let mockActivatedRoute = {
        params : observableOf({
            keyword : "pop"
        })
    };

    let carouselStoreServiceMock = {
            selectSubCategory: _.noop,
            selectSearch: () => observableOf("kw"),
            selectSuperCategory   : jasmine.createSpy("selectSuperCategory"),
            superCategoryCarousels: { subscribe: jasmine.createSpy('superCatSubscription') }
        };

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            declarations: [ SearchResultsComponent ],
            imports: [
                TranslationModule
            ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: SearchService, useValue: SearchServiceMock.getSpy() },
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: CarouselService, useValue: CarouselServiceMock.getSpy() },
                { provide: CarouselStoreService, useValue: carouselStoreServiceMock },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(SearchResultsComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        fixture.detectChanges();
    });

    describe('Infrastructure', () =>
    {
        it('should create', () =>
        {
            expect(component).toBeTruthy();
        });

    });
});

