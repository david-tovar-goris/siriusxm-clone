import { Component } from '@angular/core';
import {
    CarouselConsts,
    CarouselPageParameter,
    SearchService
} from "sxmServices";
import { ActivatedRoute } from "@angular/router";
import { ICarouselDataByType } from "sxmServices";
import { Logger } from "sxmServices";
import { noResultsPagesConst } from "../../no-results/no-results.const";
import { CarouselStoreService } from "../../common/service/carousel.store.service";
import { TranslateService } from "@ngx-translate/core";
import { AutoUnSubscribe } from "../../common/decorator";
import { filter, switchMap } from "rxjs/internal/operators";

/**
 * @MODULE:     client
 * @CREATED:    08/02/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  SearchResultsComponent used to load all search results for a key word
 */

@Component({
    selector: 'app-search-results',
    templateUrl: './search-results.component.html',
    styleUrls: [ "./search-results.component.scss" ]
})
@AutoUnSubscribe()
export class SearchResultsComponent
{

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("SearchResultsComponent");

    /**
     * Stores subscriptions to un subscribe when component is destroyed
     * @type {Array<Subscription>}
     * @memberof SearchResultsComponent
     */
    private subscriptions: Array<any> = [];

    private carouselData: ICarouselDataByType;

    /**
     * searchKeyword holds the keyword the user is queried for
     */
    public searchKeyword: string = '';

    /**
     * Boolean flag to check if API response is completed
     */
    public isLoading: boolean = true;

    /**
     * noResultsPageName page/screen Name to render the NoResultsComponent
     */
    public noResultsPageName: string = noResultsPagesConst.SEARCH;

    /**
     * @param searchService - used to fetch getSearchResults from API
     * @param route - Used to get the route params
     * @param carouselService - Used to get search carousels
     */
    constructor(private searchService: SearchService,
                private route: ActivatedRoute,
                private carouselStoreService: CarouselStoreService,
                private translate: TranslateService)
    {
        this.subscriptions.push(this.route.params.pipe(
            filter(param => param && param.keyword),
            switchMap(param =>
            {
                this.searchService.getSearchResults(param.keyword);

                this.searchService.updateSearchKeyword(param.keyword);

                this.searchKeyword = param.keyword;

                this.isLoading = true;

                const params : Array<CarouselPageParameter> = [];

                params.push({ paramName : CarouselConsts.SEARCH_PARAM, paramValue : param.keyword });

                return this.carouselStoreService.selectSearch(params);

            }),
            filter(carouselData => !!carouselData)
        ).subscribe((carouselData) =>
        {
            if (!carouselData) { return; }
            this.isLoading    = false;
            this.carouselData = carouselData;
        }));
    }
}
