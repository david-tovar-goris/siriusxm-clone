import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import {ContentTilesModule} from "../common/component/tiles/content-tiles.module";
import {SearchLandingComponent} from "./search-landing/search-landing.component";
import { SearchResultsComponent } from "./search-results/search-results.component";
import { CarouselModule } from "../carousel/carousel.module";
import { NoResultsModule } from "../no-results/no-results.module";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "app/common/shared.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        CarouselModule,
        NoResultsModule,
        TranslateModule,
        ContentTilesModule,
        SharedModule
    ],
    declarations: [
        SearchResultsComponent,
        SearchLandingComponent
    ],
    exports: []
})

export class SearchModule
{
}
