import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import {Injector, NO_ERRORS_SCHEMA} from "@angular/core";
import { AppComponent } from "app/app.component";
import { TranslationModule } from "./translate/translation.module";
import { WINDOW } from "./common/service/window/window.service";
import {
    AuthenticationService,
    InitializationService,
    InitializationStatusCodes,
    SxmAnalyticsService,
    AppMonitorService,
    ResumeService,
    IProfileData,
    ConfigService,
    TuneService,
    ApiCodes,
    FreeTierService
} from "sxmServices";
import {BehaviorSubject, of as observableOf} from "rxjs";
import {
    AlertServiceMock,
    AuthenticationServiceMock,
    RouterStub
} from "../../test/mocks";
import { Router } from "@angular/router";
import { SplashScreenServiceMock } from "../../test/mocks/splash-screen.service.mock";
import { SplashScreenService } from "app/common/service/splash-screen/splash-screen.service";
import { Location } from "@angular/common";
import { TuneClientService } from "./common/service/tune/tune.client.service";
import { ModalService } from "./common/service/modal/modal.service";
import { ModalServiceMock } from "../../test/mocks/modal.service.mock";
import { NavigationService } from "./common/service/navigation.service";
import { navigationServiceMock } from "../../test/mocks/navigation.service.mock";
import { ClientInactivityService } from "./inactivity/client-inactivity.service";
import { SingletonVideoService } from "./video/singleton-video/singleton-video.service";
import { LoadingScreenService } from "./common/service/loading-screen/loading-screen.service";
import { ConfigServiceMock }     from "../../test/mocks/config.service.mock";
import { AlertClientService } from "app/common/service/alert/alert.client.service";
import { FlepzScreenService } from "app/common/service/flepz/flepz-screen.service";
import { FlepzScreenServiceMock } from "../../test/mocks/flepz-screen.service.mock";
import { PlaybackCompleteService } from "./autoplay/playback-complete.service";
import { ContextMenuServiceMock } from "../../test/mocks/context-menu.service.mock";
import { ContextMenuService } from "./context-menu";
import { StickyService } from "app/common/service/sticky/sticky.service";
import { OpenAccessOverlayService } from "app/open-access/popups/open-access-overlay/open-access-overlay.service";
import { DynamicAssetLoaderService } from "./common/service/dynamic-asset-loader.service";
import { DynamicAssetLoaderServiceMock } from "../../test/mocks/dynamic-asset-loader.service.mock";
import {TranslateService} from "@ngx-translate/core";
import { MockTranslateService } from "../../test/mocks/translate.service";

describe("AppComponent", function()
{

    let component: AppComponent;
    let element: HTMLElement;
    let fixture: ComponentFixture<AppComponent>;

    /* Stubs */
    let window = WINDOW;
    let mockInitService: InitializationService = ({
        initState: observableOf(InitializationStatusCodes.RUNNING),
        state: "running"
    }) as any;
    let location = {
        go: jasmine.createSpy("go"),
        path (includeHash?: boolean): string
        {
            return "https:local-dev.siriusxm.com/now-playing";
        }
    };
    let sxmAnalyticsServiceMock = {
        updateNowPlayingData: function() {},
        logAnalyticsTag: function() {},
        fireTerminalEvent: function() {}
    };
    let appMonitorServiceMock = {
        faultStream: observableOf(330)
    };
    let resumeServiceMock = {
        profileData: observableOf({} as IProfileData),
        resumeIsComplete$: observableOf(false),
        resumeMedia: observableOf(false)
    };
    let clientInactivityServiceMock = {};
    let singletonVideoServiceMock = {};
    let loadingScreenServiceMock = {};
    let playbackCompleteServiceMock = {};
    let stickyServiceMock = {
        fullHeightPx$: new BehaviorSubject(null)
    };
    let openAccessOverlayServiceMock = {
        open: jasmine.createSpy('open')
    };
    let configServiceMock = new ConfigServiceMock();
    let tuneClientServiceCode = 0;
    let tuneClientServiceMock = {
        tuneResponseSubject: observableOf(tuneClientServiceCode),
        tune:(payload)=> {return observableOf("");},
        isPlayingContentSupported:(val, playerType)=>
        {
            return false;
        },
        getMediaType: ()=>
        {
            return "Media_Type";
        }
    } as unknown as TuneService;

    let mockTranslateService = MockTranslateService.getSpy();
    let mockFreeTierService = {};

    beforeEach(async(() =>
    {

        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [
                AppComponent
            ],
            providers: [
                { provide: WINDOW, useValue: window },
                { provide: InitializationService, useValue: mockInitService },
                { provide: AuthenticationService, useClass: AuthenticationServiceMock },
                { provide: Router, useClass: RouterStub },
                { provide: SplashScreenService, useValue: SplashScreenServiceMock },
                { provide: Location, useValue: location },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: TuneClientService, useValue: tuneClientServiceMock },
                { provide: AppMonitorService, useValue: appMonitorServiceMock },
                { provide: ModalService, useValue: ModalServiceMock },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: ResumeService, useValue: resumeServiceMock },
                { provide: ClientInactivityService, useValue: clientInactivityServiceMock },
                { provide: SingletonVideoService, useValue: singletonVideoServiceMock },
                { provide: LoadingScreenService, useValue: loadingScreenServiceMock },
                { provide: ConfigService, useValue: configServiceMock },
                { provide: AlertClientService, useClass: AlertServiceMock },
                { provide: FlepzScreenService, useClass: FlepzScreenServiceMock },
                { provide: PlaybackCompleteService, useValue: playbackCompleteServiceMock },
                { provide: ContextMenuService, useClass: ContextMenuServiceMock },
                { provide: StickyService, useValue: stickyServiceMock },
                { provide: OpenAccessOverlayService, useValue: openAccessOverlayServiceMock },
                { provide: DynamicAssetLoaderService, useClass: DynamicAssetLoaderServiceMock },
                { provide: TranslateService, useValue: mockTranslateService },
                { provide: FreeTierService, useValue: mockFreeTierService}
            ],
            schemas: [
                NO_ERRORS_SCHEMA
            ]

        }).compileComponents();

        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        element = fixture.nativeElement;

        fixture.detectChanges();
    }));

    it("can instantiate the component", async(function()
    {
        fixture.detectChanges();
        expect(component).toBeTruthy();
    }));

    it("can map urls to page titles", function()
    {
        let translate = TestBed.get(TranslateService);
        spyOn(translate, 'instant');
        component.urlToTitle("home/foryou");
        expect(translate.instant).toHaveBeenCalled();
    });

    it("can check if the mini player is shown based on the tune response", async(function()
    {
        fixture.detectChanges();

        component.tuneClientService.tuneResponseSubject.subscribe((response) =>
        {
            expect(component.showMiniPlaying).toBe(true);
        });

    }));

});
