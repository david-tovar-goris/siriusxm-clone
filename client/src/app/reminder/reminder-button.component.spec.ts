import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { ChangeDetectorRef } from "@angular/core";
import { ReminderButtonComponent } from "./reminder-button.component";
import { AlertClientService } from "../common/service/alert/alert.client.service";
import { of as observableOf } from "rxjs";
import { TranslationModule } from "../translate/translation.module";
import { TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../test/mocks/translate.service";
import { AlertServiceMock, MockStore } from "../../../test/mocks";
import { IReminderAssets } from "./reminder.interface";
import { mockAlert } from "../../../../servicelib/src/test/mocks/alerts/alert.mock";
import { AlertType } from "sxmServices";
import { SHOW_GUID } from "../common/component/list-items/edp-schedule/edp-schedule-item.consts";
import { IAppStore } from "../common/store";
import { Store } from "@ngrx/store";
import { MockDirective } from "../../../test/mocks/component.mock";

describe("ReminderButtonComponent", () =>
{
    let component: ReminderButtonComponent,
        fixture: ComponentFixture<ReminderButtonComponent>,
        alertClientService:AlertClientService,
        channelId = "howardstern100",
        appStoreMock = new MockStore<IAppStore>(),
        showGuid = "7480055f-bdd6-b22f-d47b-39340bfadac2";

    beforeEach(() =>
    {
        spyOn(appStoreMock, "select").and.returnValue(observableOf({}));
        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [
                ReminderButtonComponent,
                MockDirective({
                    selector: "[sxm-new-analytics]",
                    inputs: ["tagName", "userPath", "skipScreenName", "pageFrame", "edp", "tagText", "buttonName", "elementType", "buttonName",
                             "elementPosition", "action", "inputType", "tileType", "tileContentType" ]})
            ],
            providers: [
                { provide: AlertClientService, useClass: AlertServiceMock },
                { provide: ChangeDetectorRef, useClass: ChangeDetectorRef },
                { provide: Store, useValue: appStoreMock },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(ReminderButtonComponent);
        component = fixture.componentInstance;
        alertClientService = TestBed.get(AlertClientService);
        alertClientService.createAlert = jasmine.createSpy("createAlert").and.returnValue(observableOf(true));
        alertClientService.removeAlert = jasmine.createSpy("removeAlert").and.returnValue(observableOf(true));

        component.reminderAssets = {
            assetKey : SHOW_GUID,
            assetGuid : showGuid,
            channelId : channelId
        } as IReminderAssets;

        fixture.detectChanges();
    });

    describe('component creation', () =>
    {
        it("should exists", () =>
        {
            expect(component).toBeTruthy();
        });
    });

    describe('toggleShowReminder', () =>
    {
        beforeEach(() =>
        {
            mockAlert.alertType = "1";
            alertClientService.getAlert = jasmine.createSpy("getAlert").and.returnValue(mockAlert);
        });

        it("should create reminder", () =>
        {
            component.toggleShowReminder();
            expect(alertClientService.createAlert).toHaveBeenCalledWith(channelId, showGuid, AlertType.SHOW);
            expect(component.reminderImageSrc).toEqual("../../assets/images/reminder-on.svg");
        });

        it("should un-subscribe the reminder", () =>
        {
            component.toggleShowReminder();
            component.toggleShowReminder();
            expect(alertClientService.removeAlert).toHaveBeenCalledWith(mockAlert.alertId, mockAlert.assetGuid, AlertType.SHOW);
        });

    });
});
