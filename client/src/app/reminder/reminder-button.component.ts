import {
    Component,
    ChangeDetectorRef,
    Input
} from "@angular/core";

import { TranslateService } from "@ngx-translate/core";
import {
    IAlert,
    ICarouselDataByType,
    AlertType
} from "sxmServices";
import { AutoUnSubscribe } from "../common/decorator";
import { AlertClientService } from "../common/service/alert/alert.client.service";
import { ReminderButtonConstant } from "./reminder-button.constant";
import { IReminderAssets } from "./reminder.interface";
import { selectShowReminderCarousel } from "../common/store/carousel.store";
import { Store } from "@ngrx/store";
import { IAppStore } from "../common/store";
import { Observable } from "rxjs";
import { filter } from 'rxjs/operators';
import {
    AnalyticsEDPNames, 
    AnalyticsElementTypes, 
    AnalyticsInputTypes,
    AnalyticsPageFrames, 
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants, 
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "reminder-button",
    templateUrl: "./reminder-button.component.html",
    styleUrls: ["./reminder-button.component.scss"]
})

@AutoUnSubscribe()
export class ReminderButtonComponent
{
    //Analytics constants
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsEDPNames = AnalyticsEDPNames;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    /**
     * Image that will be toggled based on whether the reminder is set or not
     */
    public reminderImageSrc: string = "";
    public isListView : boolean;

    /**
     * View type is list or header
     */
    @Input() viewType: 'list' | 'header';

    private _reminderAssets : IReminderAssets;
    @Input() set reminderAssets (assets: IReminderAssets)
    {
        this._reminderAssets = assets;
        this.initialize();
    }

    get reminderAssets()
    {
        return this._reminderAssets;
    }

    public showReminderCarousel: Observable<ICarouselDataByType> = this.store.select(selectShowReminderCarousel);

    /**
     * used to store alert object
     */
    private alert: IAlert;

    /**
     * Constructor
     * @param {TranslateService} translate
     * @param {AlertClientService} alertClientService
     */
    constructor(private translate: TranslateService,
                private store: Store<IAppStore>,
                private changeDetectorRef: ChangeDetectorRef,
                private alertClientService: AlertClientService)
    {
        this.showReminderCarousel.pipe(filter(data => !!data && !!this.reminderAssets)).subscribe(reminderCarousel =>
        {
            this.fetchAlert();
        });
    }

    public initialize()
    {
        this.isListView = this.viewType === ReminderButtonConstant.LIST_VIEW;
        this.fetchAlert();
    }

    /**
     * Sets the reminder image status
     */
    public updateReminderImage() : void
    {
        if (this.alert)
        {
            this.reminderImageSrc = this.viewType === ReminderButtonConstant.HEADER ?
                                    "../../assets/images/reminder-on-white.svg" : "../../assets/images/reminder-on.svg";
        }
        else
        {
            this.reminderImageSrc = this.viewType === ReminderButtonConstant.HEADER ?
                                    "../../assets/images/reminder-off-white.svg" : "../../assets/images/reminder-off.svg";
        }
        this.changeDetectorRef.detectChanges();
    }

    public fetchAlert()
    {
        this.alert = this.alertClientService.getAlert(this.reminderAssets.assetGuid, AlertType.SHOW);
        this.updateReminderImage();
    }

    /**
     * Toggles the reminder button
     */
    public toggleShowReminder()
    {
        if (this.alert)
        {
            this.alertClientService.removeAlert(this.alert.alertId, this.alert.assetGuid, +this.alert.alertType)
                .subscribe((response) => this.fetchAlert());
        }
        else
        {
            this.alertClientService.createAlert(this.reminderAssets.channelId, this.reminderAssets.assetGuid, AlertType.SHOW)
                .subscribe((response) => this.fetchAlert());
        }
    }
}
