import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TranslationModule } from "../translate/translation.module";
import { AlertClientService } from "../common/service/alert/alert.client.service";
import { ReminderButtonComponent } from "./reminder-button.component";
import { SharedModule } from "app/common/shared.module";

@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        SharedModule
    ],
    declarations: [
        ReminderButtonComponent
    ],
    exports: [
        ReminderButtonComponent
    ],
    providers: [
        AlertClientService
    ]
})

export class ReminderButtonModule
{
}
