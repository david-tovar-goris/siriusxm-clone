export interface IReminderAssets
{
    assetKey: string;
    assetGuid: string;
    channelId: string;
    contentType?: string;
    subContentType?: string;
}
