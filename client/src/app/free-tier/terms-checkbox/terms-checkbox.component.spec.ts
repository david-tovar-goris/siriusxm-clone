import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TermsCheckboxComponent } from './terms-checkbox.component';
import { TranslationModule } from "app/translate/translation.module";
import { KochavaAnalyticsService } from "sxmServices";
import { kochavaAnalyticsServiceMock } from "../../../../test/mocks/kochava.service.mock";
import { MockDirective } from "../../../../test/mocks/component.mock";

describe('TermsCheckboxComponent', () =>
{
    let component: TermsCheckboxComponent;
    let fixture: ComponentFixture<TermsCheckboxComponent>;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            declarations: [
                TermsCheckboxComponent,
                MockDirective({
                    selector: "[sxm-new-analytics]", inputs: [
                        "tagName",
                        "screen",
                        "mercuryEvent",
                        "buttonName",
                        "tagText",
                        "action",
                        "actionSource",
                        "userPath",
                        "pageFrame",
                        "elementType",
                        "elementPosition",
                        "inputType",
                        "linkName",
                        "overlayName"
                    ]
                })
            ],
            imports: [TranslationModule],
            providers: [
                { provide: KochavaAnalyticsService, useValue: kochavaAnalyticsServiceMock }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(TermsCheckboxComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () =>
    {
        expect(component).toBeTruthy();
    });

    it('can agree to terms', () =>
    {
        spyOn(component.checkedChange, 'emit');
        component.checked = false;
        component.agreeToTerms();
        expect(component.checked).toEqual(true);
        expect(component.checkedChange.emit).toHaveBeenCalledWith(true);

        component.agreeToTerms();
        expect(component.checked).toEqual(false);
        expect(component.checkedChange.emit).toHaveBeenCalledWith(false);
    });
});
