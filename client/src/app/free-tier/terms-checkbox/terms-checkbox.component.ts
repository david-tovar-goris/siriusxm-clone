import {
    Component,
    Input,
    ViewEncapsulation,
    Output,
    EventEmitter
} from '@angular/core';

import {
    FreeTierConstants,
    KochavaAnalyticsService,
    kochavaAnalyticsConstants
} from "sxmServices";

import {
    AnalyticsElementTypes,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsInputTypes,
    AnalyticsTagTextConstants
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: 'app-terms-checkbox',
    templateUrl: './terms-checkbox.component.html',
    styleUrls: ['./terms-checkbox.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TermsCheckboxComponent
{
    @Input() screenName: any;
    @Input() checkboxText: string;
    @Input() checked: boolean = false;
    @Input() userPath: string;
    @Input() overlayName: string;
    @Output() checkedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    public tagText: string = this.checked ? AnalyticsTagTextConstants.UNCHECK_AGREEMENT
        : AnalyticsTagTextConstants.CHECK_AGREEMENT;

    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;

    constructor(private kochavaService : KochavaAnalyticsService) {}

    agreeToTerms()
    {
       this.checked = !this.checked;
       this.checkedChange.emit(this.checked);

        if (this.checked)
        {
            switch (this.screenName) {
                case FreeTierConstants.PAGE_NAME.FOOTER_SUBSCRIBE:
                    // Log the kochava event in subscribe flow.
                    this.kochavaService.recordAction(
                        kochavaAnalyticsConstants.SCREEN_NAMES.CREDENTIALS,
                        kochavaAnalyticsConstants.FLOWS.SUBSCRIBE,
                        kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
                        kochavaAnalyticsConstants.EVENT_ACTIONS.PRIVACY_CHECKBOX,
                        kochavaAnalyticsConstants.EVENT_NAMES.SUBSCRIBE_CREDENTIALS_PRIVACYCHECKBOX
                    );
                    break;
                default:
                    // Log the kochava event in explore flow.
                    this.kochavaService.recordAction(
                        kochavaAnalyticsConstants.SCREEN_NAMES.CREDENTIALS,
                        kochavaAnalyticsConstants.FLOWS.EXPLORE,
                        kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
                        kochavaAnalyticsConstants.EVENT_ACTIONS.PRIVACY_CHECKBOX,
                        kochavaAnalyticsConstants.EVENT_NAMES.EXPLORE_CREDENTIALS_PRIVACYCHECKBOX
                    );
                        break;
            }
            // Log the kochava event.
            this.kochavaService.recordAction(
                kochavaAnalyticsConstants.SCREEN_NAMES.CREDENTIALS,
                kochavaAnalyticsConstants.FLOWS.SUBSCRIBE,
                kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
                kochavaAnalyticsConstants.EVENT_ACTIONS.PRIVACY_CHECKBOX,
                kochavaAnalyticsConstants.EVENT_NAMES.SUBSCRIBE_CREDENTIALS_PRIVACYCHECKBOX
            );
        }
        this.tagText = this.checked ? AnalyticsTagTextConstants.UNCHECK_AGREEMENT
            : AnalyticsTagTextConstants.CHECK_AGREEMENT;
    }
}
