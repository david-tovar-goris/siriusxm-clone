import {
    async,
    ComponentFixture,
    TestBed
} from '@angular/core/testing';

import { UpsellPageComponent } from './upsell-page.component';
import { NavigationService } from "app/common/service/navigation.service";
import {MockComponent, MockDirective} from "../../../../test/mocks/component.mock";
import { Router } from "@angular/router";
import { RouterStub } from "../../../../test";
import { Location } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { of as observableOf } from "rxjs";
import { screensInfo } from "../../../../test/mocks/data/screen-info.mock";
import {
    EFreeTierFlow,
    FreeTierService,
    IFreeTierScreen, KochavaAnalyticsService, SxmAnalyticsService
} from "sxmServices";
import {kochavaAnalyticsServiceMock} from "../../../../test/mocks/kochava.service.mock";

describe('UpsellOverlayComponent', () =>
{
    let component: UpsellPageComponent;
    let fixture: ComponentFixture<UpsellPageComponent>;

    beforeEach(async(() =>
    {
        let location = {
            path (includeHash?: boolean): string
            {
                return "https:local-dev.siriusxm.com/";
            }
        };

        let freeTierServiceMock: any = {
            freeTierScreenInfo$: observableOf(screensInfo as IFreeTierScreen[]),
            getScreenInfo: jasmine.createSpy("getScreenInfo")
        } as any as FreeTierService;

        let sxmAnalyticsServiceMock = {
            updateNowPlayingData: function() {},
            logAnalyticsTag: function() {},
            fireTerminalEvent: function() {}
        };

        TestBed.configureTestingModule({
            declarations: [
                UpsellPageComponent,
                MockComponent({
                    selector: "overlay-container", inputs: ["showOverlay", "enableCloseIcon"]
                }),
                MockComponent({
                    selector: "app-register", inputs: [
                        "selectedFTFeature",
                        "selectedFTFeatureChange",
                        "closeRegisterOverlay"
                    ]
                }),
                MockDirective({
                    selector: "[sxm-new-analytics]", inputs: [
                        "tagName",
                        "screen",
                        "mercuryEvent",
                        "buttonName",
                        "tagText",
                        "action",
                        "actionSource",
                        "userPath",
                        "pageFrame",
                        "elementType",
                        "elementPosition",
                        "inputType",
                        "linkName"
                    ]
                })
            ],
            providers: [
                NavigationService,
                { provide: Router, useClass: RouterStub },
                { provide: Location, useValue: location },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: FreeTierService, useValue: freeTierServiceMock },
                { provide: KochavaAnalyticsService, useValue: kochavaAnalyticsServiceMock },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(UpsellPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () =>
    {
        expect(component).toBeTruthy();
    });

    it('can go to explore subscriptions.', () =>
    {
        const exploreSubscriptionsLink = "https://www.siriusxm.com/streaming";
        spyOn(window, 'open');
        component.explore();
        expect(window.open).toHaveBeenCalledWith(jasmine.anything(), "_blank");
    });

    it('can go to login screen.', () =>
    {
        component.login();
        expect(component.selectedFTFeature).toEqual(EFreeTierFlow.ACCESS_NOW);
        expect(component.showRegisterOverlay).toEqual(true);
    });

    it('can close the register overlay.', () =>
    {
        component.closeRegisterOverlay();
        expect(component.selectedFTFeature).toEqual(null);
        expect(component.showRegisterOverlay).toEqual(false);
    });
});
