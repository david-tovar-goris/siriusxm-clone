import { Component, OnInit } from '@angular/core';
import { NavigationService } from "app/common/service/navigation.service";
import { TranslateService } from "@ngx-translate/core";
import {
    EFreeTierFlow,
    FreeTierConstants,
    FreeTierService,
    IUpsellScreenInfo,
    KochavaAnalyticsService,
    kochavaAnalyticsConstants,
    SxmAnalyticsService
} from "sxmServices";

import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsPageNames,
    AnalyticsScreens,
    AnalyticsDisclaimerLinkNames,
    AnalyticsMercuryEventConstants
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: 'app-upsell-page',
    templateUrl: './upsell-page.component.html',
    styleUrls: ['./upsell-page.component.scss']
})
export class UpsellPageComponent implements OnInit
{
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsMercuryEventConstants = AnalyticsMercuryEventConstants;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    private exploreSubscriptionsLink = this.translate.instant('login.links.exploreSubscriptionOffers');
    public upsellPageScreenInfo: IUpsellScreenInfo = null;
    public showRegisterOverlay: boolean = false;

    public selectedFTFeature: EFreeTierFlow ;

    constructor(private navigationService: NavigationService,
                private translate: TranslateService,
                private freeTierService: FreeTierService,
                private kochavaService : KochavaAnalyticsService,
                private analyticsService: SxmAnalyticsService)
    {
        this.freeTierService.freeTierScreenInfo$.subscribe(() =>
        {
            this.upsellPageScreenInfo = this.freeTierService.getScreenInfo(FreeTierConstants.PAGE_NAME.UPSELL_SCREEN);
        });
    }

    ngOnInit()
    {
        // Log SXM analytics page view.
        this.analyticsService.logAnalyticsTag({
            name: AnalyticsTagNameConstants.WEB_FREE_ACCESS_LTUX,
            elementType: AnalyticsElementTypes.SCREEN,
            userPath: AnalyticsUserPaths.WEB_FREE_ACCESS_LTUX_LOAD,
            pageFrame: AnalyticsPageFrames.PAGE,
            action: AnalyticsTagActionConstants.PAGE_LOAD,
            actionSource: AnalyticsTagActionSourceConstants.WEB_FLOW,
            screen: AnalyticsScreens.WEB_FREE_ACCESS_LTUX,
            mercuryEvent: AnalyticsMercuryEventConstants.EVENT_SXM_EVEREST_TAG_WEB_FREE_ACCESS_LTUX
        });

        // Logs kochava page view.
        this.kochavaService.recordPageView(
            kochavaAnalyticsConstants.SCREEN_NAMES.STREAMING_OVERLAY,
            kochavaAnalyticsConstants.FLOWS.EXPIRED,
            kochavaAnalyticsConstants.EVENT_TYPES.LOAD,
            kochavaAnalyticsConstants.EVENT_ACTIONS.LOAD,
            kochavaAnalyticsConstants.EVENT_NAMES.EXPIRED_STREAMINGOVERLAY_LOAD
        );
    }

    explore()
    {
        // log kochava event.
        this.kochavaService.recordAction(
            kochavaAnalyticsConstants.SCREEN_NAMES.STREAMING_OVERLAY,
            kochavaAnalyticsConstants.FLOWS.EXPIRED,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.EXPLORE_PLANS,
            kochavaAnalyticsConstants.EVENT_NAMES.EXPIRED_STREAMINGOVERLAY_EXPLOREPLANS
        );
        const streamingUrl = this.upsellPageScreenInfo && this.upsellPageScreenInfo.subscribeStreaming.url ?
                             this.upsellPageScreenInfo.subscribeStreaming.url : this.exploreSubscriptionsLink;
        window.open(streamingUrl, "_blank");
    }

    login()
    {
        // log kochava event.
        this.kochavaService.recordAction(
            kochavaAnalyticsConstants.SCREEN_NAMES.STREAMING_OVERLAY,
            kochavaAnalyticsConstants.FLOWS.EXPIRED,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.LOGIN,
            kochavaAnalyticsConstants.EVENT_NAMES.EXPIRED_STREAMINGOVERLAY_LOGIN
        );

        this.selectedFTFeature = EFreeTierFlow.ACCESS_NOW;
        this.showRegisterOverlay = true;
    }

    closeRegisterOverlay()
    {
        this.showRegisterOverlay = false;
        this.selectedFTFeature = null;
    }
}
