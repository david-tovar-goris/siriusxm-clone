import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { TranslationModule } from "app/translate/translation.module";
import { SharedModule } from "app/common/shared.module";
import { RouterModule } from "@angular/router";

import { ContextualDataModule } from "../contextual-data/contextual-data.module";
import { WelcomePageComponent } from './welcome/welcome-page.component';
import { WelcomeCarouselComponent } from './welcome-carousel/welcome-carousel.component';
import { RegisterComponent } from './register/register.component';
import { UpsellPageComponent } from './upsell/upsell-page.component';
import { TermsCheckboxComponent } from './terms-checkbox/terms-checkbox.component';
import { UpsellBannerComponent } from './upsell-banner/upsell-banner.component';
import { RegisterService } from "./services/register.service";

@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        SharedModule,
        RouterModule,
        FormsModule,
        ContextualDataModule
    ],
    declarations: [
        WelcomePageComponent,
        WelcomeCarouselComponent,
        RegisterComponent,
        UpsellPageComponent,
        RegisterComponent,
        TermsCheckboxComponent,
        UpsellBannerComponent
    ],
    exports: [
        RegisterComponent,
        UpsellBannerComponent
    ],
    providers: [
        RegisterService
    ]
})

export class FreeTierModule {}
