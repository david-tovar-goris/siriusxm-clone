import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WelcomePageComponent } from './welcome-page.component';
import { TranslateModule, TranslateService }  from "@ngx-translate/core";
import {
    ConfigService,
    ExploreService,
    FreeTierConstants,
    FreeTierService,
    IAppConfig,
    IFreeTierScreen, kochavaAnalyticsConstants,
    KochavaAnalyticsService, SxmAnalyticsService
} from "sxmServices";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import {MockComponent, MockDirective} from "../../../../test/mocks/component.mock";
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import { VideoUIModeService } from "app/video/video-ui-mode/video-ui-mode.service";
import { RegisterService } from "app/free-tier/services/register.service";
import { By } from "@angular/platform-browser";
import { of as observableOf } from "rxjs";
import { footerInfo, screensInfo, welcomePageScreenInfo } from "../../../../test/mocks/data/screen-info.mock";
import { APP_CONFIG } from "app/sxmservicelayer/sxm.service.layer.module";

describe('WelcomePageComponent', () =>
{
    let component: WelcomePageComponent;
    let fixture: ComponentFixture<WelcomePageComponent>;
    let debugElement;

    let configServiceMock: any = {
        getRelativeUrlSettings: jasmine.createSpy("getRelativeUrlSettings")
    } as any as ConfigService;

    let videoUIModeServiceMock: any = {
        setToFreeTier: jasmine.createSpy("setToFreeTier"),
        setToDormant: jasmine.createSpy("setToDormant")
    } as any as VideoUIModeService;

    let exploreServiceMock: any = {
        initiateTeaser: jasmine.createSpy("initiateTeaser"),
        onReelClose$: observableOf(true)
    } as any as ExploreService;

    let freeTierServiceMock: any = {
        freeTierScreenInfo$: observableOf(screensInfo as IFreeTierScreen[]),
        getScreenInfo: jasmine.createSpy("getScreenInfo")
            .withArgs(FreeTierConstants.PAGE_NAME.WELCOME_SCREEN).and.returnValue(welcomePageScreenInfo)
            .withArgs('iap_footer_subscribe_flow').and.returnValue(footerInfo)
    } as any as FreeTierService;

    let mockAppConfig: any = {
        initialPathname: ''
    } as any as IAppConfig;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    function KochavaAnalyticsMockConstructor()
    {
        this.recordPageView = function() {};
        this.recordAction = function() {};
    }

    let kochavaAnalyticsServiceMock = new KochavaAnalyticsMockConstructor();

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            declarations: [
                WelcomePageComponent,
                MockComponent({ selector: "[app-legal-links]", inputs: []}),
                MockComponent({ selector: "[app-welcome-carousel]", inputs: ["carouselImages"]}),
                MockComponent({ selector: "[contextual-data-control]", inputs: ["relativeUrls"]}),
                MockDirective({ selector: "[sxm-new-analytics]", inputs: ["tagName"]})
            ],
            imports: [
                TranslateModule
            ],
            providers: [
                { provide: APP_CONFIG, useValue: mockAppConfig },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: ConfigService, useValue: configServiceMock },
                { provide: ExploreService, useValue: exploreServiceMock },
                { provide: FreeTierService, useValue: freeTierServiceMock },
                { provide: RegisterService },
                { provide: VideoUIModeService, useValue: videoUIModeServiceMock },
                { provide: KochavaAnalyticsService, useValue: kochavaAnalyticsServiceMock },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock }
            ],
            schemas: [
                NO_ERRORS_SCHEMA,
                CUSTOM_ELEMENTS_SCHEMA
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(WelcomePageComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        fixture.detectChanges();
    });

    it('should create', () =>
    {
        expect(component).toBeTruthy();
    });

    it('onClick Explore button should call onExplore method', () =>
    {
        expect(component).toBeTruthy();
        spyOn(component, "onExplore");
        fixture.debugElement.query(By.css('.action-button-2'))
            .triggerEventHandler('click', {});
        fixture.detectChanges();
        expect(component.onExplore).toHaveBeenCalled();
    });

    it('onClick Subscribe button should call onSubscribe method', () =>
    {
        spyOn(component, "onSubscribe");
        fixture.debugElement.query(By.css('.action-button-1'))
            .triggerEventHandler('click', {});
        fixture.detectChanges();
        expect(component.onSubscribe).toHaveBeenCalled();
    });

    it('For access_now neritic action onClick footer access button should call onAccessNow method', () =>
    {
        footerInfo.accessBtn = {
            text: "Sign In or Sign Up",
            neriticAction: {
                app: "",
                flowType: "subscribe",
                screen: "iap_accessNow_screen",
                videoUrl: ""
            }
        };

        freeTierServiceMock.getScreenInfo = jasmine.createSpy("getScreenInfo")
            .withArgs(FreeTierConstants.PAGE_NAME.WELCOME_SCREEN).and.returnValue(welcomePageScreenInfo)
            .withArgs('iap_footer_subscribe_flow').and.returnValue(footerInfo);
        spyOn(component, "onAccessNow");
        fixture.debugElement.query(By.css('.access-now-container a'))
            .triggerEventHandler('click', {});
        fixture.detectChanges();
        expect(component.onAccessNow).toHaveBeenCalled();
    });

    it('For subscribe_screen neritic action onClick footer access button should call onSubscribe method', () =>
    {
        footerInfo.accessBtn = {
            text: "Subscribe",
            neriticAction: {
                app: "",
                flowType: "subscribe",
                screen: "iap_subscribe_screen",
                videoUrl: ""
            }
        };

        freeTierServiceMock.getScreenInfo = jasmine.createSpy("getScreenInfo")
            .withArgs(FreeTierConstants.PAGE_NAME.WELCOME_SCREEN).and.returnValue(welcomePageScreenInfo)
            .withArgs('iap_footer_subscribe_flow').and.returnValue(footerInfo);

        spyOn(component, "onSubscribe");
        fixture.debugElement.query(By.css('.access-now-container a'))
            .triggerEventHandler('click', {});
        fixture.detectChanges();
        expect(component.onSubscribe).toHaveBeenCalled();
    });

    it('For subscribe_screen and explore flowtype, onClick footer access button should call onExplore method', () =>
    {
        footerInfo.accessBtn = {
            text: "Explore",
            neriticAction: {
                app: "",
                flowType: "explore",
                screen: "iap_subscribe_screen",
                videoUrl: ""
            }
        };

        freeTierServiceMock.getScreenInfo = jasmine.createSpy("getScreenInfo")
            .withArgs(FreeTierConstants.PAGE_NAME.WELCOME_SCREEN).and.returnValue(welcomePageScreenInfo)
            .withArgs('iap_footer_subscribe_flow').and.returnValue(footerInfo);

        spyOn(component, "onExplore");
        fixture.debugElement.query(By.css('.access-now-container a'))
            .triggerEventHandler('click', {});
        fixture.detectChanges();
        expect(component.onExplore).toHaveBeenCalled();
    });

    it("can log the welcome page load event", () =>
    {
        spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
        spyOn(kochavaAnalyticsServiceMock, "recordPageView");

        component.getDeepLinkExists(true);

        expect(kochavaAnalyticsServiceMock.recordPageView).toHaveBeenCalledWith(
            kochavaAnalyticsConstants.SCREEN_NAMES.WELCOME,
            kochavaAnalyticsConstants.FLOWS.GENERAL,
            kochavaAnalyticsConstants.EVENT_TYPES.LOAD,
            kochavaAnalyticsConstants.EVENT_ACTIONS.LOAD,
            kochavaAnalyticsConstants.EVENT_NAMES.GENERAL_WELCOME_LOAD
        );

        expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalledWith(jasmine.any(Object));
        expect(component.deepLinkDataExists).toEqual(true);
    });

    it("should log the kochava explore click event", () =>
    {
        spyOn(kochavaAnalyticsServiceMock, "recordAction");

        fixture.debugElement.query(By.css('.action-button-2'))
            .triggerEventHandler('click', {});
        fixture.detectChanges();

        expect(kochavaAnalyticsServiceMock.recordAction).toHaveBeenCalledWith(
            kochavaAnalyticsConstants.SCREEN_NAMES.WELCOME,
            kochavaAnalyticsConstants.FLOWS.GENERAL,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.EXPLORE,
            kochavaAnalyticsConstants.EVENT_NAMES.GENERAL_WELCOME_EXPLORE
        );
    });

    it("should log the kochava subscribe click event", () =>
    {
        spyOn(kochavaAnalyticsServiceMock, "recordAction");

        fixture.debugElement.query(By.css('.action-button-1'))
            .triggerEventHandler('click', {});
        fixture.detectChanges();

        expect(kochavaAnalyticsServiceMock.recordAction).toHaveBeenCalledWith(
            kochavaAnalyticsConstants.SCREEN_NAMES.WELCOME,
            kochavaAnalyticsConstants.FLOWS.GENERAL,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.SUBSCRIBE,
            kochavaAnalyticsConstants.EVENT_NAMES.GENERAL_WELCOME_SUBSCRIBE
        );
    });

    it("should log the kochava open register overlay click event", () =>
    {
        spyOn(kochavaAnalyticsServiceMock, "recordAction");

        fixture.debugElement.query(By.css('.subscribe-button'))
            .triggerEventHandler('click', {});
        fixture.detectChanges();

        expect(kochavaAnalyticsServiceMock.recordAction).toHaveBeenCalledWith(
            kochavaAnalyticsConstants.SCREEN_NAMES.WELCOME,
            kochavaAnalyticsConstants.FLOWS.GENERAL,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.SUBSCRIBE,
            kochavaAnalyticsConstants.EVENT_NAMES.GENERAL_WELCOME_SUBSCRIBE
        );
    });
});
