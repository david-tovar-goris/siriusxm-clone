import { AfterViewInit, Component, Inject, ViewEncapsulation } from '@angular/core';
import { take } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import {
    ConfigService,
    EFreeTierFlow,
    ExploreService,
    FreeTierConstants,
    FreeTierService,
    IAppConfig,
    IFreeTierButton,
    IFreeTierNeriticAction,
    IFreeTierOverlayInfo,
    IRelativeUrlSetting,
    IWelcomeScreenInfo,
    IWelcomeSubScreenInfo,
    kochavaAnalyticsConstants,
    KochavaAnalyticsService,
    SxmAnalyticsService
} from "sxmServices";
import { VideoUIModeService } from "app/video/video-ui-mode/video-ui-mode.service";
import { RegisterService } from "../services/register.service";
import { appRouteConstants } from "app/app.route.constants";
import { APP_CONFIG } from "app/sxmservicelayer/sxm.service.layer.module";
import {
    AnalyticsDisclaimerLinkNames,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsMercuryEventConstants,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import { ContextService } from "app/free-tier/services/context.service";

@Component({
    selector: 'app-welcome-page',
    templateUrl: './welcome-page.component.html',
    styleUrls: ['./welcome-page.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class WelcomePageComponent implements AfterViewInit{

    /*
        Constants for SXM analytics
     */
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsDisclaimerLinkNames = AnalyticsDisclaimerLinkNames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsMercuryEventConstants = AnalyticsMercuryEventConstants;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;

    public appleStoreUrl: string = this.translate.instant('login.links.appleStore');
    public googlePlayStoreUrl: string = this.translate.instant('login.links.googlePlayStore');
    public showRegisterOverlay: boolean = false;

    public welcomePageScreenInfo: IWelcomeScreenInfo = null;
    public footerAccessInfo: IWelcomeSubScreenInfo = null;
    public exploreButton: IFreeTierButton;

    public showTeaser: boolean = false;
    public carouselImages = [];

    public relativeUrls: Array<IRelativeUrlSetting>;

    public selectedFTFeature: EFreeTierFlow ;
    public overlayData: IFreeTierOverlayInfo = null;

    /**
     * detects the flag if deep link exists or not.
     * @type {boolean}
     */
    public deepLinkDataExists: boolean = true;

    constructor(@Inject(APP_CONFIG) private config: IAppConfig,
                private translate: TranslateService,
                private exploreService: ExploreService,
                private videoUIModeService: VideoUIModeService,
                private freeTierService: FreeTierService,
                private registerService: RegisterService,
                private configService: ConfigService,
                private analyticsService: SxmAnalyticsService,
                private kochavaService : KochavaAnalyticsService,
                private contextService: ContextService)
    {
        this.relativeUrls = this.configService.getRelativeUrlSettings();
        this.freeTierService.freeTierScreenInfo$.subscribe(() =>
        {
            this.welcomePageScreenInfo = this.freeTierService.getScreenInfo(FreeTierConstants.PAGE_NAME.WELCOME_SCREEN);
            this.footerAccessInfo = this.freeTierService.getScreenInfo(this.welcomePageScreenInfo.subScreenName);

            this.exploreButton = this.welcomePageScreenInfo.buttonOne.neriticAction.flowType === EFreeTierFlow.EXPLORE
                                 ? this.welcomePageScreenInfo.buttonOne
                                 : this.welcomePageScreenInfo.buttonTwo;
            this.carouselImages = this.welcomePageScreenInfo.carouselImages;
        });

    }

    ngAfterViewInit()
    {
        //Open respective Overlay if app access through deeplinks.
        if(this.config.initialPathname.includes(appRouteConstants.SUBSCRIBE_SCREEN))
        {
            this.onSubscribe();
        }
        else if(this.config.initialPathname.includes(appRouteConstants.ACCESS_NOW))
        {
            this.onAccessNow();
        }
        else if(this.config.initialPathname.includes(appRouteConstants.EXPLORE))
        {
            this.onExplore(this.exploreButton ? this.exploreButton.neriticAction : {} as IFreeTierNeriticAction);
        }
    }

    /* Analytics tags for welcome screen page load */
    private logWelcomePageLoad()
    {
        // Logs the Kochava page load event.
        this.kochavaService.recordPageView(
            kochavaAnalyticsConstants.SCREEN_NAMES.WELCOME,
            kochavaAnalyticsConstants.FLOWS.GENERAL,
            kochavaAnalyticsConstants.EVENT_TYPES.LOAD,
            kochavaAnalyticsConstants.EVENT_ACTIONS.LOAD,
            kochavaAnalyticsConstants.EVENT_NAMES.GENERAL_WELCOME_LOAD
        );

        // Logs SXM analytics page load event.
        this.analyticsService.logAnalyticsTag({
            name: this.deepLinkDataExists ? AnalyticsTagNameConstants.WEB_DEEP_LINK_PAGE : AnalyticsTagNameConstants.WEB_WELCOME_PAGE,
            screen: this.deepLinkDataExists ? AnalyticsScreens.WEB_DEEP_LINK_PAGE : AnalyticsScreens.WEB_WELCOME_PAGE,
            mercuryEvent: AnalyticsMercuryEventConstants.EVENT_SXM_EVEREST_TAG_WEB_WELCOME_PAGE,
            elementType: AnalyticsElementTypes.SCREEN,
            userPath: this.deepLinkDataExists ? AnalyticsScreens.WEB_DEEP_LINK_PAGE + '_'
                + AnalyticsTagActionConstants.LOAD : AnalyticsScreens.WEB_WELCOME_PAGE + '_'
                + AnalyticsTagActionConstants.LOAD,
            pageFrame: AnalyticsPageFrames.PAGE,
            action: AnalyticsTagActionConstants.PAGE_LOAD,
            actionSource: AnalyticsTagActionSourceConstants.WEB_WELCOME_LOAD
        });
    }

    onExplore(neriticAction: IFreeTierNeriticAction)
    {
        // Logs the Kochava click event.
        this.kochavaService.recordAction(
            kochavaAnalyticsConstants.SCREEN_NAMES.WELCOME,
            kochavaAnalyticsConstants.FLOWS.GENERAL,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.EXPLORE,
            kochavaAnalyticsConstants.EVENT_NAMES.GENERAL_WELCOME_EXPLORE
        );

        this.showTeaser = true;
        this.subscribeOnReelClose();
        this.selectedFTFeature = EFreeTierFlow.EXPLORE;
        this.videoUIModeService.setToFreeTier();
        this.exploreService.initiateTeaser(neriticAction.videoUrl);
    }

    onSubscribe()
    {
        // Logs the Kochava click event.
        this.kochavaService.recordAction(
            kochavaAnalyticsConstants.SCREEN_NAMES.WELCOME,
            kochavaAnalyticsConstants.FLOWS.GENERAL,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.SUBSCRIBE,
            kochavaAnalyticsConstants.EVENT_NAMES.GENERAL_WELCOME_SUBSCRIBE
        );

        this.selectedFTFeature = EFreeTierFlow.SUBSCRIBE;
        this.openRegisterOverlay();
    }

    onAccessNow()
    {
        // Logs the Kochava click event.
        this.kochavaService.recordAction(
            kochavaAnalyticsConstants.SCREEN_NAMES.WELCOME,
            kochavaAnalyticsConstants.FLOWS.GENERAL,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.ACCESS_NOW,
            kochavaAnalyticsConstants.EVENT_NAMES.GENERAL_WELCOME_ACCESSNOW
        );

        this.selectedFTFeature = EFreeTierFlow.ACCESS_NOW;
        this.openRegisterOverlay();
    }

    openRegisterOverlay()
    {
        this.showRegisterOverlay = true;
    }

    closeRegisterOverlay()
    {
        this.logRegisterOverlayClose();
        this.showRegisterOverlay = false;
        this.selectedFTFeature = null;
    }

    getOverlayNameByContext()
    {
        return this.contextService.getOverlayNameByContext(this.selectedFTFeature);
    }

    logRegisterOverlayClose()
    {
        this.analyticsService.logAnalyticsTag({
            name: AnalyticsTagNameConstants.WEB_OVERLAY_CLOSE,
            elementType: AnalyticsElementTypes.BUTTON,
            userPath: AnalyticsUserPaths.WEB_OVERLAY_ELEMENT + '_' + this.getOverlayNameByContext(),
            pageFrame: AnalyticsPageFrames.OVERLAY,
            action: AnalyticsTagActionConstants.CLOSE,
            screen: AnalyticsScreens.WEB_WELCOME_PAGE,
            overlayName: AnalyticsTagNameConstants.WEB_SUBSCRIBE_CREATE_CREDENTIALS,
            inputType: AnalyticsInputTypes.MOUSE,
            elementPosition: 0
        });
    }

    public subscribeOnReelClose()
    {
        this.exploreService.onReelClose$.pipe(take(1))
            .subscribe(() =>
        {
            this.videoUIModeService.setToDormant();
            this.showTeaser = false;
            this.openRegisterOverlay();
        });
    }

    /**
     * gets the boolean value from contextual data component which detects the deepLink exists or not.
     * @param {boolean} deepLinkExists
     */
    getDeepLinkExists(deepLinkExists: boolean)
    {
        this.deepLinkDataExists = deepLinkExists;
        this.logWelcomePageLoad();
    }

    /**
     * Action for  Neritic Link action
     */
    public onNeriticAction(neriticAction: IFreeTierNeriticAction)
    {
        switch(neriticAction.screen)
        {
            case "iap_subscribe_screen": {
                neriticAction.flowType === EFreeTierFlow.SUBSCRIBE ? this.onSubscribe() : this.onExplore(neriticAction);
                break;
            }
            case "iap_accessNow_screen": {
                this.onAccessNow();
                break;
            }
        }
    }

}
