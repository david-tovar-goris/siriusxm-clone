import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RegisterComponent } from './register.component';
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import {MockComponent, MockDirective} from "../../../../test/mocks/component.mock";
import { RegisterService } from "../services/register.service";
import { FormsModule } from "@angular/forms";
import {
    ApiCodes,
    AuthenticationService, EFreeTierFlow, FreeTierConstants,
    FreeTierService, IFreeTierButton, IFreeTierLink, IFreeTierNeriticAction,
    IFreeTierScreen,
    InitializationService,
    InitializationStatusCodes, KochavaAnalyticsService,
    SxmAnalyticsService
} from "sxmServices";
import { AuthenticationServiceMock } from "../../../../test";
import {BehaviorSubject, Observable, of as observableOf} from "rxjs";
import { Location } from "@angular/common";
import { NavigationService } from "app/common/service/navigation.service";
import { navigationServiceMock } from "../../../../test/mocks/navigation.service.mock";
import { screensInfo } from "../../../../test/mocks/data/screen-info.mock";
import { FlepzScreenService } from "app/common/service/flepz/flepz-screen.service";
import { LoginServiceMock } from "../../../../test/login.service.mock";
import { LoginService } from "app/auth/login/services/login.service";
import { kochavaAnalyticsServiceMock } from "../../../../test/mocks/kochava.service.mock";
import {
    AnalyticsMercuryEventConstants,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants, AnalyticsTagNameConstants
} from "app/analytics/sxm-analytics-tag.constants";

describe('RegisterComponent', () =>
{
    let component: RegisterComponent;
    let fixture: ComponentFixture<RegisterComponent>;

    let screenInfo = {
        logo: "string",
        backgroundImg: "string",
        registerNow: "string",
        username: "string",
        password: "string",
        emailError: "string",
        ftNotEligibleError: "string",
        forgotPassword: {
            name: "string",
            value: "string",
            url: "string"
        },
        checkbox: "string",
        subScreenName: FreeTierConstants.PAGE_NAME.FOOTER_SUBSCRIBE,
        submitBtn: {
            text: "string",
            neriticAction: {
                screen: "string",
                flowType: EFreeTierFlow.SUBSCRIBE,
                videoUrl: "string",
                app: "string"
            }
        }
    };

    let location = {
        go: jasmine.createSpy("go"),
        path (includeHash?: boolean): string
        {
            return "https:local-dev.siriusxm.com/now-playing";
        }
    };

    let freeTierServiceMock: any = {
        freeTierScreenInfo$: observableOf(screensInfo as IFreeTierScreen[]),
        getScreenInfo: jasmine.createSpy("getScreenInfo").and.returnValue(screenInfo),
        previewEndTime$: observableOf({}),
        getLegalLinks: jasmine.createSpy("getLegalLinks").and.returnValue([])
    } as any as FreeTierService;

    let mockLoginService = new LoginServiceMock();

    let flepzScreenServiceMock = {
        enableIframe: jasmine.createSpy('enableIframe')
    };

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
        this.fireTerminalEvent = function() {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    let registerServiceMock = {
        onRegisterAuthErr$: observableOf(),
        onRegisterAuthErrSubject: new BehaviorSubject<number>(0),
        registerUser: jasmine.createSpy("registerUser")
    };

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            declarations: [
                RegisterComponent,
                MockComponent({
                    selector: "app-terms-checkbox",
                    inputs: ['checkboxText', 'checked', "screenName", "userPath", "overlayName", "subScreenName"]
                }),
                MockDirective({
                    selector: "[sxm-new-analytics]", inputs: [
                        "tagName",
                        "screen",
                        "mercuryEvent",
                        "buttonName",
                        "tagText",
                        "action",
                        "actionSource",
                        "userPath",
                        "pageFrame",
                        "elementType",
                        "elementPosition",
                        "inputType",
                        "linkName",
                        "overlayName",
                        "screenName",
                        "skipCurrentPosition"
                    ]
                })
            ],
            imports: [
                TranslateModule,
                FormsModule
            ],
            providers: [
                { provide: TranslateService, useValue: MockTranslateService.getSpy() },
                { provide: AuthenticationService, useClass: AuthenticationServiceMock },
                { provide: InitializationService, useValue : { initState : observableOf(InitializationStatusCodes.RUNNING)}},
                { provide: Location, useValue: location },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: FreeTierService, useValue: freeTierServiceMock },
                { provide: FlepzScreenService, useValue: flepzScreenServiceMock },
                { provide: LoginService, useValue: mockLoginService },
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: KochavaAnalyticsService, useValue: kochavaAnalyticsServiceMock },
                { provide: RegisterService, useValue: registerServiceMock }

            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () =>
    {
        expect(component).toBeTruthy();
        expect(component.isUserSubscribed).toBe(false);
        expect(component.passwordMinLength).toBe(6);
        expect(component.authError).toBe(false);
    });

    it('enables iFrame if the url is https', () =>
    {
        component.enableIframe("iFrame", "https://player.siriusxm.com", true);
        expect(component.loginService.getTarget).toHaveBeenCalledWith("https://player.siriusxm.com", true);
        expect(flepzScreenServiceMock.enableIframe).toHaveBeenCalledWith("iFrame");
    });

    it('should open the overlay with access context', () =>
    {
        component.openAccessOverlay();
        expect(component.selectedFTFeature).toEqual("accessnow");
    });

    it('can toggle the password field', () =>
    {
        component.togglePasswordField();
        expect(component.fieldTextType).toEqual("text");
        component.togglePasswordField();
        expect(component.fieldTextType).toEqual("password");
    });

    it('can reset the form', () =>
    {
        component.userName = "username";
        component.password = "password";
        component.errorMsg = "errorMsg";
        component.isUserSubscribed = true;
        component.isTermsAccepted = true;
        component.fieldTextType = "text";

        component.formReset();

        expect(component.userName).toEqual("");
        expect(component.password).toEqual("");
        expect(component.errorMsg).toEqual("");
        expect(component.isUserSubscribed).toEqual(false);
        expect(component.isTermsAccepted).toEqual(false);
        expect(component.fieldTextType).toEqual("password");
    });

    it('can set the overlay content', () =>
    {
        component.setOverlayContent("accessnow", true);
        expect(freeTierServiceMock.getScreenInfo).toHaveBeenCalledWith("error_121");

        component.setOverlayContent("accessnow", false);
        expect(freeTierServiceMock.getScreenInfo).toHaveBeenCalledWith("iap_accessNow_screen");

        component.setOverlayContent("explore", false);
        expect(freeTierServiceMock.getScreenInfo).toHaveBeenCalledWith("iap_subscribe_screen");
    });

    it('can log the analytics for the invalid login event', () =>
    {
        spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag").and.callThrough();
        component.logInvalidLogin(101);
        fixture.detectChanges();
        expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalledWith({
            name: AnalyticsTagNameConstants.WEB_INVALID_LOGIN,
            action: AnalyticsTagActionConstants.OPEN,
            actionSource: AnalyticsTagActionSourceConstants.WEB_FLOW,
            messageType: 101,
            screen: AnalyticsScreens.WEB_WELCOME_PAGE,
            userPath: AnalyticsTagNameConstants.WEB_INVALID_LOGIN + AnalyticsScreens.WEB_WELCOME_PAGE,
            mercuryEvent: AnalyticsMercuryEventConstants.EVENT_SXM_EVEREST_TAG_WEB_INVALID_LOGIN
        });
    });
});
