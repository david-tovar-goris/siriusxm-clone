import {
    Component,
    EventEmitter,
    Input
} from '@angular/core';

import {
    ApiCodes,
    EFreeTierFlow,
    FreeTierConstants,
    FreeTierService,
    ISubscribeScreenInfo,
    IWelcomeSubScreenInfo,
    KochavaAnalyticsService,
    kochavaAnalyticsConstants,
    SxmAnalyticsService
} from "sxmServices";

import { RegisterService } from "../services/register.service";
import { appRouteConstants } from "app/app.route.constants";
import { NavigationService } from "app/common/service/navigation.service";
import { FlepzScreenService } from "app/common/service/flepz/flepz-screen.service";
import { LoginService } from "app/auth/login/services/login.service";
import {
    AnalyticsElementTypes,
    AnalyticsPageFrames,
    AnalyticsScreens,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsInputTypes,
    AnalyticsTagTextConstants,
    AnalyticsMercuryEventConstants,
    AnalyticsOverlayNameConstants,
    AnalyticsDisclaimerLinkNames
} from "app/analytics/sxm-analytics-tag.constants";
import { ContextService } from "app/free-tier/services/context.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent
{
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsDisclaimerLinkNames = AnalyticsDisclaimerLinkNames;
    public AnalyticsElementTypes = AnalyticsElementTypes;

    public fieldTextType: string = "password";
    public tagText: string = AnalyticsTagTextConstants.SHOW_PASSWORD;
    public isAccessNow: Boolean = false;
    public isExplore: Boolean = false;

    private _selectedFTFeature: EFreeTierFlow;
    @Input() set selectedFTFeature (selectedFTFeature: EFreeTierFlow)
    {
        let userSubscribe = this.isUserSubscribed;
        this._selectedFTFeature = selectedFTFeature;
        this.isAccessNow = this._selectedFTFeature === EFreeTierFlow.ACCESS_NOW;
        this.isExplore = this._selectedFTFeature === EFreeTierFlow.EXPLORE;
        this.formReset();
        if (selectedFTFeature)
        {
            this.setOverlayContent(this._selectedFTFeature, userSubscribe);
        }
    }
    get selectedFTFeature() : EFreeTierFlow { return this._selectedFTFeature; }
    @Input() selectedFTFeatureChange: EventEmitter<EFreeTierFlow> = new EventEmitter<EFreeTierFlow>();
    @Input() closeRegisterOverlay: Function;
    @Input() deepLinkDataExists: boolean;
    public footerAccessInfo: IWelcomeSubScreenInfo = null;
    public overlayName: string;
    public overlayScreenInfo: ISubscribeScreenInfo = null;

    public userName: string = "";
    public password: string = "";

    public isTermsAccepted: boolean = false;

    public errorMsg: string = "";
    public isUserSubscribed:Boolean = false;

    public eFreeTierFlow: typeof EFreeTierFlow = EFreeTierFlow;

    private emailExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    public passwordMinLength = 6;
    public authError: boolean = false;

    constructor(private registerService: RegisterService,
                private navigationService: NavigationService,
                private freeTierService: FreeTierService,
                private kochavaService : KochavaAnalyticsService,
                private flepzScreenService: FlepzScreenService,
                public loginService: LoginService,
                private analyticsService: SxmAnalyticsService,
                private contextService: ContextService)
    {
        this.subscribeAuthErr();
    }

    getUserPathByContext()
    {
        let userPath = AnalyticsUserPaths.WEB_OVERLAY;

        if (this.deepLinkDataExists)
        {
            this.overlayName = AnalyticsUserPaths.WEB_DEEP_LINK_CREATE_CREDENTIALS;
        }
        else if (this.isExplore)
        {
            this.overlayName = AnalyticsUserPaths.WEB_EXPLORE_CREATE_CREDENTIALS;
        }
        else if (this.isAccessNow)
        {
            this.overlayName = AnalyticsUserPaths.WEB_ACCESS_NOW;
        }
        else
        {
            this.overlayName = AnalyticsUserPaths.WEB_SUBSCRIBE_CREATE_CREDENTIALS;
        }

        return userPath + "_" + this.overlayName;
    }

    getDisclaimerContext()
    {
        return this.contextService.getDisclaimerContext(this.selectedFTFeature);
    }

    openAccessOverlay()
    {
        this.kochavaService.recordAction(
            kochavaAnalyticsConstants.SCREEN_NAMES.CREDENTIALS,
            kochavaAnalyticsConstants.FLOWS.SUBSCRIBE,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.ACCESS_NOW,
            kochavaAnalyticsConstants.EVENT_NAMES.SUBSCRIBE_CREDENTIALS_ACCESSNOW
        );
        this.selectedFTFeature = EFreeTierFlow.ACCESS_NOW;
        this.selectedFTFeatureChange.emit(this.selectedFTFeature);
    }

    onSubmit()
    {
        this.authError = false;

        if(this.emailExp.test(this.userName) || this.isAccessNow)
        {
            // kochava log error
            switch (this.overlayScreenInfo.subScreenName)
            {
                case FreeTierConstants.PAGE_NAME.FOOTER_ACCESS_NOW:
                    // Logs as access now.
                    this.kochavaService.recordAction(
                        kochavaAnalyticsConstants.SCREEN_NAMES.CREDENTIALS,
                        kochavaAnalyticsConstants.FLOWS.SIGN_IN_SIGN_UP,
                        kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
                        kochavaAnalyticsConstants.EVENT_ACTIONS.LOGIN,
                        kochavaAnalyticsConstants.EVENT_NAMES.SIGNINSIGNUP_CREDENTIALS_LOGIN
                    );
                    break;
                case FreeTierConstants.PAGE_NAME.FOOTER_SUBSCRIBE:
                    // Logs as subscribe.
                    this.kochavaService.recordAction(
                        kochavaAnalyticsConstants.SCREEN_NAMES.CREDENTIALS,
                        kochavaAnalyticsConstants.FLOWS.SUBSCRIBE,
                        kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
                        kochavaAnalyticsConstants.EVENT_ACTIONS.ACCEPT_CONTINUE,
                        kochavaAnalyticsConstants.EVENT_NAMES.SUBSCRIBE_CREDENTIALS_ACCEPTCONTINUE
                    );
                    break;
            }
            this.errorMsg = "";
            this.registerService.registerUser(this.userName, this.password, this.selectedFTFeature, this.closeRegisterOverlay);
        }
        else
        {
            this.authError = true;
            this.errorMsg = this.overlayScreenInfo.emailError;
            this.logInvalidLogin(this.errorMsg);
        }
    }

    togglePasswordField()
    {
        this.fieldTextType = this.fieldTextType === "password" ? "text" : "password";
        this.tagText = this.fieldTextType === "password"
            ? AnalyticsTagTextConstants.SHOW_PASSWORD
            : AnalyticsTagTextConstants.HIDE_PASSWORD;
    }

    formReset()
    {
        this.userName = "";
        this.password = "";
        this.errorMsg = "";
        this.isUserSubscribed = false;
        this.isTermsAccepted = false;
        this.fieldTextType = "password";
        this.authError = false;
    }

    getOverlayByContext()
    {
        return this.contextService.getOverlayNameByContext(this.selectedFTFeature);
    }

    setOverlayContent(ftFeature, userSubscribed)
    {
        let pageName = FreeTierConstants.PAGE_NAME.SUBSCRIBE_SCREEN;
        if (ftFeature === EFreeTierFlow.ACCESS_NOW)
        {
            pageName = userSubscribed ? FreeTierConstants.PAGE_NAME.USER_ALREADY_SUBSCRIBED
                                      : FreeTierConstants.PAGE_NAME.ACCESS_NOW;
        }

        this.freeTierService.freeTierScreenInfo$.subscribe(() =>
        {
            this.overlayScreenInfo = this.freeTierService.getScreenInfo(pageName);
            this.footerAccessInfo = this.overlayScreenInfo ?
                this.freeTierService.getScreenInfo(this.overlayScreenInfo.subScreenName) : null;

            switch (this.overlayScreenInfo.subScreenName)
            {
                case FreeTierConstants.PAGE_NAME.FOOTER_SUBSCRIBE:
                    // Logs as subscribe.
                    this.kochavaService.recordPageView(
                        kochavaAnalyticsConstants.SCREEN_NAMES.CREDENTIALS,
                        kochavaAnalyticsConstants.FLOWS.SUBSCRIBE,
                        kochavaAnalyticsConstants.EVENT_TYPES.LOAD,
                        kochavaAnalyticsConstants.EVENT_ACTIONS.LOAD,
                        kochavaAnalyticsConstants.EVENT_NAMES.SUBSCRIBE_CREDENTIALS_LOAD
                    );
                    break;
                case FreeTierConstants.PAGE_NAME.FOOTER_ACCESS_NOW:
                    // Logs as sign in sign up.
                    this.kochavaService.recordPageView(
                        kochavaAnalyticsConstants.SCREEN_NAMES.CREDENTIALS,
                        kochavaAnalyticsConstants.FLOWS.SIGN_IN_SIGN_UP,
                        kochavaAnalyticsConstants.EVENT_TYPES.LOAD,
                        kochavaAnalyticsConstants.EVENT_ACTIONS.LOAD,
                        kochavaAnalyticsConstants.EVENT_NAMES.SIGNINSIGNUP_CREDENTIALS_LOAD
                    );
                    break;
            }
        });

        this.analyticsService.logAnalyticsTag({
            name: AnalyticsTagNameConstants.WEB_OVERLAY_ELEMENT,
            action: AnalyticsTagActionConstants.OPEN,
            actionSource: AnalyticsTagActionSourceConstants.WEB_FLOW,
            screen: AnalyticsScreens.WEB_WELCOME_PAGE,
            mercuryEvent: AnalyticsMercuryEventConstants.EVENT_SXM_EVEREST_TAG_WEB_OVERLAY_ELEMENT,
            userPath: this.getUserPathByContext(),
            pageFrame: AnalyticsPageFrames.OVERLAY,
            overlayName: this.getOverlayByContext(),
            elementType: AnalyticsElementTypes.OVERLAY
        });
    }

    /**
     * enables iFrame only if url is https
     */
    public enableIframe(iFrame, url, httpsTarget) : void
    {
        if (this.loginService.getTarget(url, httpsTarget) === httpsTarget)
        {
            if (iFrame === "getStarted") this.logGetStartedClick();
            this.flepzScreenService.enableIframe(iFrame);
        }
    }

    logGetStartedClick()
    {
        // log kochava event.
        this.kochavaService.recordAction(
            kochavaAnalyticsConstants.SCREEN_NAMES.CREDENTIALS,
            kochavaAnalyticsConstants.FLOWS.SIGN_IN_SIGN_UP,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.GET_STARTED,
            kochavaAnalyticsConstants.EVENT_NAMES.SIGNINSIGNUP_CREDENTIALS_GETSTARTED
        );
    }

    public subscribeAuthErr()
    {
        this.registerService.onRegisterAuthErr$.subscribe( error =>
        {
            switch(error)
            {
                case ApiCodes.FREE_TIER_NOT_ELIGIBLE:
                {
                    this.isUserSubscribed = true;
                    this.selectedFTFeature = EFreeTierFlow.ACCESS_NOW;
                    this.selectedFTFeatureChange.emit(this.selectedFTFeature);
                    break;
                }
                case ApiCodes.FREE_TIER_EXPIRED:
                {
                    this.closeRegisterOverlay();
                    this.navigationService.go([appRouteConstants.FT_UPSELL]);
                    break;
                }
                case ApiCodes.STREAMING_ACC_NOT_FOUND:
                {
                    this.password = "";
                    this.flepzScreenService.enableIframe('getStarted',
                                                    `${this.loginService.getStartedUrl}&errorcode=127`);
                    break;
                }
                case ApiCodes.INVALID_CREDENTIALS:
                {
                    this.authError = true;
                    this.errorMsg = this.registerService.getErrorMessage(error);

                    this.logInvalidLogin(this.errorMsg);

                    break;
                }
                default:
                {
                    this.errorMsg = this.registerService.getErrorMessage(error);
                }
            }
        });
    }
    logInvalidLogin(error)
    {
        this.analyticsService.logAnalyticsTag({
            name: AnalyticsTagNameConstants.WEB_INVALID_LOGIN,
            action: AnalyticsTagActionConstants.OPEN,
            actionSource: AnalyticsTagActionSourceConstants.WEB_FLOW,
            messageType: error,
            screen: AnalyticsScreens.WEB_WELCOME_PAGE,
            userPath: AnalyticsTagNameConstants.WEB_INVALID_LOGIN + AnalyticsScreens.WEB_WELCOME_PAGE,
            mercuryEvent: AnalyticsMercuryEventConstants.EVENT_SXM_EVEREST_TAG_WEB_INVALID_LOGIN
        });
    }
}
