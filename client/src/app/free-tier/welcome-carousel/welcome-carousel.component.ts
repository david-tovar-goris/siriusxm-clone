import { Component, NgZone, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { freeTierCarouelOptions } from "app/carousel/content/slick-responsive-breakpoints";
import * as $ from "jquery";
import "slick-carousel";
import { FreeTierConstants } from "sxmServices";

@Component({
    selector: 'app-welcome-carousel',
    templateUrl: './welcome-carousel.component.html',
    styleUrls: ['./welcome-carousel.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class WelcomeCarouselComponent implements OnInit {

    // for slick carousel
    $ = $;

    private fallbackImage: string = FreeTierConstants.WELCOME_CAROUSEL.FALLBACK_IMAGE;
    public _carouselImages: string[] = [];

    @Input() set carouselImages(images: string[])
    {
        this._carouselImages = images || [];
    }

    constructor(public ngZone: NgZone) {}

    ngOnInit()
    {
        setTimeout(() =>
        {
            if(this._carouselImages.length > 0) this.createCarousel();
        },500);
    }

    /**
     * Used to create the carousel and assign to local carousel ref property
     */
    private createCarousel(): void
    {
        const opts = Object.assign(freeTierCarouelOptions, {
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000,
            zIndex: 10
        });
        const selector = ".hero-carousel";

        let element;
        this.ngZone.runOutsideAngular(() =>
        {
            element = (this.$(selector) as any).not('.slick-initialized').slick(opts);
        });
        return element;
    }

    public getImageStyle(img)
    {
        const useImg = img ? img : this.fallbackImage;
        return "url('" + useImg + "?width=640&height=480&preserveAspect=true')";
    }
}
