import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WelcomeCarouselComponent } from './welcome-carousel.component';
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { FreeTierConstants } from "sxmServices";

describe('WelcomeCarouselComponent', () =>
{
    let component: WelcomeCarouselComponent;
    let fixture: ComponentFixture<WelcomeCarouselComponent>;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            declarations: [ WelcomeCarouselComponent ],
            imports: [
                TranslateModule
            ],
            providers: [
                { provide: TranslateService, useValue: MockTranslateService.getSpy() }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(WelcomeCarouselComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () =>
    {
        expect(component).toBeTruthy();
    });

    it('should set the carousel images array', () =>
    {
        expect(component._carouselImages.length).toBe(0);
        component.carouselImages = ["img1", "img2", "img3"];
        expect(component._carouselImages.length).toBe(3);
    });

    it('can get the image style (background url)', () =>
    {
        let imageStyle = component.getImageStyle("test.jpg");
        expect(imageStyle).toBe("url('test.jpg?width=640&height=480&preserveAspect=true')");
    });

    it('can get the fallback image if no image provided', () =>
    {
        let imageStyle = component.getImageStyle(undefined);
        expect(imageStyle).toBe("url('" +
            FreeTierConstants.WELCOME_CAROUSEL.FALLBACK_IMAGE +
            "?width=640&height=480&preserveAspect=true')");
    });
});
