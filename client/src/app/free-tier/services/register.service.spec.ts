import { RegisterService } from "app/free-tier/services/register.service";
import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { of as observableOf } from "rxjs";
import { EFreeTierFlow, InitializationStatusCodes } from "sxmServices";
import { MockAppConfig } from "../../../../../servicelib/src/test/mocks/app.config.mock";

describe('Register Service', function()
{
    let translateService = MockTranslateService.getSpy();
    let authenticationServiceMock = {
        login: jasmine.createSpy().and.returnValue(observableOf({
            authenticationData: {
                remainingLockoutMinutes: 5,
                remainingLockoutSeconds: 1000,
                sessionID: "session id",
                username: "joe"
            }
        }))
    };
    let initializationService = {
        initState : observableOf(InitializationStatusCodes.RUNNING),
        reload: jasmine.createSpy().and.returnValue(observableOf({}))
    };
    let location = {
        path (includeHash?: boolean): string
        {
            return "https:local-dev.siriusxm.com/recently-played";
        },
        go: jasmine.createSpy().and.returnValue(observableOf(true))
    };
    let appConfig = new MockAppConfig();

    let freeTierService = {
        getScreenInfo: jasmine.createSpy("getScreenInfo")
    };

    let freeTierKochavaService = {
        logAction: jasmine.createSpy("logAction")
    };

    let registerService = new RegisterService(
        translateService as any,
        authenticationServiceMock as any,
        initializationService as any,
        location as any,
        freeTierKochavaService as any,
        freeTierService as any,
        appConfig
    );

    it("can instantiate the service", () =>
    {
        expect(registerService).toBeDefined();
    });

    it("can register a new user", () =>
    {
        registerService.registerUser(
            "someUserName",
            "somePassword",
            EFreeTierFlow.SUBSCRIBE,
            () => {}
        );

        expect(authenticationServiceMock.login).toHaveBeenCalledWith(
            "someUserName", "somePassword", EFreeTierFlow.SUBSCRIBE
        );
    });

    it("can get the error message by code", () =>
    {
        // invalid creds
        let errorMessage = registerService.getErrorMessage(101);
        expect(errorMessage).toBe("We don't recognize your username or password. Please try again.");

        // account locked
        let accountLocked: any = {
            "header": "Too Many Incorrect Login Attempts",
            "description": "Your SiriusXM account has been temporarily disabled for the next {{ minutes }}:{{ seconds }} minutes.",
            "buttonOne": {
                "target": "iframe_forgotUsernamePassword",
                "text": "Forgot Username or Password",
                "link": "https://streaming.siriusxm.com/?src=everestplayer&recoverlogin=true&hideheader=true"
            },
            "buttonTwo": {
                "text": "OK"
            }
        };

        errorMessage = registerService.getErrorMessage(102);
        expect(errorMessage).toEqual(accountLocked);

        const oacPassword: any = {
            "header": "Oops!",
            "description": "It appears you have logged in using your Online Account Center password.  Please log in again using your Streaming password.",
            "buttonOne": {
            "target": "iframe_forgotUsernamePassword",
                "text": "Reset Password",
                "link": "https://streaming.siriusxm.com/?src=everestplayer&recoverlogin=true&hideheader=true"
            },
                "buttonTwo": {
                "text": "Sign In Again"
            }
        };

        errorMessage = registerService.getErrorMessage(105);
        expect(errorMessage).toEqual(oacPassword);

        const expiredSubscription: any = {
            "header": "Your SiriusXM Internet Radio has expired. ",
            "description": "Don't miss everything SiriusXM has to offer. Please purchase a subscription to continue listening. ",
            "buttonOne": {
                "target": "redirectToExternalLink",
                "text": "Subscribe",
                "link": "https://care.siriusxm.com/expiredsxir"
            },
            "buttonTwo": {
                "text": "Not Now"
            }
        };

        errorMessage = registerService.getErrorMessage(5230);
        expect(errorMessage).toEqual(expiredSubscription);

        let errForbidden: any = {
            "header": "Too Many Incorrect Login Attempts",
            "description": "Your SiriusXM account has been temporarily disabled.",
            "buttonOne": {
                "target": "iframe_forgotUsernamePassword",
                "text": "Forgot Username or Password",
                "link": "https://streaming.siriusxm.com/?src=everestplayer&recoverlogin=true&hideheader=true"
            },
            "buttonTwo": {
                "text": "OK"
            }
        };

        errorMessage = registerService.getErrorMessage(403);
        expect(errorMessage).toEqual(errForbidden);

        errorMessage = registerService.getErrorMessage(undefined);
        expect(errorMessage).toBe("We don't recognize your username or password. Please try again.");
    });

    it("can handle login success if flow is subscribe", () =>
    {

        spyOn(window, "open").and.returnValue(false);

        registerService.registerUser(
            "someUserName",
            "somePassword",
            EFreeTierFlow.SUBSCRIBE,
            ()=> {}
        );

        expect(authenticationServiceMock.login).toHaveBeenCalledWith(
            "someUserName",
            "somePassword",
            EFreeTierFlow.SUBSCRIBE);
    });

    it("can handle login success when flow is not subscribe", () =>
    {
        registerService.registerUser(
            "someUserName",
            "somePassword",
            EFreeTierFlow.EXPLORE,
            ()=> {}
        );

        expect(initializationService.reload).toHaveBeenCalled();
        expect(authenticationServiceMock.login).toHaveBeenCalled();
    });
});
