import { TestBed } from '@angular/core/testing';

import { ContextService } from './context.service';
import { EFreeTierFlow } from "sxmServices";
import { AnalyticsOverlayNameConstants, AnalyticsUserPaths } from "app/analytics/sxm-analytics-tag.constants";

let service: ContextService;

describe('ContextService', () =>
{

  beforeEach(() =>
  {
      TestBed.configureTestingModule({});
      service = TestBed.get(ContextService);
  });

    it('should be created', () =>
    {
        expect(service).toBeTruthy();
    });

    it('should get the disclaimer context(userPath)', () =>
    {
        let context = service.getDisclaimerContext(EFreeTierFlow.ACCESS_NOW);
        expect(context).toEqual(AnalyticsUserPaths.WEB_ACCESS_NOW);
        context = service.getDisclaimerContext(EFreeTierFlow.EXPLORE);
        expect(context).toEqual(AnalyticsUserPaths.WEB_EXPLORE_CREATE_CREDENTIALS);
        context = service.getDisclaimerContext(EFreeTierFlow.SUBSCRIBE);
        expect(context).toEqual(AnalyticsUserPaths.WEB_SUBSCRIBE_CREATE_CREDENTIALS);
        context = service.getDisclaimerContext("default");
        expect(context).toEqual(AnalyticsUserPaths.WEB_WELCOME_PAGE);
    });

    it('should get the overlay name by context', () =>
    {
        let context = service.getOverlayNameByContext(EFreeTierFlow.ACCESS_NOW);
        expect(context).toEqual(AnalyticsOverlayNameConstants.WEB_ACCESS_NOW);
        context = service.getOverlayNameByContext(EFreeTierFlow.SUBSCRIBE);
        expect(context).toEqual(AnalyticsOverlayNameConstants.WEB_SUBSCRIBE_CREATE_CREDENTIALS);
        context = service.getOverlayNameByContext(EFreeTierFlow.EXPLORE);
        expect(context).toEqual(AnalyticsOverlayNameConstants.WEB_EXPLORE_CREATE_CREDENTIALS);
    });
});
