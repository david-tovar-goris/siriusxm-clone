import { Inject, Injectable }   from "@angular/core";
import { Location }             from "@angular/common";
import { TranslateService }     from "@ngx-translate/core";
import { BehaviorSubject, Observable }      from "rxjs";

import { appRouteConstants }    from "../../app.route.constants";
import {
    ApiCodes,
    AuthenticationService,
    EFreeTierFlow,
    IAppConfig,
    IAuthenticationResponse,
    InitializationService,
    Logger,
    FreeTierKochavaService,
    FreeTierService,
    FreeTierConstants
} from "sxmServices";
import { APP_CONFIG }           from "../../sxmservicelayer/sxm.service.layer.module";

@Injectable()
export class RegisterService {

    private static logger: Logger = Logger.getLogger("RegisterService");

    private onRegisterAuthErrSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);
    public onRegisterAuthErr$ :Observable<number> = this.onRegisterAuthErrSubject;

    constructor(private translate: TranslateService,
                private authenticationService: AuthenticationService,
                private initializationService: InitializationService,
                private location: Location,
                private freeTierKochavaService: FreeTierKochavaService,
                private freeTierService: FreeTierService,
                @Inject(APP_CONFIG) public appConfig: IAppConfig)
    {}

    /**
     * Will register if user is new else it returns authentication
     */
    public registerUser(userName: string, password: string, screenFlow: EFreeTierFlow, closeOverlay?: Function)
    {
        this.authenticationService.login(userName, password, screenFlow)
            .subscribe(this.onLoginSuccess.bind(this,screenFlow, closeOverlay),this.onLoginFailure.bind(this, screenFlow));
    }

    private onLoginSuccess(screenFlow: EFreeTierFlow, closeOverlay:Function, response: IAuthenticationResponse)
    {
        if (response !== null && response.authenticationData)
        {
            this.freeTierKochavaService.logAction('LoginSuccess', screenFlow);
            if(screenFlow === EFreeTierFlow.SUBSCRIBE)
            {
                const screenInfo = this.freeTierService.getScreenInfo(FreeTierConstants.PAGE_NAME.SUBSCRIBE_SCREEN);
                const streamingUrl = screenInfo && screenInfo.submitBtn && screenInfo.submitBtn.url
                                     ? screenInfo.submitBtn.url : "https://www.siriusxm.com/streaming";
                window.open(streamingUrl,"_blank");
                if(closeOverlay) closeOverlay();
                return;
            }
            /*
                If successful login from the open access overlay if there's a query string from a deep link,
                route there instead of home before refreshing the app. This maintains the deep link. If there's
                no query string then route to home and refresh the app.
             */
            const queryString:string = this.appConfig.contextualInfo.queryString;
            const rootWithQueryString:string = "/?" + queryString;

            if (queryString && queryString !== "")
            {
                this.location.go(rootWithQueryString);
            }
            else
            {
                this.location.go(appRouteConstants.HOME);
            }

            this.initializationService.reload(() => { location.reload(); });
        }
    }

    private onLoginFailure(screenFlow: EFreeTierFlow, error)
    {
        RegisterService.logger.warn(`RegisterFault( Error: ${error.message} )`);
        this.onRegisterAuthErrSubject.next(error.code);
        this.freeTierKochavaService.logAction('LoginFailure', screenFlow, error.code);
    }

    /**
     * Returns error message for respective login error code
     * @param code
     */
    public getErrorMessage(code): string
    {
        switch (code)
        {
            case ApiCodes.INVALID_CREDENTIALS:
               return this.translate.instant('login.errors.invalidLogin');
            case ApiCodes.ACCOUNT_LOCKED:
                return this.translate.instant('login.errors.modals.accountLocked');
            case ApiCodes.OAC_PASSWORD:
                return this.translate.instant('login.errors.modals.oacLoginAttempt');
            case ApiCodes.EXPIRED_SUBSCRIPTION:
                return this.translate.instant('login.errors.modals.expiredSubscription');
            case ApiCodes.SR_REQ_FORBIDDEN:
                return this.translate.instant('login.errors.modals.errForbidden');
            default:
                return this.translate.instant('login.errors.invalidLogin');
        }

    }
}
