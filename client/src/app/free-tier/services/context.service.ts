import { Injectable } from '@angular/core';
import { EFreeTierFlow } from "sxmServices";
import { AnalyticsOverlayNameConstants, AnalyticsUserPaths } from "app/analytics/sxm-analytics-tag.constants";

@Injectable({
  providedIn: 'root'
})
export class ContextService {

  constructor() {}

    public getDisclaimerContext(selectedFTFeature)
    {
        let disclaimerContext;

        switch (selectedFTFeature)
        {
            case EFreeTierFlow.ACCESS_NOW:
                disclaimerContext = AnalyticsUserPaths.WEB_ACCESS_NOW;
                break;
            case EFreeTierFlow.EXPLORE:
                disclaimerContext = AnalyticsUserPaths.WEB_EXPLORE_CREATE_CREDENTIALS;
                break;
            case EFreeTierFlow.SUBSCRIBE:
                disclaimerContext = AnalyticsUserPaths.WEB_SUBSCRIBE_CREATE_CREDENTIALS;
                break;
            default:
                disclaimerContext = AnalyticsUserPaths.WEB_WELCOME_PAGE;
                break;
        }

        return disclaimerContext;
    }

    public getOverlayNameByContext(selectedFTFeature)
    {
        let overlayName: string;

        switch (selectedFTFeature)
        {
            case EFreeTierFlow.ACCESS_NOW:
                overlayName = AnalyticsOverlayNameConstants.WEB_ACCESS_NOW;
                break;
            case EFreeTierFlow.SUBSCRIBE:
                overlayName = AnalyticsOverlayNameConstants.WEB_SUBSCRIBE_CREATE_CREDENTIALS;
                break;
            case EFreeTierFlow.EXPLORE:
                overlayName = AnalyticsOverlayNameConstants.WEB_EXPLORE_CREATE_CREDENTIALS;
                break;
        }

        return overlayName;
    }
}
