import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UpsellBannerComponent } from './upsell-banner.component';
import { MockComponent, MockDirective } from "../../../../test/mocks/component.mock";
import { TranslateModule } from "@ngx-translate/core";
import {
    AuthenticationService,
    ConfigService, EFreeTierFlow,
    FreeTierService,
    IFreeTierScreen,
    IUpsellBannerSubscribeInfo,
    KochavaAnalyticsService,
    MediaPlayerService
} from "sxmServices";
import { ConfigServiceMock } from "../../../../test/mocks/config.service.mock";
import { AuthenticationServiceMock } from "../../../../test";
import { of, of as observableOf } from "rxjs";
import { screensInfo } from "../../../../test/mocks/data/screen-info.mock";
import { NavigationService } from "app/common/service/navigation.service";
import { navigationServiceMock } from "../../../../test/mocks/navigation.service.mock";
import { MediaPlayerServiceMock } from "../../../../test/mocks/media-player.service.mock";
import { TranslationModule } from "app/translate/translation.module";
import { kochavaAnalyticsServiceMock } from "../../../../test/mocks/kochava.service.mock";

describe('UpsellBannerComponent', () =>
{
    let component: UpsellBannerComponent;
    let fixture: ComponentFixture<UpsellBannerComponent>;

    let upsellBannerSubscribeInfo: IUpsellBannerSubscribeInfo = {
        backgroundImg: "path to image",
        line1: "line 1 copy",
        line2: "line 2 copy",
        subscribeBtn: {
            text: "button test",
            neriticAction: {
                screen: "screen name",
                flowType: EFreeTierFlow.SUBSCRIBE,
                videoUrl: "video url",
                app: "app"
            }
        }
    };

    let freeTierServiceMock: any = {
        freeTierScreenInfo$: observableOf(screensInfo as IFreeTierScreen[]),
        getScreenInfo: jasmine.createSpy("getScreenInfo").and.returnValue(upsellBannerSubscribeInfo),
        previewEndTime$: observableOf({})
    } as any as FreeTierService;

    let mediaPlayerServiceMock = new MediaPlayerServiceMock();
    let configServiceMock = new ConfigServiceMock();

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                TranslateModule,
                TranslationModule
            ],
            declarations: [
                UpsellBannerComponent,
                MockComponent({
                    selector: "app-count-down-timer",
                    inputs: ['previewEndTime']
                }),
                MockDirective({
                    selector: "[sxm-sticky]",
                    inputs: ["stickyClass", "alwaysSticky"]
                }),
                MockDirective({
                    selector: "[sxm-new-analytics]", inputs: [
                        "tagName",
                        "screen",
                        "mercuryEvent",
                        "buttonName",
                        "tagText",
                        "action",
                        "actionSource",
                        "userPath",
                        "pageFrame",
                        "elementType",
                        "elementPosition",
                        "inputType",
                        "linkName",
                        "overlayName",
                        "screenName",
                        "shim",
                        "perfVal1"
                    ]
                })
            ],
            providers: [
                { provide: ConfigService, useValue: configServiceMock },
                { provide: AuthenticationService, useValue: AuthenticationServiceMock.getSpy() },
                { provide: FreeTierService, useValue: freeTierServiceMock },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: MediaPlayerService, useValue: mediaPlayerServiceMock },
                { provide: KochavaAnalyticsService, useValue: kochavaAnalyticsServiceMock }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(UpsellBannerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        spyOn(freeTierServiceMock.freeTierScreenInfo$, "subscribe").and.returnValue(of({}));
    });

    it('should create', () =>
    {
        expect(component).toBeTruthy();
    });

    it('can get signup copy', () =>
    {
        spyOn(configServiceMock, "getOpenAccessCopy");
        component.getSignUpCopy();
        expect(configServiceMock.getOpenAccessCopy).toHaveBeenCalledWith("npOpenAccessSignUpButtonCopy");
    });

    it('can handle the free preview end event', () =>
    {
        spyOn(mediaPlayerServiceMock.mediaPlayer, "stop");
        spyOn(navigationServiceMock, "go");

        component.onFreePreviewEnd({});

        expect(mediaPlayerServiceMock.mediaPlayer.stop).toHaveBeenCalled();
    });
});
