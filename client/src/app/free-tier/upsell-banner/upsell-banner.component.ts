import { Component, OnInit } from '@angular/core';
import {
    AuthenticationService,
    ConfigService,
    ISession,
    FreeTierService,
    IUpsellBannerSubscribeInfo,
    MediaPlayerService,
    KochavaAnalyticsService,
    kochavaAnalyticsConstants
} from "sxmServices";
import { SubscriptionLike as ISubscription } from "rxjs/internal/types";
import { AutoUnSubscribe } from "app/common/decorator";
import { FreeTierConstants } from "../../../../../servicelib/src/free-tier/free-tier.constants";
import { appRouteConstants } from "app/app.route.constants";
import { NavigationService } from "app/common/service/navigation.service";
import { TranslateService } from "@ngx-translate/core";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths,
    AnalyticsTagShimConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsMercuryEventConstants,
    AnalyticsScreens
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: 'app-upsell-banner',
    templateUrl: './upsell-banner.component.html',
    styleUrls: ['./upsell-banner.component.scss']
})

@AutoUnSubscribe([])
export class UpsellBannerComponent implements OnInit {

    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsTagShimConstants = AnalyticsTagShimConstants;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsMercuryEventConstants = AnalyticsMercuryEventConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    private subscriptions: Array<ISubscription> = [];
    public upsellBannerSubscribeInfo: IUpsellBannerSubscribeInfo = null;

    public session: ISession;
    public bannerText: string[];
    public previewEndTime: string;
    public timeRemaining: string;

    constructor(private configService: ConfigService,
                private authenticationService: AuthenticationService,
                private freeTierService: FreeTierService,
                private navigationService: NavigationService,
                private mediaPlayerService: MediaPlayerService,
                private translate: TranslateService,
                private kochavaService : KochavaAnalyticsService)
    {

        this.subscriptions.push(authenticationService.userSession.subscribe(data =>
        {
            this.session = data;
        }));

        this.freeTierService.freeTierScreenInfo$.subscribe(() =>
        {
            this.upsellBannerSubscribeInfo = this.freeTierService.getScreenInfo(FreeTierConstants.PAGE_NAME.UPSELL_BANNER_SUBSCRIBE);
            let lineText = this.upsellBannerSubscribeInfo.line1 ?
                           this.upsellBannerSubscribeInfo.line1.replace(/{.*}/, '~') : "";
            this.bannerText = lineText.split("~");

        });

        this.freeTierService.previewEndTime$.subscribe((endTime) =>
        {
            this.previewEndTime = endTime;
        });
    }

    ngOnInit()
    {
        // Logs the Kochava page load event.
        this.kochavaService.recordPageView(
            kochavaAnalyticsConstants.SCREEN_NAMES.FREE_ACCESS,
            kochavaAnalyticsConstants.FLOWS.BANNER,
            kochavaAnalyticsConstants.EVENT_TYPES.LOAD,
            kochavaAnalyticsConstants.EVENT_ACTIONS.LOAD,
            kochavaAnalyticsConstants.EVENT_NAMES.BANNER_FREEACCESS_LOAD
        );
    }

    trackTimerForKochava(event: any)
    {
        this.timeRemaining = event.timeDifference;
    }

    explore()
    {
        // Logs the Kochava click event.
        this.kochavaService.recordAction(
            kochavaAnalyticsConstants.SCREEN_NAMES.FREE_ACCESS,
            kochavaAnalyticsConstants.FLOWS.BANNER,
            kochavaAnalyticsConstants.EVENT_TYPES.CLICK,
            kochavaAnalyticsConstants.EVENT_ACTIONS.SUBSCRIBE,
            kochavaAnalyticsConstants.EVENT_NAMES.BANNER_FREEACCESS_SUBSCRIBE,
            this.timeRemaining
        );
    }

    public getSignUpCopy()
    {
        return this.configService.getOpenAccessCopy("npOpenAccessSignUpButtonCopy");
    }

    public onFreePreviewEnd(event)
    {
        if (this.mediaPlayerService.mediaPlayer)
        {
            this.mediaPlayerService.mediaPlayer.stop();
        }
        this.navigationService.go([ appRouteConstants.FT_UPSELL ]);
    }
}
