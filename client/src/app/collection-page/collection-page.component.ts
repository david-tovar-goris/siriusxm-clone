import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {ICarouselData, ICarouselDataByType, Logger} from "sxmServices";
import {CarouselStoreService} from "../common/service/carousel.store.service";
import {CarouselService} from "../carousel/carousel.service";
import {NavigationService} from "../common/service/navigation.service";
import {displayViewType} from "../channel-list/component/display.view.type";
import { Observable, SubscriptionLike as ISubscription } from "rxjs";
import { filter, map, tap } from "rxjs/operators";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector   : "collection-page",
    templateUrl: "./collection-page.component.html",
    styleUrls  : [ "./collection-page.component.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class CollectionPageComponent implements OnInit
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    public pageTitle: string;
    public pageText: string;
    public carousel$: Observable<ICarouselDataByType>;
    public collectionCarousel$: Observable<ICarouselData>;
    public displayViewType: displayViewType =  displayViewType.channels;
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("CollectionPageComponent");

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions: Array<ISubscription> = [];

    /**
     * Constructor
     * @param {Router} router
     * @param {CarouselStoreService} carouselStoreService
     * @param {ActivatedRoute} activeRoute
     * @param {CarouselService} carouselService
     */
    constructor(private activeRoute: ActivatedRoute,
                public carouselService: CarouselService,
                private navigationService: NavigationService,
                public carouselStoreService: CarouselStoreService)
    { }

    /**
     * ngOnInit
     */
    ngOnInit(): void
    {
        CollectionPageComponent.logger.debug("ngOnInit()");

        this.carousel$ = this.carouselStoreService.collectionCarousels
            .pipe(
                filter(data => !!data),
                tap( (carouselData: ICarouselDataByType) =>
                {
                    //TODO currently we are not getting the title for this carousel page from the API,
                    // once API fixes it we will remove this
                    this.pageTitle = carouselData.pageTextLine1 || 'All Xtra Channels';
                    this.pageText = carouselData.pageTextLine2;
                }),
                map( (carouselData: ICarouselDataByType) => carouselData)
            );

        this.collectionCarousel$ = this.carousel$
            .pipe(
                map( (carouselData: ICarouselDataByType) =>
                {
                    if (carouselData.zone && carouselData.zone[0] &&  carouselData.zone[0].content)
                    {
                        return carouselData.zone[0].content.length > 0 ? carouselData.zone[0].content[0] : { tiles: [] };
                    }

                    return null;
                })
            );
    }

    public goBack()
    {
        this.navigationService.goBack();
    }
}
