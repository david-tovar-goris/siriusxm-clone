import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TranslationModule } from "../translate/translation.module";
import { CollectionPageComponent } from "./collection-page.component";
import { NoResultsModule } from "../no-results/no-results.module";
import { ContextMenuModule } from "../context-menu/context-menu.module";
import { SharedModule } from "../common/shared.module";
import { FavoritesModule } from "../favorites/favorites.module";
import { CollectionPageResolver } from "./collection-page.resolver";
import { CarouselModule } from "../carousel";
import { VerticalListModule } from "../common/component/list-items/vertical-list/vertical-list.module";


@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        NoResultsModule,
        ContextMenuModule,
        FavoritesModule,
        SharedModule,
        CarouselModule,
        VerticalListModule
    ],
    declarations: [
      CollectionPageComponent
    ],
    exports: [
        CollectionPageComponent
    ],
    providers: [
        CollectionPageResolver
    ]
})

export class CollectionPageModule
{
}

