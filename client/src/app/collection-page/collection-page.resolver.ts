import { first, switchMap, skipWhile } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot
} from "@angular/router";
import { Observable } from "rxjs";
import { ICarouselDataByType, InitializationService, InitializationStatusCodes, Logger } from "sxmServices";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { SplashScreenService } from "../common/service/splash-screen/splash-screen.service";

@Injectable()
export class CollectionPageResolver implements Resolve<{}>
{
    constructor(private initService: InitializationService,
                private carouselStoreService: CarouselStoreService,
                private splashScreenService: SplashScreenService)
    {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    {
        return this.initService.initState.pipe(
                   skipWhile((data) => data !== InitializationStatusCodes.RUNNING),
                   switchMap(() => resolveCollectionPageData(route, this.splashScreenService, this.carouselStoreService)),
                   first());
    }
}

function resolveCollectionPageData(route: ActivatedRouteSnapshot,
                                   splashScreenService: SplashScreenService,
                                   carouselStoreService:CarouselStoreService): Observable<ICarouselDataByType>
{
    splashScreenService.closeSplashScreen();

    return carouselStoreService.selectCollectionCarousel(route.params[ 'url' ]);
}
