import { Component } from "@angular/core";
import { appRouteConstants } from "../app.route.constants";
import { NavigationService } from "../common/service/navigation.service";
import { TranslateService } from "@ngx-translate/core";

@Component(
    {
        selector: "sxm-footer",
        templateUrl: "./footer.component.html",
        styleUrls: [ "./footer.component.scss" ]
    })

export class FooterComponent
{
    /*added render attribute to footerBtnInfo, in case we want to display these buttons in future*/
    public footerBtnInfo: Array<any> = [
        { name: "allChannels", img: "../assets/images/all-channels.svg", class: "channel", render:true },
        { name: "onDemand", img: "../assets/images/on-demand-icon.svg", class: "ondemand", render:false },
        { name: "videos", img: "../assets/images/video.svg", class: "video", render:false }
    ];

    public footerAnalyticProps: {};

    constructor(private navigationService: NavigationService,
                private translate: TranslateService)
    {
        this.footerBtnInfo = this.footerBtnInfo.filter(button => button.render);
    }

    // #TODO: This is a temporary alert for the footer icons till we have defined routes
    routeOnClick(selectedIcon: string)
    {
        if (selectedIcon === "channel")
        {
            this.navigationService.go([appRouteConstants.ALL_CHANNELS]);
        }
        if(selectedIcon === "ondemand")
        {
            this.navigationService.go([appRouteConstants.ALL_ON_DEMAND]);
        }
        if(selectedIcon === "video")
        {
            this.navigationService.go([appRouteConstants.CATEGORY_PAGE, appRouteConstants.ALL_VIDEO]);
        }
    }
}
