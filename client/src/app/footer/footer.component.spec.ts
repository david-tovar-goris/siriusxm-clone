import * as _ from "lodash";

import {
    async,
    ComponentFixture,
    TestBed
} from "@angular/core/testing";

import { MockComponent } from "../../../test/mocks/component.mock";
import { NavigationService } from "../common/service/navigation.service";
import { navigationServiceMock } from "../../../test/mocks/navigation.service.mock";
import { TranslationModule } from "../translate/translation.module";
import { ToastService } from "../common/service/toast/toast.service";

import { FooterComponent } from "./footer.component";

describe("FooterComponent", () =>
{
    let component: FooterComponent;
    let fixture: ComponentFixture<FooterComponent>;

    const toastServiceMock = {
        open: _.noop
    };

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
                imports: [
                    TranslationModule
                ],
                declarations: [
                    FooterComponent
                ],
                providers: [
                    { provide: NavigationService, useValue: navigationServiceMock },
                    { provide: ToastService, useValue: toastServiceMock }
                ]
            })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(FooterComponent);
        component = fixture.componentInstance;

        fixture.detectChanges();
    });

    it("should be created", () =>
    {
        expect(component).toBeTruthy();
    });

    it("should expose 'footerBtnInfo'", () =>
    {
        expect(component.footerBtnInfo).toBeDefined();
        expect(typeof component.footerBtnInfo).toEqual("object");
        expect(Array.isArray(component.footerBtnInfo)).toEqual(true);
    });

    it("should expose 'routeOnClick'", () =>
    {
        expect(component.routeOnClick).toBeDefined();
        expect(typeof component.routeOnClick).toEqual("function");
    });
});
