import { switchMap, map, skipWhile, first } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import {
    ActivatedRouteSnapshot,
    CanActivate,
    RouterStateSnapshot
} from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { InitializationService, InitializationStatusCodes, Logger } from "sxmServices";
import { getChannels, getLiveChannels} from "../common/store/channel-list.store";
import { IAppStore } from "../common/store/app.store";


@Injectable()
export class RecentlyPlayedRouteGuard implements CanActivate
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ProfileCarouselRouteGuard");

    /**
     * Constructor.
     */
    constructor(private store: Store<IAppStore>,
                private initializationService: InitializationService) {}

    /**
     * Determines if the user can navigate to the requested route if init service is running.
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {boolean}
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>
    {
        return (this.initializationService.initState.pipe(
                    skipWhile(initState => initState !== InitializationStatusCodes.RUNNING),
                    switchMap(() => channelLineUpExists(this.store)),
                    first())) as any as Observable<boolean>;
    }
}

/**
 * Determines if the channel line up data available or not.
 * @param {Store<IAppStore>} store
 * @returns {Observable<boolean>}
 */
function channelLineUpExists(store: Store<IAppStore>): Observable<boolean>
{
    return store.select(getLiveChannels).pipe(skipWhile(channels => channels.length === 0),
                map((data) =>
                {
                    return true;
                }));
}
