import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TranslationModule } from "../translate/translation.module";
import { RecentlyPlayedComponent } from "./recently-played.component";
import { NoResultsModule } from "../no-results/no-results.module";
import { ContextMenuModule } from "../context-menu/context-menu.module";
import { SharedModule } from "../common/shared.module";
import { RecentlyPlayedStoreService } from "../common/service/recently-played.store.service";
import { FavoritesModule } from "../favorites/favorites.module";
import { ContentTilesModule } from "../common/component/tiles/content-tiles.module";
import { RecentlyPlayedRouteGuard } from "./recently-played.route-guard";


@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        NoResultsModule,
        ContextMenuModule,
        FavoritesModule,
        SharedModule,
        ContentTilesModule
    ],
    declarations: [
        RecentlyPlayedComponent
    ],
    exports: [
        RecentlyPlayedComponent
    ],
    providers: [
        RecentlyPlayedStoreService,
        RecentlyPlayedRouteGuard
    ]
})

export class RecentlyPlayedModule
{
}

