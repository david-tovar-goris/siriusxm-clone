import { map, filter } from 'rxjs/operators';
import { Component, EventEmitter, OnInit, Input, Output, ChangeDetectionStrategy } from "@angular/core";
import { Store } from "@ngrx/store";
import { TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs";
import { Logger, ICarouselData, ITile, AppMonitorService, AppErrorCodes } from "sxmServices";
import { noResultsPagesConst } from "../no-results/no-results.const";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { NeriticLinkService } from "../neritic-links/neritic-link.service";
import { IAppStore } from "../common/store/app.store";
import { selectRecentsContentCarousel } from "../common/store/carousel.store";
import { CarouselOrientationTypeConsts } from "sxmServices";
import { CarouselService } from "../carousel/carousel.service";
import { NeriticLinkOptions } from "../neritic-links/neritic-link.interface";
import { CMOption } from "app/context-menu";
import { TileContextMenuOptionsService } from "app/context-menu/options/tile-context-menu-options.service";
import {
    AnalyticsCarouselNames,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector: "sxm-recently-played",
    templateUrl: "./recently-played.component.html",
    styleUrls: [ "./recently-played.component.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class RecentlyPlayedComponent implements OnInit
{
    /**
     * Analytics Constants
     */
     public AnalyticsUserPaths = AnalyticsUserPaths;
     public AnalyticsElementTypes = AnalyticsElementTypes;
     public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
     public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
     public AnalyticsInputTypes = AnalyticsInputTypes;
     public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
     public AnalyticsCarouselNames = AnalyticsCarouselNames;

    /**
     * Internal logger - Logs messages to console.
     */
    private static logger: Logger = Logger.getLogger("RecentlyPlayedComponent");


    private recentlyPlayedCarousel$: Observable<ICarouselData> = this.store
                                                                     .select(selectRecentsContentCarousel)
                                                                     .pipe(filter(carousel => !!carousel));

    private recentlyPlayedTiles$: Observable<ITile[]> =
       this.recentlyPlayedCarousel$.pipe(
           filter(carousel => !!carousel),
            map(carousel =>
            {
                return carousel.tiles || [];
            }));

    /**
     * Component output that dispatches an event indicating a channel has been favorited. This allows the parent component
     * to listen to the event and act accordingly.
     */
    @Output() favorite = new EventEmitter();

    /**
     * noResultsPageName page/screen Name to render the RecentlyPlayed Component
     */
    public noResultsPageName: string = noResultsPagesConst.RECENTLY_PLAYED;

    /**
     *
     * @param {TranslateService} translate
     * @param {ContextMenuAnalyticsService} contextMenuAnalyticsService
     * @param {CarouselStoreService} carouselStoreService
     * @param {CarouselService} carouselService
     * @param {NeriticLinkService} neriticLinkService
     * @param {AppMonitorService} appMonitorService
     * @param {Store} store
     * @param appMonitorService
     */
    constructor(public translate: TranslateService,
                public tileContextMenuOptionsService: TileContextMenuOptionsService,
                private carouselStoreService: CarouselStoreService,
                private carouselService: CarouselService,
                private neriticLinkService: NeriticLinkService,
                private appMonitorService: AppMonitorService,
                private store: Store<IAppStore>)
    {
        RecentlyPlayedComponent.logger.debug("RecentlyPlayed Component Constructor");
    }

    ngOnInit()
    {
         this.carouselStoreService.selectRecents();
    }

    /**
     * Opens the Modal prompting the user to confirm clearing all the recents.
     */
    public openClearAllRecentsModal(): void
    {
        this.appMonitorService.triggerFaultError({ faultCode: AppErrorCodes.FLTT_RECENTLY_PLAYED_CLEAR_SELECTED });
    }


    public onClickTile(tile): void
    {
        const self = this;
        const options: NeriticLinkOptions = {
            onSuccess: () => { self.carouselStoreService.selectRecents(); },
            onError: () => {}
        };
        this.neriticLinkService.takePrimaryTileAction(tile, options);
    }


    public getCMOptionsFunc(tile): () => CMOption[]
    {
        return function()
        {
            return this.tileContextMenuOptionsService.getCMOptions(tile);
        }.bind(this);
    }
}
