import { TestBed } from '@angular/core/testing';
import { RecentlyPlayedComponent } from "./recently-played.component";
import { MockTranslateService } from "../../../test/mocks/translate.service";
import { TranslateService } from "@ngx-translate/core";
import { CarouselStoreService } from "../common/service/carousel.store.service";
import { CarouselService } from "../carousel/carousel.service";
import { NeriticLinkService } from "../neritic-links";
import { Store } from "@ngrx/store";
import { BehaviorSubject } from "rxjs";

import {
    AppErrorCodes
} from "sxmServices";
import { AppMonitorService } from "sxmServices";
import { ITile } from "sxmServices";
import { TileContextMenuOptionsService } from "app/context-menu/options/tile-context-menu-options.service";
import { TileContextMenuOptionsServiceMock } from "../../../test/mocks/tile-context-menu-options.service.mock";

describe('RecentlyPlayedComponent', function()
{
    beforeEach(function()
    {
        this.appMonitorService = {
            triggerFaultError: jasmine.createSpy('triggerFaultError')
        };

        this.carouselStoreService = {

        };

        this.carouselService = {

        };

        this.neriticLinkService = {
            takePrimaryTileAction: jasmine.createSpy('takePrimaryTileAction')
        };

        this.carouselStoreSubject = new BehaviorSubject(null);

        this.store = {
            select: () => this.carouselStoreSubject
        };

        TestBed.configureTestingModule({
            providers: [
                RecentlyPlayedComponent,
                { provide: TranslateService, useClass: MockTranslateService },
                { provide: AppMonitorService, useValue: this.appMonitorService },
                { provide: TileContextMenuOptionsService, useClass: TileContextMenuOptionsServiceMock },
                { provide: CarouselStoreService, useValue: this.carouselStoreService },
                { provide: CarouselService, useValue: this.carouselService },
                { provide: NeriticLinkService, useValue: this.neriticLinkService },
                { provide: NeriticLinkService, useValue: this.neriticLinkService },
                { provide: Store, useValue: this.store }
            ]
        });

        this.component = TestBed.get(RecentlyPlayedComponent);
        this.appMonitorService = TestBed.get(AppMonitorService);
    });

    describe('openClearAllRecentsModal()', function()
    {
        it('calls triggerFaultError() on appMonitorService', function()
        {
            this.component.openClearAllRecentsModal();
            expect(this.appMonitorService.triggerFaultError)
                .toHaveBeenCalledWith({ faultCode: AppErrorCodes.FLTT_RECENTLY_PLAYED_CLEAR_SELECTED });

        });
    });

    describe('onClickTile()', function()
    {
        it('calls takePrimaryTileAction() on neriticLinkService', function()
        {
            const tile = {} as ITile;
            this.component.onClickTile(tile);
            expect(this.neriticLinkService.takePrimaryTileAction)
                .toHaveBeenCalled();
        });
    });
});
