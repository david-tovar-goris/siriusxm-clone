import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { OrderByPipe } from "../common/pipe/order-by.pipe";
import { TimeFormatPipe } from "../common/pipe/time-format.pipe";
import { LocalizeTileTextPipe } from "../common/pipe/localize-tile-text.pipe";
import { FilterPipe } from "../filter/filter.pipe";
import { DynamicLoadingScreenComponent } from "./component/loading-screen/loading-screen.component";
import { DynamicModalComponent } from "./component/modal/modal.component";
import { DynamicSplashScreenComponent } from "./component/splash-screen/splash-screen.component";
import { StickyDirective } from "./directive/sticky/sticky.directive";
import { CarouselStoreService } from "./service/carousel.store.service";
import { FlepzScreenService } from "./service/flepz/flepz-screen.service";
import { LoadingScreenService } from "./service/loading-screen/loading-screen.service";
import { ModalService } from "./service/modal/modal.service";
import { NowPlayingIndicatorService } from "./service/now-playing-indicator/now-playing-indicator.service";
import { SplashScreenService } from "./service/splash-screen/splash-screen.service";
import { StickyService } from "./service/sticky/sticky.service";
import { TuneClientService } from "./service/tune/tune.client.service";
import { FooterComponent } from "../footer/footer.component";
import { TranslationModule } from "../translate/translation.module";
import { LoadingSpinnerComponent } from "./component/loading-spinner/loading-spinner.component";
import { ProgressCursorService } from "./service/progress-cursor/progress-cursor.service";
import { ProgressCursorDirective } from "./directive/progress-cursor/progress-cursor.directive";
import { LanguageToggleComponent } from "./component/language-toggle/language-toggle.component";
import { ImageDownloadFailed } from "./directive/image-download-failed.directive";
import { SxmNewAnalyticsDirective } from "../analytics/sxm-analytics.directive";
import { EllipsisDirective } from "app/common/directive/ellipsis/ellipsis.directive";
import { OverlayContainerComponent } from "app/common/component/overlay-container/overlay-container.component";
import { CountDownTimerComponent } from './component/count-down-timer/count-down-timer.component';

@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        LazyLoadImageModule
    ],
    declarations: [
        EllipsisDirective,
        SxmNewAnalyticsDirective,
        StickyDirective,
        ImageDownloadFailed,
        DynamicModalComponent,
        FilterPipe,
        OrderByPipe,
        TimeFormatPipe,
        LocalizeTileTextPipe,
        DynamicSplashScreenComponent,
        DynamicLoadingScreenComponent,
        FooterComponent,
        LoadingSpinnerComponent,
        ProgressCursorDirective,
        LanguageToggleComponent,
        OverlayContainerComponent,
        CountDownTimerComponent
    ],
    exports: [
        EllipsisDirective,
        SxmNewAnalyticsDirective,
        StickyDirective,
        ImageDownloadFailed,
        FilterPipe,
        OrderByPipe,
        TimeFormatPipe,
        LocalizeTileTextPipe,
        FooterComponent,
        LoadingSpinnerComponent,
        ProgressCursorDirective,
        LanguageToggleComponent,
        LazyLoadImageModule,
        OverlayContainerComponent,
        CountDownTimerComponent
    ],
    providers: [
        StickyService,
        ModalService,
        CarouselStoreService,
        SplashScreenService,
        LoadingScreenService,
        FlepzScreenService,
        NowPlayingIndicatorService,
        TuneClientService,
        ProgressCursorService
    ],
    entryComponents: [
        DynamicModalComponent,
        DynamicSplashScreenComponent,
        DynamicLoadingScreenComponent
    ]
})

export class SharedModule
{
}
