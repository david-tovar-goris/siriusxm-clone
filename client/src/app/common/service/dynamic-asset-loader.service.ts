import { filter, take } from 'rxjs/operators';
import { Inject, Injectable } from "@angular/core";
import {
    AssetLoaderService,
    IInternalAsset,
    IExternalAsset,
    ASSET_KIND_CONST,
    IAsset
} from "sxmServices";
import { DOCUMENT } from '@angular/common';
import { Subject ,  fromEvent } from "rxjs";

@Injectable()
export class DynamicAssetLoaderService
{
    public isAssetLoaded$: Subject<{ assetName: string, loaded: boolean }> = new Subject();

    constructor(private assetLoaderService: AssetLoaderService,
                @Inject(DOCUMENT) private document: any)
    {
    }

    public init()
    {
        this.assetLoaderService.asset$
            .subscribe((asset: IAsset) =>
            {
                switch (asset.kind)
                {
                    case(ASSET_KIND_CONST.EXTERNAL):
                        return this.loadExternalScript(asset as IExternalAsset);
                    case(ASSET_KIND_CONST.INTERNAL):
                        return this.loadInternalScript(asset as IInternalAsset);
                }
            });
    }

    /**
     * Used to Load SCRIPT Asset
     * @param asset
     */
    public loadInternalScript(asset: IInternalAsset)
    {
        let script = this.document.createElement('script');
        script.type = 'text/javascript';
        script.src = `${asset.dirPath}`;
        this.performAssetOnloadCallback(script, asset);
        this.document.getElementsByTagName('body')[0].appendChild(script);
    }

    public loadExternalScript(asset: IExternalAsset)
    {
        let script = this.document.createElement('script');
        script.type = 'text/javascript';
        script.src = asset.url.indexOf("https://") === -1 && asset.url.indexOf("http://") === -1
                        ? `https://${asset.url}` : `${asset.url}`;
        this.performAssetOnloadCallback(script, asset);
        this.document.getElementsByTagName('body')[0].appendChild(script);
    }

    private performAssetOnloadCallback(script, asset: IAsset)
    {
        if (script.readyState)
        {
            fromEvent(script, 'readystatechange').pipe(
                filter(() => script.readyState === "loaded" || script.readyState === "complete"),
                take(1)
            ).subscribe(() =>
                {
                    asset.onLoadCallback();
                    script.onreadystatechange = null;
                    this.isAssetLoaded$.next({ assetName: asset.name, loaded: true });
                });
        }
        else
        {
            fromEvent(script, 'load').pipe(
                take(1)
            ).subscribe(() =>
            {
                if(asset.onLoadCallback)
                {
                    asset.onLoadCallback();
                }
                this.isAssetLoaded$.next({ assetName: asset.name, loaded: true });
            });
        }
    }
}
