import { Injectable } from "@angular/core";
import {
    IChannel,
    CarouselTypeConst,
    IMediaShow,
    Logger,
    neriticActionConstants, CarouselConsts
} from "sxmServices";
import { ChannelListStoreService } from "app/common/service/channel-list.store.service";
import { NavigationService } from "app/common/service/navigation.service";
import { appRouteConstants } from "app/app.route.constants";

@Injectable()
export class ShowService
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ShowService");

    constructor(public channelListStoreService: ChannelListStoreService,
                private navigationService: NavigationService) {}

    /**
     * Select channel, and show then navigate to Enhanced Show EDP page
     */
    public selectShow (channel: IChannel, show: IMediaShow): void
    {
        let url =
                `${CarouselConsts.PAGE_NAME}=${CarouselTypeConst.ENHANCED_SHOW_EDP}&showGuid=${show.assetGUID}`;
        this.navigationService.go([appRouteConstants.ENHANCED_EDP, url, { queryParamsHandling: "merge" }]);
    }
}
