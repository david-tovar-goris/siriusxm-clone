import { TestBed } from "@angular/core/testing";
import { Logger } from "sxmServices";

import { ChannelListStoreService } from "app/common/service/channel-list.store.service";
import { ChannelListServiceMock } from "../../../../../test/mocks/channel-list.service.mock";

import { channelMock } from "../../../../../test/mocks/data/channel.mock";
import { onDemandShowMock } from "../../../../../test/mocks/data/ondemand-show.mock";

import { NavigationService } from "app/common/service/navigation.service";
import { navigationServiceMock } from "../../../../../test/mocks/navigation.service.mock";

import { ShowService } from "./show.service";

describe("ShowService", () =>
{

    let service: ShowService;

    beforeEach(() =>
    {

        Logger.level = Logger.LEVEL_ERROR;

        TestBed.configureTestingModule({
            providers: [
                ShowService,
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: NavigationService, useValue: navigationServiceMock }
            ]
        });

        service = TestBed.get(ShowService);

    });

    afterEach(() =>
    {
        service = null;
        Logger.level = Logger.LEVEL_DEBUG;
    });

    it("should be truthy", () =>
    {
        expect(service).toBeTruthy();
    });

    it("should be able to `selectShow()`", () =>
    {
        spyOn(service, "selectShow");
        service.selectShow(channelMock, onDemandShowMock);
        expect(service.selectShow).toHaveBeenCalled();
    });

});
