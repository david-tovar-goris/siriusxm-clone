import {
    ComponentFactoryResolver,
    Injectable, ViewContainerRef
} from "@angular/core";
import {DynamicSplashScreenComponent} from "../../component/splash-screen/splash-screen.component";
import { AppMonitorService, AppErrorCodes } from "sxmServices";

@Injectable()
export class SplashScreenService
{
    private splashIsOpen: boolean = false;
    private splashScreenInstance;
    private splashScreenTimer: NodeJS.Timer;
    private SPLASH_SCREEN_TIMEOUT_REPORT_LENGTH = 5000;

    constructor(private factoryResolver: ComponentFactoryResolver,
                private appMonitorService: AppMonitorService){}

    /**s
     * Creates the Dynamic Splash Screen
     */
    public addDynamicSplashScreen(viewContainerRef: ViewContainerRef): void
    {
        const component = this.factoryResolver.resolveComponentFactory(DynamicSplashScreenComponent);
        this.splashScreenInstance = viewContainerRef.createComponent(component);
        this.splashIsOpen = true;
        document.getElementById("loadingText").focus(); // necessary for screen reader to announce 'loading' alert
        this.splashScreenTimer = setTimeout(() =>
        {
            this.appMonitorService.triggerFaultError({ faultCode: AppErrorCodes.FLTT_LOADING_SHOWN_MORE_THAN_5_SECONDS });
        }, this.SPLASH_SCREEN_TIMEOUT_REPORT_LENGTH);
    }

    public closeSplashScreen(): void
    {
        clearTimeout(this.splashScreenTimer);
        if (!this.splashScreenInstance) return;
        this.splashScreenInstance.destroy();
    }
}
