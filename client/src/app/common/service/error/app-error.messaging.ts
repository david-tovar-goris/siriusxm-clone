import {
    ApiCodes,
    AppMonitorService,
    AuthenticationService,
    IAppMonitor,
    IAppErrorState,
    APP_ERROR_STATE_CONST,
    SessionMonitorService,
    MediaPlayerService
}                                       from "sxmServices";
import { Injectable } from "@angular/core";
import { TranslateService }             from "@ngx-translate/core";
import { ModalService }                 from "../modal/modal.service";
import { OverlayService }               from "../overlay/overlay.service";

/**
 * @MODULE:     Client
 * @CREATED:    03/07/18
 * @COPYRIGHT:  2018 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  AppErrorMessaging is used Display error/warning to User and respective code
 */


@Injectable()
export class AppErrorMessaging
{
    private isMediaContentPlaying: boolean;

    private appErrorState: IAppErrorState = {} as IAppErrorState;

    constructor(private translate: TranslateService,
                private modalService: ModalService,
                private overlayService: OverlayService,
                private mediaPlayerService: MediaPlayerService,
                private appMonitorService: AppMonitorService,
                private authenticationService: AuthenticationService,
                private sessionMonitorService: SessionMonitorService)
    {
        // this.appMonitorService.appMessage.subscribe((data: IAppMonitor) =>
        // {
        //     this.displayErrorMessage(data.message);
        // });

        this.sessionMonitorService.appErrorState.subscribe((appErrorState : IAppErrorState)=>
        {
            this.appErrorState = appErrorState;

            //This is if user is already playing and we stopped the media-- it will resume back once the SIMULTANEOUS_LISTEN_DISABLED
            if(this.isMediaContentPlaying
               && appErrorState.IS_SIMULTANEOUS_LISTEN === APP_ERROR_STATE_CONST.SIMULTANEOUS_LISTEN_DISABLED)
            {
                this.mediaPlayerService.mediaPlayer.play();
            }
        });
    }

    /**
     * Checks the API error code and displays the relative message
     */
    public displayErrorMessage(appMessage): void
    {
        let overlayData:any;

        switch (appMessage.apiCode)
        {

            case ApiCodes.AOD_EXPIRED_CONTENT:

                //We are not maintaining the aodExpired state in appErrorState
                // as we need show the Message everyTime episode failed
                overlayData = this.translate.instant('errors.aodExpired');
                overlayData.errorCode = appMessage.code;
                this.overlayService.open({
                    overlayData: overlayData
                });
                break;

            case ApiCodes.CHANNEL_NOT_AVAILABLE:

                overlayData = this.translate.instant('errors.unavailableNowPlayChannel');
                overlayData.errorCode = appMessage.code;
                this.overlayService.open({
                    overlayData: overlayData
                });
                break;

            default:
                break;
        }
    }
}
