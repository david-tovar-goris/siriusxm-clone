import { Injectable } from "@angular/core";
import { Location } from "@angular/common";
import { appRouteConstants } from "app/app.route.constants";
import {
    AuthenticationService,
    AppMonitorService,
    APP_ERROR_STATE_CONST,
    SessionMonitorService,
    ClientFault,
    StorageService,
    AlertType,
    TunePayload,
    AppErrorCodes,
    GeolocationService,
    TuneService,
    ConfigService,
    SettingsService
} from "sxmServices";
import { EErrorTemplate } from "./error-template.enum";
import { ClientInactivityService } from "../../../inactivity/client-inactivity.service";
import { RecentlyPlayedStoreService } from "../../../common/service/recently-played.store.service";
import { AlertClientService } from "../alert/alert.client.service";
import { SettingsConstants } from "sxmServices";
import { NavigationService } from "../navigation.service";
import { FlepzScreenService } from "../flepz/flepz-screen.service";
import { languageConstants } from "../../component/language-toggle/language.consts";
import { TranslateService } from "@ngx-translate/core";
import { BrowserUtil } from "app/common/util/browser.util";

@Injectable()
export class FaultMap
{
    public map: Map<number, IFaultData> = new Map<number, IFaultData>([
        [21008, {
            code: 21008,
            name: "FLTT_GUP_LEVEL_BYPASS_MANAGE_NOTIFICAIONS_DISABLED",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.gupLevelBypass",
            button1Action: null,
            button2Action: null
        }],
        [21009, {
            code: 21009,
            name: "FLTT_GUP_ID_UNAVAILABLE_NEW_USER_BYPASS_INITIATION ",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.gupIdUnavailable",
            button1Action: null,
            button2Action: null
        }],
        [21007, {
            code: 21007,
            name: "FLTT_GUP_LEVEL_BYPASS_INITIATION ",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.gupIdUnavailable",
            button1Action: null,
            button2Action: null
        }],
        [21020, {
            code: 21020,
            name: "FLTT_CAROUSELS_BYPASS ",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.carouselBypass",
            button1Action: null,
            button2Action: null
        }],
        [21017, {
            code: 21017,
            name: "FLTT_SEARCH_UNAVAILABLE_OR_BYPASS ",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.searchBypass",
            button1Action: null,
            button2Action: null
        }],
        [21019, {
            code: 21019,
            name: "FLTT_VOD_BYPASS",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.vodBypass",
            button1Action: null,
            button2Action: null
        }],
        [21014, {
            code: 21014,
            name: "FLTT_VOD_BYPASS ",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.vodBypass",
            button1Action: null,
            button2Action: null
        }],
        [21010, {
            code: 21010,
            name: "FLTT_AIC_BYPASS ",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.aicBypass",
            button1Action: null,
            button2Action: null
        }],
        [21024, {
            code: 21024,
            name: "FLTT_ARTIST_RADIO_BYPASS ",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.seededBypass",
            button1Action: null,
            button2Action: null
        }],
        [25005, {
            code: 25005,
            name: "FLTT_LOGOUT",
            type: EErrorTemplate.MODAL,
            translationKey: "whatever",
            button1Action: () =>
            {
                this.authenticationService.logout()
                    .subscribe(onLogout.bind(this),onLogout.bind(this));

                function onLogout()
                {
                    this.location.go(`${ appRouteConstants.AUTH.LOGIN }`);
                    window.location.reload();
                }
            },
            button2Action: null
        }],
        [40002, {
            code: 40002,
            name: "FLTT_CONTENT_NOT_IN_PACKAGE_BUSINESS",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.notInPackage",
            button1Action: null,
            button2Action: null
        }],
        [28029, {
          code: 28029,
          name: "FLTT_IT_SYSTEMS_DOWN",
          type: EErrorTemplate.MODAL,
          translationKey: "errors.modals.technicalDifficulties",
          button1Action: () =>
          {
            this.authenticationService.reclaimSession();
          },
          button2Action: () =>
          {
            window.location.reload();
          }
        }]
    ]);

    constructor(
        private authenticationService: AuthenticationService,
        private location: Location,
        private appMonitorService: AppMonitorService,
        private sessionMonitorService: SessionMonitorService,
        private clientInactivityService: ClientInactivityService,
        private recentlyPlayedStoreService: RecentlyPlayedStoreService,
        private alertClientService: AlertClientService,
        private settingsService: SettingsService,
        private geolocationService: GeolocationService,
        private tuneService: TuneService,
        private navigationService: NavigationService,
        private flepzScreenService: FlepzScreenService,
        private configService: ConfigService,
        private translate: TranslateService,
        storageService: StorageService
    )
    {
        this.map.set(13001, {
            code: 13001,
            name: "FLTT_INACTIVITY_TIMEOUT",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.inactivity",
            button1Action: () =>
            {
                this.clientInactivityService.continueListening();
            },
            button2Action: () => {}
        });
        this.map.set(13003, {
            code: 13003,
            name: "FLTT_SIMULTANEOUS_LISTEN",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.simultaneousLogin",
            button1Action: () =>
            {
                this.authenticationService.reclaimSession();
            },
            button2Action: () =>
            {
                this.sessionMonitorService.updateSimultaneousListen(APP_ERROR_STATE_CONST.SIMULTANEOUS_LISTEN_CANCELED);
            }
        });
        this.map.set(13005, {
            code: 13005,
            name: "FLTT_SIMULTANEOUS_LISTEN_WEB",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.simultaneousLogin",
            button1Action: () =>
            {
                this.authenticationService.reclaimSession();
            },
            button2Action: () =>
            {
                this.sessionMonitorService.updateSimultaneousListen(APP_ERROR_STATE_CONST.SIMULTANEOUS_LISTEN_CANCELED);
            }
        });
        this.map.set(30003, {
            code: 30003,
            name: "FLTT_UNAVAILABLE_CONTENT",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.unavailableContent",
            getInterpolationKey: (fault: ClientFault) =>
            {
                let key = "errors.unavailableContent.interpolation.";
                if (fault.url.indexOf('live') >= 0)
                {
                    return key + "channel";
                }
                return key + "show";
            },
            button1Action: null,
            button2Action: null
        });
        this.map.set(29330, {
            code: 29330,
            name: "FLTT_STREAMING_GAME_BLACKOUT",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.gameBlockOut",
            button1Action: null,
            button2Action: null
        });
        this.map.set(30004, {
            code: 30004,
            name: "FLTT_UNAVAILABLE_CONTENT_FROM_LOCAL_CACHE",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.unavailableContentFromLocalCache",
            getInterpolationKey: (fault: ClientFault) =>
            {
                let key = "errors.unavailableContentFromLocalCache.interpolation.";
                if (fault.url.indexOf('live') >= 0)
                {
                    return key + "channel";
                }
                return key + "episode";
            },
            button1Action: null,
            button2Action: null
        });
        /*this.map.set(28023, {
            code: 28023,
            name: "GUP_UPDATE_FAILED",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.gupUpdateFailure",
            button1Action: null,
            button2Action: null
        });*/
        this.map.set(40001, {
            code: 40001,
            name: "FLTT_CONTENT_NOT_IN_SUBSCRIPTION_PACKAGE",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.contentNotInSubscriptionPackage"
        });
        this.map.set(17002, {
            code: 17002,
            name: "FLTT_UNAVAILABLE_GUP_DATA",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.gupUnavailable",
            button1Action: null,
            button2Action: null
        });
        this.map.set(14020, {
            code: 14020,
            name: "FLTT_AOD_UNAVAILABLE",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.aodUnavailable"
        });
        this.map.set(15004, {
            code: 15004,
            name: "FLTT_RESUME_ERROR",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.technicalDifficultiesWithSorry",
            button1Action: null,
            button2Action: null
        });
        this.map.set(29020, {
            code: 29020,
            name: "FLTT_HTTP_AUTHENTICATION_FAILURE ",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.httpAuthFailure"
        });
        this.map.set(9000, {
            code: 9000,
            name: "FLTT_TUNE_NON_RECOVERABLE_FAILURE",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.technicalDifficultiesWithSorry"
        });
        this.map.set(2020, {
            code: 2020,
            name: "FLTT_NO_IP_CONNECTION",
            type: EErrorTemplate.TOAST_WITHOUT_CTA,
            translationKey: "errors.noIPConnection"
        });
        this.map.set(28010, {
            code: 28010,
            name: "FLTT_RECENTLY_PLAYED_CLEAR_SELECTED",
            type: EErrorTemplate.MODAL,
            translationKey: "recentlyPlayed.recentsModal",
            button1Action: () =>
            {
                this.recentlyPlayedStoreService.removeAllRecentlyPlayed();
            },
            button2Action: () =>
            {
                this.sessionMonitorService.updateSimultaneousListen(APP_ERROR_STATE_CONST.SIMULTANEOUS_LISTEN_CANCELED);
            }
        });
        this.map.set(14013, {
            code: 14013,
            name: "FLTT_EXPIRED_AOD_CONTENT",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.aodExpired"
        });
        this.map.set(30005, {
            code: 30005,
            name: "FLTT_UNAVAILABLE_NOW_PLAY_CHANNEL",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.unavailableNowPlayChannel"
        });
        this.map.set(29902, {
            //we want to display the orignal 201 fault code here
            code: 29002,
            name: "FLTT_AUTHENTICATION_REQUIRED",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.unexpectedAuthRequired",
            onClose: () =>
            {
                storageService.removeItem('showAuthFailureMessage');
            }
        });
        this.map.set(26001, {
            code: 26001,
            name: "FLTT_CONTENT_ALERT",
            type: EErrorTemplate.TOAST_WITHOUT_CTA,
            translationKey: "reminders.setShowReminderSuccess"
        });
        this.map.set(90000, {
            code: 90000,
            name: "FAUX_LIVE_VIDEO_REMINDER_MODAL",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.wantToBeNotifiedLiveVideo",
            button1Action: (context) =>
            {
                this.alertClientService.createAlert(
                    'howardstern100',
                    context.showGUID,
                    AlertType.LIVE_VIDEO_START
                );
            },
            button2Action: () => {}
        });
        this.map.set(9012, {
            code: 9012,
            name: "FLTT_VIDEO_RECOVERY_TIMEOUT_SYSTEM_ERROR",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.videoRecoveryTimeoutSystemError"
        });
        this.map.set(1000, {
            code: 1000,
            name: "FLTT_CHROME_CAST_GENERIC_ERROR",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.chromecastGenericError"
        });
        this.map.set(1001, {
            code: 1001,
            name: "FLTT_CHROME_CAST_SESSION_TRANSFER_ERROR",
            type: EErrorTemplate.OVERLAY,
            translationKey: "errors.chromecastSessionTransferError"
        });
        this.map.set(1002, {
            code: 1002,
            name: "FLTT_CHROME_CAST_CONTENT_NOT_SUPPORTED_VOD",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.chromecastContentNotSupportedVOD"
        });
        this.map.set(1004, {
            code: 1004,
            name: "FLTT_CHROME_CAST_CONTENT_NOT_SUPPORTED_PODCAST",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.chromecastContentNotSupportedPodcast"
        });
        this.map.set(103, {
            code: 103,
            name: "FLTT_SATELLITE_ACCOUNT_ONLY",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.noSxir",
            button1Action: () =>
            {
                this.openIframeResource("getStarted", "iframe_getStarted");
            }
        });
        this.map.set(29017, {
            code: 29017,
            name: "FLTT_NO_STREAMING_ACCESS",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.selectLiteSxir",
            button1Action: () =>
            {
                this.authenticationService.logout()
                    .subscribe(onLogout.bind(this),onLogout.bind(this));

                function onLogout()
                {
                    this.storageService.clearAll();
                    this.location.go(`${appRouteConstants.AUTH.LOGIN}`);
                    BrowserUtil.reload();
                }
            }
        });
        this.map.set(115, {
            code: 115,
            name: "FLTT_MULTIPLE_SXIR",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.multipleSxir",
            button2Action: () =>
            {
                this.openIframeResource("forgotUsernamePassword", "iframe_forgotUsernamePassword");
            }
        });
        this.map.set(324, {
            code: 324,
            name: "STATION_LIMIT_REACHED",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.stationMaxLimitReached",
            button1Action: () =>
            {
                this.navigationService.go([appRouteConstants.SEEDED_SETTINGS]);
            },
            button2Action: () => {}
        });
        this.map.set(303, {
            code: 303,
            name: "FLTT_LOCATION_RESTRICTED_CONTENT",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.geolocationDisallowed",
            button1Action: () =>
            {
                this.retryGeoLocation();
            }
        });
        this.map.set(307, {
            code: 307,
            name: "GEO_LOCATION_UNAVAILABLE",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.geolocationUndetermined",
            button1Action: () =>
            {
                this.retryGeoLocation();
            }
        });
        this.map.set(315, {
            code: 315,
            name: "FLTT_SR_SOURCE_ENDED",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.pandoraGenericError"
        });
        this.map.set(325, {
            code: 325,
            name: "FLTT_SR_NO_CLEAN_CONTENT",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.pandoraGenericError"
        });
        this.map.set(600, {
            code: 600,
            name: "FLTT_MODULE_ERROR",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.genericError"
        });
        this.map.set(8801, {
            code: 8801,
            name: "FLTT_DECODER_FAILURE",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.pandoraGenericError"
        });
        this.map.set(90001, {
            code: 90001,
            name: "FAUX_UNSUBSCRIBE_TO_SUBSCRIBE_MODAL",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.unsubscribeToSubscribe",
            button1Action: () =>
            {
                this.settingsService.switchGlobalSettingOnOrOff(
                    true, // turn it on
                    SettingsConstants.NOTIFICATION_SUBSCRIPTION_SHOW_REMINDERS);
            },
            button2Action: () => {}
        });
        this.map.set(90002, {
            code: 90002,
            name: "FLTT_PLAYBACK_FAILURE",
            type: EErrorTemplate.MODAL,
            translationKey: "errors.modals.pandoraGenericError"
        });
    }

    public openIframeResource(iFrameType: string, iFrameElementId: string): void
    {
        this.flepzScreenService.enableIframe(iFrameType);
        const localeKey          = this.translate.currentLang === languageConstants.ENGLISH_CANADA ?
                                   languageConstants.ENGLISH : this.translate.currentLang;
        const onBoardingSettings = this.configService.getOnBoardingSettings(localeKey);
        let resourceUrl          = "",
            iFrame;
        switch (iFrameType)
        {
            case "getStarted":
                resourceUrl = onBoardingSettings.getStartedUrl;
                break;
            case "forgotUsernamePassword":
                resourceUrl = onBoardingSettings.passwordRecoveryUrl;
                break;
        }
        if (resourceUrl.length > 0)
        {
            setTimeout(() =>
            {
                iFrame = document.getElementById(iFrameElementId);
                if (iFrame)
                {
                    iFrame.setAttribute('src', resourceUrl);
                }
            }, 100);
        }
    }

    public retryGeoLocation(): void
    {
        const lastCheckedContent = this.geolocationService.getLastCheckedContent();
        const payLoad: TunePayload = {
            channelId : lastCheckedContent.channelId,
            episodeIdentifier: lastCheckedContent.episodeId,
            contentType : lastCheckedContent.contentType
        };

        this.tuneService.tune(payLoad).subscribe();

        this.appMonitorService.triggerFaultError({faultCode: AppErrorCodes.FLTT_VERIFYING_LOCATION});
    }
}

export interface IFaultData
{
  code: number;
  name: string;
  type: EErrorTemplate;
  translationKey: string;
  getInterpolationKey?: (fault: ClientFault) => string;
  button1Action?: (arg: any) => void;
  button2Action?: (arg: any) => void;
  onClose?: () => void;
}
