/**
 * Describes the template/experience to use for an error.
 * Errors are presented to the user as one of five archtypes.
 */
export enum EErrorTemplate {
    /**
     * Overlays are used when there is more information
     * than what can be conveyed in a toast,
     * but there is no action that the user needs to take.
     */
    OVERLAY,

    /**
     * Modal errors are used when the user must make a decision.
     * Since modals cannot be dismissed, they should only be
     * used when a user decision is necessary to continue.
     */
    MODAL,

    /**
     * This type of toast is used when the user needs to be informed
     * of something but there is no action that can be taken.
     */
    TOAST_WITHOUT_CTA,

    /**
     * These are page specific and use the orange color to
     * differentiate from the content currently on screen.
     */
    INLINE
}
