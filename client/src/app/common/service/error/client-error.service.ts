import { combineLatest as observableCombineLatest, Observable, SubscriptionLike as ISubscription } from 'rxjs';
import { take, map, tap, filter, distinctUntilChanged } from 'rxjs/operators';
import { ApplicationRef, Injectable } from "@angular/core";
import { TranslateDefaultParser, TranslateService } from "@ngx-translate/core";

import {
    ApiCodes,
    ApiLayerTypes,
    AppErrorCodes,
    AppMonitorService,
    ClientFault,
    EError,
    ErrorService,
    IErrorInformation,
    Logger,
    MediaPlayerService
} from "sxmServices";

import { OverlayData } from "../../component/overlay/overlay.interface";
import { ToastData, ToastInformation } from "../../component/toast/toast.interface";
import { IModalData } from "../modal/modal.interface";
import { ModalService } from "../modal/modal.service";
import { ToastService } from "../toast/toast.service";
import { EErrorTemplate } from "./error-template.enum";
import { FaultMap, IFaultData } from "./fault.map";
import { BrowserUtil } from "../../util/browser.util";

type ErrorData = IModalData | ToastData | OverlayData;

@Injectable()
export class ClientErrorService {
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ClientErrorService");

    public offlineSubscription: ISubscription;

    public reminderToastSubscription: ISubscription;

    /**
     * Constructor.
     * @param {ErrorService} errorService
     * @param {ToastService} toastService
     * @param {TranslateService} translator
     * @param {ModalService} modalService
     * @param {ApplicationRef} applicationRef
     * @param {AppMonitorService} appMonitorService
     */
    constructor(private errorService: ErrorService,
                private toastService: ToastService,
                private translator: TranslateService,
                private modalService: ModalService,
                private applicationRef: ApplicationRef,
                private appMonitorService: AppMonitorService,
                private mediaPlayerService: MediaPlayerService,
                private faultMap: FaultMap) {}
    /**
     * Allows observables to be registered with the client error service to display errors when
     * certain conditions are met.
     *
     * Because of the fact that observables are lazy, they do not run until subscribe is called,
     * we can build up complex logic to determine when to display an error and what kind of error it is.
     *
     * This method will subscribe to the observable and wait for it to emit a value.
     * Once a value is received it will determine the correct template to be shown and
     * handle the displaying of the error.
     *
     * @param {Observable<[ EError, ErrorData]>} obs
     * @returns {ISubscription}
     * @memberof ClientErrorService
     */
    public registerErrorObservable(obs: Observable<[EError, boolean, ErrorData]>): ISubscription
    {
        return obs.pipe(
            map(errorTuple => this.determineTemplate(errorTuple)),
            tap(this.handleError.bind(this)))
            .subscribe();
    }

    private onModalClose( onCloseCallback: ()=>void ): void
    {
        if( typeof onCloseCallback === "function")
        {
            onCloseCallback();
        }
        this.appMonitorService.topFaultHandled();
        this.handleModalQueue();
    }

    private getFaultData(fault: any) : IFaultData
    {
        let faultData: IFaultData = this.faultMap.map.get(fault.faultCode.faultCode);
        if (!faultData && fault.apiCode)
        {
            let apiFaultData: IFaultData = this.faultMap.map.get(fault.apiCode);
            if (apiFaultData)
            {
                faultData = apiFaultData;
                faultData.code = fault.faultCode.faultCode;
            }
        }
        return faultData;
    }

    private handleModalQueue(): void
    {
        const fault = this.appMonitorService.getNextFault();
        if(this.appMonitorService.hasQueuedFaults()
            && (!this.modalService.modalIsOpen ||
                (fault && fault.apiCode && fault.apiCode === ApiCodes.ARTIST_RADIO_STATION_LIMIT_REACHED)))
        {
            const faultData: IFaultData = this.getFaultData(fault);

            if (fault.apiCode === ApiCodes.GEO_LOCATION_UNAVAILABLE || fault.apiCode === ApiCodes.GEO_LOCATION_ERROR)
            {
                this.mediaPlayerService.mediaPlayer.stop();
            }

            //temporary while we're moving all errors to use the new thing
            //later we can remove this so we have a good signal for when an error isnt defined in fault.map
            //maybe replace it with a throw error to make it eaiser for dev to read
            //clear out current before returning
            if (!faultData || this.shouldSkipFaultForCanada(fault))
            {
                this.appMonitorService.topFaultHandled();
                return;
            }

            if (faultData.type === EErrorTemplate.MODAL || faultData.type === EErrorTemplate.OVERLAY)
            {
                let translation = this.translator.instant(faultData.translationKey);

                if (faultData.getInterpolationKey)
                {
                    let interpolationKey = faultData.getInterpolationKey(fault);
                    let params = { interpolation: this.translator.instant(interpolationKey) };
                    Object.keys(translation)
                        .forEach(key =>
                    {
                        translation[key] = (new TranslateDefaultParser()).interpolate(translation[key], params);
                    });
                }

                if(fault.faultCode.faultCode === AppErrorCodes.FLTT_CHROME_CAST_CONTENT_NOT_SUPPORTED_VOD.faultCode ||
                   fault.faultCode.faultCode === AppErrorCodes.FLTT_CHROME_CAST_CONTENT_NOT_SUPPORTED_AIC.faultCode)
                {
                    translation.description =  translation.description + fault.metaData.description;
                }
                const modalData: IModalData = { ...translation } as IModalData;
                modalData.modalType = faultData.type;

                if (faultData.code
                    && faultData.code <= AppErrorCodes.FIRST_FAKE_FAULT.faultCode)
                {
                    modalData.errorCode = faultData.code.toString();
                }

                modalData.onClose = this.onModalClose.bind(this, faultData.onClose);

                if (faultData.button1Action)
                {
                    modalData.buttonOne = {
                        text: translation.buttonOne.text,
                        action: faultData.button1Action.bind(this.faultMap, fault.context)
                    };
                }

                if (faultData.button2Action)
                {
                    modalData.buttonTwo = {
                        text: translation.buttonTwo.text,
                        action: faultData.button2Action.bind(this.faultMap, fault.context)
                    };
                }

                this.openModal(modalData);
            }
        }
    }

    /**
     * checks if certain error codes can be skipped for Canada
     */
    public shouldSkipFaultForCanada(fault: ClientFault) : boolean
    {
        /*skip 103 & 115 API codes for Canada*/
        const skipCodeList = [ApiCodes.MULTIPLE_SXIR, ApiCodes.SATELLITE_ACCOUNT_ONLY];
        return BrowserUtil.getAppRegion() === ApiLayerTypes.REGION_CA && skipCodeList.indexOf(fault.apiCode) >= 0;
    }

    /**
     * Initialization method.
     * Subscribes to the ErrorInformation observable.
     *
     * When the toastInformation.isInErrorState value changes to true, display our overlay/modal/toast
     * When the toastInformation.isInErrorState value changes to false, remove our overlay/modal/toast
     */
    public init(): void
    {
        ClientErrorService.logger.debug("init()");


        this.appMonitorService.newFaults.pipe(filter(Boolean)).subscribe(() =>
        {
            this.handleModalQueue();
        });


        this.appMonitorService.faultStream.pipe(filter( (fault) =>
        {
            const faultData: IFaultData = this.getFaultData(fault);
            return faultData && faultData.type !== EErrorTemplate.MODAL;
        }))
        .subscribe( (fault: ClientFault) =>
        {
            //do something here
        });

        const restartError = this.errorService.errorInformation.pipe(
            filter(val => val.errorType === EError.RESTART));

        this.registerErrorObservable(observableCombineLatest(restartError, this.translator.get("errors.restart"),
            (val: IErrorInformation, translation) =>
                [
                    val.errorType,
                    val.isInErrorState,
                    { messagePath: translation, isAltColor: false }
                ]
            ) as any as Observable<[ EError, boolean, ToastData ]>
        );

        /**
         * Error handler for an invalid url to media.
         * EError.INVALID_TOKEN
         * i18n: errors.restart
         */
        const invalidTokenError = this.errorService.errorInformation.pipe(
            filter(val => val.errorType === EError.INVALID_TOKEN),
            distinctUntilChanged());
        this.registerErrorObservable(
            observableCombineLatest(
                invalidTokenError,
                this.translator.get("errors.restart"),
                (error,
                 translation) =>
                [
                    error.errorType,
                    error.isInErrorState,
                    {
                        messagePath: translation,
                        closeToastTime: 10000
                    } as ToastData
                ] as [ EError, boolean, ToastData ]
            )
        );

        // If the app is auto-paused by Safari on initial playback show the user a modal telling them how to fix this.
        // This applies to video content.
        const safariAutoPlayPause = this.errorService.errorInformation.pipe(
            filter(val => val.errorType === EError.SAFARI_AUTO_PLAY_PAUSE),
            distinctUntilChanged());
        this.registerErrorObservable(
            observableCombineLatest(safariAutoPlayPause, this.translator.get("errors.safariAutoPlayPause"), (error, translation) =>
                [
                    error.errorType,
                    false,
                    { ...translation } as IModalData
                ] as [ EError, boolean, IModalData ]
            )
        );

        /**
         * Error handler for when autoplay stops an audio stream.
         * EError.PLAYBACK_DENIED
         * i18n: errors.audioPlaybackDenied
         */
        const audioPlaybackDenied = this.errorService.errorInformation.pipe(
            filter(val => val.errorType === EError.PLAYBACK_DENIED),
            distinctUntilChanged());
        this.registerErrorObservable(
            observableCombineLatest(audioPlaybackDenied, this.translator.get("errors.audioPlaybackDenied"), (error, translation) =>
                [
                    error.errorType,
                    false,
                    { ...translation } as IModalData
                ] as [ EError, boolean, IModalData ]
            )
        );

        /**
         * Error handler for when autoplay stops a video stream.
         * If autoplay blocks a video show the modal telling them to manually click the play button.
         * This is a generic version of safariAutoPlayPause above but doesn't show the Safari instructions.
         * EError: AUTOPLAY_DENIED
         * i18n:
         */
        const autoPlayDenied = this.errorService.errorInformation.pipe(
            filter(val => val.errorType === EError.AUTOPLAY_DENIED),
            distinctUntilChanged());
        this.registerErrorObservable(
            observableCombineLatest(autoPlayDenied, this.translator.get("errors.autoPlayDenied"), (error, translation) =>
                [
                    error.errorType,
                    false,
                    { ...translation } as IModalData
                ] as [ EError, boolean, IModalData ]
            )
        );

        // Error handler for buffer empty.
        const videoPlayerBufferEmpty = this.errorService.errorInformation.pipe(
            filter(val => val.errorType === EError.BUFFER_EMPTY),
            distinctUntilChanged());
        this.registerErrorObservable(
            observableCombineLatest(
                videoPlayerBufferEmpty, this.translator.get("errors.videoPlayerBufferEmpty"),
                (error, translation) =>
                    [
                        error.errorType,
                        error.isInErrorState,
                        { messagePath: translation, isAltColor: false }
                    ]
            ) as any as Observable<[ EError, boolean, ToastData ]>
        );

        this.offlineSubscription = this.registerErrorObservable(
            observableCombineLatest(
                this.errorService.errorInformation.pipe(filter(info =>
                {
                    return info.errorType === EError.NO_IP_CONNECTION;
                })),
                this.translator.get("errors.noIPConnection"),
                (info: IErrorInformation, translation) =>
                    [
                        info.errorType,
                        info.isInErrorState,
                        { messagePath: translation, isAltColor: true, errorType: info.errorType }
                    ]
            ) as any as Observable<[EError, boolean, ToastData]>
        );

        // Error handler for reminders set successfully.
        this.reminderToastSubscription = this.registerErrorObservable(
            this.errorService.errorInformation.pipe(filter(info =>
            {
                return info.errorType === EError.FLTT_CONTENT_ALERT
                  || info.errorType === EError.LIVE_VIDEO_REMINDER;
            }),map(info =>
            {
                let translation = info.errorType === EError.LIVE_VIDEO_REMINDER
                  ? this.translator.instant('reminders.setLiveVideoReminderSuccess')
                  : this.translator.instant('reminders.setShowReminderSuccess');

                return [
                    info.errorType,
                    info.isInErrorState,
                    { messagePath: translation, isAltColor: true, errorType: info.errorType, hasCloseButton: true, closeToastTime:10000 }
                ];
            })) as any as Observable<[EError, boolean, ToastData]>

        );

        this.registerErrorObservable(
            observableCombineLatest(
                this.errorService.errorInformation.pipe(filter(info =>
                {
                    return info.errorType === EError.FLTT_NO_TRACKS_TO_PLAY;
                })),
                this.translator.get("errors.noMoreTracksToPlay"),
                (info: IErrorInformation, translation) =>
                    [
                        info.errorType,
                        info.isInErrorState,
                        { messagePath: translation, isAltColor: false, errorType: info.errorType, hasCloseButton: true, closeToastTime:10000 }
                    ]
            ) as any as Observable<[EError, boolean, ToastData]>
        );

        this.registerErrorObservable(
            observableCombineLatest(
                this.errorService.errorInformation.pipe(filter(info =>
                {
                    return info.errorType === EError.FLTT_AIC_BYPASS;
                })),
                this.translator.get("errors.aicBypass.header"),
                (info: IErrorInformation, translation) =>
                    [
                        info.errorType,
                        info.isInErrorState,
                        { messagePath: translation, isAltColor: false, errorType: info.errorType, hasCloseButton: true, closeToastTime:10000 }
                    ]
            ) as any as Observable<[EError, boolean, ToastData]>
        );

        this.registerErrorObservable(
            observableCombineLatest(
                this.errorService.errorInformation.pipe(filter(info =>
                {
                    return info.errorType === EError.FLTT_ARTIST_RADIO_BYPASS;
                })),
                this.translator.get("errors.seededBypass.header"),
                (info: IErrorInformation, translation) =>
                    [
                        info.errorType,
                        info.isInErrorState,
                        { messagePath: translation, isAltColor: false, errorType: info.errorType, hasCloseButton: true, closeToastTime:10000 }
                    ]
            ) as any as Observable<[EError, boolean, ToastData]>
        );
    }

    /**
     * Determine which template to use based on the error type.
     * Ex: overlay/modal/toast
     */
    private determineTemplate([errorType, isInErrorState, errorData]: [EError, boolean, ErrorData]): [EErrorTemplate, boolean, ErrorData]
    {
        let errorTemplate: EErrorTemplate;

        switch(errorType)
        {
            case EError.BUFFER_EMPTY:
            case EError.RESTART:
            case EError.NO_IP_CONNECTION:
            case EError.FLTT_CONTENT_ALERT:
            case EError.LIVE_VIDEO_REMINDER:
            case EError.FLTT_NO_TRACKS_TO_PLAY:
            case EError.FLTT_ARTIST_RADIO_BYPASS:
            case EError.FLTT_AIC_BYPASS:
            case EError.INVALID_TOKEN:
                errorTemplate = EErrorTemplate.TOAST_WITHOUT_CTA;
                (<ToastData>errorData).errorType = errorType;
                break;

            case EError.GUP_ID_UNAVAILABLE_NEW_USER_BYPASS_INITIATION:
            case EError.GUP_LEVEL_BYPASS_INITIATION:
            case EError.GEORESTRICTED_CONTENT:
            case EError.GEOLOCATION_UNDETERMINED:
            case EError.SAFARI_AUTO_PLAY_PAUSE:
                errorTemplate = EErrorTemplate.MODAL;
                break;
            case EError.PLAYBACK_DENIED:
                errorTemplate = EErrorTemplate.MODAL;
                break;
            case EError.AUTOPLAY_DENIED:
                errorTemplate = EErrorTemplate.MODAL;
                break;
            case EError.BUFFER_EMPTY_NO_RECOVERY:
                errorTemplate = EErrorTemplate.OVERLAY;
                break;

            // fallback to toast if we don't recognize the error type
            case EError.UNKNOWN:
            default:
                errorTemplate = EErrorTemplate.TOAST_WITHOUT_CTA;
                errorData = { messagePath: "errors.unknown" };
        }

        ClientErrorService.logger.debug(`determineTemplate( errorTemplate = ${errorTemplate} )`);

        return [errorTemplate, isInErrorState, errorData];
    }

    /**
     * Open an overlay/modal/toast based on the error template
     */
    private handleError([errorTemplate, isInErrorState, errorData]: [EErrorTemplate, boolean, ErrorData]): void
    {
        switch (errorTemplate)
        {
            case EErrorTemplate.OVERLAY:
            case EErrorTemplate.MODAL:
                (errorData as IModalData).modalType = errorTemplate;
                this.openModal(errorData as IModalData);
                break;
            case EErrorTemplate.TOAST_WITHOUT_CTA:
                if (isInErrorState)
                {
                    this.openToastWithoutCTA(errorData as ToastData);
                }
                else
                {
                    this.toastService.close(errorData as ToastData);
                }
                break;
            case EErrorTemplate.INLINE:
                this.openInlineError();
                break;
            default:
                ClientErrorService.logger.warn(
                    `determineTemplate( Found an unknown error template of ${errorTemplate}. Add case for this template. )`
                );
        }
    }

    private openModal(modalData: IModalData): void
    {
        ClientErrorService.logger.debug(`openModal()`);
        this.modalService.addDynamicModal(modalData);
    }

    private openToastWithoutCTA(newToastData): void
    {
        ClientErrorService.logger.debug(`openToastWithoutCTA()`);

        this.toastService.toastInformationSubject.pipe(
            take(1))
            .subscribe((prevToastInfo: ToastInformation) =>
            {
                // this callback implements priority treatment for toasts
                // e.g., if we are currently displaying the internet connection toast
                // no other toasts are allowed until that one is dismissed.
                // Toasts currently are not in the priority queue.

                if (prevToastInfo.data &&
                    prevToastInfo.data.errorType === EError.NO_IP_CONNECTION &&
                    prevToastInfo.isOpen)
                {
                    if (newToastData.data &&
                        newToastData.data.errorType === EError.NO_IP_CONNECTION)
                    {
                        this.toastService.open(newToastData);
                    }
                    // do nothing
                }
                else
                {
                    this.toastService.open(newToastData);
                }
            });
    }

    private openInlineError(): void
    {
        ClientErrorService.logger.debug(`openInlineError()`);
    }
}
