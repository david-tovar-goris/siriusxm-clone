import { StickyService } from "./sticky.service";

describe("Sticky Service Test Suite >>", function ()
{
    beforeEach(function ()
    {
        this.makeDirective = function(height: number)
        {
            return {
                elementRef: {
                    nativeElement: {
                        clientHeight: height
                    }
                },
                isSticky: false
            };
        };

        this.directives = [
            this.makeDirective(20),
            this.makeDirective(40),
            this.makeDirective(30)
        ];

        this.service = new StickyService();

        this.service.registerStickyable(this.directives[0]);
        this.service.registerStickyable(this.directives[1]);
        this.service.registerStickyable(this.directives[2]);
    });

    describe("getOffset()", function ()
    {
        describe('when none are sticky', function()
        {
            it('returns 0', function()
            {
                expect(this.service.getOffset(this.directives[2]))
                    .toBe(0);
            });
        });

        describe('when one is sticky', function()
        {
            beforeEach(function()
            {
                this.directives[1].isSticky = true;
            });

            it('returns offset of sticky', function()
            {
                expect(this.service.getOffset(this.directives[2]))
                    .toBe(40);
            });
        });


        describe('when two or more are sticky', function()
        {
            beforeEach(function()
            {
                this.directives[0].isSticky = true;
                this.directives[1].isSticky = true;
            });

            it('returns sum of client heights up to that directive', function()
            {
                expect(this.service.getOffset(this.directives[2]))
                    .toBe(60);
            });

            it('returns sum of client hights before', function()
            {
                expect(this.service.getOffset(this.directives[1]))
                    .toBe(20);
            });
        });
    });
});
