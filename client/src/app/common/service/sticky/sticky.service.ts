import { Injectable } from "@angular/core";
import { StickyDirective } from "app/common/directive/sticky/sticky.directive";
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class StickyService
{
    private stickyDirectives: StickyDirective[] = [];

    public fullHeightPx$: BehaviorSubject<number> = new BehaviorSubject<number>(null);

    public registerStickyable(directive: StickyDirective): void
    {
        this.stickyDirectives.push(directive);
    }

    public unregisterStickyable(directive: StickyDirective): void
    {
        this.stickyDirectives.splice(this.stickyDirectives.indexOf(directive), 1);
    }

    public getOffset(directive: StickyDirective): number
    {
        const index = this.stickyDirectives.indexOf(directive);

        return this.stickyDirectives.slice(0, index)
            .reduce((sum, directive) =>
            {
                if (directive.isSticky)
                {
                    return sum + directive.elementRef.nativeElement.clientHeight;
                }
                return sum;
            }, 0);
    }

    public getFullHeight(): number
    {
        return this.stickyDirectives.reduce((sum, directive) =>
        {
            if (directive.isSticky)
            {
                return sum + directive.elementRef.nativeElement.clientHeight;
            }
            return sum;
        }, 0);
    }


    public emitFullHeight(): void
    {
        const fullHeight = this.getFullHeight();
        this.fullHeightPx$.next(fullHeight);
    }
}
