import {
    ComponentFactoryResolver,
    Injectable, ViewContainerRef
} from "@angular/core";
import {DynamicLoadingScreenComponent} from "../../component/loading-screen/loading-screen.component";

@Injectable()
export class LoadingScreenService
{
    private rootViewContainer: ViewContainerRef;
    private component;

    public get isOpen(): boolean
    {
        return this.component
            && this.component.instance
            && this.component.instance.screenOpen;
    }

    constructor(private factoryResolver: ComponentFactoryResolver){}

    /**
     * Sets the rootViewContainer to the container ref passed in
     */
    public setRootViewContainerRef(viewContainerRef): void
    {
        this.rootViewContainer = viewContainerRef;
    }

    /**s
     * Creates the Dynamic Loading Screen
     */
    public addDynamicSplashScreen(): void
    {
        const factory = this.factoryResolver.resolveComponentFactory(DynamicLoadingScreenComponent);
        this.component = factory.create(this.rootViewContainer.parentInjector);
        this.component.instance.screenOpen = true;
        this.rootViewContainer.insert(this.component.hostView);
    }

    /**
     * Closes the Loading Screen
     */
    public closeSplashScreen(): void
    {
        this.component.instance.screenOpen = false;
    }
}
