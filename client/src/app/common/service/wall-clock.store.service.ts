import { Injectable } from "@angular/core";
import { IWallClockStore } from "../store/wall-clock.store";
import { Observable } from "rxjs";
import { LiveTimeService } from "sxmServices";
import * as WallClockActions from "../action";
import { IAppStore } from "../store";
import { Store } from "@ngrx/store";

import * as moment from "moment";


@Injectable()
export class WallClockStoreService
{
  wallClockStore: Observable<IWallClockStore>;

  constructor(private liveTimeService: LiveTimeService,
              private store: Store<IAppStore>)
  {
    this.wallClockStore = this.store.select("wallClockStore");

    this.liveTimeService.wallClock.subscribe(currentWallClock =>
      {
        this.store.dispatch(new WallClockActions.updateWallClock(new Date(currentWallClock)));
      });
  }
}
