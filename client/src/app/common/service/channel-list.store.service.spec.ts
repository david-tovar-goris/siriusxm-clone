import { ChannelListStoreService } from "app/common/service/channel-list.store.service";
import { of as observableOf } from "rxjs";
import { ICategory, IChannel, IMediaCut, ISubCategory, ISuperCategory, mockForYouData } from "sxmServices";
import { createChannel } from "../../../../test/mocks/channel-list.service.mock";
import { AuthenticationServiceMock } from "../../../../test/mocks/authentication.service.mock";
import { MockStore } from "../../../../test/mocks/redux.store.mock";
import { IAppStore } from "app/common/store";
import { displayViewType } from "app/channel-list/component/display.view.type";
import * as ChannelList from "app/common/action/channel-list.action";
import { ISelectedCategory } from "app/common/store/channel-list.store";

describe("Channel List Service Test Suite >>", function()
{
    beforeEach(function()
    {
        this.superCategoryForYou = mockForYouData;

        this.superCatList = [
            this.superCategoryForYou
        ];

        this.channelHighwayMock = createChannel("12345", "highway") as IChannel;

        this.channelLineupServiceMoc = {

            channelLineupChanged : observableOf(false),

            channelLineup: {
                superCategories: observableOf(this.superCatList),
                channels: observableOf([this.channelHighwayMock])
            },
            findChannelById: jasmine.createSpy('findChannelById').and.returnValue(this.channelHighwayMock)
        };

        this.carouselStoreServiceMock = {
            selectSuperCategory: jasmine.createSpy("selectSuperCategory"),
            selectSubCategory: jasmine.createSpy("selectSubCategory")
        };

        this.currentlyPlayingMedia = {
            cut: {} as IMediaCut,
            firstCut: {} as IMediaCut,
            mediaType: "mediaType",
            mediaId: "mediaType",
            channelId: "channelId"
        };

        this.currentlyPlyingServiceMock = {
            currentlyPlayingData: observableOf(this.currentlyPlayingMedia)
        };

        this.nowPlayingStoreServiceMock = {
            selectNowPlayingChannel: jasmine.createSpy("selectNowPlayingChannel")
        };

        this.appStoreMock = new MockStore<IAppStore>();

        this.categoryCountryMock = {
            name: "Country",
            key: "country"
        };

        this.appStoreMockChannelStore = {
            selectedSuperCategory: {},
            superCategories: this.superCatList,
            selectedCategory: {
                category: this.categoryCountryMock as ISubCategory,
                viewType: displayViewType.channels
            }
        };

        this.appStoreMock.mockState("channelStore", this.appStoreMockChannelStore);

        this.authenticationServiceMock = new AuthenticationServiceMock();

        this.channelListStoreService = new ChannelListStoreService(
            this.channelLineupServiceMoc,
            this.carouselStoreServiceMock,
            this.currentlyPlyingServiceMock,
            this.nowPlayingStoreServiceMock,
            this.appStoreMock,
            this.authenticationServiceMock);

        spyOn(this.channelListStoreService.store, "dispatch");
    });

    describe("Infrastructure >>", function()
    {
        it("initialises the channel list store", function()
        {
            expect(this.channelListStoreService.channelStore).toBeDefined();
        });
    });

    describe("loadlineup() method", function()
    {
        it("dispatches action to load superCategory list", function()
        {
            this.channelListStoreService.loadLineup();
            expect(this.channelListStoreService.store.dispatch)
                .toHaveBeenCalledWith(new ChannelList.LoadSuperCategories(this.superCatList as Array<ISuperCategory>));
        });
    });

    describe("loadLiveChannels() method", function()
    {
        it("dispatches action to load channel list", function()
        {
            this.channelListStoreService.loadLiveChannels();
            expect(this.channelListStoreService.store.dispatch)
                .toHaveBeenCalledWith(new ChannelList.LoadLiveChannels([this.channelHighwayMock]));
        });
    });

    describe("selectSuperCategory() method", function()
    {
        it("dispatches action to select superCategory", function()
        {
            this.channelListStoreService.selectSuperCategory(this.superCategoryForYou as ISuperCategory);
            expect(this.channelListStoreService.store.dispatch)
                .toHaveBeenCalledWith(new ChannelList.SelectSuperCategory(this.superCategoryForYou as ISuperCategory));
        });
    });

    describe("selectSuperCategoryByChannelId() method", function()
    {
        it("calls selectSuperCategory with selected superCategory", function()
        {
            spyOn(this.channelListStoreService, "selectSuperCategory");
            this.channelListStoreService.selectSuperCategoryByChannelId(this.channelHighwayMock.channelNumber);
            expect(this.channelListStoreService.selectSuperCategory).toHaveBeenCalledWith({ key: "Entertainment" });
        });
    });

    describe('selectChannelByChannelID() >> ', function()
    {
        it('dispatches action to select channel', function()
        {
            this.channelListStoreService.selectChannelByChannelID(this.channelHighwayMock.channelNumber);
            const channel = new ChannelList.SelectChannel(this.channelListStoreService.channelLineupService
                                                              .findChannelById(this.channelHighwayMock.channelNumber));
            expect(this.channelListStoreService.store.dispatch).toHaveBeenCalledWith(channel);
        });
    });

    describe("selectCategory() method", function()
    {
        it("dispatches the SelectCategory action with the `country` category", function()
        {
            this.channelListStoreService.selectCategory(this.categoryCountryMock as ICategory, displayViewType.channels);
            const selectedCategoryExpected = {
                category: this.categoryCountryMock as ISubCategory,
                viewType: displayViewType.channels
            };
            expect(this.channelListStoreService.store.dispatch)
                .toHaveBeenCalledWith(new ChannelList.SelectCategory(selectedCategoryExpected as ISelectedCategory));
        });
    });

    describe("selectChannel() method", function()
    {
        it("Should dispatch the SelectChannel action with the `highway` channel", function()
        {
            this.channelListStoreService.selectChannel(this.channelHighwayMock as IChannel);
            expect(this.channelListStoreService.store.dispatch)
                .toHaveBeenCalledWith(new ChannelList.SelectChannel(this.channelHighwayMock as IChannel));
        });
    });

    describe("selectChannelByChannelID() method", function()
    {
        it("calls selectChannel() method with selected channel", function()
        {
            spyOn(this.channelListStoreService,"selectChannel");
            this.channelListStoreService.selectChannelByChannelID(this.channelHighwayMock.channelId);
            expect(this.channelListStoreService.selectChannel).toHaveBeenCalledWith(this.channelHighwayMock as IChannel);
        });
    });

    describe("sort() method", function()
    {
        it("Should dispatch the SortChannelsByNumber action", function()
        {
            this.channelListStoreService.sort();
            expect(this.channelListStoreService.store.dispatch).toHaveBeenCalledWith(new ChannelList.SortChannelsByNumber());
        });
    });

    describe("filter() method", function()
    {
        const filterByName: string = "name";
        const filterByNumber: string = "12345";

        it(`Should dispatch the FilterChannelsByName action with the filter ${filterByName}.`, function()
        {
            this.channelListStoreService.filter(observableOf([
                filterByName,
                displayViewType.channels
            ] as any));
            expect(this.channelListStoreService.store.dispatch).toHaveBeenCalledWith(new ChannelList.FilterChannelsByName(filterByName));
        });

        it(`Should dispatch the FilterChannelsByNumber action with the filter ${filterByNumber}.`, function()
        {
            this.channelListStoreService.filter(observableOf([
                filterByNumber,
                displayViewType.channels
            ]) as any);
            expect(this.channelListStoreService.store.dispatch).toHaveBeenCalledWith(new ChannelList.FilterChannelsByNumber(filterByNumber));
        });

        it(`Should dispatch the FilterChannelsByShowName action with the filter ${filterByName}.`, function()
        {
            this.channelListStoreService.filter(observableOf([
                filterByName,
                displayViewType.ondemand
            ]) as any);
            expect(this.channelListStoreService.store.dispatch).toHaveBeenCalledWith(new ChannelList.FilterChannelsByShowName(filterByName));
        });

        it(`Should log error if view type not defined.`, function()
        {
            this.channelListStoreService.filter(observableOf([
                filterByName,
                null
            ]) as any);

            expect(this.channelListStoreService.store.dispatch).not.toHaveBeenCalled();
        });
    });

    describe('getChannelPdt()', function()
    {
        const playingArtist = "playingArtist";
        const playingTitle = "playingTitle";
        const channelName = "ChannelName";

        it('returns channel name when playingArtist and playingTitle are not provided', function()
        {
            expect(this.channelListStoreService.getChannelPdt('', '', channelName)).toEqual(channelName);
        });

        it('returns proper pdt when playingArtist, playingTitle are provided', function()
        {
            expect(this.channelListStoreService.getChannelPdt(playingArtist, playingTitle, channelName))
                .toEqual(playingArtist + ' - ' + playingTitle);
        });
    });
});
