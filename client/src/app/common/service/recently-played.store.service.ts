import { Store } from "@ngrx/store";
import { Injectable } from "@angular/core";
import { IAppStore, IRecentlyPlayedStore } from "../store";
import {
    CarouselService,
    ICarouselDataByType,
    ITile,
    RecentlyPlayedService
} from "sxmServices";
import { CarouselStoreService } from "./carousel.store.service";
import { Observable } from "rxjs";


@Injectable()
export class RecentlyPlayedStoreService
{
    /**
     * List of recently played list wrapped in observables from the NGRX recently played store.
     */
    public recentlyPlayed: Observable<IRecentlyPlayedStore>;
    public recents : Store<ICarouselDataByType>;

    /**
     * Creates an instance of RecentlyPlayedStoreService
     *
     * Sets up a subscriber to servicelib recentlyPlayedService. When the API returns a new list of recently played items,
     * it dispatches an even to the store in order to update the store with the list of recently played items.
     * @param {RecentlyPlayedService} recentlyPlayedService
     * @param {Store<IAppStore>} store
     * @param {CarouselStoreService} carouselStoreService
     * @param {CarouselService} carouselService
     */
    constructor(private recentlyPlayedService: RecentlyPlayedService,
                private store: Store<IAppStore>,
                private carouselStoreService : CarouselStoreService,
                private carouselService : CarouselService)
    {
        this.recentlyPlayed = this.store.select(state => state.recentlyPlayedStore);
    }

    /**
     * Removes all the recently played data
     * @returns {Observable<boolean>}
     */
    public removeAllRecentlyPlayed()
    {
        this.recentlyPlayedService.removeAllRecentlyPlayed().subscribe((response: boolean) =>
            {
                if (response)
                {
                    this.carouselStoreService.selectRecents();
                }
            }
        );
    }

    /**
     * Removes a recently played item from the recently played data
     * @param {ITile} tile
     */
    public removeRecentlyPlayedItem(tile: ITile)
    {
        if (!tile.tileAssetInfo.recentPlayGuid) { return; }

        this.recentlyPlayedService.removeRecentlyPlayedItem(tile.tileAssetInfo.recentPlayGuid,
                                                            tile.tileAssetInfo.recentPlayAssetGuid,
                                                            tile.tileAssetInfo.recentPlayType)
                                                            .subscribe((response: boolean) =>
                                                                {
                                                                    if (response)
                                                                    {
                                                                        this.carouselStoreService.selectRecents();
                                                                    }
                                                                }
                                                            );
    }
}
