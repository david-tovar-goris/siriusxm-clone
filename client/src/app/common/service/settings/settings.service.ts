import { Injectable } from "@angular/core";
import { ConfigService } from "sxmServices";

@Injectable()
export class SettingsService
{
    constructor(private configService: ConfigService){}

    /**
     *
     * @param {"string"} configName
     * @param {"string"} booleanConfigProp
     * @returns {boolean}
     *
     * Find the Setting your looking for and tells you whether it should be turned on or off
     */
    public isSettingOn(configName: string, booleanConfigProp: string): boolean
    {
        let settingOn: boolean = true,
            configSetting;

        this.configService.configurationSubject.subscribe((data) =>
        {
            if(!data) return;
            configSetting = data.components.find((element) => { return element.name === configName; });

            return settingOn = configSetting.settings[0][booleanConfigProp];
        }).unsubscribe();

        return settingOn;
    }
}
