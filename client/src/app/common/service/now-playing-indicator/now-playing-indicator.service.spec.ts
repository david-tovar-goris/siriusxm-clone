import { TestBed } from "@angular/core/testing";
import {
    NowPlayingIndicatorService
} from "./now-playing-indicator.service";
import {
    ContentTypes,
    MediaPlayerService
} from "sxmServices";
import { of } from "rxjs";
import { Store } from "@ngrx/store";

describe("NowPlayingIndicatorService", () =>
{
    let service: NowPlayingIndicatorService,
        mockMediaPlayerService = {
            mediaPlayer: {
                currentlyPlayingMedia: {
                    channelId: "howard100",
                    mediaType: ContentTypes.LIVE_AUDIO,
                    episode: {}
                }
            },
            isMediaContentPlaying: true
        };

    let mockStore = {
        select: function (action)
        {
            return of(true);
        }
    };

    const setupModule = () =>
    {
        TestBed.configureTestingModule({
            providers: [
                NowPlayingIndicatorService,
                { provide: Store, useValue: mockStore },
                { provide: MediaPlayerService, useValue: mockMediaPlayerService }
            ]
        });
        service = TestBed.get(NowPlayingIndicatorService);
    };

    describe("", () =>
    {
        let spy;
        beforeEach(() =>
        {
            setupModule();

        });

        it("Should called createPlayingIndicatorSrc and return observable", function()
        {
            let fakeObservable = of("abc");
            spyOn(service,"createPlayingIndicatorSrc").and.returnValue(fakeObservable);
            expect(service.createPlayingIndicatorSrc()).toBe(fakeObservable);
        });
    });
});
