import { Injectable } from "@angular/core";
import { ContentTypes, ITileAssetInfo, MediaPlayerService } from "sxmServices";
import { BehaviorSubject, Observable } from "rxjs";
import { Store } from "@ngrx/store";
import { getIsMediaPlaying } from "../../../common/store/now-playing.store";
import { IAppStore } from "app/common/store";
import { distinctUntilChanged, map } from "rxjs/operators";

export type tileContentType = "channel" | "show" | "aod" | "vod" | "additionalchannel";

@Injectable()

export class NowPlayingIndicatorService
{
    public playingIndicatorSrc$: Observable<string>;
    public isMediaPlaying$: Observable<boolean>;

    constructor(private mediaPlayerService: MediaPlayerService,
                private store: Store<IAppStore>)
    {
        this.isMediaPlaying$ = this.store.select(getIsMediaPlaying);
        this.playingIndicatorSrc$ = this.createPlayingIndicatorSrc();
    }

    /**
     *  create and return playing Indicator src observable
     */
    createPlayingIndicatorSrc()
    {
        return this.isMediaPlaying$.pipe(
            map( (isMediaPlaying: boolean) =>
            {
                return  isMediaPlaying ? "../../../assets/images/currently-playing.gif"
                                                   : "../../../assets/images/paused.png";
            }),
            distinctUntilChanged());
    }
}
