import { MockStore } from "../../../../test/mocks/redux.store.mock";
import { RecentlyPlayedStoreService } from "./recently-played.store.service";
import { mockNormalizedCarousel } from "sxmServices";
import { IAppStore } from "../store/app.store";
import { of as observableOf } from "rxjs";
import { RecentlyPlayedStoreServiceMock } from "../../../../test/mocks/recently-played.store.service.mock";

describe('RecentlyPlayedStoreService', () =>
{
    let recentlyPlayedStoreService,
        mockStore,
        recentlyPlayedData = mockNormalizedCarousel;

    beforeEach(() =>
    {
        mockStore = new MockStore<IAppStore>();
        spyOn(mockStore, "select").and.returnValue(observableOf(mockNormalizedCarousel));
        recentlyPlayedStoreService = new RecentlyPlayedStoreServiceMock();
    });

    describe("removeAllRecentlyPlayed()", () =>
    {
        it("uses the recentlyPlayedStoreService to remove all recently played data on API", () =>
        {

            recentlyPlayedStoreService.removeAllRecentlyPlayed(recentlyPlayedData);
            expect(recentlyPlayedStoreService.removeAllRecentlyPlayed).toHaveBeenCalled();
        });
    });

    describe("removeRecentlyPlayedItem()", () =>
    {
        it("uses the recentlyPlayedStoreService to remove a selected recently played item from recently played data on API", () =>
        {

            recentlyPlayedStoreService.removeRecentlyPlayedItem(recentlyPlayedData[ 0 ]);
            expect(recentlyPlayedStoreService.removeRecentlyPlayedItem).toHaveBeenCalled();
        });
    });
});
