import { of as observableOf } from "rxjs";
import { CarouselServiceMock } from "../../../../test/mocks/carousel.service.mock";
import { FavoriteListStoreService } from "./favorite-list.store.service";
import { FavoriteServiceMock } from "../../../../test/mocks/favorite.service.mock";
import { MockStore } from "../../../../test/mocks/redux.store.mock";
import { IAppStore } from "../store";
import {
    IGroupedFavorites,
    InitializationService,
    InitializationStatusCodes
} from "sxmServices";

describe("FavoriteListStoreService", () =>
{
    let favoriteListStoreService,
        initializationService,
        favorites: IGroupedFavorites,
        mockStore;


    beforeEach(() =>
    {
        favorites = {
            channels: [],
            shows: [],
            episodes: []
        };

        initializationService = {};
        initializationService.initState = observableOf(InitializationStatusCodes.RUNNING);

        mockStore = new MockStore<IAppStore>();
        spyOn(mockStore, "select").and.returnValue(observableOf(favorites));

        favoriteListStoreService = new FavoriteListStoreService(CarouselServiceMock.getSpy(),
                                                                initializationService as InitializationService,
                                                                FavoriteServiceMock.getSpy(),
                                                                mockStore);
    });

    it("sets the property favorites to the favoritesList.favorites value from the store", () =>
    {
        let subscription,
            favs;

        subscription = favoriteListStoreService.favorites.subscribe((state) =>
        {
            favs = state;
        });

        expect(favs).toEqual(favorites);

        subscription.unsubscribe();
    });

    describe("updateFavorite()", () =>
    {
        it("uses the favoriteService to update favorite on API", () =>
        {
            spyOn(favoriteListStoreService, 'getFavoriteCarousels');
            FavoriteServiceMock.getSpy().updateFavorite.and.returnValue(observableOf(true));
            favoriteListStoreService.updateFavorite();
            expect(FavoriteServiceMock.getSpy().updateFavorite).toHaveBeenCalled();
        });
    });


    describe("updateFavorites()", () =>
    {
        it("uses the favoriteService to update favorites on API", () =>
        {
            spyOn(favoriteListStoreService, 'getFavoriteCarousels');
            FavoriteServiceMock.getSpy().updateFavorites.and.returnValue(observableOf(true));
            favoriteListStoreService.updateFavorites([]);
            expect(FavoriteServiceMock.getSpy().updateFavorites).toHaveBeenCalled();
        });
    });
});

