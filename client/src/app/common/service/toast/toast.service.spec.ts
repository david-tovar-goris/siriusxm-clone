import { ToastService } from "./toast.service";
import {
    ToastData,
    ToastInformation
} from "../../component/toast/toast.interface";
import { TestBed } from "@angular/core/testing";

describe("Toast Service Test Suite >>", () =>
{
    let service: ToastService = null;

    let toastDataMock: ToastData = {
        messagePath: "errors.foo.bar",
        buttonCTAText: "baz quz",
        screen: ""
    };

    let toastInformation: ToastInformation;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            providers: [
                ToastService
            ]
        });

        service = TestBed.get(ToastService);

        service.toastInformation.subscribe(val =>
        {
            toastInformation = val;
        });
    });

    describe("Infrastructure >>", () =>
    {
        it("Should have a test subject.", () =>
        {
            expect(service).toBeDefined();
        });

        it("Should expose the expected public properties.", () =>
        {
            expect(service.toastInformation).toBeDefined();
            expect(typeof service.close).toEqual("function");
            expect(typeof service.open).toEqual("function");
        });
    });

    describe("Toast Service execution", () =>
    {
        it("sets .isOpen to true when .open() is called", () =>
        {
            service.open(toastDataMock);

            expect(toastInformation.isOpen).toEqual(true);
        });

        it("sets .isOpen to false when .close() is called", () =>
        {
            service.close();

            expect(toastInformation.isOpen).toEqual(false);
        });

    });
});
