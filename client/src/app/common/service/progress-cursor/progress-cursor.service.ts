import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class ProgressCursorService
{
    public isSpinning: BehaviorSubject<boolean>;

    constructor()
    {
        this.isSpinning = new BehaviorSubject<boolean>(false);
    }

    startSpinning()
    {
        this.isSpinning.next(true);
    }

    stopSpinning()
    {
        this.isSpinning.next(false);
    }
}
