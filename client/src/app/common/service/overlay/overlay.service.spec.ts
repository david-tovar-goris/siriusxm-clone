import { OverlayService } from "./overlay.service";
import {OverlayConfig, OverlayData, OverlayInformation, OverlayResult} from "../../component/overlay/overlay.interface";
import {TestBed} from "@angular/core/testing";
import {FlepzScreenService} from "../flepz/flepz-screen.service";

describe("Overlay Service Test Suite >>", () =>
{
    let service: OverlayService = null;
    let overlayConfigMock: OverlayConfig = {
        overlayData:{
            header: 'Header',
            description: "DDescription",
            buttonOneText: "OK",
            buttonTwoText: "Cancel",
            screen: ''
        },
        overlayStyles: {
            height: '10px',
            width: '10px'
        }
    };
    let overlayInformation:OverlayInformation;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
           providers: [
               FlepzScreenService,
               OverlayService
           ]
        });
        service = TestBed.get(OverlayService);
        service.overlayInformation.subscribe(val =>
        {
            overlayInformation = val;
        });
    });

    describe("Infrastructure >> ", () =>
    {
        it("Should have a test subject.", () =>
        {
            expect(service).toBeDefined();
        });

        it("Should expose the expected public properties.", () =>
        {
            expect(service.overlayInformation).toBeDefined();
        });
    });

    describe("Overlay Service execution", () =>
    {
        it("open overlay ", () =>
        {
            service.open(overlayConfigMock).subscribe((result: OverlayResult) =>
            {
                expect(result).toEqual(null);
            });

            expect(overlayInformation.open).toEqual(true);
        });

        it("close overlay ", () =>
        {
            service.close();
            expect(overlayInformation.open).toEqual(false);
        });

    });
});
