import { share } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { BehaviorSubject ,  Observable } from "rxjs";
import {
    OverlayInformation,
    OverlayData,
    OverlayResult,
    OverlayConfig
} from "../../component/overlay/overlay.interface";

@Injectable()
export class OverlayService
{
    /**
     * subject to store the Data of overlay component
     * @type {overlayInformation}
     */
    public overlayInformationSubject: BehaviorSubject<OverlayInformation>;
    public overlayInformation: Observable<OverlayInformation> = null;

    /**
     * Subject for notifying the user that the Overlay has closed.
     */
    private overlayResult: BehaviorSubject<OverlayResult>;
    public screenName: string;
    private openOverlayData: Array<OverlayConfig> = [];

    constructor()
    {
        this.overlayInformationSubject = new BehaviorSubject({
            open: false,
            config: null
        });
        this.overlayInformation = this.overlayInformationSubject.pipe(share());
    }

    /**
     * Used to open overlay component. Once triggered, will update the overlayId and enableOverlay stores.
     * Applied overflow:hidden on body tag to make it not scrollable while Overlay is open
     * Reset the overlayResult observable so that multiple subscriptions will not be created.
     * @param overlayId contains an id
     * @returns {Subscription}
     */
    public open(overlayConfig:OverlayConfig): Observable<any>
    {
        const {overlayData, overlayStyles} = overlayConfig;
        this.openOverlayData.push(overlayConfig);
        this.screenName = overlayData.screen;
        this.overlayResult = new BehaviorSubject<OverlayResult>(null);

        /**
         * @description
         *      This hardcoded error code will just
         *      be for the benefit of QA seeing something
         */
        overlayData.errorCode = overlayData.errorCode ? overlayData.errorCode : "";

        if (this.openOverlayData)
        {
            this.overlayInformationSubject.next({
                open: true,
                config: this.openOverlayData[this.openOverlayData.length-1]
            });

            document.body.style.overflow = "hidden";
        }
        return this.overlayResult.asObservable();
    }

    /**
     * Used to close the overlay component.
     * Removed overflow:hidden on body tag to make it scrollable while Overlay is closed
     */
    public close(data ?): void
    {
        this.openOverlayData.pop();
        if(this.openOverlayData.length > 0)
        {
            this.overlayInformationSubject.next({
                open: true,
                config: this.openOverlayData[this.openOverlayData.length-1]
            });
        }
        else
        {
            this.overlayInformationSubject.next({
                open: false,
                config: null
            });
           document.body.style.overflow = '';
        }

        if (data)
        {
            this.overlayResult.next(data);
        }
    }
}
