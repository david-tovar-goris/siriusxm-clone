import { Injectable } from "@angular/core";
import { BehaviorSubject ,  Observable } from "rxjs";
import { OverlayResult } from "../../component/overlay/overlay.interface";
import {
    INotification,
    INotificationInformation,
    INotificationFeedback
} from "sxmServices";

@Injectable()
export class NotificationService
{
    /**
     * subject to store the Data of notification component
     * @type {INotificationInformation}
     */
    public notificationInformationSubject: BehaviorSubject<INotificationInformation>;
    public notificationInformation: Observable<INotificationInformation> = null;

    /**
     * Subject for notifying the user that the Notification has closed.
     */
    private notificationResult: BehaviorSubject<OverlayResult>;

    /**
     * The payload for the post request made when the user
     * interacts with this overlay
     */
    private notificationFeedback: INotificationFeedback;

    constructor()
    {
        this.notificationInformationSubject = new BehaviorSubject({
            open: false,
            data: null
        });
        this.notificationInformation = this.notificationInformationSubject;
    }

    /**
     * Used to open notification component. Once triggered, will update the notificationId and enableNotification stores.
     * Applied overflow:hidden on body tag to make it not scrollable while Notification is open
     * Reset the notificationResult observable so that multiple subscriptions will not be created.
     * @param notification
     * @returns {Subscription}
     */
    public open(notification?: INotification): Observable<any>
    {
        this.notificationResult = new BehaviorSubject<OverlayResult>(null);

        if (notification)
        {
            this.notificationInformationSubject.next({
                open: true,
                data: notification
            });

            document.body.style.overflow = "hidden";
        }
        return this.notificationResult.asObservable();
    }

    /**
     * Used to close the notification component.
     * Removed overflow:hidden on body tag to make it scrollable while Notification is closed
     */
    public close(data?: any): void
    {

        this.notificationInformationSubject.next({
            open: false,
            data: null
        });

        document.body.style.overflow = '';

        if (data)
        {
            this.notificationResult.next(data);
        }
    }

}
