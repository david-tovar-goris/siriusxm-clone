/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />

import { NotificationService } from "./notification.service";

describe('NotificationService', () =>
{

    let service: NotificationService;

    beforeEach(() =>
    {
        service = new NotificationService();
    });

    afterEach(() =>
    {
        service = null;
    });

    it("Should exist", () =>
    {
        expect(service).toBeDefined();
        expect(service).toBeTruthy();
    });

});
