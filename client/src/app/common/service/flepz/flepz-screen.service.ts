import {Injectable} from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Injectable()
export class FlepzScreenService
{
    /**
     * Boolean for turning the forgotUsernamePassword iFrame off/on
     */
    public forgotUsernamePasswordIframe: boolean = false;

    /**
     * Boolean for turning the getStarted iFrame off/on
     */
    public getStartedIframe: boolean = false;

    public srcUrl: SafeResourceUrl;

    constructor(public sanitizer: DomSanitizer){}

    /**
     * Listens to the actionType of the iFrame's message to determine to close the iFrame or not.
     * @param {any} actionType
     */

    public onMessageEvent({ data: { widgetInterface: { actionType: { handleEvent }} = { actionType: { handleEvent: '' }}}}): void
    {
        switch (handleEvent.toLowerCase())
        {
            case "back to login":
            case "backtologin":
            case "resetpwdcompletebacktologin":
            case "retrieveuncompletebacktologin":
            case "trialcompletedbacktologin":
                this.forgotUsernamePasswordIframe = false;
                this.getStartedIframe = false;
        }
    }

    /**
     * Turns the specific iFrames on/off
     * @param {string} iFrameType
     */

    public enableIframe(iFrameType: string, url: string = ""): void
    {
        if(url) this.srcUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);

        switch(iFrameType)
        {
            case 'forgotUsernamePassword':
                this.forgotUsernamePasswordIframe = true;
                break;
            case 'getStarted':
                this.getStartedIframe = true;
                break;
        }
    }
}
