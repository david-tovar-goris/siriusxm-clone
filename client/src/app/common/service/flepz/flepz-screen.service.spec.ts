import {TestBed} from "@angular/core/testing";
import {FlepzScreenService} from "./flepz-screen.service";

describe('FlepzScreenService', () =>
{
    let service: FlepzScreenService;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            providers: [FlepzScreenService]
        });

        service = TestBed.get(FlepzScreenService);
    });

    describe('onMessageEvent()', () =>
    {
        it('turns the iFrames off', () =>
        {
            window.addEventListener('message', service.onMessageEvent.bind(() =>
            {
                expect(service.forgotUsernamePasswordIframe).toEqual(false);
            }));
            let event = new MessageEvent('message', {
                data: {
                    widgetInterface: {
                        actionType: {
                            handleEvent: 'back to login'
                        }
                    }
                }
            });
            service.forgotUsernamePasswordIframe = true;
            window.dispatchEvent(event);
        });
    });

    describe('enableIframe()', () =>
    {
        it('turns Forgout Username and Password iFrame on', () =>
        {
            service.enableIframe('forgotUsernamePassword');
            expect(service.forgotUsernamePasswordIframe).toEqual(true);
        });

        it('turns Get Started iFrame on', () =>
        {
            service.enableIframe('getStarted');
            expect(service.getStartedIframe).toEqual(true);
        });
    });
});
