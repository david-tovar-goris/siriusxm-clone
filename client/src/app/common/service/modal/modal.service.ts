import {
    ApplicationRef,
    ComponentFactoryResolver,
    Injectable,
    ViewContainerRef,
    ComponentRef
} from "@angular/core";
import { DynamicModalComponent } from "../../component/modal/modal.component";
import {IModalData} from "./modal.interface";

@Injectable()
export class ModalService
{
    /**
     * Used to disable scrolling when the modal is open.
     *
     * @type {boolean}
     * @memberof ModalService
     */
    public get modalIsOpen(): boolean
    {
        return this.openModalCount > 0;
    }    

    public get openModalCount(): number
    {
        return this.modalComponents.length;
    }

    private closeModalFns: Function[] = [];

    public modalComponents = [];

    constructor(private factoryResolver: ComponentFactoryResolver,
                private applicationRef: ApplicationRef)
    {}

    /**
     * Creates the Dynamic Modal
     */
    public addDynamicModal(modalData: IModalData): ComponentRef<any>
    {
        const component = this.factoryResolver.resolveComponentFactory(DynamicModalComponent);
        const dynamicComponent = this.applicationRef.components[0].instance.viewContainerRef.createComponent(component);
        dynamicComponent.instance._ref = dynamicComponent;
        dynamicComponent.instance.modalData = modalData;
        const closeModalFn = dynamicComponent.instance.close.bind(dynamicComponent.instance);
        this.closeModalFns.push(closeModalFn);
        dynamicComponent.instance.close = this.onCloseModal(closeModalFn, dynamicComponent);
        
        
        this.modalComponents.push(dynamicComponent);

        return dynamicComponent;
    }

    /**
     * Closes the last known modal.
     */
    public close(): void
    {
        this.closeModalFns.pop()();
    }

    /**
     * Closes the last known modal.
     */
    public closeAll(): void
    {
        this.closeModalFns.forEach( (fn) => fn() );
        this.closeModalFns = [];
    }

    private onCloseModal(closeModalFn: Function, modalComponent: any): () => void
    {
        return () =>
        {
            this.modalComponents = this.modalComponents.filter(component => component !== modalComponent);
            this.closeModalFns = this.closeModalFns.filter(func => func !== closeModalFn);

            closeModalFn();
        };
    }
}
