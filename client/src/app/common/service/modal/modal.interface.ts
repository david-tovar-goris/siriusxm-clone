import { EErrorTemplate } from "../error/error-template.enum";

export type ModalType = EErrorTemplate.MODAL | EErrorTemplate.OVERLAY;
export interface IModalData
{
    logo?: ImageInfo;
    header: string;
    description?: string;
    descriptionSecondary?: string;
    promoImage?: ImageInfo;
    errorCode?: string;
    modalType: ModalType;
    buttonOne?: IModalButtonData;
    buttonTwo?: IModalButtonData;
    onClose?: () => void ;
    customClass?: string;
}

export interface IModalButtonData
{
    text: string;
    action?: Function;
    target?: string;
    link?: string;
}

export interface ImageInfo
{
    url: string;
    altText: string;
}
