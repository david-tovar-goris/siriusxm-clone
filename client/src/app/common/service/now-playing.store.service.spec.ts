/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />

import { EMPTY as empty, of as observableOf } from "rxjs";
import {
    ChannelLineupService,
    CurrentlyPlayingService,
    IAodEpisode,
    IChannel,
    ICurrentlyPlayingMedia,
    IDmcaInfo,
    IMediaCut,
    IMediaEpisode,
    IMediaShow,
    IMediaVideo,
    IOnDemandShow,
    IPlayhead,
    ISubCategory,
    ISuperCategory,
    IVodEpisode
} from "sxmServices";
import { mock } from "ts-mockito";
import { MediaPlayerFactoryMock } from "../../../../../servicelib/src/test/mocks/media-player.factory.mock";
import { ChannelLineupServiceMock } from "../../../../test/mocks";
import { MockStore } from "../../../../test/mocks/redux.store.mock";
import {
    INowPlayingActionPayload, SelectBackgroundColor,
    SelectNowPlaying
} from "../action/now-playing.action";
import { IAppStore } from "../store/app.store";
import { INowPlayingStore } from "../store/now-playing.store";
import { NowPlayingStoreService } from "./now-playing.store.service";

describe("Now Playing Store Service Test Suite >>", () =>
{
    let testSubject: NowPlayingStoreService = null;
    let currentlyPlayingService             = null;
    let mediaPlayerFactory                  = null;
    let channelLineupService                = null;
    let appStoreMock                        = null;
    let nowPlayingStore: INowPlayingStore   = null;
    let carouselStoreService                = null;
    let dmcaService                         = null;
    let channelStore: any                   = null;
    let onDemandStore: any                  = null;

    const trackName: string       = "trackName";
    const channelNumber: number   = 56;
    const channelName: string     = "The Highway";
    const artistName: string      = "Eric Church";
    const albumTitle: string      = "The Outsiders";
    const albumArt: string        = "albumArt";
    const artistInfo: string      = "";
    const backgroundImage: string = "backgroundImage";
    const backgroundColor: string = "backgroundColor";

    let expected: any;
    let nowPlayingData: INowPlayingActionPayload      = null;
    let currentlyPlayingMedia: ICurrentlyPlayingMedia = null;

    beforeEach(() =>
    {
        // Create the mock channel lineup testSubject.
        currentlyPlayingService = mock(CurrentlyPlayingService);
        channelLineupService = new ChannelLineupServiceMock();
        currentlyPlayingService = {
            currentlyPlayingData:
                {
                    subscribe: (result: Function, fault: Function) => result(currentlyPlayingMedia),
                    flatMap: (mapResult: Function) => mapResult(currentlyPlayingMedia)
                }
        };
        mediaPlayerFactory = new MediaPlayerFactoryMock();
        mediaPlayerFactory.currentMediaPlayer = empty;

        // Create the mock app store.
        appStoreMock = new MockStore<IAppStore>();

        // Create the mock channel lineup testSubject.
        carouselStoreService = {
            selectNowPlaying : jasmine.createSpy("selectNowPlaying"),
            cleanNowPlayingUpNext  : jasmine.createSpy("cleanNowPlayingUpNext"),
            nowPlayingCarousels:  observableOf(null)
        };

        dmcaService = {
            isUnrestricted: jasmine.createSpy('isUnrestricted'),
            isRestricted: jasmine.createSpy('isRestricted'),
            isDisallowed: jasmine.createSpy('isDisallowed')
        };

        nowPlayingStore = {
            albumName      : "albumName",
            albumImage     : "albumImage",
            artistName     : "artistName",
            artistInfo     : "artistInfo",
            backgroundImage: "backgroundImage",
            backgroundColor: "backgroundColor",
            channelNumber  : "channelNumber",
            channelName    : "channelName",
            cut            : {} as IMediaCut,
            video          : {} as IMediaVideo,
            dmcaInfo           : {} as IDmcaInfo,
            episode        : {} as IMediaEpisode,
            lastPlayheadTimestamp : 0,
            playhead : {} as IPlayhead,
            mediaId        : "mediaId",
            channel        : {} as IChannel,
            mediaType      : "mediaType",
            show           : {} as IMediaShow,
            showMiniPlayer : false,
            startTime          : 0,
            trackName      : "trackName",
            type           : "type",
            youJustHeard   : [ {
                albumImageUrl: "imageUrl",
                title        : "title",
                artistName   : "artistName",
                cut          : {} as IMediaCut
            }
            ]
        };

        channelStore = {
            superCategories      : [] as Array<ISuperCategory>,
            selectedSuperCategory: {} as ISuperCategory,
            categories           : [] as Array<ISubCategory>,
            selectedCategory     : {} as ISubCategory,
            channels             : [] as Array<IChannel>,
            liveChannels         : [] as Array<IChannel>,
            selectedChannel      : {
                channelNumber         : channelNumber,
                clientBufferDuration  : 100,
                disableRecommendations: false,
                geoRestrictions       : 100,
                aodShowCount          : 100,
                inactivityTimeout     : 100,
                isAvailable           : true,
                isBizMature           : true,
                isFavorite            : true,
                isMature              : true,
                isMySxm               : true,
                isPersonalized        : true,
                isPlayByPlay          : true,
                mediumDescription     : "mediumDescription",
                name                  : channelName,
                shortDescription      : "shortDescription",
                siriusChannelNumber   : 100,
                sortOrder             : 100,
                spanishContent        : true,
                streamingName         : "streamingName",
                url                   : "url",
                xmChannelNumber       : 100,
                xmServiceId           : 100,
                satOnly               : true
            },
            nextChannel          : {} as IChannel,
            prevChannel          : {} as IChannel,
            filtered             : [] as Array<IChannel>,
            filter               : "",
            sort                 : ""
        };

        currentlyPlayingMedia = {
            cut            : {} as IMediaCut,
            mediaType: "mediaType",
            mediaId  : "mediaId",
            channelId : "channelId",
            dmcaInfo : {} as IDmcaInfo,
            episode  : {} as IMediaEpisode,
            show     : {} as IMediaShow,
            firstCut : {} as IMediaCut,
            duration : 0,
            playhead : {} as IPlayhead,
            video    : undefined,
            youJustHeard   : [ {
                albumImageUrl: "imageUrl",
                title        : "title",
                artistName   : "artistName",
                cut          : {} as IMediaCut
            }
            ]
        };

        onDemandStore = {
            selectedShow: {} as IOnDemandShow,
            selectedAodEpisode: {} as IAodEpisode,
            selectedVodEpisode: {} as IVodEpisode
        };

        appStoreMock.mockState("nowPlayingStore", nowPlayingStore);
        appStoreMock.mockState("channelStore", channelStore);
        appStoreMock.mockState("onDemandStore", onDemandStore);
    });

    describe("Infrastructure >> ", () =>
    {
        it("Should have a test subject.", () =>
        {
            // Create the test subject.
            testSubject = new NowPlayingStoreService(appStoreMock,
                carouselStoreService,
                currentlyPlayingService,
                channelLineupService,
                mediaPlayerFactory,
                dmcaService);

            expect(testSubject).toBeDefined();
        });

        it("Should expose the expected public properties.", () =>
        {
            // Create the test subject.
            testSubject = new NowPlayingStoreService(appStoreMock,
                carouselStoreService,
                currentlyPlayingService,
                channelLineupService,
                mediaPlayerFactory,
                dmcaService);

            expect(testSubject.nowPlayingStore).toBeDefined();
            expect(testSubject.nowPlayingStore).not.toEqual(null);

            expect(testSubject.channelStore).toBeDefined();
            expect(testSubject.channelStore).not.toEqual(null);
        });
    });

    describe("Subscribe to now playing data >> ", () =>
    {
        it("Should dispatch the SelectNowPlaying action with the now playing data.", () =>
        {
            // Spy on the dispatching of the Redux action.
            spyOn(appStoreMock, "dispatch");

            // Spy on and mock 3rd party objects used by NowPlayingStoreService
            spyOn(ChannelLineupService, "getChannelImage").and.callFake((cut) => backgroundImage);

            // Create the test subject.
            testSubject = new NowPlayingStoreService(appStoreMock,
                carouselStoreService,
                currentlyPlayingService,
                channelLineupService,
                mediaPlayerFactory,
                dmcaService);

            nowPlayingData = {
                albumImage     : albumArt,
                albumName      : albumTitle,
                artistInfo     : artistInfo,
                artistName     : artistName,
                backgroundImage: backgroundImage,
                backgroundColor: backgroundColor,
                channelName    : channelName,
                channelNumber  : String(channelNumber),
                cut            : currentlyPlayingMedia.cut as IMediaCut,
                video          : null,
                playhead       : {} as IPlayhead,
                dmcaInfo           : {} as IDmcaInfo,
                episode        : {} as IMediaEpisode,
                mediaId        : "mediaId",
                channel        : {} as IChannel,
                mediaType      : "mediaType",
                show           : currentlyPlayingMedia.show,
                trackName      : trackName,
                type           : currentlyPlayingMedia.mediaType,
                youJustHeard   : [ {
                    albumImageUrl: "imageUrl",
                    title        : "title",
                    artistName   : "artistName",
                    cut          : {} as IMediaCut
                }
                ]
            };

            expect(appStoreMock.dispatch).toHaveBeenCalled();

            expected = new SelectNowPlaying(nowPlayingData);
            expect(appStoreMock.dispatch).toHaveBeenCalled();
        });

        it("Should NOT dispatch the SelectNowPlaying action.", () =>
        {
            // Kill the subscription data.
            currentlyPlayingMedia = null;

            // Spy on the dispatching of the Redux action.
            spyOn(appStoreMock, "dispatch");

            // Create the test subject.
            testSubject = new NowPlayingStoreService(appStoreMock,
                carouselStoreService,
                currentlyPlayingService,
                channelLineupService,
                mediaPlayerFactory,
                dmcaService);

            expect(appStoreMock.dispatch).not.toHaveBeenCalled();
        });
    });

    describe("Subscribe to now playing carousel data >> ", () =>
    {
        it("Should dispatch the SelectBackgroundColor action with the background color.", () =>
        {
            // Spy on the dispatching of the Redux action.
            spyOn(appStoreMock, "dispatch");

            const backgroundColor = "#FFFF";
            carouselStoreService = {
                selectNowPlaying : jasmine.createSpy("selectNowPlaying"),
                cleanNowPlayingUpNext  : jasmine.createSpy("cleanNowPlayingUpNext"),
                nowPlayingCarousels:  observableOf({
                    pageBackgroundColor : backgroundColor
                })

            };
            // Create the test subject.
            testSubject = new NowPlayingStoreService(appStoreMock,
                carouselStoreService,
                currentlyPlayingService,
                channelLineupService,
                mediaPlayerFactory,
                dmcaService);

            expect(appStoreMock.dispatch).toHaveBeenCalled();

            expected = new SelectBackgroundColor(backgroundColor);
            expect(appStoreMock.dispatch).toHaveBeenCalledWith(expected);
        });
    });
});
