import {
  createFeatureSelector,
  createSelector
} from "@ngrx/store";
import * as moment from "moment";


export interface IWallClockStore
{
    wallClock : Date;
}

export const selectWallClockState = createFeatureSelector<IWallClockStore>("wallClockStore");

export const getWallClock = createSelector(
  selectWallClockState,
  (state: IWallClockStore): Date => state.wallClock
);
