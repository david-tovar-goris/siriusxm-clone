import { IGroupedFavorites } from "sxmServices";

export interface IFavoriteListStore
{
    favorites: IGroupedFavorites;
}
