import { IRecentlyPlayed } from "sxmServices";

export interface IRecentlyPlayedStore
{
    recentlyPlayedList : Array <IRecentlyPlayed>;
}
