import {
    createFeatureSelector,
    createSelector
} from "@ngrx/store";
import { IApronSegment } from "sxmServices";

export interface IApronSegmentStore
{
    // List of segments to show on the now playing UI.
    segments: IApronSegment[];

    // Flag indicating if the list of segments should show in the UI.
    displaySegments: boolean;

    // Flag indicating if the list of segments can expand and collapse.
    canExpandSegmentsView: boolean;

    // The currently playing segment.
    currentlyPlayingSegment: IApronSegment;
}

export const selectApronSegmentState = createFeatureSelector<IApronSegmentStore>("apronSegmentStore");

export const selectSegments = createSelector(
    selectApronSegmentState,
    (state: IApronSegmentStore): IApronSegment[] => state.segments
);

export const selectDisplaySegments = createSelector(
    selectApronSegmentState,
    (state: IApronSegmentStore): boolean => state.displaySegments
);

export const selectCanExpandSegmentsView = createSelector(
    selectApronSegmentState,
    (state: IApronSegmentStore): boolean => state.canExpandSegmentsView
);

export const selectCurrentlyPlayingSegment = createSelector(
    selectApronSegmentState,
    (state: IApronSegmentStore): IApronSegment => state.currentlyPlayingSegment
);
