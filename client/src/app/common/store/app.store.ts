import { IApronSegmentStore } from "./apron-segment.store";
import { ICarouselStore } from "./carousel.store";
import { IChannelListStore } from "./channel-list.store";
import { IFavoriteListStore } from "./favorite-list.store";
import { INowPlayingStore } from "./now-playing.store";
import { IRecentlyPlayedStore } from "./recently-played.store";
import { IWallClockStore } from "./wall-clock.store";

export interface IAppStore
{
    channelStore: IChannelListStore;
    carouselStore: ICarouselStore;
    nowPlayingStore: INowPlayingStore;
    favoriteList: IFavoriteListStore;
    recentlyPlayedStore: IRecentlyPlayedStore;
    apronSegmentStore: IApronSegmentStore;
    wallClockStore: IWallClockStore;
}
