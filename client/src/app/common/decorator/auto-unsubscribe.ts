import { Subscription } from "rxjs";

/**
 * @MODULE:     client
 * @CREATED:    09/26/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 * AutoUnSubscribe Class decorator that will automatically un-subscribe from observables and events
 * Usage @AutoUnSubscribe(["channelSubscription", "loadingSubscription"])
 */

export function AutoUnSubscribe(blackList = [])
{

    return function (constructor: any)
    {
        const originalNgOnDestroy = constructor.prototype.ngOnDestroy;

        const messageWarnings = {
            invalidArraySubscription: `No subscriptions detected. If you only have one subscription 
                    please store it as a property on the class. If you have multiple subscriptions, store them in an array named 
                    subscriptions on the class.`,
            invalidClass: "Please remove the AutoUnSubscribe decorator if your class doesn't have subscriptions."
        };

        let isValueArray = false;
        let prop;
        let value;

        //Check at least one subscription is declared in the class.
        let isClassValid: boolean = false;

        //Check whether the property value is array and type of subscription
        let isSubscriptionsArray;

        constructor.prototype.ngOnDestroy = function ()
        {
            let keys = Object.keys(this);

            for (let i = 0; i < keys.length; i++)
            {
                prop = keys[ i ];

                value = this[ prop ];

                if (blackList.indexOf(prop) > -1 || !value) continue;

                isValueArray = Array.isArray(value);

                if (isValueArray)
                {
                    if(!checkArrayIsValidSubscription(value)) continue;

                    value.forEach((subscription: Subscription) =>
                    {
                        unSubscribe(subscription);
                    });
                }
                else
                {
                    unSubscribe(value);
                }
            }

            originalNgOnDestroy && typeof originalNgOnDestroy === 'function' && originalNgOnDestroy.apply(this, arguments);

            if (!isClassValid)
            {
                console.warn(messageWarnings.invalidClass);
            }
        };

        /**
         * Unsubscribes :- if the value is a subscription it will be un-subscribed
         * @param  {value} value
         */
        function unSubscribe(value): void
        {
            if (!value || typeof value.unsubscribe !== 'function') return;

            value.unsubscribe();

            isClassValid = true;
        }

        /**
         * Check Array Value has proper subscription :-
         * @param value
         */
        function checkArrayIsValidSubscription(value)
        {

            isSubscriptionsArray = (value[ 0 ] && typeof value[ 0 ].unsubscribe === 'function');

            if (!isSubscriptionsArray) return false;

            if (prop === 'subscriptions')
            {
                return true;
            }
            else
            {
                console.warn(messageWarnings.invalidArraySubscription);
            }
        }
    };


}

