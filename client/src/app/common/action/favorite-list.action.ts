import { IAction } from "./";
import { IGroupedFavorites } from "sxmServices";

export const FAVORITES_LOADED: string = "[Favorite-List] Favorites loaded";

export class FavoritesLoaded implements IAction
{
    readonly type = FAVORITES_LOADED;

    constructor(public payload: IGroupedFavorites)
    {}
}


export type FavoriteListAction = FavoritesLoaded;
