import * as OnDemandListActions from "./on-demand-list.action";

describe("OnDemandListAction", () =>
{
    describe("SelectShow", () =>
    {
        describe("creates a Redux action", () =>
        {
            let show,
                selectShowAction;

            beforeEach(() =>
            {
                show             = {name: "showName"};
                selectShowAction = new OnDemandListActions.SelectShow(show);
            });

            it("with the new show as the payload", () =>
            {
                expect(selectShowAction.payload).toEqual(show);
            });

            it("with the SelectShow constant as the type", () =>
            {
                expect(selectShowAction.type).toEqual(OnDemandListActions.SELECT_SHOW);
            });
        });
    });

    describe("SelectAodEpisode", () =>
    {
        describe("creates a Redux action", () =>
        {
            let episode,
                selectEpisodeAction;

            beforeEach(() =>
            {
                episode             = {name: "episodeName"};
                selectEpisodeAction = new OnDemandListActions.SelectAodEpisode(episode);
            });

            it("with the new aodEpisode as the payload", () =>
            {
                expect(selectEpisodeAction.payload).toEqual(episode);
            });

            it("with the SelectAodEpisode constant as the type", () =>
            {
                expect(selectEpisodeAction.type).toEqual(OnDemandListActions.SELECT_AOD_EPISODE);
            });
        });
    });
});

