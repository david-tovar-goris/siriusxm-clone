import { IAction } from "./";

export const SELECT_SHOW: string    = "[On-Demand-List] selectShow";
export const SELECT_AOD_EPISODE: string = "[On-Demand-Show-List] selectAodEpisode";
export const SELECT_VOD_EPISODE: string = "[On-Demand-Show-List] selectVodEpisode";

export class SelectShow implements IAction
{
    readonly type = SELECT_SHOW;

    constructor(public payload)
    {
    }
}

export class SelectAodEpisode implements IAction
{
    readonly type = SELECT_AOD_EPISODE;

    constructor(public payload)
    {
    }
}

export class SelectVodEpisode implements IAction
{
    readonly type = SELECT_VOD_EPISODE;

    constructor(public payload)
    {
    }
}

export type OnDemandListAction =
    SelectShow |
    SelectAodEpisode|
    SelectVodEpisode;
