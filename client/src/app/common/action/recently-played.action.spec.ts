import * as RecentlyPlayedActions from "./recently-played.action";
import { IRecentlyPlayed } from "sxmServices";


describe("RecentlyPlayedAction", () =>
{
    describe("RecentlyPlayed", () =>
    {
        describe("creates a Redux action", () =>
        {
            let recentlyPlayedList,
                recentlyPlayedAction;

            beforeEach(() =>
            {
                recentlyPlayedList = [ {} ] as IRecentlyPlayed[],
                    recentlyPlayedAction = new RecentlyPlayedActions.loadRecentlyPlayedList(recentlyPlayedList);
            });

            it("with the new recently played array as the payload", () =>
            {
                expect(recentlyPlayedAction.payload).toEqual(recentlyPlayedList);
            });

            it("with the LOAD_RECENTLY_PLAYED_LIST constant as the type", () =>
            {
                expect(recentlyPlayedAction.type).toEqual(RecentlyPlayedActions.LOAD_RECENTLY_PLAYED_LIST);
            });
        });
    });
});
