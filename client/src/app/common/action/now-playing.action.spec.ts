/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />

import * as NowPlayingActions from "./now-playing.action";

describe("NowPlayingAction", () =>
{
    describe("SelectNowPlaying", () =>
    {
        describe("creates a Redux action", () =>
        {
            let nowPlayingPayload,
                selectNowPlayingAction;

            beforeEach(() =>
            {
                nowPlayingPayload      = {
                    channelName    : "nowPlayingChannel",
                    channelNumber  : "1234",
                    artistName     : "nowPlayingArtist",
                    trackName      : "nowPlayingTrackName",
                    albumName      : "nowPlayingAlbumName",
                    albumImage     : "nowPlayingAlbumImage",
                    artistInfo     : "nowPlayingArtistInfo",
                    backgroundImage: "nowPlayingBackgroundImage",
                    showName       : "nowPlayingShowName",
                    type           : "live"
                };
                selectNowPlayingAction = new NowPlayingActions.SelectNowPlaying(nowPlayingPayload);
            });

            it("with the new now playing as the payload", () =>
            {
                expect(selectNowPlayingAction.payload).toEqual(nowPlayingPayload);
            });

            it("with the SelectNowPlaying constant as the type", () =>
            {
                expect(selectNowPlayingAction.type).toEqual(NowPlayingActions.SELECT_NOW_PLAYING);
            });
        });
    });
});

