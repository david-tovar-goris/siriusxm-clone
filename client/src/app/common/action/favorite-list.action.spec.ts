import * as FavoriteListActions from "./favorite-list.action";
import { IFavoriteItem } from "sxmServices";

describe("FavoriteListActions", () =>
{
    describe("FavoritesLoaded", () =>
    {
        describe("creates a Redux action", () =>
        {
            let favArray,
            favLoadedAction;

            beforeEach(() =>
            {
                favArray = [{}] as IFavoriteItem[],
                favLoadedAction = new FavoriteListActions.FavoritesLoaded(favArray);
            });

            it("with the new favorites array as the payload", () =>
            {
                expect(favLoadedAction.payload).toEqual(favArray);
            });

            it("with the FAORITES_LOADED constant as the type", () =>
            {
                expect(favLoadedAction.type).toEqual(FavoriteListActions.FAVORITES_LOADED);
            });
        });
    });
});

