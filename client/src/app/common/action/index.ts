import { Action } from "@ngrx/store";

export interface IAction extends Action
{
    payload: any;
}

export * from "./favorite-list.action";
export * from "./recently-played.action";
export * from "./wall-clock.action";
