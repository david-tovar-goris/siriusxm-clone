export class LocalizeTileTextConst
{
    public static SUNDAY : string = "common.daysOfTheWeek.sunday";
    public static MONDAY : string = "common.daysOfTheWeek.monday";
    public static TUESDAY : string = "common.daysOfTheWeek.tuesday";
    public static WEDNESDAY : string = "common.daysOfTheWeek.wednesday";
    public static THURSDAY : string = "common.daysOfTheWeek.thursday";
    public static FRIDAY : string = "common.daysOfTheWeek.friday";
    public static SATURDAY : string = "common.daysOfTheWeek.saturday";
    public static TODAY : string = "common.today";
    public static YESTERDAY : string = "common.yesterday";
    public static TOMORROW : string = "common.tomorrow";

    public static MINUTES : string = "common.minutes";
    public static HOURS : string = "common.hours";
    public static DURATION : string = "common.duration";
}
