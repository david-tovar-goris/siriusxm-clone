/**
 * @MODULE:     client
 * @CREATED:    10/05/2017
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  Custom Order/Sort Pipe that sorts by date, most recent by default.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { OrderByPipeConst } from './order-by.pipe.const';
export type OrderPreference = "reverse" | "descending" | "ascending";
@Pipe({
    name: 'orderBy'
})

export class OrderByPipe implements PipeTransform {
    /**
     * Returns an array of sorted episodes based on the dateProp passed.
     * The order property by default is most recent, but you can pass an optional arg 'reverse' or 'descending'.
     * @param {any[]} list
     * @param {string} dateProp
     * @param {OrderPreference} order
     * @returns {any[]}
     */
    transform(list: any[],
              dateProp: string,
              order?: OrderPreference)
    {
        if(list.length === 0 || dateProp === '') return list;

        return list.sort((item1, item2) =>
            {
                let episodeTime1 = new Date(item1[dateProp]).getTime();
                let episodeTime2 = new Date(item2[dateProp]).getTime();

                if(order === OrderByPipeConst.REVERSE || order === OrderByPipeConst.DESCENDING)
                {
                    return episodeTime1 - episodeTime2;
                }
                else
                {
                    return episodeTime2 - episodeTime1;
                }
            });
    }
}
