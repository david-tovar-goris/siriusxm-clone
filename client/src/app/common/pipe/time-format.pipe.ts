/**
 * @MODULE:     client
 * @CREATED:    10/06/2017
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  Custom Time Format Pipe
 *  Used in the "On Demand" episodes list to format episode duration and airdate.
 */
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { TranslateService } from "@ngx-translate/core";

export type TimeFormatType = 'episodeDuration' | 'episodeAirDate' | 'episodeAirDateOnly';

@Pipe({
    name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

    private airDateString: string = "";

    constructor(private translate: TranslateService)
    {
        this.translate.get('timeFormat.airDate').subscribe((data) =>
        {
             this.airDateString = data + " ";
        });
    }

    /**
     * Presently takes an episode object and an argument of the type formatted time
     * and returns a formatted time.
     *
     * episodeDuration ex: 5h 15m
     * episodeAirDate ex: (if same week) Tuesday OR 10.02.17
     *
     * @param item
     * @param {TimeFormatType} formatType
     * @returns {any}
     */
    transform(item,
              formatType: TimeFormatType) : any
    {
        let formattedTime;

        switch(formatType)
        {
            case 'episodeDuration':
                formattedTime = this.getEpisodeDuration(item);
            break;

            case 'episodeAirDate':
                formattedTime = this.getEpisodeAirDate(item);
            break;

            case 'episodeAirDateOnly':
                formattedTime = this.getEpisodeAirDateOnly(item);
                break;

            default:
                formattedTime = item;
        }

        return formattedTime;
    }

    /**
     * Returns a formatted episode duration based on whether the duration has hours/minutes
     * @param item
     * @returns {string}
     */
    getEpisodeDuration(item): any
    {
        if (item && (item.duration || item.vodDuration))
        {
            const duration = item.duration || item.vodDuration,
                  formattedDuration = moment.duration(duration),
                  newHours = formattedDuration.hours() ? formattedDuration.hours() + 'h ' : '',
                  newMinutes = formattedDuration.minutes() ? formattedDuration.minutes() + 'm' : '';

            return newHours + newMinutes;
        }

        return 0;
    }

    /**
     * Returns a formatted episode airdate based on whether it's the same week or not.
     * @param item
     * @returns {string}
     */
    getEpisodeAirDate(item): string
    {
        if(!item.originalAirDate) return "";

        const presentDate = moment(),
            episodeAirDate = moment(item.originalAirDate),
            isSameWeek: boolean = episodeAirDate.isSame(presentDate, 'week');
        return isSameWeek ? episodeAirDate.format('dddd') : this.airDateString + episodeAirDate.format('MM.DD.YY');
    }

    /**
     * Returns a formatted episode airdate (MM.DD.YY)
     * @param item
     * @returns {string}
     */
    getEpisodeAirDateOnly(item): string
    {
        return moment(item.originalAirDate).format('MM.DD.YY');
    }
}
