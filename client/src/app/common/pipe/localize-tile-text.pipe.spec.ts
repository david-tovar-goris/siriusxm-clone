import { MockTranslateService } from "../../../../test/mocks/translate.service";
import { LocalizeTileTextPipe } from "./localize-tile-text.pipe";

describe('LocalizeTileTextPipe', () =>
{
    let localizeTileTextPipe: LocalizeTileTextPipe,
        input:string;

    beforeEach(() =>
    {
        localizeTileTextPipe = new LocalizeTileTextPipe(MockTranslateService.getSpy());
    });

    describe('when the input is not in valid format : Today|Tomorrow|Yesterday 2h 6m', () =>
    {
        it('should return the original value', () =>
        {
            input = 'this is just the text without any recognizable text pattern';
            expect(localizeTileTextPipe.transform(input)).toEqual(input);
        });
    });

    describe('when the input has valid pattern e.g. Today|Tomorrow|Yesterday 2h 6m', () =>
    {
        it('should return the localized string with : Today', () =>
        {
            input = 'Today 2h 6m';
            expect(localizeTileTextPipe.transform('today 2h 6m')).toEqual(input);
        });

        it('should return the localized string with : Tomorrow', () =>
        {
            input = 'Tomorrow 2h 6m';
            expect(localizeTileTextPipe.transform('tomorrow 2h 6m')).toEqual(input);
        });

        it('should return the localized string with : Yesterday', () =>
        {
            input = 'Yesterday 2h 6m';
            expect(localizeTileTextPipe.transform('yesterday 2h 6m')).toEqual(input);
        });

        it('should return the localized string with : Yesterday', () =>
        {
            input = 'This is test for string with : Yesterday 2h 6m';
            expect(localizeTileTextPipe.transform('This is test for string with : yesterday 2h 6m')).toEqual(input);
        });
    });
});
