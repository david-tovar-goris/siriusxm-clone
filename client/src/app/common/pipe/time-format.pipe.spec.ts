import { TimeFormatPipe } from "./time-format.pipe";
import { MockTranslateService } from "../../../../test/mocks/translate.service";

describe('TimeFormatPipe', () =>
{
    let timeFormatPipe: TimeFormatPipe,
        time = {},
        timeFormatType,
        formattedTime,
        presentTime;

    beforeEach(() =>
    {
        timeFormatPipe = new TimeFormatPipe( MockTranslateService.getSpy());
        presentTime = new Date("2017-11-09T10:00:00.000+0000");
        jasmine.clock().mockDate(presentTime);
    });

    afterEach(() =>
    {
        jasmine.clock().uninstall();
    });

    describe('when the formatType is not "episodeDuration" or "episodeAirDate"', () =>
    {
        it('should return the original item', () =>
        {
            time = 'timestamp';
            timeFormatType = 'unknown';

            expect(timeFormatPipe.transform(time, timeFormatType)).toEqual(time);
        });
    });

    describe('when the formatType is "episodeDuration"', () =>
    {
        it('should return a formatted episode duration', () =>
        {
            time =
            {
                duration: "P0DT6H1M6.000S"
            };
            timeFormatType = "episodeDuration";
            formattedTime = '6h 1m';

            expect(timeFormatPipe.transform(time, timeFormatType)).toEqual(formattedTime);
        });
    });

    describe('when the formatType is "episodeAirDate" and the date is within a week', () =>
    {
        it('should return a formatted episode air date for the day of the week', () =>
        {
            time =
            {
                originalAirDate: "2017-11-09T10:00:00.000+0000"
            };
            timeFormatType = "episodeAirDate";
            formattedTime = 'Thursday';

            expect(timeFormatPipe.transform(time, timeFormatType)).toEqual(formattedTime);
        });
    });

    describe('when the formatType is "episodeAirDate" and the date is not within a week', () =>
    {
        it('should return a formatted episode air date for the day of the week', () =>
        {
            const timeFormatTranslation = {
                "airDate": "AirDate "
            };
            time =
                {
                    originalAirDate: "2017-09-09T10:00:00.000+0000"
                };

            timeFormatType = "episodeAirDate";
            formattedTime = timeFormatTranslation.airDate + '09.09.17';
            expect(timeFormatPipe.transform(time, timeFormatType)).toEqual(formattedTime);
        });
    });

    describe('when the formatType is "episodeAirDateOnly"', () =>
    {
        it('should return a formatted episode air date', () =>
        {
            time =
                {
                    originalAirDate: "2017-09-09T10:00:00.000+0000"
                };
            timeFormatType = "episodeAirDateOnly";
            formattedTime = '09.09.17';

            expect(timeFormatPipe.transform(time, timeFormatType)).toEqual(formattedTime);
        });
    });
});
