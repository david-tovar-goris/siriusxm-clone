/**
 * @MODULE:     client
 * @CREATED:    03/06/2018
 * @COPYRIGHT:  2018 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  Custom Pipe to localize tile text
 *  Typically used in tiles e.g. line1/line2.
 */
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { DateUtilConst } from "../../../../../servicelib/src/util";
import { LocalizeTileTextConst } from "./localize-tile-text.const";

@Pipe({
    name: "localizeTileText"
})
export class LocalizeTileTextPipe implements PipeTransform {
    private translationData = [
        {
            "key"        : LocalizeTileTextConst.SUNDAY,
            "regex"      : new RegExp(DateUtilConst.SUNDAY, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.MONDAY,
            "regex"      : new RegExp(DateUtilConst.MONDAY, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.TUESDAY,
            "regex"      : new RegExp(DateUtilConst.TUESDAY, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.WEDNESDAY,
            "regex"      : new RegExp(DateUtilConst.WEDNESDAY, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.THURSDAY,
            "regex"      : new RegExp(DateUtilConst.THURSDAY, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.FRIDAY,
            "regex"      : new RegExp(DateUtilConst.FRIDAY, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.SATURDAY,
            "regex"      : new RegExp(DateUtilConst.SATURDAY, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.TODAY,
            "regex"      : new RegExp(DateUtilConst.TODAY, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.YESTERDAY,
            "regex"      : new RegExp(DateUtilConst.YESTERDAY, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.TOMORROW,
            "regex"      : new RegExp(DateUtilConst.TOMORROW, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.DURATION,
            "regex"      : new RegExp(DateUtilConst.DURATION, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.MINUTES,
            "regex"      : new RegExp(DateUtilConst.MINUTES, "gi"),
            "translation": ""
        },
        {
            "key"        : LocalizeTileTextConst.HOURS,
            "regex"      : new RegExp(DateUtilConst.HOURS, "gi"),
            "translation": ""
        }
    ];

    constructor(private translate: TranslateService)
    {
        this.translationData.forEach((translationObject) =>
        {
            this.translate.get(translationObject.key).subscribe((data: string) => translationObject.translation = data);
        });
    }

    /**
     * it takes an string with the pattern like Today/Tomorrow/Yesterday/ANY_DAY_OF_THE_WEEK e.g. Today · 43m
     * and returns the same string with localized days.
     * @param item
     */
    transform(item: string): string
    {
        if (!item) return "";

        let transformedVal: string = item;

        this.translationData.forEach((translationObject) =>
        {
            if(transformedVal.match(translationObject.regex))
            {
                transformedVal = transformedVal.replace(translationObject.regex, translationObject.translation);
            }
        });

        return transformedVal;
    }
}
