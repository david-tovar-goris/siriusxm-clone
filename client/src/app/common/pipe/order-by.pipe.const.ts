export class OrderByPipeConst
{
    public static REVERSE : string = "reverse";
    public static DESCENDING : string = "descending";
}
