import { OrderByPipe, OrderPreference  } from "./order-by.pipe";

describe('OrderBy Pipe', () =>
{
    let orderByPipe: OrderByPipe,
        episodeList = [],
        dateProp: string,
        order: OrderPreference;

     beforeEach(() =>
    {
        orderByPipe = new OrderByPipe();
    });

    describe('when the aodEpisode list is empty', () =>
    {
        it('returns the original aodEpisode list', () =>
        {
            episodeList = [];
            dateProp = 'originalAirDate';

            expect(orderByPipe.transform(episodeList, dateProp)).toEqual(episodeList);
        });
    });

    describe('when the dateProp is an empty string', () =>
    {
        it('returns the original aodEpisode list', () =>
        {
            episodeList = [ {} ];
            dateProp = '';

            expect(orderByPipe.transform(episodeList, dateProp)).toEqual(episodeList);
        });
    });

    describe('when passed an aodEpisode list with an originalAirDate property', () =>
    {
        it('should return an array with the episodes sorted most recent by originalAirDate', () =>
        {
            episodeList = [
                {
                    originalAirDate: "2017-09-04T10:00:00.000+0000"
                },
                {
                    originalAirDate: "2017-10-28T10:00:00.000+0000"
                }
            ];
            dateProp = 'originalAirDate';

            const newEpisodeList = [
                {
                    originalAirDate: "2017-10-28T10:00:00.000+0000"
                },
                {
                    originalAirDate: "2017-09-04T10:00:00.000+0000"
                }
            ];

            expect(orderByPipe.transform(episodeList, dateProp)).toEqual(newEpisodeList);
        });
    });

    describe('when passed an aodEpisode list with an originalAirDate property and order property of reverse', () =>
    {
        it('should return an array with the episodes sorted latest by originalAirDate', () =>
        {
            episodeList = [
                {
                    originalAirDate: "2017-10-28T10:00:00.000+0000"
                },
                {
                    originalAirDate: "2017-09-04T10:00:00.000+0000"
                }
            ];
            dateProp = 'originalAirDate';
            order = 'reverse';

            const newEpisodeList = [
                {
                    originalAirDate: "2017-09-04T10:00:00.000+0000"
                },
                {
                    originalAirDate: "2017-10-28T10:00:00.000+0000"
                }
            ];

            expect(orderByPipe.transform(episodeList, dateProp, order)).toEqual(newEpisodeList);
        });
    });

});
