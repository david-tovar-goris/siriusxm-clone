const KEY_TAB = 9;

export class FocusUtil
{
    private static previouslyFocusedElement;

    private static onClose;

    /**
     * Places focus on first actionable element in a dialog element.  If overlay/modal is outside of 'app-wrap' element,
     * hideAppWrap must be set to 'true'.
     * @param dialogElement modal/overlay
     * @param {boolean} isAppWrapHidden boolean indicating whether app-wrap should be hidden from screen reader
     */
    public static setupAccessibleDialog(dialogElement, isAppWrapHidden: boolean = false)
    {
        if (!dialogElement) return;
        document.getElementsByClassName('app-wrap')[0].setAttribute('aria-hidden', isAppWrapHidden.toString());
        FocusUtil.setupFocusTrap(dialogElement, dialogElement.querySelectorAll('[data-focus-trap]'));
    }

    public static closeFocusedDialog(): void
    {
        document.getElementsByClassName('app-wrap')[0].setAttribute('aria-hidden', 'false');
        if (this.onClose) { this.onClose(); }
        if (this.previouslyFocusedElement) { this.previouslyFocusedElement.focus(); }
    }

    private static setupKeyDownEvent(dialogElement, nodeList)
    {
        if (!dialogElement) return;
        const keyDownEventListener = (e) =>
        {
            switch (e.keyCode)
            {
                case KEY_TAB:
                    if (nodeList.length === 1)
                    {
                        e.preventDefault();
                        nodeList[0].focus();
                    }

                    e.shiftKey ? FocusUtil.handlesBackwardsTab(nodeList, e) : FocusUtil.handleForwardsTab(nodeList, e);
                    break;
                default:
                    break;
            }
        };

        dialogElement.addEventListener('keydown', keyDownEventListener);
        this.onClose = () =>
        {
            dialogElement.removeEventListener('keydown', keyDownEventListener);
        };
    }

    private static setFocusToElement(element)
    {
        FocusUtil.previouslyFocusedElement = document.activeElement;
        element.focus();
    }

    private static setupFocusTrap(dialogElement, nodeList)
    {
        if (!dialogElement) return;
        FocusUtil.setFocusToElement(nodeList[0]);
        FocusUtil.setupKeyDownEvent(dialogElement, nodeList);
    }

    private static handlesBackwardsTab(nodeList, event)
    {
        if (document.activeElement === nodeList[0])
        {
            event.preventDefault();
            nodeList[nodeList.length - 1].focus();
        }
    }

    private static handleForwardsTab(nodeList, event)
    {
        if (document.activeElement === nodeList[nodeList.length - 1])
        {
            event.preventDefault();
            nodeList[0].focus();
        }
    }

    public static readonly DEFAULT_FOCUSED_INDEX = 9999;
}
