import { Inject } from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { inCompanyBeta,
         inDev,
         inQA } from "sxmServices";

export class PWAUtil{

    constructor( @Inject(DOCUMENT) private document: any){}

    /**
     * Returns manifest file name with respective to domain name.
     * if no domain matches it returns default manifest file name as prod.
     */
    public static getManifestFileName():string
    {
        const hostName = window.location.host;
        let manifestFileName = "manifest";

        if(inCompanyBeta(hostName))
        {
            manifestFileName = `${manifestFileName}-cb`;
        }
        else if(inQA(hostName))
        {
            manifestFileName = `${manifestFileName}-qa`;
        }
        else if(inDev(hostName))
        {
            manifestFileName = `${manifestFileName}-local`;
        }

        return `${manifestFileName}.json`;
    }

    public static setManifestFile()
    {
        const linkEle =  document.querySelector('#sxm-manifest-placeholder');
        if(linkEle)
        {
            linkEle.setAttribute('href', `/${this.getManifestFileName()}`);
        }
    }
}
