import { ITile } from "sxmServices";

export class TileUtil
{

    public static normalizeImageUrl(url: string, queryParam: string = "")
    {
        if(!url) return null;

        url =  url.match(/\?/) ?
               url + "&" + queryParam :
               url + "?" + queryParam ;

        return url;
    }

    public static getDefaultBackground(tile: ITile, fallbackColor: string = "")
    {

        let backgroundColor: string = fallbackColor;

        if(tile.tileAssetInfo.isPandoraPodcast)
        {
            backgroundColor = "#243A5C";
        }

        return {
                'background-color': backgroundColor,
                'background-image': 'linear-gradient(to bottom, rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0))'
        };
    }

}
