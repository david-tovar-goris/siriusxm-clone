import { ApplicationForegroundChecker } from "./applicationForegroundChecker";

describe('ApplicationForegroundChecker', () =>
{
    describe('init()', () =>
    {
        it('adds an event listener for `visibilitychange`', () =>
        {
            spyOn(document, 'addEventListener');
            ApplicationForegroundChecker.init();
            expect((document.addEventListener as any).calls.argsFor(0)[0]).toEqual('visibilitychange');
        });
    });
});
