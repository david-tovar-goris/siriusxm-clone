export class ApplicationForegroundChecker
{
    private static visible: boolean = true;

    public static get appIsInForeground(): boolean
    {
        return ApplicationForegroundChecker.visible;
    }

    public static init(): void
    {
        document.addEventListener('visibilitychange',
            ApplicationForegroundChecker.handleVisibilityChange,
            false);
    }

    private static handleVisibilityChange(): boolean
    {
        if (document.hidden)
        {
            return ApplicationForegroundChecker.visible = false;
        }
        else
        {
            return ApplicationForegroundChecker.visible = true;
        }
    }
}
