import { BrowserUtil }          from "app/common/util/browser.util";
import { PrivateBrowsingUtil } from "./privateBrowsing.util";
import { testObservable }      from "../../../../../servicelib/src/test/test.util";
import { take } from 'rxjs/operators';

describe('checkPrivateBrowsingMode', () =>
{
  beforeEach(() =>
  {
    this._window = {
      navigator : {},
      indexedDB: {
        open: () => {}
      }
    };
  });

  describe('when the browser is Chrome', () =>
  {
    beforeEach(() =>
    {
      spyOn(BrowserUtil,"getBrowserName").and.returnValue(BrowserUtil.BROWSERS.CHROME);
      spyOn(BrowserUtil,"isChrome").and.returnValue(true);
    });

    describe('when the filesystem api is available', () =>
    {
      beforeEach(() =>
      {
        this._window.RequestFileSystem = jasmine.createSpy('RequestFileSystem').and.callFake((unusedA, unusedB, onSuccess) =>
        {
          onSuccess();
        });
      });

      it('requests file system resources', (done) =>
      {
        testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
        {
          expect(this._window.RequestFileSystem).toHaveBeenCalledWith(this._window.TEMPORARY,
                                                                jasmine.any(Number),
                                                                jasmine.any(Function),
                                                                jasmine.any(Function));
          done();
        });
      });

      describe('when requesting fs resources errors', () =>
      {
        it('emits true', (done) =>
        {
          this._window.RequestFileSystem.and.callFake((unusedA, unusedB, unusedOnSuccess, onError) =>
          {
            onError();
          });

          testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
          {
            expect(bool).toBe(true);
            done();
          });
        });
      });

      describe('when requesting fs resources succeeds', () =>
      {
        it('emits false', (done) =>
        {
          this._window.RequestFileSystem.and.callFake((unusedA, unusedB, onSuccess) =>
          {
            onSuccess();
          });

          testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
          {
            expect(bool).toBe(false);
            done();
          });
        });
      });
    });

    describe('when the filesystem api is unavailable', () =>
    {
      it('emits false', (done) =>
      {
        this._window.RequestFileSystem = null;
        this._window.webkitRequestFileSystem = null;

        testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
        {
          expect(bool).toBe(false);
          done();
        });
      });
    });
  });

  describe('when the browser is FireFox', () =>
  {
    beforeEach(() =>
    {
        spyOn(BrowserUtil,"getBrowserName").and.returnValue(BrowserUtil.BROWSERS.FIREFOX);
    });

    describe('when the indexedDB API is available', () =>
    {
      beforeEach(() =>
      {
        spyOn(this._window.indexedDB, 'open');
      });

      it('tries to access the indexedDB', (done) =>
      {
        let db = { onerror: null, onsuccess: null };
        this._window.indexedDB.open.and.callFake(() => db);

        testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
        {
          expect(this._window.indexedDB.open).toHaveBeenCalled();
          done();
        });

        db.onsuccess();
      });

      describe('when accessing the indexedDB runtime errors', () =>
      {
        it('emits true', (done) =>
        {
          this._window.indexedDB.open.and.callFake(() =>
          {
            throw new Error("TEST");
          });

          testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
          {
            expect(bool).toBe(true);
            done();
          });
        });
      });

      describe('when accessing the indexedDB calls it\'s error callback', () =>
      {
        it('emits true', (done) =>
        {
          let db = { onerror: null, onsuccess: null };
          this._window.indexedDB.open.and.returnValue(db);
          testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
          {
            expect(bool).toBe(true);
            done();
          });
          db.onerror();
        });
      });

      describe('when accessing the indexedDB succeeds', () =>
      {
        it('emits false', (done) =>
        {
          let db = { onerror: null, onsuccess: null };
          this._window.indexedDB.open.and.returnValue(db);

          testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
          {
            expect(bool).toBe(false);
            done();
          });

          db.onsuccess();
        });
      });
    });

    describe('when the indexedDB API is unavailable', () =>
    {
      it('emits false', (done) =>
      {
        this._window.indexedDB = null;

        testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
        {
          expect(bool).toBe(false);
          done();
        });
      });
    });
  });

  describe('when the browser is IE or Edge', () =>
  {
    beforeEach(() =>
    {
      spyOn(BrowserUtil,"getBrowserName").and.returnValue(BrowserUtil.BROWSERS.EDGE);
      this._window.PointerEvent = {};
      this._window.MSPointerEvent = {};
    });

    describe('when the browser is less than IE 10', () =>
    {
      it('emits false', (done) =>
      {
        this._window.PointerEvent = null;
        this._window.MSPointerEvent = null;

        testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
        {
          expect(bool).toBe(false);
          done();
        });
      });
    });

    describe('the indexedDB API is available', () =>
    {
      it('emits false', (done) =>
      {
        testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
        {
          expect(bool).toBe(false);
          done();
        });
      });
    });
  });

  describe('when the browser cannot be detected', () =>
  {
    it('emits false', (done) =>
    {
      this._window.navigator.userAgent = 'SOME OTHER BROWSER';

      testObservable(PrivateBrowsingUtil.checkPrivateBrowsingMode(this._window).pipe(take(1)), (bool) =>
      {
        expect(bool).toBe(false);
        done();
      });
    });
  });
});
