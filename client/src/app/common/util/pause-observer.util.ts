import { switchMap } from 'rxjs/operators';
import { Subject , Observable, NEVER } from "rxjs";


export interface IPausibleStream
{
    pauseController: Subject<any>;
    pausibleStream: Observable<any>;
}

/**
 * Make a stream pausible -- this functionality has yet to make it into RxJS 5.x so this function wraps the
 * underlying impl that the authors use: https://github.com/ReactiveX/rxjs/issues/1542
 *
 * Once created the stream can be paused and resumed by calling `pauseController.next(true)` and
 * `pauseController.next(false)`respectively.
 *
 * NOTE: The diff is in that in RxJS 4.x one can simply call `observable.pausible()`
 *
 * @param {Observable<any>} target
 * @returns {IPausibleStream}
 */
export function makeStreamPausible(target: Observable<any>, keepValues: boolean = false): IPausibleStream
{
    // Create a cold observable.
    const pauseController: Subject<any> = new Subject();

    // All the magic is here -- when `pausable.next()` is called the boolean passed will either
    // pause or resume the stream. Passing true pauses the stream as `Observable.never()` is like
    // a no-op function and never pushes a value; conversely, passing `false` resumes the stream
    // and returns the target stream.
    const pausibleStream = pauseController.pipe(switchMap(paused => paused ? NEVER : target));

    // Create the result that contains the stream to pause and the stream that controls the pausing.
    const result: IPausibleStream =
    {
        pauseController: pauseController,
        pausibleStream: pausibleStream
    };

    return result;
}
