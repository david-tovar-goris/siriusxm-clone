import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { EdpScheduleItemModule } from "../edp-schedule/edp-schedule-item.module";
import { VerticalListEdpScheduleComponent } from "./vertical-list-edp-schedule.component";
import { SharedModule } from "app/common/shared.module";

@NgModule({
    imports: [
        CommonModule,
        EdpScheduleItemModule,
        SharedModule
    ],
    declarations: [
        VerticalListEdpScheduleComponent
    ],
    providers: [
    ],
    exports     : [
        VerticalListEdpScheduleComponent
    ]
})

export class VerticalListEdpScheduleModule
{
}
