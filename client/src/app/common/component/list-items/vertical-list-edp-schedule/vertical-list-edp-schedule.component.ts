import {
    Component,
    Input
} from "@angular/core";
import {
    ICarouselData,
    Logger
} from "sxmServices";

@Component({
    selector   : "vertical-list-edp-schedule",
    templateUrl: "./vertical-list-edp-schedule.component.html",
    styleUrls  : [ "./vertical-list-edp-schedule.component.scss" ]
})

export class VerticalListEdpScheduleComponent
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("VerticalListEdpScheduleComponent");

    /**
     * schedule Type : Channel / Show / Episode
     */
    @Input() scheduleType?: string = '';

    /**
     * sets the local properties
     * @param setCarouselData
     */
    @Input()
    set carouselData(setCarouselData: ICarouselData)
    {
        this.title = setCarouselData.title && setCarouselData.title.textValue ? setCarouselData.title.textValue : "";
        this._carouselData = setCarouselData;
    }

    /**
     * returns the carousel data
     */
    get carouselData() : ICarouselData { return this._carouselData; }

    /**
     * To provide the title to the template
     */
    public title: string = "";

    /**
     * stores the carousel data.
     */
    private _carouselData: ICarouselData;

    /**
     * Constructor
     */
    constructor() {}

    /**
     * Adding trackBy function for *ngFor to increase performance
     */
    public trackByFn(index, item)
    {
        return index;
    }
}
