import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import {
    ChannelLineupService,
    ITile,
    TunePayload,
    MediaPlayerService
} from "sxmServices";
import { MockComponent } from "../../../../../../test/mocks/component.mock";
import { NowPlayingIndicatorServiceMock } from "../../../../../../test/mocks/now-playing-indicator.service.mock";
import { TranslationModule } from "../../../../translate/translation.module";
import { NowPlayingIndicatorService } from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { TuneServiceMock } from "../../../../../../test/mocks/tune.service.mock";
import { TuneClientService } from "../../../service/tune/tune.client.service";
import { EpisodeTileComponent } from "./episode-list-item.component";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import { TileContextMenuOptionsServiceMock } from "../../../../../../test/mocks/tile-context-menu-options.service.mock";
import { SharedModule } from "../../../shared.module";
import { CarouselService } from "../../../../carousel/carousel.service";
import { CarouselServiceMock } from "../../../../../../test/mocks";
import { MediaPlayerServiceMock } from "../../../../../../test/mocks/media-player.service.mock";
import { NeriticLinkService } from "app/neritic-links";
import { NeriticLinkServiceMock } from "../../../../../../test/mocks/neritic-link.service.mock";

describe("EpisodeListItemComponent", () =>
{
    let component: EpisodeTileComponent,
        fixture: ComponentFixture<EpisodeTileComponent>,
        tuneClientService: TuneClientService,
        tileData : ITile = {
            primaryNeriticLink: { contentType: "", channelId: '' },
            tileAssetInfo     : { showGuid: '', aodEpisodecaId: "testGuid" },
            neriticLinkData   : [ { contentSubType: 'aod' } ],
            fgImageUrl        : "url",
            bgImageUrl        : "url",
            tileContentSubType: "aod",
            iconImageType     : "icon-center",
            tileBanner        : {display: true},
            channelInfo       : {channelId: 'testId', artwork: { background: { url: 'url'}}}
        } as ITile;

    beforeEach(() =>
    {
        tileData.line3 = undefined;
        EpisodeTileComponent.prototype.line3Ellipse =
            {
                nativeElement: {
                    innerHTML   : "",
                    scrollHeight: 0,
                    offsetHeight: 0
                }
            };
         TestBed.configureTestingModule({
            imports     : [
                TranslationModule,
                SharedModule
            ],
            declarations: [
                EpisodeTileComponent,
                MockComponent({
                    selector: "favorite-btn",
                    inputs  : [
                        "listAsset",
                        "color",
                        "viewType"
                    ]
                }),
                MockComponent({ selector: "context-menu", inputs: [ "getOptionsFunc", "ariaText" ] })
            ],
            providers   : [
                { provide: TileContextMenuOptionsService, useClass: TileContextMenuOptionsServiceMock },
                { provide: NowPlayingIndicatorService, useClass: NowPlayingIndicatorServiceMock },
                { provide: TuneClientService, useClass: TuneServiceMock },
                { provide: CarouselService, useClass: CarouselServiceMock },
                { provide: MediaPlayerService, useClass: MediaPlayerServiceMock},
                { provide: NeriticLinkService, useClass: NeriticLinkServiceMock}
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture     = TestBed.createComponent(EpisodeTileComponent);
        tuneClientService = TestBed.get(TuneClientService);
        component   = fixture.componentInstance;
        component.tileData = tileData;
        fixture.detectChanges();
    });

    describe("getShowImage()", () =>
    {
        it("should fetch a show image", () =>
        {
            spyOn(ChannelLineupService, "getShowOrEpisodeImage").and.returnValue("testImage");
            const imageStyle = component.getShowImage();
            expect(imageStyle)
                .toEqual(`${component.tileData.bgImageUrl}?width=320&height=240&preserveAspect=true`);
        });
    });
});
