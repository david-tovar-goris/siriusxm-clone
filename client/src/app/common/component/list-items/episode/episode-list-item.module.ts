import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ContextMenuModule } from "../../../../context-menu/context-menu.module";
import { FavoritesModule } from "../../../../favorites/favorites.module";
import { TranslationModule } from "../../../../translate/translation.module";
import { SharedModule } from "../../../shared.module";
import { EpisodeTileComponent } from "./episode-list-item.component";

@NgModule({
    imports     : [
        CommonModule,
        ContextMenuModule,
        FavoritesModule,
        TranslationModule,
        SharedModule
    ],
    declarations: [
        EpisodeTileComponent
    ],
    exports     : [
        EpisodeTileComponent
    ]
})
export class EpisodeTileModule
{
}
