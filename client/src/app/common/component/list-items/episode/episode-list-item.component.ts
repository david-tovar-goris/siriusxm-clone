import { map } from 'rxjs/operators';
import {
    Component,
    ElementRef,
    Input,
    ViewChild,
    AfterViewInit,
    OnDestroy
} from "@angular/core";
import {
    IFavoriteAsset,
    CarouselTypeConst,
    ITile,
    TunePayload,
    MediaPlayerService,
    ContentTypes
} from "sxmServices";

import { Observable ,  Subject } from "rxjs";
import {
    NowPlayingIndicatorService,
    tileContentType
} from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { TuneClientService } from "../../../service/tune/tune.client.service";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import { FavoriteButtonConstant } from "../../../../favorites/button/favorite-button.constant";
import { CarouselService } from "../../../../carousel/carousel.service";
import { CMOption } from "../../../../context-menu/context-menu.interface";
import { NeriticLinkService } from "app/neritic-links";
import { EllipsisStringUtil } from "app/common/util/ellipsisString.util";
import { TileUtil } from "app/common/util/tile.util";

@Component({
    selector   : "episode-list-item",
    templateUrl: "./episode-list-item.component.html",
    styleUrls  : [ "./episode-list-item.component.scss" ]
})

export class EpisodeTileComponent
{
    /////////////////////////////////////////////////////////////////////////////////////
    // Inputs
    /////////////////////////////////////////////////////////////////////////////////////
    /**
     * holds the filter text
     * @type {string}
     */
    @Input() filterText?: string = '';

    /**
     * Backed up tileData.
     */
    private _tileData: ITile;

    public backgroundStyle = null;

    private contextMenuOptions: CMOption[] = [];

    private getCMOptionsFunc: () => CMOption[];

    /**
     * show data used to populate the view
     */
    @Input()
    set tileData(tileData: ITile)
    {
        this._tileData   = tileData;
        this.channelId   = tileData.tileAssetInfo.channelId;

        this.episodeGuid = tileData.tileContentSubType === ContentTypes.AOD ? tileData.tileAssetInfo.aodEpisodecaId
                                                                            : tileData.tileAssetInfo.vodEpisodeGuid;
        this.episodeGuid = tileData.tileAssetInfo.isPandoraPodcast ? tileData.tileAssetInfo.episodeGuid : this.episodeGuid;

        this.showGuid          = tileData.tileAssetInfo.showGuid;
        this.showImageStyle    = this.getShowImage();
        this.iconImageUrl      = this.getIconImageUrl();
        this.favoriteAssetInfo =
            {
                title         : tileData.line2,
                channelId     : this.channelId,
                assetGuid     : this.episodeGuid,
                contentType   : CarouselTypeConst.EPISODE_TILE,
                subContentType: tileData.tileContentSubType,
                isIrisPodcast : tileData.tileAssetInfo.isPandoraPodcast
            };

        this.getCMOptionsFunc = function()
        {
            return this.tileData ? this.tileContextMenuOptionsService.getCMOptions(this.tileData) : [];
        }.bind(this);

        this.backgroundStyle = this.getBackgroundStyle();
    }

    /**
     * Returns the  tile data
     * @returns {IOnDemandShow}
     */
    get tileData(): ITile
    {
        return this._tileData;
    }

    /**
     * Stores the channel id
     */
    public channelId: string;

    /**
     * Stores the show guid
     */
    public showGuid: string;

    /**
     * Stores the episode guid
     */
    public episodeGuid: string;

    /**
     * Stores Show image style object
     */
    public showImageStyle: {};

    /**
     * Holds the Icon Image url
     * @type {string}
     */
    public iconImageUrl = "";

    /**
     * Hold the favorite asset info
     */
    public favoriteAssetInfo: IFavoriteAsset;

    /**
     * List View constant
     * @type {string}
     */
    public listView = FavoriteButtonConstant.LIST_VIEW;


    private clampSubscription = null;

    /**
     * Indicates FG image loaded or not.
     */
    public isImageLoaded: boolean = true;

    /**
     * Element reference to the line3Ellipse
     */
    @ViewChild('line3Ellipse') line3Ellipse: ElementRef;

    @ViewChild('titleEllipse') titleEllipse: ElementRef;

    constructor(public tileContextMenuOptionsService: TileContextMenuOptionsService,
                public nowPlayingIndicatorService: NowPlayingIndicatorService,
                private tuneClientService: TuneClientService,
                public carouselService: CarouselService,
                private mediaPlayerService : MediaPlayerService,
                private neriticLinkService: NeriticLinkService)
    {
    }

    /**
     * fetches a show image url
     */
    getShowImage(): {}
    {
        return this._tileData.bgImageUrl ? `${this._tileData.bgImageUrl}?width=320&height=240&preserveAspect=true`: '';
    }

    /**
     * fetches a icon image url
     */
    getIconImageUrl(): string
    {
        if (this._tileData.iconImageType === CarouselTypeConst.API_IMAGE_ICON_CENTER)
        {
            return this._tileData.tileContentSubType === CarouselTypeConst.VOD_EPISODE_TILE
                ? '../../assets/images/video.svg'
                : '../../assets/images/headphones-color.svg';
        }

        return this._tileData.tileContentSubType === CarouselTypeConst.VOD_EPISODE_TILE
            ? '../../assets/images/video-white.svg'
            : '../../assets/images/headphones-white.svg';
    }

    /**
     * Generates a background gradient style if the tile has a background color
     * @returns {object}
     */
    public getBackgroundStyle(): object
    {
        const backgroundColor = this._tileData.backgroundColor
                                ? this._tileData.backgroundColor
                                : null;
        const gradient = 'linear-gradient(to bottom, rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0))';

        /**
         * For Iris podcast if FG image not available display default background.
         */
        if(this.tileData.tileAssetInfo.isPandoraPodcast && !this._tileData.fgImageUrl)
        {
            return TileUtil.getDefaultBackground(this._tileData);
        }

        if(backgroundColor)
        {
            return {
                'background-color': backgroundColor,
                'background-image': gradient
            };
        }
        else
        {
            const fallbackBgUrl = this._tileData.channelInfo && this._tileData.channelInfo.artwork.background.url;
            if(this.tileData.tileAssetInfo.isPandoraPodcast && fallbackBgUrl)
            {
                return {
                    'background-image': `url(${fallbackBgUrl}?width=320&height=240&preserveAspect=true)`
                };
            }
        }
        return {
            'background-image': gradient
        };
    }

    public getImageAltText(imgAltText:string): string
    {
        return EllipsisStringUtil.ellipsisString(imgAltText, 42);
    }


    /**
     * Set default background color for tile if image fails to load
     */
    public onFGImageError(err: any)
    {
        if(this._tileData.tileAssetInfo.isPandoraPodcast)
        {
            this.isImageLoaded = false;
            this.backgroundStyle = TileUtil.getDefaultBackground(this._tileData);
        }
    }
}
