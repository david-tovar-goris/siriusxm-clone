import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import {
    Router,
    ActivatedRoute
} from "@angular/router";
import { TuneServiceMock } from "../../../../../../test/mocks/tune.service.mock";
import { TuneClientService } from "../../../service/tune/tune.client.service";
import { ChannelTileComponent } from "./channel-list-item.component";
import { ITile } from "sxmServices";
import { ChannelListServiceMock } from "../../../../../../test/mocks/channel-list.service.mock";
import { MockComponent} from "../../../../../../test/mocks/component.mock";
import { ChannelListStoreService } from "../../../service/channel-list.store.service";
import { TranslationModule } from "../../../../translate/translation.module";
import { SharedModule } from "../../../shared.module";
import { NowPlayingIndicatorService } from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { NowPlayingIndicatorServiceMock } from "../../../../../../test/mocks/now-playing-indicator.service.mock";
import { CarouselServiceMock } from "../../../../../../test/mocks/index";
import { CarouselService } from "app/carousel/carousel.service";

describe("ChannelListItem", () =>
{
    let component: ChannelTileComponent,
        fixture: ComponentFixture<ChannelTileComponent>,
        router = { navigate: jasmine.createSpy('navigate') },
        tuneClientService: TuneClientService;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SharedModule
            ],
            declarations: [
                ChannelTileComponent,
                MockComponent({ selector: "favorite-btn", inputs: ["listAsset", "color", "viewType"]} ),
                MockComponent({ selector: "context-menu", inputs: [ "getOptionsFunc" ] })
            ],
            providers: [
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: Router, useValue: router },
                { provide: ActivatedRoute, useValue: { snapshot: { params: ''} } },
                { provide: NowPlayingIndicatorService, useClass: NowPlayingIndicatorServiceMock },
                { provide: TuneClientService, useClass: TuneServiceMock },
                { provide: CarouselService, useValue: CarouselServiceMock.getSpy() }
            ]
        }).compileComponents();
        ChannelTileComponent.prototype.tileData = {
            primaryNeriticLink: { contentType: "", channelId: '' },
            tileAssetInfo     : { showGuid: '', aodEpisodecaId: "testGuid" },
            neriticLinkData   : [ { contentSubType: 'aod' } ],
            fgImageUrl        : "url",
            bgImageUrl        : "url",
            tileContentSubType: "channel",
            channelInfo       :
                {}
        } as ITile;


    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(ChannelTileComponent);
        tuneClientService = TestBed.get(TuneClientService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
});
