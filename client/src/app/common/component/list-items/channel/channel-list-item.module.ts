import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import {ChannelTileComponent} from "./channel-list-item.component";
import {ContextMenuModule} from "../../../../context-menu/context-menu.module";
import {FavoritesModule} from "../../../../favorites/favorites.module";
import {TranslationModule} from "../../../../translate/translation.module";
import {SharedModule} from "../../../shared.module";

@NgModule({
    imports: [
        CommonModule,
        ContextMenuModule,
        FavoritesModule,
        TranslationModule,
        SharedModule
    ],
    declarations: [
        ChannelTileComponent
    ],
    exports: [
        ChannelTileComponent
    ]
})
export class ChannelTileModule
{
}
