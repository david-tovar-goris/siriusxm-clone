import { map } from 'rxjs/operators';
import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnInit
} from "@angular/core";
import {ActivatedRoute}                from "@angular/router";
import { Observable }                    from "rxjs";
import {
    IFavoriteAsset,
    ITile,
    CarouselTypeConst,
    MediaUtil,
    MediaPlayerService
} from "sxmServices";
import {
    NowPlayingIndicatorService,
    tileContentType
}                                      from "../../../service/now-playing-indicator/now-playing-indicator.service";
import {TuneClientService}             from "../../../service/tune/tune.client.service";
import {CMOption}                      from "../../../../context-menu/context-menu.interface";
import {FavoriteButtonConstant}        from "../../../../favorites/button/favorite-button.constant";
import {TileContextMenuOptionsService} from "../../../../context-menu/options/tile-context-menu-options.service";
import {NeriticLinkService}            from "../../../../neritic-links/neritic-link.service";
import {FavoriteListStoreService}      from "../../../service/favorite-list.store.service";
import { CarouselService } from "app/carousel/carousel.service";

class ChannelListProps
{
    prop6 : string = "button";
    eVar6 : string = "button";
    prop8 : string = "contentTile";
    eVar8 : string = "contentTile";
    prop9 : string = "tap";
    eVar9 : string = "tap";
    eVar11: string;
    prop11: string;
    prop30: string;
    eVar30: string;
    eVar32: string;
    prop32: string;
    eVar75: string = "Tag#47";
    prop75: string = "Tag#47";

    constructor(channelName : string,filterText : string,tileNumber : string)
    {
        this.eVar11 = this.prop11 = channelName;
        this.prop30 = this.eVar30 = filterText ? "channelsFilterResultsList" : "channelsList";
        this.prop32 = this.eVar32 = tileNumber;
    }
}

class FavoriteAssetInfo
{
    title: string;
    channelId: string;
    assetGuid: string;
    contentType: string;
    subContentType: string;
    isIrisPodcast: boolean;

    constructor(tile : ITile)
    {
        this.title          = tile.tileAssetInfo.channelName;
        this.channelId      = tile.tileAssetInfo.channelId;
        this.assetGuid      = FavoriteListStoreService.getAssetGuid(tile.tileContentType, tile.tileContentSubType ,tile.tileAssetInfo);
        this.contentType    = tile.tileContentType;
        this.subContentType = MediaUtil.isMultiTrackAudioMediaType(tile.tileContentSubType)
                              ? tile.tileContentSubType
                              : CarouselTypeConst.CHANNEL_TILE;
        this.isIrisPodcast  = tile.tileAssetInfo.isPandoraPodcast;

    }
}

@Component({
               selector        : "channel-list-item",
               changeDetection : ChangeDetectionStrategy.OnPush,
               templateUrl     : "./channel-list-item.component.html",
               styleUrls       : ["./channel-list-item.component.scss"]
           })
export class ChannelTileComponent
{
    /**
     * holds the index for the channel tile
     * @type {string}
     */
    @Input() index: string = "0";

    /**
     * filter text
     */
    @Input() filterText: string = "";


    @Input() line1Classes?: string[] = [];

    @Input() line2Classes?: string[] = ['focusable', 'bold-font'];

    @Input() line3Classes?: string[] = [];

    public backgroundStyle: any = null;

    public lazyLoadUrl: string = null;


    private getCMOptionsFunc: () => CMOption[];

    /**
     * Tile data used to populate the view
     */
    private _tileData: ITile; // backing store

    @Input() set tileData(tile: ITile)
    {
        this._tileData                 = tile;
        this.favoriteAssetInfo         = new FavoriteAssetInfo(tile);

        this.getCMOptionsFunc = function()
        {
            return this.tileData ? this.tileContextMenuOptionsService.getCMOptions(this.tileData) : [];
        }.bind(this);

        this.lazyLoadUrl = this.getLazyLoadUrl();
        this.backgroundStyle = this.getBackgroundStyle();
    }

    get tileData(): ITile
    {
        return this._tileData;
    }

    constructor(public nowPlayingIndicatorService: NowPlayingIndicatorService,
                public neriticLinkService: NeriticLinkService,
                private route: ActivatedRoute,
                private tuneClientService: TuneClientService,
                private mediaPlayerService: MediaPlayerService,
                public tileContextMenuOptionsService: TileContextMenuOptionsService,
                private carouselService : CarouselService) {}

    /**
     * Favorite asset info - used to feed data into fav button component
     */
    public favoriteAssetInfo: IFavoriteAsset;

    /**
     * List View constant
     * @type {string}
     */
    public listView = FavoriteButtonConstant.LIST_VIEW;

    /**
     * Boolean flag to check if tile is Additional Channel
     */
    get isAdditionalChannel()
    {
        return this.tileData.tileContentSubType === CarouselTypeConst.ADDITIONAL_CHANNEL;
    }

    public trackTile(tile : ITile)
    {
        return tile.tileGuid;
    }

    /**
     * generates a lazyload background image url if the tile has a background image.
     * @returns {string}
     */

    public getLazyLoadUrl(): string
    {
        return this.tileData.bgImageUrl
               ? this.tileData.bgImageUrl + '?width=320&height=240&preserveAspect=true'
               : null;
    }

    /**
     * Generates a background gradient style if the tile has a background color
     * @returns {object}
     */

    public getBackgroundStyle(): object
    {
        const tileBgColor =  this.tileData.backgroundColor ? this.tileData.backgroundColor : null;
        const backgroundColor = (this.carouselService.isSeededRadioTile(this.tileData)) ? 'transparent' : tileBgColor;

        return {
            'background-color': backgroundColor,
            'background-image': 'linear-gradient(to bottom, rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0))'
        };
    }

}
