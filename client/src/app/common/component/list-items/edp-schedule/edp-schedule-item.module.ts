import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { TranslationModule } from "../../../../translate/translation.module";
import { EdpScheduleItemComponent } from "./edp-schedule-item.component";
import { ReminderButtonModule } from "../../../../reminder/reminder-button.module";

@NgModule({
    imports: [
        CommonModule,
        ReminderButtonModule,
        TranslationModule
    ],
    declarations: [
        EdpScheduleItemComponent
    ],
    exports: [
        EdpScheduleItemComponent
    ]
})
export class EdpScheduleItemModule
{
}
