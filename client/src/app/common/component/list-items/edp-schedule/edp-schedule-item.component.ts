import {
    Component,
    Input
} from "@angular/core";
import {
    CarouselTypeConst,
    ITile
} from "sxmServices";
import { TranslateService } from "@ngx-translate/core";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import { AlertClientService } from "../../../service/alert/alert.client.service";
import * as _ from "lodash";
import { ON_AIR } from "./edp-schedule-item.consts";
import { IReminderAssets } from "../../../../reminder/reminder.interface";

@Component({
    selector   : "edp-schedule-item",
    templateUrl: "./edp-schedule-item.component.html",
    styleUrls : ["./edp-schedule-item.component.scss"]
})

export class EdpScheduleItemComponent
{

    /**
     * schedule Type : Channel / Show / Episode
     */
    @Input() scheduleType?: string = '';

    /**
     * Backed up tileData.
     */
    private _tileData: ITile;

    public isOnAir : boolean = false;
    public isShowSchedule : boolean = false;
    public reminderAssets : IReminderAssets = {assetKey:"", assetGuid:"", channelId: ""};

    /**
     * Constructor
     * @param {AlertClientService} alertClientService
     */
    constructor(private translate: TranslateService,
                private alertClientService: AlertClientService,
                public tileContextMenuOptionsService: TileContextMenuOptionsService)
    {}

    /**
     * show data used to populate the view
     */
    @Input()
    set tileData(tileData: ITile)
    {
        this._tileData = tileData;
        this.isShowSchedule = this.scheduleType === CarouselTypeConst.ENHANCED_SHOW_EDP;

        const bannerText:string  = _.get(tileData, "tileBanner.bannerText", "");
        const bannerClass:string = _.get(tileData, "tileBanner.bannerClass", "");
        this.isOnAir = (bannerClass === ON_AIR && bannerText.length > 0);

        this.setReminderAssets();
    }

    /**
     * sets reminder asset info from the tileData
     */
    public setReminderAssets() : void
    {
        if (this.tileData.tileAssetInfo.showGuid.length > 0)
        {
            this.reminderAssets.assetKey = "showGuid";
            this.reminderAssets.assetGuid = this.tileData.tileAssetInfo.showGuid;
        }
        this.reminderAssets.channelId = this.tileData.tileAssetInfo.channelId;
    }

    /**
     * Returns the  tile data
     * @returns {ITile}
     */
    get tileData(): ITile
    {
        return this._tileData;
    }
}
