import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { EdpScheduleItemComponent } from "./edp-schedule-item.component";
import {
    CarouselTypeConst,
    mockNormalizedTile
} from "sxmServices";
import { TranslationModule } from "../../../../translate/translation.module";
import { AlertClientService } from "../../../service/alert/alert.client.service";
import { AlertServiceMock } from "../../../../../../test/mocks";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import { TileContextMenuOptionsServiceMock } from "../../../../../../test/mocks/tile-context-menu-options.service.mock";
import { of as observableOf } from "rxjs";
import { MockTranslateService } from "../../../../../../test/mocks/translate.service";
import { TranslateService } from "@ngx-translate/core";
import {
    SHOW_GUID,
    ON_AIR
} from "./edp-schedule-item.consts";
import { MockComponent } from "../../../../../../test/mocks/component.mock";

describe("EdpScheduleItem", () =>
{
    let component: EdpScheduleItemComponent,
        fixture: ComponentFixture<EdpScheduleItemComponent>,
        alertClientService:AlertClientService,
        channelId = "channelId",
        assetKey = "showGuid",
        showGuid  = mockNormalizedTile.tileAssets.find( (asset) => asset.assetInfoKey === SHOW_GUID).assetInfoValue;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [
                EdpScheduleItemComponent,
                MockComponent({ selector: "reminder-button", inputs: ["reminderAssets", "viewType"]} )
            ],
            providers: [
                { provide: AlertClientService, useClass: AlertServiceMock },
                { provide: TileContextMenuOptionsService, useClass: TileContextMenuOptionsServiceMock },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(EdpScheduleItemComponent);
        component = fixture.componentInstance;
        alertClientService = TestBed.get(AlertClientService);
        alertClientService.createAlert = jasmine.createSpy("createAlert").and.returnValue(observableOf(true));
        alertClientService.removeAlert = jasmine.createSpy("removeAlert").and.returnValue(observableOf(true));

        component.tileData = mockNormalizedTile;
        component.scheduleType = CarouselTypeConst.ENHANCED_CHANNEL_EDP;

        fixture.detectChanges();
    });

    describe('component creation', () =>
    {
        it("should exists", () =>
        {
            expect(component).toBeTruthy();
        });
    });

    describe('setReminderAssets', () =>
    {
        it("should set reminder assets for the reminder button", () =>
        {
            component.setReminderAssets();
            expect(component.reminderAssets).toEqual(
                {
                    assetKey: assetKey,
                    assetGuid: showGuid,
                    channelId: channelId
                }
            );
        });
    });

    describe('when tile banner has onAir flag on', () =>
    {
        beforeEach(() =>
        {
            mockNormalizedTile.tileBanner.bannerClass = ON_AIR;
            mockNormalizedTile.tileBanner.bannerText  = "On Air";
            component.tileData = mockNormalizedTile;
        });

        it("isOnAir flag should be true", () =>
        {
            expect(component.isOnAir).toEqual(true);
        });
    });
});
