import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

import { CategoryTileComponent } from "./category-list-item.component";
import {TranslationModule} from "../../../../translate/translation.module";
import {SharedModule} from "../../../shared.module";

@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        SharedModule
    ],
    declarations: [
        CategoryTileComponent
    ],
    exports: [
        CategoryTileComponent
    ]
})
export class CategoryTileModule
{
}
