import {
    ChangeDetectionStrategy,
    Component,
    Input
} from "@angular/core";
import {ActivatedRoute}                from "@angular/router";
import {
    ITile,
    CarouselTypeConst
} from "sxmServices";
import {NeriticLinkService}            from "../../../../neritic-links/neritic-link.service";

@Component({
    selector        : "category-list-item",
    changeDetection : ChangeDetectionStrategy.OnPush,
    templateUrl     : "./category-list-item.component.html",
    styleUrls       : ["./category-list-item.component.scss"]
})

export class CategoryTileComponent
{
    /**
     * holds the index for the channel tile
     * @type {string}
     */
    @Input() index: string = "0";

    /**
     * filter text
     */
    @Input() filterText: string = "";

    @Input() line1Classes?: string[] = ['focusable', 'bold-font'];

    @Input() line2Classes?: string[] = [];

    @Input() line3Classes?: string[] = [];

    public backgroundStyle: any = null;

    public lazyLoadUrl: string = null;

    /**
     * Tile data used to populate the view
     */
    private _tileData: ITile; // backing store

    @Input() set tileData(tile: ITile)
    {
        this._tileData                 = tile;
        this.lazyLoadUrl = this.getLazyLoadUrl();
        this.backgroundStyle = this.getBackgroundStyle();
    }

    get tileData(): ITile
    {
        return this._tileData;
    }

    constructor(public neriticLinkService: NeriticLinkService,
                private route: ActivatedRoute) {}

    /**
     * Boolean flag to check if tile is Additional Channel
     */
    get isAdditionalChannel()
    {
        return this.tileData.tileContentSubType === CarouselTypeConst.ADDITIONAL_CHANNEL;
    }

    public trackTile(tile : ITile)
    {
        return tile.tileGuid;
    }

    /**
     * generates a lazyload background image url if the tile has a background image.
     * @returns {string}
     */

    public getLazyLoadUrl(): string
    {
        return this.tileData.bgImageUrl
               ? this.tileData.bgImageUrl
               : null;
    }

    /**
     * Generates a background gradient style if the tile has a background color
     * @returns {object}
     */

    public getBackgroundStyle(): object
    {
        const backgroundColor =  this.tileData.backgroundColor ? this.tileData.backgroundColor : null;

        return {
            'background-color': backgroundColor,
            'background-image': 'linear-gradient(to bottom, rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0))'
        };
    }

}
