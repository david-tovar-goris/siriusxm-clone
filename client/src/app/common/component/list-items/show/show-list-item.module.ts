import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ContextMenuModule } from "../../../../context-menu/context-menu.module";
import { FavoritesModule } from "../../../../favorites/favorites.module";
import { TranslationModule } from "../../../../translate/translation.module";
import { ShowTileComponent } from "./show-list-item.component";
import { SharedModule } from "../../../shared.module";

@NgModule({
    imports: [
        CommonModule,
        ContextMenuModule,
        FavoritesModule,
        TranslationModule,
        SharedModule
    ],
    declarations: [
        ShowTileComponent
    ],
    exports: [
        ShowTileComponent
    ]
})
export class ShowTileModule
{
}
