import {
    Component,
    Input,
    OnInit
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import {
    ChannelLineupService,
    IChannel, IFavoriteAsset,
    IOnDemandShow,
    CarouselTypeConst,
    ITile,
    neriticActionConstants,
    MediaPlayerService
} from "sxmServices";
import { appRouteConstants } from "../../../../app.route.constants";
import { TranslateService } from "@ngx-translate/core";
import { NavigationService } from "../../../service/navigation.service";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import { FavoriteButtonConstant } from "../../../../favorites/button/favorite-button.constant";
import {
    NowPlayingIndicatorService,
    tileContentType
} from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { CarouselService } from "../../../../carousel/carousel.service";
import { NeriticLinkService } from "../../../../neritic-links/neritic-link.service";
import { CMOption } from "../../../../context-menu/context-menu.interface";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { TileUtil } from "../../../util/tile.util";

@Component({
    selector   : "show-list-item",
    templateUrl: "./show-list-item.component.html",
    styleUrls  : [ "./show-list-item.component.scss" ]
})

export class ShowTileComponent implements OnInit
{
    /**
     * holds the index for the channel tile
     * @type {string}
     */
    @Input() index: string = '0';

    /**
     * filter text
     */
    @Input() filterText?: string = '';

    /**
     * Stores whether to display audio or video type
     * @type {string}
     */
    @Input() episodeType?: string = appRouteConstants.ON_DEMAND.AUDIO;


    @Input() showContextMenu: boolean = true;

    public backgroundStyle: any = null;

    public lazyLoadUrl: string = null;

    /**
     * Backed up tileData.
     */
    private _tileData: ITile;


    private getCMOptionsFunc: () => CMOption[];

    /**
     * show data used to populate the view
     */
    @Input()
    set tileData(tileData: ITile)
    {
        this._tileData             = tileData;
        this.channelId             = tileData.tileAssetInfo.channelId;
        this.showGuid              = tileData.tileAssetInfo.showGuid;
        this.selectedChannel       = this.channelLineupService.findChannelById(this.channelId);
        this.isAOD                 = this.episodeType === appRouteConstants.ON_DEMAND.AUDIO;
        this.favoriteAssetInfo     =
            {
                title         : tileData.line2,
                channelId     : this.channelId,
                assetGuid     : this.showGuid,
                contentType   : CarouselTypeConst.SHOW_TILE,
                subContentType: CarouselTypeConst.SHOW_TILE,
                isIrisPodcast : tileData.tileAssetInfo.isPandoraPodcast
            };

        this.getCMOptionsFunc = function()
        {
            return this.tileData ? this.tileContextMenuOptionsService.getCMOptions(this.tileData) : [];
        }.bind(this);

        this.lazyLoadUrl = this.getLazyLoadUrl();
        this.backgroundStyle = this.getBackgroundStyle();
    }

    /**
     * Returns the  tile data
     * @returns {IOnDemandShow}
     */
    get tileData(): ITile
    {
        return this._tileData;
    }

     /**
     * Holds the flag dictates the channel number to be displayed or not
     * @type {boolean}
     */
    displayChannelNumber: boolean = false;

    /**
     * Holds is aod or not
     */
    isAOD: boolean;

    /**
     * Stores Show guid
     */
    public showGuid: string;

    /**
     * Stores channel id
     */
    public channelId: string;

    /**
     * Holds the value for channel related to the selected show.
     */
    public selectedChannel: IChannel ;

    /**
     * Holds the favorite asset info
     */
    public favoriteAssetInfo: IFavoriteAsset;

    /**
     * List View constant
     * @type {string}
     */
    public listView = FavoriteButtonConstant.LIST_VIEW;

    constructor(private channelLineupService: ChannelLineupService,
                private route: ActivatedRoute,
                private translate: TranslateService,
                public tileContextMenuOptionsService: TileContextMenuOptionsService,
                private navigationService: NavigationService,
                private nowPlayingIndicatorService: NowPlayingIndicatorService,
                public carouselService: CarouselService,
                private neriticLinkService: NeriticLinkService,
                private mediaPlayerService : MediaPlayerService)
    {}

    ngOnInit()
    {
        this.selectedChannel = {} as IChannel;
        // If the page is "Show Reminder Details" or "Manage Show Reminders" -- display "Airs on channelNumber " text on the UI
        if (this.route.snapshot.url[ 0 ].path.includes(appRouteConstants.MANAGE_SHOW_REMINDERS) ||
            this.route.snapshot.url[ 0 ].path.includes(appRouteConstants.SHOW_REMINDER_DETAILS))
        {
            this.displayChannelNumber = true;
        }
    }

    /**
     * Navigates to enhanced show edp page
     */
    public selectShow (): void
    {
       const showDetailsAction = this._tileData.neriticLinkData
                                      .find((neriticLinkData) =>
                                          neriticLinkData.functionalGroup === neriticActionConstants.FUNCTIONAL_GROUPS.SHOW_DETAILS);
       this.neriticLinkService.takeSecondaryTileAction(this._tileData, showDetailsAction);
    }

    /**
     * generates a lazyload background image url if the tile has a background image.
     * @returns {string}
     */

    public getLazyLoadUrl(): string
    {
        return this._tileData.bgImageUrl
               ? this._tileData.bgImageUrl + '?width=320&height=240&preserveAspect=true'
               : null;
    }

    /**
     * Generates a background gradient style if the tile has a background color
     * @returns {object}
     */

    public getBackgroundStyle(): object
    {
        const backgroundColor = this._tileData.backgroundColor
                                ? this._tileData.backgroundColor
                                : null;

        return !this._tileData.fgImageUrl ? TileUtil.getDefaultBackground(this._tileData, backgroundColor)
                : {
                     'background-color': backgroundColor,
                     'background-image': 'linear-gradient(to bottom, rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0))'
                  };
    }

    /**
     * generates a lazyload foreground image url if the tile has a foreground image.
     * @returns {string}
     */

    public getFgImgLazyLoadUrl(url): string
    {
        return TileUtil.normalizeImageUrl(url, 'width=320&height=240&preserveAspect=true');
    }

    /**
     * Set default background color for tile if image fails to load
     */
    public onImageStateChange(event: any)
    {
        if(event.reason == "loading-failed" && this._tileData.tileAssetInfo.isPandoraPodcast)
        {
            this.backgroundStyle = TileUtil.getDefaultBackground(this._tileData);
        }
    }
}
