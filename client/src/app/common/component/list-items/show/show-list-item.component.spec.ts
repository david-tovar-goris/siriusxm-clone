import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { ActivatedRoute } from "@angular/router";
import { ShowTileComponent } from "./show-list-item.component";
import {
    ChannelListServiceMock
} from "../../../../../../test/mocks/channel-list.service.mock";
import { TranslationModule } from "../../../../translate/translation.module";
import { MockComponent } from "../../../../../../test/mocks/component.mock";
import {
    ChannelLineupService, ITile, MediaPlayerService
} from "sxmServices";
import { ChannelLineupServiceMock } from "../../../../../../test/mocks/channel-lineup.service.mock";
import { SharedModule } from "../../../shared.module";
import { ChannelListStoreService } from "../../../service/channel-list.store.service";
import { NavigationService } from "../../../service/navigation.service";
import { navigationServiceMock } from "../../../../../../test/mocks/navigation.service.mock";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import { TileContextMenuOptionsServiceMock } from "../../../../../../test/mocks/tile-context-menu-options.service.mock";
import { NowPlayingIndicatorService } from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { NowPlayingIndicatorServiceMock } from "../../../../../../test/mocks/now-playing-indicator.service.mock";
import { CarouselService } from "../../../../carousel/carousel.service";
import { CarouselServiceMock } from "../../../../../../test/mocks";
import { NeriticLinkService } from "../../../../neritic-links/neritic-link.service";
import { NeriticLinkServiceMock } from "../../../../../../test/mocks/neritic-link.service.mock";
import { tileMock } from "../../../../../../test/mocks/data/tile.mock";
import { MediaPlayerServiceMock } from "../../../../../../test/mocks/media-player.service.mock";

describe("ShowListItemComponent", () =>
{
    let component: ShowTileComponent,
        fixture: ComponentFixture<ShowTileComponent>;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports     : [
                TranslationModule,
                SharedModule
            ],
            declarations: [
                ShowTileComponent,
                MockComponent({ selector: "favorite-btn", inputs: [ "asset", "color", "viewType", "listAsset" ] }),
                MockComponent({ selector: "context-menu", inputs: [ "getOptionsFunc", "ariaText" ] })
            ],
            providers   : [
                { provide: ActivatedRoute, useValue: { snapshot: { url: [ { path: "manage-show-reminders" } ] } } },
                { provide: ChannelLineupService, useClass: ChannelLineupServiceMock },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: TileContextMenuOptionsService, useClass: TileContextMenuOptionsServiceMock },
                { provide: NavigationService, useValue: navigationServiceMock },
                { provide: NowPlayingIndicatorService, useClass: NowPlayingIndicatorServiceMock },
                { provide: CarouselService, useClass: CarouselServiceMock },
                { provide: NeriticLinkService, useClass: NeriticLinkServiceMock},
                { provide: MediaPlayerService, useClass: MediaPlayerServiceMock}
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture   = TestBed.createComponent(ShowTileComponent);
        component = fixture.componentInstance;

        component.tileData = {
            primaryNeriticLink: { contentType: "", channelId: '' },
            tileAssetInfo: { showGuid: '' },
            neriticLinkData: [{ contentSubType: 'aod' }],
            fgImageUrl: "url",
            bgImageUrl: "url"
        } as ITile;

        fixture.detectChanges();
    });

    describe("ngOnInit()", () =>
    {
        describe("displayChannelNumber property", () =>
        {
            describe("when the route is for 'manage-show-reminders'", () =>
            {
                it("should display the channel number for the show", () =>
                {
                    expect(component.displayChannelNumber).toEqual(true);
                });
            });
        });

        describe("isAOD property", () =>
        {
            describe("when the episode type is audio", () =>
            {
                it("should be set to true", () =>
                {
                    expect(component.isAOD).toEqual(true);
                });
            });

            describe("when the episode type is not audio", () =>
            {
                it("should be set to false", () =>
                {
                    fixture.destroy();
                    fixture   = TestBed.createComponent(ShowTileComponent);
                    component = fixture.componentInstance;

                    component.episodeType = 'video';
                    component.tileData = tileMock;
                    fixture.detectChanges();

                    expect(component.isAOD).toEqual(false);
                });
            });
        });
    });
});
