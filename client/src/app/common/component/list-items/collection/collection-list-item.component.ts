import {
    Component,
    Input
} from "@angular/core";
import {
    ITile
} from "sxmServices";
import { CarouselService } from "../../../../carousel/carousel.service";

@Component({
    selector   : "collection-list-item",
    templateUrl: "./collection-list-item.component.html",
    styleUrls  : [ "./collection-list-item.component.scss" ]
})

export class CollectionListItemComponent
{
    public backgroundStyle: any = null;

    public lazyLoadUrl: string = null;

    /**
     * Tile data used to populate the view
     */
    private _tileData: ITile; // backing store

    get tileData(): ITile
    {
        return this._tileData;
    }

    @Input() set tileData(tile: ITile)
    {
        this._tileData                 = tile;
        this.lazyLoadUrl = this.getLazyLoadUrl();
        this.backgroundStyle = this.getBackgroundStyle();
    }

    constructor(public carouselService: CarouselService)
    {}

    /**
     * generates a lazyload background image url if the tile has a background image.
     * @returns {string}
     */

    public getLazyLoadUrl(): string
    {
        return this.tileData.bgImageUrl
               ? this.tileData.bgImageUrl + '?width=320&height=240&preserveAspect=true'
               : null;
    }

    /**
     * Generates a background gradient style if the tile has a background color
     * @returns {object}
     */

    public getBackgroundStyle(): object
    {
        const tileBgColor =  this.tileData.backgroundColor ? this.tileData.backgroundColor : null;
        return {
            'background-color': tileBgColor,
            'background-image': 'linear-gradient(to bottom, rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0))'
        };
    }
}
