import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { CollectionListItemComponent } from "./collection-list-item.component";
import { SharedModule } from "../../../shared.module";
import { CarouselService } from "../../../../carousel/carousel.service";
import { CarouselServiceMock } from "../../../../../../test/mocks/carousel.service.mock";
import { CommonModule } from "@angular/common";

describe("CollectionListItem", () =>
{
    let component: CollectionListItemComponent,
        fixture: ComponentFixture<CollectionListItemComponent>;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                SharedModule
            ],
            declarations: [
                CollectionListItemComponent
            ],
            providers: [ { provide: CarouselService, useValue: CarouselServiceMock }]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(CollectionListItemComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
});
