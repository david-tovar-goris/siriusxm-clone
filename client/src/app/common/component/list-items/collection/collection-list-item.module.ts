import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { SharedModule } from "../../../shared.module";
import { CollectionListItemComponent } from "./collection-list-item.component";

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [
        CollectionListItemComponent
    ],
    exports: [
        CollectionListItemComponent
    ]
})
export class CollectionTileModule
{
}
