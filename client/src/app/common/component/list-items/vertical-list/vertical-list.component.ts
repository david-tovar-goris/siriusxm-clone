import {ChangeDetectionStrategy, Component, Input, OnInit} from "@angular/core";
import {ICarouselData, ITile, Logger} from "sxmServices";
import {CarouselService} from "../../../../carousel/carousel.service";

import { Subject , BehaviorSubject, fromEvent } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { AutoUnSubscribe } from "../../../decorator/auto-unsubscribe";
import {NavigationService} from "../../../service/navigation.service";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsFindingMethods
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    selector   : "vertical-list",
    templateUrl: "./vertical-list.component.html",
    styleUrls  : [ "./vertical-list.component.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})

@AutoUnSubscribe()
export class VerticalListComponent implements OnInit
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;
    @Input() pageTitleAna?: string = '';

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("VerticalListComponent");


    @Input() showHeader: boolean = true;

    /**
     * Determines whether the header should have goback or not as few verticle list's doesn't qualify for goback
     */
    @Input() enableBackButton: boolean = false;

    /**
     * sets the local properties
     * @param setCarouselData
     */
    @Input()
    set carouselData(setCarouselData: ICarouselData)
    {
        this.title = setCarouselData && setCarouselData.title && setCarouselData.title.textValue ? setCarouselData.title.textValue : "";
        this._carouselData = setCarouselData;
        this.loadMore(20, this._carouselData.tiles);
    }

    /**
     * returns the carousel data
     */
    get carouselData() : ICarouselData { return this._carouselData; }

    /**
     * To provide the title to the template
     */
    public title: string = "";

    /**
     * stroes the carousel data.
     */
    private _carouselData: ICarouselData;


    private clampSubject$: Subject<any> = new Subject<any>();


    private tileIterable$: BehaviorSubject<ITile[]> = new BehaviorSubject([]);

    public subscriptions: any = [];

    /**
     * The number of tiles that have been loaded.
     */
    public numberLoaded: number = 0;


    /**
     * Constructor
     * @param {CarouselService} carouselService
     */
    constructor(public carouselService: CarouselService,
                private navigationService: NavigationService)
    {}

    /**
     * ngOnInit
     */
    ngOnInit(): void
    {
        VerticalListComponent.logger.debug("ngOnInit()");

        this.subscriptions.push(
            fromEvent(window, 'resize')
                .pipe(debounceTime(500))
                .subscribe(event =>
                {
                    this.clampSubject$.next(event);
                }),
            fromEvent(window, 'scroll')
                .pipe(debounceTime(250))
                .subscribe(() =>
                {
                    if(this.isScrolledToBottom())
                    {
                        this.loadMore(20, this._carouselData.tiles);
                    }
                })
        );
    }

    /**
     * Adding trackBy function for *ngFor to increase performance
     */
    public trackByFn(index, item)
    {
        if (item.tileAssetInfo
            && item.tileAssetInfo.episodeGuid)
        {
            return item.tileAssetInfo.episodeGuid;
        }
        return index;
    }

    /**
     * take the user to previous screen if this verticle list is valid to have back button
     */
    public goBack()
    {
        if (this.enableBackButton)
        {
            this.navigationService.goBack();
        }
    }


    public line1ClassesForTile(tile: ITile)
    {
        return this.carouselService.isSeededRadioTile(tile)
            ? ['focusable', 'bold-font']
            : [];
    }


    public line2ClassesForTile(tile: ITile)
    {
        return this.carouselService.isSeededRadioTile(tile)
            ? []
            : ['focusable', 'bold-font'];
    }

    /**
     * Returns boolean if scrolled down the page a ways.
     */
    public isScrolledToBottom()
    {
        const clientHeight = document.documentElement.clientHeight;
        const scrollTop = getScrollTop();
        const scrollHeight = document.documentElement.scrollHeight;

        return scrollTop + clientHeight >= scrollHeight - 100;

        function getScrollTop()
        {
            const el = document.scrollingElement || document.documentElement;
            return el.scrollTop;
        }
    }

    /**
     * Loads number of tiles in addition to the tiles already loaded.
     */
    public loadMore(numberToLoad, tiles)
    {
        if (this.numberLoaded === tiles.length) return;

        let newTotal = this.numberLoaded + numberToLoad;
        if (newTotal > tiles.length)
        {
            newTotal = tiles.length;
        }

        this.tileIterable$.next(
            tiles.slice(
                0,
                newTotal
            )
        );
        this.numberLoaded = newTotal;
    }
}
