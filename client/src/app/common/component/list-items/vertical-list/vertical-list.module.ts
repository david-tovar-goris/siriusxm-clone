import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ShowTileModule } from "../show/show-list-item.module";
import { VerticalListComponent } from "./vertical-list.component";
import { ChannelTileModule } from "../channel/channel-list-item.module";
import { EpisodeTileModule } from "../episode/episode-list-item.module";
import { CategoryTileModule } from "../category/category-list-item.module";
import { SharedModule } from '../../../shared.module';
import { CarouselService } from '../../../../carousel/carousel.service';
import { ChannelListStoreService } from '../../../service/channel-list.store.service';
import { CollectionTileModule } from "../collection/collection-list-item.module";

@NgModule({
    imports     : [
        CommonModule,
        ChannelTileModule,
        EpisodeTileModule,
        CategoryTileModule,
        ShowTileModule,
        CollectionTileModule,
        SharedModule
    ],
    declarations: [
        VerticalListComponent
    ],
    providers: [
        CarouselService,
        ChannelListStoreService
    ],
    exports     : [
        VerticalListComponent
    ]
})

export class VerticalListModule
{
}

