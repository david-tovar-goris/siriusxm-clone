import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ElementRef,
    HostListener,
    Input,
    OnInit,
    ViewChild,
    OnChanges
} from '@angular/core';
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths,
    AnalyticsEDPNames
} from "app/analytics/sxm-analytics-tag.constants";
import { TranslateService } from "@ngx-translate/core";
import { CarouselTypeConst } from "sxmServices";

@Component({
  selector: 'app-line-clamp',
  templateUrl: './line-clamp.component.html',
  styleUrls: ['./line-clamp.component.scss']
})
export class LineClampComponent implements OnInit, AfterViewInit, OnChanges
{

    @Input() showDescriptionCopy: any;
    @Input() carousel;
    @Input() anaTileAsset;
    @ViewChild('showDescription') showDescription: ElementRef;
    @ViewChild('showCopy') showCopy: ElementRef;

    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public carouselTypeAnaConst = CarouselTypeConst;
    public AnalyticsEDPNames = AnalyticsEDPNames;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;

    constructor(private translateService: TranslateService,
              private changeDetectorRef: ChangeDetectorRef)
    {
        this.buttonLabels = {
            more: this.translateService.instant('enhancedEdp.readMore'),
            less: this.translateService.instant('enhancedEdp.readLess')
        };
    }

    public buttonLabels: any;
    public isTruncated: boolean;
    public showMoreLessButton: boolean;
    public moreLessButtonLabel: string;
    public copyHeight: number;
    public containerHeight: number;
    public initialHeight: number;
    public shouldTruncate: boolean;
    public showLink: boolean;

    @HostListener('window:resize', ['$event'])
    onResize()
    {
        this.showLink = this.copyHeight > this.initialHeight;
        this.ngAfterViewInit();
    }

    getShouldTruncate(): boolean
    {
        this.copyHeight = this.showCopy.nativeElement.clientHeight;
        this.containerHeight = this.initialHeight;
        this.shouldTruncate = this.copyHeight > this.containerHeight;
        return this.shouldTruncate;
    }

    toggleMoreLess(): void
    {
        if (this.shouldTruncate)
        {
            this.shouldTruncate = false;
            this.showDescription.nativeElement.classList.add('show-description-less');
            this.showDescription.nativeElement.classList.remove('show-description-more');
            this.moreLessButtonLabel = this.buttonLabels.more;
        }
        else
        {
            this.shouldTruncate = true;
            this.showDescription.nativeElement.classList.add('show-description-more');
            this.showDescription.nativeElement.classList.remove('show-description-less');
            this.moreLessButtonLabel = this.buttonLabels.less;
        }
    }

    ngOnInit(): void
    {
        this.moreLessButtonLabel = this.buttonLabels.more;
        this.isTruncated = true;
        this.showMoreLessButton = true;
        this.containerHeight = 100;
        this.initialHeight = 100;
    }

    ngAfterViewInit(): void
    {
        this.copyHeight = this.showCopy.nativeElement.clientHeight;
        this.containerHeight = this.showDescription.nativeElement.clientHeight;
        this.shouldTruncate = this.getShouldTruncate();
        this.showLink = this.copyHeight > this.initialHeight;
        this.changeDetectorRef.detectChanges();
        this.toggleMoreLess();
    }

    ngOnChanges(): void
    {
        this.changeDetectorRef.detectChanges();
        this.onResize();
    }
}
