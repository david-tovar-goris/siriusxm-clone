export interface OverlayData
{
    header: string;
    description: string;
    buttonOneText?: string;
    buttonTwoText?: string;
    buttonOneActionType?: string;
    buttonOneTarget?: string;
    buttonOneLink?: string;
    errorCode?: string;
    screen: string;
    imagePath?: string;
    class?: string;
}

export interface OverlayStlyes{
    height: string;
    width: string;
    bgColor?: string;
}

export interface OverlayConfig{
    overlayData: OverlayData;
    overlayStyles?: OverlayStlyes;
}

export interface OverlayResult
{
    status: string;
    data?: any;
}

export interface OverlayInformation
{
    open: boolean;
    config: OverlayConfig;
}
