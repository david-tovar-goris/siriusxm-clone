import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { NavigationModule } from "../../../navigation/navigation.module";
import { OverlayComponent } from "./overlay.component";
import { SharedModule } from '../../shared.module';
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        NavigationModule,
        SharedModule,
        TranslateModule
    ],
    declarations: [
        OverlayComponent
    ],
    exports:[
        OverlayComponent
    ]
})

export class OverlayModule
{
}
