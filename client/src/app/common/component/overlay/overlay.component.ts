import {AfterViewChecked, Component, ElementRef, OnInit, Renderer2, ViewChild} from "@angular/core";
import { OverlayService } from "../../service/overlay/overlay.service";
import { Subscription } from "rxjs";
import { AutoUnSubscribe } from "../../decorator";
import { OverlayData, OverlayInformation } from "./overlay.interface";
import { FlepzScreenService } from "../../service/flepz/flepz-screen.service";
import { TranslateService } from "@ngx-translate/core";
import { FocusUtil } from "../../util/focus.util";
import {
    AnalyticsElementTypes,
    AnalyticsErrorNumbers,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsTagTextConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";
import { SxmAnalyticsService } from "sxmServices";

/**
 * @MODULE:     client
 * @CREATED:    07/11/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *   OverlayComponent used to show overlay on different screens
 */

@Component({
    selector: "app-overlay",
    templateUrl: "./overlay.component.html",
    styleUrls: [ "./overlay.component.scss" ]

})

@AutoUnSubscribe([])
export class OverlayComponent
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsErrorNumbers = AnalyticsErrorNumbers;
    public overlayNameAnaProp;

    private _overlaycontainer: ElementRef;
    @ViewChild('overlaycontainer') set content(overlaycontainer : ElementRef)
    {
        this._overlaycontainer = overlaycontainer;
        if(this.overlayInformation.open)
        {
            FocusUtil.setupAccessibleDialog(this._overlaycontainer.nativeElement);
        }
    }

    @ViewChild('overlayBody') overlayBody;

    /**
     *  Holds the overlay information of open/close and data(header/Desc).
     */
    public overlayInformation: OverlayInformation;

    /**
     * Stores subscriptions to unsubscribe when component is destroyed
     *
     * @type {Subscription}
     * @memberof OverlayComponent
     */
    public subscription: Subscription;

    public overlayStyles = {
        height: '40vh',
        width: '50%',
        bgColor:"rgba(9, 17, 29, 0.95)"
    };

    /**
     * Subscribes to the overlayService.enableOverlay observable.
     * Subscribes to the overlayService.activeOverlay observable.
     * @param {OverlayService} overlayService
     * @param {FlepzScreenService} flepzScreenService
     */
    constructor(private overlayService: OverlayService,
                private flepzScreenService: FlepzScreenService,
                public renderer: Renderer2,
                public translate: TranslateService,
                private sxmAnalyticsService: SxmAnalyticsService)
    {

        this.subscription = this.overlayService.overlayInformation.subscribe((val: OverlayInformation) =>
        {
            if(this._overlaycontainer && !val.open)
            {
                FocusUtil.closeFocusedDialog();
            }
            if(val.config && val.config.overlayData && val.open)
            {
                const overlayStyles = val.config.overlayStyles || this.overlayStyles;
                setTimeout(()=>
                {
                    this.renderer.setStyle(this.overlayBody.nativeElement, 'height',  overlayStyles.height);
                    this.renderer.setStyle(this.overlayBody.nativeElement, 'width',  overlayStyles.width);
                    this.renderer.setStyle(this.overlayBody.nativeElement, 'visibility',  'visible');

                    if(overlayStyles.bgColor)
                    {
                        this.renderer.setStyle(this._overlaycontainer.nativeElement, 'background-color',  overlayStyles.bgColor);

                    }
                },0);

                /**
                 * For Analytics purpose
                 */
                this.overlayNameAnaProp = val.config.overlayData.screen;
                const errorNumber = val.config.overlayData.errorCode ? parseInt(val.config.overlayData.errorCode)
                                                                     : AnalyticsErrorNumbers.DEFAULT_ERROR_MSG_NUMBER;
                this.sxmAnalyticsService.logAnalyticsTag({
                        name: AnalyticsTagNameConstants.ERROR_MESSAGE,
                        userPath: AnalyticsUserPaths.ERROR_MESSAGE + '_' + errorNumber.toString(),
                        messageType: val.config.overlayData.header,
                        errorNumber: errorNumber,
                        skipNpData: true,
                        skipOrientation: true,
                        skipActionSource: true,
                        skipContentSource: true,
                        skipCurrentPosition: true
                });
            }
            this.overlayInformation = val;

        });
    }


    /**
     * when user closes the overlay
     */
    close(): void
    {
        this.overlayService.close();
    }

    /**
     * when user clicks ok/wants to move forward
     */
    ok(): void
    {
        const data = {
            status: "ok",
            data: null
        };

        this.overlayService.close(data);
    }

    /**
     * Uses the type to determine the button action
     * @param modalData
     */
    public buttonOneAction({ buttonOneActionType }: OverlayData): void
    {
        switch (buttonOneActionType)
        {
            case "forgotUsernamePassword":
                this.close();
                this.openForgotUsernamePasswordPage();
                break;
            default:
                this.close();
        }
    }

    /**
     * Opens the Forgot Username or Password page
     */
    private openForgotUsernamePasswordPage(): void
    {
        this.flepzScreenService.enableIframe("forgotUsernamePassword");
    }

}
