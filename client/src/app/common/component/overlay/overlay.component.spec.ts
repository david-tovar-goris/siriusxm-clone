import {
    async,
    ComponentFixture, fakeAsync,
    TestBed, tick
} from '@angular/core/testing';
import { OverlayComponent } from './overlay.component';
import { OverlayService } from "../../service/overlay/overlay.service";
import { OverlayServiceMock } from "../../../../../test/mocks/overlay.service.mock";
import { By } from "@angular/platform-browser";
import { DebugElement } from '@angular/core';
import { FlepzScreenService } from "../../service/flepz/flepz-screen.service";
import { SharedModule } from '../../shared.module';
import { ChannelListStoreService } from '../../service/channel-list.store.service';
import { ChannelLineupService, SxmAnalyticsService } from 'sxmServices';
import { channelLineupServiceMock, RouterStub } from "../../../../../test/index";
import { ChannelListServiceMock } from "../../../../../test/mocks/index";
import { TranslateModule } from "@ngx-translate/core";
import { FocusUtil } from "../../util/focus.util";
import { Store } from "@ngrx/store";
import { Router } from "@angular/router";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";

describe('OverlayComponent', () =>
{
    let component: OverlayComponent;
    let fixture: ComponentFixture<OverlayComponent>;
    let debugElement: DebugElement;

    function SXMAnalyticsMockConstructor()
    {
        this.updateNowPlayingData = function() {};
        this.logAnalyticsTag = function(tag) {};
    }

    let sxmAnalyticsServiceMock = new SXMAnalyticsMockConstructor();

    beforeEach(async(() =>
    {

        this.store = {
            select: () => new BehaviorSubject(null)
        };

        TestBed.configureTestingModule({
            imports: [
                SharedModule,
                TranslateModule.forRoot()
            ],
            declarations: [
                OverlayComponent
            ],
            providers: [
                { provide: OverlayService, useValue: OverlayServiceMock.getSpy() },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: ChannelLineupService, useValue: channelLineupServiceMock },
                FlepzScreenService,
                { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                { provide: Store, useValue: this.store},
                { provide: Router, useClass: RouterStub}
            ]
        }).compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(OverlayComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        component.overlayInformation.open = false;
        fixture.detectChanges();
    });

    describe('Overlay Execution', () =>
    {
        beforeEach(() =>
        {
            spyOn(FocusUtil,"setupAccessibleDialog");
            spyOn(FocusUtil,"closeFocusedDialog");
        });

        it('opens the Overlay', async(() =>
        {
            expect(component.overlayInformation.open).toEqual(false);
            OverlayServiceMock.open({});
            fixture.detectChanges();
            expect(component.overlayInformation.open).toEqual(true);
        }));

        it('closes the Overlay on click of close(X) icon', () =>
        {
            OverlayServiceMock.open({});
            expect(component.overlayInformation.open).toEqual(true);
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.overlay-close'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "OvlyClose"
                    }
                });
            fixture.detectChanges();
            expect(OverlayServiceMock.getSpy().close).toHaveBeenCalled();
        });
    });

    describe("Overlay Analytics", () =>
    {
        beforeEach(() =>
        {
            spyOn(FocusUtil,"setupAccessibleDialog");
            spyOn(FocusUtil,"closeFocusedDialog");
        });

        it ("should log the close button click.", () =>
        {
            OverlayServiceMock.open({});
            expect(component.overlayInformation.open).toEqual(true);
            spyOn(sxmAnalyticsServiceMock, "logAnalyticsTag");
            fixture.detectChanges();
            fixture.debugElement.query(By.css('.overlay-close'))
                .triggerEventHandler('click', {
                    target: {
                        tagName: "OvlyClose"
                    }
                });
            fixture.detectChanges();
            expect(OverlayServiceMock.getSpy().close).toHaveBeenCalled();
            expect(sxmAnalyticsServiceMock.logAnalyticsTag).toHaveBeenCalled();
        });
    });
});
