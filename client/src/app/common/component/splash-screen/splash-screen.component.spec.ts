import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";

import { DynamicSplashScreenComponent } from "./splash-screen.component";
import { TranslationModule } from "../../../translate/translation.module";

describe("DynamicSplashScreenComponent", () =>
{
    let component: DynamicSplashScreenComponent;
    let fixture: ComponentFixture<DynamicSplashScreenComponent>;
    let router = { navigate: jasmine.createSpy('navigate') };


    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                TranslationModule
            ],
            declarations: [
                DynamicSplashScreenComponent
            ]
        })
        .compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(DynamicSplashScreenComponent);
        component = fixture.componentInstance;

    });

    describe("Infrastructure", () =>
    {
        it("should create the Splash Screen Component", () =>
        {
            expect(component).toBeTruthy();
        });
    });
});

