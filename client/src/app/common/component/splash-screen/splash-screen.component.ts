import {
    Component, Input
} from "@angular/core";

import { ContextualConstants } from "sxmServices";
@Component({
    selector: "dynamic-splash-screen",
    templateUrl: "splash-screen.component.html",
    styleUrls: [ "./splash-screen.component.scss" ]
})

export class DynamicSplashScreenComponent {}
