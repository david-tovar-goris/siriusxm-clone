import { Component } from "@angular/core";
import { Observable } from "rxjs";

import {
    getIsVideo,
    getIsLiveVideoAvailable
} from "../../store/now-playing.store";

import { IAppStore } from "../../store/app.store";
import { MediaTimestampService } from "sxmServices";
import { Store } from "@ngrx/store";
import { TuneClientService } from "../../service/tune/tune.client.service";
import { UpdateLastPlayhead } from "../../action/now-playing.action";

@Component({
    selector: "switch-playback-button",
    templateUrl: "./switch-playback-button.component.html",
    styleUrls: [ "./switch-playback-button.component.scss" ]
})
export class SwitchPlaybackButton
{
    /**
     * Indicates if there's live video available for the current stream.
     */
    public isLiveVideoAvailable$: Observable<boolean> = this.store.select(getIsLiveVideoAvailable);

    /**
     * Indicates if video (live or on demand) is the current content type.
     */
    public isVideo$: Observable<boolean> = this.store.select(getIsVideo);

    /**
     * Constructor.
     */
    constructor(private mediaTimestampService: MediaTimestampService,
                private store: Store<IAppStore>,
                private tuneClientService: TuneClientService)
    {
    }

    /**
     * Toggles between live audio and live video media playback.
     */
    public toggleLivePlayback()
    {
        this.store.dispatch(new UpdateLastPlayhead(this.mediaTimestampService.playheadTimestamp));
        this.tuneClientService.switchPlayback();
    }
}
