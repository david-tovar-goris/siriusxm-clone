import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SwitchPlaybackButton } from "./switch-playback-button.component";
import { TranslationModule } from "../../../translate/translation.module";

export const COMPONENTS = [
    SwitchPlaybackButton
];

export const SERVICES = [

];

@NgModule({
    imports: [
        CommonModule,
        TranslationModule
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
    providers: SERVICES
})

export class SwitchPlaybackModule
{
}
