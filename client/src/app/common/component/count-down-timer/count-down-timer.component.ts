import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {interval, Subscription} from "rxjs";
import * as moment from "moment";

@Component({
  selector: 'app-count-down-timer',
  templateUrl: './count-down-timer.component.html',
  encapsulation: ViewEncapsulation.None
})
export class CountDownTimerComponent implements OnInit
{
    public endTime: any;

    @Input() set previewEndTime (previewEndTime: string)
    {
        this.endTime = moment(new Date(previewEndTime));
    }
    @Output() freePreviewEnded: EventEmitter<boolean> = new EventEmitter();
    private subscription: Subscription;
    public timeDifference: string;
    public zeroTimer: string = '00:00:00';
    @Output() onTimeDifferenceForKochava = new EventEmitter<{timeDifference: string}>();

    constructor() { }

    private getTimeDifference()
    {
        const duration = moment.duration(this.endTime.diff(moment()));
        this.timeDifference = moment.utc(duration.as('milliseconds')).format('HH:mm:ss');
        if (this.timeDifference === this.zeroTimer)
        {
            this.freePreviewEnded.emit(true);
            this.subscription.unsubscribe();
        }
        this.onTimeDifferenceForKochava.emit({timeDifference: this.timeDifference});
    }

    ngOnInit()
    {
        this.subscription = interval(1000)
            .subscribe(x => { this.getTimeDifference(); });
    }

    ngOnDestroy()
    {
        this.subscription.unsubscribe();
    }
}
