import {
    Component, Input
} from "@angular/core";

@Component({
    selector: "dynamic-loading-screen",
    templateUrl: "loading-screen.component.html",
    styleUrls: [ "./loading-screen.component.scss" ]
})

export class DynamicLoadingScreenComponent
{
    /**
     *
     * screenOpen - Opens/Closes the Loading screen @type {boolean}
     */
    @Input() screenOpen: boolean;

    constructor(){}
}

