import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { NavigationModule } from "../../../navigation/navigation.module";
import { ToastComponent } from "./toast.component";
import { SharedModule } from "../../shared.module";
import { TranslationModule } from "../../../translate/translation.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        NavigationModule,
        SharedModule,
        TranslationModule
    ],
    declarations: [
        ToastComponent
    ],
    exports:[
        ToastComponent
    ]
})

export class ToastModule
{
}
