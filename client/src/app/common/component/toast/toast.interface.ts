export interface ToastData
{
    /**
     * Path in the i18n JSON
     */
    messagePath: string;
    customMessage?: string;
    buttonCTAText?: string;
    isAltColor?: boolean;
    closeToastTime?: number;
    hasCloseButton?: boolean;
    /**
     * Provides a context by optionally allowing
     * the caller of .open to indicate where it was fired from.
     */
    screen?: string;
    errorType?: any;
}

export interface ToastInformation
{
    isOpen: boolean;
    data: ToastData;
}
