import {
    async,
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { ToastComponent } from "./toast.component";
import { ToastService } from "../../service/toast/toast.service";
import { ToastServiceMock } from "../../../../../test/mocks/toast.service.mock";
import { TranslationModule } from "../../../translate/translation.module";
import { SharedModule } from "../../shared.module";
import { ChromecastService } from "sxmServices";
import { mock } from "ts-mockito";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";

describe("ToastComponent", () =>
{
    let component: ToastComponent;
    let fixture: ComponentFixture<ToastComponent>;

    const data = {
        status: "ok",
        data: null
    };

    beforeEach(async(() =>
    {
        let chromecastServiceMock = mock(ChromecastService);
        chromecastServiceMock.chromecastInfo = new BehaviorSubject<any>(null);

        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SharedModule
            ],
            declarations: [
                ToastComponent
            ],
            providers: [
                { provide: ToastService, useValue: ToastServiceMock.getSpy() },
                { provide: ChromecastService, useFactory: () => { return chromecastServiceMock; } }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(ToastComponent);
        component = fixture.componentInstance;

        fixture.detectChanges();
    });

    describe("Infrastructure", () =>
    {
        it("should created the Toast Component", () =>
        {
            expect(component).toBeTruthy();
        });

        it("should expose the expected public properties", () =>
        {
            expect(component.toastInformation).toBeDefined();
            expect(component.onClickCTA).toBeDefined();
            expect(component.close).toBeDefined();
        });
    });

    describe("Toast Execution", () =>
    {
        it("should open the Toast Component on .open()", async(() =>
        {
            expect(component.toastInformation.isOpen).toEqual(false);
            ToastServiceMock.open({});
            fixture.detectChanges();

            expect(component.toastInformation.isOpen).toEqual(true);
        }));
    });
});
