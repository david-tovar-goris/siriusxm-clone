import { Component, Input } from "@angular/core";
import { ChromecastConst, ChromecastService, Logger } from "sxmServices";
import { ToastService } from "../../service/toast/toast.service";
import {
    ToastInformation
} from "./toast.interface";
import { TranslateService } from "@ngx-translate/core";
import { BehaviorSubject, Observable } from "rxjs";
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { filter, map } from "rxjs/operators";

/**
 * @MODULE:     client
 * @CREATED:    01/26/18
 * @COPYRIGHT:  2018 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *   ToastComponent is used to show a toast overlay
 *   above the player controls on any screen of the application.
 */

@Component({
    selector: "toast-overlay",
    templateUrl: "./toast.component.html",
    styleUrls: [ "./toast.component.scss" ]
})

export class ToastComponent
{

    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("ToastComponent");

    /**
     * Used to position the toast when miniplayer is enabled and disabled
     */
    @Input() isMiniPlayerEnabled: boolean = true;

    /**
     * Holds the toast information.
     */
    public toastInformation: ToastInformation;

    /**
     * holds the toast close timer
     */
    private toastTimerId: any = null;

    public alertMsg: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    public isCasting$: Observable<boolean> =  null;

    /**
     * Subscribes to the ToastInformation observable.
     *
     * When the toastInformation.isOpen value changes to true, our toast overlay is rendered.
     * When the toastInformation.isOpen value changes to false, our toast overlay disappears.
     *
     * @param {ToastService} toastService
     * @param {TranslateService} translateService
     * @param {LiveAnnouncer} liveAnnouncer
     */
    constructor (private toastService: ToastService,
                 private translateService: TranslateService,
                 private liveAnnouncer: LiveAnnouncer,
                 private chromecastService: ChromecastService)
    {
        this.toastService.toastInformation
            .subscribe((val: ToastInformation) =>
            {
                this.toastInformation = val;

                if (val.data)
                {
                    const translation = this.translateService.instant(val.data.messagePath);
                    const alertText = val.data.customMessage
                        ? val.data.customMessage + " " + translation
                        : translation;

                    this.alertMsg.next(alertText);

                    this.liveAnnouncer.announce(alertText);
                }

                if (this.toastTimerId)
                {
                    clearTimeout(this.toastTimerId);
                    this.toastTimerId = null;
                }

                this.toastTimerId = val.data && val.data.closeToastTime ? setTimeout(this.close.bind(this), val.data.closeToastTime) : null;
            });

        this.observeChromecastData();
    }
    onClickCTA(): void
    {
        ToastComponent.logger.debug(`onClickCTA()`);

        this.close();
    }

    /**
     * Closes the toast overlay.
     */
    close (): void
    {
        ToastComponent.logger.debug(`close()`);
        if (this.toastTimerId)
        {
            clearTimeout(this.toastTimerId);
            this.toastTimerId = null;
        }
        this.toastService.close();
    }

    /**
     * Get the caststatus to determine the bottom value.
     */
    private observeChromecastData(): void
    {
        this.isCasting$ = this.chromecastService.chromecastInfo.pipe(
            filter(castingData => !!castingData),
            map(castingData => castingData.castStatusMessage !== ChromecastConst.CASTING_NONE));
    }
}
