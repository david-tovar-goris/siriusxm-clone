import { Component, Input } from "@angular/core";

@Component({
    selector: "loading-spinner",
    template: `<img *ngIf="isLoading"
                    class="loading-spinner"
                    src="../../../assets/images/buffering-blue@2x.png"
                    srcset="../../../assets/images/buffering-blue@3x.png 2x"
                    alt=""/>
               <p class="alert-hidden"
                  role="alert">
                    {{ (isLoading ? 'common.loading' : 'common.loaded') | translate }}
               </p>`,
    styleUrls: ["./loading-spinner.component.scss"]
})

export class LoadingSpinnerComponent
{
    @Input() isLoading: boolean;
}
