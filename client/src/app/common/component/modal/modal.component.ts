import {
    Component,
    Input,
    OnInit,
    OnDestroy,
    AfterViewInit
} from "@angular/core";
import {
    IModalData,
    IModalButtonData
} from "../../service/modal/modal.interface";
import { EErrorTemplate } from "../../service/error/error-template.enum";
import { FocusUtil } from "../../util/focus.util";
import {
    AnalyticsTagActionConstants,
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsScreens,
    AnalyticsTagNameConstants
} from "../../../analytics/sxm-analytics-tag.constants";
import { AppErrorCodes } from "sxmServices";
import { BrowserUtil }from "../../util/browser.util";

@Component({
    selector: "dynamic-modal",
    templateUrl: "modal.component.html",
    styleUrls: [ "./modal.component.scss" ]
})

export class DynamicModalComponent implements OnInit, AfterViewInit, OnDestroy
{
    /**
     * Analytics Constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsScreens = AnalyticsScreens;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsInputTypes = AnalyticsInputTypes;

    /**
     * Analytics props
     */
    public tagName: string;
    public modalName: string;
    public btnOneText: string;
    public btnTwoText: string;
    public btnOneUserPath: string;
    public btnTwoUserPath: string;

    /**
     * The data used to populate the modal.
     */
    @Input() modalData: IModalData;

    /**
     * Flag indicating if the modal is open or closed.
     * @type {boolean}
     */
    private closed: boolean = false;

    public isOverlay: boolean;

    public _ref: any;

    /**
     * Error code and type used when Safari blocks auto-play by pausing the video on startup. This is a
     * special case and we have specific modal styling as a result.
     *
     * NOTE: The string error codee below matches the error code in the localization bundle for the key:
     *
     * errors.safariAutoPlayPause.errorCode
     *
     * And is the only way this specific modal template will work.
     */
    public SAFARI_AUTO_PLAY_FAILED_ERROR: string = "SAFARI-AUTO-PLAY-FAILED";

    /**
     * Error code and type used when autoplay blocks an audio stream.
     *
     * key: errors.audioPlaybackDenied.errorCode (PLAYBACK_DENIED)
     *
     * template is different for Safari which has user controlled autoplay in the browser
     * Firefox, Edge, Chrome use a generic template since the user can't control autoplay
     *
     * isSafari: allows switching template items between Safari specific and generic
     */
    public PLAYBACK_DENIED: string = "PLAYBACK_DENIED";

    /**
     * Error code and type used when autoplay blocks a video stream.
     *
     * key: errors.autoPlayDenied.errorCode (AUTOPLAY_DENIED)
     *
     * template is different for Safari which has user controlled autoplay in the browser
     * Firefox, Edge, Chrome use a generic template since the user can't control autoplay
     *
     * isSafari: allows switching template items between Safari specific and generic
     */
    public AUTOPLAY_DENIED: string = "AUTOPLAY-DENIED";
    public isSafari: boolean = BrowserUtil.isSafari();

    ngOnInit()
    {
        this.isOverlay = this.modalData.modalType === EErrorTemplate.OVERLAY;
        this.setAnalyticsConstants();
    }

    ngAfterViewInit()
    {
        FocusUtil.setupAccessibleDialog(document.getElementById('dynamic-modal'), true);
    }

    ngOnDestroy()
    {
        FocusUtil.closeFocusedDialog();
    }

    /**
     *
     * ModalOpen - Opens/Closes the modals @type {boolean}
     */
    public close()
    {
        this._ref.destroy();
        this.closed = true;
        if(this.modalData.onClose)
        {
            this.modalData.onClose();
        }
    }

    /**
     * Calls the provided action. If no action is provided it will default to closing the modal
     * If closeModal is true the modal will be closed after the provided action is taken
     * @param modalData
     */
    public takeButtonAction({ action = this.close.bind(this) }: IModalButtonData): void
    {
        action();

        if (!this.closed) this.close();
    }

    /**
     * sets analytics properties
     */
    setAnalyticsConstants()
    {
        const errorCode = parseInt(this.modalData.errorCode);
        switch(errorCode)
        {
            case AppErrorCodes.FLTT_RECENTLY_PLAYED_CLEAR_SELECTED.faultCode:
                this.tagName = AnalyticsTagNameConstants.MDL_BUTTON;
                this.modalName = AnalyticsTagNameConstants.MDL_CONFIRM_DELETE_MODAL;
                this.btnOneText = this.modalData.buttonOne.text;
                this.btnTwoText = this.modalData.buttonTwo.text;
                this.btnOneUserPath = `${AnalyticsTagNameConstants.MDL_BUTTON}_${this.modalName}_${this.btnOneText}`;
                this.btnTwoUserPath = `${AnalyticsTagNameConstants.MDL_BUTTON}_${this.modalName}_${this.btnTwoText}`;
                break;
        }
    }
}
