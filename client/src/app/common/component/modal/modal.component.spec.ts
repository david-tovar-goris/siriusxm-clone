import { TestBed } from "@angular/core/testing";
import { DynamicModalComponent } from "./modal.component";

describe("DynamicModalComponent", () =>
{
    let fixture, component;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            declarations: [
                DynamicModalComponent
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(DynamicModalComponent);
        component = fixture.componentInstance;
    });

    describe('pending - close()', () =>
    {

    });


    describe('pending - takeButtonAction()', () =>
    {

    });
});
