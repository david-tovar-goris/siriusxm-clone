import {
    Component,
    EventEmitter,
    Input,
    Output,
    HostListener
} from "@angular/core";
import { KeyboardEventKeyTypes } from "sxmServices";

/**
 * @MODULE:     client
 * @CREATED:    04/16/21
 * @COPYRIGHT:  2021 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *   OverlayContainerComponent used to show overlay on different screens
 */

@Component({
    selector: "overlay-container",
    templateUrl: "./overlay-container.component.html",
    styleUrls: [ "./overlay-container.component.scss" ]

})

export class OverlayContainerComponent
{
    @Input() showOverlay: boolean = false;
    @Output() showOverlayChange: EventEmitter<boolean> = new EventEmitter();

    @Input() enableCloseIcon: boolean = true;
    @Output() onClose: EventEmitter<void> = new EventEmitter();
    /**
     * Keyboard event handler
     */
    @HostListener("document:keydown", [ "$event" ])
    onKeyPress(event: KeyboardEvent)
    {
        if (event.key === KeyboardEventKeyTypes.ESC || event.key === KeyboardEventKeyTypes.ESCAPE)
        {
            this.onOverlayClose();
        }
    }

    public onOverlayClose()
    {
        this.showOverlay = false;
        this.showOverlayChange.emit(this.showOverlay);
        this.onClose.emit();
    }
}
