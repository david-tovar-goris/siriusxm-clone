import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { NavigationModule } from "../../../navigation/navigation.module";
import { SharedModule } from '../../shared.module';

import { NotificationOverlayComponent } from "./notification.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        NavigationModule,
        SharedModule
    ],
    declarations: [
        NotificationOverlayComponent
    ],
    exports:[
        NotificationOverlayComponent
    ],
     providers: []
})

export class NotificationModule
{
}
