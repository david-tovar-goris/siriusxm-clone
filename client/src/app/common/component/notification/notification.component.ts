import {
    Component,
    OnInit
} from "@angular/core";

import { NotificationService } from "../../service/notification/notification.service";
import { AutoUnSubscribe } from "../../decorator";
import { FlepzScreenService } from "../../service/flepz/flepz-screen.service";
import {
    INotification,
    INotificationInformation,
    IButton,
    Logger,
    MessagingService,
    INeriticLinkData
} from "sxmServices";

import { NeriticLinkService } from "../../../neritic-links";

import { SubscriptionLike as ISubscription } from "rxjs";
import { FocusUtil } from "../../util/focus.util";
import { OverlayService } from "../../../common/service/overlay/overlay.service";
import { TranslateService } from "@ngx-translate/core";

/**
 * @MODULE:     client
 * @CREATED:    07/11/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *   OverlayComponent used to show overlay on different screens
 */

@Component({
    selector: "notification-overlay",
    templateUrl: "./notification.component.html",
    styleUrls: [ "./notification.component.scss" ]

})

@AutoUnSubscribe([])
export class NotificationOverlayComponent implements OnInit
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("NotificationOverlayComponent");

    /**
     *  Holds the overlay information of open/close and data(header/Desc).
     */
    public notificationInformation: INotificationInformation = {
        data: null,
        open: false
    };

    public currentNotification: INotification;

    /**
     * Stores subscriptions to unsubscribe when component is destroyed
     *
     * @type {Subscription}
     * @memberof NotificationComponent
     */
    public subscriptions: Array<ISubscription> = [];

    /**
     * @param {MessagingService} messagingService
     * @param {NeriticLinkService} neriticLinkService
     * @param {NotificationService} notificationService
     * @param {FlepzScreenService} flepzScreenService
     */
    constructor(private messagingService: MessagingService,
                private neriticLinkService: NeriticLinkService,
                private notificationService: NotificationService,
                private overlayService: OverlayService,
                private translate: TranslateService,
                private flepzScreenService: FlepzScreenService) {}

    ngOnInit()
    {
        NotificationOverlayComponent.logger.debug(`ngOnInit()`);
        this.subscribeToNotificationInformation();
        this.subscribeToNotification();
        this.neriticLinkService.invalidNeriticLink.subscribe((neriticLinkData: INeriticLinkData) =>
            {
                if (neriticLinkData.linkType)
                {
                    this.showErrorMessageOverlay();
                }
            }
        );
    }

    public subscribeToNotificationInformation(): void
    {
        NotificationOverlayComponent.logger.debug(`subscribeToNotificationInformation()`);

        const informationSuccess = (info: INotificationInformation): void =>
        {
            if (info) this.notificationInformation = info;
        };

        const informationFault = (fault: any): void =>
        {
            NotificationOverlayComponent.logger.warn(`informationFault( ${JSON.stringify(fault)} )`);
        };

        this.subscriptions.push(
            this.notificationService.notificationInformation.subscribe(
                informationSuccess.bind(this),
                informationFault
            )
        );

    }

    public subscribeToNotification(): void
    {
        NotificationOverlayComponent.logger.debug(`subscribeToNotification()`);

        const notificationSuccess = (notification: INotification) =>
        {
            if (notification && Object.getOwnPropertyNames(notification).length > 2)
            {
                this.notificationService.open(notification);
                this.currentNotification = notification;
                /*
                This is a hack due to this component getting loaded on App load. The way to fix this would be
                to have a service that subscribes the the notificationService on app load and creates this component
                dynamically. Till now this hack allows time for Angular to do it's dirty check and show this component.
                    */
                setTimeout(() =>
                {
                    FocusUtil.setupAccessibleDialog(document.getElementById('notification-dialog'), true);
                    this.sendConfirm();
                }, 500);
            }
        };

        const notificationFault = (fault: any): void =>
        {
            NotificationOverlayComponent.logger.warn(`notificationFault( ${JSON.stringify(fault)} )`);
        };


        this.subscriptions.push(
            this.messagingService.notification.subscribe(
                notificationSuccess.bind(this),
                notificationFault
            )
        );
    }

    /**
     * when user closes the overlay
     */
    close(): void
    {
        NotificationOverlayComponent.logger.debug(`close()`);

        /**
         * @description when a Notification is closed we change its
         * priority to -1 to avoid dispatching it again
         */
        this.notificationInformation.data.priority = -1;

        this.notificationService.close();
        FocusUtil.closeFocusedDialog();
        this.messagingService.emitMarketingMessage();
    }


    /**
     * Send confirmation API request
     */
    public sendConfirm(): void
    {
        NotificationOverlayComponent.logger.debug(`sendConfirm()`);

        const confirmSuccess = (response: any): void =>
        {
            NotificationOverlayComponent.logger.debug(`confirmSuccess( ${JSON.stringify(response)} )`);
        };

        const confirmFault = (fault: any): void =>
        {
            NotificationOverlayComponent.logger.warn(`confirmFault( ${JSON.stringify(fault)} )`);
        };

        this.subscriptions.push(
            this.messagingService.sendConfirm(this.currentNotification)
                .subscribe(
                    confirmSuccess,
                    confirmFault
                )
        );

        this.currentNotification = null;
    }



    /**
     * @description gets the current notification and the correct button,
     * builds an INotificationFeedback and sends it to the API
     *
     * @todo handle neritic actions per button
     *
     * @param buttonKey
     */
    public onButtonClick(buttonKey: string): void
    {
        NotificationOverlayComponent.logger.debug(`onButtonClick()`);
        const DISMISS_BUTTON = "dismissButton";
        let notification: INotification = this.notificationInformation.data;
        let button: IButton = this.messagingService.getButtonFromNotification(notification, buttonKey);
        let neriticLink: INeriticLinkData;

        const feedbackSuccess = (response: any): void =>
        {
            NotificationOverlayComponent.logger.debug(`feedbackSuccess( ${JSON.stringify(response)} )`);
        };

        const feedbackFault = (fault: any): void =>
        {
            NotificationOverlayComponent.logger.warn(`feedbackFault( ${JSON.stringify(fault)} )`);
        };

        this.messagingService.sendFeedback(notification, button)
            .subscribe(
                feedbackSuccess,
                feedbackFault
            );

        if((buttonKey !== DISMISS_BUTTON) && button.neriticLink)
        {
            if(button.neriticLink)
            {
                neriticLink = this.neriticLinkService.getNeriticData(button.neriticLink);
                this.neriticLinkService.takeAction(neriticLink);
            }
        }

        this.close();
    }

    /**
     * opens overlay with generic error message
     */
    public showErrorMessageOverlay() : void
    {
        this.overlayService.open({
            overlayData: this.translate.instant('login.messagingNeriticLinkOverlay')
        });
    }
}
