/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />

import {
    async,
    TestBed,
    ComponentFixture
} from "@angular/core/testing";
import { NotificationOverlayComponent } from "./notification.component";
import {
    INotification,
    INotificationInformation,
    MessagingService
} from "sxmServices";
import { messagingServiceMock } from "../../../../../../servicelib/src/test/mocks/app-messaging/messaging.service.mock";
import { NeriticLinkService } from "../../../neritic-links";
import { NotificationService } from "../../service/notification/notification.service";
import { notificationServiceMock } from "../../../../../test/mocks/notification.service.mock";
import { FlepzScreenService } from "../../service/flepz/flepz-screen.service";
import { NeriticLinkServiceMock } from "../../../../../test/mocks/neritic-link.service.mock";
import { FlepzScreenServiceMock } from "../../../../../test/mocks/flepz-screen.service.mock";
import { SharedModule } from '../../shared.module';
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { DebugElement } from "@angular/core";
import { notificationMock } from "../../../../../../servicelib/src/test/mocks/app-messaging/notification.mock";
import { OverlayService } from "../../service/overlay/overlay.service";
import { OverlayServiceMock } from "../../../../../test/mocks/overlay.service.mock";

describe('NotificationOverlayComponent', () =>
{
    let fixture: ComponentFixture<NotificationOverlayComponent>,
        component: NotificationOverlayComponent,
        debugElement: DebugElement,
        notificationInformation: INotificationInformation = {
            open: false,
            data: {} as INotification
        },
        neriticLinkServiceMock = new NeriticLinkServiceMock(),
        testBed;

    beforeEach(async(() =>
    {

        testBed = TestBed.configureTestingModule({
            imports: [
                SharedModule,
                TranslateModule.forRoot()
            ],
            declarations: [
                NotificationOverlayComponent
            ],
            providers: [
                { provide: MessagingService, useValue: messagingServiceMock },
                { provide: NeriticLinkService, useValue: neriticLinkServiceMock },
                { provide: NotificationService, useValue: notificationServiceMock },
                { provide: OverlayService, useValue: OverlayServiceMock },
                { provide: FlepzScreenService, useClass: FlepzScreenServiceMock }
            ]
        });

        testBed.compileComponents();

    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(NotificationOverlayComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        component.notificationInformation = notificationInformation;
        fixture.detectChanges();
    });

    afterEach(() =>
    {
        component = null;
        fixture = null;
        debugElement = null;
    });

    it("Should exist", () =>
    {
        expect(component).toBeDefined();
        expect(component).toBeTruthy();
    });

    describe('onButtonClick()', () =>
    {
        let buttonMock;

        beforeEach(() =>
        {
            buttonMock = notificationMock.buttons.primaryButton;
            messagingServiceMock.getButtonFromNotification.and.returnValue(buttonMock);
            spyOn(component, 'close');
        });

        /**
         * depending on what we use to parse the neritic link
         * either this test or the previous one should be x'd out
         */
        it('calls the neriticLink action', () =>
        {
            component.onButtonClick('primaryButton');
            expect(neriticLinkServiceMock.getNeriticData).toHaveBeenCalledWith(
                buttonMock.neriticLink
            );
        });

        it('closes the overlay', () =>
        {
            component.onButtonClick('primaryButton');
            expect(component.close).toHaveBeenCalled();
        });
    });
});
