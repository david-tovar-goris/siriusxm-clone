import {
    Component,
    Inject
} from "@angular/core";

import { TranslateService } from "@ngx-translate/core";
import {languageConstants} from "./language.consts";
import {
    AuthenticationService,
    IAppConfig,
    StorageService
} from "sxmServices";
import { APP_CONFIG } from "../../../sxmservicelayer/sxm.service.layer.module";
import { Location } from "@angular/common";
import { overlayConsts } from "app/profile/profile.const";

@Component({
    selector: "language-toggle",
    templateUrl: "./language-toggle.component.html",
    styleUrls: [ "./language-toggle.component.scss" ]
})


export class LanguageToggleComponent
{

    constructor(private translate: TranslateService,
                private storageService : StorageService,
                private authenticationService: AuthenticationService,
                @Inject(APP_CONFIG) public appConfig : IAppConfig,
                public location : Location) {}

    /**
     * Retrieves the current language from local storage (if present) or
     * the browser
     */
     getCurrentLanguage() : string
    {
        const langSetting = this.storageService.getItem('language');
        return langSetting ? langSetting : this.translate.getBrowserLang();
    }

    /**
     * Return true if the current language is set to French
     * @returns {boolean}
     */
    isFrench(): boolean
    {
        const currentLang = this.getCurrentLanguage();
        return currentLang == languageConstants.FRENCH ? true : false;
    }

    /**
     * Sets the current language in local storage
     */
    private setLocalStorageLanguage(language : string)
    {
        this.storageService.setItem('language',language);
    }

    /**
     * Toggles language from English to French or vice versa.
     * Reload the app and use the language selected by the user.
     * @param {string} language
     */
    private toggleLang(language: string)
    {
        const currentLang = this.getCurrentLanguage();

        if (language !== currentLang)
        {
            this.setLocalStorageLanguage(language);

            //set the session storage variable to display open access overlay
            if(this.authenticationService.isOpenAccessEligible())
            {
                window.sessionStorage.setItem(overlayConsts.DISPLAY_OA_OVERLAY , "true");
            }

            const queryParams = this.appConfig.contextualInfo.queryString;
            const requestURL = queryParams ?  "/?"+queryParams : "" ;

            this.location.go(`${requestURL}`);
            location.reload();
        }
    }
}
