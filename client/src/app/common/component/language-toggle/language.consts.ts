/**
 * @CREATED:    02/23/18
 * @COPYRIGHT:  2018 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *  LanguageConstants are used to determine the language settings of the app.
 **/


export const languageConstants = {
    ENGLISH: "en",
    FRENCH: "fr",
    ENGLISH_CANADA: "en-ca"
};
