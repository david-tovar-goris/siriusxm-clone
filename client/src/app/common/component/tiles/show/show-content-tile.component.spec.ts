import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { NowPlayingIndicatorServiceMock } from "../../../../../../test/mocks/now-playing-indicator.service.mock";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import { ITile, MediaPlayerService } from "sxmServices";
import { NowPlayingIndicatorService } from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { MockComponent } from "../../../../../../test/mocks/component.mock";
import { TileContextMenuOptionsServiceMock } from "../../../../../../test/mocks/tile-context-menu-options.service.mock";
import { ShowContentTileComponent } from "./show-content-tile.component";
import { TranslationModule } from "../../../../translate/translation.module";
import { LocalizeTileTextPipe } from "../../../pipe/localize-tile-text.pipe";
import { CarouselService } from "../../../../carousel/carousel.service";
import { DoubleTapOpenContextMenu } from "../../../../context-menu/directives/double-tap-open-context-menu.directive";
import { ContextMenuService } from "../../../../context-menu";
import { ContextMenuServiceMock } from "../../../../../../test/mocks/context-menu.service.mock";
import { CarouselServiceMock } from "../../../../../../test/mocks";
import { MediaPlayerServiceMock } from "../../../../../../test/mocks/media-player.service.mock";
import { Observable } from "rxjs";
import { ChangeDetectorRef } from "@angular/core";
import { LazyLoadImageModule } from 'ng-lazyload-image';

describe('ShowContentTileComponent', () =>
{
    let component: ShowContentTileComponent,
        fixture: ComponentFixture<ShowContentTileComponent>,
        mockMediaPlayerService = new MediaPlayerServiceMock();

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                LazyLoadImageModule
            ],
            declarations: [
                ShowContentTileComponent,
                LocalizeTileTextPipe,
                DoubleTapOpenContextMenu,
                MockComponent({ selector: "tile-image", inputs: [ "tileData" ] })
            ],
            providers: [
                { provide: MediaPlayerService, useValue : mockMediaPlayerService },
                { provide: TileContextMenuOptionsService, useClass: TileContextMenuOptionsServiceMock },
                { provide: NowPlayingIndicatorService, useClass: NowPlayingIndicatorServiceMock },
                { provide: CarouselService, useClass: CarouselServiceMock },
                { provide: ChangeDetectorRef, useClass: ChangeDetectorRef },
                { provide: ContextMenuService, useClass: ContextMenuServiceMock }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(ShowContentTileComponent);
        component = fixture.componentInstance;

        component.tileOrientation = "";
        component.titleLine = {nativeElement: {innerHtml:"line 2", style:{webkitLineClamp:1}}};
        component.tileData = {
            primaryNeriticLink: { contentType: "", channelId: '' },
            tileAssetInfo: { showGuid: '' },
            neriticLinkData: [{ contentSubType: 'aod' }],
            line1: "line 1",
            line1$: Observable.create((observer) => observer.next("line 1")),
            line2: "line 2",
            line2$: Observable.create((observer) => observer.next("line 2")),
            line3: "line 3",
            line3$: Observable.create((observer) => observer.next("line 3")),
            tileShape : "square",
            fgImageUrl: "url",
            bgImageUrl: "url",
            tileBanner: {
                bannerClass: "",
                bannerText : "New show",
                bannerColor : ""
            }
        } as ITile;

        fixture.detectChanges();
    });

    describe('isAOD - Property', () =>
    {
        describe('when the show is aod', () =>
        {
            it('should evaluate as true', () =>
            {
                expect(component.isAOD).toEqual(true);
            });
        });
    });
});
