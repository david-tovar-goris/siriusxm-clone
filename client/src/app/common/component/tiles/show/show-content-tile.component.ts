import { BehaviorSubject, combineLatest as observableCombineLatest, Observable, Subject } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import {
    Component,
    Input,
    ViewChild,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    ElementRef,
    OnInit,
    AfterViewInit
} from "@angular/core";
import {
    CarouselTypeConst,
    ContentTypes,
    ITile,
    TileShapeType,
    MediaPlayerService
} from "sxmServices";
import { AutoUnSubscribe } from "../../../../common/decorator";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import {
    NowPlayingIndicatorService,
    tileContentType
} from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { CMOption } from "../../../../context-menu";
import { CarouselService } from "../../../../carousel/carousel.service";
import { TranslateService } from "@ngx-translate/core";
import { EllipsisStringUtil } from "../../../util/ellipsisString.util";
import { TileUtil } from "../../../util/tile.util";

@AutoUnSubscribe()
@Component({
    selector: "show-content-tile",
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: "../show/show-content-tile.component.html",
    host: { '(window:resize)': 'this.resizeEvent$.next(event)' },
    styleUrls: [ "../content-tile.scss" ]
})

export class ShowContentTileComponent implements OnInit
{
    private _tileData: ITile;
    @Input() lazyLoadEvent$: Subject<boolean>;
    public lazyLoadOnInit$: BehaviorSubject<boolean> = new BehaviorSubject(null);
    @Input() set tileData(tile: ITile)
    {
        this._tileData = tile;
        const contentType: string = (this.tileData.tileContentSubType === CarouselTypeConst.ADDITIONAL_CHANNEL)
            ? this.tileData.tileContentSubType
            : this.tileData.tileContentType;
        this.tileShape = this.tileData.tileShape || "square";
        this.isAOD = this.tileData.neriticLinkData[0].contentSubType === ContentTypes.AOD;
        this.tileTitleLinesToClamp = (this.tileData.line3 && this.tileData.line3.length > 0) ? 1 : 2;

        /*
            This data is not populated if the tile is a Pandora Podcast
         */

        if (!this.tileData.tileAssetInfo.isPandoraPodcast && this.tileData.line2)
        {
            this.isUpdating = true;
            observableCombineLatest(this.resizeEvent$.pipe(debounceTime(500)), this.tileData.line2$)
                .subscribe((data: [any, string]) =>
                {
                    this.titleLine.nativeElement.innerHTML = data[1];
                    EllipsisStringUtil.clamp(this.titleLine.nativeElement,
                        {clamp: this.tileTitleLinesToClamp, useNativeClamp: true});
                    this.isUpdating = false;
                    this.changeDetectorRef.markForCheck();
                });

            this.resizeEvent$.next(null);
        }

        this.lazyLoadUrl = this.getLazyLoadUrl();
        this.backgroundStyle = this.getBackgroundStyle();
    }

    get tileData() : ITile
    {
        return this._tileData;
    }

    /**
     * Optional tile shape configuration
     */
    @Input() tileShape?: TileShapeType;

    /**
     * If the carouselOrientation is "grid", this input is used add styles as a "grid" tile
     */
    @Input() tileOrientation: string;

    public vod = ContentTypes.VOD;
    public aod = ContentTypes.AOD;
    public live = ContentTypes.LIVE_AUDIO;
    public isAOD: boolean;
    public isUpdating: boolean = false;

    public lazyLoadUrl: string = null;

    public backgroundStyle: any = null;

    @ViewChild('titleLine') titleLine: ElementRef;

    private resizeEvent$: Subject<any> = new Subject<any>();
    public tileTitleLinesToClamp: number = 2;

    constructor(public nowPlayingIndicatorService: NowPlayingIndicatorService,
                public carouselService: CarouselService,
                private mediaPlayerService : MediaPlayerService,
                private changeDetectorRef: ChangeDetectorRef,
                public translate : TranslateService) {}

    ngOnInit()
    {
        if(!this.lazyLoadEvent$)
        {
            this.lazyLoadEvent$ = this.lazyLoadOnInit$;
            this.lazyLoadOnInit$.next(true);
        }
    }

    /**
     * gets context Menu Options for the show
     */

    public getLazyLoadUrl(): string
    {
        return TileUtil.normalizeImageUrl(this.tileData.bgImageUrl, 'width=320&height=240&preserveAspect=true');
    }

    public getBackgroundStyle(): object
    {
        const backgroundColor = this.tileData.backgroundColor
            ? this.tileData.backgroundColor
            : null;

        return {
            'background-color': backgroundColor,
            'background-image': 'linear-gradient(to bottom, rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0))'
        };
    }

    /**
     * Set default background color for tile if image fails to load
     */
    public onTileImageError(err: any)
    {
        if(this.tileData.tileAssetInfo.isPandoraPodcast)
        {
            this.backgroundStyle = TileUtil.getDefaultBackground(this.tileData);
        }
    }
}
