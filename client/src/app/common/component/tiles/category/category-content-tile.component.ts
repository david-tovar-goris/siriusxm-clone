import { BehaviorSubject, combineLatest as observableCombineLatest, Observable, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import {
    Component,
    Input,
    ElementRef,
    ViewChild,
    ChangeDetectorRef,
    ChangeDetectionStrategy, OnInit
} from "@angular/core";
import { ITile } from "sxmServices";
import { TranslateService } from "@ngx-translate/core";
import { AutoUnSubscribe } from "../../../decorator/auto-unsubscribe";
import { EllipsisStringUtil } from "../../../util/ellipsisString.util";
import { BrowserUtil } from "app/common/util/browser.util";

@Component({
    selector: "category-content-tile",
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: "./category-content-tile.component.html",
    host: { '(window:resize)': 'this.resizeEvent$.next(event)' },
    styleUrls: [ "../content-tile.scss" ]
})

@AutoUnSubscribe()
/**
 * Component to handle "category" tiles (tile.tileContentType).
 */
export class CategoryContentTileComponent implements OnInit
{
    private _tileData : ITile;
    public lazyLoadUrl: string = null;
    @Input() lazyLoadEvent$?: Subject<boolean>;
    public lazyLoadOnInit$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
    @Input() set tileData(tile : ITile)
    {
        this._tileData = tile;
        this.tileTitleLinesToClamp = this.isChannelsAvailableText() ? 1 : 2;
        this.isUpdating = true;
        observableCombineLatest(this.resizeEvent$.pipe(debounceTime(500)),
            this.tileData.line1$)
                  .subscribe((data : [ any, string ]) =>
                  {
                      this.titleLine.nativeElement.innerHTML = data[ 1 ];
                      EllipsisStringUtil.clamp(this.titleLine.nativeElement,
                          { clamp: this.tileTitleLinesToClamp, useNativeClamp: true });
                      this.isUpdating = false;
                      this.changeDetectorRef.markForCheck();
                  });
        this.lazyLoadUrl = this.getLazyLoadUrl();
        this.resizeEvent$.next(null);
    }

    get tileData() : ITile
    {
        return this._tileData;
    }

    /**
     * If the carouselOrientation is "grid", this input is used add styles as a "grid" tile
     */
    @Input() tileOrientation: string;

    @ViewChild('titleLine') titleLine: ElementRef;

    private resizeEvent$: Subject<any> = new Subject<any>();
    public tileTitleLinesToClamp : number = 2;
    public isUpdating : boolean = false;

    constructor(private translate: TranslateService,
                private changeDetectorRef: ChangeDetectorRef)
    {}

    ngOnInit(): void
    {
        if(!this.lazyLoadEvent$)
        {
            this.lazyLoadEvent$ = this.lazyLoadOnInit$;
            this.lazyLoadOnInit$.next(true);
        }
    }

    private isChannelsAvailableText()
    {
        if (!this.tileData.subCategoryInfo || !this.tileData.subCategoryInfo.channelList) { return; }

        let channelsAvailableText,
            translateText;

        this.translate.get("tile.channelsAvailable").subscribe((data) =>
        {
            translateText = data;
        });

        channelsAvailableText = this.tileData.subCategoryInfo.channelList.length + " " + translateText;

        return channelsAvailableText;
    }

    public getLazyLoadUrl(): string
    {
        return this.tileData.bgImageUrl
               ? this.tileData.bgImageUrl + '?width=320&height=240&preserveAspect=true'
               : null;
    }
}
