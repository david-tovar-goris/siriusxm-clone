import { Component, Input, EventEmitter, Output } from "@angular/core";
import { ITile } from "sxmServices";
import { TileUtil } from "../../../util/tile.util";

@Component({
    selector: "tile-image",
    template: `
        <div *ngIf="tileData.fgImageUrl && !isLoadFailed; then image else text"></div>

        <ng-template #image>

            <img class="channel-image {{ tileData.fgImageType }} {{ tileData.tileContentSubType }} {{ tileData.tileContentType }}"
                 [ngClass]="{ 'iris-image' : tileData.tileAssetInfo.isPandoraPodcast }"
                 [src]="tileImageUrl"
                 (error) = "onImageError($event)"
                 alt=""
                 role="presentation"
                 draggable="false">

        </ng-template>

        <ng-template #text>
            <p class="image-alt-text"
               [ngClass]="{ 'iris-alt-text' : tileData.tileAssetInfo.isPandoraPodcast }"
               role="presentation"
               aria-hidden="true">{{ tileData.imageAltText }}</p>
        </ng-template>
    `,
    styleUrls: ["./tile-image.component.scss"]
})

export class TileImageComponent {
    public tileImageUrl:string;
    private _tileData : ITile;
    public isLoadFailed = false;

    @Output() error: EventEmitter<any> = new EventEmitter();
    @Input() set tileData(tile : ITile)
    {
        this._tileData = tile;
        if(this.tileData.fgImageUrl)
        {
            this.tileImageUrl =  TileUtil.normalizeImageUrl(this.tileData.fgImageUrl, "width=160&height=120&preserveAspect=true");
        }
        else
        {
            this.error.emit();
        }
    }

    get tileData() : ITile
    {
        return this._tileData;
    }

    /**
     * Emit if tile image fails to load
     * @param err
     */
    public onImageError(err)
    {
        this.isLoadFailed = true;
        this.error.emit(err);
    }
}
