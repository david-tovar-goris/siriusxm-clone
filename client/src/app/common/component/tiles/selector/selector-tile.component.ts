import {Component, Input}   from "@angular/core";
import {ICarouselSelector}  from "sxmServices";
import {NeriticLinkService} from "../../../../neritic-links";
import {
    AnalyticsElementTypes,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths,
    AnalyticsTagTextConstants
} from "app/analytics/sxm-analytics-tag.constants";
import { Router } from "@angular/router";

@Component({
    selector: "selector-tile",
    templateUrl: "./selector-tile.component.html",
    styleUrls: ["./selector-tile.component.scss"]
})

export class SelectorTileComponent
{
    /**
     * Analytics Constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsTagTextConstants = AnalyticsTagTextConstants;

    @Input() selector: ICarouselSelector = {} as ICarouselSelector;

    private route: string;

    /**
     * Constructor
     * @param {NavigationService} navigationService
     * @param {NeriticLinkService} neriticLinkService
     * @param {Router} router
     */
    constructor(public neriticLinkService: NeriticLinkService,
                private router: Router)
    {
    }

    ngOnInit()
    {
        this.route = this.router.url;
    }

    /**
     * For Analytics
     */
    public getSegmentButtonAnaTags(type): {
        tagName: string,
        userPath: string,
        buttonName: string,
        tagText: string
    }
    {
        switch(type)
        {
            case 'ALLCHANNELS':
                return {
                    tagName: AnalyticsTagNameConstants.SEGMENT_BTN_ALLCHANNELS,
                    userPath: AnalyticsUserPaths.SEGMENT_BTN_ALLCHANNELS,
                    buttonName: AnalyticsTagNameConstants.SEGMENT_BTN_ALLCHANNELS_BUTTON_NAME,
                    tagText: null
                };
            case 'VIDEOS':
                return {
                    tagName: AnalyticsTagNameConstants.SEGMENT_BTN_VIDEOS,
                    userPath: AnalyticsUserPaths.SEGMENT_BTN_VIDEOS,
                    buttonName: AnalyticsTagNameConstants.SEGMENT_BTN_VIDEOS_BUTTON_NAME,
                    tagText: AnalyticsTagTextConstants.SEGMENT_BTN_VIDEOS
                };
            default:
                return {
                    tagName: '',
                    userPath: '',
                    buttonName: '',
                    tagText: ''
                };
        }
    }
}
