import { TestBed }             from "@angular/core/testing";
import {NeriticLinkService}    from "../../../../neritic-links";
import {SelectorTileComponent} from "./selector-tile.component";

describe("SelectorTileComponent", () =>
{
    let fixture,
        component;

    beforeEach(async () =>
    {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [
                SelectorTileComponent
            ],
            providers: [
                { provide: NeriticLinkService, useValue: NeriticLinkService }
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(SelectorTileComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
});
