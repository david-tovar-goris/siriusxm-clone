import { Component, Input, OnInit } from '@angular/core';
import { CarouselService } from "app/carousel/carousel.service";

@Component({
    selector: 'pills-list',
    templateUrl: './pills-list.component.html',
    styleUrls: ['./pills-list.component.scss']
})
export class PillsListComponent {

    @Input() title;
    @Input() pillsList;

    constructor(private carouselService: CarouselService)
    { }

}
