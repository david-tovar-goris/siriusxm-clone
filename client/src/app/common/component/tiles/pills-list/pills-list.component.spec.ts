import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslationModule } from "../../../../translate/translation.module";
import { PillsListComponent } from './pills-list.component';
import { SharedModule } from "app/common/shared.module";
import { CarouselService } from "app/carousel/carousel.service";
import { CarouselServiceMock } from "../../../../../../test/mocks/carousel.service.mock";
import {PillTileComponent} from "app/common/component/tiles/pill-tile/pill-tile.component";
import {MockComponent} from "../../../../../../test/mocks/component.mock";

describe('PillsListComponent', () =>
{
    let component: PillsListComponent;
    let fixture: ComponentFixture<PillsListComponent>;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                TranslationModule],
            declarations: [ PillsListComponent,
                MockComponent({ selector: "pill-tile", inputs: ["tileData"]} )],
            providers : [
                { provide: CarouselService, useValue: CarouselServiceMock.getSpy() }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(PillsListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () =>
    {
        expect(component).toBeTruthy();
    });
});
