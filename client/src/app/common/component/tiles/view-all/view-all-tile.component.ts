import {
    Component,
    Input
} from "@angular/core";
import {
    ICarouselData,
    ITile
} from "sxmServices";

@Component({
    selector: "view-all-tile",
    templateUrl: "./view-all-tile.component.html",
    styleUrls: ["../content-tile.scss"]
})

export class ViewAllTileComponent
{
    /**
     * Tile data used to populate the view
     */
    private _tileData: ITile; // backing store

    private _carouselData: ICarouselData;

    @Input() set tileData(tile: ITile)
    {
        this._tileData = tile;
    }

    get tileData(): ITile
    {
        return this._tileData;
    }

    @Input() set carouselData(carousel: ICarouselData)
    {
        this._carouselData = carousel;
    }

    get carouselData(): ICarouselData
    {
        return this._carouselData;
    }

    get tileShape():string
    {
        return this._carouselData.type === 'hero' ? 'rectangle' : this._carouselData.tiles[0].tileShape;
    }

    constructor()
    {
    }

}
