import {
    ComponentFixture,
    TestBed
}                             from "@angular/core/testing";
import {
    ICarouselData,
    ITile
}                             from "sxmServices";
import {ViewAllTileComponent} from "./view-all-tile.component";
import {Observable}           from "rxjs";
import { TranslationModule } from "../../../../translate/translation.module";
import { tileMock } from "../../../../../../test/mocks/data/tile.mock";

describe("ViewAllTileComponent()", () =>
{
    let component: ViewAllTileComponent,
        fixture: ComponentFixture<ViewAllTileComponent>;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports: [TranslationModule],
            declarations: [
                ViewAllTileComponent
            ],
            providers: []
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture                      = TestBed.createComponent(ViewAllTileComponent);
        component                    = fixture.componentInstance;
        component.tileData           = {
            primaryNeriticLink: {
                contentType: ""
            },
            fgImageUrl: "url",
            bgImageUrl: "url",
            iconImageUrl: "iconImgUrl",
            channelNumber: "Ch 100",
            subtext: "View All",
            line1: "line 1",
            line1$: Observable.create((observer) => observer.next("line 1")),
            line2: "line 2",
            line2$: Observable.create((observer) => observer.next("line 2")),
            line3: "line 3",
            line3$: Observable.create((observer) => observer.next("line 3")),
            tileBanner: {}
        } as ITile;
        component.carouselData       = {} as ICarouselData;
        component.carouselData.title = Object.assign({}, component.carouselData.title, {textValue: "viewAll"});
        component.carouselData.tiles = [tileMock];
        fixture.detectChanges();
    });

    it("should create", () =>
    {
        expect(component).toBeDefined();
    });
});
