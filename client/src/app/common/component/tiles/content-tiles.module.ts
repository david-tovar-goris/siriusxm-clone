import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChannelContentTileComponent } from "./channel/channel-content-tile.component";
import { ShowContentTileComponent } from "./show/show-content-tile.component";
import { CategoryContentTileComponent } from "./category/category-content-tile.component";
import { CarouselService } from "../../../carousel/carousel.service";
import { EpisodeContentTileComponent } from "./episode/episode-content-tile.component";
import { NeriticLinkService } from "../../../neritic-links";
import { ContextMenuModule } from "../../../context-menu/context-menu.module";
import { SharedModule } from "../../shared.module";
import { TileImageComponent } from "./tile-image/tile-image.component";
import { TranslateModule } from "@ngx-translate/core";
import { CollectionContentTileComponent } from "./collection/collection-content-tile.component";
import {ViewAllTileComponent} from "./view-all/view-all-tile.component";
import {SelectorTileComponent} from "./selector/selector-tile.component";
import { PillTileComponent } from './pill-tile/pill-tile.component';
import { PillsListComponent } from './pills-list/pills-list.component';


@NgModule({
    imports: [
        CommonModule,
        ContextMenuModule,
        SharedModule,
        TranslateModule
    ],
    declarations: [
        ChannelContentTileComponent,
        EpisodeContentTileComponent,
        ShowContentTileComponent,
        CategoryContentTileComponent,
        CollectionContentTileComponent,
        TileImageComponent,
        ViewAllTileComponent,
        SelectorTileComponent,
        PillsListComponent,
        PillTileComponent
    ],
    exports: [
        ChannelContentTileComponent,
        EpisodeContentTileComponent,
        ShowContentTileComponent,
        CategoryContentTileComponent,
        CollectionContentTileComponent,
        ViewAllTileComponent,
        SelectorTileComponent,
        PillsListComponent,
        PillTileComponent
    ],
    providers: [
        NeriticLinkService,
        CarouselService
    ]
})

export class ContentTilesModule
{
}
