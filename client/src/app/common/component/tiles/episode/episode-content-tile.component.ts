import { BehaviorSubject, combineLatest as observableCombineLatest, Observable, Subject } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import {
    Component,
    Input,
    ViewChild,
    ElementRef,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
    AfterViewInit
} from "@angular/core";
import {
    CarouselTypeConst,
    ITile,
    TileShapeType,
    MediaPlayerService
} from "sxmServices";
import { AutoUnSubscribe } from "../../../decorator/auto-unsubscribe";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import {
    NowPlayingIndicatorService,
    tileContentType
} from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { CarouselService } from "../../../../carousel/carousel.service";
import { TranslateService } from "@ngx-translate/core";
import { LocalizeTileTextPipe } from "../../../pipe/localize-tile-text.pipe";
import { EllipsisStringUtil } from "../../../util/ellipsisString.util";
import { TileUtil } from "../../..//util/tile.util";
import { CMOption } from "../../../../context-menu";
import * as _ from "lodash";
import { BrowserUtil } from "app/common/util/browser.util";

@Component({
    selector: "episode-content-tile",
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: "./episode-content-tile.component.html",
    styleUrls: [ "./episode-content-tile.component.scss" ],
    host: { '(window:resize)': 'this.resizeEvent$.next(event)' },
    providers: [ LocalizeTileTextPipe ]
})

@AutoUnSubscribe()

export class EpisodeContentTileComponent
{

    private _tileData : ITile;
    public backgroundStyle: any = null;
    public lazyLoadUrl: string = null;
    @Input() lazyLoadEvent$?: Subject<boolean>;
    public lazyLoadOnInit$: BehaviorSubject<boolean> = new BehaviorSubject(null);

    @Input() set tileData(tile : ITile)
    {
        this._tileData = tile;
        this.tileShape = this.tileData.tileShape || CarouselTypeConst.SQUARE_TILE_SHAPE;
        const contentType : string = this.tileData.tileContentSubType;

        this.isVodRectangleTile = this.carouselService.isVodEpisode(this._tileData) &&
            this.tileShape !== CarouselTypeConst.SQUARE_TILE_SHAPE;
        this.tileTitleLinesToClamp = (_.get(this as any, "tileData.line3", "").trim().length === 0)
                                     && !this.isVodRectangleTile?
                                     2 : 1;
        this._percentConsumed = tile.percentConsumed;

        this.isUpdating = true;
        observableCombineLatest(this.resizeEvent$.pipe(debounceTime(500)),
            this.tileData.line2$)
                  .subscribe((data : [ any, string ]) =>
                  {
                      this.titleLine.nativeElement.innerHTML = data[ 1 ];
                      EllipsisStringUtil.clamp(this.titleLine.nativeElement,
                          {clamp:this.tileTitleLinesToClamp, useNativeClamp:true});
                      this.isUpdating = false;
                      this.changeDetectorRef.markForCheck();
                  });

        this.resizeEvent$.next(null);
        this.lazyLoadUrl = this.getLazyLoadUrl();
        this.backgroundStyle = this.getBackgroundStyle();


    }

    get tileData() : ITile
    {
        return this._tileData;
    }

    /**
     * Optional tile shape configuration
     */
    @Input() tileShape?: TileShapeType;

    /**
     * If the carouselOrientation is "grid", this input is used add styles as a "grid" tile
     */
    @Input() tileOrientation: string;

    @ViewChild('titleLine') titleLine: ElementRef;

    private resizeEvent$: Subject<any> = new Subject<any>();
    public tileTitleLinesToClamp: number = 2;
    public isUpdating: boolean = false;
    public isVodRectangleTile: boolean = false;

    constructor(public nowPlayingIndicatorService: NowPlayingIndicatorService,
                public carouselService: CarouselService,
                public translate : TranslateService,
                private mediaPlayerService : MediaPlayerService,
                private changeDetectorRef: ChangeDetectorRef,
                private localizeTileTextPipe: LocalizeTileTextPipe)
    {
    }

    ngOnInit()
    {
        this.tileShape = this.tileShape || this.tileData.tileShape || CarouselTypeConst.SQUARE_TILE_SHAPE;
        if(!this.lazyLoadEvent$)
        {
            this.lazyLoadEvent$ = this.lazyLoadOnInit$;
            this.lazyLoadOnInit$.next(true);
        }
    }

    public getContentType(): string
    {
        return this.tileData.primaryNeriticLink.contentType;
    }

    /**
     * Formats episode title with ellipsis based on the window width.
     * @param title
     * @returns {string}
     */
    public formatTitleText(title): string
    {
      let titleText;
      let windowInnerWidth = window.innerWidth;

      titleText = this.localizeTileTextPipe.transform(title);

      return titleText;
    }


    /**
     * gets percentConsumed for the episode
     */
    private _percentConsumed : number = 0;
    public get percentConsumed() : number
    {
        return this._percentConsumed;
    }

    public getLazyLoadUrl(): string
    {
        return this.tileData.bgImageUrl
               ? this.tileData.bgImageUrl + '?width=320&height=240&preserveAspect=true'
               : null;
    }

    public getBackgroundStyle(): object
    {
        const backgroundColor = this.tileData.backgroundColor
                                ? this.tileData.backgroundColor
                                : null;

        const gradient = 'linear-gradient(to bottom, rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0))';

        if(backgroundColor)
        {
            return {
                'background-color': backgroundColor,
                'background-image': gradient
            };
        }
        else
        {
            const fallbackBgUrl = this.tileData.channelInfo && this.tileData.channelInfo.artwork.background.url;
            if(this.tileData.tileAssetInfo.isPandoraPodcast && fallbackBgUrl)
            {
                return {
                    'background-image': `url(${fallbackBgUrl}?width=320&height=240&preserveAspect=true)`
                };
            }
        }
        return {
            'background-image': gradient
        };
    }

    /**
     * Set default background color for tile if image fails to load
     */
    public onTileImageError(err: any)
    {
        if(this.tileData.tileAssetInfo.isPandoraPodcast)
        {
            this.backgroundStyle = TileUtil.getDefaultBackground(this.tileData);
        }
    }
}
