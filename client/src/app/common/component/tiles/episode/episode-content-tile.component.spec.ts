import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { ChannelListServiceMock } from "../../../../../../test/mocks/channel-list.service.mock";
import { ChannelListStoreService } from "../../../service/channel-list.store.service";
import { MockComponent } from "../../../../../../test/mocks/component.mock";
import {
    ChannelLineupService,
    ITile,
    MediaPlayerService
} from "sxmServices";
import { CarouselServiceMock, ChannelLineupServiceMock, RouterStub } from "../../../../../../test/mocks";
import { EpisodeContentTileComponent } from "./episode-content-tile.component";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import { NowPlayingIndicatorService } from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { TileContextMenuOptionsServiceMock } from "../../../../../../test/mocks/tile-context-menu-options.service.mock";
import { NowPlayingIndicatorServiceMock } from "../../../../../../test/mocks/now-playing-indicator.service.mock";
import { CarouselService } from "../../../../carousel/carousel.service";
import { TranslationModule } from "../../../../translate/translation.module";
import { SharedModule } from "../../../shared.module";
import { DoubleTapOpenContextMenu } from "../../../../context-menu/directives/double-tap-open-context-menu.directive";
import { ContextMenuServiceMock } from "../../../../../../test/mocks/context-menu.service.mock";
import { ContextMenuService } from "../../../../context-menu";
import { MediaPlayerServiceMock } from "../../../../../../test/mocks/media-player.service.mock";
import { EllipsisStringUtil } from "../../../util/ellipsisString.util";
import { Observable } from "rxjs";
import { ChangeDetectorRef } from "@angular/core";
import { LazyLoadImageModule } from 'ng-lazyload-image';

describe("EpisodeContentTileComponent()", () =>
{
    let component: EpisodeContentTileComponent,
        fixture: ComponentFixture<EpisodeContentTileComponent>,
        mockMediaPlayerService = new MediaPlayerServiceMock();

    beforeEach(() =>
    {
        spyOn(EllipsisStringUtil, "clamp").and.returnValue({ 'original': "original", 'clamped': "clamped" });
        TestBed.configureTestingModule({
            imports: [
                TranslationModule,
                SharedModule,
                LazyLoadImageModule
            ],
            declarations: [
                EpisodeContentTileComponent,
                DoubleTapOpenContextMenu,
                MockComponent({ selector: "context-menu", inputs: [ "getOptionsFunc", "analytics" ] }),
                MockComponent({ selector: "tile-image", inputs: [ "tileData" ] })
            ],
            providers: [
                { provide: MediaPlayerService, useValue : mockMediaPlayerService },
                { provide: ChannelListStoreService, useValue: ChannelListServiceMock.getSpy() },
                { provide: ChannelLineupService, useClass: ChannelLineupServiceMock },
                { provide: TileContextMenuOptionsService, useClass: TileContextMenuOptionsServiceMock },
                { provide: NowPlayingIndicatorService, useClass: NowPlayingIndicatorServiceMock },
                { provide: CarouselService, useClass: CarouselServiceMock },
                { provide: ChangeDetectorRef, useClass: ChangeDetectorRef },
                { provide: ContextMenuService, useClass: ContextMenuServiceMock }
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(EpisodeContentTileComponent);
        component = fixture.componentInstance;

        component.titleLine = {nativeElement: {innerHtml:"line 2", style:{webkitLineClamp:1}}};
        component.tileData = {
            primaryNeriticLink: {
                contentType: ""
            },
            fgImageUrl: "url",
            bgImageUrl: "url",
            iconImageUrl: "iconImgUrl",
            channelNumber: "Ch 100",
            subtext: "Howard Stern",
            line1: "line 1",
            line1$: Observable.create((observer) => observer.next("line 1")),
            line2: "line 2",
            line2$: Observable.create((observer) => observer.next("line 2")),
            line3: "line 3",
            line3$: Observable.create((observer) => observer.next("line 3")),
            tileBanner: {}
        } as ITile;
        component.tileOrientation = "";
        component.tileShape = "square";

        fixture.detectChanges();
    });
});
