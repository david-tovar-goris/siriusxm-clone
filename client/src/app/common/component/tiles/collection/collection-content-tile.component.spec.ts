import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import {
    ITile
} from "sxmServices";
import { MockComponent } from "../../../../../../test/mocks/component.mock";
import { CollectionContentTileComponent } from "./collection-content-tile.component";
import { Observable } from "rxjs";
import { ChangeDetectorRef } from "@angular/core";
import { LazyLoadImageModule } from 'ng-lazyload-image';

describe('CollectionContentTileComponent', () =>
{
    let component: CollectionContentTileComponent,
        fixture: ComponentFixture<CollectionContentTileComponent>,
        tileData = {
            primaryNeriticLink: { contentType: "", channelId: '' },
            tileAssetInfo: { showGuid: '' },
            neriticLinkData: [{ contentSubType: 'aod' }],
            line1: "line 1",
            line1$: Observable.create((observer) => observer.next("line 1")),
            line2: "line 2",
            line2$: Observable.create((observer) => observer.next("line 2")),
            line3: "line 3",
            line3$: Observable.create((observer) => observer.next("line 3")),
            tileShape : "square",
            fgImageUrl: "url",
            bgImageUrl: "url",
            subCategoryInfo : {channelList:[]},
            tileBanner: {
                bannerClass: "",
                bannerText : "New show",
                bannerColor : ""
            }
        } as ITile;

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports: [
                LazyLoadImageModule
            ],
            declarations: [
                CollectionContentTileComponent,
                MockComponent({ selector: "tile-image", inputs: [ "tileData" ] })
            ],
            providers: [
                { provide: ChangeDetectorRef, useClass: ChangeDetectorRef }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(CollectionContentTileComponent);
        component = fixture.componentInstance;

        component.tileOrientation = "";
        component.titleLine = {nativeElement: {innerHtml:"line 2", style:{webkitLineClamp:1}}};
        component.tileData = tileData;

        fixture.detectChanges();
    });

    describe('component creation', () =>
    {
        it("should exists", () =>
        {
            expect(component).toBeTruthy();
        });

        it('line1 should match line1 value set as tile property', () =>
        {
            expect(component.tileData.line1).toEqual(tileData.line1);
        });
    });
});
