import { combineLatest as observableCombineLatest, Observable, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import {
    Component,
    Input,
    ViewChild,
    ElementRef,
    ChangeDetectorRef,
    ChangeDetectionStrategy
} from "@angular/core";
import { ITile } from "sxmServices";
import { AutoUnSubscribe } from "../../../decorator/auto-unsubscribe";
import * as _ from "lodash";
import { EllipsisStringUtil } from "../../../util/ellipsisString.util";

@Component({
    selector: "collection-content-tile",
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: "./collection-content-tile.component.html",
    host: { '(window:resize)': 'this.resizeEvent$.next(event)' },
    styleUrls: [ "../content-tile.scss" ]
})

@AutoUnSubscribe()
/**
 * Component to handle "collection" tiles (tile.tileContentType).
 */
export class CollectionContentTileComponent
{
    private _tileData : ITile;
    public lazyLoadUrl: string = null;
    @Input() lazyLoadEvent$: Subject<boolean>;
    @Input() set tileData(tile : ITile)
    {
        this._tileData = tile;
        this.tileTitleLinesToClamp = (_.get(this as any, "tileData.line2", "").trim().length === 0) ? 2 : 1;

        this.isUpdating = true;
        observableCombineLatest(this.resizeEvent$.pipe(debounceTime(500)),
            this.tileData.line1$)
                  .subscribe((data : [ any, string ]) =>
                  {
                      this.titleLine.nativeElement.innerHTML = data[ 1 ];
                      EllipsisStringUtil.clamp(this.titleLine.nativeElement,
                          { clamp: this.tileTitleLinesToClamp, useNativeClamp: true });
                      this.isUpdating = false;
                      this.changeDetectorRef.markForCheck();
                  });
        this.resizeEvent$.next(null);
        this.lazyLoadUrl = this.getLazyLoadUrl();
    }

    get tileData() : ITile
    {
        return this._tileData;
    }

    /**
     * If the carouselOrientation is "grid", this input is used add styles as a "grid" tile
     */
    @Input() tileOrientation: string;

    @ViewChild('titleLine') titleLine: ElementRef;

    private resizeEvent$: Subject<any> = new Subject<any>();
    public tileTitleLinesToClamp : number = 2;
    public isUpdating : boolean = false;

    constructor(private changeDetectorRef: ChangeDetectorRef){}

    public getLazyLoadUrl(): string
    {
        return this.tileData.bgImageUrl
               ? this.tileData.bgImageUrl + '?width=320&height=240&preserveAspect=true'
               : null;
    }

}
