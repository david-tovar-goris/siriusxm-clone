import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PillTileComponent } from './pill-tile.component';
import {CarouselService} from "app/carousel/carousel.service";
import {CarouselServiceMock} from "../../../../../../test/mocks";
import {TranslationModule} from "app/translate/translation.module";
import {ITile} from "sxmServices";

describe('PillTileComponent', () =>
{
  let component: PillTileComponent;
  let fixture: ComponentFixture<PillTileComponent>;

  beforeEach(async(() =>
  {
    TestBed.configureTestingModule({
      imports: [
            TranslationModule],
      declarations: [ PillTileComponent ],
      providers : [
            { provide: CarouselService, useValue: CarouselServiceMock.getSpy() }
        ]
    })
    .compileComponents();
      PillTileComponent.prototype.tileData = {
          primaryNeriticLink: { contentType: "", channelId: '' },
          tileAssetInfo     : { showGuid: '', aodEpisodecaId: "testGuid" },
          neriticLinkData   : [ { contentSubType: 'aod' } ],
          fgImageUrl        : "url",
          bgImageUrl        : "url",
          tileContentSubType: "channel",
          channelInfo       :
              {}
      } as ITile;

  }));

  beforeEach(() =>
  {
    fixture = TestBed.createComponent(PillTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () =>
  {
    expect(component).toBeTruthy();
  });
});
