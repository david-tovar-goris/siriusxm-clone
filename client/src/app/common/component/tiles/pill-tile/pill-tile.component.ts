import {Component, Input, OnInit} from '@angular/core';
import { CarouselService } from "app/carousel/carousel.service";

@Component({
  selector: 'pill-tile',
  templateUrl: './pill-tile.component.html',
  styleUrls: ['./pill-tile.component.scss']
})
export class PillTileComponent implements OnInit {

  @Input() tileData;
  constructor(private carouselService: CarouselService) { }

  ngOnInit()
  {}

}
