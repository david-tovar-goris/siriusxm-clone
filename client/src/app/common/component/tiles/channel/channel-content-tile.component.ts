import { BehaviorSubject, combineLatest as observableCombineLatest, Observable, Subject } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import * as _ from "lodash";

import {
    ChangeDetectionStrategy,
    Component,
    Input,
    ViewChild,
    ChangeDetectorRef,
    ElementRef,
    OnInit, AfterViewInit
} from "@angular/core";
import { CarouselTypeConst, ConfigService, ITile, MediaUtil, MediaPlayerService } from "sxmServices";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import { NowPlayingIndicatorService, tileContentType } from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { TranslateService } from "@ngx-translate/core";
import { EllipsisStringUtil } from "../../../util/ellipsisString.util";
import { CMOption } from "../../../../context-menu/context-menu.interface";

@Component({
               selector       : "channel-content-tile",
               changeDetection: ChangeDetectionStrategy.OnPush,
               templateUrl    : "./channel-content-tile.component.html",
               host           : { '(window:resize)': 'this.resizeEvent$.next(event)' },
               styleUrls      : [ "../content-tile.scss" ]
           })

export class ChannelContentTileComponent implements OnInit
{
    /**
     * Tile data used to populate the view
     */
    private _tileData: ITile; // backing store

    public isUpdating: boolean = false;

    public lazyLoadUrl: string = null;

    public backgroundStyle: any = null;

    @Input() lazyLoadEvent$: Subject<boolean>;
    public lazyLoadOnInit$: BehaviorSubject<boolean> = new BehaviorSubject(null);
    @Input() set tileData(tile : ITile)
    {
        this._tileData = tile;
        this.isAdditionalChannel   = (this.tileData.tileContentSubType === CarouselTypeConst.ADDITIONAL_CHANNEL);

        const contentType : string = MediaUtil.isMultiTrackAudioMediaType(this.tileData.tileContentSubType)
                                     ? this.tileData.tileContentSubType
                                     : this.tileData.tileContentType;

        if (contentType === CarouselTypeConst.SEEDED_RADIO)
        {
            this.tileTitleLinesToClamp = 1;
        }
        else
        {
            this.tileTitleLinesToClamp = (this.isAdditionalChannel
                || _.get(this as any, "tileData.line3", "").trim().length === 0) ? 2 : 1;
        }

        this.isUpdating = true;
        observableCombineLatest(this.resizeEvent$.pipe(debounceTime(500)),
                                 this.tileData.line2$)
                  .subscribe((data : [ any, string ]) =>
                             {
                                 this.titleLine.nativeElement.innerHtml = '<div>' + data[ 1 ] + '</div>';

                                 EllipsisStringUtil.clamp(this.titleLine.nativeElement,
                                                          { clamp: this.tileTitleLinesToClamp, useNativeClamp: true });
                                 this.isUpdating = false;
                                 this.changeDetectorRef.markForCheck();
                             });

        this.resizeEvent$.next(null);

        this.lazyLoadUrl = this.getLazyLoadUrl();
        this.backgroundStyle = this.getBackgroundStyle();
    }

    get tileData() : ITile
    {
        return this._tileData;
    }

    /**
     * If the carouselOrientation is "grid", this input is used add styles as a "grid" tile
     */
    @Input() tileOrientation : string;

    public isAdditionalChannel : boolean = false;
    public liveBanner : string           = CarouselTypeConst.ON_WATCH_LIVE_BANNER_CLASS;

    /**
     * Element reference to the secondLineEllipse
     */
    @ViewChild('titleLine') titleLine : ElementRef;

    private resizeEvent$ : Subject<any>   = new Subject<any>();
    public tileTitleLinesToClamp : number = 2;

    constructor(private mediaPlayerService : MediaPlayerService,
                public nowPlayingIndicatorService : NowPlayingIndicatorService,
                public configService : ConfigService,
                private changeDetectorRef: ChangeDetectorRef,
                public translate : TranslateService)
    {}

    ngOnInit()
    {
        if(!this.lazyLoadEvent$)
        {
            this.lazyLoadEvent$ = this.lazyLoadOnInit$;
            this.lazyLoadOnInit$.next(true);
        }
    }

    public getLazyLoadUrl(): string
    {
        return this.tileData.bgImageUrl
            ? this.tileData.bgImageUrl + '?width=320&height=240&preserveAspect=true'
            : null;
    }


    public getBackgroundStyle(): object
    {
        const backgroundColor = this.tileData.backgroundColor
            ? this.tileData.backgroundColor
            : null;

        return {
            'background-color': backgroundColor,
            'background-image': 'linear-gradient(to bottom, rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0))'
        };
    }
}
