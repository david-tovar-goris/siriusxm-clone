import {
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { MockComponent } from "../../../../../../test/mocks/component.mock";
import {ConfigServiceMock} from "../../../../../../test/mocks/config.service.mock";
import { ChannelContentTileComponent } from "./channel-content-tile.component";
import {
    ITileReminders,
    ITile,
    ConfigService
} from "sxmServices";
import { TileContextMenuOptionsService } from "../../../../context-menu/options/tile-context-menu-options.service";
import { NowPlayingIndicatorService } from "../../../service/now-playing-indicator/now-playing-indicator.service";
import { NowPlayingIndicatorServiceMock } from "../../../../../../test/mocks/now-playing-indicator.service.mock";
import { TileContextMenuOptionsServiceMock } from "../../../../../../test/mocks/tile-context-menu-options.service.mock";
import { DoubleTapOpenContextMenu } from "../../../../context-menu/directives/double-tap-open-context-menu.directive";
import { ContextMenuService }       from "../../../../context-menu";
import { ContextMenuServiceMock }   from "../../../../../../test/mocks/context-menu.service.mock";
import { TranslationModule }        from "../../../../translate/translation.module";
import { MockTranslateService }     from "../../../../../../test/mocks/translate.service";
import { TranslateService }         from "@ngx-translate/core";
import { MediaPlayerServiceMock }   from "../../../../../../test/mocks/media-player.service.mock";
import { MediaPlayerService } from "sxmServices";
import { EllipsisStringUtil } from "../../../util/ellipsisString.util";
import { Observable } from "rxjs";
import { ChangeDetectorRef } from "@angular/core";
import { LazyLoadImageModule } from 'ng-lazyload-image';

describe("ChannelContentTileComponent()", () =>
{
    let component: ChannelContentTileComponent,
        fixture: ComponentFixture<ChannelContentTileComponent>,
        mockMediaPlayerService = new MediaPlayerServiceMock();


    beforeEach(() =>
    {
        spyOn(EllipsisStringUtil, "clamp").and.returnValue({ 'original': "original", 'clamped': "clamped" });
        TestBed.configureTestingModule({
            imports : [TranslationModule, LazyLoadImageModule],
            declarations: [
                ChannelContentTileComponent,
                DoubleTapOpenContextMenu,
                MockComponent({ selector: "context-menu", inputs: [ "getOptionsFunc", "analytics" ] }),
                MockComponent({ selector: "tile-image", inputs: [ "tileData" ] })
            ],
            providers: [
                { provide: MediaPlayerService, useValue : mockMediaPlayerService },
                { provide: TileContextMenuOptionsService, useClass: TileContextMenuOptionsServiceMock },
                { provide: NowPlayingIndicatorService, useClass: NowPlayingIndicatorServiceMock },
                { provide: ConfigService, useValue: ConfigServiceMock },
                { provide: ContextMenuService, useClass: ContextMenuServiceMock },
                { provide: ChangeDetectorRef, useClass: ChangeDetectorRef },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() }
            ]
        }).compileComponents();
    });

    beforeEach(() =>
    {
        fixture = TestBed.createComponent(ChannelContentTileComponent);
        component = fixture.componentInstance;

        component.tileOrientation ="";
        component.titleLine = {nativeElement: {innerHtml:"line 2", style:{webkitLineClamp:1}}};
        component.tileData = {
            primaryNeriticLink: {
                channelId: "howard100"
            },
            fgImageUrl: "url",
            bgImageUrl: "url",
            reminders: {showReminderSet: false, liveVideoReminderSet: false } as ITileReminders,
            line1: "line 1",
            line1$: Observable.create((observer) => observer.next("line 1")),
            tileAssets : [],
            line2: "line 2",
            line2$: Observable.create((observer) => observer.next("line 2")),
            line3: "line 3",
            line3$: Observable.create((observer) => observer.next("line 3")),
            tileContentSubType: "channel",
            images: [],
            channelNumber: "Ch 100",
            subtext: "Howard Stern",
            tileBanner: {
                bannerClass: "",
                bannerText : "New Episode",
                bannerColor : ""
            },
            channelInfo: {
                playingArtist: "playingArtist",
                playingTitle: "playingTitle"
            }
        } as ITile;

        fixture.detectChanges();
    });

    describe("component creation", () =>
    {
        it("should exists", () =>
        {
            expect(component).toBeTruthy();
        });
    });
});
