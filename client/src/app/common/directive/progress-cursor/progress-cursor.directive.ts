import { Directive, ElementRef } from "@angular/core";
import { ProgressCursorService } from "../../service/progress-cursor/progress-cursor.service";
import { AutoUnSubscribe } from "../../decorator";
import { SubscriptionLike as ISubscription } from "rxjs";

@Directive({
    selector: '[progressCursor]'
})

@AutoUnSubscribe([])
export class ProgressCursorDirective
{
    private subscriptions: ISubscription[] = [];

    constructor(private elem: ElementRef,
                public progressCursorService: ProgressCursorService)
    {
        this.subscriptions.push(
            this.progressCursorService.isSpinning.subscribe((isSpinning) =>
            {
                if (isSpinning) return this.startAnimation();
                this.stopAnimation();
            })
        );
    }

    startAnimation()
    {
        this.elem.nativeElement.style.cursor = "progress";
    }

    stopAnimation()
    {
        this.elem.nativeElement.style.cursor = null;
    }
}
