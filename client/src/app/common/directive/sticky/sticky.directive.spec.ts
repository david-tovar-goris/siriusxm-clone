import { Component } from "@angular/core";
import { By } from "@angular/platform-browser";
import {
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import { StickyDirective } from "./sticky.directive";
import { StickyService } from "../../service/sticky/sticky.service";

describe('StickyDirective', function()
{
    beforeEach(function()
    {
        this.fixture = TestBed.configureTestingModule({
            declarations: [StickyDirective, StickyTestComponent],
            providers: [StickyService]
        })
        .createComponent(StickyTestComponent);

        window.scrollTo(0, 0);
        this.fixture.detectChanges();
        this.debugElement = this.fixture.debugElement;
    });

    it('there are 3 sticky directives', function()
    {
        const directives = this.debugElement.queryAll(By.directive(StickyDirective));
        expect(directives.length).toBe(3);
    });

    it('when alwaysSticky true, starts out sticky', function()
    {
        const debugElement = this.debugElement.query(By.css('#three'));
        expect(debugElement.classes['sticky-bar']).toBe(true);
    });

    it('when alwaysSticky false, does not start out sticky', function()
    {
        const debugElement = this.debugElement.query(By.css('#one'));
        expect(debugElement.classes['sticky-bar']).not.toBe(true);
    });

    it('adds sticky class when scrolled down far enough', function()
    {
        window.scrollTo(0, 0);
        window.scrollTo(0, document.body.scrollHeight);
        window.dispatchEvent(new Event('scroll'));
        this.fixture.detectChanges();
        const debugElement = this.fixture.debugElement.query(By.css('#one'));
        expect(debugElement.classes['sticky-bar']).toBe(true);
    });
});


// shell test Component to test the Directive
@Component({
    template: `
        <div id="container"
             style="height:10000px;">
            <div id="one"
                 sxm-sticky
                 [stickyClass]="'sticky-bar'">
                Sticky Header 1
            </div>
            <div id="two">
                Sticky Header 2
            </div>
            <div id="three"
                 sxm-sticky
                 [stickyClass]="'sticky-bar'"
                 [alwaysSticky]="true">
                Sticky Header 3
            </div>
            <div id="four"
                 sxm-sticky
                 [stickyClass]="'sticky-bar'">
                Sticky Header 4
            </div>
        </div>
    `
})

class StickyTestComponent
{
}
