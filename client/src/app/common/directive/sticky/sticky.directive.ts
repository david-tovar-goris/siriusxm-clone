import {
    Directive,
    Input,
    Renderer2,
    ElementRef,
    OnInit,
    AfterViewInit,
    OnDestroy
} from '@angular/core';
import { StickyService } from "./../../service/sticky/sticky.service";
import { SubscriptionLike as ISubscription, Observable, fromEvent } from "rxjs";
import { AutoUnSubscribe } from "../../decorator/auto-unsubscribe";
import { DEFAULT_STICKY_HEIGHT } from '../../service/sticky/sticky.const';

/**
 * @MODULE:     client
 * @CREATED:    08/20/17
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     StickyDirective used to stick the elements to the top position
 */

@Directive({
    selector: '[sxm-sticky]'
})

@AutoUnSubscribe([])
export class StickyDirective implements OnInit, AfterViewInit, OnDestroy
{

    /**
     * Holding the last scroll position of the screen
     */
    private prevPageYOffset: number = 0;

    /**
     * Holds the Initial position of the element along with the height.
     */
    private originalPosition: number = 0;

    /**
     * Store boolean to check whether the current element is sticky or not
     */
    public isSticky: boolean = false;


    /**
     * Reference to the stickyHeight/scroll subscriptions- used to un-subscribe on destroy.
     */
    private subscriptions: Array<ISubscription> = [];


    /**
     * Holds the value of top position if it has to be sticky.
     */
    private stickyHeight: number = 0;


    /**
     * Input property  used to add/remove Class to the Sticky element.
     */
    @Input('stickyClass') stickyClass: string;

    @Input() alwaysSticky?: boolean = false;

    constructor(public elementRef: ElementRef,
                private renderer: Renderer2,
                private stickyService: StickyService)
    {

    }

    /**
     * Subscribes to scroll event and Total height of Sticky elements of window.
     */
    ngOnInit(): void
    {
        this.stickyService.registerStickyable(this);

        let scrollSubscription = fromEvent(window, 'scroll').subscribe(() => this.onScroll());
        let resizeSubscription = fromEvent(window, 'resize').subscribe(() => this.onResize());

        this.subscriptions.push(scrollSubscription);
        this.subscriptions.push(resizeSubscription);
    }

    ngAfterViewInit()
    {
        const elementPosition = this.getElementPosition(this.elementRef.nativeElement);
        this.originalPosition = elementPosition.height + elementPosition.top;
        this.stickyHeight = this.stickyService.getOffset(this);
        this.compute();
    }

    /**
     * When this component is destroyed, remove the subscription of the stickyHeight
     * and also update stickyHeight in the service if the current element is sticky.
     */
    ngOnDestroy(): void
    {
        this.stickyService.unregisterStickyable(this);
    }

    /**
     * This is the main computation method. Called on scroll
     */
    public compute(): void
    {
        const pageYOffset = window.pageYOffset;

        if (pageYOffset < 0) return;

        const elementPosition = this.getElementPosition(this.elementRef.nativeElement);


        //original position(originalPosition) of the element need to determined when no elements has been stickied.
        //When we directly land on the category screen, loading the hero carousels  has been delayed
        // which causes the storing of wrong original element position during ngAfterViewInit. we are fixing it as mentioned below
        // if we ever find the originalPosition value to be less than (elementPosition.height + elementPosition.top)
        // we are upding with the new value
        if(this.originalPosition < elementPosition.height + elementPosition.top)
        {

            this.originalPosition = elementPosition.height + elementPosition.top;
        }


        let makeElementStick, makeElementUnStick;
        if (this.alwaysSticky)
        {
            makeElementStick = true;
            makeElementUnStick = false;
        }
        else
        {
            makeElementStick = this.checkElementEligibleForStick(pageYOffset, elementPosition);
            makeElementUnStick = this.checkElementEligibleForUnStick(pageYOffset, elementPosition);
        }

        this.stickyHeight = this.stickyService.getOffset(this);


        if (makeElementStick)
        {
            this.setSticky();
            this.updateStickyHeightSum();
        }
        else if (makeElementUnStick)
        {
            this.unsetSticky();
            this.updateStickyHeightSum();
        }
        this.prevPageYOffset = pageYOffset;
    }

    /**
     * On scroll of window.
     */
    public onScroll(): void
    {
        this.compute();
    }

    private onResize()
    {
        let elementPosition = this.getElementPosition(this.elementRef.nativeElement);
        this.originalPosition = elementPosition.height + elementPosition.top;

        if(this.isSticky)
        {
            this.stickyHeight = this.stickyService.getOffset(this);
            this.setStyle('top', this.stickyHeight + 'px');
        }
        this.updateStickyHeightSum();
    }

    /**
     * Checks whether element is eligible for Stick
     * isScrollingDown--- checks- Is user scrolling down or not
     * !this.isSticky---  checks the element is not already stick
     * isElementBelow --if element reaches max top position (i.e) this.stickyHeight - then stick the element.
     * @param {scrollPosition, elementPosition}
     * @returns {boolean}
     */
    private checkElementEligibleForStick(scrollPosition, elementPosition): boolean
    {
        let isScrollingDown = scrollPosition > this.prevPageYOffset;
        let isElementBelowStickyHeight = elementPosition.top <= this.stickyHeight;

        return isScrollingDown && !this.isSticky && isElementBelowStickyHeight;
    }

    /**
     * Checks whether element is eligible for Stick
     * isScrollingTop --  checks- Is user scrolling up or not -> UnStick applied only if screen scrolls up
     * this.isSticky -- Unstick applied if the element is already stick
     * (scrollPosition <= elementPosition.top) && (scrollPosition + elementPosition.height) <= this.originalPosition
     * --if there are no elements- while scrolling up ->unstick last stick element
     *
     * @param {scrollPosition, elementPosition}
     * @returns {boolean}
     */
    private checkElementEligibleForUnStick(scrollPosition, elementPosition): boolean
    {

        let isScrollingTop = scrollPosition < this.prevPageYOffset;
        let isElementAboveScrollPosition = scrollPosition <= this.originalPosition - (elementPosition.top + elementPosition.height);
        let isElementAboveOriginalPosition = scrollPosition + elementPosition.height <= this.originalPosition;

        return isScrollingTop && this.isSticky && isElementAboveScrollPosition && isElementAboveOriginalPosition;
    }

    /**
     * update the StickyHeight for the app
     */
    private updateStickyHeightSum(): void
    {
        this.stickyService.emitFullHeight();
    }

    /**
     * Sets the current element Sticky.
     */
    private setSticky(): void
    {
        this.isSticky = true;
        this.setStyle('top', this.stickyHeight + 'px');
        this.setClass(true);
    }

    /**
     * Removes the Sticky from the current element.
     */
    private unsetSticky(): void
    {
        if (this.alwaysSticky)
        {
            return;
        }
        this.isSticky = false;
        this.setStyle('top', '');
        this.setClass(false);
    }

    /**
     * Sets a new style value to given CSS keys/property
     * @param {key} property - CSS key.
     * @param {value} Value - updated Value for the above Key.
     */
    private setStyle(key: string, value: string): void
    {
        this.renderer.setStyle(this.elementRef.nativeElement, key, value);
    }

    /**
     * Add or Remove the Class to the element based on the Sticky or unsticky
     * @param {add} Boolean check to add or remove the class
     */
    private setClass(add: boolean): void
    {
        if (add)
        {
            this.renderer.addClass(this.elementRef.nativeElement, this.stickyClass);
        }
        else
        {
            this.renderer.removeClass(this.elementRef.nativeElement, this.stickyClass);
        }

    }

    /**
     * Add or Remove the Class to the element based on the Sticky or unsticky
     * @param {element} Pass the native element
     * @return {element position, height, weight}
     */
    private getElementPosition(element): any
    {
        return element.getBoundingClientRect();
    }
}
