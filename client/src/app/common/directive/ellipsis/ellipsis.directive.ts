import { AfterViewInit, Directive, ElementRef, Input, OnDestroy } from "@angular/core";

@Directive({
    selector: '[ellipsis]'
})

/*
* Note: For this directive to work, element should have below CSS
* as it works by calculating no of lines to not exceed element's max-height/ height
*
* display: inline-block (recommended), block or flex element
* max-height property should be set;
* white-space: normal;
* */
export class EllipsisDirective implements AfterViewInit, OnDestroy
{
    @Input('ellipsis') ellipsis: string;

    private line: string;

    constructor(private el: ElementRef)
    {
    }

    ngOnInit()
    {
        this.line = this.ellipsis;
        this.line = (this.line) ? this.line.replace(/(\r\n\t|\n|\r\t)/gm," ") : "";
        this.line = (this.line) ? this.line.replace(/\s\s+/g, ' ') : "";
        window.addEventListener('resize', this.ellipse);
        window.addEventListener('scroll', this.ellipse);
    }

    ngAfterViewInit(): void
    {
        this.ellipse();
    }

    ngOnDestroy(): void
    {
        window.removeEventListener('resize', this.ellipse);
        window.removeEventListener('scroll', this.ellipse);
    }

    private ellipse = () =>
    {
        let finalDescription                      = this.line;
//        this.line                      = finalDescription;
        this.el.nativeElement.innerHTML = this.line;
        let elementHeight                         = this.el.nativeElement.offsetHeight;

        // If the text already fits then we can just return
        if (elementHeight >= this.el.nativeElement.scrollHeight) { return; }

        // Otherwise, we need to perform a divide and conquer search in order to find text that fits, and then
        // put an ellipsis (three periods) after the text to show that it has been truncated
        let startLength = 0;
        let endLength   = finalDescription.length;

        do
        {
            // NOTE : When dealing with reflow, we use 6 w's instead of 3 .'s to reserve enough space for the
            // ellipsis that we will be adding to the end of the text in the UI

            this.line                      = finalDescription;
            this.el.nativeElement.innerHTML = this.line + ' WWWWWW';
            elementHeight                             = this.el.nativeElement.offsetHeight;

            // Divide and conquer search.
            // Between the start length and the end length, we keep moving down by 50% until we find a length
            // that fits
            while (elementHeight < this.el.nativeElement.scrollHeight)
            {
                const newLength                           = startLength + ((endLength - startLength) / 2);
                finalDescription                          = this.line;

                this.line                      = this.line.substring(0, newLength);
                this.el.nativeElement.innerHTML = this.line + ' WWWWWW';
                endLength                                 = this.line.length;
            }

            // Make start and end length "tighter" to reduce the search space.
            startLength = endLength;
            endLength   = finalDescription.length;

            // And go around again until we have narrowed down the search space to cover one size that is the
            // largest size that still fits.

        } while (endLength > startLength + 1);

        // Take the text that we have narrowed down to and add the 3 .'s to show that the text has been truncated
       // this.el.nativeElement.innerHTML = this.line + ' ...';
        this.line = this.line + ' ...';
        this.el.nativeElement.innerHTML = this.line;
    }
}
