import { Directive, Input } from "@angular/core";
import { AppMonitorService, ContentTypes, AppErrorCodes } from "sxmServices";


@Directive({
    selector: "img[download-failed]",
    host: {
        "(error)": "reportError()",
        "[src]": "src",
        "[class]": "class"
    }
})
export class ImageDownloadFailed {

    @Input('download-failed') downloadFailed: {
        contentType: string,
        channelLogo?: string,
        fallbackAlbumImageUrl?: string,
        defaultImgClass?: string,
        fallbackClass?: string
    };

    private _src: string;

    @Input() set src (newSrc: string)
    {
        this._src = newSrc;
        this.class = this.downloadFailed.defaultImgClass;
    }

    get src()
    {
        return this._src;
    }

    @Input() class: string;

    constructor(private appMonitorService: AppMonitorService)
    { }

    reportError()
    {
        switch (this.downloadFailed.contentType)
        {
            case ContentTypes.AOD:
                this.appMonitorService.triggerFaultError({
                    faultCode: AppErrorCodes.FLTT_AOD_IMAGE_DOWNLOAD_FAILED,
                    url: this.src
                });
                break;
            case ContentTypes.ADDITIONAL_CHANNELS:
                this.src = this.downloadFailed.channelLogo;
                this.class = "channel-logo-image";
                break;
            case ContentTypes.SEEDED_RADIO:
                this.src = this.downloadFailed.fallbackAlbumImageUrl;
                if (this.downloadFailed.fallbackClass && this.class.indexOf(this.downloadFailed.fallbackClass) === -1)
                {
                    this.class += " " + this.downloadFailed.fallbackClass;
                }
                break;
        }
    }
}
