import { IFavoriteItem } from "sxmServices";
import * as FavoriteListActions from "../action";
import { favoriteListReducer } from "./favorite-list.reducer";

describe("favoriteListReducer", () =>
{
    let initialState;

    beforeEach(() =>
    {
        initialState = {
            favorites: {
                channels: [],
                shows: [],
                episodes: []
            }
        };
    });

    afterEach(() =>
    {
        initialState = null;
    });

    describe("When the action type is not handled by the reducer", () =>
    {
        it("should return the the provided state", () =>
        {
            expect(favoriteListReducer(initialState,
            { type: "Random Value", payload: {channels: [], shows: [], episodes: []} })).toEqual(initialState);
        });
    });

    describe("When the action type is FAVORITES_LOADED", () =>
    {
        it("should update the store's favorite list to contain the action payload", () =>
        {
            let newFavList = {
                channels: [],
                shows: [],
                episodes: []
            };

            expect(
                favoriteListReducer(initialState, { type: FavoriteListActions.FAVORITES_LOADED, payload: newFavList }).favorites
            ).toEqual(newFavList);
        });
    });
});
