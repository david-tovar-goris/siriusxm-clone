import { IRecentlyPlayed } from "sxmServices";
import * as RecentlyPlayedActions from "../action";
import { recentlyPlayedListReducer } from "./recently-played-list.reducer";

describe("recentlyPlayedListReducer", () =>
{
    let initialState;

    beforeEach(() =>
    {
        initialState = {
            recentlyPlayedList: []
        };
    });

    afterEach(() =>
    {
        initialState = null;
    });

    describe("when the action type is unknown", () =>
    {
        it("defaults and returns the given state", () =>
        {
            expect(recentlyPlayedListReducer(initialState, { type: "unknown", payload: [] })).toEqual(initialState);
        });
    });

    describe("When the action type is LOAD_RECENTLY_PLAYED_LIST", () =>
    {
        it("should load a list of recently played items", () =>
        {
            let newRecentlyPlayedList = [ {} as IRecentlyPlayed ];

            expect(
                recentlyPlayedListReducer(initialState, {
                    type: RecentlyPlayedActions.LOAD_RECENTLY_PLAYED_LIST,
                    payload: newRecentlyPlayedList
                }).recentlyPlayedList
            ).toEqual(newRecentlyPlayedList);
        });
    });
});
