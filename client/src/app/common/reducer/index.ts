import { ActionReducerMap } from "@ngrx/store";
import { channelsReducer } from "./channel-list.reducer";
import { favoriteListReducer } from "./favorite-list.reducer";
import { recentlyPlayedListReducer } from "./recently-played-list.reducer";

export const rootReducer: ActionReducerMap<any> = {
    channelsReducer,
    favoriteListReducer,
    recentlyPlayedListReducer
};
