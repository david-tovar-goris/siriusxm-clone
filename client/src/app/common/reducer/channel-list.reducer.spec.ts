/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />

import {
    IOnDemandShow,
    IChannel,
    ISubCategory,
    ISuperCategory
} from "sxmServices";
import {
    createCategories,
    createCategory,
    createChannel,
    createChannels,
    createSuperCategories,
    createSuperCategory
} from "../../../../test/mocks/channel-list.service.mock";
import { IAction } from "../action";
import * as ChannelList from "../action/channel-list.action";
import { channelsReducer as reducer } from "./channel-list.reducer";
import { ISelectedCategory } from "../store/channel-list.store";
import {displayViewType} from "../../channel-list/component/display.view.type";

describe("Channel List Reducer Test Suite >>", () =>
{
    let initialState: any = null;

    beforeEach(() =>
    {
        initialState = {
            superCategories: [],
            selectedSuperCategory: {},
            categories: [],
            selectedCategory: {
                category: {} as ISubCategory
            } as ISelectedCategory,
            channels: [],
            liveChannels: [],
            selectedChannel: {} ,
            nextChannel: {},
            prevChannel: {},
            filtered: [],
            filter: "",
            sort: ""
        };
    });

    afterEach(() =>
    {
        initialState = null;
    });

    describe("Infrastructure >>", () =>
    {
        it("Should have a defined test subject.", () =>
        {
            expect(reducer).toBeDefined();
        });

    });

    describe("Actions >>", () =>
    {
        it("Should return the initial default state when no action is provided.", () =>
        {
            expect(reducer(null, null)).toBeDefined();
            expect(reducer(null, null)).toEqual(initialState);
        });

        it("Should return the initial default state when the action type is unknown.", () =>
        {
            expect(reducer(null, null)).toEqual(initialState);
        });

        it("Should load a list of super categories into the store.", () =>
        {
            const superCategories: Array<ISuperCategory> = createSuperCategories(10);
            const action: IAction = new ChannelList.LoadSuperCategories(superCategories);
            const newState = reducer(null, action);
            initialState.superCategories = superCategories;

            expect(newState).toEqual(initialState);
            expect(newState.superCategories).toEqual(superCategories);
        });

        it("Should select a super category and populate the list of categories from that selected super category in the store.", () =>
        {
            // Create a known ID to use when finding the selected item.
            const id: string = "12345";

            // Create a super category with the expected ID.
            const superCategory: ISuperCategory = createSuperCategory();
            superCategory.categoryGuid = id;

            // Pass the selected super category to the Redux action as the payload.
            const action: IAction = new ChannelList.SelectSuperCategory(superCategory);

            // Create a list of super categories with the expected ID on the first item.
            initialState.superCategories = createSuperCategories();
            initialState.superCategories[ 0 ] = superCategory;

            // Execute the reducer function.
            const newState = reducer(initialState, action);

            expect(newState.selectedSuperCategory).toEqual(superCategory);
            expect(newState.categories).toEqual(superCategory.categoryList);
        });

        it("Should select a category and populate the list of channels from that selected category in the store.", () =>
        {
            // Create a known ID to use when finding the selected item.
            const id: string = "12345";

            // Create a category with the expected ID.
            const selectedCategory: ISelectedCategory = {
                category : createCategory(),
                viewType: displayViewType.channels
            };
            selectedCategory.category.categoryGuid = id;
            // Pass the selected category to the Redux action as the payload.
            const action: IAction = new ChannelList.SelectCategory(selectedCategory);

            // Create a list of categories with the expected ID on the first item.
            initialState.categories = createCategories();
            initialState.categories[ 0 ] = selectedCategory.category;

            // Execute the reducer function.
            const newState = reducer(initialState, action);

            expect(newState.selectedCategory).toEqual(selectedCategory);
            expect(newState.channels).toEqual(selectedCategory.category.channelList);
            expect(newState.filtered.length).toEqual(selectedCategory.category.channelList.length);
        });

        it("Should reset the filter to the default when selecting a new category.", () =>
        {
            // Create a filter that was the last state's filter.
            const filter: string = "filter";

            // Create a known ID to use when finding the selected item.
            const id: string = "12345";

            // Create a category with the expected ID.
             const selectedCategory: ISelectedCategory = {
                category : createCategory(),
                viewType: displayViewType.channels
            };
            selectedCategory.category.categoryGuid = id;

            // Pass the selected category to the Redux action as the payload.
            const action: IAction = new ChannelList.SelectCategory(selectedCategory);

            // Create a list of categories with the expected ID on the first item.
            initialState.filter = filter;
            initialState.categories = createCategories();
            initialState.categories[ 0 ] = selectedCategory.category;

            // Execute the reducer function.
            const newState = reducer(initialState, action);

            expect(newState.filter).not.toEqual(filter);
            expect(newState.filter).toEqual("");
        });

        it("Should select a channel in the store.", () =>
        {
            // Create a known ID to use when finding the selected item.
            const id: string = "12345";

            // Create a channel with the expected ID.
            const channel: IChannel = createChannel();
            channel.channelId = id;

            // Pass the selected channel to the Redux action as the payload.
            const action: IAction = new ChannelList.SelectChannel(channel);

            // Create a list of channels with the expected ID on the first item.
            initialState.channels = createChannels();
            initialState.liveChannels = createChannels();
            initialState.channels[ 0 ] = channel;

            // Execute the reducer function.
            const newState = reducer(initialState, action);

            expect(newState.selectedChannel).toEqual(channel);
        });

        it("Should filter the channel list by name in the store.", () =>
        {
            const filter: string = "foo";
            const notFilter: string = "not-filter";
            const channels: Array<IChannel> = createChannels(2, notFilter);
            const action: IAction = new ChannelList.FilterChannelsByName(filter);
            initialState.channels = channels;
            const firstItem: IChannel = channels[ 0 ];
            firstItem.name = filter;
            const newState = reducer(initialState, action);

            expect(newState.filtered.length).toEqual(1);
            expect(newState.filtered[ 0 ].name.toLowerCase()).toContain(filter.toLowerCase());
        });

        it("Should filter the channel list by number in the store.", () =>
        {
            const filter: string = "10";
            const notFilter: string = null;
            const channels: Array<IChannel> = createChannels(2);
            let action: IAction = new ChannelList.FilterChannelsByNumber(filter);
            initialState.channels = channels;
            const firstItem: IChannel = channels[ 0 ];
            firstItem.channelNumber = parseInt(filter);
            const newState = reducer(initialState, action);

            // Make sure the original list of channels wasn't tainted.
            expect(newState.channels.length).toEqual(2);

            // Expect the filtered list to only contain the one channel that has the expected number filter.
            expect(newState.filtered.length).toEqual(1);

            // Expect the channel number of the only item in the filtered list to equal the number filter.
            expect((newState.filtered[ 0 ] as IChannel).channelNumber).toEqual(parseInt(filter));
        });

        it("Should filter the channel list by show name in the store.", () =>
        {
            const filter: string = "medium1";
            const notFilter: number = 11;
            const channels: Array<IChannel> = createChannels(2, 2, 1);
            const action: IAction = new ChannelList.FilterChannelsByShowName(filter);
            initialState.channels = channels;
            const firstItem: IChannel = channels[ 0 ];
            firstItem.shows[ 0 ].showDescription[ "longTitle" ] = filter;
            const newState = reducer(initialState, action);

            // Make sure the original list of channels wasn't tainted.
            expect(newState.channels.length).toEqual(2);

            // Expect the filtered list to only contain the one channel that has the expected number filter.
            expect(newState.filtered.length).toEqual(1);

            // Expect the channel number of the only item in the filtered list to equal the number filter.
            expect((newState.filtered[ 0 ].shows[ 0 ] as IOnDemandShow).showDescription[ "longTitle" ]).toEqual(filter);
        });

        it("Should load live channels in the store.", () =>
        {
            // Create a channels .
            const channels: Array<IChannel> = createChannels();

            // Pass the selected channels to the Redux action as the payload.
            const action: IAction = new ChannelList.LoadLiveChannels(channels);

            // Execute the reducer function.
            const newState = reducer(initialState, action);

            expect(newState.liveChannels).toEqual(channels);
        });
    });
});
