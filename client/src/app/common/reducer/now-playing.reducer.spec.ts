/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />

import {
    IChannel,
    IDmcaInfo,
    IMediaCut,
    IMediaEpisode,
    IMediaItem,
    IMediaShow,
    IVodEpisode
} from "sxmServices";
import { IAction } from "../action/action.interface";
import {
    INowPlayingActionPayload,
    NowPlayingAction,
    SelectNowPlaying
} from "../action/now-playing.action";
import { nowPlayingReducer as reducer } from "./now-playing.reducer";
import { ENowPlayingStatus } from "app/now-playing/now-playing-status.enum";

describe("Now Playing Reducer Test Suite >>", () =>
{
    let initialState: any = null;

    const dmca: IDmcaInfo = {
        backSkipDur        : 100,
        maxBackSkips       : 100,
        fwdSkipDur         : 100,
        maxFwdSkips        : 100,
        maxSkipDur         : 100,
        maxTotalSkips      : 100,
        channelContentType : "channelContentType",
        irNavClass         : "irNavClass",
        playOnSelect       : "playOnSelect"
    };

    beforeEach(() =>
    {
        initialState = {
            albumName: "",
            albumImage: "",
            artistName: "",
            artistInfo: "",
            backgroundImage: "",
            backgroundColor: "",
            channelName: "",
            channelNumber: "",
            cut : {},
            dmcaInfo: {
                irNavClass: ''
            } as IDmcaInfo,
            episode: {
                times: {
                    isoStartTime: '',
                    isoEndTime: ''
                }
            } as IMediaEpisode,
            lastPlayheadTimestamp: 0,
            mediaId: "",
            mediaType: "",
            nowPlayingStatus: ENowPlayingStatus.ENTERING_FIRST_TIME,
            playhead: null,
            show: {} as IMediaShow,
            showMiniPlayer: false,
            startTime: 0,
            type: "",
            trackName: "",
            videoPlayerData: {
                mediaAssetMetadata: {
                    apronSegments: [],
                    episodeTitle: "",
                    seriesName: "",
                    mediaId: ""
                },
                playheadTime: 0,
                vodEpisode: {} as IVodEpisode
            },
            youJustHeard : [],
            video: null,
            posterImageUrl: "",
            stationId: '',
            hideFromChannelList: false
        };
    });

    afterEach(() =>
    {
        initialState = null;
    });

    describe("Infrastructure >>", () =>
    {
        it("Should have a defined test subject.", () =>
        {
            expect(reducer).toBeDefined();
        });

    });

    describe("Actions >>", () =>
    {
        describe("Default Parameter Check >>", () =>
        {
            it("Should return the initial default state when no action is provided.", () =>
            {
                expect(reducer()).toBeDefined();
                expect(reducer()).toEqual(initialState);

                expect(reducer(null, null)).toBeDefined();
                expect(reducer(null, null)).toEqual(initialState);
            });

            it("Should return the initial default state when the action provided is falsy.", () =>
            {
                expect(reducer()).toBeDefined();
                expect(reducer()).toEqual(initialState);

                expect(reducer(null, null)).toBeDefined();
                expect(reducer(null, null)).toEqual(initialState);
            });
        });

        it("Should return the initial default state when the action type is unknown.", () =>
        {
            expect(reducer(null, { type: "unknown" } as NowPlayingAction)).toEqual(initialState);
        });

        it("Should set the current now playing data into the store.", () =>
        {
            const nowPlayingData: INowPlayingActionPayload = {
                albumName: "albumName",
                albumImage: "albumImage",
                artistName: "artistName",
                artistInfo: "artistInfo",
                backgroundImage: "backgroundImage",
                backgroundColor: "",
                channelNumber: "channelNumber",
                channelName: "channelName",
                cut   : {} as IMediaCut,
                video : {} as IMediaItem,
                playhead : null,
                dmcaInfo: {
                    irNavClass: ''
                } as IDmcaInfo,
                episode: {
                    times: {
                        isoStartTime: '',
                        isoEndTime: ''
                    }
                } as IMediaEpisode,
                mediaId: "mediaId",
                channel : {} as IChannel,
                mediaType: "mediaType",
                show: {} as IMediaShow,
                trackName: "trackName",
                type: "type",
                youJustHeard   : [ {
                    albumImageUrl: "imageUrl",
                    title        : "title",
                    artistName   : "artistName",
                    cut          : {} as IMediaCut
                }
                ],
                posterImageUrl: "",
                stationId: '',
                hideFromChannelList: false
            };
            const action: IAction = new SelectNowPlaying(nowPlayingData);
            const newState = reducer(null, action);

            /**
             * AB: with the addition of discrete actions/payloads and observable slices
             * there will be fields returned from the reducer via initialState
             * that were NOT in any single payload
             */
            let result = Object.assign({}, initialState, nowPlayingData);
            expect(newState).toEqual(result);
        });

    });

});
