import * as FavoriteListActions from "../action";
import { FavoriteListAction } from "../action/favorite-list.action";
import { IFavoriteListStore } from "../store/favorite-list.store";

const initialState: IFavoriteListStore =
    {
        favorites: {
            channels: [{
                defaultFav: true
            }],
            shows: [{
                defaultFav: true
            }],
            episodes: [{
                defaultFav: true
            }]
        }
    };

export function favoriteListReducer(state = initialState, action: FavoriteListAction): IFavoriteListStore
{
    switch (action.type)
    {
        case FavoriteListActions.FAVORITES_LOADED:
            return {
                ...state,
                favorites: action.payload
            };

        default:
            return state;
    }
}
