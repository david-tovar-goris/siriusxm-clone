import {
    ComponentFixture,
    TestBed
}                                   from "@angular/core/testing";
import { AllChannelsComponent }     from "./all-channels.component";
import { TranslationModule }        from "../translate/translation.module";
import { MockComponent }            from "../../../test/mocks/component.mock";
import { ChannelListStoreService }  from "../common/service/channel-list.store.service";
import { SharedModule }             from "../common/shared.module";
import { of as observableOf }               from "rxjs";
import { NavigationService }        from "../common/service/navigation.service";
import { navigationServiceMock }    from "../../../test/mocks/navigation.service.mock";
import { CarouselServiceMock, RouterStub } from '../../../test/mocks';
import { CarouselService }          from "../carousel/carousel.service";
import { CarouselStoreService }     from "../common/service/carousel.store.service";
import { CarouselStoreServiceMock } from "../../../test/mocks/carousel.store.service.mock";
import { ITile, SxmAnalyticsService } from "sxmServices";
import { filter } from 'rxjs/operators';
import { Store } from "@ngrx/store";
import { Router } from "@angular/router";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";

describe("AllChannelListComponent", () =>
{
    let component : AllChannelsComponent,
        fixture : ComponentFixture<AllChannelsComponent>;

    beforeEach(() =>
               {
                   let sxmAnalyticsServiceMock = {
                       updateNowPlayingData: function() {},
                       logAnalyticsTag: function() {}
                   };

                   this.store = {
                       select: () => new BehaviorSubject(null)
                   };

                   TestBed.configureTestingModule({
                                                      imports     : [
                                                          TranslationModule,
                                                          SharedModule
                                                      ],
                                                      declarations: [
                                                          AllChannelsComponent,
                                                          MockComponent({
                                                                            selector: "channel-list-item",
                                                                            inputs  : [ "tileData",
                                                                                        "index",
                                                                                        "filterText" ]
                                                                        }),
                                                          MockComponent({
                                                                            selector: "sxm-filter",
                                                                            inputs  : [ "filterType" ]
                                                                        }),
                                                          MockComponent({
                                                                            selector: "sxm-no-results",
                                                                            inputs  : [ "results",
                                                                                        "filterText",
                                                                                        "pageName" ]
                                                                        })
                                                      ],
                                                      providers   : [
                                                          {
                                                              provide : ChannelListStoreService,
                                                              useValue: { channelStore: observableOf({ liveChannels: [ { name: "name" } ] }) }
                                                          },
                                                          {
                                                              provide : NavigationService,
                                                              useValue: navigationServiceMock
                                                          },
                                                          { provide: CarouselService, useValue: CarouselServiceMock },
                                                          {
                                                              provide : CarouselStoreService,
                                                              useClass: CarouselStoreServiceMock
                                                          },
                                                          { provide: SxmAnalyticsService, useValue: sxmAnalyticsServiceMock },
                                                          { provide: Store, useValue: this.store},
                                                          { provide: Router, useClass: RouterStub}
                                                      ]
                                                  }).compileComponents();
               });

    beforeEach(() =>
               {
                   fixture   = TestBed.createComponent(AllChannelsComponent);
                   component = fixture.componentInstance;
                   fixture.detectChanges();
               });

    describe("ngOnInit()", () =>
    {
        it("should pull carousels from the carousel store", () =>
        {
            const subscription = component.tiles$.pipe(
                                          filter((tiles : ITile[]) => !!tiles))
                                          .subscribe((tiles : ITile[]) =>
                                                     {
                                                         const mockCarousel = CarouselStoreServiceMock.carouselMockData;
                                                         const mockTiles    = mockCarousel.tiles;

                                                         expect(tiles).toEqual(mockTiles as any);
                                                     });
            subscription.unsubscribe();
        });
    });
});
