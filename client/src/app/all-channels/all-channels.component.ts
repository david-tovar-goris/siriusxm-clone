import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component, ElementRef,
    NgZone, ViewChild
} from "@angular/core";
import { NavigationService }         from "../common/service/navigation.service";
import { AutoUnSubscribe }           from "../common/decorator";
import { CarouselStoreService }      from "../common/service/carousel.store.service";
import {
    combineLatest as observableCombineLatest,
    SubscriptionLike as ISubscription,
    Observable,
    BehaviorSubject
} from "rxjs";
import { map, debounceTime } from "rxjs/operators";
import {filterConstants}             from "../filter/filter.constants";
import {FilterPipe}                  from "../filter/filter.pipe";
import {
    ICarouselDataByType,
    ITile
} from "sxmServices";
import {
    AnalyticsCarouselNames,
    AnalyticsElementTypes, 
    AnalyticsFindingMethods,
    AnalyticsInputTypes,
    AnalyticsPageFrames,
    AnalyticsTagActionConstants,
    AnalyticsTagActionSourceConstants,
    AnalyticsTagNameConstants,
    AnalyticsUserPaths
} from "app/analytics/sxm-analytics-tag.constants";

@Component({
    templateUrl: "./all-channels.component.html",
    changeDetection : ChangeDetectionStrategy.OnPush,
    styleUrls  : [ "./all-channels.component.scss" ],
    providers : [FilterPipe]
})

@AutoUnSubscribe()
export class AllChannelsComponent implements AfterViewInit
{
    /**
     * Analytics constants
     */
    public AnalyticsTagNameConstants = AnalyticsTagNameConstants;
    public AnalyticsUserPaths = AnalyticsUserPaths;
    public AnalyticsPageFrames = AnalyticsPageFrames;
    public AnalyticsElementTypes = AnalyticsElementTypes;
    public AnalyticsTagActionConstants = AnalyticsTagActionConstants;
    public AnalyticsTagActionSourceConstants = AnalyticsTagActionSourceConstants;
    public AnalyticsInputTypes = AnalyticsInputTypes;
    public AnalyticsCarouselNames = AnalyticsCarouselNames;
    public AnalyticsFindingMethods = AnalyticsFindingMethods;

    private static MAX_TILES_TO_RENDER_AT_ONCE = 10;
    private static MS_BETWEEN_RENDERS          = 50;

    /**
     * Observable that is used to trigger tiles to be filtered when filter text is changed by user
     */
    private filterText$ = new BehaviorSubject<string>("");

    /**
     * Stores subscription to favorites so we can unsubscribe later.
     */
    private subscriptions : Array<ISubscription> = [];

    private renderMoreTilesTimer:any;

    /**
     * Backing store and getter for the tiles that will be passed to the channel list items children for the view
     */
    private _tiles$ = new BehaviorSubject<ITile[]>(null);
    public get tiles$() { return this._tiles$ as Observable<ITile[]>; }

    @ViewChild('backBtn') backBtn: ElementRef;

    constructor(private navigationService : NavigationService,
                private filterPipe: FilterPipe,
                private carouselStoreService : CarouselStoreService,
                private ngzone: NgZone) {}

    /**
     * We want the initialization code here to run outside the view zonejs change detection.  The reason for this
     * is because we are going to handle change detection through the tiles$ and filterText$ observables, so we do
     * not need zonejs triggered Angular change detection uneccesarily as we get the all channels carousel pulled
     * from the servicelib and debounce the user filter text
     */
    ngOnInit() { this.ngzone.runOutsideAngular(this.onInit.bind(this)); }

    ngAfterViewInit()
    {
       // accessibility land on all channels button when all channels page is loaded
       this.backBtn.nativeElement.focus();
    }

    /**
     * Function to be used to track tiles with the ngFor trackBy functionality.  Keeps angular from having to
     * completely rewrite the DOM when tiles are added or deleted or changed.
     * @param index of the tile (ignored here)
     * @param tile that we want to track
     */
    public trackBy(index : number,tile : ITile) { return tile.channelNumber; }

    /**
     * Update the filter observable when the input text changes
     * @param event is the text we want to filter on
     */
    public updateFilter(event) { this.filterText$.next(event as string); }

    /**
     * Go back to the previous page
     */
    public goBack() : void { this.navigationService.goBack(); }

    /**
     * Ovserve and debounce the all channels carousel observables and the filter text observables, and then combine
     * them to feed data to the channel list item child components to manage the change detection for those components
     */
    private onInit()
    {
       const subscription = observableCombineLatest(this.carouselStoreService.selectAllChannelsCarousels(),
                                                      this.filterText$.pipe(debounceTime(500))).pipe(
                                       map(this.updateAndFilterTiles.bind(this)))
                                       .subscribe((tiles : ITile[]) => { this.setTiles(tiles); });

        this.subscriptions.push(subscription);
   }

    /**
     * Track tiles
     */
    public trackTile(index, tile: ITile)
    {
        return tile.tileGuid;
    }

    /**
     * Trigger the tile$ observable with new data
     *
     * This function will break up the tile array into smaller groups and feed them to the template.  This, along
     * with the usage of the track by capability of Angular allows us to break up the UI rendering into smaller
     * pieces and avoid locking up the browser trying to render the *entire* UI all at once.
     *
     * @param {ITile[]} tiles to trigger the observable with
     */
    private setTiles(tiles : ITile[])
    {
        this._tiles$.next([]);

        if (this.renderMoreTilesTimer)
        {
            clearTimeout(this.renderMoreTilesTimer);
        }

        renderMoreTiles.bind(this)(0);

        function renderMoreTiles(startIndex)
        {
            const newTiles                 = tiles.slice(startIndex,
                                                         startIndex +  AllChannelsComponent.MAX_TILES_TO_RENDER_AT_ONCE);
            let tilesToRender : ITile[]  = this._tiles$.getValue() || [];
            startIndex                    += newTiles.length;

            this._tiles$.next(tilesToRender.concat(newTiles));

            if (startIndex < tiles.length)
            {
                this.renderMoreTilesTimer = setTimeout(() => { renderMoreTiles.bind(this)(startIndex); }, AllChannelsComponent.MS_BETWEEN_RENDERS);
            }
        }
    }

    /**
     * Take the combined all channels carousel and user filter text and get a list of filtered tiles to pass to
     * the channel list item child components
     *
     * @param {[ICarouselDataByType , string]} update is an array with the all channels carousel and filter text
     * @returns {ITile[]} array of new tiles that have been filtered by the filter text if necessary
     */
    private updateAndFilterTiles(update : [ ICarouselDataByType, string ]) : ITile[]
    {
        return this.filterTiles(AllChannelsComponent.getTilesFromFirstCarousel(update[0]),
                                                update[1]);
    }

    /**
     * Filter the tile array by the given text
     * @param {ITile[]} tiles is the array of files to filter
     * @param filterText is the text to filter the tiles with
     * @returns {ITile[]} array of filtered tiles
     */
    private filterTiles(tiles : ITile[], filterText) : ITile[]
    {
        return this.filterPipe.transform(tiles, filterText, [filterConstants.CHANNEL_NAME, filterConstants.LINE1]);
    }

    /**
     * Go to the all channels carousel and get the first set of tiles.  This will be all the channel tiles
     * @param {ICarouselDataByType} carousel is the carousel object with the all channels carousel
     * @returns {ITile[]} the array of all the channel tiles from the carousel
     */
    private static getTilesFromFirstCarousel(carousel : ICarouselDataByType) : ITile[]
    {
        if (carousel
            && carousel.zone
            && carousel.zone.length > 0
            && carousel.zone[0].content
            && carousel.zone[0].content.length > 0
            && !!carousel.zone[0].content[0].tiles)
        {
            return carousel.zone[0].content[0].tiles;
        }

        return [] as ITile[];
    }
}



