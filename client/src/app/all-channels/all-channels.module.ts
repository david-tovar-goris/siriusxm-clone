import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { AllChannelsComponent } from "./all-channels.component";
import { NoResultsModule } from "../no-results/no-results.module";
import { FilterModule } from "../filter/filter.module";
import { ChannelTileModule } from "../common/component/list-items/channel/channel-list-item.module";
import { SharedModule } from "../common/shared.module";
import { TranslationModule } from "../translate/translation.module";
import { CarouselService } from '../carousel/carousel.service';
import { ChannelListStoreService } from '../common/service/channel-list.store.service';

@NgModule({
    imports: [
        CommonModule,
        TranslationModule,
        NoResultsModule,
        FilterModule,
        ChannelTileModule,
        SharedModule
    ],
    declarations: [
        AllChannelsComponent
    ],
    providers: [
        CarouselService,
        ChannelListStoreService
    ],
    exports: [
        AllChannelsComponent
    ]
})
export class AllChannelsModule
{
}
