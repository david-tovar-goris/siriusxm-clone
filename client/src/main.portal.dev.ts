import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

import {PortalModule} from "./portal/portal.module";

platformBrowserDynamic().bootstrapModule(PortalModule);
