import "zone.js/dist/long-stack-trace-zone";
import "zone.js/dist/proxy.js";
import "zone.js/dist/sync-test";
import 'zone.js/dist/jasmine-patch';
import "zone.js/dist/async-test";
import "zone.js/dist/fake-async-test";

import { getTestBed }          from "@angular/core/testing";
import {
    BrowserDynamicTestingModule,
    platformBrowserDynamicTesting
}                              from "@angular/platform-browser-dynamic/testing";
import { APP_CONFIG }          from "app/sxmservicelayer/sxm.service.layer.module";
import { getTestServiceConfig } from "app/sxmservicelayer/service.config.test";

declare const require: any;

declare global {
    interface Window {
        s : any;
        addAdobeAnalytics: Function;
    }
}

window.s = {
    t : function () {},
    // mock this for tests, sometimes it fails otherwise
    addAdobeAnalytics: function() { }
};

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
    BrowserDynamicTestingModule,
    platformBrowserDynamicTesting([{ provide : APP_CONFIG, useValue : getTestServiceConfig() }])
);
// Then we find all the tests.
const context = require.context( "./app", true, /\.spec\.ts$/ );
// And load the modules.
context.keys().map(context);
