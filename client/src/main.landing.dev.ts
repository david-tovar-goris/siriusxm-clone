import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

import { LandingModule } from "./landing/landing.module";

platformBrowserDynamic().bootstrapModule(LandingModule);
