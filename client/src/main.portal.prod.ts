import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import {PortalModule} from "./portal/portal.module";

enableProdMode();

platformBrowserDynamic().bootstrapModule(PortalModule);
