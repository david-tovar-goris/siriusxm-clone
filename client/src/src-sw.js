importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

if (workbox) {

    workbox.core.skipWaiting();

    const CACHE_NAME = 'offline-html';
    const FALLBACK_HTML_URL = './src/offline.html';
    self.addEventListener('install', async (event) => {
        event.waitUntil(
            caches.open(CACHE_NAME)
                .then((cache) => cache.add(FALLBACK_HTML_URL))
        );
    });

    workbox.navigationPreload.enable();

    const networkOnly = new workbox.strategies.NetworkOnly();
    const navigationHandler = async (params) => {
        if(navigator.onLine)
        {
            return await networkOnly.handle(params);
        }
        else
        {
            return caches.match(FALLBACK_HTML_URL, {
                cacheName: CACHE_NAME,
            });
        }
    };

    // Register this strategy to handle all navigations.
    workbox.routing.registerRoute(
        new workbox.routing.NavigationRoute(navigationHandler)
    );

    workbox.precaching.precacheAndRoute(self.__WB_MANIFEST)


} else {
    console.log(`Workbox didn't load`);
}


