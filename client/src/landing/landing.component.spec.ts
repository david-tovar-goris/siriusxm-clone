import {
    async,
    ComponentFixture,
    TestBed
} from "@angular/core/testing";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { CommonModule } from "@angular/common";
import { LandingComponent } from "./landing.component";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { ContextualService } from "sxmServices";
import { ContextualServiceMock } from "../../test/mocks/contextual.service.mock";
import { MockTranslateService } from "../../test/mocks/translate.service";

describe("LandingComponent", () =>
{
    let component: LandingComponent;
    let fixture: ComponentFixture<LandingComponent>;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            imports     : [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
                HttpModule,
                CommonModule,
                TranslateModule
            ],
            declarations: [
                LandingComponent
            ],
            providers   : [
                { provide: ContextualService, useValue: ContextualServiceMock.getSpy() },
                { provide: TranslateService, useValue: MockTranslateService.getSpy() }
            ]
        }).compileComponents();
    }));

    beforeEach(() =>
    {
        fixture   = TestBed.createComponent(LandingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () =>
    {
        expect(component instanceof LandingComponent).toBe(true);
    });

    it("should be set contextual landing data", () =>
    {
        expect(component.contextualLandingData).toBe(ContextualServiceMock.contextualData as any);
    });
});
