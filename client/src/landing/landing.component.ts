import {
    Component,
    OnDestroy
} from "@angular/core";
import {
    Logger,
    ContextualService,
    IContextualLandingData,
    IContextualData,
    IContextualLandingAppInfo,
    DeepLinkTypes
} from "sxmServices";
import * as moment from 'moment';
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";

/**
 * @MODULE:     Landing
 * @CREATED:    01/11/18
 * @COPYRIGHT:  2017 Sirius XM Radio Inc.
 *
 * @DESCRIPTION:
 *
 *     LandingComponent used to load landing page - used for mobile devices
 */
@Component({
    moduleId   : module.id,
    selector   : "app-sxm-landing-client",
    templateUrl: "landing.component.html",
    styleUrls  : [ "landing.component.scss" ],
    providers  : []
})

export class LandingComponent implements OnDestroy
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("LandingComponent");

    /**
     * Hold the landing page data to render the page.
     * @type {IContextualLandingData}
     */
    public contextualLandingData: IContextualLandingData = this.getEmptyContextualData();

    /**
     * description prefix
     * @type {string}
     */
    public descriptionPrefix: string = "";

    /**
     * Used to hold the contextual service subscription
     */
    private subscription: Subscription;

    /**
     * Default title value
     */
    private defaultTitle: string = "";

    /**
     * default title line2
     */
    private defaultTitle2: string = "";

    /**
     * Default description value
     */
    private defaultDescription: string = "";

    /**
     * Constructor
     * @param {ContextualService} contextualService
     */
    constructor(private contextualService: ContextualService,
                private translate: TranslateService)
    {
        let browserLang = translate.getBrowserLang();
        const isFrench      = browserLang.match(/fr/) ? true : false;
        this.subscription = contextualService.initializeContextualLanding(isFrench)
                                             .subscribe((response: IContextualLandingData) =>
                                             {
                                                 if (response && !response.contextualData)
                                                 {
                                                     this.contextualLandingData = response;
                                                     this.contextualLandingData.contextualData = this.getDefaultContextualData();
                                                 }
                                                 else
                                                 {
                                                     this.contextualLandingData = response ? response : this.getEmptyContextualData();
                                                 }
                                                 this.descriptionPrefix = this.getAODDateLabel();
                                             }) as any as Subscription;
    }

    /**
     * Used to go to store url.
     */
    public openStore()
    {
        window.location.href = this.contextualLandingData.storeUrl;
    }

    /**
     * Used to go to store url.
     * NOTE: IOS goes to signup url and android goes to url
     */
    public downloadAction()
    {
        window.location.href = this.contextualLandingData.actionButtonUrl;
    }

    /**
     * open app using app link.
     */
    public openApp(): void
    {
        if (this.contextualLandingData.appDeepLink)
        {
            window.location.href = this.contextualLandingData.appDeepLink;
        }
    }

    /**
     * gets the date label for a AOD type
     * @returns {string}
     */
    private getAODDateLabel(): string
    {
        const expirationDate  = this.contextualLandingData.contextualData.expirationDate;
        const originalAirDate = this.contextualLandingData.contextualData.originalAirDate;
        if (!expirationDate && !originalAirDate)
        {
            return "";
        }
        const specialString = this.translate.instant("landing.special").toUpperCase() + " - ";
        if (this.contextualLandingData.contextualData.isSpecial)
        {
            return specialString;
        }

        const originalAirDateLabel = this.createAirDateLabel(originalAirDate);
        const expDateLabel         = this.createExpDateLabel(expirationDate);
        if (expDateLabel)
        {
            return expDateLabel + " - ";
        }
        else if (originalAirDateLabel)
        {
            return originalAirDateLabel + " - ";
        }
        else
        {
            return specialString;
        }
    }

    /**
     * Creates the original air date label
     * @param {string} originalAirDate
     * @returns {string}
     */
    private createAirDateLabel(originalAirDate: string): string
    {
        let airDate;

        const date        = moment(originalAirDate),
              presentDate = moment(),
              yesterDay   = moment().subtract(1, 'day'),
              airDateText = this.translate.instant("landing.airDate").toUpperCase();

        if (!originalAirDate)
        {
            return "";
        }

        if (date.isSame(presentDate, 'day'))
        {
            airDate = this.translate.instant("landing.notificationToday");
        }
        else if (date.isSame(yesterDay, 'day'))
        {
            airDate = this.translate.instant("landing.notificationYesterday");
        }
        else if (date.isSame(presentDate, 'week'))
        {
            airDate = date.format('D').toUpperCase();
        }
        else
        {
            airDate = airDateText + " " + date.format('MM/DD/YYYY');
        }

        return airDate.replace(/\//g, "\.");
    }

    /**
     * creates the expiration date label
     * @param {string} expirationDate
     * @returns {string}
     */
    private createExpDateLabel(expirationDate: string): string
    {
        const date         = moment(expirationDate),
              duration     = moment.duration(date.diff(moment())),
              expTimeHours = duration.asHours(),
              expDateText  = this.translate.instant("landing.expire");

        if (!expirationDate)
        {
            return "";
        }

        if (expTimeHours > 0 &&
            expTimeHours <= 24)
        {
            return expDateText + " " + Math.ceil(expTimeHours) + this.translate.instant("landing.hours").toUpperCase();
        }

        return "";
    }

    /**
     * gets the empty object
     * @returns {IContextualLandingData}
     */
    private getEmptyContextualData(): IContextualLandingData
    {
        return {
            contentType             : "",
            channelName             : "",
            iosVersion              : 0,
            universalLink           : "",
            storeUrl                : "",
            actionButtonUrl         : "",
            operatingSystem         : "",
            contextualLandingAppInfo: {} as IContextualLandingAppInfo,
            contextualData          : {} as IContextualData,
            actionButtonText        : ""
        } as IContextualLandingData;
    }

    /**
     * ngOnDestroy
     */
    ngOnDestroy()
    {
        this.subscription.unsubscribe();
    }

    /**
     * returns the default contextual data.
     * @returns {IContextualData}
     */
    private getDefaultContextualData(): IContextualData
    {
        this.translate.get("landing.defaultTitle").subscribe((value) =>
        {
            this.contextualLandingData.contextualData.title = value;
            this.contextualLandingData.contextualData.title2 = this.translate.instant( "landing.defaultTitle2");
            this.contextualLandingData.contextualData.description = this.translate.instant( "landing.defaultDescription");
        });

        return {
            imageUrl: "",
            title: this.defaultTitle,
            title2: this.defaultTitle2,
            subTitle: "",
            description: this.defaultDescription,
            originalAirDate: "",
            expirationDate: "",
            buttonText: "landing.listenNow",
            isSpecial: false,
            deepLinkType: DeepLinkTypes.LIVE
        };
    }
}
