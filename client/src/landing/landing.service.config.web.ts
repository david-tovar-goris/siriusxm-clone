import { ApiLayerTypes, IAppConfig, inDev, inQA } from "sxmServices";
import { BrowserUtil }          from "app/common/util/browser.util";
import { PrivateBrowsingUtil } from "../app/common/util/privateBrowsing.util";
import { UUIDUtil } from "app/common/util/uuid.util";

declare const sxmAppVersion : string;
declare const sxmGitHashNumber : string;
declare const sxmTeamCityBuildNumber : string;

export function getLandingServiceConfig()
{
    //TODO : Cannot use the StorageService here as it has to be injected
    const language = localStorage.getItem('language');

    const apiEndPoint = BrowserUtil.getApiEndpoint();
    const clientCapabilities = ["enhancedEDP", "seededRadio", "tabSortOrder", "zones", "cpColorBackground", "additionalVideo", "podcast", "irisPodcast"];
    const landingServiceConfig : IAppConfig = {
        apiEndpoint           : BrowserUtil.getApiEndpoint(),
        domainName            : BrowserUtil.getDomainName(),
        allProfilesData       : undefined,
        appId                 : "LandingWebClient",
        clientConfiguration   : undefined,
        resultTemplate        : "web",
        adsWizzSupported      : true,
        inPrivateBrowsingMode : PrivateBrowsingUtil.checkPrivateBrowsingMode( window ) as any,
        restart               : BrowserUtil.reload,
        deviceInfo            : {
            appRegion              : BrowserUtil.getAppRegion(),
            language               : ( BrowserUtil.getAppRegion() === 'CA') ? `${language}-ca` : language,
            browser                : "Chrome",
            browserVersion         : "54",
            clientDeviceId         : null,
            clientCapabilities     : clientCapabilities,
            clientDeviceType       : "web",
            deviceModel            : "LandingWebClient",
            osVersion              : "Windows",
            platform               : "Web",
            player                 : "html5",
            sxmAppVersion          : sxmAppVersion,
            sxmGitHashNumber       : sxmGitHashNumber,
            sxmTeamCityBuildNumber : sxmTeamCityBuildNumber,
            isChromeBrowser        : BrowserUtil.isChrome(),
            isMobile               : BrowserUtil.isMobile(),
            isNative               : BrowserUtil.isNative(),
            supportsAddlChannels   : true,
            supportsVideoSdkAnalytics: true
        },
        contextualInfo        : {
            userAgent   : navigator.userAgent || navigator.vendor,
            queryString : window.location.search.substring( 1 ),
            host        : window.location.host,
            hostName    : window.location.hostname,
            protocol    : "https:"
        },
        defaultSuperCategory : null,
        loginRequired: false,
        uniqueSessionId : UUIDUtil.generate(),
        isFreeTierEnable: BrowserUtil.getAppRegion() !== ApiLayerTypes.REGION_CA,
        initialPathname : BrowserUtil.getBrowserPathname()
    };

    return landingServiceConfig;
}
