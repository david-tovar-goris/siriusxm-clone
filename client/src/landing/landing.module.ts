import {
    Logger,
    providers,
    injectAppConfig,
    IAppConfig,
    ContextualService
} from "sxmServices";
import { LandingComponent } from "./landing.component";
import { NgModule, InjectionToken } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { getLandingServiceConfig } from "./landing.service.config.web";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import {
    HttpClient,
    HttpClientModule
} from "@angular/common/http";
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from "@ngx-translate/core";

export const APP_CONFIG   = new InjectionToken<IAppConfig>("app.config");
let serviceConfigProvider = { provide: APP_CONFIG, useValue: getLandingServiceConfig() };

/**
 * Creates the translation loader.
 *
 * NOTE: This method is required to be exported to work with AOT compilation builds.
 *
 * @param {Http} http - The HTTP service to load the JSON translation bundles.
 * @returns {TranslateHttpLoader}
 */
export function createTranslateLoader(http: HttpClient)
{
    return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

injectAppConfig(serviceConfigProvider);

@NgModule({
    declarations: [
        LandingComponent
    ],
    imports     : [
        BrowserModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide   : TranslateLoader,
                useFactory: createTranslateLoader,
                deps      : [ HttpClient ]
            }
        })
    ],
    providers   : providers,
    bootstrap   : [ LandingComponent ],
    exports     : [ LandingComponent ]
})

export class LandingModule
{
    /**
     * Internal logger.
     */
    private static logger: Logger = Logger.getLogger("LandingModule");

    /**
     * Constructor
     * @param  translate - module used to set up translation
     * @param  contextualService
     */
    constructor(private translate: TranslateService,
                private contextualService: ContextualService)
    {
        const url = contextualService.getDeepLinkUrl();
        if (url)
        {
            window.location.href = url;
        }

        translate.addLangs([
            "en",
            "fr",
            "en-ca"
        ]);
        translate.setDefaultLang("en");

        let browserLang = translate.getBrowserLang();
        const lang      = browserLang.match(/en|fr|en-ca/) ? browserLang : "en";
        LandingModule.logger.debug(`Constructor( Current locale: ${lang} )`);
        translate.use(lang);
    }
}
