/**
 * This file contains the api proxy specific configuration for webpack.  This is used with the
 * "advanced" configuration approach specified in the following link from the webpack 2 documentation
 *
 * https://webpack.js.org/guides/production/#advanced-approach
 */

// The API that the proxy wil forward to unless overridden at the command line
const DEFAULT_API      = "qaus.qa4";
const DEFAULT_API_PATH = DEFAULT_API.split('.');

const DEV_ENDPOINTS =
        {
          intever3 : "player-devintever3.mountain.siriusxm",
          medusadev: "webclientdev.mountain.siriusxm"
        };

const QA_ENDPOINTS =
        {
          qa4 : "player-k2qa4.mountain.siriusxm",
          qa5 : "player-k2qa5.mountain.siriusxm",
          medusaqa: "webclientqa.mountain.siriusxm"
        };

const VANITY_ENDPOINTS =
        {
          player      : "player.siriusxm",
          beta        : "playerbeta.mountain.siriusxm",
          companybeta : "companybeta.siriusxm",
          publicbeta  : "beta.mountain.siriusxm",
          medusabeta : "webclientbeta.siriusxm",
          medusarc   : "webclientrc.siriusxm"
        };

// All the available APIs for proxy.  These will get filled in with the endpoint conatants above and then given the
// final .com or .ca address based on whether its a us or canadian enpoint.
const APIS = {
  devus    : {},
  devca    : {},
  qaus     : {},
  qaca     : {},
  vanityus : {},
  vanityca : {}
};

module.exports = env => {
  /*
   * Go to all the different endpoint types and create .com and .ca version that can be used from DEFAULT_API or with
   * the command line override
   */
  Object.keys(DEV_ENDPOINTS).forEach((key) => {
    APIS.devus[key] = DEV_ENDPOINTS[key] + ".com";
    APIS.devca[key] = DEV_ENDPOINTS[key] + ".ca";
  });

  Object.keys(QA_ENDPOINTS).forEach((key) => {
    APIS.qaus[key] = QA_ENDPOINTS[key] + ".com";
    APIS.qaca[key] = QA_ENDPOINTS[key] + ".ca";
  });

  Object.keys(VANITY_ENDPOINTS).forEach((key) => {
    APIS.vanityus[key] = VANITY_ENDPOINTS[key] + ".com";
    APIS.vanityca[key] = VANITY_ENDPOINTS[key] + ".ca";
  });

  let target = APIS[DEFAULT_API_PATH[0]][DEFAULT_API_PATH[1]];
  let API    = env.api;

  console.log(env);

  if (env.apiOverride === 'true') {
    API = require('./local.proxy.conf');
  }
  if (API) {
    const API_PATH     = (API) ? API.split('.') : DEFAULT_API_PATH;
    const type         = API_PATH[0];
    const endpoint     = API_PATH[1];
    const customTarget = (APIS[type] && APIS[type][endpoint]) ? APIS[type][endpoint] : undefined;

    if (!customTarget) {
      console.log(`Cannot use API ${API} defaulting to ${DEFAULT_API}`);
    }
    else {
      target = customTarget;
    }
  }

  console.log(`API will be proxied to ${target}`);

  const PROXY_CONFIG = [
    {
      context      : ["/rest"],
      target       : "https://" + target,
      secure       : false,
      changeOrigin : true
    }
  ];

  return PROXY_CONFIG;
};
