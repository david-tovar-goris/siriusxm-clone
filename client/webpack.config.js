const commonConfig    = require("./webpack.common.js");
const Merge           = require("webpack-merge");

module.exports = env =>
{
    const buildTypeConfig = require(`./webpack.${env.build}.js`);
    return Merge(commonConfig(env),buildTypeConfig(env));
};
