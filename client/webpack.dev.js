const HtmlWebpackPlugin = require('html-webpack-plugin');
const path              = require('path');

module.exports = env =>
{
    var mainClientDev = "./src/main.client.dev.ts";
    var mainClientProd = "./src/main.client.prod.ts";
    var deepLinkDev = "./src/main.landing.dev.ts";
    var deepLinkProd = "./src/main.landing.prod.ts";
    var portalDev = "./src/main.portal.dev.ts";
    var portalProd = "./src/main.portal.prod.ts";
    var entryPoints = {};

    switch (env.app) {
        case "client": {
           entryPoints = {
                client: env.optimize === "true" ? mainClientProd : mainClientDev
            };
            console.log("Currently running dev server as  - Client ");
            break;
        }
        case "landing": {
            entryPoints = {
                client: env.optimize === "true" ? mainClientProd : mainClientDev,
                deepLink: env.optimize === "true" ? deepLinkProd : deepLinkDev,
                "polyfills.client": ["./src/polyfills.ts"],
                "styles.client": ["./src/styles.scss"]
            };
            console.log("Currently running dev server as  - Landing & Client ");
            break;
        }
        case "portal": {
            entryPoints = {
                portal: env.optimize === "true" ? portalProd : portalDev,
                "polyfills.client": ["./src/polyfills.ts"],
                "styles.client": ["./src/styles.scss"]
            };
            console.log("Currently running dev server as  - Portal & Client ");
            break;
        }
        case "landingCA": {
            entryPoints = {
                client: env.optimize === "true" ? mainClientProd : mainClientDev,
                deepLinkCA: env.optimize === "true" ? deepLinkProd : deepLinkDev,
                "polyfills.client": ["./src/polyfills.ts"],
                "styles.client": ["./src/styles.scss"]
            };
            console.log("Currently running dev server as  - LandingCA & Client ");
            break;
        }
        default: {
            throw "Invalid app";
        }
    }
    return {
        resolve: {
            "alias": {
                "sxm-audio-player": path.resolve(__dirname, "node_modules/sxm-audio-player/bin/web-audio-player.js"),
            }
        },
        devtool : 'source-map',
        plugins: getPlugins(env),
        entry: entryPoints,
        mode: env.analyze ? "production" : "development"
    };
};

function getPlugins(env)
{
    let plugins = [];

    // Special Case- For dev landing server, have to host client as well . below code is hosting index.html under same port.
    if(env.app === "landing" || env.app === "landingCA")
    {
        const entryPoints         = [ "inline", "polyfills.client", "sw-register", "styles.client", "vendor.client", "client" ];
        plugins.push(new HtmlWebpackPlugin({
            "template"       : "./src/" + "index.html",
            "filename"       : "./" + "index.html",
            "hash"           : false,
            "inject"         : true,
            "compile"        : true,
            "favicon"        : false,
            "minify"         : false,
            "cache"          : true,
            "showErrors"     : true,
            "chunks"         : entryPoints,
            "excludeChunks"  : [ 'dev-int' ],
            "title"          : "Webpack App",
            "xhtml"          : true,
            "chunksSortMode" : function sort(left, right)
            {
                let leftIndex  = entryPoints.indexOf(left.names[ 0 ]);
                let rightindex = entryPoints.indexOf(right.names[ 0 ]);
                if (leftIndex > rightindex)
                {
                    return 1;
                }
                else if (leftIndex < rightindex)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }));
    }

    plugins = [
        ...plugins
    ];

    return plugins;
}

