const path                   = require('path');
const WebpackPwaManifest     = require('webpack-pwa-manifest');

const pwaManifestJSON = {
    name: "SiriusXM",
    short_name: "SiriusXM",
    filename: "manifest.json",
    start_url: "/",
    display: "standalone",
    orientation: "portrait",
    background_color: "#3740ff",
    theme_color: "#fff",
    fingerprints: false,
    icons: [
        {
            "src": path.resolve("src/assets/images/SXM_AppIcon_1024x1024.png"),
            "sizes": "72x72",
            "type": "image/png"
        },
        {
            "src": path.resolve("src/assets/images/SXM_AppIcon_1024x1024.png"),
            "sizes": "96x96",
            "type": "image/png"
        },
        {
            "src": path.resolve("src/assets/images/SXM_AppIcon_1024x1024.png"),
            "sizes": "192x192",
            "type": "image/png"
        },
        {
            "src": path.resolve("src/assets/images/SXM_AppIcon_1024x1024.png"),
            "sizes": "512x512",
            "type": "image/png"
        }
    ]
};

const getPWADevPlugins = () =>
{
    const pwaPlugins = [];
    /**
     * For Local Manifest file
     */
    const pwaLocalManifest = {
        ...pwaManifestJSON,
        name: "SiriusXM-local",
        short_name: "SiriusXM-local",
        filename: "manifest-local.json"

    };
    pwaPlugins.push(new WebpackPwaManifest(pwaLocalManifest));

    /**
     * For Companybeta Manifest file
     */
    const pwaCBManifest = {
        ...pwaManifestJSON,
        name: "SiriusXM-cb",
        short_name: "SiriusXM-cb",
        filename: "manifest-cb.json"

    };
    pwaPlugins.push(new WebpackPwaManifest(pwaCBManifest));

    /**
     * For QA Manifest file
     */
    const pwaQAManifest = {
        ...pwaManifestJSON,
        name: "SiriusXM-qa",
        short_name: "SiriusXM-qa",
        filename: "manifest-qa.json"
    };
    pwaPlugins.push(new WebpackPwaManifest(pwaQAManifest));

    return pwaPlugins;

};

const getProdPlugin = () =>
{
    return new WebpackPwaManifest(pwaManifestJSON);
};

module.exports = {
    getDevPlugins: getPWADevPlugins,
    getProdPlugin: getProdPlugin
};
