import { of as observableOf }   from "rxjs";
import { mockTileData } from "./carousel.service.mock";

export class CarouselStoreServiceMock
{
    static carouselMockData = {
        type: "content", guid: "0010", title: { textValue: "Continue Listening" }, tiles: mockTileData
    };

    public selectContentCarousel;
    public selectRecents;
    public selectViewAll;
    public carouselStore;
    public selectAllChannelsCarousels;
    public viewAllCarousels;
    public selectProfile;

    constructor()
    {
        this.selectProfile = jasmine.createSpy('selectProfile').and.returnValue(observableOf({
            zone: [ { content: [ CarouselStoreServiceMock.carouselMockData ] } ]
        }));
        this.selectContentCarousel      = jasmine.createSpy('selectContentCarousel');
        this.selectRecents              = jasmine.createSpy('selectRecents')
                                                 .and
                                                 .returnValue(observableOf({ content: [] }));
        this.selectViewAll              = jasmine.createSpy('selectViewAll')
                                                 .and
                                                 .returnValue(observableOf({ content: [] }));
        this.viewAllCarousels = observableOf({
            zone: [ {
                content: [ CarouselStoreServiceMock.carouselMockData ],
                hero   : [ CarouselStoreServiceMock.carouselMockData ]
            }
            ]
        });
        this.selectAllChannelsCarousels = jasmine.createSpy('selectAllChannelsCarousels')
                                                 .and
                                                 .returnValue(observableOf(CarouselStoreServiceMock.carouselMockData));
        this.carouselStore              = observableOf({
                                                            selectedContentCarousel: CarouselStoreServiceMock.carouselMockData
                                                        });
    }
}
