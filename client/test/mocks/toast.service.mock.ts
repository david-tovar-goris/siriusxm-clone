import { BehaviorSubject, Observable, of as observableOf } from "rxjs";

import { ToastInformation } from "../../src/app/common/component/toast/toast.interface";

export class ToastServiceMock
{

    private static toastInformation: ToastInformation = {
        isOpen: true,
        data: {
            messagePath: "errors.toasty.message",
            buttonCTAText: "Got it",
            isAltColor: false
        }
    };

    private static _spy = jasmine.createSpyObj("toastServiceSpy", [
        "open",
        "close"
    ]);

    public static getSpy(): any
    {
        this._spy.toastInformation = new BehaviorSubject({
            isOpen: false,
            data: null
        });

        return this._spy;
    }

    public static open(data)
    {
        this._spy.toastInformation.next(this.toastInformation);

        ToastServiceMock.getSpy().open.and.returnValue(observableOf(null));
    }

}

