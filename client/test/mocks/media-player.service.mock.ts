import { mediaPlayerMock } from "../../../servicelib/src/test/mocks/media-player.mock";
import { SubscriptionLike as ISubscription ,  Observable, of as observableOf } from "rxjs";

export class MediaPlayerServiceMock
{
    public mediaPlayer = mediaPlayerMock;
    public mediaPlayerSubscriptions: Array<ISubscription> = [];
    public playheadSubFunctions: Array<Function> = [];
    public isMediaContentPlaying: boolean = true;
    public isContentPlaying : Observable<boolean> = observableOf(true);

    constructor()
    {
    }

    public isNewMediaPlayer()
    {
        return false;
    }

    public isPlaying = () => true;
}
