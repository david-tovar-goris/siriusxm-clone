import { BehaviorSubject } from "rxjs";

/**
 * @todo this in no way reflects the updated SegmentsService.
 * luckily the ApronSegmentsComponent doesn't really use
 * anything from the service yet. But we will need to update
 * this eventually to be more similar to the actual SegmentsService
 */
export class SegmentsServiceMock
{
    private readyState: boolean = false;
    public nowPlayingDataLoaded: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor()
    {
    }

    public isSegmentCurrentlyPlaying()
    {

    }

    public setReadyState(readyState: boolean)
    {
        this.readyState = readyState;
    }
}
