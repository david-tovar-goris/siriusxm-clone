import { of as observableOf } from "rxjs";

export class ContextualServiceMock
{
    private static _spy = jasmine.createSpyObj("mockContextualServiceSpy", [ '' ]);

    public static contextualData = {
        contentType             : "type",
        channelName             : "channelName",
        goToStore               : false,
        iosVersion              : 0,
        universalLink           : "universalLink",
        storeUrl                : "storeUrl",
        operatingSystem         : "os",
        contextualLandingAppInfo: {
            appImageSrc      : "appImageSrc",
            gtmId            : "downloadApp-iOS",
            accessibiltyLabel: "accessibiltyLabel"
        },
        contextualData          : {
            imageUrl       : "imageUrl",
            title          : "title",
            subTitle       : "subTitle",
            description    : "description",
            originalAirDate: "originalAirDate",
            expirationDate : "expirationDate",
            buttonText     : "buttonText"
        }
    };

    public static getSpy(): any
    {
        this._spy.contextualData              = observableOf(this.contextualData);
        this._spy.initializeContextualLanding = function ()
        {
            return observableOf(ContextualServiceMock.contextualData);
        };

        return this._spy;
    }
}
