export class NeriticLinkServiceMock
{
    liveChannels = [];

    public takePrimaryTileAction;
    public navigateToShowsList;
    public navigateToEpisodeListing;
    public toggleFavorite;
    public takeAction;
    public getNeriticData;
    public setShowReminder;
    public getTileContent;
    public takeSecondaryTileAction;
    public removeRecentlyPlayedTile;
    private appSignUp;
    public searchFocusSubject;
    public searchFocus;
    public invalidNeriticLinkSubject;
    public invalidNeriticLink;
    public getFavorite;

    constructor()
    {
        this.takePrimaryTileAction = jasmine.createSpy('takePrimaryTileAction ');
        this.takeSecondaryTileAction = jasmine.createSpy('takeSecondarySecondaryTileAction');
        this.getTileContent = jasmine.createSpy('getTileContent');
        this.navigateToShowsList = jasmine.createSpy('navigateToShowsList');
        this.navigateToEpisodeListing = jasmine.createSpy('navigateToEpisodeListing');
        this.toggleFavorite = jasmine.createSpy('toggleFavorite');
        this.setShowReminder = jasmine.createSpy('setShowReminder');
        this.removeRecentlyPlayedTile = jasmine.createSpy('removeRecentlyPlayedTile');
        this.takeAction = jasmine.createSpy('takeAction');
        this.getNeriticData = jasmine.createSpy('getNeriticData');
        this.appSignUp = jasmine.createSpy('appSignUp');
        this.searchFocusSubject = jasmine.createSpyObj('searchFocusSubject', [
            "next"
        ]);
        this.searchFocus = jasmine.createSpyObj('searchFocus', [
            "subscribe"
        ]);
        this.invalidNeriticLinkSubject = jasmine.createSpyObj('invalidNeriticLinkSubject', [
            "next"
        ]);
        this.invalidNeriticLink = jasmine.createSpyObj('invalidNeriticLink', [
            "subscribe"
        ]);

        this.getFavorite = jasmine.createSpy('getFavorite ').and.returnValue("");
    }
}
