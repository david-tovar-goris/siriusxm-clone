import {
    Component,
    Directive
} from '@angular/core';

export function MockComponent(options: Component): Component
{
    let metadata: Component =
            {
                selector: options.selector,
                template: options.template || '',
                inputs: options.inputs,
                outputs: options.outputs
            };
    return Component(metadata)(class _
    {
    } as any);
}

export function MockDirective(options: Component): Directive
{
    let metadata: Directive =
            {
                selector: options.selector,
                inputs  : options.inputs,
                outputs : options.outputs
            };
    return Directive(metadata)(class _
    {
    } as any);
}
