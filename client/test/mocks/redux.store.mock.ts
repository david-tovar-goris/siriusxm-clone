import { BehaviorSubject } from "rxjs";

/**
 * Standard mock store that can be used in unit tests to mock a @ngrx/store.
 */
export class MockStore<T>
{
    /**
     * List of reducers, aka stores that act as observable publishers.
     */
    reducers = new Map<string, BehaviorSubject<any>>();

    /**
     * used to set a fake state
     * @param reducerName name of your reducer
     * @param data the mockstate you want to have
     */
    mockState(reducerName, data)
    {
        this.select(reducerName).next(data);
    }

    /**
     * simple solution to support selecting/subscribing to this mockstore as usual.
     * @param pathOrMapFn reducer name or function.
     * @returns {undefined|BehaviorSubject<any>}
     */
    select(pathOrMapFn: ((state: T) => any) | string): BehaviorSubject<any>
    {
        let mapped$: BehaviorSubject<any>;

        if (typeof pathOrMapFn === "string")
        {
            // mapped$ = pluck.call(this, pathOrMapFn, ...paths);
            if (!this.reducers.has(pathOrMapFn))
            {
                const bs = new BehaviorSubject({});
                this[ pathOrMapFn ] = bs;
                this.reducers.set(pathOrMapFn, bs);
            }
            return this.reducers.get(pathOrMapFn);
        }
        else if (typeof pathOrMapFn === "function")
        {
            let result = pathOrMapFn.call(this, this);
            if(!result)
            {
                // TODO: BMR: 01/08/2018: Need a way to handle selector functions that look deeper than just a root level store.
                const selectorWarning: string = `Unknown selector function: ${JSON.stringify(pathOrMapFn)}`;
                console.warn(selectorWarning);
                result = new BehaviorSubject(selectorWarning);
                result.next(selectorWarning);
            }
            return result;
            // mapped$ = map.call(this, pathOrMapFn);
        }
        else
        {
            throw new TypeError(
                `Unexpected type '${typeof pathOrMapFn}' in select operator,` +
                ` expected 'string' or 'function'`
            );
        }
    }

    /**
     * NOTE: This must be spyed on during testing.
     * @param data
     */
    dispatch(data: any)
    {
        // SPY
    }
}
