export class NowPlayingIndicatorServiceMock
{
    public getPlayingIndicator;

    constructor()
    {
        this.getPlayingIndicator = jasmine.createSpy("getPlayingIndicator").and.returnValue('');
    }
}
