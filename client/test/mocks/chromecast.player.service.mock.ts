import { of as observableOf } from "rxjs";

export class chromecastPlayerServiceMock
{
    public switchPlayerType$ = observableOf("live");

    constructor()
    {
    }

}
