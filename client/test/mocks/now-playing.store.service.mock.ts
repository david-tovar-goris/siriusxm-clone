import { of as observableOf } from "rxjs";
import {nowPlayingStoreMock} from "./data/now-playing-store.mock";
import { mediaShowMock } from "./data/media-show.mock";

export class NowPlayingStoreServiceMock
{
    public selectNowPlayingRoute;
    public selectChannelLogoUrl;
    public selectChannelLogoAltText;
    public selectNowPlayingStatus;
    public selectVideoPlayerData;
    public subscribeToChannelStore;
    public subscribeToNowPlayingData;
    public nowPlayingStore;
    public updateVideoMarker;
    public show$;
    public resetNowPlaying;
    public enterNowPlaying;
    public selectNowPlayingChannel;
    public videoPlayerDataSlice;

    constructor()
    {
        this.selectNowPlayingRoute = jasmine.createSpy('selectNowPlayingRoute');
        this.selectChannelLogoUrl = jasmine.createSpy('selectChannelLogoUrl');
        this.selectChannelLogoAltText = jasmine.createSpy('selectChannelLogoAltText');
        this.selectNowPlayingStatus = jasmine.createSpy('selectNowPlayingStatus');
        this.selectVideoPlayerData = jasmine.createSpy('selectVideoPlayerData');
        this.subscribeToChannelStore = jasmine.createSpy('subscribeToChannelStore');
        this.subscribeToNowPlayingData = jasmine.createSpy('subscribeToNowPlayingData');
        this.nowPlayingStore = observableOf(nowPlayingStoreMock);
        this.updateVideoMarker = jasmine.createSpy('updateVideoMarker');
        this.show$ = observableOf({ mediaShowMock });
        this.resetNowPlaying = jasmine.createSpy("resetNowPlaying");
        this.enterNowPlaying = jasmine.createSpy("enterNowPlaying");
        this.selectNowPlayingChannel = jasmine.createSpy("selectNowPlayingChannel");
        this.videoPlayerDataSlice = observableOf({ nowPlayingStoreMock });
    }
}
