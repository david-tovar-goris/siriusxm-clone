export class SplashScreenServiceMock
{
    private static _spy = jasmine.createSpyObj("splashScreenService", [
        "addDynamicSplashScreen",
        "closeSplashScreen"
    ]);

    public static getSpy(): any
    {
        return this._spy;
    }
}
