import { IFavoriteItem } from "sxmServices";
import { of as observableOf } from "rxjs";

export class FavoriteServiceMock
{
    private static _spy = jasmine.createSpyObj("favoriteServiceSpy", [
        "updateFavorite",
        "updateFavorites",
        "getFavorites"
    ]);

    public static favorites: Array<IFavoriteItem> = [
        {
            assetGUID: "blah",
            assetType: "show",
            channelId: "2",
            showLogos: [],
            showImages: [],
            assetName: "assetName02",
            contentType:"contentType02",
            episodeCount: 2
        },
        {
            assetGUID: "blah-blah",
            assetType: "show",
            channelId: "3",
            showLogos: [],
            showImages: [],
            assetName: "assetName03",
            contentType:"contentType03",
            episodeCount: 3
        },
        {
            channelId: "1",
            assetType: "live",
            assetGUID: "guid",
            showImages: [],
            showLogos: [] ,
            assetName: "assetName01",
            contentType:"contentType01",
            episodeCount: 1
        },
        {
            channelId: "2",
            assetType: "live",
            assetGUID: "guid2",
            showImages: [],
            showLogos: [],
            assetName: "assetName02",
            contentType:"contentType02",
            episodeCount: 2
        },
        {
            channelId: "2",
            assetType: "episode",
            assetGUID: "guid2",
            showImages: [],
            showLogos: [],
            assetName: "assetName02",
            contentType:"contentType02",
            episodeCount: 2
        },
        {
            channelId: "2",
            assetType: "episode",
            assetGUID: "guid2",
            showImages: [],
            showLogos: [],
            assetName: "assetName02",
            contentType:"contentType02",
            episodeCount: 2
        }
        ];

    public static getSpy(): any
    {
        this._spy.favorites = observableOf(FavoriteServiceMock.favorites);

        return this._spy;
    }
}
