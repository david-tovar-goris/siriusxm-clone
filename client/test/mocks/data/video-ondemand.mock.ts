import {onDemandEpisodeMock} from "./ondemand-episode.mock";

export const offlinePlaybackMock = {
    highProfile: 0,
    lowProfile: 0,
    mediumProfile: 0
};

export const vodEpisodeMock = {
    ...onDemandEpisodeMock,
    allowDownload: true,
    disableAutoBanners: true,
    disableRecommendations: true,
    hosts: '',
    hostList: [''],
    topics: '',
    topicList: [''],
    rating: '',
    offlinePlayback: offlinePlaybackMock
};

