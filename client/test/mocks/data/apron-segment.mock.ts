import { IApronSegment } from "../../../../servicelib/src/mediaplayer/media-asset-metadata.interface";
import { IMediaSegment } from "sxmServices";

export const apronSegmentMock: IApronSegment = {
    title: 'segment title',
    timeDisplay: '',
    startTimeInSeconds: 0,
    endTimeInSeconds: 0,
    secondsFromBeginningOfEpisode: 0,
    mediaSegment: {} as IMediaSegment
};
