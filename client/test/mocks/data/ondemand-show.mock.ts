import {onDemandShowDescriptionMock} from "./ondemand-show-description.mock";
import {aodEpisodeMock} from "./aod-episode.mock";
import {vodEpisodeMock} from "./video-ondemand.mock";

export const onDemandShowMock = {
    type: '',
    disableAllBanners: true,
    episodeCount: 0,
    newEpisodeCount: 0,
    showDescription: onDemandShowDescriptionMock,
    aodEpisodes: [aodEpisodeMock],
    vodEpisodes: [vodEpisodeMock],
    isFavorite: true
};
