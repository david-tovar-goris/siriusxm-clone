import {timeMock} from "./time.mock";

export const mediaItemMock = {
    assetGUID: '',
    layer: '',
    times: timeMock
};
