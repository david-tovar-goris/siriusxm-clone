export const screensInfo = [{
    "id": "iap_welcome_screen",
    "fields": [{
        "name": "sxm-logo",
        "type": "image",
        "value": "%Image%/Images/screen/sxmLogo.png"
    }, {
        "name": "img-group",
        "fields": [{
            "name": "sxm-logo1",
            "type": "image",
            "value": "%Image%/Images/screen/image-1.png"
        }, {
            "name": "sxm-logo2",
            "type": "image",
            "value": "%Image%/Images/screen/image-2.png"
        }, {
            "name": "sxm-logo3",
            "type": "image",
            "value": "%Image%/Images/screen/image-3.png"
        }]
    }, {
        "name": "button1",
        "supportedLocale": "US,CA",
        "actionNeriticLink": "AppScreen:screen:iap_subscribe_screen:flowType=subscribe",
        "type": "input-button",
        "value": "SUBSCRIBE",
        "supportedPlatform": "ios,web,android"
    }, {
        "name": "button2",
        "supportedLocale": "US,CA",
        "actionNeriticLink": "App:video:%Image%/images/screen/tease-reel.mp4;AppScreen:screen:iap_subscribe_screen:flowType=explore",
        "type": "input-button",
        "value": "EXPLORE",
        "supportedPlatform": "ios,web,android"
    }, {
        "name": "bg-img",
        "type": "image",
        "value": "%Image%/images/screen/background.png"
    }, {
        "name": "footer",
        "type": "sub-screen",
        "value": "AppScreen:screen:iap_footer_subscribe_flow"
    }]
}, {
    "id": "iap_subscribe_screen",
    "fields": [{
        "name": "sxm-logo",
        "type": "image",
        "value": "%Image%/images/screen/sxmLogo.png"
    }, {
        "name": "header",
        "type": "text",
        "value": "please register now to enjoy awesome content."
    }, {
        "name": "error_email_invalid",
        "type": "text",
        "value": "Sorry, email address is not valid"
    }, {
        "name": "error_account_exists",
        "type": "text",
        "value": "please login, you already have a streaming account."
    }, {
        "name": "enter_email",
        "type": "input-text",
        "value": "email"
    }, {
        "name": "enter_password",
        "type": "input-text",
        "value": "password"
    }, {
        "name": "tc_checkbox",
        "type": "input-checkbox",
        "value": "I accept and agree to SiriusXM?s <a href='https://xxxx/customer_agreement.html'>Customer Agreement</a> and <a href='https://xxxx/privacy_policy.html'>Privacy Policy.</a>"
    }, {
        "name": "button1",
        "actionNeriticLink": "App:api:auth",
        "type": "input-button",
        "value": "ACCEPT & CONTINUE"
    }, {
        "name": "bg_img",
        "type": "image",
        "value": "%Image%/images/screen/background.png"
    }, {
        "name": "footer",
        "type": "sub-screen",
        "value": "AppScreen:screen:iap_footer_subscribe_flow"
    }]
}, {
    "id": "iap_accessNow_screen",
    "fields": [{
        "name": "sxm-logo",
        "type": "image",
        "value": "%Image%/images/screen/sxmLogo.png"
    }, {
        "name": "header",
        "type": "text",
        "value": "Sign in to start streaming."
    }, {
        "name": "error_email_invalid",
        "type": "text",
        "value": "Sorry, email address is not valid"
    }, {
        "name": "enter_email",
        "type": "input-text",
        "value": "email"
    }, {
        "name": "enter_password",
        "type": "input-text",
        "value": "password"
    }, {
        "name": "button1",
        "actionNeriticLink": "App:iap_auth",
        "type": "input-button",
        "value": "SIGN IN"
    }, {
        "name": "forgot_password",
        "type": "text",
        "value": "<a href = 'https://xxxxxx/forgot_password.html' > Forgot Your Password? </a>"
    }, {
        "name": "bg-img",
        "type": "image",
        "value": "%Image%/images/screen/background.png"
    }, {
        "name": "footer",
        "type": "sub-screen",
        "value": "AppScreen:screen:iap_footer_accessNow_flow"
    }]
}, {
    "id": "error_121",
    "fields": [{
        "name": "sxm-logo",
        "type": "image",
        "value": "%Image%/images/screen/sxmLogo.png"
    }, {
        "name": "header",
        "type": "text",
        "value": "Please sign in, you already have a streaming account."
    }, {
        "name": "enter_email",
        "type": "input-text",
        "value": "email"
    }, {
        "name": "enter_password",
        "type": "input-text",
        "value": "password"
    }, {
        "name": "button1",
        "actionNeriticLink": "App:iap_auth",
        "type": "input-button",
        "value": "SIGN IN"
    }, {
        "name": "forgot_password",
        "type": "text",
        "value": "<a href = 'https://xxxxxx/forgot_password.html' > Forgot Your Password? </a>"
    }, {
        "name": "bg-img",
        "type": "image",
        "value": "%Image%/images/screen/background.png"
    }, {
        "name": "footer",
        "type": "sub-screen",
        "value": "AppScreen:screen:iap_footer_accessNow_flow"
    }]
}, {
    "id": "iap_footer_subscribe_flow",
    "fields": [{
        "name": "text1",
        "type": "text",
        "value": "Already a subscriber or have a trial in your car?"
    }, {
        "name": "button_link",
        "actionNeriticLink": "AppScreen:screen:iap_accessNow_screen:flowType=accessnow",
        "type": "input-button",
        "value": "Sign In or Sign Up"
    }, {
        "name": "customer_agreement",
        "value": "<a href = 'http://www.siriusxm.com/pdf/siriusxm_customeragreement_eng.pdf'>Customer Agreement</a>"
    }, {
        "name": "privacy_policy",
        "value": "<a href = 'http://www.siriusxm.com/pdf/siriusxm_privacypolicy_eng.pdf'>Privacy Policy</a>"
    }, {
        "name": "ccpa_request",
        "value": "<a href = 'https://www.siriusxm.com/ccparequest_DoNotSellMyInfo'>Do Not Sell My Personal Information</a>"
    }]
}, {
    "id": "iap_footer_accessNow_flow",
    "fields": [{
        "name": "text1",
        "type": "text",
        "value": "Already have SiriusXM and need a login for streaming?  Let's link your in-car subscription or trial."
    }, {
        "name": "button_outline",
        "actionNeriticLink": "App:flepz",
        "type": "input-button",
        "value": "Sign up for streaming"
    }, {
        "name": "customer_agreement",
        "value": "<a href = 'http://www.siriusxm.com/pdf/siriusxm_customeragreement_eng.pdf'>Customer Agreement</a>"
    }, {
        "name": "privacy_policy",
        "value": "<a href = 'http://www.siriusxm.com/pdf/siriusxm_privacypolicy_eng.pdf'>Privacy Policy</a>"
    }, {
        "name": "ccpa_request",
        "value": "<a href = 'https://www.siriusxm.com/ccparequest_DoNotSellMyInfo'>Do Not Sell My Personal Information</a>"
    }]
}, {
    "id": "iap_upsell_banner_subscribe",
    "fields": [{
        "name": "text1",
        "type": "text",
        "value": "Your preview expires in {HH:mm}"
    }, {
        "name": "text2",
        "type": "text",
        "value": "Subscribe to get more from SiriusXM."
    }, {
        "name": "button1",
        "actionNeriticLink": "AppScreen:screen:iap_select_package",
        "type": "input-button",
        "value": "SUBSCRIBE TO STREAMING"
    }, {
        "name": "bg-img",
        "type": "image",
        "value": "%Image%/images/screen/background.png"
    }]
}, {
    "id": "iap_upsell_banner_try",
    "fields": [{
        "name": "text1",
        "type": "text",
        "value": "Not sure? Explore Premier for up to 3 hours free."
    }, {
        "name": "button1",
        "actionNeriticLink": "AppScreen:screen:for_you",
        "type": "input-button",
        "value": "TRY IT OUT"
    }, {
        "name": "bg_img",
        "type": "image",
        "value": "%Image%/images/screen/background.png"
    }]
}, {
    "id": "iap_get_started_premier",
    "fields": [{
        "name": "header",
        "type": "text",
        "value": "Welcome to Platinum"
    }, {
        "name": "description",
        "type": "text",
        "value": "Start by searching for channels, listening to what's on air now, and exploring all you can do with Premier."
    }, {
        "name": "button1",
        "actionNeriticLink": "Api:refresh_session",
        "type": "input-button",
        "value": "START STREAMING"
    }, {
        "name": "bg-img",
        "type": "image",
        "value": "%Image%/images/screen/premier-plan-background.jpg"
    }]
}, {
    "id": "iap_get_started_essential",
    "fields": [{
        "name": "header",
        "type": "text",
        "value": "Welcome to Essential"
    }, {
        "name": "description",
        "type": "text",
        "value": "Start by searching for channels, listening to what's on air now, and exploring all you can do with Essential."
    }, {
        "name": "button1",
        "actionNeriticLink": "Api:refresh_session",
        "type": "input-button",
        "value": "START STREAMING"
    }, {
        "name": "bg-img",
        "type": "image",
        "value": "%Image%/images/screen/essential-plan-background.jpg"
    }]
}, {
    "id": "iap_appStoreBilled_subscription_expired",
    "fields": [{
        "name": "header",
        "type": "text",
        "value": "Your Subscription Expired"
    }, {
        "name": "description",
        "type": "text",
        "value": "But, this doesn't have to be the end. Resubscribe and keep streaming the wide variety of content you love."
    }, {
        "name": "button_filled",
        "actionNeriticLink": "AppScreen:screen:iap_select_package",
        "type": "input-button",
        "value": "RESUBSCRIBE"
    }, {
        "name": "button_text",
        "actionNeriticLink": "AppScreen:screen:iap_welcome_screen",
        "type": "input-button",
        "value": "NOT NOW"
    }]
}, {
    "id": "iap_directBilled_subscription_expired",
    "fields": [{
        "name": "header",
        "type": "text",
        "value": "Your Subscription Expired"
    }, {
        "name": "description",
        "type": "text",
        "value": "Unfortunately, in-car plans can?t be renewed in the app.  Please contact SiriusXM or you can continue to subscribe to streaming only."
    }, {
        "name": "button_outline",
        "actionNeriticLink": "AppScreen:screen:iap_welcome_screen",
        "type": "input-button",
        "value": "GET STREAMING ONLY"
    }]
}, {
    "id": "error_122",
    "fields": [{
        "name": "header",
        "type": "text",
        "value": "Subscribe to Keep Listening"
    }, {
        "name": "description",
        "type": "text",
        "value": "Your free access has ended."
    }, {
        "name": "button_filled",
        "actionNeriticLink": "AppScreen:screen:iap_select_package",
        "type": "input-button",
        "value": "SUBSCRIBE TO STREAMING"
    }, {
        "name": "text1",
        "type": "text",
        "value": "Already subscribed? Access your account"
    }, {
        "name": "button1",
        "actionNeriticLink": "AppScreen:screen:iap_accessNow_screen:flowType=accessnow",
        "type": "input-button",
        "value": "Log In"
    }]
}, {
    "id": "iap_legalese",
    "fields": []
}, {
    "id": "iap_deep_link_screen",
    "fields": [{
        "name": "deeplink"
    }, {
        "name": "button1",
        "actionNeriticLink": "AppScreen:screen:iap_subscribe_screen:flowType=subscribe",
        "type": "input-button",
        "value": "SUBSCRIBE"
    }, {
        "name": "button2",
        "actionNeriticLink": "App:video:%Image%/images/screen/tease-reel.mp4;AppScreen:screen:iap_subscribe_screen:flowType=explore",
        "type": "input-button",
        "value": "EXPLORE"
    }, {
        "name": "bg-img",
        "type": "image",
        "value": "%Image%/images/screen/background.png"
    }, {
        "name": "footer",
        "type": "sub-screen",
        "value": "AppScreen:screen:iap_footer_subscribe_flow"
    }]
}, {
    "id": "iap_auth_get_packages",
    "fields": [{
        "name": "upsell_banner_try",
        "type": "sub-screen",
        "value": "AppScreen:screen:iap_upsell_banner_try"
    }, {
        "name": "get_packages",
        "type": "sub-screen",
        "value": "App:carousel?page-name=iap_get_packages&app-id=%{app-id%}&store=%{store%}"
    }, {
        "name": "legal_text",
        "type": "sub-screen",
        "value": "AppScreen:screen:iap_packages_legal_footer"
    }]
}, {
    "id": "iap_select_package",
    "fields": [{
        "name": "title",
        "value": "Select Your Plan"
    }, {
        "name": "get_packages",
        "type": "sub-screen",
        "value": "App:carousel?page-name=iap_get_packages&app-id=%{app-id%}&store=%{store%}"
    }, {
        "name": "legal_text",
        "type": "sub-screen",
        "value": "AppScreen:screen:iap_packages_legal_footer"
    }]
}, {
    "id": "iap_manage_subscription",
    "fields": [{
        "name": "title",
        "value": "Manage Your Subscription"
    }, {
        "name": "get_packages",
        "type": "sub-screen",
        "value": "App:carousel?page-name=iap_manage_subscription&app-id=%{app-id%}&store=%{store%}"
    }, {
        "name": "legal_text",
        "type": "sub-screen",
        "value": "AppScreen:screen:iap_packages_legal_footer"
    }]
}, {
    "id": "iap_packages_legal_footer",
    "fields": [{
        "name": "legal_text1",
        "value": "Payment will be charged to your Google Play account at the end of your trial or confirmation of purchase if you are not starting a trial. Subscription automatically renews unless it is canceled at least 24 hours before the end of your trial or current  period. There are no refunds. You can manage and cancel your subscriptions by going to your account settings on the App Store after purchase. Offer Terms apply. Skips may be limited by certain licensing restrictions. For more information, please visit our <a href = 'App:web:http://www.siriusxm.com/customeragreement'>Terms of Use</a>, <a href = 'App:web:http://www.siriusxm.com/privacy'>Privacy Policy</a>, and <a href = ''>Subscription Terms</a>."
    }, {
        "name": "legal_text2",
        "value": "Already Subscribed? <a href = ''>Restore Purchase</a>"
    }]
}, {
    "id": "for_you",
    "fields": [{
        "name": "upsell_banner_subscribe",
        "type": "sub-screen",
        "value": "AppScreen:screen:iap_upsell_banner_subscribe"
    }]
}];


export const welcomePageScreenInfo = {
    backgroundImg: "https://siriusxm-priuatart.akamaized.net/images/screen/background.png",
    buttonOne: {
        text: "SUBSCRIBE",
        neriticAction: {
            app: "",
            flowType: "subscribe",
            screen: "iap_subscribe_screen",
            videoUrl: ""
        }
    },
    buttonTwo: {
        text: "EXPLORE",
        neriticAction: {
            app: "",
            flowType: "explore",
            screen: "iap_subscribe_screen",
            videoUrl: "https://siriusxm-priuatart.akamaized.net/images/screen/tease-reel.mp4"
        }
    },
    carouselImages: [
        "https://siriusxm-priuatart.akamaized.net/images/screen/image-1.png",
        "https://siriusxm-priuatart.akamaized.net/images/screen/image-2.png",
        "https://siriusxm-priuatart.akamaized.net/images/screen/image-3.png"
    ],
    logo: "https://siriusxm-priuatart.akamaized.net/images/screen/sxmLogo.png",
    subScreenName: "iap_footer_subscribe_flow"
};

export const footerInfo = {
    accessBtn: {
        text: "Sign In or Sign Up",
        neriticAction: {
            app: "",
            flowType: "accessnow",
            screen: "iap_accessNow_screen",
            videoUrl: ""
        }
    },
    carouselImages: [
        {
            link: "<a href = 'http://www.siriusxm.com/pdf/siriusxm_customeragreement_eng.pdf'>Customer Agreement</a>",
            name: "customer_agreement"
        },
        {
            link: "<a href = 'http://www.siriusxm.com/pdf/siriusxm_privacypolicy_eng.pdf'>Privacy Policy</a>",
            name: "privacy_policy"
        },
        {
            link: "<a href = 'https://www.siriusxm.com/ccparequest_DoNotSellMyInfo'>Do Not Sell My Personal Information</a>",
            name: "ccpa_request"
        }
    ],
    accessHeader: "Already a subscriber or have a trial in your car?",
    subScreenName: "iap_footer_subscribe_flow"
};
