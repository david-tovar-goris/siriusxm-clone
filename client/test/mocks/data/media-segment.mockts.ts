import { mediaItemMock } from "./media-item.mock";
import { legacyIdMock } from "./legacy-id.mock";

export const mediaSegmentMock = {
    ...mediaItemMock,
    duration: 0,
    legacyId: legacyIdMock,
    longDescription: '',
    shortDescription: '',
    endTimeInSeconds: 0,
    startTimeInSeconds: 0,
    type: ''
};
