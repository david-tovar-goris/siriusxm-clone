import { mediaItemMock } from "./media-item.mock";
import { legacyIdMock } from "./legacy-id.mock";

export const mediaCutMock = {
    ...mediaItemMock,
    consumptionInfo: '',
    duration: 0,
    legacyId: legacyIdMock,
    title: '',
    cutContentType: '',
    contentType: ''
};
