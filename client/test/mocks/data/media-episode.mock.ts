import {mediaShowMock} from "./media-show.mock";
import {hostMock} from "./host.mock";
import {drmInfoMock} from "./drm-info.mock";
import {dmcaInfoMock} from "./dmca-info.mock";
import {mediaItemMock} from "./media-item.mock";
import { mediaSegmentMock } from "./media-segment.mockts";

export const mediaEpisodeMock = {
    ...mediaItemMock,
    allowDownload : false,
    disableAllBanners : false,
    disableAutoBanners : false,
    disableRecommendations : false,
    duration : 0,
    episodeGUID : "",
    dmcaInfo: dmcaInfoMock,
    drmInfo: drmInfoMock,
    highlighted: true,
    hosts: [hostMock],
    hot: true,
    legacyId: {},
    longTitle: '',
    longDescription: '',
    mediumTitle: '',
    originalIsoAirDate: '',
    shortDescription: '',
    show: mediaShowMock,
    valuable: true,
    segments: [
        mediaSegmentMock
    ]
};
