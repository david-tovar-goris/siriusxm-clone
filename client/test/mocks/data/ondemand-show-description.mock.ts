import {connectInfoMock} from "./connect-info.mock";
import {showImageMock} from "./image.mock";

export const onDemandShowDescriptionMock = {
    connectInfo: connectInfoMock,
    guid: '',
    images: [showImageMock],
    shortId: '',
    longDescription: '',
    shortDescription : '',
    longTitle: '',
    mediumTitle: '',
    programType: '',
    relatedChannelIds: ['']
};
