import {connectInfoMock} from "./connect-info.mock";
import {futureAiringMock} from "./future-airing.mock";
import {showImageMock} from "./image.mock";
import {mediaItemMock} from "./media-item.mock";

export const mediaShowMock = {
    ...mediaItemMock,
    isPlaceholderShow : false,
    isLiveVideoEligible : false,
    programType : "",
    aodEpisodeCount: 0,
    connectInfo: connectInfoMock,
    disableRecommendations: true,
    futureAirings: [futureAiringMock],
    images: [showImageMock],
    legacyId: {},
    longTitle: '',
    longDescription: '',
    mediumTitle: '',
    shortDescription: '',
    type: '',
    channelId: ''
};
