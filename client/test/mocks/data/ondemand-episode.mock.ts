import {onDemandShowMock} from "./ondemand-show.mock";

export const onDemandEpisodeMock = {
    disableAllBanners: true,
    episodeGuid: '',
    show: onDemandShowMock,
    longDescription: '',
    shortDescription : '',
    longTitle: '',
    mediumTitle: '',
    isFavorite: true
};
