import { ITile, ITileImage, ITilePosition, ITileSize } from "sxmServices";
import { Observable ,  BehaviorSubject } from "rxjs";
import { imageMock } from "./image.mock";
import {IAssetInfoType} from "../../../../servicelib/src/carousel/carousel.types";

export const v2RawTileAssetInfo: IAssetInfoType = {
    assetInfoKey: '',
    assetInfoValue: ''
};

export const tilePositionMock: ITilePosition = {
    left: 0,
    top: 0
};

export const tileSizeMock: ITileSize = {
    width: 0,
    height: 0
};

export const tileImageMock: ITileImage = {
    artwork: imageMock,
    layer: 0,
    renderPosition: tilePositionMock,
    renderSize: tileSizeMock
};

export const tileMock: ITile = {
    bgImageUrl: '',
    fgImageUrl: '',
    assetGuid: '',
    primaryNeriticLink: { contentType: "", channelId: '' },
    tileAssetInfo: { showGuid: '', channelId: '' },
    tileBanner: {display: true, bannerClass: '', bannerText: '', bannerColor: ''},
    line1: '',
    line1$: new BehaviorSubject<string>("") as any,
    line2: '',
    line2$: new BehaviorSubject<string>("") as any,
    reminders: {liveVideoReminderSet: false, showReminderSet: false},
    images: [tileImageMock],
    tileAssets: [v2RawTileAssetInfo],
    isTunedTo$: new BehaviorSubject<boolean>(false)
};
