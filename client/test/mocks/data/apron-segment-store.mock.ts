import { apronSegmentMock } from "./apron-segment.mock";
import { IApronSegment } from "../../../../servicelib/src/mediaplayer/media-asset-metadata.interface";

function createSegmentsForStoreMock(startingSegment: IApronSegment, length: number): Array<IApronSegment>
{
    let results: Array<IApronSegment> = [];
    let newSegment = createSegment(startingSegment, true);
    results.push(newSegment);

    for (let i = 0; i < (length - 1); i++)
    {
        newSegment = createSegment(newSegment);
        results.push(newSegment);
    }

    return results;

}

function createSegment(segment: IApronSegment, preventStartChange: boolean = false): IApronSegment
{
    let newSegment: IApronSegment = cloneSegment(segment);

    if (!preventStartChange)
    {
        newSegment.startTimeInSeconds = (newSegment.startTimeInSeconds + 300);
    }

    newSegment.endTimeInSeconds = (newSegment.startTimeInSeconds + 300);
    return newSegment;
}

function cloneSegment(segment: IApronSegment): IApronSegment
{
    return Object.assign({}, segment);
}

const segmentsMock: Array<IApronSegment> = createSegmentsForStoreMock(apronSegmentMock, 5);

export const apronSegmentStoreMock = {
    segments: segmentsMock,
    displaySegments: true,
    canExpandSegmentsView: true,
    currentlyPlayingSegment: segmentsMock[0],
    liveSegment: segmentsMock[0]
};
