import { ENowPlayingStatus } from "app/now-playing/now-playing-status.enum";
import {
    IMediaEpisode,
    IPlayhead
} from "sxmServices";
import { dmcaInfoMock } from "./dmca-info.mock";
import { mediaCutMock } from "./media-cut.mock";
import { mediaEpisodeMock } from "./media-episode.mock";
import { mediaShowMock } from "./media-show.mock";
import { createFeatureSelector, createSelector } from "@ngrx/store";
import { INowPlayingStore } from "../../../src/app/common/store/now-playing.store";

export const nowPlayingStoreMock = {
    albumName: '',
    albumImage: '',
    artistName: '',
    artistInfo: '',
    backgroundImage: '',
    backgroundColor: '',
    channelName: '',
    channelNumber: '',
    contentType: "",
    cutContentType: "",
    dmcaInfo: dmcaInfoMock,
    cut: mediaCutMock,
    episode: mediaEpisodeMock,
    lastPlayheadTimestamp : 0,
    mediaId: '',
    mediaType: '',
    nowPlayingStatus: ENowPlayingStatus.ENTERING_FIRST_TIME,
    playhead: {
        currentTime : {
            milliseconds: 0,
            seconds: 0,
            zuluMilliseconds: 0,
            zuluSeconds: 0
        }
    } as IPlayhead,
    show: mediaShowMock,
    showMiniPlayer: false,
    startTime: 0,
    trackName: '',
    type: '',
    videoPlayerData: {
        mediaAssetMetadata: {
            apronSegments: [],
            episodeTitle: "",
            seriesName: "",
            mediaId: ""
        },
        playheadTime: 0,
        vodEpisode: {} as IMediaEpisode
    },
    video: null,
    youJustHeard: [],
    stationId: ''
};


export const selectNowPlayingState = createFeatureSelector<INowPlayingStore>("nowPlayingStoreMock");


export const getCurrentEpisodeZuluStartTime = createSelector(
    selectNowPlayingState,
    (state: INowPlayingStore): number =>
    {
        return 1000;

    }
);
