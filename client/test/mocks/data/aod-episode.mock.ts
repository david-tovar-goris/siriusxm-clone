import {dmcaInfoMock} from "./dmca-info.mock";
import {drmInfoMock} from "./drm-info.mock";
import {publicationInfoMock} from "./publication-info.mock";
import {channelMock} from "./channel.mock";
import {onDemandEpisodeMock} from "./ondemand-episode.mock";

export const aodEpisodeMock = {
    ...onDemandEpisodeMock,
    type: '',
    episodeGuid: '1234',
    aodEpisodeGuid: '1234',
    baseTime: '',
    expiringSoon: true,
    featured: true,
    featuredPriority: true,
    highlighted: true,
    hot: true,
    originalAirDate: '',
    percentConsumed: 1,
    programType: '',
    special: true,
    valuable: true,
    channels: [channelMock],
    contentUrlList: [''],
    contextualBanners: [''],
    dmcaInfo: dmcaInfoMock,
    drmInfo: drmInfoMock,
    hosts: [''],
    publicationInfo: publicationInfoMock,
    assetGuid : "assetGuid"
};
