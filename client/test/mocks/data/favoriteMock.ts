import {showImageMock} from "./image.mock";

export const favoriteItemMock = {
    assetGUID: '',
    assetType: 'show',
    assetName: '',
    contentType: '',
    episodeCount: 0,
    channelId: '1',
    showLogos: [showImageMock],
    showImages: [showImageMock]
};
