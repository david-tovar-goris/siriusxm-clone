export const playheadTimeMock = {
    milliseconds: 0,
    seconds: 0,
    zuluMilliseconds: 0,
    zuluSeconds: 0
};

export const playheadMock = {
    currentTime: playheadTimeMock,
    startTime: playheadTimeMock
};
