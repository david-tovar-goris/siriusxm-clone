export const imageMock = {
    name: '',
    platform: '',
    url: '',
    width: 0,
    height: 0
};

export const showImageMock = {
    ...imageMock,
    encrypted: true,
    type: ''
};
