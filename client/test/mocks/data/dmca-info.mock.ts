export const dmcaInfoMock = {
    backSkipDur: 1,
    channelContentType: '',
    fwdSkipDur: 1,
    irNavClass: '',
    maxBackSkips: 1,
    maxFwdSkips: 1,
    maxSkipDur: 1,
    maxTotalSkips: 1,
    playOnSelect: ''
} ;
