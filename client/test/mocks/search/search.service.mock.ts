import { of as observableOf } from "rxjs";

export class SearchServiceMock
{
    private static _spy = jasmine.createSpyObj("mockSearchServiceSpy", [
        'clearSearchResults',
        'updateSearchKeyword'
    ]);

    public static recentSearchResults = [];
    public static autoCompleteSearchResults = [];
    public static directTuneSearchResults = [

        {
            isGeoRestricted: false,
            aodEpisodeCount: 4,
            aodShowCount: 4,
            categoryList: [],
            channelGuid: "c2a517d4-c02f-06eb-77cf-f81bfa719188",
            channelId: "hotjamz",
            channelNumber: 46,
            clientBufferDuration: 18000,
            disableRecommendations: false,
            freeToAir: true,
            geoRestrictions: 0,
            imageList: [],
            inactivityTimeout: 0,
            ipOnly: false,
            isAvailable: true,
            isBizMature: true,
            isFavorite: false,
            isMature: false,
            isMySxm: true,
            isPersonalized: false,
            isPlayByPlay: false,
            liveDelay: 160,
            markerLists: [],
            mediumDescription: "What’s HOT now, in R&B and Hip-Hop!",
            name: "The Heat",
            phonetics: [],
            phoneticsVersion: 0,
            playingArtist: "Kendrick Lamar/Zacari",
            playingTitle: "LOVE.",
            satOnly: false,
            shortDescription: "Today's R&B Hits",
            shows: [],
            siriusChannelNumber: 46,
            sortOrder: 570,
            spanishContent: false,
            streamingName: "Today's R&B Hits",
            subscribed: true,
            type: "live",
            upsell: false,
            url: "http://www.siriusxm.com/theheat",
            xmChannelNumber: 46,
            tuneMethod: "",
            xmServiceId: 67
        }
    ];

    public static getSpy(): any
    {
        this._spy.recentSearchResults = observableOf(this.recentSearchResults);
        this._spy.directTuneSearchResults = observableOf(this.directTuneSearchResults);
        this._spy.autoCompleteSearchResults = observableOf(this.autoCompleteSearchResults);
        this._spy.searchKeyword = observableOf('pop');
        this._spy.getAutoCompleteSearchResults = function ()
        {
            return observableOf(true);
        };
        this._spy.getSearchResults = function ()
        {
            return observableOf(true);
        };

        return this._spy;
    }
}
