/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

import { of as observableOf } from "rxjs";
import { INotificationFeedback } from "sxmServices";

export class MockNotificationService
{
    constructor() {}

    public notificationInformationSubject = jasmine.createSpyObj('notificationInformationSubject', [
        "next"
    ]);

    public notificationInformation = jasmine.createSpyObj('notificationInformation', [
        "subscribe"
    ]);

    private notificationResult = jasmine.createSpyObj('notificationResult', [
        "next"
    ]);

    private notificationFeedback: INotificationFeedback;

    public open = jasmine.createSpy('open').and.returnValue(observableOf({}));

    public close = jasmine.createSpy('close');
}

export const notificationServiceMock = {
    notificationInformationSubject: jasmine.createSpyObj('notificationInformationSubject', [
        "next"
    ]),
    notificationInformation: jasmine.createSpyObj('notificationInformation', [
        "subscribe"
    ]),
    notificationResult: jasmine.createSpyObj('notificationResult', [
        "next"
    ]),
    notificationFeedback: {},
    open: jasmine.createSpy('open').and.returnValue(observableOf({})),
    close: jasmine.createSpy('close'),
    callAction: jasmine.createSpy('callAction')
};
