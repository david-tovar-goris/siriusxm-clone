/**
 * This is known as a barrel file. It allows developers to package and rollup multiple exports
 * into a single file and thus single import/export; e.g., `export * from "./authentication/service/";`
 */
export * from "./authentication.service.mock";
export * from "./router.mock";
export * from "./channel-list.service.mock";
export * from "./carousel.service.mock";
export * from "./redux.store.mock";
export * from "./channel-lineup.service.mock";
export * from "./favorite-list.store.service.mock";
export * from "./alert.service.mock";

export function getRandomInt(min, max): number
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function createUuid(): string
{
    return String(getRandomInt(0, 1000000));
}
