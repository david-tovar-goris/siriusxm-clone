export class SkipButtonServiceMock
{
    public displaySkip;
    public maxTotalSkips;
    public skipAgainTime;
    public playingData;
    public getIdentifier;

    constructor()
    {
        this.displaySkip = jasmine.createSpy('displaySkip');
        this.skipAgainTime = { date: '2017-12-05T06:00:00-06:00', format() { return "Formatted Date"; } };
        this.maxTotalSkips = 6;
        this.playingData = { channelId: "siriushits1" };
        this.getIdentifier = jasmine.createSpy('getIdentifier');
    }
}
