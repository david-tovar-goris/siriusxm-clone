import { of as observableOf } from "rxjs";
import { liveTimeMock } from "./data/live-time.mock";

export class LiveTimeServiceMock
{
    public liveTime$;

    constructor()
    {
        this.liveTime$ = observableOf(liveTimeMock);
    }
}
