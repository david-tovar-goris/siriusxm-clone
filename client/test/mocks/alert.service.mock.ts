import { of as observableOf } from "rxjs";

export class AlertServiceMock
{
    public notificationAlerts = observableOf({});
    constructor()
    {
    }

    public createAlert(channelId: string, assetGuid: string, contentType: string): void
    {
        return;
    }

    public getAlert()
    {
        return null;
    }
}
