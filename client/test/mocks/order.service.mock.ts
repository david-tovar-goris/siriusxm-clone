import { Observable, of as observableOf } from "rxjs";

export class OrderServiceMock
{
    dragItemIndex;
    dragOverInedx: number | null = 0;

    getDragOverIndex (): Observable<number>
    {
        return observableOf(this.dragOverInedx);
    }

    setDragItemIndex (num: number): void
    {
         this.dragItemIndex = num;
    }

    setDragOverIndex(num: number): void
    {
        this.dragOverInedx = num;
    }

    updateFavorites(): void
    {

    }
}
