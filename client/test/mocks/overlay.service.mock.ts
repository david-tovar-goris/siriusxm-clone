import { of as observableOf,  BehaviorSubject } from "rxjs";

export class OverlayServiceMock
{

    private static overlayInformation = {
        open: true,
        data: {
            header: 'Header',
            description: "DDescription",
            buttonOneText: "OK",
            buttonTwoText: "Cancel",
            analyticsTag: {close: false, ok:false}
        },
        config: {
            overlayData: {

            }
        }
    };

    private static _spy = jasmine.createSpyObj("overlayServiceSpy", [
        "open",
        "close"
    ]);

    public static getSpy(): any
    {
        this._spy.overlayInformation = new BehaviorSubject({
            open:false,
            data:null
        });
        return this._spy;
    }

    public static open(data)
    {
        this._spy.overlayInformation.next(this.overlayInformation);
        OverlayServiceMock.getSpy().open.and.returnValue(observableOf(null));
    }

}
