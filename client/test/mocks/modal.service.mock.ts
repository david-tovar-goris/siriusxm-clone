import { BehaviorSubject } from "rxjs";

export class ModalServiceMock
{

    private static modalInformation ={
        open:true,
        data:{
            header: 'Header',
            description: "Description",
            buttonOneText: "OK",
            buttonTwoText: "Cancel"
        }
    };

    private static _spy = jasmine.createSpyObj("modalServiceSpy", [
        "addDynamicModal",
        "setRootViewContainerRef"
    ]);

    public static getSpy(): any
    {
        this._spy.overlayInformation = new BehaviorSubject({
            open:false,
            data:null
        });
        return this._spy;
    }
}
