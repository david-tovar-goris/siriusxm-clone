import { of as observableOf, throwError as observableThrowError, Observable } from 'rxjs';
import { FreeTierStatus, ISession } from "sxmServices";

export class AuthenticationServiceMock
{
    private static session: ISession = {
        username: "",
        authenticated: true,
        exited : false,
        forgotPassword: false,
        startTrial: false,
        loggedOut: false,
        loginAttempts: 0,
        loginFaultCode: 0,
        loginFaultMessages: null,
        timeStamp: Date.now(),
        sessionID: "12345",
        accountLocked: false,
        accountExpired: false,
        trialAccountExpired: false,
        remainingLockoutMinutes: 5,
        remainingLockoutSeconds: 20,
        activeOpenAccessSession: false,
        openAccessStatus: "unavailable",
        isInPrivateBrowsingMode: false,
        duplicateLogin: false,
        itDown : false,
        activeFreeTierSession: false,
        freeTierStatus: FreeTierStatus.UNAVAILABLE
    };

    private static _spy = jasmine.createSpyObj("authenticationServiceSpy", [
        "login",
        "logout",
        "session",
        "isAuthenticated",
        "openAccessLogin"
    ]);

    public static loginErrorMsg: string = "Login error";

    public static getSpy(): any
    {
        this._spy.session = AuthenticationServiceMock.session;
        this._spy.userSession = observableOf(AuthenticationServiceMock.session);

        return this._spy;
    }

    public static loginSuccess(): void
    {
        AuthenticationServiceMock.getSpy()
            .login.and.returnValue(observableOf(AuthenticationServiceMock.session));
    }

    public static loginFault(error: {}): void
    {
        error = error || AuthenticationServiceMock.loginErrorMsg;
        AuthenticationServiceMock.getSpy()
            .login.and.returnValue(observableThrowError(error));
    }

    public login(userName, password, screenFlow): Observable<any>
    {
        return observableOf({});
    }
}
