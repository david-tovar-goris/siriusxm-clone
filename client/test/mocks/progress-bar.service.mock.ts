import { of, Observable } from "rxjs";
import {IEvent} from "sxmServices";

export class ProgressBarServiceMock
{
    public isBeingDragged: Observable<boolean> = of(false);
    public playHeadTime: Observable<number> = of(50);

    public onResize(width, startX){}

    public onClickProgressBar(event, width, startX){}

    public onDragMove = (evt: IEvent) =>
    {
        return true;
    }

    public onDragStop = (evt: IEvent) =>
    {
        return true;
    }

}
