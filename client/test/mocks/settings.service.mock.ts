import { Observable, of as observableOf } from "rxjs";

export class SettingsServiceMock
{
    private static _spy = jasmine.createSpyObj("mockSettingServiceSpy", [
        'updateSettings',
        'isGeneralSettingOn',
        'switchGlobalSettingOnOrOff',
        'getAudioQuality '
    ]);

    public static deviceSettings = [
        {
            name: "AudioQuality",
            value: "Normal"
        }
    ];
    public static globalSettings = [
        {
            name: "TuneStart",
            value: "on"
        },
        {
            name: "MiniPlay",
            value: "on"
        },
        {
            name: "NotificationSubscriptionShowReminders",
            value: "on"
        },
        {
            name: "NotificationSubscriptionSuggestedShow",
            value: "off"
        },
        {
            name: "DisplayAICOverlay",
            value: "on"
        }
    ];

    public isSettingOn = (configName: string, booleanConfigProp: string) => false;

    public static getSpy(): any
    {
        const mockSettings = {
            deviceSettings: SettingsServiceMock.deviceSettings as any[],
            globalSettings: SettingsServiceMock.globalSettings as any[]
        };

        SettingsServiceMock._spy.settings = observableOf(mockSettings);

        SettingsServiceMock._spy.isReminderSettingOn = (settingType, defaultValue:false) =>
        {
            let theSetting = (mockSettings.globalSettings as any[]).find(setting => setting.name === settingType);
            return theSetting ? theSetting.value === "on" : defaultValue;
        };

        SettingsServiceMock._spy.getAudioQuality = () =>
        {
            return mockSettings.deviceSettings[0].value;
        };

        SettingsServiceMock._spy.isGlobalSettingOn = () =>
        {
            return true;
        };

        SettingsServiceMock._spy.isDeviceSettingOn = () =>
        {
            return true;
        };

        SettingsServiceMock._spy.isGeneralSettingOn = (settingType, defaultValue:false) =>
        {
            let theSetting = (mockSettings.globalSettings as any[]).find(setting => setting.name === settingType);
            return theSetting ? theSetting.value === "on" : defaultValue;
        };
        SettingsServiceMock._spy.isPandoraXModalFlagOn = () => true;

        SettingsServiceMock._spy.isCoachmarkFlagOn = (coachmark): any =>
        {
            return false;
        };

        return SettingsServiceMock._spy;
    }
}
