import {
    IChannel,
    IOnDemandEpisode
} from "sxmServices";
import {Observable, of as observableOf} from "rxjs";

export class TuneServiceMock
{
    constructor()
    {
    }

    public tune(channel: IChannel, contentType: string, episode?: IOnDemandEpisode, startTime?: number): Observable<boolean>
    {
        return observableOf(true);
    }

    public tuneResponseSubject(): Observable<any>
    {
        return observableOf(true);
    }
    // public tuneResponseSubject = jasmine.createSpy('tuneResponseSubject').and.returnValue(observableOf(true));
    // tuneResponseSubject = () => observableOf(true);
    tuneLive = () => observableOf(true);
    tuneAOD = () => observableOf(true);
    tuneVOD = () => observableOf(true);
    isPlayingContentSupported = () => true;
    isAICOnByPassMode = () => false;
    isArtistRadioOnByPass = () => false;
}
