export class Skip15SecondsServiceMock
{
    public displaySkipForward;
    public displaySkipBackward;
    public disableBack1Hour = jasmine.createSpy("disableBack1Hour");
    public displaySkip15BackBtn: () => void;
    public isSkipFwd15VisibilityHidden: () => boolean;

    constructor()
    {
        this.displaySkipForward = jasmine.createSpy('displaySkipForward');
        this.displaySkipBackward= jasmine.createSpy('displaySkipForward');
        this.displaySkip15BackBtn = () =>
        {
            return;
        };
        this.isSkipFwd15VisibilityHidden = () =>
        {
            return false;
        };
    }

}
