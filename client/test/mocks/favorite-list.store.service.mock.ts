import { of as observableOf } from "rxjs";
import { favList } from '../../../servicelib/src/test/mocks/favorites/favorite.response';

export class FavoriteListStoreServiceMock
{
    favorites = observableOf({ favorites: {channels:favList} });
    public updateFavorite: object;

    public updateFavorites: object;
    public toggleFavorite: object;
    public getAssetGuid: object;
    public getFavoriteCarousels: object;
    public getFavorite: object;

    constructor()
    {
        this.updateFavorite = jasmine.createSpy("updateFavorite").and.returnValue(observableOf(true));
        this.updateFavorites = jasmine.createSpy("updateFavorites");
        this.toggleFavorite = jasmine.createSpy("toggleFavorite");
        this.getAssetGuid = jasmine.createSpy("getAssetGuid").and.returnValue("assetGuid");
        this.getFavoriteCarousels = jasmine.createSpy("getFavoriteCarousels");
        this.getFavorite = jasmine.createSpy("getFavorite");
    }
}
