import { of as observableOf } from "rxjs";
import { NAME_LIST_VIEW_SHOW_LOGO } from "sxmServices";
export class OnDemandListServiceMock
{
    public selectShow;
    public selectAodEpisode;
    public selectVodEpisode;
    public onDemandStore;

    constructor()
    {
        this.selectShow = jasmine.createSpy('selectShow');
        this.selectAodEpisode = jasmine.createSpy('selectAodEpisode');
        this.selectVodEpisode = jasmine.createSpy('selectVodEpisode');

        this.onDemandStore = observableOf({
            selectedShow: { name: "selectedShow",
                showDescription: {
                    images: [{
                        name: NAME_LIST_VIEW_SHOW_LOGO,
                        width: 150,
                        height: 150 ,
                        url:  "testUrl.jpg"
                    }]
                }
            },
            selectedAodEpisode: {
                mediumTitle: "mediumTitle",
                aodEpisodeGUID: "abc-dinfidf"
            }
        });
    }

    public selectShowByGuid()
    {
        return observableOf(true);
    }

    public selectVodEpisodeByGuid(){}

    public selectAodEpisodeByGuid(){}
}
