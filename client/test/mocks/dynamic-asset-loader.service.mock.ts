import { BehaviorSubject } from "rxjs";
import { IInternalAsset } from "sxmServices";

export class DynamicAssetLoaderServiceMock
{
    public isAssetLoaded$ = new BehaviorSubject({
        assetName: "adobe analytics",
        loaded   : true
    });

    public loadInternalScript(asset: IInternalAsset)
    {}
}
