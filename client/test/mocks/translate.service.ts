import { of as observableOf } from "rxjs";

export class MockTranslateService
{
    private static _spy = jasmine.createSpyObj("mockTranslateServiceSpy", [
        'addLangs',
        'use',
        'setDefaultLang',
        'instant'
    ]);

    public static getSpy(): any
    {
        this._spy.currentLang = 'en';
        this._spy.use = function(lang)
        {
            return;
        };
        this._spy.getBrowserLang = function ()
        {
            return 'en';
        };
        this._spy.onLangChange = observableOf({
            lang: 'en'
        });
        this._spy.onDefaultLangChange = observableOf({});
        this._spy.onTranslationChange = observableOf({});
        this._spy.get = function (key:string)
        {
            let observable;
            switch(key)
            {
                case "common.today":
                    observable = observableOf("Today");
                    break;
                case "common.yesterday":
                    observable = observableOf("Yesterday");
                    break;
                case "common.tomorrow":
                    observable = observableOf("Tomorrow");
                    break;
                case "profile.logoutModalData":
                    observable = observableOf({ buttonTwo: { action: "logout"}});
                    break;
                default:
                    observable = observableOf("AirDate");
            }
            return observable;
        };
        this._spy.instant = function (key)
        {
            switch(key)
            {
                case 'login.errors.invalidLogin':
                    return 'We don\'t recognize your username or password. Please try again.';
                case 'login.errors.modals.accountLocked':
                    return {
                        "header": "Too Many Incorrect Login Attempts",
                        "description": "Your SiriusXM account has been temporarily disabled for the next {{ minutes }}:{{ seconds }} minutes.",
                        "buttonOne": {
                            "target": "iframe_forgotUsernamePassword",
                            "text": "Forgot Username or Password",
                            "link": "https://streaming.siriusxm.com/?src=everestplayer&recoverlogin=true&hideheader=true"
                        },
                        "buttonTwo": {
                            "text": "OK"
                        }
                    };
                case 'login.errors.modals.oacLoginAttempt':
                    return {
                        "header": "Oops!",
                        "description": "It appears you have logged in using your Online Account Center password.  Please log in again using your Streaming password.",
                        "buttonOne": {
                            "target": "iframe_forgotUsernamePassword",
                            "text": "Reset Password",
                            "link": "https://streaming.siriusxm.com/?src=everestplayer&recoverlogin=true&hideheader=true"
                        },
                        "buttonTwo": {
                            "text": "Sign In Again"
                        }
                    };
                case 'login.errors.modals.expiredSubscription':
                    return {
                        "header": "Your SiriusXM Internet Radio has expired. ",
                        "description": "Don't miss everything SiriusXM has to offer. Please purchase a subscription to continue listening. ",
                        "buttonOne": {
                            "target": "redirectToExternalLink",
                            "text": "Subscribe",
                            "link": "https://care.siriusxm.com/expiredsxir"
                        },
                        "buttonTwo": {
                            "text": "Not Now"
                        }
                    };
                case 'login.errors.modals.errForbidden':
                    return {
                        "header": "Too Many Incorrect Login Attempts",
                        "description": "Your SiriusXM account has been temporarily disabled.",
                        "buttonOne": {
                            "target": "iframe_forgotUsernamePassword",
                            "text": "Forgot Username or Password",
                            "link": "https://streaming.siriusxm.com/?src=everestplayer&recoverlogin=true&hideheader=true"
                        },
                        "buttonTwo": {
                            "text": "OK"
                        }
                    };
                default:
                    return 'We don\'t recognize your username or password. Please try again.';
            }
        };
        return this._spy;
    }

}
