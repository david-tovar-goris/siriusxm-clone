import { IAodEpisode, IChannel, IRecentlyPlayed, IVodEpisode } from "sxmServices";
import { createEpisodes , createVODEpisodes  } from"../../test/mocks/channel-list.service.mock";
export class RecentlyPlayedStoreServiceMock
{
    public getRecentlyPlayedData;
    public removeAllRecentlyPlayed;
    public removeRecentlyPlayedItem;

    constructor()
    {
        this.getRecentlyPlayedData = jasmine.createSpy('getRecentlyPlayedData');
        this.removeAllRecentlyPlayed = jasmine.createSpy('removeAllRecentlyPlayed');
        this.removeRecentlyPlayedItem = jasmine.createSpy('removeRecentlyPlayedItem');
    }

    public static mockRecentlyPlayedData: Array<IRecentlyPlayed> =
        [ {
            aodDownload: false,
            aodPercentConsumed: 0,
            assetGuid: "9389",
            assetType: "channel",
            channelGuid: "9389",
            deviceGuid: "ed7764f5-6375-4ab0-96c1-7a40defacb93",
            endDateTime: new Date("2017-10-02T20:56:12"),
            gupId: "D7F5B6220B7026542E3E63C6D2E90B54",
            incognito: false,
            recentPlayType: "live",
            recentlyPlayedGuid: "e6cfda45-e606-4842-8086-ab5e8d0e87ac",
            startDateTime: new Date("2017-10-02T20:56:12"),
            startStreamDateTime: new Date("2017-10-02T20:56:12"),
            showTitle: "showTitle",
            channel: {
                channelNumber : 100,
                playingTitle: "playingTitle",
                firstSuperCategory : {
                    name: "firstCategoryName"
                },
                imageList: [
                    {
                        name    : "list view channel logo",
                        platform: "",
                        width   : 120,
                        height  : 120,
                        url     : "imageUrl1"
                    }]
            } as IChannel,
            aodEpisode :  createEpisodes("id", 1, "name"),
            vodEpisode :  createVODEpisodes("id", 1, "name")
        },
            {
                aodDownload: false,
                aodPercentConsumed: 0,
                assetGuid: "9389",
                assetType: "aodEpisode",
                channelGuid: "9389",
                deviceGuid: "ed7764f5-6375-4ab0-96c1-7a40defacb93",
                endDateTime: new Date("2017-10-02T20:56:12"),
                gupId: "D7F5B6220B7026542E3E63C6D2E90B54",
                incognito: false,
                recentPlayType: "aod",
                recentlyPlayedGuid: "e6cfda45-e606-4842-8086-ab5e8d0e87ac",
                startDateTime: new Date("2017-10-02T20:56:12"),
                startStreamDateTime: new Date("2017-10-02T20:56:12"),
                showTitle: "showTitle",
                channel: {
                    channelNumber : 100,
                    playingTitle: "playingTitle",
                    firstSuperCategory : {
                        name: "firstCategoryName"
                    },
                    imageList: [
                        {
                            name    : "list view channel logo",
                            platform: "",
                            width   : 120,
                            height  : 120,
                            url     : "imageUrl1"
                        }]
                } as IChannel,
                aodEpisode :  createEpisodes("id", 1, "name"),
                vodEpisode :  createVODEpisodes("id", 1, "name")
            }];
}
