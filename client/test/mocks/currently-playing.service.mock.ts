import { of as observableOf } from "rxjs";

export const currentlyPlayingDataMock = {
    cut: {
        times: {

        }
    }
};

export class CurrentlyPlayingServiceMock
{
    public currentlyPlayingData;

    constructor()
    {
        this.currentlyPlayingData = observableOf(currentlyPlayingDataMock);
    }

    getCurrentEpisodeZuluStartTime() { }
}
