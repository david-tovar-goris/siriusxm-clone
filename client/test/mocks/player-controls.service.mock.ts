export class PlayerControlsServiceMock
{
    public liveTime: number = 0;
    public playheadTime: number = 0;
    public segmentMarkerTimestamps: Array<number>;
    public progressBarWidth;

    constructor()
    {
    }

    public destroy(): void
    {
        this.playheadTime = 0;
        this.segmentMarkerTimestamps = [];
        this.progressBarWidth =  jasmine.createSpy('progressBarWidth').and.returnValue(1000);
    }

    public seek(seekTime)
    {
        return seekTime;
    }

    public canGotoLive()
    {
        return false;
    }

    public static restart()
    {
        return true;
    }

    public static gotoLive()
    {
        return true;
    }
}
