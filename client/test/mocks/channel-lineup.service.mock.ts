import { of as observableOf, Observable } from 'rxjs';
import {
    mockChannelLineup
} from "../../../servicelib/src/test/mocks/channel.lineup";
import { mockForYouData } from "./data/for-you.mock";


let superCategoryForYouMock = mockForYouData;
let superCategoryListMock = [
    superCategoryForYouMock
];

export class ChannelLineupServiceMock
{
    public findChannelById;
    public findShowByGuid;
    public findAodEpisodeByGuid;
    public findVodEpisodeByGuid;
    public getVodEpisodeData;
    public findChannelByIdOrGuid;
    public channelLineup = {
        channels: observableOf([])
    };

    constructor()
    {
        this.findChannelById = jasmine.createSpy('findChannelById').and.returnValue({ channelNumber: 100 });
        this.findChannelByIdOrGuid = jasmine.createSpy('findChannelByIdOrGuid').and.returnValue({});
        this.findShowByGuid = jasmine.createSpy('findShowByGuid').and.returnValue(observableOf({}));
        this.findAodEpisodeByGuid = jasmine.createSpy('findAodEpisodeByGuid').and.returnValue(observableOf({}));
        this.findVodEpisodeByGuid = jasmine.createSpy('findVodEpisodeByGuid').and.returnValue(observableOf({}));
        this.getVodEpisodeData = jasmine.createSpy('getVodEpisodeData').and.returnValue(observableOf({}));
    }
}

// Create the mock channel lineup testSubject.
export const channelLineupServiceMock = {
    channelLineup: {
        superCategories: {
            subscribe: (result: Function, fault: Function) => result(superCategoryListMock),
            flatMap: (mapResult: Function) => mapResult(superCategoryListMock)
        },
        channels : {
            subscribe: (result: Function, fault: Function) => result(mockChannelLineup),
            flatMap: (mapResult: Function) => mapResult(mockChannelLineup)
        }

    },
    discoverAod: function ()
    {
        return observableOf([]);
    },
    findChannelById: function ()
    {
        return {};
    },
    findShowByGuid: function ()
    {
        return observableOf({});
    },
    findAodEpisodeByGuid: function ()
    {
        return observableOf({});
    },
    findVodEpisodeByGuid: function ()
    {
        return observableOf({});
    }
};
