import { of as observableOf, BehaviorSubject } from "rxjs";
import { playheadMock } from "./data/playhead.mock";

export class MediaTimestampServiceMock
{
    public livePointTimestamp: number = 300;
    public playheadTimestamp: number = 150;
    public segmentMarkerTimestamps = [];
    public pausePlayheadTimestampUpdate;
    public durationTimestamp: number = 0;
    private dmcaStore;
    private dmcaData;

    /**
     * An observable (hot, subscribe returns most recent item) that can be used to obtain the current live timestamp in seconds.
     */
    public liveTime$ = new BehaviorSubject(100);

    /**
     * An observable (hot, subscribe returns most recent item) that can be used to obtain the current playhead timestamp in seconds.
     */
    public playheadTimestamp$ = new BehaviorSubject(0);

    /**
     * An observable (hot, subscribe returns most recent item) that can be used to obtain the current duration timestamp in seconds.
     */
    public durationTimestamp$ = jasmine.createSpy('durationTimestamp$');

    /**
     * An observable (hot, subscribe returns most recent item) that can be used to obtain the current duration timestamp in seconds.
     */
    public isPlayheadBehindLive$ = new BehaviorSubject<any>(null);

    /**
     * The media type stream.
     */
    public mediaType$ = jasmine.createSpy("mediaType$");

    constructor()
    {
        this.setPlayheadTimestamp = jasmine.createSpy('setPlayheadTimestamp');
    }

    public observePlayheadTime = (fnc = this.setPlayheadTimestamp.bind(this)) =>
    {
        return observableOf(playheadMock).subscribe((playhead) =>
        {
            return observableOf(fnc(playhead.currentTime.seconds));
        });
    }

    public setPlayheadTimestamp = (playheadTimestamp: number) =>
    {
        if (!this.pausePlayheadTimestampUpdate)
        {
            return this.playheadTimestamp = playheadTimestamp;
        }
    }

    public getDurationTimestamp()
    {
        return this.durationTimestamp;
    }

    public isPlayheadTimestampAtLive()
    {
        return true;
    }
}

