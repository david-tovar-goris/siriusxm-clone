import { mediaPlayerMock } from "../../../servicelib/src/test/mocks/media-player.mock";

export class MediaPlayerFactoryMock
{
    getMediaPlayer = () =>
    {
        return mediaPlayerMock;
    }
}
