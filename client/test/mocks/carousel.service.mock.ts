import { of as observableOf } from "rxjs";

export const mockTileData = [
    {
        action: { actionNeriticLink: "Api:tune:aod:big80s:49d63668-ab3a-4fb3-92e7-794f2d4db35e" },
        primaryNeriticLink: { contentType: "liveAudio", channelId: "siriuslove", assetGuid: "123" },
        tileAssetGuid: { showGuid: "123" },
        subtext: { textFontColor: "", textValue: "Insight" },
        channelNumber: { textFontColor: "", textValue: "Ch 48" },
        bgImageUrl: "../../assets/mock-images/img3.jpg",
        fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/8183-1-31-00-180x180.png",
        neriticLinkData: { contentType: "liveAudio", contentSubType: '', channelId: "siriuslove" },
        superCategoryInfo: { key: "Music"},
        subCategoryInfo: { key: "pop" , channelList: []}
    },
    {
        action: { actionNeriticLink: "Api:tune:aod:big80s:49d63668-ab3a-4fb3-92e7-794f2d4db35e" },
        primaryNeriticLink: { contentType: "liveAudio", channelId: "siriuslove", assetGuid: "123" },
        tileAssetGuid: { showGuid: "234" },
        subtext: { textFontColor: "", textValue: "NPR" },
        channelNumber: { textFontColor: "", textValue: "Ch 49" },
        bgImageUrl: "../../assets/mock-images/img1.jpg",
        fgImageUrl: "http://pri.art.uat.streaming.siriusxm.com/images/channel/20170816/nprnow-1-31-00-180x180.png",
        neriticLinkData: { contentType: "aod", contentSubType: 'aod', channelId: "siriuslove" },
        superCategoryInfo: { key: "Music"},
        subCategoryInfo: { key: "pop" , channelList: []}
    }
];

export class CarouselServiceMock
{
    public isChannelTile;
    public isEpisodeTile;
    public isShowTile;
    public isVodEpisode;
    public isCategoryTile;
    public hasFgImage;
    public getImageAltText;
    public getAriaLabelContentType;

    constructor()
    {
        this.isChannelTile = jasmine.createSpy("isChannelTile");
        this.isEpisodeTile = jasmine.createSpy("isEpisodeTile");
        this.isShowTile = jasmine.createSpy("isShowTile");
        this.isVodEpisode = jasmine.createSpy("isVodEpisode");
        this.isCategoryTile = jasmine.createSpy("isCategoryTile");
        this.hasFgImage = jasmine.createSpy("hasFgImage");
        this.getImageAltText = jasmine.createSpy("getImageAltText");
        this.getAriaLabelContentType = jasmine.createSpy("getAriaLabelContentType");
    }

    static contentCarouselMockData = [
        { type: "content", guid: "0010", title: { textValue: "Continue Listening" }, additionalText: "more", tiles: mockTileData },
        { type: "content", guid: "0100", title: "Recommended For You", additionalText: "more", tiles: mockTileData }
    ];

    static heroCarouselMockData = [
        { type: "hero", guid: "0001", title: "", additionalText: "", tiles: mockTileData },
        { type: "hero", guid: "0002", title: "", additionalText: "", tiles: mockTileData }
    ];

    private static _spy = jasmine.createSpyObj("carouselServiceSpy", [
        "selectCarouselsBySuperCategory",
        "selectCarouselsBySubCategory",
        "selectCarouselsByPage",
        "getFavoriteTiles"
    ]);

    public static getSpy(): any
    {
        this._spy.carouselData = observableOf({
            zone:[
                {
                    content: CarouselServiceMock.contentCarouselMockData,
                    hero: CarouselServiceMock.heroCarouselMockData
                }
            ]
        });

        this._spy.selectCarouselsByPage = () =>
        {
            return observableOf(true);
        };
        this._spy.isChannelTile = () =>
        {
            return true;
        };
        this._spy.isAodEpisode = () =>
        {
            return true;
        };
        this._spy.isShowTile = () =>
        {
            return true;
        };
        this._spy.isCategoryTile = () =>
        {
            return true;
        };
        this._spy.isAodEpisode = () =>
        {
            return true;
        };
        this._spy.isVodEpisode = () =>
        {
            return true;
        };
        this._spy.isEpisodeTile = () =>
        {
            return true;
        };

        this._spy.isSeededRadioTile = () =>
        {
            return true;
        };

        this._spy.isAdditionalChannelTile = () =>
        {
            return true;
        };

        this._spy.isCollectionTile = () =>
        {
            return true;
        };

        this._spy.getFavoriteTiles = () => observableOf({ channels: [], shows: [], episodes: [] });
        this._spy.getImageAltText = () => '';
        this._spy.getAriaLabelContentType = () => '';
        this._spy.isViewAllTile = () => false;

        return this._spy;
    }
}
