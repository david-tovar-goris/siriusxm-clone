import * as _ from "lodash";
import { of } from "rxjs";

export const navigationServiceMock = {
    go: _.noop,
    goBack: _.noop,
    activeRoute:{
        url:'/mock/route'
    },
    routerEventUrl: of("/home/foryou")
};
