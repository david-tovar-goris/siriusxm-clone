export class ListItemContextMenuOptionsServiceMock
{
    public getChannelCMOptions;
    public getEpisodeCMOptions;
    public getShowCMOptions;

    constructor()
    {
        this.getChannelCMOptions = jasmine.createSpy("getChannelCMOptions");
        this.getEpisodeCMOptions = jasmine.createSpy("getEpisodeCMOptions");
        this.getShowCMOptions = jasmine.createSpy("getShowCMOptions");
    }
}
