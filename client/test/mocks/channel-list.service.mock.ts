import { of as observableOf } from "rxjs";
import {
    IAodEpisode,
    ICategory,
    IChannel,
    ISubCategory,
    ISuperCategory,
    IOnDemandShow,
    IVodEpisode
} from "sxmServices";
import { createUuid } from "./index";

export class ChannelListServiceMock
{
    private static _spy = jasmine.createSpyObj("channelListServiceSpy", [
        "loadLineup",
        "filter",
        "sort",
        "selectChannel",
        "selectCategory",
        "selectSuperCategory",
        "selectChannelByChannelID"
    ]);

    public static getSpy(): any
    {
        this._spy.channelStore = observableOf({
            selectedChannel      : { name: "selectedChannel" },
            selectedCategory: {
                category: {
                    name: "selectedCategory"
                }
            },
            selectedSuperCategory: { name: "selectedSuperCategory" },
            filtered             : [],
            filteredOnDemandShowsForChannels : [],
            superCategories      : [],
            categories           : [],
            channels             : [],
            liveChannels         : [{
                channelId: "channelId1"
            },
                {
                    channelId: "channelId2"
                }],
            channel: { name: "channel" },
            nextChannel: { name: "nextChannel" },
            prevChannel: { name: "prevChannel" }
        });

        return this._spy;
    }
}

export const createSuperCategory: Function = (uuid: string): ISuperCategory =>
{
    uuid                     = uuid || createUuid();
    const numberUuid: number = parseInt(uuid, 10);

    return {
        categoryGuid: `categoryGuid-${uuid}`,
        isPrimary   : false,
        key         : `key-${uuid}`,
        name        : `name-${uuid}`,
        aodShowCount: numberUuid,
        description : `description-${uuid}`,
        images      : { images: [] },
        sortOrder   : numberUuid,
        url         : `url-${uuid}`,
        categoryList: createCategories()
    };
};

export const createCategory: Function = (uuid: string): ISubCategory =>
{
    uuid                     = uuid || createUuid();
    const numberUuid: number = parseInt(uuid, 10);

    return {
        categoryGuid: `categoryGuid-${uuid}`,
        isPrimary   : false,
        key         : `key-${uuid}`,
        name        : `name-${uuid}`,
        aodShowCount: numberUuid,
        description : `description-${uuid}`,
        images      : { images: [] },
        imageList   : [ ],
        sortOrder   : numberUuid,
        url         : `url-${uuid}`,
        channelList : createChannels(),
        onDemandShows: [],
        onDemandPullTime: Date.now()
    };
};

export const createChannel: Function = (id: string, name: string = "foo"): IChannel =>
{
    id                       = id || createUuid();
    name                     = name ? `${name}-${id}` : `name-${id}`;
    const numberUuid: number = parseInt(id, 10);

    return {
        channelNumber         : numberUuid,
        clientBufferDuration  : numberUuid,
        disableRecommendations: false,
        geoRestrictions       : numberUuid,
        aodShowCount          : numberUuid,
        inactivityTimeout     : numberUuid,
        isAvailable           : true,
        isBizMature           : false,
        isFavorite            : false,
        isMature              : false,
        isMySxm               : false,
        isPersonalized        : false,
        isPlayByPlay          : false,
        mediumDescription     : `mediumDescription-${id}`,
        name                  : name,
        satOnly               : false,
        subscribed            : true,
        shortDescription      : `shortDescription-${id}`,
        siriusChannelNumber   : numberUuid,
        sortOrder             : numberUuid,
        spanishContent        : false,
        streamingName         : `streamingName-${id}`,
        url                   : `url-${id}`,
        xmChannelNumber       : numberUuid,
        xmServiceId           : numberUuid,
        tuneMethod            : "",
        firstSuperCategory    : { key: "Entertainment" } as ISuperCategory,
        firstSubCategory      : { key: "howard", name: "pop" } as ISubCategory,
        isGeoRestricted: false
    };
};

export const createEpisodes: Function = (id: string,
                                         episodeCount: number = 10,
                                         name: string): Array<IAodEpisode> =>
{
    id                       = id || createUuid();
    name                     = name ? `${name}-${id}` : `name-${id}`;
    const numberUuid: number = parseInt(id, 10);

    let episodes: Array<IAodEpisode> = [];
    for (let i: number = 0; i < episodeCount; i++)
    {
        const episode: IAodEpisode = {
            type             : "episode",
            episodeGuid      : name,
            baseTime         : "",
            disableAllBanners: false,
            expiringSoon     : false,
            featured         : false,
            featuredPriority : false,
            highlighted      : false,
            hot              : false,
            longDescription  : `longDescription-${id}`,
            shortDescription : `shortDescription-${id}`,
            longTitle        : `longTitle-${id}`,
            mediumTitle      : `mediumTitle-${id}`,
            originalAirDate  : "2017-10-28T10:00:00.000+0000",
            percentConsumed  : numberUuid,
            programType      : `programType-${id}`,
            special          : false,
            valuable         : false,
            duration         : `duration-${id}`,
            contentUrlList   : [],
            contextualBanners: [],
            dmcaInfo         : null,
            drmInfo          : null,
            hosts            : [],
            publicationInfo  : null,
            show             : {
                relatedChannelIds: [`channel-${id}`],
                assetGUID: `asset-${id}`
            },
            isFavorite       : false
        } as IAodEpisode;
        episodes.push(episode);
    }

    return episodes;
};

export const createVODEpisodes: Function = (id: string,
                                         vodEpisodeCount: number = 10,
                                         name: string): Array<IVodEpisode> =>
{
    id                       = id || createUuid();
    name                     = name ? `${name}-${id}` : `name-${id}`;
    const numberUuid: number = parseInt(id, 10);

    let episodes: Array<IVodEpisode> = [];
    for (let i: number = 0; i < vodEpisodeCount; i++)
    {
        const episode: IVodEpisode = {
            allowDownload         : false,
            disableAutoBanners    : false,
            disableRecommendations: false,
            hosts                 : `hosts -${id}`,
            hostList              : [],
            topics                : `topics -${id}`,
            topicList             : [],
            rating                : ``,
            offlinePlayback       : null,
            disableAllBanners     : false,
            episodeGuid           : name,
            show                  : {
                relatedChannelIds : [`channel-${id}`],
                assetGUID         : `asset -${id}`
            },
            longDescription       : `longDescription-${id}`,
            shortDescription      : `shortDescription-${id}`,
            longTitle             : `longTitle-${id}`,
            mediumTitle           : `mediumTitle-${id}`,
            isFavorite            : false
        } as IVodEpisode;

        episodes.push(episode);
    }

    return episodes;
};

export const createShowsAndEpisodes: Function = (id: string,
                                                 showCount: number    = 10,
                                                 episodeCount: number = 10,
                                                 name: string         = "foo"): Array<IOnDemandShow> =>
{
    id   = id || createUuid();
    name = name ? `${name}-${id}` : `name-${id}`;

    let shows: Array<IOnDemandShow> = [];

    for (let i: number = 0; i < showCount; i++)
    {
        const show: IOnDemandShow =
                  {
                      type             : `show-${id}`,
                      disableAllBanners: false,
                      episodeCount     : episodeCount,
                      newEpisodeCount  : episodeCount,
                      showDescription  : {
                          connectInfo      : null,
                          guid             : `guid-${id}`,
                          images           : [],
                          shortId          : `shortId-${id}`,
                          longDescription  : `longDescription-${id}`,
                          shortDescription : `shortDescription-${id}`,
                          relatedChannelIds: ["channelId"],
                          longTitle        : `longTitle-${id}`,
                          mediumTitle      : `mediumTitle-${id}`,
                          programType      : `programType-${id}`
                      },
                      aodEpisodes      : createEpisodes(id, episodeCount, name),
                      vodEpisodes      : [],
                      isFavorite       : false,
                      relatedChannelIds : [`channel-${id}`],
                      assetGUID         : `assetGuid-${id}`
                  } as IOnDemandShow;
        shows.push(show);
    }
    return shows;
};


export const createSuperCategories: Function = (count: number = 10): Array<ISuperCategory> =>
{
    let result: Array<ISuperCategory> = [];

    for (let i: number = 0; i < count; i++)
    {
        result.push(createSuperCategory(i));
    }
    return result;
};

export const createCategories: Function = (count: number = 10): Array<ICategory> =>
{
    let result: Array<ICategory> = [];

    for (let i: number = 0; i < count; i++)
    {
        result.push(createCategory(i));
    }
    return result;
};

export const createChannels: Function = (count: number        = 10,
                                         name: string         = "foo",
                                         showCount: number    = 3,
                                         episodeCount: number = 3): Array<IChannel> =>
{
    let result: Array<IChannel> = [];

    for (let i: number = 0; i < count; i++)
    {
        let channel   = createChannel(i, name);
        channel.shows = createShowsAndEpisodes(i, showCount, episodeCount, name);
        result.push(channel);
    }
    return result;
};
