import { BehaviorSubject, Observable, of as observableOf } from "rxjs";
import { OnBoardingSettings } from "../../../servicelib/src/config/interfaces/onboarding-configuration";
import {IComponentConfig} from "sxmServices";

export class ConfigServiceMock
{
    public nuDetect$ = jasmine.createSpy('nuDetect').and.returnValue(observableOf({
        settings: [{
            enableNudetect: true
        }]
    }));

    public getUrlConfiguration = function(){};
    public getConfig = jasmine.createSpy('getConfig').and.returnValue(observableOf(true));
    public normalizeRelativeUrlConfig = jasmine.createSpy('normalizeRelativeUrlConfig');
    public configurationSubject = {
        components: [
            {
                name: "ForgotLink",
                settings: [
                    {
                        enableForgotLink: true
                    }
                ],
                errors: []
            },
            {
                name: "OnBoarding",
                settings: [
                    {
                        disableOnboarding: true
                    }
                ],
                errors: []
            },
            {
                name: "FAQURL",
                settings: [
                    {
                        "urlSettings": [
                            {
                                "name": "URL",
                                "value": "https://listenercare.siriusxm.com/app/answers/list/p/908",
                                "type": "PAGE"
                            }
                        ],
                        "platform": "WEB",
                        "locale": "en"
                    }
                ],
                errors: []
            },
            {
                name: "FeedbackUrl",
                settings: [
                    {
                        "urlSettings": [
                            {
                                "name": "URL",
                                "value": "https://listenercare.siriusxm.com/app/feedback/incidents.c$media_player_version/620"
                            }
                        ],
                        "locale": "en"
                    },
                    {
                        "urlSettings": [
                            {
                                "name": "URL",
                                "value": "https://listenercare.siriusxm.com/app/feedback/incidents.c$media_player_version/620"
                            }
                        ],
                        "locale": "es"
                    }
                ],
                errors: []
            },
            {
                name: "OAC",
                settings: [
                    {
                        "urlSettings": [
                            {
                                "name": "URL",
                                "value": "https://care.siriusxm.com/login_view.action"
                            }
                        ],
                        "locale": "en"
                    },
                    {
                        "stringSetting": {
                            "name": "maxProfiles",
                            "value": "5"
                        }
                    }
                ],
                errors: []
            },
            {
                name: "PrivacyPolicy",
                settings: [
                    {
                        "urlSettings": [
                            {
                                "name": "URL",
                                "value": "http://www.siriusxm.com/pdf/siriusxm_privacypolicy_eng.pdf",
                                "type": "PDF"
                            }
                        ],
                        "platform": "WEB",
                        "locale": "en"
                    },
                    {
                        "urlSettings": [
                            {
                                "name": "URL",
                                "value": "http://www.siriusxm.com/pdf/siriusxm_privacypolicy_span.pdf",
                                "type": "PDF"
                            }
                        ],
                        "platform": "WEB",
                        "locale": "es"
                    }
                ],
                errors: []
            },
            {
                name: "CustomerAgreement",
                settings: [
                    {
                        "urlSettings": [
                            {
                                "name": "URL",
                                "value": "http://www.siriusxm.com/pdf/siriusxm_customeragreement_eng.pdf",
                                "type": "PDF"
                            }
                        ],
                        "platform": "WEB",
                        "locale": "en"
                    },
                    {
                        "urlSettings": [
                            {
                                "name": "URL",
                                "value": "http://www.siriusxm.com/pdf/siriusxm_customeragreement_span.pdf",
                                "type": "PDF"
                            }
                        ],
                        "platform": "WEB",
                        "locale": "es"
                    }
                ],
                errors: []
            }
        ]
    };

    public configurationBehaviorSubject = {
        subscribe: (result: Function, fault: Function) => result(this.configurationSubject)
    };
    public configuration = observableOf(this.configurationSubject);
    public liveVideoEnabled = () => true;
    public getSeededRadioFallbackUrl = () => "";
    public getOnBoardingSettings = (lang: string) => { return {};};
    public getOpenAccessCopy = (copy) => true;
}
