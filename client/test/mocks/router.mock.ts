import { of as observableOf } from "rxjs" ;

export class RouterStub
{
    url: string = "Home";

    navigate()
    {
        return Promise.resolve(true);
    }

    isActive(url:string,exact:boolean)
    {
        return false;
    }

    navigateByUrl()
    {
        return Promise.resolve(true);
    }

    events = observableOf({url : "Home"});
}

const param = {  "type" : "video" };

export class ActivatedRouteStub
{
   public route = { params : observableOf(param) };
}
