export class LoginServiceMock
{
    public appDownloadUrls = {android: 'android-url', apple: 'apple-url', windows: 'windows-url'};
    public getStartedUrl = "getStartedUrl";
    public customerAgreementUrl = "customerAgreementUrl";
    public privacyPolicyUrl = "privacyPolicyUrl";
    public forgotPasswordUrl = "forgotPasswordUrl";
    public dataPrivacyUrl = "dataPrivacyUrl";
    public userSession = { username: '' };
    public disableLoginButton = (username = '', password = '') => username.length < 2 || password.length < 6 ;
    public login = jasmine.createSpy('login');
    public getTarget = jasmine.createSpy('').and.returnValue(true);
    public clearLoginErrorMessage = jasmine.createSpy('clearLoginErrorMessage');
}
