import { SxmCliPocPage } from "./app.po";

describe("sxm-cli-poc App", () =>
{
    let page: SxmCliPocPage;

    beforeEach(() =>
    {
        page = new SxmCliPocPage();
    });

    it("should display welcome message", () =>
    {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual("Welcome to app!!");
    });
});
